.class public Ly/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ly/b;


# instance fields
.field private A:Ljava/util/Set;

.field private final B:Ljava/util/List;

.field private C:Ljava/util/Iterator;

.field private final D:Ljava/util/Comparator;

.field private E:LZ/a;

.field private F:I

.field private final G:Ljava/util/Map;

.field private final b:Lcom/google/android/maps/driveabout/vector/aV;

.field private volatile c:LG/a;

.field private final d:LD/c;

.field private e:Lo/h;

.field private f:LC/a;

.field private final g:LD/a;

.field private h:F

.field private i:Lo/aS;

.field private j:Lcom/google/android/maps/driveabout/vector/aF;

.field private k:Ljava/util/Iterator;

.field private l:Ljava/util/ArrayList;

.field private m:I

.field private n:Ljava/util/ArrayList;

.field private o:I

.field private final p:Ljava/util/Map;

.field private q:I

.field private r:F

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private volatile w:Z

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ly/b;

    invoke-direct {v0}, Ly/b;-><init>()V

    sput-object v0, Ly/d;->a:Ly/b;

    return-void
.end method

.method public constructor <init>(LG/a;LD/a;Landroid/content/res/Resources;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ly/d;->B:Ljava/util/List;

    new-instance v0, Ly/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ly/f;-><init>(Ly/e;)V

    iput-object v0, p0, Ly/d;->D:Ljava/util/Comparator;

    const/4 v0, 0x0

    iput v0, p0, Ly/d;->F:I

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ly/d;->G:Ljava/util/Map;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iput-object p1, p0, Ly/d;->c:LG/a;

    iput-object p2, p0, Ly/d;->g:LD/a;

    new-instance v0, LD/c;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, LD/c;-><init>(I)V

    iput-object v0, p0, Ly/d;->d:LD/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iput v2, p0, Ly/d;->m:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    iput v2, p0, Ly/d;->o:I

    return-void
.end method

.method static final a(LC/a;)I
    .locals 4

    const v3, 0x48435000

    invoke-virtual {p0}, LC/a;->m()F

    move-result v0

    invoke-virtual {p0}, LC/a;->l()I

    move-result v1

    invoke-virtual {p0}, LC/a;->k()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v0

    div-float v0, v1, v0

    cmpl-float v1, v0, v3

    if-lez v1, :cond_0

    sub-float/2addr v0, v3

    const v1, 0x38d1b717

    mul-float/2addr v0, v1

    const/high16 v1, 0x42300000

    add-float/2addr v0, v1

    :goto_0
    float-to-int v0, v0

    return v0

    :cond_0
    const v1, 0x3966afcd

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method static a(Lo/H;Lo/aj;)I
    .locals 6

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v1

    :cond_0
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->f()I

    move-result v0

    :goto_1
    move v2, v1

    :goto_2
    invoke-virtual {p0}, Lo/H;->b()I

    move-result v3

    if-ge v1, v3, :cond_5

    invoke-virtual {p0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v4

    invoke-virtual {v4}, Lo/I;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lo/I;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lo/I;->j()Lo/aj;

    move-result-object v3

    invoke-virtual {v3}, Lo/aj;->e()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lo/I;->j()Lo/aj;

    move-result-object v3

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    invoke-virtual {v3}, Lo/ao;->f()I

    move-result v3

    :goto_3
    invoke-virtual {v4}, Lo/I;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/2addr v3, v5

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {v4}, Lo/I;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v2, v2, 0x8

    :cond_2
    invoke-virtual {v4}, Lo/I;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    int-to-float v2, v2

    invoke-virtual {v4}, Lo/I;->k()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/16 v0, 0xa

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    move v3, v0

    goto :goto_3
.end method

.method static a(Lo/n;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lo/n;->h()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v0

    :cond_0
    :goto_0
    return v1

    :pswitch_1
    check-cast p0, Lo/af;

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lo/af;->d()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lo/af;->c(I)Lo/H;

    move-result-object v2

    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v3

    invoke-static {v2, v3}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    check-cast p0, Lo/U;

    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v0

    invoke-virtual {p0}, Lo/U;->e()Lo/aj;

    move-result-object v1

    invoke-static {v0, v1}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v0

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    invoke-virtual {p0}, Lo/U;->e()Lo/aj;

    move-result-object v2

    invoke-static {v1, v2}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v1

    add-int/2addr v1, v0

    goto :goto_0

    :pswitch_3
    check-cast p0, Lo/K;

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lo/K;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lo/K;->a(I)Lo/H;

    move-result-object v2

    invoke-virtual {p0}, Lo/K;->e()Lo/aj;

    move-result-object v3

    invoke-static {v2, v3}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_4
    move v1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lo/K;Ly/b;Z)LF/m;
    .locals 11

    const/4 v0, 0x0

    invoke-virtual {p1}, Lo/K;->c()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lo/K;->b()Lo/X;

    move-result-object v1

    invoke-virtual {v1}, Lo/X;->a()Lo/ad;

    move-result-object v2

    iget-object v3, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v3, v2}, Lo/aS;->b(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Ly/d;->a(Lo/X;)Lo/X;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lo/K;->e()Lo/aj;

    move-result-object v0

    iget-object v1, p0, Ly/d;->c:LG/a;

    iget v1, v1, LG/a;->i:F

    iget-object v2, p0, Ly/d;->c:LG/a;

    iget v2, v2, LG/a;->j:I

    iget-object v4, p0, Ly/d;->c:LG/a;

    iget v4, v4, LG/a;->k:I

    iget-object v5, p0, Ly/d;->f:LC/a;

    invoke-virtual {v5}, LC/a;->m()F

    move-result v5

    invoke-static {v0, v1, v2, v4, v5}, LF/m;->a(Lo/aj;FIIF)F

    move-result v5

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lo/K;->a(I)Lo/H;

    move-result-object v2

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-object v6, v0, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v7, v0, LG/a;->l:F

    iget-object v8, p0, Ly/d;->f:LC/a;

    iget-object v9, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-boolean v10, v0, LG/a;->q:Z

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v10}, LF/D;->a(Lo/K;Ly/b;Lo/H;Lo/X;ZFLcom/google/android/maps/driveabout/vector/aX;FLC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lo/U;Ly/b;Z)LF/m;
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p1}, Lo/U;->o()[Lo/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lo/a;->b()Lo/T;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ly/d;->a(Lo/T;Ly/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lo/U;->k()F

    move-result v1

    iget v2, p0, Ly/d;->h:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Lo/U;->n()F

    move-result v1

    const/high16 v2, -0x40800000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lo/U;->n()F

    move-result v1

    iget v2, p0, Ly/d;->h:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    invoke-direct {p0, p1}, Ly/d;->a(Lo/U;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Ly/d;->f:LC/a;

    iget-object v4, p0, Ly/d;->d:LD/c;

    iget-object v5, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v6, p0, Ly/d;->c:LG/a;

    iget-object v7, p0, Ly/d;->E:LZ/a;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v7}, LF/L;->a(Lo/U;Ly/b;ZLC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;LZ/a;)LF/L;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lo/X;)Lo/X;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Ly/d;->e:Lo/h;

    invoke-virtual {v0, p1, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-ne v6, v1, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->d()F

    move-result v2

    move v4, v1

    move-object v3, v0

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->d()F

    move-result v1

    cmpl-float v0, v1, v2

    if-lez v0, :cond_3

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method static a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    sget-object v0, Ly/d;->a:Ly/b;

    new-instance v2, Lo/k;

    invoke-direct {v2, v1}, Lo/k;-><init>(I)V

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/b;

    new-instance v3, Lo/k;

    add-int/lit8 v4, v1, -0x1

    invoke-direct {v3, v4}, Lo/k;-><init>(I)V

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/b;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/j;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ly/b;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/k;

    invoke-virtual {v2, v1}, Lo/k;->a(Lo/j;)V

    goto :goto_1
.end method

.method private a(Lo/K;Ly/b;ZZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Ly/d;->a(Lo/K;Ly/b;Z)LF/m;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->c(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LF/m;->a_(Z)V

    :cond_0
    return-void
.end method

.method private a(Lo/U;Ly/b;ZZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Ly/d;->a(Lo/U;Ly/b;Z)LF/m;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->c(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LF/m;->a_(Z)V

    :cond_0
    return-void
.end method

.method private a(Lo/af;Ly/b;ZZ)V
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lo/af;->b()Lo/X;

    move-result-object v1

    invoke-virtual {v1}, Lo/X;->a()Lo/ad;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ly/d;->i:Lo/aS;

    invoke-virtual {v3, v2}, Lo/aS;->b(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Ly/d;->a(Lo/X;)Lo/X;

    move-result-object v15

    if-eqz v15, :cond_3

    invoke-virtual {v15}, Lo/X;->d()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Ly/d;->f:LC/a;

    invoke-virtual {v2}, LC/a;->y()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ly/d;->f:LC/a;

    invoke-virtual {v3}, LC/a;->m()F

    move-result v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42200000

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    const/4 v2, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lo/af;->d()I

    move-result v1

    if-ge v2, v1, :cond_3

    const v1, 0x3f333333

    invoke-virtual {v15, v1}, Lo/X;->a(F)Lo/T;

    move-result-object v4

    const v1, 0x3e99999a

    invoke-virtual {v15, v1}, Lo/X;->a(F)Lo/T;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Ly/d;->c:LG/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Ly/d;->f:LC/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Ly/d;->d:LD/c;

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move/from16 v6, p3

    invoke-static/range {v1 .. v9}, LF/L;->a(Lo/af;ILy/b;Lo/T;Lo/T;ZLG/a;LC/a;LD/c;)LF/L;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Ly/d;->e()F

    move-result v9

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-boolean v1, v1, LG/a;->o:Z

    if-eqz v1, :cond_2

    const/16 v7, 0xa

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lo/af;->c(I)Lo/H;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget v10, v1, LG/a;->l:F

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-object v11, v1, LG/a;->a:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget-object v12, v0, Ly/d;->f:LC/a;

    move-object/from16 v0, p0

    iget-object v13, v0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-boolean v14, v1, LG/a;->q:Z

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object v6, v15

    move/from16 v8, p3

    invoke-static/range {v3 .. v14}, LF/D;->a(Lo/af;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v1

    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Ly/d;->c(LF/m;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p4, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LF/m;->a_(Z)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method private a(Lo/n;Ly/b;ZZ)V
    .locals 1

    invoke-direct {p0, p2}, Ly/d;->a(Ly/b;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lo/af;

    if-eqz v0, :cond_2

    check-cast p1, Lo/af;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/af;Ly/b;ZZ)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lo/K;

    if-eqz v0, :cond_3

    check-cast p1, Lo/K;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/K;Ly/b;ZZ)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lo/U;

    if-eqz v0, :cond_0

    check-cast p1, Lo/U;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/U;Ly/b;ZZ)V

    goto :goto_0
.end method

.method private a(J)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot run labeler loop until all previous labels have either been copied into new label table or destroyed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    invoke-direct {p0, v0, v7, v4, v4}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    :cond_1
    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Ly/d;->s:I

    iget v1, p0, Ly/d;->q:I

    if-ge v0, v1, :cond_3

    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    :cond_2
    :goto_0
    return v2

    :cond_3
    move v1, v2

    :goto_1
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    if-nez v0, :cond_4

    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_4
    iget v0, p0, Ly/d;->s:I

    iget v3, p0, Ly/d;->q:I

    if-lt v0, v3, :cond_5

    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v3, p0, Ly/d;->m:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->b()Ly/c;

    move-result-object v3

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    if-gt v0, v3, :cond_2

    :cond_5
    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_6

    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    goto :goto_0

    :cond_6
    if-lez v1, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    cmp-long v0, v5, p1

    if-ltz v0, :cond_7

    move v2, v4

    goto :goto_0

    :cond_7
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    if-nez v0, :cond_9

    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v0

    iget-object v3, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->b()Ly/c;

    move-result-object v3

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    invoke-virtual {v0}, Ly/c;->b()I

    move-result v5

    if-ne v3, v5, :cond_8

    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v3

    iget-object v5, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    iget-object v0, p0, Ly/d;->B:Ljava/util/List;

    iget-object v3, p0, Ly/d;->D:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    :cond_9
    :goto_3
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    add-int/lit8 v3, v1, 0x1

    if-lez v1, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_a

    move v2, v4

    goto/16 :goto_0

    :cond_a
    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_b

    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    iget v1, p0, Ly/d;->s:I

    iget v5, p0, Ly/d;->q:I

    if-lt v1, v5, :cond_c

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v5, p0, Ly/d;->m:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    invoke-virtual {v1}, LF/m;->t()I

    move-result v1

    invoke-virtual {v0}, Ly/c;->b()I

    move-result v5

    if-lt v1, v5, :cond_c

    move v0, v3

    :goto_4
    iget-object v1, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object v7, p0, Ly/d;->C:Ljava/util/Iterator;

    move v1, v0

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v1

    invoke-virtual {v0}, Ly/c;->c()Ly/b;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2, v4}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    iget v0, p0, Ly/d;->s:I

    iget v1, p0, Ly/d;->q:I

    if-le v0, v1, :cond_d

    iget v0, p0, Ly/d;->m:I

    invoke-direct {p0, v0}, Ly/d;->c(I)V

    :cond_d
    move v1, v3

    goto :goto_3

    :cond_e
    move v0, v1

    goto :goto_4
.end method

.method private a(LF/m;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Ly/d;->h:F

    invoke-virtual {p1}, LF/m;->r()F

    move-result v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->a(Ly/b;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v0

    instance-of v0, v0, Lo/U;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v0

    check-cast v0, Lo/U;

    invoke-direct {p0, v0}, Ly/d;->a(Lo/U;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LF/m;->s()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    iget v0, p0, Ly/d;->h:F

    invoke-virtual {p1}, LF/m;->s()F

    move-result v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private static a(LF/m;LF/m;)Z
    .locals 3

    invoke-virtual {p0}, LF/m;->v()Lo/n;

    move-result-object v0

    invoke-interface {v0}, Lo/n;->a()Lo/o;

    move-result-object v0

    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v1

    invoke-interface {v1}, Lo/n;->a()Lo/o;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    instance-of v2, v0, Lo/p;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lo/p;

    if-eqz v2, :cond_0

    sget-object v2, Lo/o;->a:Lo/o;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lo/T;Ly/b;)Z
    .locals 1

    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v0, p1}, Lo/aS;->a(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Ly/d;->b(Lo/T;Ly/b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lo/U;)Z
    .locals 2

    invoke-virtual {p1}, Lo/U;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    iget v1, p0, Ly/d;->t:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Ly/d;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ly/b;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ly/d;->A:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ly/b;)Lo/j;
    .locals 2

    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p1, :cond_2

    sget-object p1, Ly/d;->a:Ly/b;

    :cond_2
    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    if-nez v0, :cond_0

    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    sget-object v1, Ly/d;->a:Ly/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    invoke-direct {p0}, Ly/d;->g()V

    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v3, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v0, v3}, LF/m;->a(Lo/aS;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v0}, Ly/d;->g(LF/m;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    invoke-direct {p0, v0}, Ly/d;->d(LF/m;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v0}, Ly/d;->e(LF/m;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v3, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v3}, LF/m;->b(LD/a;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Ly/d;->o:I

    return-void
.end method

.method private b(J)Z
    .locals 5

    :try_start_0
    invoke-direct {p0, p1, p2}, Ly/d;->a(J)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    throw v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "#:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ly/d;->F:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Ly/d;->F:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " T:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " numL:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "Labeler.runLabeler"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LF/m;)Z
    .locals 2

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-boolean v0, v0, LG/a;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {p1}, LF/m;->o()Lo/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aS;->b(Lo/ae;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {p1, v0}, LF/m;->a(Lo/aS;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(LF/m;LF/m;)Z
    .locals 1

    instance-of v0, p0, LF/L;

    if-eqz v0, :cond_0

    instance-of v0, p1, LF/L;

    if-eqz v0, :cond_0

    check-cast p1, LF/L;

    invoke-virtual {p1}, LF/L;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, LF/L;

    invoke-virtual {p0}, LF/L;->y()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lo/T;Ly/b;)Z
    .locals 1

    invoke-direct {p0, p2}, Ly/d;->b(Ly/b;)Lo/j;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lo/j;->a(Lo/T;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-direct {p0, v0}, Ly/d;->f(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Ly/d;->s:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ly/d;->s:I

    :cond_0
    iget-object v1, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v1}, LF/m;->b(LD/a;)V

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {v0}, LF/m;->u()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Ly/d;->m:I

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Ly/d;->f()V

    :cond_1
    return-void
.end method

.method private c(LF/m;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_3

    invoke-static {v0, p1}, Ly/d;->b(LF/m;LF/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v3}, Ly/d;->c(I)V

    :cond_0
    iget-object v0, p0, Ly/d;->f:LC/a;

    iget-object v3, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0, v3}, LF/m;->b(LC/a;LD/a;)Z

    invoke-direct {p0, p1}, Ly/d;->g(LF/m;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Ly/d;->d(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    iget-object v3, p0, Ly/d;->f:LC/a;

    iget-object v4, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v3, v4}, LF/m;->a(LC/a;LD/a;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, p1}, Ly/d;->g(LF/m;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Ly/d;->d(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0}, LF/m;->b(LD/a;)V

    :cond_4
    :goto_1
    return v1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    if-eqz v0, :cond_8

    iget-object v0, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0}, LF/m;->b(LD/a;)V

    goto :goto_1

    :cond_8
    invoke-direct {p0, p1}, Ly/d;->e(LF/m;)V

    move v1, v2

    goto :goto_1
.end method

.method private d(LF/m;)Z
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, LF/m;->t()I

    move-result v6

    invoke-virtual {p1}, LF/m;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p1}, LF/m;->n()Lo/ae;

    move-result-object v7

    invoke-virtual {v7}, Lo/ae;->a()Lo/ad;

    move-result-object v8

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v3

    :goto_1
    if-ge v5, v9, :cond_7

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Ly/d;->a(LF/m;LF/m;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, LF/m;->r()F

    move-result v4

    invoke-virtual {v0}, LF/m;->r()F

    move-result v10

    cmpl-float v4, v4, v10

    if-lez v4, :cond_2

    invoke-direct {p0, v5}, Ly/d;->c(I)V

    :cond_0
    :goto_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LF/m;->r()F

    move-result v4

    invoke-virtual {v0}, LF/m;->r()F

    move-result v10

    cmpg-float v4, v4, v10

    if-gez v4, :cond_4

    :cond_3
    :goto_3
    return v2

    :cond_4
    invoke-virtual {v0}, LF/m;->n()Lo/ae;

    move-result-object v4

    invoke-virtual {v4}, Lo/ae;->a()Lo/ad;

    move-result-object v10

    invoke-virtual {v10, v8}, Lo/ad;->a(Lo/ae;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v4, v7}, Lo/ae;->a(Lo/ae;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, LF/m;->w()Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v2

    :goto_4
    if-gt v1, v4, :cond_5

    if-ne v1, v4, :cond_3

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    if-le v6, v0, :cond_3

    :cond_5
    invoke-direct {p0, v5}, Ly/d;->c(I)V

    goto :goto_2

    :cond_6
    move v4, v3

    goto :goto_4

    :cond_7
    move v2, v3

    goto :goto_3
.end method

.method private e(LF/m;)V
    .locals 3

    invoke-direct {p0, p1}, Ly/d;->f(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ly/d;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ly/d;->s:I

    iget v0, p0, Ly/d;->m:I

    if-ltz v0, :cond_0

    invoke-virtual {p1}, LF/m;->t()I

    move-result v1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v2, p0, Ly/d;->m:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    if-ge v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Ly/d;->m:I

    :cond_1
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, LF/m;->x()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/i;

    iget-object v2, p0, Ly/d;->g:LD/a;

    invoke-virtual {v2}, LD/a;->l()Lz/k;

    move-result-object v2

    invoke-virtual {v2, v0}, Lz/k;->a(Lz/i;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private f()V
    .locals 4

    const v1, 0x7fffffff

    const/4 v0, -0x1

    iput v0, p0, Ly/d;->m:I

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LF/m;->t()I

    move-result v3

    if-ge v3, v2, :cond_0

    invoke-direct {p0, v0}, Ly/d;->f(LF/m;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, LF/m;->t()I

    move-result v2

    iput v1, p0, Ly/d;->m:I

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private f(LF/m;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, LF/m;->m()F

    move-result v1

    iget v2, p0, Ly/d;->r:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ly/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Ly/d;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    iget-object v4, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v4}, LF/m;->b(LD/a;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Ly/d;->m:I

    iput-boolean v2, p0, Ly/d;->w:Z

    iput-boolean v2, p0, Ly/d;->x:Z

    iput-boolean v2, p0, Ly/d;->y:Z

    :cond_2
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    iput-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    iput-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v0, p0, Ly/d;->o:I

    iget v1, p0, Ly/d;->m:I

    iput v1, p0, Ly/d;->o:I

    iput v0, p0, Ly/d;->m:I

    iput v2, p0, Ly/d;->s:I

    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private g(LF/m;)Z
    .locals 2

    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->b(Ly/b;)Lo/j;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LF/m;->n()Lo/ae;

    move-result-object v1

    invoke-interface {v0, v1}, Lo/j;->a(Lo/ae;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LF/T;LC/a;)LF/m;
    .locals 1

    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-static {p1, v0, p2}, LF/L;->a(LF/T;Lcom/google/android/maps/driveabout/vector/aV;LC/a;)LF/L;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    iget-object v2, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v2}, LF/m;->b(LD/a;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    iget-object v0, p0, Ly/d;->d:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, LF/m;->a(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(LC/a;Lo/aS;ILjava/util/Iterator;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILA/c;)V
    .locals 10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move/from16 v0, p9

    int-to-long v3, v0

    add-long/2addr v3, v1

    iput-object p1, p0, Ly/d;->f:LC/a;

    iput-object p4, p0, Ly/d;->k:Ljava/util/Iterator;

    iput-object p5, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    iput-object p2, p0, Ly/d;->i:Lo/aS;

    iput p3, p0, Ly/d;->t:I

    move-object/from16 v0, p7

    iput-object v0, p0, Ly/d;->A:Ljava/util/Set;

    new-instance v1, Lo/h;

    invoke-virtual {p2}, Lo/aS;->c()Lo/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lo/h;-><init>(Lo/ae;)V

    iput-object v1, p0, Ly/d;->e:Lo/h;

    iget-object v1, p0, Ly/d;->G:Ljava/util/Map;

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Ly/d;->a(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {p1}, Ly/d;->a(LC/a;)I

    move-result v1

    iget v2, p0, Ly/d;->q:I

    if-eq v1, v2, :cond_0

    iput v1, p0, Ly/d;->q:I

    iget-object v2, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/vector/aV;->a(I)V

    :cond_0
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->m()F

    move-result v1

    iget-object v2, p0, Ly/d;->f:LC/a;

    invoke-virtual {v2}, LC/a;->m()F

    move-result v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43480000

    mul-float/2addr v1, v2

    iput v1, p0, Ly/d;->r:F

    invoke-direct {p0}, Ly/d;->g()V

    iget-object v1, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Ly/d;->C:Ljava/util/Iterator;

    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->s()F

    move-result v1

    iput v1, p0, Ly/d;->h:F

    iget v1, p0, Ly/d;->t:I

    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v2

    iget-object v7, p0, Ly/d;->f:LC/a;

    invoke-virtual {v7}, LC/a;->h()Lo/T;

    move-result-object v7

    move-object/from16 v0, p10

    invoke-virtual {v2, v7, v0}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/bE;->b()I

    move-result v2

    if-ge v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Ly/d;->u:Z

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_7

    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, LF/m;->v()Lo/n;

    move-result-object v7

    invoke-interface {v7}, Lo/n;->a()Lo/o;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-direct {p0, v1}, Ly/d;->a(LF/m;)Z

    move-result v7

    if-nez v7, :cond_4

    :cond_1
    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    :cond_2
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v7, p0, Ly/d;->f:LC/a;

    iget-object v8, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7, v8}, LF/m;->b(LC/a;LD/a;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v1}, Ly/d;->b(LF/m;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v1}, Ly/d;->g(LF/m;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-direct {p0, v1}, Ly/d;->d(LF/m;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-direct {p0, v1}, Ly/d;->e(LF/m;)V

    goto :goto_2

    :cond_5
    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_8

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    invoke-virtual {v1}, LF/m;->v()Lo/n;

    move-result-object v7

    invoke-virtual {v1}, LF/m;->q()Ly/b;

    move-result-object v8

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v1

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v1, v9}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_8
    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v1, -0x1

    iput v1, p0, Ly/d;->o:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Ly/d;->x:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Ly/d;->y:Z

    invoke-direct {p0, v3, v4}, Ly/d;->b(J)Z

    move-result v1

    iput-boolean v1, p0, Ly/d;->v:Z

    return-void
.end method

.method public a(LG/a;)V
    .locals 1

    iget-object v0, p0, Ly/d;->c:LG/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Ly/d;->c:LG/a;

    invoke-virtual {p0}, Ly/d;->b()V

    :cond_0
    return-void
.end method

.method public a(LZ/a;)V
    .locals 0

    iput-object p1, p0, Ly/d;->E:LZ/a;

    return-void
.end method

.method public a(Lo/aS;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ly/d;->g()V

    invoke-virtual {p1}, Lo/aS;->a()Lo/aR;

    move-result-object v3

    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    if-eqz v0, :cond_0

    iget-object v5, p0, Ly/d;->f:LC/a;

    iget-object v6, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v5, v6}, LF/m;->b(LC/a;LD/a;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, LF/m;->o()Lo/ad;

    move-result-object v5

    invoke-virtual {v3, v5}, Lo/aR;->b(Lo/ae;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v0}, Ly/d;->e(LF/m;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v5}, LF/m;->b(LD/a;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Ly/d;->o:I

    iput-boolean v7, p0, Ly/d;->x:Z

    iput-boolean v7, p0, Ly/d;->y:Z

    iput-boolean v2, p0, Ly/d;->v:Z

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    iget-object v0, p0, Ly/d;->d:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly/d;->w:Z

    return-void
.end method

.method public b(I)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-boolean v2, p0, Ly/d;->x:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Ly/d;->y:Z

    if-eqz v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Ly/d;->y:Z

    invoke-direct {p0, v2}, Ly/d;->b(Z)V

    iput-boolean v4, p0, Ly/d;->x:Z

    iput-boolean v4, p0, Ly/d;->y:Z

    :cond_1
    invoke-direct {p0, v0, v1}, Ly/d;->b(J)Z

    move-result v0

    iput-boolean v0, p0, Ly/d;->v:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Ly/d;->v:Z

    return v0
.end method

.method public d()Ly/g;
    .locals 2

    new-instance v0, Ly/g;

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ly/g;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method e()F
    .locals 3

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v0, v0, LG/a;->c:I

    int-to-float v0, v0

    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    const/high16 v2, 0x41680000

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v0, v0, LG/a;->b:I

    int-to-float v0, v0

    iget-boolean v2, p0, Ly/d;->u:Z

    if-nez v2, :cond_0

    add-float/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method
