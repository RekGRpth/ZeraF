.class public Lba/d;
.super Law/a;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static b:I

.field private static c:I


# instance fields
.field a:Z

.field private final d:I

.field private final e:Lba/e;

.field private final f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private h:Z

.field private final i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lba/d;->b:I

    return-void
.end method

.method public constructor <init>(Lba/e;LaN/B;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Law/a;-><init>()V

    iput-boolean v3, p0, Lba/d;->h:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lba/d;->i:J

    iput-object p1, p0, Lba/d;->e:Lba/e;

    sget v0, Lba/d;->c:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lba/d;->c:I

    iput v0, p0, Lba/d;->d:I

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gs;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p2}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x33

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method private r()V
    .locals 10

    const/16 v6, 0x16

    const/16 v1, 0x11

    const/4 v5, 0x0

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    move v0, v5

    :goto_0
    if-ge v0, v4, :cond_1

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v3, 0x19

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v1, v5

    add-int/lit8 v5, v0, 0x1

    move v0, v5

    move v5, v1

    goto :goto_0

    :cond_0
    move v4, v5

    :cond_1
    if-nez v4, :cond_2

    :goto_1
    return-void

    :cond_2
    const-string v0, "sv"

    iget-wide v1, p0, Lba/d;->j:J

    iget-wide v6, p0, Lba/d;->i:J

    sub-long/2addr v1, v6

    long-to-int v1, v1

    iget-wide v2, p0, Lba/d;->k:J

    iget-wide v6, p0, Lba/d;->i:J

    sub-long/2addr v2, v6

    long-to-int v2, v2

    iget-wide v6, p0, Lba/d;->l:J

    iget-wide v8, p0, Lba/d;->i:J

    sub-long/2addr v6, v8

    long-to-int v3, v6

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/I;->a(Ljava/lang/String;IIIII)V

    goto :goto_1
.end method


# virtual methods
.method public a(II)V
    .locals 3

    const/16 v2, 0x35

    iget-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x36

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x37

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 2

    iget-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lba/d;->j:J

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lba/d;->h:Z

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lba/d;->a:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataInput;->skipBytes(I)I

    :goto_0
    return v4

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lba/d;->k:J

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gs;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lba/d;->l:J

    invoke-direct {p0}, Lba/d;->r()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x21

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    iput-boolean v4, p0, Lba/d;->a:Z

    throw v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x28

    return v0
.end method

.method public d_()V
    .locals 1

    iget-object v0, p0, Lba/d;->e:Lba/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lba/d;->e:Lba/e;

    invoke-interface {v0, p0}, Lba/e;->a(Lba/d;)V

    :cond_0
    return-void
.end method

.method public k()Z
    .locals 2

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public m()Lam/f;
    .locals 4

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lba/d;->n()[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-interface {v1, v0, v2, v3}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public n()[B
    .locals 2

    iget-object v0, p0, Lba/d;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    return-object v0
.end method

.method public o()V
    .locals 3

    sget v0, Lba/d;->b:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lba/d;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lba/d;->q()Las/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Las/d;->b(I)V

    sget v1, Lba/d;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    invoke-virtual {v0}, Las/d;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lba/d;->p()V

    goto :goto_0
.end method

.method p()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lba/d;->m:Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method q()Las/d;
    .locals 2

    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lba/d;->p()V

    return-void
.end method

.method public s_()Z
    .locals 1

    iget-boolean v0, p0, Lba/d;->h:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SVR["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lba/d;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
