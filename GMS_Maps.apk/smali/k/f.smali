.class public Lk/f;
.super Lk/g;
.source "SourceFile"


# instance fields
.field private d:Z

.field private e:J

.field private f:Lo/aQ;

.field private g:Lo/aQ;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(LA/c;LA/b;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lk/g;-><init>(LA/c;LA/b;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lk/f;->e:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lk/f;->a(LA/c;D)Z

    move-result v0

    iput-boolean v0, p0, Lk/f;->d:Z

    return-void
.end method

.method private static a(LA/c;D)Z
    .locals 2

    sget-object v0, LA/c;->j:LA/c;

    if-eq p0, v0, :cond_0

    sget-object v0, LA/c;->k:LA/c;

    if-eq p0, v0, :cond_0

    sget-object v0, LA/c;->l:LA/c;

    if-ne p0, v0, :cond_1

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->k()Lo/aB;

    move-result-object v0

    iget-object v1, p0, Lk/f;->b:LA/b;

    invoke-interface {v1}, LA/b;->a()Lo/aB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aB;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/T;)F
    .locals 2

    iget-boolean v0, p0, Lk/f;->d:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lk/g;->a(Lo/T;)F

    move-result v0

    const/high16 v1, 0x3f800000

    sub-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lk/g;->a(Lo/T;)F

    move-result v0

    goto :goto_0
.end method

.method public a()J
    .locals 2

    iget-wide v0, p0, Lk/f;->e:J

    return-wide v0
.end method

.method public a(LC/a;)Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    iget-object v0, p0, Lk/f;->f:Lo/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/f;->f:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lk/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, p0, Lk/f;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lk/f;->e:J

    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0, p1}, Lk/f;->c(LC/a;)I

    move-result v2

    iget-object v3, p0, Lk/f;->b:LA/b;

    invoke-interface {v3}, LA/b;->a()Lo/aB;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p0, v1, v2}, Lk/f;->a(Lo/aQ;Ljava/util/ArrayList;)V

    :cond_1
    iput-object v2, p0, Lk/f;->h:Ljava/util/List;

    iput-object v1, p0, Lk/f;->f:Lo/aQ;

    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lo/aQ;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/aQ;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v4, -0x1

    :goto_2
    if-lt v0, v2, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public b(LC/a;)Ljava/util/List;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    iget-object v0, p0, Lk/f;->g:Lo/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/f;->g:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/f;->i:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0, p1}, Lk/f;->c(LC/a;)I

    move-result v2

    invoke-static {v0, v2}, Lo/aq;->b(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p0, v1, v2}, Lk/f;->a(Lo/aQ;Ljava/util/ArrayList;)V

    :cond_1
    iput-object v1, p0, Lk/f;->g:Lo/aQ;

    iput-object v2, p0, Lk/f;->i:Ljava/util/List;

    iget-object v0, p0, Lk/f;->i:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected c(LC/a;)I
    .locals 4

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iget-object v1, p0, Lk/f;->c:Lcom/google/android/maps/driveabout/vector/bF;

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v2

    iget-object v3, p0, Lk/f;->a:LA/c;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/bE;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    float-to-int v0, v0

    goto :goto_0
.end method
