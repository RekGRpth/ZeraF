.class public final Laf/l;
.super LbH/k;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, LbH/k;-><init>()V

    invoke-direct {p0}, Laf/l;->h()V

    return-void
.end method

.method static synthetic g()Laf/l;
    .locals 1

    invoke-static {}, Laf/l;->i()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 0

    return-void
.end method

.method private static i()Laf/l;
    .locals 1

    new-instance v0, Laf/l;

    invoke-direct {v0}, Laf/l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Laf/l;
    .locals 2

    invoke-static {}, Laf/l;->i()Laf/l;

    move-result-object v0

    invoke-virtual {p0}, Laf/l;->c()Laf/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/l;->a(Laf/j;)Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Laf/j;)Laf/l;
    .locals 1

    invoke-static {}, Laf/j;->a()Laf/j;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :cond_0
    return-object p0
.end method

.method public a(LbH/f;LbH/i;)Laf/l;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    sget-object v0, Laf/j;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/j;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Laf/l;->a(Laf/j;)Laf/l;

    :cond_0
    return-object p0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Laf/l;->a(Laf/j;)Laf/l;

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public b()Laf/j;
    .locals 2

    invoke-virtual {p0}, Laf/l;->c()Laf/j;

    move-result-object v0

    invoke-virtual {v0}, Laf/j;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Laf/l;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .locals 1

    invoke-virtual {p0, p1, p2}, Laf/l;->a(LbH/f;LbH/i;)Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/j;
    .locals 2

    new-instance v0, Laf/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laf/j;-><init>(LbH/k;Laf/k;)V

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .locals 1

    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .locals 1

    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .locals 1

    invoke-virtual {p0}, Laf/l;->b()Laf/j;

    move-result-object v0

    return-object v0
.end method
