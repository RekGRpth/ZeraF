.class Lx/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lx/g;->a:Ljava/util/ArrayList;

    iput p1, p0, Lx/g;->c:I

    invoke-virtual {p0}, Lx/g;->a()Lx/f;

    move-result-object v0

    invoke-virtual {v0}, Lx/f;->a()Lx/f;

    return-void
.end method


# virtual methods
.method public a()Lx/f;
    .locals 2

    iget v0, p0, Lx/g;->b:I

    iget-object v1, p0, Lx/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    new-instance v0, Lx/f;

    invoke-direct {v0}, Lx/f;-><init>()V

    iget-object v1, p0, Lx/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget v1, p0, Lx/g;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lx/g;->b:I

    return-object v0

    :cond_0
    iget-object v0, p0, Lx/g;->a:Ljava/util/ArrayList;

    iget v1, p0, Lx/g;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/f;

    invoke-virtual {v0}, Lx/f;->b()V

    goto :goto_0
.end method

.method public b()Lx/f;
    .locals 2

    iget v0, p0, Lx/g;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lx/g;->b:I

    iget-object v0, p0, Lx/g;->a:Ljava/util/ArrayList;

    iget v1, p0, Lx/g;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/f;

    return-object v0
.end method

.method public c()Lx/f;
    .locals 2

    iget-object v0, p0, Lx/g;->a:Ljava/util/ArrayList;

    iget v1, p0, Lx/g;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/f;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lx/g;->c:I

    return v0
.end method
