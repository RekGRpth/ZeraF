.class public Ln/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/aJ;


# instance fields
.field private final a:Ljava/util/Map;

.field private b:Ljava/util/Set;

.field private final c:F

.field private d:J

.field private e:I

.field private f:F

.field private g:Z


# direct methods
.method private constructor <init>(F)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ln/k;->a:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ln/k;->d:J

    iput v2, p0, Ln/k;->e:I

    const/4 v0, 0x0

    iput v0, p0, Ln/k;->f:F

    iput-boolean v2, p0, Ln/k;->g:Z

    iput p1, p0, Ln/k;->c:F

    return-void
.end method

.method constructor <init>(Lo/z;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ln/k;->a:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ln/k;->d:J

    iput v2, p0, Ln/k;->e:I

    const/4 v0, 0x0

    iput v0, p0, Ln/k;->f:F

    iput-boolean v2, p0, Ln/k;->g:Z

    invoke-virtual {p1}, Lo/z;->e()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Ln/k;->c:F

    invoke-virtual {p0, p1}, Ln/k;->a(Lo/z;)Z

    return-void
.end method

.method private static a(F)F
    .locals 3

    mul-float v0, p0, p0

    const/high16 v1, 0x40400000

    const/high16 v2, 0x40000000

    mul-float/2addr v2, p0

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(FFF)F
    .locals 1

    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method private static a(FFFFF)F
    .locals 2

    cmpg-float v0, p0, p1

    if-gtz v0, :cond_0

    :goto_0
    return p3

    :cond_0
    cmpl-float v0, p0, p2

    if-ltz v0, :cond_1

    move p3, p4

    goto :goto_0

    :cond_1
    sub-float v0, p0, p1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    add-float/2addr p3, v0

    goto :goto_0
.end method

.method static a(FFIFF)F
    .locals 6

    const/high16 v5, 0x42c80000

    const/high16 v4, 0x41a00000

    const/high16 v3, 0x41900000

    const/4 v1, 0x0

    cmpl-float v0, p4, v1

    if-lez v0, :cond_1

    const/high16 v0, 0x40400000

    invoke-static {p1, v3, v4, v0, v1}, Ln/k;->a(FFFFF)F

    move-result v0

    :goto_0
    const/high16 v2, 0x41200000

    invoke-static {p0, v1, v2, v1, v0}, Ln/k;->a(FFFFF)F

    move-result v0

    invoke-static {p3}, Ln/k;->a(F)F

    move-result v1

    and-int/lit8 v2, p2, 0x2

    if-eqz v2, :cond_3

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    :cond_0
    :goto_1
    return v0

    :cond_1
    cmpg-float v0, p4, v1

    if-gez v0, :cond_2

    const/high16 v0, -0x3fc00000

    const/high16 v2, -0x40800000

    invoke-static {p1, v3, v4, v0, v2}, Ln/k;->a(FFFFF)F

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_0

    const/high16 v2, 0x3f800000

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    goto :goto_1
.end method

.method static a(IF)I
    .locals 4

    const/high16 v1, 0x3f800000

    invoke-static {p1}, Ln/k;->a(F)F

    move-result v0

    and-int/lit8 v2, p0, 0x4

    if-eqz v2, :cond_0

    :goto_0
    and-int/lit8 v2, p0, 0x10

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    const v3, 0x3f19999a

    invoke-static {v0, v2, v1, v3, v1}, Ln/k;->a(FFFFF)F

    move-result v0

    invoke-static {v1, v0, v0, v0}, Lx/d;->a(FFFF)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    and-int/lit8 v2, p0, 0x8

    if-eqz v2, :cond_1

    sub-float v0, v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0, v0, v0, v0}, Lx/d;->a(FFFF)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public a(LC/a;Lo/T;)F
    .locals 5

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    iget v2, p0, Ln/k;->e:I

    iget v3, p0, Ln/k;->f:F

    iget v4, p0, Ln/k;->c:F

    invoke-static {v0, v1, v2, v3, v4}, Ln/k;->a(FFIFF)F

    move-result v0

    invoke-virtual {p2}, Lo/T;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aJ;)I
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Ln/k;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ln/k;->b()F

    move-result v0

    check-cast p1, Ln/k;

    invoke-virtual {p1}, Ln/k;->b()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    :cond_0
    return v0
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Ln/k;->b:Ljava/util/Set;

    if-nez v0, :cond_0

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Ln/k;->b:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, Ln/k;->b:Ljava/util/Set;

    return-object v0
.end method

.method a(Lo/r;)Ln/k;
    .locals 4

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v1, Ln/k;

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v1, v0}, Ln/k;-><init>(F)V

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v3

    invoke-virtual {v3, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v0}, Ln/k;->a(Lo/z;)Z

    goto :goto_1

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Ln/k;->a(IJ)V

    return-void
.end method

.method a(IJ)V
    .locals 1

    iput p1, p0, Ln/k;->e:I

    iput-wide p2, p0, Ln/k;->d:J

    const/4 v0, 0x0

    iput v0, p0, Ln/k;->f:F

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V
    .locals 6

    const/16 v5, 0x1e00

    const/4 v4, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p0, p2, p4}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v0

    invoke-virtual {p2}, LC/a;->w()F

    move-result v2

    mul-float/2addr v0, v2

    invoke-interface {v1, v4, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget v2, p0, Ln/k;->e:I

    iget v3, p0, Ln/k;->f:F

    invoke-static {v2, v3}, Ln/k;->a(IF)I

    move-result v2

    invoke-static {v1, v2}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v2

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aI;->f:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aI;->g:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aI;->h:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v2, v3, :cond_2

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ln/k;->g:Z

    iget-boolean v0, p0, Ln/k;->g:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LD/a;->w()V

    invoke-interface {v1, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    const/16 v0, 0x202

    const/16 v2, 0xff

    const/16 v3, 0x80

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-boolean v1, p0, Ln/k;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LD/a;->x()V

    :cond_0
    const/4 v1, -0x1

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method a(J)Z
    .locals 3

    iget-wide v0, p0, Ln/k;->d:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43fa0000

    div-float/2addr v0, v1

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    invoke-static {v0, v1, v2}, Ln/k;->a(FFF)F

    move-result v0

    iput v0, p0, Ln/k;->f:F

    invoke-virtual {p0}, Ln/k;->c()Z

    move-result v0

    return v0
.end method

.method public a(Lo/z;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lo/z;->e()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Ln/k;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    iget-object v0, p0, Ln/k;->b:Ljava/util/Set;

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lo/z;->e()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Ln/k;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lo/z;->b()Lo/r;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    goto :goto_1
.end method

.method public b()F
    .locals 1

    iget v0, p0, Ln/k;->c:F

    return v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Ln/k;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Ln/k;->f:F

    const/high16 v1, 0x3f800000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-virtual {p0, p1}, Ln/k;->a(Lcom/google/android/maps/driveabout/vector/aJ;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    invoke-virtual {p0}, Ln/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ln/k;->e:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Ln/k;->e:I

    const/4 v0, 0x0

    iput v0, p0, Ln/k;->f:F

    return-void
.end method

.method public f()Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ln/k;->a(J)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Ln/k;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Ln/k;->c:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "animationStartTimeMs"

    iget-wide v2, p0, Ln/k;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/G;->a(Ljava/lang/String;J)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "animationPosition"

    iget v2, p0, Ln/k;->f:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;F)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "animationType"

    iget v2, p0, Ln/k;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "featureIds"

    iget-object v2, p0, Ln/k;->b:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
