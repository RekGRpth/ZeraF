.class public Ln/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/c;
.implements Ls/e;


# instance fields
.field private final a:Lr/z;

.field private final b:Lr/n;

.field private final c:Lo/aq;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Collection;

.field private volatile f:Z

.field private volatile g:Ln/d;


# direct methods
.method public constructor <init>(Lr/z;Lr/n;Lo/aq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ln/b;->d:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ln/b;->e:Ljava/util/Collection;

    iput-object p1, p0, Ln/b;->a:Lr/z;

    iput-object p2, p0, Ln/b;->b:Lr/n;

    iput-object p3, p0, Ln/b;->c:Lo/aq;

    return-void
.end method

.method private static a(Lo/aL;)Ljava/util/Collection;
    .locals 5

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lo/aL;->k()Lo/aO;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    invoke-interface {v0}, Lo/n;->h()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    check-cast v0, Lo/f;

    invoke-virtual {v0}, Lo/f;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lo/f;->a()Lo/o;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v4, Lo/o;->a:Lo/o;

    if-eq v3, v4, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Ln/b;->g:Ln/d;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Ln/b;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln/b;->g:Ln/d;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ln/d;->a(Ln/b;Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ln/b;->g:Ln/d;

    iget-object v1, p0, Ln/b;->e:Ljava/util/Collection;

    invoke-interface {v0, p0, v1}, Ln/d;->a(Ln/b;Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lo/aq;
    .locals 1

    iget-object v0, p0, Ln/b;->c:Lo/aq;

    return-object v0
.end method

.method public a(Ln/d;)V
    .locals 2

    iput-object p1, p0, Ln/b;->g:Ln/d;

    iget-object v0, p0, Ln/b;->a:Lr/z;

    iget-object v1, p0, Ln/b;->c:Lo/aq;

    invoke-interface {v0, v1, p0}, Lr/z;->a(Lo/aq;Ls/e;)V

    return-void
.end method

.method public a(Lo/aq;ILo/ap;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_5

    :cond_2
    :goto_1
    const/4 v0, 0x0

    if-eqz p3, :cond_3

    check-cast p3, Lo/aL;

    invoke-static {p3}, Ln/b;->a(Lo/aL;)Ljava/util/Collection;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_6

    :cond_4
    invoke-direct {p0}, Ln/b;->b()V

    goto :goto_0

    :cond_5
    if-ne p2, v1, :cond_2

    iput-boolean v1, p0, Ln/b;->f:Z

    goto :goto_1

    :cond_6
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/f;

    new-instance v2, Ln/c;

    invoke-direct {v2, v0}, Ln/c;-><init>(Lo/f;)V

    iget-object v0, p0, Ln/b;->d:Ljava/util/Map;

    invoke-static {v2}, Ln/c;->a(Ln/c;)Lo/r;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_7
    iget-object v0, p0, Ln/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/c;

    iget-object v2, p0, Ln/b;->b:Lr/n;

    invoke-static {v0}, Ln/c;->a(Ln/c;)Lo/r;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Lr/n;->a(Lo/r;Ls/c;)V

    goto :goto_3
.end method

.method public a(Lo/r;ILo/y;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Ln/b;->d:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/c;

    iget-object v2, p0, Ln/b;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    monitor-exit v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lo/y;->e()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln/c;->a(Lo/T;)V

    iget-object v1, p0, Ln/b;->e:Ljava/util/Collection;

    invoke-virtual {v0}, Ln/c;->a()Ln/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    if-ne p2, v3, :cond_3

    iput-boolean v3, p0, Ln/b;->f:Z

    :cond_3
    if-eqz v2, :cond_0

    invoke-direct {p0}, Ln/b;->b()V

    goto :goto_0
.end method
