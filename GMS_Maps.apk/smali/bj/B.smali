.class public Lbj/B;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:I

.field private final c:Lcom/google/googlenav/ai;

.field private final d:Ljava/util/List;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "^.*\\((.*)\\)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbj/B;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/J;Z)V
    .locals 7

    const/4 v3, 0x6

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbj/B;->d:Ljava/util/List;

    iput-object p1, p0, Lbj/B;->c:Lcom/google/googlenav/ai;

    iput p2, p0, Lbj/B;->b:I

    iput-boolean p4, p0, Lbj/B;->e:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->cj()Lcom/google/googlenav/ao;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v2}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v3, :cond_2

    invoke-virtual {v2}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    :cond_2
    iget-object v4, p0, Lbj/B;->d:Ljava/util/List;

    move v0, p2

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lbj/B;->a(ILcom/google/googlenav/J;Lcom/google/googlenav/ao;ILjava/util/List;Z)V

    invoke-virtual {v2}, Lcom/google/googlenav/ao;->c()Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/ao;->c()Lcom/google/googlenav/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/googlenav/ao;->c()Lcom/google/googlenav/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbj/B;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x536

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lbj/B;->d:Ljava/util/List;

    new-instance v3, Lbj/D;

    iget-object v4, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, p2

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v3, v1, v0, v4, p3}, Lbj/D;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/google/googlenav/J;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(ILcom/google/googlenav/J;Lcom/google/googlenav/ao;ILjava/util/List;Z)V
    .locals 4

    invoke-virtual {p2}, Lcom/google/googlenav/ao;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbj/bv;

    const v1, 0x7f040116

    invoke-virtual {p2}, Lcom/google/googlenav/ao;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p3, :cond_1

    new-instance v2, Lbj/y;

    invoke-virtual {p2}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ar;

    add-int v3, p0, v1

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v0, v3, p1, p5}, Lbj/y;-><init>(Lcom/google/googlenav/ar;ILcom/google/googlenav/J;Z)V

    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/B;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 6

    iget-boolean v0, p0, Lbj/B;->e:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    new-instance v2, Lbj/C;

    invoke-direct {v2}, Lbj/C;-><init>()V

    const v0, 0x7f100368

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/C;->a:Landroid/view/View;

    new-instance v0, Lbj/bw;

    invoke-direct {v0}, Lbj/bw;-><init>()V

    iput-object v0, v2, Lbj/C;->b:Lbj/bw;

    iget-object v1, v2, Lbj/C;->b:Lbj/bw;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/bw;->a:Landroid/widget/TextView;

    const v0, 0x7f100020

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lbj/C;->c:Landroid/widget/LinearLayout;

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/H;

    const v3, 0x7f040135

    iget-object v4, v2, Lbj/C;->c:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v3}, Lbj/H;->a(Landroid/view/View;)Lbj/bB;

    move-result-object v0

    check-cast v0, Lbj/A;

    iget-object v3, v2, Lbj/C;->c:Landroid/widget/LinearLayout;

    iget-object v4, v0, Lbj/A;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, v2, Lbj/C;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    check-cast p2, Lbj/C;

    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/H;

    iget-object v1, p2, Lbj/C;->b:Lbj/bw;

    invoke-interface {v0, p1, v1}, Lbj/H;->a(Lcom/google/googlenav/ui/e;Lbj/bB;)V

    const/4 v0, 0x1

    move v2, v0

    :goto_1
    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lbj/B;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/H;

    iget-object v1, p2, Lbj/C;->d:Ljava/util/List;

    add-int/lit8 v3, v2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbj/bB;

    invoke-interface {v0, p1, v1}, Lbj/H;->a(Lcom/google/googlenav/ui/e;Lbj/bB;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget-object v0, p2, Lbj/C;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lbj/C;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p2, Lbj/C;->d:Ljava/util/List;

    iget-object v1, p2, Lbj/C;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/A;

    iget-object v0, v0, Lbj/A;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbj/B;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f04013e

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
