.class public Lbj/bq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field private static final a:Ljava/util/List;


# instance fields
.field private final b:I

.field private final c:Ljava/lang/CharSequence;

.field private final d:I

.field private final e:[Ljava/lang/CharSequence;

.field private final f:[Ljava/lang/CharSequence;

.field private final g:[Ljava/lang/CharSequence;

.field private final h:Ljava/lang/CharSequence;

.field private final i:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v0, 0x7f100372

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f100374

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f100375

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f100376

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lbj/bq;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(ILcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lbj/bq;->f:[Ljava/lang/CharSequence;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lbj/bq;->g:[Ljava/lang/CharSequence;

    iput p1, p0, Lbj/bq;->b:I

    invoke-virtual {p2}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020384

    iput v0, p0, Lbj/bq;->d:I

    const-string v0, ""

    iput-object v0, p0, Lbj/bq;->c:Ljava/lang/CharSequence;

    :goto_0
    iget-object v0, p2, Lcom/google/googlenav/cx;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    const/4 v0, 0x4

    if-ge v2, v0, :cond_5

    if-ge v2, v3, :cond_2

    iget-object v0, p2, Lcom/google/googlenav/cx;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cy;

    iget-object v4, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    iget-object v5, v0, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v5, v1}, Lbj/bq;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v4, v2

    iget-object v1, p0, Lbj/bq;->f:[Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lbj/bq;->b(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v1, v2

    iget-object v1, p0, Lbj/bq;->g:[Ljava/lang/CharSequence;

    iget-object v0, v0, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lbj/bq;->b(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v2

    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    const v0, 0x7f020382

    iput v0, p0, Lbj/bq;->d:I

    const/16 v0, 0x484

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/bq;->c:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    if-ne v2, v3, :cond_4

    invoke-virtual {p2}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v1, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-direct {p0, p4, v0}, Lbj/bq;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v2

    iget-object v0, p0, Lbj/bq;->f:[Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-direct {p0, p3, v1}, Lbj/bq;->b(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v0, p0, Lbj/bq;->g:[Ljava/lang/CharSequence;

    const/4 v1, 0x1

    invoke-direct {p0, p3, v1}, Lbj/bq;->b(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    iget-object v0, p0, Lbj/bq;->f:[Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    goto :goto_3

    :cond_5
    invoke-virtual {p2}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    invoke-static {v0, v3, p3, p4, p5}, Lcom/google/googlenav/ui/bC;->a(ZILjava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, ""

    iput-object v0, p0, Lbj/bq;->h:Ljava/lang/CharSequence;

    :goto_5
    invoke-static {p6}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x61f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f0096

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {p6, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;I[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbj/bq;->i:Ljava/lang/CharSequence;

    :goto_6
    return-void

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/bq;->h:Ljava/lang/CharSequence;

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lbj/bq;->i:Ljava/lang/CharSequence;

    goto :goto_6
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/CharSequence;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    const v0, 0x7f0f0101

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0f0104

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    const/high16 v1, 0x43b40000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)Ljava/lang/CharSequence;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    const v0, 0x7f0f0103

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0f0102

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/bq;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 5

    new-instance v2, Lbj/bs;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lbj/bs;-><init>(Lbj/br;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x4

    if-ge v1, v0, :cond_0

    sget-object v0, Lbj/bq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, v2, Lbj/bs;->a:[Landroid/widget/TextView;

    const v0, 0x7f10007b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    iget-object v4, v2, Lbj/bs;->b:[Landroid/widget/TextView;

    const v0, 0x7f100373

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    iget-object v0, v2, Lbj/bs;->c:[Landroid/view/View;

    aput-object v3, v0, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const v0, 0x7f100371

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/bs;->f:Landroid/widget/TextView;

    const v0, 0x7f100377

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/bs;->e:Landroid/widget/TextView;

    const v0, 0x7f100051

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/bs;->d:Landroid/view/View;

    const v0, 0x7f10037a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/bs;->g:Landroid/widget/TextView;

    const v0, 0x7f100370

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/bs;->h:Landroid/view/View;

    const v0, 0x7f100379

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/bs;->i:Landroid/view/View;

    return-object v2
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 6

    const/16 v1, 0x8

    const/4 v2, 0x0

    check-cast p2, Lbj/bs;

    iget-object v0, p2, Lbj/bs;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lbj/bq;->a(Landroid/content/Context;)Z

    move-result v3

    move v0, v2

    :goto_0
    const/4 v4, 0x4

    if-ge v0, v4, :cond_2

    iget-object v4, p2, Lbj/bs;->a:[Landroid/widget/TextView;

    aget-object v4, v4, v0

    iget-object v5, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    aget-object v5, v5, v0

    invoke-static {v4, v5}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_0

    iget-object v4, p2, Lbj/bs;->b:[Landroid/widget/TextView;

    aget-object v4, v4, v0

    iget-object v5, p0, Lbj/bq;->g:[Ljava/lang/CharSequence;

    aget-object v5, v5, v0

    invoke-static {v4, v5}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v4, p0, Lbj/bq;->e:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    if-eqz v4, :cond_1

    iget-object v4, p2, Lbj/bs;->c:[Landroid/view/View;

    aget-object v4, v4, v0

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p2, Lbj/bs;->b:[Landroid/widget/TextView;

    aget-object v4, v4, v0

    iget-object v5, p0, Lbj/bq;->f:[Ljava/lang/CharSequence;

    aget-object v5, v5, v0

    invoke-static {v4, v5}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object v4, p2, Lbj/bs;->c:[Landroid/view/View;

    aget-object v4, v4, v0

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p2, Lbj/bs;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lbj/bq;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/bs;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lbj/bs;->f:Landroid/widget/TextView;

    iget v4, p0, Lbj/bq;->d:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p2, Lbj/bs;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p2, Lbj/bs;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lbj/bq;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p2, Lbj/bs;->d:Landroid/view/View;

    new-instance v4, Lbj/br;

    invoke-direct {v4, p0}, Lbj/br;-><init>(Lbj/bq;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p2, Lbj/bs;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lbj/bq;->i:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v4, p2, Lbj/bs;->h:Landroid/view/View;

    if-eqz v3, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lbj/bs;->i:Landroid/view/View;

    if-eqz v3, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040142

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
