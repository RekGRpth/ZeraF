.class public Lbj/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/googlenav/J;

.field final c:Z

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ar;ILcom/google/googlenav/J;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lbj/y;->d:I

    invoke-virtual {p1}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/y;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/y;->e:Ljava/lang/String;

    iput-object p3, p0, Lbj/y;->b:Lcom/google/googlenav/J;

    iput-boolean p4, p0, Lbj/y;->c:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/y;->d:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/A;

    invoke-direct {v1}, Lbj/A;-><init>()V

    const v0, 0x7f100352

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/A;->a:Landroid/view/ViewGroup;

    const v0, 0x7f100034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/A;->b:Landroid/widget/TextView;

    const v0, 0x7f10001f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/A;->c:Landroid/view/View;

    return-object v1
.end method

.method protected a(Lbj/A;Lcom/google/googlenav/ui/e;)V
    .locals 2

    iget-object v0, p1, Lbj/A;->a:Landroid/view/ViewGroup;

    new-instance v1, Lbj/z;

    invoke-direct {v1, p0, p2}, Lbj/z;-><init>(Lbj/y;Lcom/google/googlenav/ui/e;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p2, Lbj/A;

    iget-object v0, p2, Lbj/A;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/y;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p2, p1}, Lbj/y;->a(Lbj/A;Lcom/google/googlenav/ui/e;)V

    iget-object v0, p2, Lbj/A;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lbj/A;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040135

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
