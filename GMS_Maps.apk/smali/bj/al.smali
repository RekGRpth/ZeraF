.class public Lbj/al;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ai;

.field private final c:Z


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ai;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbj/al;->a:I

    iput-object p2, p0, Lbj/al;->b:Lcom/google/googlenav/ai;

    iput-boolean p3, p0, Lbj/al;->c:Z

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/widget/TextView;Lcom/google/googlenav/ui/e;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x1

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    :cond_1
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    :goto_0
    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {p2, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lbj/am;

    invoke-direct {v0, p0, p3, v1}, Lbj/am;-><init>(Lbj/al;Lcom/google/googlenav/ui/e;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void

    :cond_5
    sget-object v2, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/al;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 3

    iget-boolean v0, p0, Lbj/al;->c:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    new-instance v1, Lbj/an;

    invoke-direct {v1}, Lbj/an;-><init>()V

    const v0, 0x7f100342

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/an;->a:Landroid/view/View;

    iget-object v0, v1, Lbj/an;->a:Landroid/view/View;

    const v2, 0x7f100343

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/an;->b:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/an;->a:Landroid/view/View;

    const v2, 0x7f100344

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/an;->c:Landroid/widget/TextView;

    iget-object v0, v1, Lbj/an;->a:Landroid/view/View;

    const v2, 0x7f100345

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/an;->d:Landroid/widget/TextView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lbj/al;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->U()Ljava/util/List;

    move-result-object v2

    check-cast p2, Lbj/an;

    const/4 v0, 0x3

    new-array v3, v0, [Landroid/widget/TextView;

    iget-object v0, p2, Lbj/an;->b:Landroid/widget/TextView;

    aput-object v0, v3, v1

    const/4 v0, 0x1

    iget-object v4, p2, Lbj/an;->c:Landroid/widget/TextView;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-object v4, p2, Lbj/an;->d:Landroid/widget/TextView;

    aput-object v4, v3, v0

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    aget-object v4, v3, v0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p2, Lbj/an;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v5, v3, v2

    invoke-direct {p0, v0, v5, p1}, Lbj/al;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/widget/TextView;Lcom/google/googlenav/ui/e;)V

    add-int/lit8 v0, v2, 0x1

    array-length v2, v3

    if-lt v0, v2, :cond_4

    :cond_3
    iget-object v0, p2, Lbj/an;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040132

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
