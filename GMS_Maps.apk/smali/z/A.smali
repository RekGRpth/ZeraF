.class public abstract Lz/A;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field protected volatile c:Z

.field protected d:Z

.field protected e:[F

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:I

.field private final i:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lz/A;->a:I

    iput v1, p0, Lz/A;->b:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lz/A;->f:Ljava/lang/Object;

    iput v1, p0, Lz/A;->g:I

    iput v1, p0, Lz/A;->h:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/A;->c:Z

    iput-boolean v1, p0, Lz/A;->d:Z

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lz/A;->e:[F

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lz/A;->i:Ljava/util/List;

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, v2, v2, v2, v0}, Lz/A;->a(FFFF)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v0, p0, Lz/A;->c:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lz/A;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lz/A;->g:I

    iput v0, p0, Lz/A;->a:I

    iget v0, p0, Lz/A;->h:I

    iput v0, p0, Lz/A;->b:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz/A;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lz/A;->e:[F

    aget v0, v0, v5

    iget-object v1, p0, Lz/A;->e:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lz/A;->e:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lz/A;->e:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    invoke-static {v5}, Landroid/opengl/GLES20;->glClearStencil(I)V

    const/16 v0, 0x4500

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(FFFF)V
    .locals 2

    iget-boolean v0, p0, Lz/A;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iget-object v0, p0, Lz/A;->e:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lz/A;->e:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lz/A;->e:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lz/A;->e:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    return-void
.end method

.method protected a(II)V
    .locals 5

    iget-object v1, p0, Lz/A;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lz/A;->g:I

    iput p2, p0, Lz/A;->h:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/A;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lz/A;->i:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lz/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lz/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/e;

    iget v3, p0, Lz/A;->g:I

    iget v4, p0, Lz/A;->h:I

    invoke-virtual {v0, p0, v3, v4}, Lz/e;->a(Lz/A;II)V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method a(Lz/e;)V
    .locals 2

    iget-object v1, p0, Lz/A;->i:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lz/A;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lz/k;Lz/j;)Z
    .locals 2

    iget-boolean v0, p2, Lz/j;->e:Z

    iget-boolean v1, p0, Lz/A;->d:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p2, Lz/j;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/A;->d:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lz/A;->g:I

    return v0
.end method

.method b(Lz/e;)V
    .locals 2

    iget-object v1, p0, Lz/A;->i:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lz/A;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lz/A;->h:I

    return v0
.end method

.method d()V
    .locals 0

    return-void
.end method
