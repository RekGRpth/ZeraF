.class public Lz/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/c;


# static fields
.field private static m:Z

.field private static final o:Ljava/util/List;

.field private static final p:Ljava/util/List;

.field private static r:Ljava/lang/String;


# instance fields
.field b:I

.field c:I

.field private d:[Lz/o;

.field private e:Lz/m;

.field private f:[Ljava/util/Set;

.field private g:I

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:[Lz/z;

.field private k:Ljava/util/Set;

.field private l:Ljava/util/Set;

.field private final n:Lz/n;

.field private q:I

.field private s:Lz/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lz/k;->m:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lz/k;->o:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lz/k;->p:Ljava/util/List;

    const/4 v0, 0x0

    sput-object v0, Lz/k;->r:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->dumpStack()V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    new-instance v1, Lz/q;

    invoke-direct {v1, v0}, Lz/q;-><init>(I)V

    throw v1

    :cond_0
    return-void
.end method

.method static c()V
    .locals 2

    sget-boolean v0, Lz/k;->m:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to update live data from outside a Behavior"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-interface {v0}, Lz/x;->a()V

    :cond_0
    invoke-virtual {p0}, Lz/k;->b()V

    iget-object v2, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v0, p0, Lz/k;->g:I

    iput v0, p0, Lz/k;->h:I

    iget v0, p0, Lz/k;->g:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x2

    iput v0, p0, Lz/k;->g:I

    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v3, p0, Lz/k;->h:I

    aget-object v0, v0, v3

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->g:I

    aget-object v0, v0, v2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/b;

    invoke-interface {v0}, Lz/b;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z

    :try_start_2
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->g:I

    aget-object v0, v0, v2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/b;

    invoke-interface {v0, p0}, Lz/b;->b(Lz/c;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    sput-boolean v1, Lz/k;->m:Z

    throw v0

    :cond_2
    sput-boolean v1, Lz/k;->m:Z

    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-interface {v0}, Lz/x;->b()V

    :cond_3
    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/A;

    invoke-virtual {v0}, Lz/A;->a()V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/e;

    invoke-virtual {v0}, Lz/e;->c()V

    goto :goto_3

    :cond_5
    iget-object v2, p0, Lz/k;->j:[Lz/z;

    array-length v3, v2

    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_7

    aget-object v1, v2, v0

    if-eqz v1, :cond_6

    invoke-virtual {v1, p0}, Lz/z;->a(Lz/k;)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/e;

    invoke-virtual {v0}, Lz/e;->d()V

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/A;

    invoke-virtual {v0}, Lz/A;->d()V

    goto :goto_6

    :cond_9
    iget v0, p0, Lz/k;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/k;->q:I

    return-void
.end method

.method a(Ljavax/microedition/khronos/opengles/GL10;Lz/N;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lz/k;->n:Lz/n;

    invoke-virtual {v0}, Lz/n;->a()V

    iget-object v2, p0, Lz/k;->j:[Lz/z;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Lz/z;->b(Lz/k;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    sget v2, Lz/o;->a:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lz/k;->d:[Lz/o;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v2, 0xd57

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v0, v0, v1

    iput v0, p0, Lz/k;->b:I

    const-wide/high16 v0, 0x4000000000000000L

    iget v2, p0, Lz/k;->b:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lz/k;->c:I

    sget-object v0, Lz/k;->r:Ljava/lang/String;

    if-nez v0, :cond_3

    const/16 v0, 0x1f03

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lz/k;->r:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public a(Lz/b;Lz/O;)V
    .locals 3

    sget-object v0, Lz/c;->a:Lz/P;

    if-ne p2, v0, :cond_0

    iget-object v1, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->h:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented WakeUpCondition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lz/i;)V
    .locals 3

    iget-object v0, p0, Lz/k;->e:Lz/m;

    new-instance v1, Lz/l;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lz/l;-><init>(Lz/i;Z)V

    invoke-static {v0, v1}, Lz/m;->a(Lz/m;Lz/l;)V

    return-void
.end method

.method b()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lz/k;->e:Lz/m;

    invoke-static {v0}, Lz/m;->a(Lz/m;)Lz/l;

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_0
    iget v0, v2, Lz/l;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/i;->a(Lz/k;Lz/j;)Z

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    invoke-virtual {v0}, Lz/i;->a()B

    move-result v3

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_2

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->a(Lz/i;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/p;->a:Lz/p;

    invoke-virtual {v0, v3}, Lz/i;->a(Lz/p;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v2

    invoke-interface {v0, v2}, Lz/x;->a(Lz/i;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/i;->a(Lz/k;Lz/j;)Z

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    invoke-virtual {v0}, Lz/i;->a()B

    move-result v3

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_4

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_3

    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->b(Lz/i;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/p;->a:Lz/p;

    invoke-virtual {v0, v3}, Lz/i;->a(Lz/p;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v2

    invoke-interface {v0, v2}, Lz/x;->b(Lz/i;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/e;->a(Lz/k;Lz/j;)Z

    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v3

    invoke-virtual {v3}, Lz/e;->b()Lz/A;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v0

    invoke-virtual {v0}, Lz/e;->e()B

    move-result v3

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_0

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_5

    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->a(Lz/e;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Remove camera not implemented"

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v2}, Lz/l;->c()Lz/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lz/b;->a(Lz/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    sput-boolean v0, Lz/k;->m:Z

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const/4 v2, 0x0

    sput-boolean v2, Lz/k;->m:Z

    throw v0

    :pswitch_5
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    iget-object v3, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v4, p0, Lz/k;->h:I

    aget-object v0, v0, v4

    invoke-virtual {v2}, Lz/l;->c()Lz/b;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v0, 0x0

    :try_start_5
    sput-boolean v0, Lz/k;->m:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v0

    const/4 v2, 0x0

    :try_start_8
    sput-boolean v2, Lz/k;->m:Z

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Lz/i;)V
    .locals 3

    iget-object v0, p0, Lz/k;->e:Lz/m;

    new-instance v1, Lz/l;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lz/l;-><init>(Lz/i;Z)V

    invoke-static {v0, v1}, Lz/m;->a(Lz/m;Lz/l;)V

    return-void
.end method

.method public d()Lz/B;
    .locals 1

    iget-object v0, p0, Lz/k;->n:Lz/n;

    return-object v0
.end method

.method e()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    sget v1, Lz/o;->a:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0, v2}, Lz/o;->a(Lz/k;Lz/o;)V

    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
