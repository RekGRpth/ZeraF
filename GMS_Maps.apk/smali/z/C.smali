.class public Lz/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field protected b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz/C;->c:Ljava/lang/String;

    iput-object p2, p0, Lz/C;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/String;)I
    .locals 3

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to get "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v1, "ShaderProgram"

    const-string v2, "glGetUniformLocation"

    invoke-static {v1, v2}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    const v1, 0x8b31

    invoke-virtual {p0, v1, p1}, Lz/C;->b(ILjava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v1, 0x8b30

    invoke-virtual {p0, v1, p2}, Lz/C;->b(ILjava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "ShaderState"

    const-string v4, "glAttachShader"

    invoke-static {v2, v4}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "ShaderState"

    const-string v3, "glAttachShader"

    invoke-static {v2, v3}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lz/C;->b(I)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v2, v5, [I

    const v3, 0x8b82

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v2, v2, v0

    if-eq v2, v5, :cond_2

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected a(I)V
    .locals 1

    const-string v0, "uMVPMatrix"

    invoke-virtual {p0, p1, v0}, Lz/C;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lz/C;->b:I

    return-void
.end method

.method public a(Lz/k;Lz/j;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p2, Lz/j;->e:Z

    iget-boolean v3, p0, Lz/C;->e:Z

    if-ne v0, v3, :cond_1

    iget-boolean v0, p2, Lz/j;->f:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v0, p2, Lz/j;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p2, Lz/j;->f:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/C;->e:Z

    iget-boolean v0, p0, Lz/C;->e:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p2, Lz/j;->f:Z

    if-eqz v0, :cond_3

    iput v2, p0, Lz/C;->a:I

    :cond_3
    iget v0, p0, Lz/C;->a:I

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "Attempt to overwrite existing shader program: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lz/C;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lz/C;->c:Ljava/lang/String;

    iget-object v3, p0, Lz/C;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lz/C;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lz/C;->a:I

    iget v0, p0, Lz/C;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    iget v0, p0, Lz/C;->a:I

    invoke-virtual {p0, v0}, Lz/C;->a(I)V

    invoke-static {v2}, Landroid/opengl/GLES20;->glUseProgram(I)V

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-boolean v0, p2, Lz/j;->f:Z

    if-nez v0, :cond_6

    iget v0, p0, Lz/C;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    :cond_6
    iput v2, p0, Lz/C;->a:I

    goto :goto_2
.end method

.method b(ILjava/lang/String;)I
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v1, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x8b81

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    aget v2, v2, v0

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected b(I)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "aPosition"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v0, "ShaderProgram"

    const-string v1, "bindAttribute aPosition"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    const-string v1, "aNormal"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v0, "ShaderProgram"

    const-string v1, "bindAttribute aTextureCoord"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    const-string v1, "aColor"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v0, "ShaderProgram"

    const-string v1, "bindAttribute aColor"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    const-string v1, "aTextureCoord"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    const-string v0, "ShaderProgram"

    const-string v1, "bindAttribute aTextureCoord"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
