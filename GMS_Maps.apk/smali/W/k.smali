.class public abstract LW/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[LW/k;


# instance fields
.field protected b:LW/h;

.field protected c:LW/j;

.field protected d:LW/g;

.field protected e:LU/x;

.field protected f:Lcom/google/googlenav/ui/view/android/bF;

.field protected g:Lbi/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, LW/n;->values()[LW/n;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [LW/k;

    sput-object v0, LW/k;->a:[LW/k;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LW/h;Lbi/d;LU/x;LW/j;)V
    .locals 4

    const/4 v0, 0x0

    sget-object v1, LW/k;->a:[LW/k;

    sget-object v2, LW/a;->a:LW/a;

    aput-object v2, v1, v0

    sget-object v1, LW/k;->a:[LW/k;

    const/4 v2, 0x1

    sget-object v3, LW/e;->a:LW/e;

    aput-object v3, v1, v2

    sget-object v1, LW/k;->a:[LW/k;

    const/4 v2, 0x2

    sget-object v3, LW/f;->a:LW/f;

    aput-object v3, v1, v2

    :goto_0
    sget-object v1, LW/k;->a:[LW/k;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, LW/k;->a:[LW/k;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0, p3, p1, p2}, LW/k;->a(LW/h;LW/j;Lbi/d;LU/x;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public a(II)V
    .locals 4

    iget-object v0, p0, LW/k;->c:LW/j;

    invoke-virtual {v0}, LW/j;->c()Lbi/a;

    move-result-object v0

    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-interface {v1}, Lcom/google/android/maps/rideabout/view/j;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LW/k;->d:LW/g;

    invoke-virtual {v2}, LW/g;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lcom/google/android/maps/rideabout/view/j;->c()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->b(Lbi/a;)V

    :cond_0
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->c(Lbi/a;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/maps/rideabout/view/j;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;)V

    iget-object v0, p0, LW/k;->d:LW/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LW/g;->a(Z)V

    :cond_1
    :goto_1
    iget-object v0, p0, LW/k;->d:LW/g;

    invoke-virtual {v0, p1}, LW/g;->a(I)V

    return-void

    :cond_2
    if-eq p1, p2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;)V

    goto :goto_0

    :cond_3
    if-eq p1, p2, :cond_4

    if-eq p1, v2, :cond_6

    :cond_4
    if-le p1, v2, :cond_5

    if-lt p2, v2, :cond_6

    :cond_5
    if-ge p1, v2, :cond_1

    if-le p2, v2, :cond_1

    :cond_6
    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;)V

    iget-object v0, p0, LW/k;->d:LW/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LW/g;->a(Z)V

    goto :goto_1
.end method

.method public a(LU/z;)V
    .locals 0

    return-void
.end method

.method public final a(LW/h;LW/j;Lbi/d;LU/x;)V
    .locals 1

    iput-object p1, p0, LW/k;->b:LW/h;

    iput-object p2, p0, LW/k;->c:LW/j;

    iput-object p3, p0, LW/k;->g:Lbi/d;

    iput-object p4, p0, LW/k;->e:LU/x;

    new-instance v0, LW/g;

    invoke-direct {v0}, LW/g;-><init>()V

    iput-object v0, p0, LW/k;->d:LW/g;

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    iput-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {p0}, LW/k;->a()V

    return-void
.end method

.method public abstract a(Z)V
.end method

.method public b()V
    .locals 0

    invoke-virtual {p0}, LW/k;->e()V

    return-void
.end method

.method public b(LU/z;)V
    .locals 0

    return-void
.end method

.method protected b(Z)V
    .locals 3

    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    iget-object v1, p0, LW/k;->c:LW/j;

    invoke-virtual {v1}, LW/j;->c()Lbi/a;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->d(Lbi/a;)V

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->f()V

    :goto_1
    new-instance v2, LW/m;

    invoke-direct {v2, p0, v1}, LW/m;-><init>(LW/k;Lbi/a;)V

    invoke-interface {v0, v2}, Lcom/google/android/maps/rideabout/view/j;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->e()V

    goto :goto_1
.end method

.method public c()V
    .locals 2

    invoke-virtual {p0}, LW/k;->e()V

    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    new-instance v1, LW/l;

    invoke-direct {v1, p0, v0}, LW/l;-><init>(LW/k;Lcom/google/android/maps/rideabout/view/j;)V

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public c(LU/z;)V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    invoke-virtual {p0}, LW/k;->e()V

    return-void
.end method

.method protected abstract e()V
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, LW/k;->d:LW/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LW/g;->a(Z)V

    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->d()V

    return-void
.end method

.method public i()V
    .locals 3

    iget-object v0, p0, LW/k;->c:LW/j;

    invoke-virtual {v0}, LW/j;->c()Lbi/a;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->c(Lbi/a;)I

    move-result v0

    invoke-interface {v1}, Lcom/google/android/maps/rideabout/view/j;->b()I

    move-result v2

    if-ne v0, v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LW/k;->d:LW/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LW/g;->a(Z)V

    goto :goto_0
.end method

.method public j()V
    .locals 3

    iget-object v0, p0, LW/k;->c:LW/j;

    invoke-virtual {v0}, LW/j;->c()Lbi/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LW/k;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v1

    iget-object v2, p0, LW/k;->d:LW/g;

    invoke-virtual {v2}, LW/g;->a()Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;Z)V

    :cond_0
    return-void
.end method

.method protected k()Lcom/google/android/maps/rideabout/view/j;
    .locals 1

    iget-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->h()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    return-object v0
.end method

.method protected l()Lcom/google/android/maps/rideabout/view/i;
    .locals 1

    iget-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->i()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    return-object v0
.end method

.method protected m()Lcom/google/android/maps/rideabout/view/h;
    .locals 1

    iget-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->j()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    return-object v0
.end method

.method protected n()V
    .locals 4

    iget-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->n()Lcom/google/googlenav/ui/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    goto :goto_0
.end method

.method protected o()V
    .locals 4

    iget-object v0, p0, LW/k;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->n()Lcom/google/googlenav/ui/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xf1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    goto :goto_0
.end method

.method public p()V
    .locals 2

    iget-object v0, p0, LW/k;->d:LW/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LW/g;->b(Z)V

    invoke-virtual {p0}, LW/k;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/i;->c()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LW/k;->a(Z)V

    return-void
.end method
