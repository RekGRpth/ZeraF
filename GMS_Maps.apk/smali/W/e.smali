.class public LW/e;
.super LW/k;
.source "SourceFile"


# static fields
.field public static final a:LW/e;


# instance fields
.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LW/e;

    invoke-direct {v0}, LW/e;-><init>()V

    sput-object v0, LW/e;->a:LW/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, LW/k;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LW/e;->h:Z

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LW/e;->h:Z

    return-void
.end method

.method public a(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/e;->b:LW/h;

    sget-object v1, LW/n;->a:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->a(LU/z;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-virtual {p0}, LW/e;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/e;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->a(Z)V

    invoke-virtual {p0}, LW/e;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/h;->b()V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-super {p0}, LW/k;->b()V

    iget-boolean v0, p0, LW/e;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LW/e;->h:Z

    iget-object v0, p0, LW/e;->g:Lbi/d;

    invoke-virtual {v0}, Lbi/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LW/e;->n()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LW/e;->o()V

    goto :goto_0
.end method

.method public b(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/e;->b:LW/h;

    sget-object v1, LW/n;->c:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    return-void
.end method

.method protected e()V
    .locals 2

    invoke-virtual {p0}, LW/e;->k()Lcom/google/android/maps/rideabout/view/j;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LW/e;->a(Z)V

    iget-object v0, p0, LW/e;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LW/e;->c:LW/j;

    invoke-virtual {v1, v0}, LW/j;->c(LU/z;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LW/e;->b(Z)V

    return-void
.end method
