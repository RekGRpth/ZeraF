.class public LaV/a;
.super LaV/h;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final N:F

.field private static final O:F

.field private static final P:F

.field private static final Q:F

.field private static final a:F


# instance fields
.field private final A:[F

.field private final B:[F

.field private final C:[F

.field private D:I

.field private E:I

.field private F:I

.field private G:LaV/e;

.field private final H:Lcom/google/googlenav/common/a;

.field private I:J

.field private J:F

.field private final K:Ljava/util/WeakHashMap;

.field private final L:Ljava/util/Map;

.field private M:LaV/d;

.field private R:Landroid/view/WindowManager;

.field private b:Z

.field private c:LaV/j;

.field private d:Landroid/content/Context;

.field private e:Landroid/hardware/SensorManager;

.field private final f:LaV/f;

.field private g:I

.field private h:Z

.field private i:F

.field private j:F

.field private k:Landroid/hardware/Sensor;

.field private l:Landroid/hardware/Sensor;

.field private m:Landroid/hardware/Sensor;

.field private n:Landroid/hardware/Sensor;

.field private final o:[F

.field private final p:[F

.field private final q:[F

.field private final r:[F

.field private final s:[F

.field private final t:[F

.field private final u:[F

.field private final v:[F

.field private final w:[F

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/high16 v2, 0x40000000

    const v1, 0x3c8efa35

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    div-float/2addr v0, v2

    sput v0, LaV/a;->a:F

    const v0, 0x3d0efa35

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->N:F

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->O:F

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->P:F

    const v0, 0x3c0efa35

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->Q:F

    return-void
.end method

.method public constructor <init>(LaV/e;Lcom/google/googlenav/common/a;)V
    .locals 7

    const/16 v6, 0x9

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x3

    invoke-direct {p0}, LaV/h;-><init>()V

    iput-boolean v4, p0, LaV/a;->b:Z

    sget-object v0, LaV/j;->a:LaV/j;

    iput-object v0, p0, LaV/a;->c:LaV/j;

    new-instance v0, LaV/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LaV/a;->f:LaV/f;

    iput v3, p0, LaV/a;->g:I

    iput-boolean v4, p0, LaV/a;->h:Z

    const/high16 v0, -0x40800000

    iput v0, p0, LaV/a;->i:F

    const/high16 v0, 0x7fc00000

    iput v0, p0, LaV/a;->j:F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->o:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->p:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->q:[F

    new-array v0, v5, [F

    iput-object v0, p0, LaV/a;->r:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->s:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->t:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->u:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->v:[F

    new-array v0, v5, [F

    iput-object v0, p0, LaV/a;->w:[F

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaV/a;->x:J

    new-array v0, v6, [F

    iput-object v0, p0, LaV/a;->A:[F

    new-array v0, v6, [F

    iput-object v0, p0, LaV/a;->B:[F

    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->C:[F

    iput v3, p0, LaV/a;->D:I

    iput v3, p0, LaV/a;->E:I

    iput v3, p0, LaV/a;->F:I

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaV/a;->L:Ljava/util/Map;

    new-instance v0, LaV/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaV/d;-><init>(LaV/b;)V

    iput-object v0, p0, LaV/a;->M:LaV/d;

    iput-object p1, p0, LaV/a;->G:LaV/e;

    iput-object p2, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    return-void
.end method

.method public static a([F)F
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    aget v2, p0, v0

    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    sub-float v0, v3, v0

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method protected static a([F[F)F
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget v2, p0, v0

    aget v3, p1, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method private static a(Landroid/hardware/Sensor;)Ljava/lang/String;
    .locals 4

    const-string v0, "sensor of %s \"%s\" v%d by %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    invoke-static {v3}, LaV/a;->b(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(I[I)[Landroid/hardware/Sensor;
    .locals 6

    const/4 v3, 0x1

    const/4 v1, 0x0

    array-length v0, p2

    new-array v4, v0, [Landroid/hardware/Sensor;

    move v0, v1

    move v2, v1

    :goto_0
    array-length v5, p2

    if-ge v0, v5, :cond_4

    aget v5, p2, v0

    invoke-static {v5}, LaV/a;->b(I)Ljava/lang/String;

    invoke-virtual {p0, v5}, LaV/a;->a(I)Landroid/hardware/Sensor;

    move-result-object v5

    aput-object v5, v4, v0

    if-nez v5, :cond_2

    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    invoke-virtual {p0}, LaV/a;->e()V

    :cond_1
    if-eqz v1, :cond_3

    move-object v0, v4

    :goto_2
    return-object v0

    :cond_2
    invoke-static {v5}, LaV/a;->a(Landroid/hardware/Sensor;)Ljava/lang/String;

    invoke-virtual {p0, v5, p1}, LaV/a;->a(Landroid/hardware/Sensor;I)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "TYPE_ROTATION_VECTOR"

    goto :goto_0

    :sswitch_1
    const-string v0, "TYPE_MAGNETIC_FIELD"

    goto :goto_0

    :sswitch_2
    const-string v0, "TYPE_ACCELEROMETER"

    goto :goto_0

    :sswitch_3
    const-string v0, "TYPE_ORIENTATION"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0xb -> :sswitch_0
    .end sparse-switch
.end method

.method public static b([F[F)V
    .locals 7

    const/4 v6, 0x3

    const/4 v0, 0x0

    const/4 v3, 0x0

    move v1, v0

    move v2, v3

    :goto_0
    if-ge v1, v6, :cond_0

    aget v4, p0, v1

    aget v5, p0, v1

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    cmpl-float v2, v1, v3

    if-nez v2, :cond_1

    :goto_1
    if-ge v0, v6, :cond_2

    aput v3, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    if-ge v0, v6, :cond_2

    aget v2, p0, v0

    div-float/2addr v2, v1

    aput v2, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method private k()Landroid/hardware/SensorManager;
    .locals 2

    iget-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, p0, LaV/a;->d:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method private l()I
    .locals 2

    iget v0, p0, LaV/a;->F:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LaV/a;->R:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LaV/a;->F:I

    goto :goto_0
.end method

.method private declared-synchronized m()V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaV/a;->G:LaV/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    iget-wide v0, p0, LaV/a;->I:J

    sub-long v0, v4, v0

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, LaV/a;->G:LaV/e;

    invoke-interface {v0}, LaV/e;->a()Landroid/location/Location;

    move-result-object v3

    if-eqz v3, :cond_0

    iput-wide v4, p0, LaV/a;->I:J

    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    double-to-float v2, v6

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    double-to-float v3, v6

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    iput v0, p0, LaV/a;->J:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized n()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LaV/a;->L:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaV/i;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/c;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    sget-object v4, LaV/b;->a:[I

    invoke-virtual {v0}, LaV/c;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_3
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a(I)Landroid/hardware/Sensor;
    .locals 1

    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    return-object v0
.end method

.method public a(LaV/i;)V
    .locals 2

    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    sget-object v1, LaV/c;->a:LaV/c;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public declared-synchronized a(LaV/j;)V
    .locals 4

    const/4 v1, 0x2

    const/4 v0, 0x3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/android/a;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v2, p0, LaV/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LaV/a;->c:LaV/j;

    if-eq v2, p1, :cond_0

    iput-object p1, p0, LaV/a;->c:LaV/j;

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v1, p1, LaV/j;->d:I

    if-nez v1, :cond_3

    :cond_2
    :goto_1
    invoke-virtual {p0}, LaV/a;->e()V

    sget-boolean v1, Lcom/google/googlenav/android/E;->d:Z

    if-nez v1, :cond_6

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-boolean v1, Lcom/google/googlenav/android/E;->h:Z

    if-nez v1, :cond_5

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0xb

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v0, 0x0

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->n:Landroid/hardware/Sensor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    iget v0, p1, LaV/j;->d:I

    goto :goto_1

    :cond_4
    sget-object v2, LaV/j;->c:LaV/j;

    if-ne p1, v2, :cond_2

    move v0, v1

    goto :goto_1

    :cond_5
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v1

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    const/4 v0, 0x1

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x3

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, LaV/a;->d:Landroid/content/Context;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, LaV/a;->d:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LaV/a;->R:Landroid/view/WindowManager;

    :cond_0
    return-void
.end method

.method public declared-synchronized a()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaV/a;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JZZ)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    if-eqz p3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    if-nez p4, :cond_2

    :try_start_0
    iget-wide v2, p0, LaV/a;->y:J

    iget-wide v4, p0, LaV/a;->z:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_4

    const-wide/high16 v2, 0x4049000000000000L

    :goto_1
    iget-wide v4, p0, LaV/a;->x:J

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    iget-wide v4, p0, LaV/a;->x:J

    sub-long v4, p1, v4

    long-to-double v4, v4

    cmpl-double v2, v4, v2

    if-lez v2, :cond_5

    :cond_3
    if-nez p4, :cond_0

    iget-object v1, p0, LaV/a;->o:[F

    iget-object v2, p0, LaV/a;->s:[F

    invoke-static {v1, v2}, LaV/a;->b([F[F)V

    iget-object v1, p0, LaV/a;->p:[F

    iget-object v2, p0, LaV/a;->t:[F

    invoke-static {v1, v2}, LaV/a;->b([F[F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    const-wide/high16 v2, 0x4059000000000000L

    goto :goto_1

    :cond_5
    if-eqz p4, :cond_8

    :try_start_1
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_7

    sget v2, LaV/a;->Q:F

    float-to-double v2, v2

    :goto_2
    iget-object v4, p0, LaV/a;->r:[F

    iget-object v5, p0, LaV/a;->w:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v4, v2

    if-ltz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    sget v2, LaV/a;->P:F

    float-to-double v2, v2

    goto :goto_2

    :cond_8
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_9

    sget v2, LaV/a;->O:F

    float-to-double v2, v2

    :goto_3
    iget-object v4, p0, LaV/a;->o:[F

    iget-object v5, p0, LaV/a;->s:[F

    invoke-static {v4, v5}, LaV/a;->b([F[F)V

    iget-object v4, p0, LaV/a;->p:[F

    iget-object v5, p0, LaV/a;->t:[F

    invoke-static {v4, v5}, LaV/a;->b([F[F)V

    iget-object v4, p0, LaV/a;->s:[F

    iget-object v5, p0, LaV/a;->u:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v4, v4, v2

    if-ltz v4, :cond_0

    iget-object v4, p0, LaV/a;->t:[F

    iget-object v5, p0, LaV/a;->v:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v4, v2

    if-gez v2, :cond_6

    goto/16 :goto_0

    :cond_9
    sget v2, LaV/a;->N:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    float-to-double v2, v2

    goto :goto_3
.end method

.method protected a(Landroid/hardware/Sensor;I)Z
    .locals 1

    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaV/a;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(JZZ)V
    .locals 9

    const/high16 v7, 0x42b40000

    const v6, 0x42652ee0

    const/4 v0, 0x1

    monitor-enter p0

    if-eqz p3, :cond_0

    :try_start_0
    iget-object v0, p0, LaV/a;->q:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-direct {p0}, LaV/a;->l()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(F)F

    move-result v0

    invoke-direct {p0}, LaV/a;->m()V

    iget v1, p0, LaV/a;->J:F

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(F)F

    move-result v0

    iget-object v1, p0, LaV/a;->f:LaV/f;

    invoke-virtual {v1, p1, p2, v0}, LaV/f;->a(JF)F

    move-result v0

    iput v0, p0, LaV/a;->i:F

    invoke-direct {p0}, LaV/a;->n()V

    iget-object v0, p0, LaV/a;->M:LaV/d;

    iget v3, p0, LaV/a;->i:F

    iget v4, p0, LaV/a;->j:F

    iget-object v5, p0, LaV/a;->c:LaV/j;

    move-wide v1, p1

    invoke-static/range {v0 .. v5}, LaV/d;->a(LaV/d;JFFLaV/j;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/i;

    iget v2, p0, LaV/a;->i:F

    iget v3, p0, LaV/a;->j:F

    invoke-interface {v0, v2, v3}, LaV/i;->a(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_0
    add-float/2addr v0, v7

    goto :goto_0

    :pswitch_1
    sub-float/2addr v0, v7

    goto :goto_0

    :pswitch_2
    const/high16 v1, 0x43340000

    add-float/2addr v0, v1

    goto :goto_0

    :cond_0
    :try_start_1
    iput-wide p1, p0, LaV/a;->x:J

    if-eqz p4, :cond_4

    iget-object v1, p0, LaV/a;->r:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->w:[F

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, LaV/a;->A:[F

    iget-object v2, p0, LaV/a;->r:[F

    invoke-static {v1, v2}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    :cond_1
    invoke-direct {p0}, LaV/a;->l()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    const/4 v1, 0x2

    move v8, v1

    move v1, v0

    move v0, v8

    :goto_2
    iget-object v2, p0, LaV/a;->A:[F

    iget-object v3, p0, LaV/a;->B:[F

    invoke-static {v2, v1, v0, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_2
    iget-object v0, p0, LaV/a;->B:[F

    const/4 v1, 0x7

    aget v0, v0, v1

    sget v1, LaV/a;->a:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget-object v0, p0, LaV/a;->B:[F

    const/4 v1, 0x1

    const/4 v2, 0x3

    iget-object v3, p0, LaV/a;->A:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_3
    iget-object v0, p0, LaV/a;->A:[F

    iget-object v1, p0, LaV/a;->C:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, v6

    sub-float/2addr v0, v7

    iput v0, p0, LaV/a;->j:F

    :goto_3
    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    mul-float/2addr v0, v6

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, LaV/a;->s:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->u:[F

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, LaV/a;->t:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->v:[F

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, LaV/a;->A:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->p:[F

    iget-object v4, p0, LaV/a;->o:[F

    invoke-static {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_5
    monitor-exit p0

    return-void

    :pswitch_3
    const/4 v1, 0x2

    const/16 v0, 0x81

    goto :goto_2

    :pswitch_4
    const/16 v1, 0x82

    goto :goto_2

    :pswitch_5
    const/16 v1, 0x81

    const/16 v0, 0x82

    goto :goto_2

    :cond_6
    :try_start_2
    iget-object v0, p0, LaV/a;->B:[F

    iget-object v1, p0, LaV/a;->C:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, v6

    iput v0, p0, LaV/a;->j:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public b(LaV/i;)V
    .locals 2

    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    sget-object v1, LaV/c;->b:LaV/c;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaV/a;->i:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaV/a;->i:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected e()V
    .locals 1

    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

.method protected declared-synchronized f()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaV/a;->b:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaV/a;->y:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaV/a;->z:J

    sget-object v0, LaV/j;->b:LaV/j;

    invoke-virtual {p0, v0}, LaV/a;->a(LaV/j;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LaV/a;->e()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaV/a;->b:Z

    sget-object v0, LaV/j;->a:LaV/j;

    iput-object v0, p0, LaV/a;->c:LaV/j;

    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_2

    iput p2, p0, LaV/a;->E:I

    :cond_0
    :goto_0
    iget-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, LaV/a;->n:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_3

    :cond_1
    :goto_1
    iget v0, p0, LaV/a;->g:I

    if-eq p2, v0, :cond_4

    iput p2, p0, LaV/a;->g:I

    invoke-direct {p0}, LaV/a;->n()V

    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/i;

    invoke-interface {v0, p2}, LaV/i;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_0

    iput p2, p0, LaV/a;->D:I

    goto :goto_0

    :cond_3
    iget v0, p0, LaV/a;->E:I

    iget v1, p0, LaV/a;->D:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result p2

    goto :goto_1

    :cond_4
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10

    const/4 v0, 0x1

    const/4 v9, 0x3

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->m:Landroid/hardware/Sensor;

    if-ne v2, v5, :cond_2

    move v2, v0

    :goto_0
    iget-object v5, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, LaV/a;->n:Landroid/hardware/Sensor;

    if-ne v5, v6, :cond_3

    :goto_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->k:Landroid/hardware/Sensor;

    if-ne v1, v5, :cond_4

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->o:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-wide v3, p0, LaV/a;->y:J

    :cond_0
    :goto_2
    invoke-virtual {p0, v3, v4, v2, v0}, LaV/a;->a(JZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v3, v4, v2, v0}, LaV/a;->b(JZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    :try_start_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->l:Landroid/hardware/Sensor;

    if-ne v1, v5, :cond_5

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->p:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-wide v3, p0, LaV/a;->z:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    if-eqz v2, :cond_6

    :try_start_2
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->q:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_6
    if-eqz v0, :cond_0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->r:[F

    const/4 v7, 0x0

    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v8, v8

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v1, v1

    if-ne v1, v9, :cond_0

    iget-object v1, p0, LaV/a;->r:[F

    const/4 v5, 0x3

    iget-object v6, p0, LaV/a;->r:[F

    invoke-static {v6}, LaV/a;->a([F)F

    move-result v6

    aput v6, v1, v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method
