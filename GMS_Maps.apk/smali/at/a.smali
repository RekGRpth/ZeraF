.class public Lat/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Z

.field private final f:J


# direct methods
.method public constructor <init>(IIIIZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lat/a;->a:I

    iput p2, p0, Lat/a;->b:I

    iput p4, p0, Lat/a;->c:I

    iput p3, p0, Lat/a;->d:I

    iput-boolean p5, p0, Lat/a;->e:Z

    invoke-static {}, Lat/a;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lat/a;->f:J

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 6

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lat/a;-><init>(IIIIZ)V

    return-void
.end method

.method private static f()J
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lat/a;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lat/a;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lat/a;->c:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lat/a;->e:Z

    return v0
.end method

.method public e()C
    .locals 1

    iget v0, p0, Lat/a;->b:I

    int-to-char v0, v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
