.class public Lm/o;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/util/List;Lm/q;)Lm/v;
    .locals 9

    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v5, v0, [I

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    aput v1, v5, v3

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    add-int/2addr v1, v7

    invoke-virtual {v0}, Lo/X;->c()Lo/T;

    move-result-object v7

    invoke-virtual {v0, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    invoke-virtual {v7, v0}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    add-int/lit8 v0, v1, 0x1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_2
    aput v1, v5, v3

    mul-int/lit8 v0, v1, 0x2

    new-array v3, v0, [D

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-static {v0, v2, v3, v2}, Lm/o;->a(Lo/X;Z[DI)V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v4, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    aget v6, v5, v1

    invoke-static {v0, v4, v3, v6}, Lm/o;->a(Lo/X;Z[DI)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    new-instance v1, Lm/g;

    invoke-static {v3, v5}, Lm/r;->a([D[I)Lm/r;

    move-result-object v0

    invoke-direct {v1, v0}, Lm/g;-><init>(Lm/r;)V

    sget-object v0, Lm/q;->e:Lm/q;

    if-ne p1, v0, :cond_4

    invoke-static {v1}, Lm/h;->b(Lm/g;)Lm/n;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v1, Lm/k;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Verification failed, the polygon violates "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lm/n;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v8}, Lm/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v0}, Lm/k;->a(Lm/n;)V

    throw v1

    :cond_4
    :try_start_0
    invoke-static {v1}, Lm/h;->a(Lm/g;)Lm/v;

    move-result-object v0

    sget-object v2, Lm/p;->a:[I

    invoke-virtual {p1}, Lm/q;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {v0, v1}, Lm/v;->a(Lm/g;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Lm/k;

    const-string v2, "Could not tessellate polygon, area not equal"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lm/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_0
    .catch Lm/k; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lm/p;->a:[I

    invoke-virtual {p1}, Lm/q;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :goto_4
    :pswitch_1
    throw v0

    :pswitch_2
    invoke-static {v1}, Lm/h;->b(Lm/g;)Lm/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lm/k;->a(Lm/n;)V

    goto :goto_4

    :cond_5
    move v0, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lm/v;LE/e;I)V
    .locals 6

    const/4 v1, 0x0

    invoke-interface {p1}, LE/e;->b()I

    move-result v0

    invoke-virtual {p0}, Lm/v;->d()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    invoke-interface {p1, v0}, LE/e;->b(I)V

    invoke-virtual {p0}, Lm/v;->d()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lm/v;->a(II)I

    move-result v3

    add-int/2addr v3, p2

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4}, Lm/v;->a(II)I

    move-result v4

    add-int/2addr v4, p2

    const/4 v5, 0x2

    invoke-virtual {p0, v0, v5}, Lm/v;->a(II)I

    move-result v5

    add-int/2addr v5, p2

    invoke-interface {p1, v3, v4, v5}, LE/e;->a(III)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(Lm/v;LE/q;Lo/T;I)V
    .locals 6

    new-instance v1, Lo/T;

    invoke-direct {v1}, Lo/T;-><init>()V

    invoke-virtual {p0}, Lm/v;->e()I

    move-result v2

    invoke-interface {p1}, LE/q;->a()I

    move-result v0

    add-int/2addr v0, v2

    invoke-interface {p1, v0}, LE/q;->a(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lm/v;->a(I)D

    move-result-wide v3

    double-to-int v3, v3

    invoke-virtual {p0, v0}, Lm/v;->b(I)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lo/T;->d(II)V

    invoke-static {v1, p2, v1}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    invoke-interface {p1, v1, p3}, LE/q;->a(Lo/T;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(Lo/X;Z[DI)V
    .locals 8

    const/4 v2, 0x0

    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-eqz p1, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v1

    invoke-virtual {p0}, Lo/X;->c()Lo/T;

    move-result-object v5

    invoke-virtual {p0, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    :goto_2
    if-ge v2, v1, :cond_3

    mul-int v5, v0, v2

    add-int/2addr v5, v3

    invoke-virtual {p0, v5, v4}, Lo/X;->a(ILo/T;)V

    add-int v5, p3, v2

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    int-to-double v6, v6

    aput-wide v6, p2, v5

    add-int v5, p3, v2

    mul-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v6

    int-to-double v6, v6

    aput-wide v6, p2, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v3, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method
