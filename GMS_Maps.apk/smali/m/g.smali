.class public Lm/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lm/r;


# direct methods
.method constructor <init>(Lm/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm/g;->a:Lm/r;

    return-void
.end method


# virtual methods
.method public a()D
    .locals 2

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->e()D

    move-result-wide v0

    return-wide v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->g()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->h()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->f()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->i()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lm/g;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lm/g;

    iget-object v0, p1, Lm/g;->a:Lm/r;

    iget-object v1, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0, v1}, Lm/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method f()Lm/r;
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v0}, Lm/r;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lm/g;->a:Lm/r;

    invoke-virtual {v1}, Lm/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
