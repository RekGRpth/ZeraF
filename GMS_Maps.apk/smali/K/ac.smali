.class public LK/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LK/ab;

.field private volatile c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LK/ac;->a:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, LK/ac;->b:LK/ab;

    return-void
.end method

.method private declared-synchronized a(ILK/ae;)V
    .locals 11

    const/4 v1, 0x0

    const/4 v9, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, -0x2

    monitor-enter p0

    if-nez p1, :cond_9

    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    iget-object v0, p0, LK/ac;->b:LK/ab;

    invoke-interface {v0}, LK/ab;->b()Ljava/util/Locale;

    move-result-object v0

    iget-object v2, p0, LK/ac;->b:LK/ab;

    invoke-interface {v2, v0}, LK/ab;->b(Ljava/util/Locale;)I

    move-result v2

    invoke-static {v6, v0}, LK/af;->b(Ljava/util/Locale;Ljava/util/Locale;)Z

    move-result v7

    invoke-static {v6, v0}, LK/af;->a(Ljava/util/Locale;Ljava/util/Locale;)Z

    move-result v8

    if-eq v2, v3, :cond_0

    if-eqz v7, :cond_0

    if-nez v8, :cond_1

    :cond_0
    iget-object v2, p0, LK/ac;->b:LK/ab;

    invoke-interface {v2, v6}, LK/ab;->a(Ljava/util/Locale;)I

    move-result v2

    :cond_1
    if-eq v2, v3, :cond_2

    if-ne v2, v9, :cond_5

    :cond_2
    move v3, v5

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_0
    const/4 v4, 0x1

    iput-boolean v4, p0, LK/ac;->c:Z

    if-nez v1, :cond_6

    const-string v1, "?"

    :goto_1
    const-string v4, "E"

    if-nez v0, :cond_3

    const-string v0, "?"

    :cond_3
    invoke-static {v4, v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "L"

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "S"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    if-eqz v3, :cond_7

    const/4 v0, 0x0

    invoke-interface {p2, v0}, LK/ae;->a(I)V

    :cond_4
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_5
    :try_start_1
    iget-object v1, p0, LK/ac;->b:LK/ab;

    invoke-interface {v1}, LK/ab;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LK/ac;->b:LK/ab;

    invoke-static {v1}, LK/aa;->a(Ljava/lang/String;)LK/aa;

    move-result-object v5

    invoke-interface {v3, v5}, LK/ab;->a(LK/aa;)V

    move v3, v4

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_7
    if-ne v2, v9, :cond_8

    const/4 v0, 0x1

    invoke-interface {p2, v0}, LK/ae;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    const/4 v0, 0x2

    :try_start_2
    invoke-interface {p2, v0}, LK/ae;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_9
    move-object v0, v1

    move v2, v3

    move v3, v5

    goto :goto_0
.end method

.method static synthetic a(LK/ac;ILK/ae;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LK/ac;->a(ILK/ae;)V

    return-void
.end method

.method private declared-synchronized a(LK/ae;LK/ab;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p2, p0, LK/ac;->b:LK/ab;

    iget-object v0, p0, LK/ac;->b:LK/ab;

    new-instance v1, LK/ad;

    invoke-direct {v1, p0, p1}, LK/ad;-><init>(LK/ac;LK/ae;)V

    invoke-interface {v0, v1}, LK/ab;->a(Landroid/speech/tts/TextToSpeech$OnInitListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()LK/ab;
    .locals 1

    iget-object v0, p0, LK/ac;->b:LK/ab;

    return-object v0
.end method

.method public a(LK/ae;)V
    .locals 2

    new-instance v0, LK/af;

    iget-object v1, p0, LK/ac;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/af;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, LK/ac;->a(LK/ae;LK/ab;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, LK/ac;->c:Z

    if-nez v1, :cond_1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LK/ac;->c:Z

    if-nez v1, :cond_0

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-boolean v1, p0, LK/ac;->c:Z

    if-nez v1, :cond_2

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    iget-object v0, p0, LK/ac;->b:LK/ab;

    invoke-interface {v0}, LK/ab;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
