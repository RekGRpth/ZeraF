.class LK/I;
.super Law/a;
.source "SourceFile"


# instance fields
.field final synthetic a:LK/F;

.field private final b:Ljava/util/Locale;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:LK/G;


# direct methods
.method public constructor <init>(LK/F;Ljava/util/Locale;ILK/G;)V
    .locals 6

    invoke-virtual {p1, p3}, LK/F;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LK/I;-><init>(LK/F;Ljava/util/Locale;ILjava/lang/String;LK/G;)V

    return-void
.end method

.method public constructor <init>(LK/F;Ljava/util/Locale;ILjava/lang/String;LK/G;)V
    .locals 0

    iput-object p1, p0, LK/I;->a:LK/F;

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p2, p0, LK/I;->b:Ljava/util/Locale;

    iput p3, p0, LK/I;->c:I

    iput-object p4, p0, LK/I;->d:Ljava/lang/String;

    iput-object p5, p0, LK/I;->e:LK/G;

    return-void
.end method

.method static synthetic a(LK/I;)I
    .locals 1

    iget v0, p0, LK/I;->c:I

    return v0
.end method

.method static synthetic b(LK/I;)Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, LK/I;->b:Ljava/util/Locale;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fj;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/file/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LK/I;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LK/I;->e:LK/G;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, LK/I;->e:LK/G;

    invoke-virtual {v2}, LK/G;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    array-length v1, v0

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fj;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_0

    const-string v1, "CannedSpeechManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Empty response for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LK/I;->b:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " voice bundle"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    iget-object v2, p0, LK/I;->b:Ljava/util/Locale;

    iget v3, p0, LK/I;->c:I

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    iget-object v6, p0, LK/I;->a:LK/F;

    invoke-static {v6}, LK/F;->a(LK/F;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, LK/G;->a(Ljava/util/Locale;IJLjava/lang/String;)LK/G;

    move-result-object v2

    iget-object v3, p0, LK/I;->a:LK/F;

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    invoke-static {v3, p0, v2, v0}, LK/F;->a(LK/F;LK/I;LK/G;[B)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v0, 0x130

    if-ne v2, v0, :cond_3

    iget-object v0, p0, LK/I;->e:LK/G;

    if-eqz v0, :cond_2

    iget-object v0, p0, LK/I;->e:LK/G;

    invoke-virtual {v0}, LK/G;->h()V

    iget-object v0, p0, LK/I;->a:LK/F;

    invoke-static {v0}, LK/F;->b(LK/F;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, LK/I;->a:LK/F;

    invoke-static {v0, p0}, LK/F;->a(LK/F;LK/I;)V

    goto :goto_1

    :cond_3
    const/16 v0, 0x194

    if-ne v2, v0, :cond_4

    const-string v0, "CannedSpeechManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get voice bundle for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LK/I;->b:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LK/I;->a:LK/F;

    invoke-static {v0, p0}, LK/F;->b(LK/F;LK/I;)V

    goto :goto_1

    :cond_4
    const-string v0, "CannedSpeechManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get voice bundle for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LK/I;->b:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LK/I;->a:LK/F;

    invoke-static {v0, p0}, LK/F;->a(LK/F;LK/I;)V

    goto :goto_1
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x27

    return v0
.end method
