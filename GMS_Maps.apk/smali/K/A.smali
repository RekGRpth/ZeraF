.class public LK/A;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:LK/A;

.field private static final d:LK/A;


# instance fields
.field private final a:Z

.field private final b:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, LK/A;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, LK/A;-><init>(ZLjava/io/File;)V

    sput-object v0, LK/A;->c:LK/A;

    new-instance v0, LK/A;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, LK/A;-><init>(ZLjava/io/File;)V

    sput-object v0, LK/A;->d:LK/A;

    return-void
.end method

.method private constructor <init>(ZLjava/io/File;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, LK/A;->a:Z

    iput-object p2, p0, LK/A;->b:Ljava/io/File;

    return-void
.end method

.method public static a()LK/A;
    .locals 1

    sget-object v0, LK/A;->d:LK/A;

    return-object v0
.end method

.method public static a(Ljava/io/File;)LK/A;
    .locals 2

    if-nez p0, :cond_0

    sget-object v0, LK/A;->c:LK/A;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LK/A;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, LK/A;-><init>(ZLjava/io/File;)V

    goto :goto_0
.end method

.method public static b()LK/A;
    .locals 1

    sget-object v0, LK/A;->c:LK/A;

    return-object v0
.end method


# virtual methods
.method public c()Ljava/io/File;
    .locals 1

    iget-object v0, p0, LK/A;->b:Ljava/io/File;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, LK/A;->a:Z

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, LK/A;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LK/A;->b:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
