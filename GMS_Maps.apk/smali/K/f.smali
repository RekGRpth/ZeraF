.class LK/f;
.super LK/a;
.source "SourceFile"


# instance fields
.field private a:LK/a;

.field private b:Ljava/lang/String;

.field private c:Ljava/io/File;

.field private final d:Landroid/os/Handler;

.field private e:I

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, LK/a;-><init>()V

    iput-object p1, p0, LK/f;->f:Landroid/content/Context;

    iput-object p2, p0, LK/f;->b:Ljava/lang/String;

    iput-object p3, p0, LK/f;->c:Ljava/io/File;

    iput-object p4, p0, LK/f;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(LK/f;LK/a;)LK/a;
    .locals 0

    iput-object p1, p0, LK/f;->a:LK/a;

    return-object p1
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LK/f;->a:LK/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/f;->a:LK/a;

    invoke-virtual {v0}, LK/a;->a()V

    :cond_0
    return-void
.end method

.method public a(LK/b;)V
    .locals 3

    iget-object v0, p0, LK/f;->c:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "AlertGenerator"

    const-string v1, "mFile=null"

    invoke-static {v0, v1}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p0}, LK/b;->a(LK/a;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LK/f;->f:Landroid/content/Context;

    iget-object v1, p0, LK/f;->c:Ljava/io/File;

    iget-object v2, p0, LK/f;->d:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, LK/P;->a(Landroid/content/Context;Ljava/io/File;Landroid/os/Handler;)LK/a;

    move-result-object v0

    iput-object v0, p0, LK/f;->a:LK/a;

    iget-object v0, p0, LK/f;->a:LK/a;

    if-nez v0, :cond_1

    invoke-interface {p1, p0}, LK/b;->a(LK/a;)V

    :goto_1
    iget v0, p0, LK/f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LK/f;->e:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, LK/f;->a:LK/a;

    new-instance v1, LK/g;

    invoke-direct {v1, p0, p1}, LK/g;-><init>(LK/f;LK/b;)V

    invoke-virtual {v0, v1}, LK/a;->a(LK/b;)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, LK/f;->a:LK/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/f;->a:LK/a;

    invoke-virtual {v0}, LK/a;->b()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LK/f;->c:Ljava/io/File;

    iput-object v0, p0, LK/f;->b:Ljava/lang/String;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LK/f;->b:Ljava/lang/String;

    return-object v0
.end method
