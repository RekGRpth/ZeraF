.class public LK/K;
.super LK/a;
.source "SourceFile"


# instance fields
.field private final a:[LK/a;


# direct methods
.method public constructor <init>([LK/a;)V
    .locals 0

    invoke-direct {p0}, LK/a;-><init>()V

    iput-object p1, p0, LK/K;->a:[LK/a;

    return-void
.end method

.method static synthetic a(LK/K;)[LK/a;
    .locals 1

    iget-object v0, p0, LK/K;->a:[LK/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v1, p0, LK/K;->a:[LK/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, LK/a;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LK/b;)V
    .locals 1

    new-instance v0, LK/M;

    invoke-direct {v0, p0, p1}, LK/M;-><init>(LK/K;LK/b;)V

    invoke-virtual {v0}, LK/M;->a()Z

    return-void
.end method

.method public b()V
    .locals 4

    iget-object v1, p0, LK/K;->a:[LK/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, LK/a;->b()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, LK/K;->a:[LK/a;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, LK/a;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
