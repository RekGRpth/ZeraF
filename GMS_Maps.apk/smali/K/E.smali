.class public LK/E;
.super LK/F;
.source "SourceFile"


# static fields
.field private static a:LK/E;


# instance fields
.field private b:LK/ab;


# direct methods
.method constructor <init>(Law/p;Landroid/content/Context;LK/ab;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LK/F;-><init>(Law/p;Landroid/content/Context;)V

    iput-object p3, p0, LK/E;->b:LK/ab;

    return-void
.end method

.method public static a(Law/p;Landroid/content/Context;LK/ab;)LK/E;
    .locals 1

    sget-object v0, LK/E;->a:LK/E;

    if-nez v0, :cond_0

    new-instance v0, LK/E;

    invoke-direct {v0, p0, p1, p2}, LK/E;-><init>(Law/p;Landroid/content/Context;LK/ab;)V

    sput-object v0, LK/E;->a:LK/E;

    :cond_0
    sget-object v0, LK/E;->a:LK/E;

    return-object v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "/cannedtts/"

    return-object v0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string v0, "voice_instructions.xml"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "voice_instructions_imperial.xml"

    goto :goto_0

    :pswitch_1
    const-string v0, "voice_instructions_yards.xml"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(LK/w;)V
    .locals 6

    invoke-virtual {p1}, LK/w;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/B;

    invoke-virtual {v0}, LK/B;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "g"

    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iget-object v3, p0, LK/E;->b:LK/ab;

    invoke-virtual {v0}, LK/B;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface {v3, v0, v4, v2, v5}, LK/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string v0, "TtsVoiceBundles"

    return-object v0
.end method
