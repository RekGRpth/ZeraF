.class public LF/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# instance fields
.field private final a:Lo/ad;

.field private final b:Lo/aq;

.field private final c:LA/c;

.field private d:[Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:I

.field private g:LF/P;

.field private final h:[F

.field private i:J

.field private j:LF/m;


# direct methods
.method private constructor <init>(Lo/aq;LA/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LF/k;->h:[F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LF/k;->i:J

    iput-object p1, p0, LF/k;->b:Lo/aq;

    iput-object p2, p0, LF/k;->c:LA/c;

    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/k;->a:Lo/ad;

    return-void
.end method

.method public static a(Lo/ap;LD/a;)LF/k;
    .locals 3

    new-instance v0, LF/k;

    invoke-interface {p0}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-interface {p0}, Lo/ap;->g()LA/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LF/k;-><init>(Lo/aq;LA/c;)V

    instance-of v1, p0, Lo/x;

    if-eqz v1, :cond_0

    check-cast p0, Lo/x;

    invoke-direct {v0, p0, p1}, LF/k;->a(Lo/x;LD/a;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {v0}, LF/k;->k()V

    goto :goto_0
.end method

.method private a(Lo/x;LD/a;)V
    .locals 2

    invoke-virtual {p1}, Lo/x;->f()[B

    move-result-object v0

    iget-object v1, p0, LF/k;->b:Lo/aq;

    invoke-static {v0, v1, p2}, LF/P;->a([BLo/aq;LD/a;)LF/P;

    move-result-object v0

    iput-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {p1}, Lo/x;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LF/k;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lo/x;->b()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LF/k;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lo/x;->c()I

    move-result v0

    iput v0, p0, LF/k;->f:I

    return-void
.end method

.method private k()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, LF/k;->d:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, LF/k;->e:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, LF/k;->f:I

    return-void
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v1, :cond_0

    const v0, 0x8000

    :cond_0
    iget-object v1, p0, LF/k;->g:LF/P;

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    return v0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1, p2}, LF/P;->a(J)V

    :cond_0
    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .locals 4

    iget-object v1, p0, LF/k;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p3, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LC/a;LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .locals 4

    iget-object v1, p0, LF/k;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1}, LF/P;->a(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/k;->j:LF/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/k;->j:LF/m;

    invoke-virtual {v0, p1}, LF/m;->a(LD/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, LF/k;->j:LF/m;

    :cond_1
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-nez v0, :cond_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-ne v0, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v2

    iget-wide v4, p0, LF/k;->i:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v2

    iput-wide v2, p0, LF/k;->i:J

    iget-object v0, p0, LF/k;->a:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, LC/a;->j()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, LC/a;->q()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, LC/a;->p()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, LC/a;->r()F

    move-result v2

    invoke-virtual {p2}, LC/a;->r()F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    iget-object v2, p1, LD/a;->k:[F

    invoke-virtual {p2, v0, v2}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p1, LD/a;->k:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p1, LD/a;->k:[F

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    :cond_1
    iget-object v2, p0, LF/k;->a:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LF/k;->h:[F

    invoke-static {p1, p2, v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F[F)V

    :cond_2
    iget-object v0, p0, LF/k;->h:[F

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-ne v0, v6, :cond_4

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1, p2, p3}, LF/P;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0

    :cond_4
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    const/16 v2, 0xf

    if-ne v0, v2, :cond_3

    sget-object v0, LF/U;->a:LF/U;

    invoke-virtual {v0, p1, p2, p3}, LF/U;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_1
.end method

.method public a(LF/m;)V
    .locals 0

    iput-object p1, p0, LF/k;->j:LF/m;

    return-void
.end method

.method public a(Ly/b;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lo/aq;
    .locals 1

    iget-object v0, p0, LF/k;->b:Lo/aq;

    return-object v0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1}, LF/P;->b(LD/a;)V

    :cond_0
    iget-object v0, p0, LF/k;->j:LF/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/k;->j:LF/m;

    invoke-virtual {v0, p1}, LF/m;->b(LD/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, LF/k;->j:LF/m;

    :cond_1
    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    invoke-static {p1, v0}, LF/P;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()LA/c;
    .locals 1

    iget-object v0, p0, LF/k;->c:LA/c;

    return-object v0
.end method

.method public d()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public e()LF/m;
    .locals 1

    iget-object v0, p0, LF/k;->j:LF/m;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->e()V

    :cond_0
    return-void
.end method

.method public h()I
    .locals 1

    iget v0, p0, LF/k;->f:I

    return v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, LF/k;->g:LF/P;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->a()I

    move-result v0

    goto :goto_0
.end method

.method public j()I
    .locals 2

    const/16 v0, 0x88

    iget-object v1, p0, LF/k;->g:LF/P;

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/k;->g:LF/P;

    invoke-virtual {v1}, LF/P;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
