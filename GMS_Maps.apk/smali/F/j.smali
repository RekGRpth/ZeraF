.class public LF/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# instance fields
.field private final a:Lo/aq;

.field private final b:Lo/ad;

.field private final c:LE/i;


# direct methods
.method public constructor <init>(Lo/aq;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LE/i;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, LF/j;->c:LE/i;

    iput-object p1, p0, LF/j;->a:Lo/aq;

    iget-object v0, p0, LF/j;->a:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/j;->b:Lo/ad;

    const/high16 v0, 0x10000

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x20

    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v2, v2}, LE/i;->a(II)V

    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v2, v0}, LE/i;->a(II)V

    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v0, v2}, LE/i;->a(II)V

    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v0, v0}, LE/i;->a(II)V

    return-void
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(LC/a;LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, LF/j;->b:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, LF/j;->b:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method public a(LF/m;)V
    .locals 0

    return-void
.end method

.method public a(Ly/b;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lo/aq;
    .locals 1

    iget-object v0, p0, LF/j;->a:Lo/aq;

    return-object v0
.end method

.method public b(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, LF/j;->a(LD/a;)V

    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    const/4 v2, 0x1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-le v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x303

    invoke-interface {v0, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p1}, LD/a;->p()V

    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    const/16 v1, 0x14

    invoke-static {p1, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()LA/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public e()LF/m;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, LF/j;->c:LE/i;

    invoke-virtual {v0}, LE/i;->b()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, LF/j;->c:LE/i;

    invoke-virtual {v0}, LE/i;->c()I

    move-result v0

    return v0
.end method
