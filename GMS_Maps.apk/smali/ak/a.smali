.class public Lak/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lak/a;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lak/a;->a:Landroid/content/Context;

    invoke-static {v0, v3}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    move-result-object v0

    new-instance v1, Lak/b;

    sget-object v2, Lak/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lak/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LaM/f;->a(LaM/h;)V

    invoke-static {v3, v3, v3}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/bi;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LaM/f;->o()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v3}, LaM/f;->b(LaM/g;)V

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized c(Landroid/content/Context;)V
    .locals 2

    const-class v1, Lak/a;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lak/a;->b:Z

    if-nez v0, :cond_0

    sput-object p0, Lak/a;->a:Landroid/content/Context;

    invoke-static {p0}, Lak/a;->d(Landroid/content/Context;)V

    const/4 v0, 0x1

    sput-boolean v0, Lak/a;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static d(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {p0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    invoke-virtual {v0}, Las/c;->d()V

    invoke-static {p0}, Lak/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->N()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/M;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->H()V

    new-instance v0, Lak/b;

    invoke-direct {v0, p0}, Lak/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lak/b;->M_()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lak/a;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a(Z)V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->a()V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lak/a;->c(Landroid/content/Context;)V

    return-void
.end method
