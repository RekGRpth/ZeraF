.class public final Laa/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Laa/e;Laa/f;)J
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p0}, Laa/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    :try_start_0
    invoke-interface {p1}, Laa/f;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v0, v1}, Laa/a;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v5

    move v4, v3

    :try_start_2
    invoke-static {v1, v4}, Laa/b;->a(Ljava/io/Closeable;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v0, v3}, Laa/b;->a(Ljava/io/Closeable;Z)V

    return-wide v5

    :catchall_0
    move-exception v5

    move v4, v2

    :try_start_3
    invoke-static {v1, v4}, Laa/b;->a(Ljava/io/Closeable;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    move-object v4, v1

    move v1, v2

    :goto_0
    const/4 v5, 0x2

    if-ge v1, v5, :cond_0

    move v1, v2

    :goto_1
    invoke-static {v0, v1}, Laa/b;->a(Ljava/io/Closeable;Z)V

    throw v4

    :cond_0
    move v1, v3

    goto :goto_1

    :catchall_2
    move-exception v1

    move-object v4, v1

    move v1, v3

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 5

    const/16 v0, 0x1000

    new-array v2, v0, [B

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    return-wide v0

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    int-to-long v3, v3

    add-long/2addr v0, v3

    goto :goto_0
.end method
