.class public Lbt/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field private D:I

.field private E:I

.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Landroid/graphics/Bitmap;

.field private final e:Landroid/graphics/Paint;

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Lbu/b;

.field private final s:Lbt/b;

.field private t:F

.field private u:Z

.field private v:Z

.field private final w:Lbu/b;

.field private x:F

.field private y:Z

.field private final z:Lbu/b;


# direct methods
.method public constructor <init>(Lbt/b;IILandroid/graphics/Bitmap;)V
    .locals 7

    const/4 v6, 0x1

    const/16 v5, 0xff

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x437f0000

    iput v0, p0, Lbt/a;->l:F

    iput v1, p0, Lbt/a;->q:I

    iput-boolean v1, p0, Lbt/a;->u:Z

    iput-boolean v1, p0, Lbt/a;->v:Z

    new-instance v0, Lbu/b;

    invoke-direct {v0}, Lbu/b;-><init>()V

    iput-object v0, p0, Lbt/a;->w:Lbu/b;

    iput v4, p0, Lbt/a;->x:F

    iput-boolean v1, p0, Lbt/a;->y:Z

    new-instance v0, Lbu/b;

    invoke-direct {v0}, Lbu/b;-><init>()V

    iput-object v0, p0, Lbt/a;->z:Lbu/b;

    const/16 v0, 0x32

    iput v0, p0, Lbt/a;->A:I

    iput-boolean v1, p0, Lbt/a;->B:Z

    iput-boolean v1, p0, Lbt/a;->C:Z

    iput v1, p0, Lbt/a;->D:I

    iput v1, p0, Lbt/a;->E:I

    iput-object p1, p0, Lbt/a;->s:Lbt/b;

    iput p2, p0, Lbt/a;->a:I

    iput p3, p0, Lbt/a;->b:I

    int-to-float v0, p2

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lbt/a;->c:I

    iput-object p4, p0, Lbt/a;->d:Landroid/graphics/Bitmap;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, -0x3f90c00000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lbt/a;->p:I

    new-instance v0, Lbu/b;

    invoke-direct {v0}, Lbu/b;-><init>()V

    iput-object v0, p0, Lbt/a;->r:Lbu/b;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbt/a;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lbt/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lbt/a;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0, v6}, Lbu/b;->a(I)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0, v4}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0, v4}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0, v6}, Lbu/b;->a(I)V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0, v4}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0, v4}, Lbu/b;->b(F)V

    return-void
.end method

.method private f()V
    .locals 6

    const/high16 v5, 0x40400000

    const/high16 v4, 0x3f800000

    const/4 v2, 0x0

    const/high16 v3, 0x40000000

    const/high16 v0, 0x40c00000

    invoke-static {v2, v0}, Lbu/c;->a(FF)F

    move-result v0

    const/high16 v1, 0x42c80000

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    cmpg-float v2, v0, v4

    if-gtz v2, :cond_0

    neg-float v0, v1

    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Lbu/c;->a(FF)F

    move-result v0

    iput v0, p0, Lbt/a;->k:F

    iget v0, p0, Lbt/a;->a:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->j:F

    :goto_0
    return-void

    :cond_0
    cmpl-float v2, v0, v4

    if-lez v2, :cond_1

    cmpg-float v2, v0, v3

    if-gtz v2, :cond_1

    iget v0, p0, Lbt/a;->b:I

    int-to-float v0, v0

    neg-float v2, v1

    div-float/2addr v2, v3

    div-float/2addr v1, v3

    invoke-static {v2, v1}, Lbu/c;->a(FF)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lbt/a;->k:F

    iget v0, p0, Lbt/a;->a:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->j:F

    goto :goto_0

    :cond_1
    cmpl-float v2, v0, v3

    if-lez v2, :cond_2

    cmpg-float v2, v0, v5

    if-gtz v2, :cond_2

    iget v0, p0, Lbt/a;->b:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->k:F

    neg-float v0, v1

    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Lbu/c;->a(FF)F

    move-result v0

    iput v0, p0, Lbt/a;->j:F

    goto :goto_0

    :cond_2
    cmpl-float v2, v0, v5

    if-lez v2, :cond_3

    const/high16 v2, 0x40800000

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    iget v0, p0, Lbt/a;->b:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->k:F

    iget v0, p0, Lbt/a;->a:I

    int-to-float v0, v0

    neg-float v2, v1

    div-float/2addr v2, v3

    div-float/2addr v1, v3

    invoke-static {v2, v1}, Lbu/c;->a(FF)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lbt/a;->j:F

    goto :goto_0

    :cond_3
    iget v0, p0, Lbt/a;->a:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->j:F

    iget v0, p0, Lbt/a;->b:I

    invoke-static {v0}, Lbu/c;->a(I)F

    move-result v0

    iput v0, p0, Lbt/a;->k:F

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 9

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000

    const/4 v6, 0x0

    const-wide/high16 v4, 0x4024000000000000L

    const/16 v1, 0xff

    iget-boolean v0, p0, Lbt/a;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbt/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Paint;->setARGB(IIII)V

    invoke-direct {p0}, Lbt/a;->f()V

    const-wide v0, 0x3fe999999999999aL

    const-wide v2, 0x3ff3333333333333L

    invoke-static {v0, v1, v2, v3}, Lbu/c;->a(DD)F

    move-result v0

    iput v0, p0, Lbt/a;->t:F

    iput v6, p0, Lbt/a;->g:F

    const/high16 v0, 0x3e800000

    const/4 v1, 0x1

    invoke-static {v1}, Lbu/c;->a(I)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x3fc00000

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000

    add-float/2addr v0, v1

    iput v0, p0, Lbt/a;->h:F

    iget v0, p0, Lbt/a;->j:F

    iget v1, p0, Lbt/a;->k:F

    iget-object v2, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v2}, Lbt/b;->b()F

    move-result v2

    iget-object v3, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v3}, Lbt/b;->c()F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lbu/c;->a(FFFF)F

    move-result v0

    const v1, 0x40490fdb

    add-float/2addr v0, v1

    iput v0, p0, Lbt/a;->f:F

    iput v8, p0, Lbt/a;->n:I

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x406f400000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit16 v0, v0, 0xfa

    iput v0, p0, Lbt/a;->o:I

    new-instance v0, Lbu/b;

    invoke-direct {v0}, Lbu/b;-><init>()V

    iput-object v0, p0, Lbt/a;->r:Lbu/b;

    iget-object v0, p0, Lbt/a;->r:Lbu/b;

    invoke-virtual {v0, v6}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbt/a;->r:Lbu/b;

    const v1, 0x3f19999a

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbt/a;->r:Lbu/b;

    iget v1, p0, Lbt/a;->o:I

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    iput v7, p0, Lbt/a;->l:F

    const/high16 v0, 0x41200000

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lbt/a;->m:F

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    mul-double/2addr v0, v4

    double-to-float v0, v0

    add-float/2addr v0, v7

    iput v0, p0, Lbt/a;->i:F

    iput-boolean v8, p0, Lbt/a;->u:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbt/a;->u:Z

    goto :goto_0
.end method

.method public a(F)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lbt/a;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0, v2}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    iget v1, p0, Lbt/a;->x:F

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    const/high16 v0, 0x41a00000

    const/high16 v1, 0x41f00000

    invoke-static {v0, v1}, Lbu/c;->a(FF)F

    move-result v0

    iput v0, p0, Lbt/a;->x:F

    iput-boolean v3, p0, Lbt/a;->y:Z

    :cond_0
    iget-boolean v0, p0, Lbt/a;->B:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbt/a;->C:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0, v2}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    const/high16 v0, 0x41000000

    div-float v0, p1, v0

    const/high16 v1, 0x42f00000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lbt/a;->A:I

    invoke-static {}, Lbu/c;->a()I

    move-result v0

    iput v0, p0, Lbt/a;->E:I

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    iget v1, p0, Lbt/a;->A:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iput-boolean v3, p0, Lbt/a;->B:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbt/a;->C:Z

    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    const/high16 v5, 0x41a00000

    const/high16 v4, 0x40000000

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lbt/a;->j:F

    iget-object v1, p0, Lbt/a;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    add-float/2addr v0, v1

    iget v1, p0, Lbt/a;->k:F

    iget-object v2, p0, Lbt/a;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lbt/a;->E:I

    iget v1, p0, Lbt/a;->D:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->b()F

    move-result v1

    mul-float/2addr v0, v1

    iget v1, p0, Lbt/a;->E:I

    iget v2, p0, Lbt/a;->D:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    iget-object v2, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v2}, Lbu/b;->b()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lbt/a;->r:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget-object v0, p0, Lbt/a;->r:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->c()F

    move-result v0

    iget-object v1, p0, Lbt/a;->r:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->b()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget v0, p0, Lbt/a;->l:F

    float-to-int v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v0, p0, Lbt/a;->l:F

    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lbt/a;->d:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v3, v3, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method a([[Lbt/c;F)V
    .locals 13

    const-wide/high16 v11, 0x4008000000000000L

    const/16 v10, 0xff

    const/high16 v9, 0x3fc00000

    const/4 v8, 0x0

    const/4 v1, 0x0

    iget v0, p0, Lbt/a;->p:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lbt/a;->p:I

    if-lez v0, :cond_a

    iget-boolean v0, p0, Lbt/a;->u:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget v0, p0, Lbt/a;->D:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbt/a;->D:I

    iget-boolean v0, p0, Lbt/a;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    iget v2, p0, Lbt/a;->x:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lbt/a;->y:Z

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v0, v8}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbt/a;->w:Lbu/b;

    const/high16 v2, 0x41f00000

    const/high16 v3, 0x42f00000

    invoke-static {v2, v3}, Lbu/c;->a(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lbu/b;->a(I)V

    :cond_0
    iget-boolean v0, p0, Lbt/a;->B:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    iget v2, p0, Lbt/a;->A:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lbt/a;->B:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbt/a;->C:Z

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0, v8}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    const/high16 v2, 0x41f00000

    const/high16 v3, 0x42f00000

    invoke-static {v2, v3}, Lbu/c;->a(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lbu/b;->a(I)V

    :cond_1
    :goto_0
    iget v0, p0, Lbt/a;->g:F

    float-to-double v2, v0

    iget v0, p0, Lbt/a;->h:F

    float-to-double v4, v0

    const-wide v6, 0x3fb999999999999aL

    sub-double/2addr v4, v6

    cmpg-double v0, v2, v4

    if-gez v0, :cond_2

    iget v0, p0, Lbt/a;->g:F

    iget v2, p0, Lbt/a;->h:F

    iget v3, p0, Lbt/a;->g:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x42700000

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, p0, Lbt/a;->g:F

    :cond_2
    iget v0, p0, Lbt/a;->n:I

    if-ge v0, v10, :cond_6

    iget v0, p0, Lbt/a;->l:F

    float-to-double v2, v0

    const-wide v4, 0x3ff199999999999aL

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iput v0, p0, Lbt/a;->l:F

    const/high16 v0, 0x437f0000

    iget v2, p0, Lbt/a;->l:F

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lbt/a;->l:F

    :cond_3
    :goto_1
    iget v0, p0, Lbt/a;->q:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    iget v2, p0, Lbt/a;->b:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff4000000000000L

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-float v2, v2

    div-float/2addr v2, p2

    float-to-int v2, v2

    if-ge v0, v2, :cond_9

    move v2, v1

    :goto_3
    iget v3, p0, Lbt/a;->a:I

    iget v4, p0, Lbt/a;->c:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, p2

    float-to-int v3, v3

    if-ge v2, v3, :cond_8

    iget v3, p0, Lbt/a;->j:F

    int-to-float v4, v2

    mul-float/2addr v4, p2

    div-float v5, p2, v9

    sub-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget v3, p0, Lbt/a;->j:F

    int-to-float v4, v2

    mul-float/2addr v4, p2

    div-float v5, p2, v9

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    iget v3, p0, Lbt/a;->k:F

    int-to-float v4, v0

    mul-float/2addr v4, p2

    div-float v5, p2, v9

    sub-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget v3, p0, Lbt/a;->k:F

    int-to-float v4, v0

    mul-float/2addr v4, p2

    div-float v5, p2, v9

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    iget v3, p0, Lbt/a;->j:F

    iget v4, p0, Lbt/a;->k:F

    int-to-float v5, v2

    mul-float/2addr v5, p2

    int-to-float v6, v0

    mul-float/2addr v6, p2

    invoke-static {v3, v4, v5, v6}, Lbu/c;->b(FFFF)F

    move-result v3

    cmpg-float v3, v3, p2

    if-gez v3, :cond_4

    aget-object v3, p1, v0

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lbt/c;->a()F

    move-result v3

    iget v4, p0, Lbt/a;->f:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x40069e955b62f741L

    cmpg-double v3, v3, v5

    if-gez v3, :cond_4

    iget v3, p0, Lbt/a;->f:F

    aget-object v4, p1, v0

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lbt/c;->a()F

    move-result v4

    iget v5, p0, Lbt/a;->f:F

    sub-float/2addr v4, v5

    iget v5, p0, Lbt/a;->i:F

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lbt/a;->f:F

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    iget-boolean v0, p0, Lbt/a;->C:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbt/a;->z:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lbt/a;->C:Z

    goto/16 :goto_0

    :cond_6
    iget v0, p0, Lbt/a;->n:I

    iget v2, p0, Lbt/a;->o:I

    add-int/lit8 v2, v2, -0x64

    if-le v0, v2, :cond_7

    iget v0, p0, Lbt/a;->l:F

    float-to-double v2, v0

    const-wide v4, 0x3feccccccccccccdL

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iput v0, p0, Lbt/a;->l:F

    iget v0, p0, Lbt/a;->m:F

    float-to-double v2, v0

    const-wide v4, 0x3fefae147ae147aeL

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iput v0, p0, Lbt/a;->m:F

    iget v0, p0, Lbt/a;->m:F

    invoke-static {v8, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lbt/a;->m:F

    goto/16 :goto_1

    :cond_7
    iget v0, p0, Lbt/a;->n:I

    iget v2, p0, Lbt/a;->o:I

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lbt/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v10, v10, v1, v1}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_9
    iget v0, p0, Lbt/a;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbt/a;->q:I

    iget v0, p0, Lbt/a;->j:F

    float-to-double v0, v0

    iget v2, p0, Lbt/a;->f:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    iget v4, p0, Lbt/a;->g:F

    iget-object v5, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v5}, Lbu/b;->b()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    div-double/2addr v2, v11

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lbt/a;->j:F

    iget v0, p0, Lbt/a;->k:F

    float-to-double v0, v0

    iget v2, p0, Lbt/a;->f:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    iget v4, p0, Lbt/a;->g:F

    iget-object v5, p0, Lbt/a;->w:Lbu/b;

    invoke-virtual {v5}, Lbu/b;->b()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    div-double/2addr v2, v11

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lbt/a;->k:F

    iget-object v0, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v0}, Lbt/b;->b()F

    move-result v0

    iget-object v1, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v1}, Lbt/b;->c()F

    move-result v1

    iget v2, p0, Lbt/a;->j:F

    iget v3, p0, Lbt/a;->k:F

    invoke-static {v0, v1, v2, v3}, Lbu/c;->b(FFFF)F

    move-result v0

    iget-object v1, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v1}, Lbt/b;->d()F

    move-result v1

    iget v2, p0, Lbt/a;->t:F

    mul-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_a

    iget v1, p0, Lbt/a;->g:F

    float-to-double v1, v1

    const-wide v3, 0x3fee666666666666L

    mul-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, p0, Lbt/a;->g:F

    iget-object v1, p0, Lbt/a;->s:Lbt/b;

    invoke-virtual {v1}, Lbt/b;->e()F

    move-result v1

    const/high16 v2, 0x40000000

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    iget v0, p0, Lbt/a;->n:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lbt/a;->n:I

    :cond_a
    iget v0, p0, Lbt/a;->n:I

    iget v1, p0, Lbt/a;->o:I

    if-le v0, v1, :cond_b

    invoke-virtual {p0}, Lbt/a;->a()V

    :cond_b
    iget v0, p0, Lbt/a;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbt/a;->n:I

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lbt/a;->v:Z

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lbt/a;->u:Z

    return v0
.end method

.method public d()V
    .locals 4

    iget-boolean v0, p0, Lbt/a;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbt/a;->v:Z

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, -0x3f90c00000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lbt/a;->p:I

    invoke-virtual {p0}, Lbt/a;->a()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    iget-boolean v0, p0, Lbt/a;->v:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbt/a;->v:Z

    :cond_0
    return-void
.end method
