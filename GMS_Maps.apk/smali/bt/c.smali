.class public Lbt/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:F

.field private final b:F

.field private final c:Lbt/d;

.field private d:F

.field private e:F

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(Lbt/d;FF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbt/c;->c:Lbt/d;

    iput p2, p0, Lbt/c;->a:F

    iput p3, p0, Lbt/c;->b:F

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lbt/c;->f:F

    return v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lbt/c;->f:F

    return-void
.end method

.method a(FF)V
    .locals 0

    iput p1, p0, Lbt/c;->d:F

    iput p2, p0, Lbt/c;->e:F

    invoke-virtual {p0}, Lbt/c;->b()V

    return-void
.end method

.method public b()V
    .locals 4

    iget v0, p0, Lbt/c;->d:F

    iget v1, p0, Lbt/c;->e:F

    iget-object v2, p0, Lbt/c;->c:Lbt/d;

    invoke-virtual {v2}, Lbt/d;->c()Lbu/f;

    move-result-object v2

    iget v2, v2, Lbu/f;->a:F

    iget-object v3, p0, Lbt/c;->c:Lbt/d;

    invoke-virtual {v3}, Lbt/d;->c()Lbu/f;

    move-result-object v3

    iget v3, v3, Lbu/f;->b:F

    invoke-static {v0, v1, v2, v3}, Lbu/c;->a(FFFF)F

    move-result v0

    const v1, 0x40490fdb

    add-float/2addr v0, v1

    iput v0, p0, Lbt/c;->g:F

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lbt/c;->g:F

    return v0
.end method
