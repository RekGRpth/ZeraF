.class public Lbt/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:F

.field private final d:[[Lbt/c;

.field private final e:Lbu/f;

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F


# direct methods
.method public constructor <init>(Lbt/e;IIFLbu/f;)V
    .locals 9

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    const/high16 v8, 0x40000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbt/d;->f:F

    const v0, 0x3727c5ac

    iput v0, p0, Lbt/d;->g:F

    iput v2, p0, Lbt/d;->j:F

    iput v2, p0, Lbt/d;->k:F

    iput p2, p0, Lbt/d;->a:I

    iput p3, p0, Lbt/d;->b:I

    iput p4, p0, Lbt/d;->c:F

    iput-object p5, p0, Lbt/d;->e:Lbu/f;

    int-to-float v0, p3

    div-float/2addr v0, p4

    float-to-int v0, v0

    int-to-float v2, p2

    div-float/2addr v2, p4

    float-to-int v2, v2

    filled-new-array {v0, v2}, [I

    move-result-object v0

    const-class v2, Lbt/c;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lbt/c;

    iput-object v0, p0, Lbt/d;->d:[[Lbt/c;

    move v2, v1

    :goto_0
    int-to-float v0, p3

    div-float/2addr v0, p4

    float-to-int v0, v0

    if-ge v2, v0, :cond_1

    move v0, v1

    :goto_1
    int-to-float v3, p2

    div-float/2addr v3, p4

    float-to-int v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lbt/d;->d:[[Lbt/c;

    aget-object v3, v3, v2

    new-instance v4, Lbt/c;

    int-to-float v5, v0

    div-float v6, p4, v8

    add-float/2addr v5, v6

    int-to-float v6, v2

    div-float v7, p4, v8

    add-float/2addr v6, v7

    invoke-direct {v4, p0, v5, v6}, Lbt/c;-><init>(Lbt/d;FF)V

    aput-object v4, v3, v0

    iget-object v3, p0, Lbt/d;->d:[[Lbt/c;

    aget-object v3, v3, v2

    aget-object v3, v3, v0

    int-to-float v4, v0

    mul-float/2addr v4, p4

    div-float v5, p4, v8

    add-float/2addr v4, v5

    int-to-float v5, v2

    mul-float/2addr v5, p4

    div-float v6, p4, v8

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lbt/c;->a(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 9

    const/4 v1, 0x0

    iget v0, p0, Lbt/d;->h:F

    iget v2, p0, Lbt/d;->j:F

    add-float/2addr v0, v2

    iput v0, p0, Lbt/d;->h:F

    iget v0, p0, Lbt/d;->i:F

    iget v2, p0, Lbt/d;->k:F

    add-float/2addr v0, v2

    iput v0, p0, Lbt/d;->i:F

    move v0, v1

    :goto_0
    iget v2, p0, Lbt/d;->b:I

    int-to-float v2, v2

    iget v3, p0, Lbt/d;->c:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    if-ge v0, v2, :cond_1

    move v2, v1

    :goto_1
    iget v3, p0, Lbt/d;->a:I

    int-to-float v3, v3

    iget v4, p0, Lbt/d;->c:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    if-ge v2, v3, :cond_0

    int-to-float v3, v2

    iget v4, p0, Lbt/d;->c:F

    mul-float/2addr v3, v4

    int-to-float v3, v0

    iget v4, p0, Lbt/d;->c:F

    mul-float/2addr v3, v4

    const/high16 v3, 0x42b40000

    const/high16 v4, 0x43070000

    add-float/2addr v3, v4

    float-to-double v3, v3

    const-wide v5, 0x3f91df46a2529d39L

    mul-double/2addr v3, v5

    iget-object v5, p0, Lbt/d;->d:[[Lbt/c;

    aget-object v5, v5, v0

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lbt/c;->c()F

    move-result v5

    float-to-double v5, v5

    const-wide v7, 0x3ff921fb54442d18L

    sub-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, p0, Lbt/d;->l:F

    iget-object v3, p0, Lbt/d;->d:[[Lbt/c;

    aget-object v3, v3, v0

    aget-object v3, v3, v2

    iget v4, p0, Lbt/d;->l:F

    invoke-virtual {v3, v4}, Lbt/c;->a(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lbt/d;->f:F

    iget v1, p0, Lbt/d;->g:F

    add-float/2addr v0, v1

    iput v0, p0, Lbt/d;->f:F

    return-void
.end method

.method public a(Lbu/f;)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1, p1}, Lbu/f;->a(Lbu/f;)V

    move v0, v1

    :goto_0
    iget v2, p0, Lbt/d;->b:I

    int-to-float v2, v2

    iget v3, p0, Lbt/d;->c:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    if-ge v0, v2, :cond_1

    move v2, v1

    :goto_1
    iget v3, p0, Lbt/d;->a:I

    int-to-float v3, v3

    iget v4, p0, Lbt/d;->c:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lbt/d;->d:[[Lbt/c;

    aget-object v3, v3, v0

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lbt/c;->b()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a()[[Lbt/c;
    .locals 1

    iget-object v0, p0, Lbt/d;->d:[[Lbt/c;

    return-object v0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lbt/d;->c:F

    return v0
.end method

.method public c()Lbu/f;
    .locals 1

    iget-object v0, p0, Lbt/d;->e:Lbu/f;

    return-object v0
.end method
