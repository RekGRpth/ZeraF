.class public LP/m;
.super LP/h;
.source "SourceFile"


# static fields
.field private static final c:[I


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LP/m;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0d0109
        0x7f0d010a
        0x7f0d010b
        0x7f0d010c
        0x7f0d010d
        0x7f0d010e
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, LP/h;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput p3, p0, LP/m;->b:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LP/m;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    iget v0, p0, LP/m;->b:I

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v1}, LP/m;->a(I)LO/P;

    move-result-object v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, LP/m;->a:Landroid/content/Context;

    sget-object v4, LP/m;->c:[I

    aget v0, v4, v0

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget v0, p0, LP/m;->b:I

    if-ne v0, v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    iget-object v1, p0, LP/m;->a:Landroid/content/Context;

    sget-object v2, LP/m;->c:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method
