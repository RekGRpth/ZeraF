.class public LP/l;
.super LP/h;
.source "SourceFile"


# static fields
.field private static final c:[I


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LP/l;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0d010f
        0x7f0d0112
        0x7f0d0110
        0x7f0d0113
        0x7f0d0111
        0x7f0d0114
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, LP/h;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput p3, p0, LP/l;->b:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LP/l;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LP/l;->a(I)LO/P;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LP/l;->a:Landroid/content/Context;

    const v3, 0x7f0d0115

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, LP/l;->b:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v1}, LP/l;->a(I)LO/P;

    move-result-object v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, LP/l;->a:Landroid/content/Context;

    sget-object v4, LP/l;->c:[I

    aget v0, v4, v0

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p0, LP/l;->b:I

    if-ne v0, v1, :cond_3

    const/4 v0, 0x4

    goto :goto_1

    :cond_2
    iget-object v1, p0, LP/l;->a:Landroid/content/Context;

    sget-object v2, LP/l;->c:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method
