.class public final enum Lh/g;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lh/g;

.field public static final enum b:Lh/g;

.field public static final enum c:Lh/g;

.field private static final synthetic d:[Lh/g;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lh/g;

    const-string v1, "FADE_IN"

    invoke-direct {v0, v1, v2}, Lh/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lh/g;->a:Lh/g;

    new-instance v0, Lh/g;

    const-string v1, "FADE_OUT"

    invoke-direct {v0, v1, v3}, Lh/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lh/g;->b:Lh/g;

    new-instance v0, Lh/g;

    const-string v1, "FADE_BETWEEN"

    invoke-direct {v0, v1, v4}, Lh/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lh/g;->c:Lh/g;

    const/4 v0, 0x3

    new-array v0, v0, [Lh/g;

    sget-object v1, Lh/g;->a:Lh/g;

    aput-object v1, v0, v2

    sget-object v1, Lh/g;->b:Lh/g;

    aput-object v1, v0, v3

    sget-object v1, Lh/g;->c:Lh/g;

    aput-object v1, v0, v4

    sput-object v0, Lh/g;->d:[Lh/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lh/g;
    .locals 1

    const-class v0, Lh/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lh/g;

    return-object v0
.end method

.method public static values()[Lh/g;
    .locals 1

    sget-object v0, Lh/g;->d:[Lh/g;

    invoke-virtual {v0}, [Lh/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lh/g;

    return-object v0
.end method
