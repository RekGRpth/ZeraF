.class public Lh/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lh/j;


# direct methods
.method public constructor <init>(JJLh/g;)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lh/e;-><init>(JJLh/g;II)V

    return-void
.end method

.method public constructor <init>(JJLh/g;II)V
    .locals 7

    const/high16 v6, 0x10000

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh/j;

    new-instance v1, Lh/c;

    long-to-float v2, p1

    add-long v3, p1, p3

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lh/c;-><init>(F)V

    invoke-direct {v0, v1}, Lh/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lh/e;->a:Lh/j;

    iget-object v0, p0, Lh/e;->a:Lh/j;

    add-long v1, p1, p3

    invoke-virtual {v0, v1, v2}, Lh/j;->setDuration(J)V

    sget-object v0, Lh/f;->a:[I

    invoke-virtual {p5}, Lh/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, v5}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, v6}, Lh/j;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, v6}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, v5}, Lh/j;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, p6}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0, p7}, Lh/j;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(JLh/g;)V
    .locals 6

    const-wide/16 v1, 0x0

    move-object v0, p0

    move-wide v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lh/e;-><init>(JJLh/g;)V

    return-void
.end method


# virtual methods
.method public a(LD/a;)I
    .locals 3

    invoke-virtual {p1}, LD/a;->d()J

    move-result-wide v0

    iget-object v2, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v2}, Lh/j;->hasStarted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v2}, Lh/j;->start()V

    :cond_0
    iget-object v2, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v2, v0, v1}, Lh/j;->a(J)V

    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0}, Lh/j;->b()I

    move-result v0

    iget-object v1, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v1}, Lh/j;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, LD/a;->a()V

    :cond_1
    return v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lh/e;->a:Lh/j;

    invoke-virtual {v0}, Lh/j;->start()V

    return-void
.end method
