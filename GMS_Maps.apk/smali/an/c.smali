.class public Lan/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lam/d;


# static fields
.field private static final g:Landroid/graphics/Paint;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lan/c;->g:Landroid/graphics/Paint;

    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method public constructor <init>(IZZIZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lan/c;->f:I

    iput p1, p0, Lan/c;->a:I

    iput-boolean p2, p0, Lan/c;->b:Z

    iput-boolean p3, p0, Lan/c;->c:Z

    iput-boolean p5, p0, Lan/c;->d:Z

    iput p4, p0, Lan/c;->f:I

    iput-boolean p6, p0, Lan/c;->e:Z

    return-void
.end method

.method private static a(Lam/e;)Landroid/graphics/Canvas;
    .locals 1

    check-cast p0, Lan/e;

    invoke-virtual {p0}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    return-object v0
.end method

.method private e()Landroid/graphics/Paint;
    .locals 3

    iget-boolean v0, p0, Lan/c;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lan/c;->e:Z

    if-eqz v0, :cond_1

    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    iget v1, p0, Lan/c;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    :goto_0
    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lan/c;->b:Z

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lan/c;->c:Z

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lan/c;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    return-object v0

    :cond_1
    sget-object v0, Lan/c;->g:Landroid/graphics/Paint;

    iget v1, p0, Lan/c;->f:I

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v0, v1, v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public a(C)I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v4, [F

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v1

    new-array v2, v4, [C

    aput-char p1, v2, v3

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    aget v0, v0, v3

    float-to-int v0, v0

    return v0
.end method

.method public a(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lan/c;->a(Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;II)I
    .locals 2

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v0

    add-int v1, p2, p3

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public a(Lam/e;Ljava/lang/String;II)V
    .locals 4

    invoke-static {p1}, Lan/c;->a(Lam/e;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, p3

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int v2, p4, v2

    int-to-float v2, v2

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lan/c;->f:I

    return v0
.end method

.method public c()I
    .locals 1

    invoke-direct {p0}, Lan/c;->e()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    neg-int v0, v0

    return v0
.end method

.method public d()I
    .locals 2

    iget v0, p0, Lan/c;->a:I

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0xc

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    return v0
.end method
