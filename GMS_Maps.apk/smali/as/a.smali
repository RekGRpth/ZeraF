.class public abstract Las/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Las/a;


# instance fields
.field private b:I

.field protected c:Las/c;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/util/Vector;

.field private f:I

.field private g:Ljava/lang/Object;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Las/a;

    sput-object v0, Las/a;->a:[Las/a;

    return-void
.end method

.method public constructor <init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Las/a;->g:Ljava/lang/Object;

    iput-object p1, p0, Las/a;->c:Las/c;

    iput-object p2, p0, Las/a;->d:Ljava/lang/Runnable;

    iput-object p3, p0, Las/a;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Las/a;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Las/a;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method protected a(I)V
    .locals 0

    iput p1, p0, Las/a;->b:I

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Las/a;->c:Las/c;

    invoke-virtual {v0, p0}, Las/c;->b(Las/a;)I

    move-result v0

    return v0
.end method

.method abstract d()I
.end method

.method protected e()[Las/a;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Las/a;->e:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Las/a;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Las/a;

    iget-object v1, p0, Las/a;->e:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    sget-object v0, Las/a;->a:[Las/a;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected f()I
    .locals 1

    iget v0, p0, Las/a;->b:I

    return v0
.end method

.method public g()V
    .locals 2

    iget-object v1, p0, Las/a;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Las/a;->f:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Las/a;->c:Las/c;

    invoke-virtual {v0, p0}, Las/c;->a(Las/a;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method abstract h()V
.end method

.method i()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Las/a;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Las/a;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget v0, p0, Las/a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Las/a;->f:I

    iget-object v0, p0, Las/a;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Las/a;->e()[Las/a;

    move-result-object v1

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Las/a;->g()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    return-void
.end method
