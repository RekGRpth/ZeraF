.class public abstract Law/b;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/concurrent/CountDownLatch;

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    return-void
.end method


# virtual methods
.method public d_()V
    .locals 1

    iget-object v0, p0, Law/b;->a:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Law/b;->k()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Law/b;->b:Ljava/lang/Object;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Law/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract k()Ljava/lang/Object;
.end method

.method protected l()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public m()Ljava/lang/Object;
    .locals 4

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Law/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    :try_start_0
    iget-object v0, p0, Law/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p0}, Law/b;->l()I

    move-result v1

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/b;->b:Ljava/lang/Object;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    iget-object v0, p0, Law/b;->b:Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
