.class public Lo/K;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/X;

.field private final c:[Lo/H;

.field private final d:Ljava/lang/String;

.field private final e:Lo/aj;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:F

.field private final j:I

.field private final k:Z

.field private final l:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V
    .locals 12

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V

    return-void
.end method

.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/K;->a:Lo/o;

    iput-object p2, p0, Lo/K;->b:Lo/X;

    iput-object p3, p0, Lo/K;->c:[Lo/H;

    const/4 v0, 0x0

    iput-object v0, p0, Lo/K;->d:Ljava/lang/String;

    iput-object p4, p0, Lo/K;->e:Lo/aj;

    iput p5, p0, Lo/K;->f:I

    iput-object p6, p0, Lo/K;->g:Ljava/lang/String;

    iput p7, p0, Lo/K;->h:I

    iput p8, p0, Lo/K;->i:F

    iput p9, p0, Lo/K;->j:I

    iput-object p10, p0, Lo/K;->l:[I

    iput-boolean p11, p0, Lo/K;->k:Z

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/K;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/K;->a(Ljava/io/DataInput;Lo/as;Z)Lo/K;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/io/DataInput;Lo/as;Z)Lo/K;
    .locals 11

    const/4 v0, 0x0

    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v1

    invoke-static {p0, v1}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v2

    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v3, v4, [Lo/H;

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-static {p0, p1, v6}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v5

    aput-object v5, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    invoke-static {v1}, Lo/O;->c(I)F

    move-result v8

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v9

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-static {v4, v9}, Lo/O;->a(II)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v10, v4, [I

    :goto_2
    if-ge v0, v4, :cond_3

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v10, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x2

    invoke-static {v4, v9}, Lo/O;->a(II)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    new-instance v0, Lo/L;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/L;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V

    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Lo/K;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V

    goto :goto_3
.end method


# virtual methods
.method public a(I)Lo/H;
    .locals 1

    iget-object v0, p0, Lo/K;->c:[Lo/H;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lo/o;
    .locals 1

    iget-object v0, p0, Lo/K;->a:Lo/o;

    return-object v0
.end method

.method public b()Lo/X;
    .locals 1

    iget-object v0, p0, Lo/K;->b:Lo/X;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lo/K;->c:[Lo/H;

    array-length v0, v0

    return v0
.end method

.method public d()F
    .locals 1

    iget v0, p0, Lo/K;->i:F

    return v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/K;->e:Lo/aj;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lo/K;->k:Z

    return v0
.end method

.method public h()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/K;->h:I

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/K;->l:[I

    return-object v0
.end method

.method public m()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lo/K;->b:Lo/X;

    invoke-virtual {v1}, Lo/X;->h()I

    move-result v3

    iget-object v1, p0, Lo/K;->c:[Lo/H;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lo/K;->c:[Lo/H;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lo/H;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lo/K;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    add-int/lit8 v1, v1, 0x38

    iget-object v2, p0, Lo/K;->g:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/K;->e:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method
