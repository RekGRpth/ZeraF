.class Lo/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method constructor <init>(Lo/T;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lo/T;->a:I

    iput v0, p0, Lo/ab;->a:I

    iget v0, p1, Lo/T;->b:I

    iput v0, p0, Lo/ab;->b:I

    iget v0, p0, Lo/ab;->a:I

    iput v0, p0, Lo/ab;->c:I

    iget v0, p0, Lo/ab;->b:I

    iput v0, p0, Lo/ab;->d:I

    return-void
.end method

.method constructor <init>(Lo/ab;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lo/ab;->a:I

    iput v0, p0, Lo/ab;->a:I

    iget v0, p1, Lo/ab;->b:I

    iput v0, p0, Lo/ab;->b:I

    iget v0, p1, Lo/ab;->c:I

    iput v0, p0, Lo/ab;->c:I

    iget v0, p1, Lo/ab;->d:I

    iput v0, p0, Lo/ab;->d:I

    return-void
.end method


# virtual methods
.method a(II)V
    .locals 1

    iget v0, p0, Lo/ab;->a:I

    if-ge p1, v0, :cond_0

    iput p1, p0, Lo/ab;->a:I

    :cond_0
    iget v0, p0, Lo/ab;->b:I

    if-ge p2, v0, :cond_1

    iput p2, p0, Lo/ab;->b:I

    :cond_1
    iget v0, p0, Lo/ab;->c:I

    if-le p1, v0, :cond_2

    iput p1, p0, Lo/ab;->c:I

    :cond_2
    iget v0, p0, Lo/ab;->d:I

    if-le p2, v0, :cond_3

    iput p2, p0, Lo/ab;->d:I

    :cond_3
    return-void
.end method

.method a(Lo/T;)V
    .locals 2

    iget v0, p1, Lo/T;->a:I

    iget v1, p1, Lo/T;->b:I

    invoke-virtual {p0, v0, v1}, Lo/ab;->a(II)V

    return-void
.end method

.method a(Lo/ab;)V
    .locals 2

    iget v0, p1, Lo/ab;->a:I

    iget v1, p1, Lo/ab;->b:I

    invoke-virtual {p0, v0, v1}, Lo/ab;->a(II)V

    iget v0, p1, Lo/ab;->c:I

    iget v1, p1, Lo/ab;->d:I

    invoke-virtual {p0, v0, v1}, Lo/ab;->a(II)V

    return-void
.end method

.method a(Lo/ad;)Z
    .locals 4

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    iget v2, p0, Lo/ab;->a:I

    iget v3, v0, Lo/T;->a:I

    if-gt v2, v3, :cond_0

    iget v2, p0, Lo/ab;->b:I

    iget v0, v0, Lo/T;->b:I

    if-gt v2, v0, :cond_0

    iget v0, p0, Lo/ab;->c:I

    iget v2, v1, Lo/T;->a:I

    if-lt v0, v2, :cond_0

    iget v0, p0, Lo/ab;->d:I

    iget v1, v1, Lo/T;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/ab;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/ab;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "),("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/ab;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/ab;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
