.class public Lo/J;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:J

.field private final e:[Landroid/graphics/Bitmap;

.field private final f:I

.field private final g:F

.field private final h:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lo/J;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo/J;->b:Ljava/lang/String;

    invoke-static {p1}, Lo/J;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo/J;->c:[Ljava/lang/String;

    invoke-static {p1}, Lo/J;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lo/J;->e:[Landroid/graphics/Bitmap;

    const/16 v0, 0x15

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lo/J;->f:I

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lo/J;->g:F

    const/16 v1, 0x3e

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lo/J;->h:F

    :goto_0
    const/16 v0, 0x8

    invoke-static {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lo/J;->d:J

    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lo/J;->g:F

    const/high16 v0, 0x41f00000

    iput v0, p0, Lo/J;->h:F

    goto :goto_0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Ljava/lang/String;
    .locals 7

    const/16 v6, 0x12

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-nez v2, :cond_1

    sget-object v0, Lo/J;->a:[Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    mul-int/lit8 v0, v2, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    mul-int/lit8 v4, v1, 0x2

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Landroid/graphics/Bitmap;
    .locals 7

    const/16 v6, 0x11

    const/4 v2, 0x0

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_0

    new-array v0, v3, [Landroid/graphics/Bitmap;

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v4

    array-length v5, v4

    invoke-static {v4, v2, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lo/J;->f:I

    return v0
.end method

.method public a(F)Z
    .locals 1

    iget v0, p0, Lo/J;->g:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lo/J;->h:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lo/J;->e:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/J;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/J;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 4

    iget-wide v0, p0, Lo/J;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lo/J;->d:J

    return-wide v0
.end method
