.class public final Lo/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static b:Lo/aj;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:[I

.field private final f:[Lo/ai;

.field private final g:Lo/ao;

.field private final h:Lo/an;

.field private final i:Lo/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lo/aj;->a:[I

    new-instance v0, Lo/aj;

    sget-object v3, Lo/aj;->a:[I

    new-array v4, v1, [Lo/ai;

    invoke-static {}, Lo/ao;->a()Lo/ao;

    move-result-object v5

    invoke-static {}, Lo/an;->a()Lo/an;

    move-result-object v6

    invoke-static {}, Lo/ai;->a()Lo/ai;

    move-result-object v7

    move v2, v1

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, Lo/aj;->b:Lo/aj;

    return-void
.end method

.method public constructor <init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lo/aj;->c:I

    iput p2, p0, Lo/aj;->d:I

    iput-object p3, p0, Lo/aj;->e:[I

    iput-object p4, p0, Lo/aj;->f:[Lo/ai;

    iput-object p5, p0, Lo/aj;->g:Lo/ao;

    iput-object p6, p0, Lo/aj;->h:Lo/an;

    iput-object p7, p0, Lo/aj;->i:Lo/ai;

    return-void
.end method

.method public static a()Lo/aj;
    .locals 1

    sget-object v0, Lo/aj;->b:Lo/aj;

    return-object v0
.end method

.method public static a(ILjava/io/DataInput;I)Lo/aj;
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    invoke-static {v2}, Lo/aj;->c(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    new-array v3, v5, [I

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_1

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v6

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move-object v3, v0

    :cond_1
    invoke-static {v2}, Lo/aj;->d(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    new-array v4, v5, [Lo/ai;

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-static {p1, p2}, Lo/ai;->a(Ljava/io/DataInput;I)Lo/ai;

    move-result-object v6

    aput-object v6, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v4, v0

    :cond_3
    invoke-static {v2}, Lo/aj;->e(I)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p1, p2}, Lo/ao;->a(Ljava/io/DataInput;I)Lo/ao;

    move-result-object v5

    :goto_2
    invoke-static {v2}, Lo/aj;->f(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p1, p2}, Lo/an;->a(Ljava/io/DataInput;I)Lo/an;

    move-result-object v6

    :goto_3
    invoke-static {v2}, Lo/aj;->g(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1, p2}, Lo/ai;->a(Ljava/io/DataInput;I)Lo/ai;

    move-result-object v7

    :goto_4
    new-instance v0, Lo/aj;

    move v1, p0

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    return-object v0

    :cond_4
    move-object v7, v0

    goto :goto_4

    :cond_5
    move-object v6, v0

    goto :goto_3

    :cond_6
    move-object v5, v0

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_0

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    const-string v0, "["

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    array-length v3, p1

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget v4, p1, v2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const-string v5, ","

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    const-string v0, "]"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static d(I)Z
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static e(I)Z
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static f(I)Z
    .locals 1

    const/16 v0, 0x8

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static g(I)Z
    .locals 1

    const/16 v0, 0x10

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    iget-object v0, p0, Lo/aj;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    array-length v0, v0

    goto :goto_0
.end method

.method public b(I)Lo/ai;
    .locals 1

    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lo/aj;->e:[I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lo/aj;->e:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->d(I)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->e(I)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lo/aj;

    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    if-nez v2, :cond_4

    iget-object v2, p1, Lo/aj;->i:Lo/ai;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    iget-object v3, p1, Lo/aj;->i:Lo/ai;

    invoke-virtual {v2, v3}, Lo/ai;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lo/aj;->d:I

    iget v3, p1, Lo/aj;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lo/aj;->e:[I

    iget-object v3, p1, Lo/aj;->e:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lo/aj;->c:I

    iget v3, p1, Lo/aj;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    iget-object v3, p1, Lo/aj;->f:[Lo/ai;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lo/aj;->h:Lo/an;

    if-nez v2, :cond_a

    iget-object v2, p1, Lo/aj;->h:Lo/an;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lo/aj;->h:Lo/an;

    iget-object v3, p1, Lo/aj;->h:Lo/an;

    invoke-virtual {v2, v3}, Lo/an;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    if-nez v2, :cond_c

    iget-object v2, p1, Lo/aj;->g:Lo/ao;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    iget-object v3, p1, Lo/aj;->g:Lo/ao;

    invoke-virtual {v2, v3}, Lo/ao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->f(I)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->g(I)Z

    move-result v0

    return v0
.end method

.method public h()Lo/ao;
    .locals 1

    iget-object v0, p0, Lo/aj;->g:Lo/ao;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/aj;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->e:[I

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/aj;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lo/aj;->h:Lo/an;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    invoke-virtual {v0}, Lo/ai;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lo/aj;->h:Lo/an;

    invoke-virtual {v0}, Lo/an;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lo/aj;->g:Lo/ao;

    invoke-virtual {v1}, Lo/ao;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public i()Lo/an;
    .locals 1

    iget-object v0, p0, Lo/aj;->h:Lo/an;

    return-object v0
.end method

.method public j()Lo/ai;
    .locals 1

    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    return-object v0
.end method

.method public k()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lo/aj;->e:[I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    if-eqz v2, :cond_1

    iget-object v4, p0, Lo/aj;->f:[Lo/ai;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lo/ai;->h()I

    move-result v3

    add-int/2addr v3, v1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lo/aj;->e:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    invoke-static {v2}, Lo/O;->a(Lo/ai;)I

    move-result v2

    add-int/lit8 v0, v0, 0x38

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Style{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lo/aj;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "fillColors"

    iget-object v2, p0, Lo/aj;->e:[I

    invoke-static {v0, v2, v1}, Lo/aj;->a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V

    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", components="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lo/aj;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", strokes="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", textStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", textBoxStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->h:Lo/an;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", arrowStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
