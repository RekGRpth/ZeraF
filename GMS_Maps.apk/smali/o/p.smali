.class public Lo/p;
.super Lo/o;
.source "SourceFile"


# instance fields
.field protected final b:J

.field protected final c:J

.field protected final d:J


# direct methods
.method constructor <init>(J)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lo/p;-><init>(JJ)V

    return-void
.end method

.method constructor <init>(JJ)V
    .locals 2

    invoke-direct {p0}, Lo/o;-><init>()V

    invoke-static {p1, p2, p3, p4}, Lo/o;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lo/p;->b:J

    iput-wide p1, p0, Lo/p;->c:J

    iput-wide p3, p0, Lo/p;->d:J

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 4

    iget-wide v0, p0, Lo/p;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lo/p;->d:J

    invoke-static {v0, v1}, Lac/c;->a(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lo/p;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lo/p;

    if-eqz v1, :cond_0

    check-cast p1, Lo/p;

    iget-wide v1, p1, Lo/p;->c:J

    iget-wide v3, p0, Lo/p;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p1, Lo/p;->d:J

    iget-wide v3, p0, Lo/p;->d:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p1, Lo/p;->b:J

    iget-wide v3, p0, Lo/p;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lo/p;->c:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lo/p;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    instance-of v0, p1, Lo/q;

    if-eqz v0, :cond_1

    check-cast p1, Lo/q;

    iget-wide v0, p1, Lo/q;->b:J

    iget-wide v2, p0, Lo/p;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lo/p;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lo/p;->b:J

    iget-wide v2, p0, Lo/p;->b:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lo/p;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
