.class public final Lo/r;
.super Lo/p;
.source "SourceFile"


# direct methods
.method public constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lo/p;-><init>(JJ)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lo/r;
    .locals 6

    :try_start_0
    invoke-static {p0}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v2

    instance-of v1, v2, Lo/p;

    if-eqz v1, :cond_0

    new-instance v3, Lo/r;

    move-object v0, v2

    check-cast v0, Lo/p;

    move-object v1, v0

    iget-wide v4, v1, Lo/p;->c:J

    check-cast v2, Lo/p;

    iget-wide v1, v2, Lo/p;->d:J

    invoke-direct {v3, v4, v5, v1, v2}, Lo/r;-><init>(JJ)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    :goto_0
    return-object v1

    :catch_0
    move-exception v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public d()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lo/r;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lo/r;

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lo/r;->d:J

    check-cast p1, Lo/r;

    iget-wide v4, p1, Lo/r;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lo/D;

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lo/r;->d:J

    check-cast p1, Lo/D;

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v4

    iget-wide v4, v4, Lo/r;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lo/r;->d:J

    iget-wide v2, p0, Lo/r;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
