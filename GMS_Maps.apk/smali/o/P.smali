.class public Lo/P;
.super Lo/aL;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:Ljava/util/ArrayList;

.field private c:Ljava/util/HashSet;

.field private d:Ljava/util/ArrayList;

.field private e:Ljava/util/ArrayList;

.field private f:J


# direct methods
.method public constructor <init>(Lo/aL;)V
    .locals 18

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lo/aL;->d()Lo/aq;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lo/aL;->e()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lo/aL;->m()B

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lo/aL;->h()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Lo/aL;->p()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Lo/aL;->g()LA/c;

    move-result-object v12

    const/4 v13, 0x0

    const-wide/16 v14, -0x1

    invoke-virtual/range {p1 .. p1}, Lo/aL;->b()J

    move-result-wide v16

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v17}, Lo/aL;-><init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lo/P;->f:J

    invoke-virtual/range {p1 .. p1}, Lo/aL;->q()[Lo/n;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lo/P;->a:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->k()Lo/aO;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lo/aO;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-interface {v1}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lo/P;->d:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lo/P;->d:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lo/P;->e:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lo/P;->e:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lo/aL;->a()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lo/P;->f:J

    return-void
.end method

.method static synthetic a(Lo/P;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lo/P;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static a(Lo/aL;Lo/aL;)Lo/aL;
    .locals 6

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lo/aL;->a()J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    invoke-virtual {p1}, Lo/aL;->a()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_5

    invoke-virtual {p1}, Lo/aL;->a()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_5

    :cond_0
    invoke-virtual {p1}, Lo/aL;->a()J

    move-result-wide v0

    move-wide v1, v0

    :goto_0
    invoke-virtual {p1}, Lo/aL;->r()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lo/aL;->a()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    :goto_1
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lo/aL;->r()I

    move-result v0

    if-lez v0, :cond_3

    instance-of v0, p0, Lo/P;

    if-eqz v0, :cond_2

    check-cast p0, Lo/P;

    :goto_2
    invoke-virtual {p0, p1}, Lo/P;->a(Lo/aL;)V

    invoke-direct {p0, v1, v2}, Lo/P;->a(J)V

    goto :goto_1

    :cond_2
    new-instance v0, Lo/P;

    invoke-direct {v0, p0}, Lo/P;-><init>(Lo/aL;)V

    move-object p0, v0

    goto :goto_2

    :cond_3
    instance-of v0, p0, Lo/P;

    if-eqz v0, :cond_4

    move-object v0, p0

    check-cast v0, Lo/P;

    invoke-direct {v0, v1, v2}, Lo/P;->a(J)V

    goto :goto_1

    :cond_4
    new-instance v0, Lo/aN;

    invoke-direct {v0}, Lo/aN;-><init>()V

    invoke-virtual {p0}, Lo/aL;->d()Lo/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lo/aN;->a(Lo/aq;)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->e()I

    move-result v3

    invoke-virtual {v0, v3}, Lo/aN;->b(I)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->h()I

    move-result v3

    invoke-virtual {v0, v3}, Lo/aN;->a(I)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lo/aN;->a([Ljava/lang/String;)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lo/aN;->b([Ljava/lang/String;)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->p()I

    move-result v3

    invoke-virtual {v0, v3}, Lo/aN;->c(I)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->q()[Lo/n;

    move-result-object v3

    invoke-virtual {v0, v3}, Lo/aN;->a([Lo/n;)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->g()LA/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lo/aN;->a(LA/c;)Lo/aN;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lo/aN;->a(J)Lo/aN;

    move-result-object v0

    invoke-virtual {p0}, Lo/aL;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lo/aN;->b(J)Lo/aN;

    move-result-object v0

    invoke-virtual {v0}, Lo/aN;->a()Lo/aL;

    move-result-object p0

    goto :goto_1

    :cond_5
    move-wide v1, v0

    goto/16 :goto_0
.end method

.method private a(J)V
    .locals 0

    iput-wide p1, p0, Lo/P;->f:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lo/P;->f:J

    return-wide v0
.end method

.method public a(I)Lo/n;
    .locals 1

    iget-object v0, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    return-object v0
.end method

.method public a(Lo/aL;)V
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lo/P;->c:Ljava/util/HashSet;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lo/aL;->r()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Lo/aL;->b(I)Lo/aI;

    move-result-object v0

    instance-of v4, v0, Lo/aK;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lo/P;->c:Ljava/util/HashSet;

    check-cast v0, Lo/aK;

    invoke-virtual {v0}, Lo/aK;->a()Lo/o;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    check-cast v0, Lo/aJ;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    iget-object v4, p0, Lo/P;->c:Ljava/util/HashSet;

    invoke-interface {v0}, Lo/n;->a()Lo/o;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aJ;

    move v1, v2

    :goto_4
    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v4

    invoke-interface {v4}, Lo/n;->l()[I

    move-result-object v4

    array-length v4, v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v4

    invoke-interface {v4}, Lo/n;->l()[I

    move-result-object v4

    aget v5, v4, v1

    iget-object v6, p0, Lo/P;->d:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v5, v6

    aput v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    invoke-virtual {v0}, Lo/aJ;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lo/aJ;->c()I

    move-result v1

    iget-object v4, p0, Lo/P;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    iget-object v1, p0, Lo/P;->a:Ljava/util/List;

    invoke-virtual {v0}, Lo/aJ;->c()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/n;

    iget-object v4, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_6

    invoke-virtual {v0}, Lo/aJ;->d()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3

    :cond_5
    iget-object v4, p0, Lo/P;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    invoke-virtual {v0}, Lo/aJ;->c()I

    move-result v1

    iget-object v4, p0, Lo/P;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_8

    const-string v1, "MutableVectorTile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid plane index on tile "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lo/aL;->g()LA/c;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lo/aJ;->a()Lo/n;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_9
    invoke-virtual {p1}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v0

    :goto_5
    array-length v1, v0

    if-ge v2, v1, :cond_b

    iget-object v1, p0, Lo/P;->e:Ljava/util/ArrayList;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lo/P;->e:Ljava/util/ArrayList;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lo/P;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, Lo/P;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lo/P;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo/P;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lo/P;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public f()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo/P;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lo/P;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, Lo/P;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public k()Lo/aO;
    .locals 2

    new-instance v0, Lo/R;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lo/R;-><init>(Lo/P;Lo/Q;)V

    return-object v0
.end method

.method public l()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lo/P;->c:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
