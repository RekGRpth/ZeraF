.class public LbH/l;
.super Ljava/io/IOException;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = -0x166db9773d0dffacL


# instance fields
.field private a:LbH/p;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, LbH/l;->a:LbH/p;

    return-void
.end method

.method static b()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static c()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "CodedInputStream encountered an embedded string or message which claimed to have negative size."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static d()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "CodedInputStream encountered a malformed varint."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static e()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "Protocol message contained an invalid tag (zero)."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static f()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "Protocol message end-group tag did not match expected tag."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static g()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "Protocol message tag had invalid wire type."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static h()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static i()LbH/l;
    .locals 2

    new-instance v0, LbH/l;

    const-string v1, "Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit."

    invoke-direct {v0, v1}, LbH/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(LbH/p;)LbH/l;
    .locals 0

    iput-object p1, p0, LbH/l;->a:LbH/p;

    return-object p0
.end method

.method public a()LbH/p;
    .locals 1

    iget-object v0, p0, LbH/l;->a:LbH/p;

    return-object v0
.end method
