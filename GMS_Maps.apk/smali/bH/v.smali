.class LbH/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LbH/e;


# instance fields
.field a:I

.field final synthetic b:LbH/s;

.field private final c:LbH/u;

.field private d:LbH/e;


# direct methods
.method private constructor <init>(LbH/s;)V
    .locals 2

    iput-object p1, p0, LbH/v;->b:LbH/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LbH/u;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LbH/u;-><init>(LbH/d;LbH/t;)V

    iput-object v0, p0, LbH/v;->c:LbH/u;

    iget-object v0, p0, LbH/v;->c:LbH/u;

    invoke-virtual {v0}, LbH/u;->a()LbH/m;

    move-result-object v0

    invoke-virtual {v0}, LbH/m;->a()LbH/e;

    move-result-object v0

    iput-object v0, p0, LbH/v;->d:LbH/e;

    invoke-virtual {p1}, LbH/s;->b()I

    move-result v0

    iput v0, p0, LbH/v;->a:I

    return-void
.end method

.method synthetic constructor <init>(LbH/s;LbH/t;)V
    .locals 0

    invoke-direct {p0, p1}, LbH/v;-><init>(LbH/s;)V

    return-void
.end method


# virtual methods
.method public a()B
    .locals 1

    iget-object v0, p0, LbH/v;->d:LbH/e;

    invoke-interface {v0}, LbH/e;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LbH/v;->c:LbH/u;

    invoke-virtual {v0}, LbH/u;->a()LbH/m;

    move-result-object v0

    invoke-virtual {v0}, LbH/m;->a()LbH/e;

    move-result-object v0

    iput-object v0, p0, LbH/v;->d:LbH/e;

    :cond_0
    iget v0, p0, LbH/v;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LbH/v;->a:I

    iget-object v0, p0, LbH/v;->d:LbH/e;

    invoke-interface {v0}, LbH/e;->a()B

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/Byte;
    .locals 1

    invoke-virtual {p0}, LbH/v;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget v0, p0, LbH/v;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LbH/v;->b()Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
