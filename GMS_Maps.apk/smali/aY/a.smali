.class public LaY/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:J

.field private b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:Z

.field private d:I

.field private e:LaY/b;


# direct methods
.method public constructor <init>(JLcom/google/googlenav/common/io/protocol/ProtoBuf;LaY/b;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    iput-wide p1, p0, LaY/a;->a:J

    iput-object p3, p0, LaY/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaY/a;->c:Z

    iput-object p4, p0, LaY/a;->e:LaY/b;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x5

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fs;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-wide v2, p0, LaY/a;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LaY/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, LaY/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_0

    iput-boolean v0, p0, LaY/a;->c:Z

    const/4 v0, -0x1

    iput v0, p0, LaY/a;->d:I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaY/a;->d:I

    iget v2, p0, LaY/a;->d:I

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, LaY/a;->c:Z

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x61

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 3

    iget-boolean v0, p0, LaY/a;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "s"

    :goto_0
    const/16 v1, 0x55

    const-string v2, "dr"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LaY/a;->e:LaY/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/a;->e:LaY/b;

    iget-boolean v1, p0, LaY/a;->c:Z

    invoke-interface {v0, v1}, LaY/b;->a(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "e"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, LaY/a;->d:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LaY/a;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public u_()V
    .locals 1

    iget-object v0, p0, LaY/a;->e:LaY/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaY/a;->e:LaY/b;

    invoke-interface {v0}, LaY/b;->a()V

    :cond_0
    return-void
.end method
