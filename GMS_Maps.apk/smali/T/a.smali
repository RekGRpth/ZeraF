.class public LT/a;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lax/w;

.field private final b:LaS/a;

.field private final c:Lbi/d;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;)V
    .locals 1

    invoke-static {}, LT/a;->b()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-static {}, Lcom/google/android/maps/rideabout/app/q;->z()LaS/a;

    move-result-object v0

    iput-object v0, p0, LT/a;->b:LaS/a;

    iget-object v0, p0, LT/a;->b:LaS/a;

    invoke-virtual {v0}, LaS/a;->o()Lax/w;

    move-result-object v0

    iput-object v0, p0, LT/a;->a:Lax/w;

    iget-object v0, p0, LT/a;->b:LaS/a;

    invoke-virtual {v0}, LaS/a;->p()Lbi/d;

    move-result-object v0

    iput-object v0, p0, LT/a;->c:Lbi/d;

    return-void
.end method

.method private a(I)V
    .locals 4

    const/4 v3, -0x1

    invoke-virtual {p0}, LT/a;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    invoke-virtual {p0}, LT/a;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/bF;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v1

    new-instance v2, LT/b;

    invoke-direct {v2, p0}, LT/b;-><init>(LT/a;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/rideabout/view/j;->setActionBarCustomHeader(Landroid/widget/ViewSwitcher;Lcom/google/android/maps/rideabout/view/l;)V

    return-void
.end method

.method public static b()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0136

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method


# virtual methods
.method protected O_()V
    .locals 1

    invoke-virtual {p0}, LT/a;->P_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LT/a;->requestWindowFeature(I)Z

    :cond_1
    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const v0, 0x7f0401b6

    invoke-direct {p0, v0}, LT/a;->a(I)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, LT/a;->h:Landroid/view/View;

    check-cast v0, Lcom/google/android/maps/rideabout/view/NavigationView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/rideabout/view/NavigationView;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 5

    const/16 v4, 0x61

    const/4 v1, -0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    iget-object v2, p0, LT/a;->f:Lcom/google/googlenav/ui/e;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    packed-switch v0, :pswitch_data_1

    :cond_0
    :goto_1
    :pswitch_0
    return v1

    :pswitch_1
    const/16 v0, 0xb55

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb58

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xb56

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xb57

    goto :goto_0

    :pswitch_5
    const-string v0, "e"

    const-string v2, "m"

    invoke-static {v4, v0, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    const-string v0, "f"

    const-string v2, "m"

    invoke-static {v4, v0, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f1004d2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xb55
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected c()Landroid/view/View;
    .locals 7

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/ui/view/android/bF;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/maps/rideabout/view/NavigationView;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/maps/rideabout/view/NavigationView;

    invoke-virtual {p0}, LT/a;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LT/a;->a:Lax/w;

    iget-object v3, p0, LT/a;->c:Lbi/d;

    iget-object v4, p0, LT/a;->f:Lcom/google/googlenav/ui/e;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/rideabout/view/NavigationView;-><init>(Landroid/content/Context;Lax/w;Lbi/d;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/m;)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/j;)V

    invoke-virtual {v6}, Lcom/google/googlenav/ui/view/android/bF;->b()V

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    invoke-virtual {p0}, LT/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, LT/a;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LT/a;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110022

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004d2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x49e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1004d3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x4b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, p1}, LaS/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const/16 v0, 0x61

    const-string v1, "e"

    const-string v2, "b"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
