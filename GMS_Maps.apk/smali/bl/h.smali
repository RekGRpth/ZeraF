.class public Lbl/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private h:Lbl/g;

.field private i:Lbl/f;

.field private j:Lbl/i;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lbl/h;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbl/h;->f:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbl/h;->g:Ljava/util/List;

    sget-object v0, Lbl/i;->a:Lbl/i;

    iput-object v0, p0, Lbl/h;->j:Lbl/i;

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/h;
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbl/h;

    invoke-direct {v0}, Lbl/h;-><init>()V

    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v2

    iput v2, v0, Lbl/h;->c:I

    sget-object v2, Lbl/i;->a:Lbl/i;

    iput-object v2, v0, Lbl/h;->j:Lbl/i;

    const/16 v2, 0x8

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v2

    if-ne v2, v3, :cond_1

    sget-object v2, Lbl/i;->b:Lbl/i;

    iput-object v2, v0, Lbl/h;->j:Lbl/i;

    :cond_1
    iget-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "uddq_experiment"

    iput-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    :cond_2
    const/4 v2, 0x7

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    iget-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "Thank you for your feedback."

    iput-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lbl/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/c;

    move-result-object v4

    iget-object v5, v0, Lbl/h;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_6

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lbl/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/d;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v4, v0, Lbl/h;->g:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    const/16 v1, 0x9

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->k:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->l:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->m:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lbl/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/h;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->n:Lbl/h;

    const/16 v1, 0xd

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->o:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method static synthetic a(Lbl/h;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lbl/f;)V
    .locals 0

    iput-object p1, p0, Lbl/h;->i:Lbl/f;

    return-void
.end method

.method public a(Lbl/g;)V
    .locals 0

    iput-object p1, p0, Lbl/h;->h:Lbl/g;

    return-void
.end method

.method public a(Lbl/e;)Z
    .locals 2

    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    iget-object v0, v0, Lbl/d;->a:Lbl/e;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lbl/h;->f:Ljava/util/List;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lbl/h;->h:Lbl/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lbl/g;
    .locals 1

    iget-object v0, p0, Lbl/h;->h:Lbl/g;

    return-object v0
.end method

.method public i()Z
    .locals 2

    sget-object v0, Lbl/i;->b:Lbl/i;

    iget-object v1, p0, Lbl/h;->j:Lbl/i;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lbl/h;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lbl/h;
    .locals 1

    iget-object v0, p0, Lbl/h;->n:Lbl/h;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbl/h;->o:Ljava/lang/String;

    return-object v0
.end method
