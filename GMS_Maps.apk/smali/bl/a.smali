.class public Lbl/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lbl/h;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lbl/h;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbl/h;->h()Lbl/g;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lbl/g;->a(J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbl/h;)Ljava/lang/String;
    .locals 2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lbl/h;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lbl/h;->o()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    const-string v1, "phone"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbl/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "homepage"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "location"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "direction"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string v1, "ap"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbl/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LaN/B;Lbl/h;Lcom/google/googlenav/ai;)V
    .locals 8

    const/4 v3, 0x0

    const-string v1, "el"

    invoke-virtual {p1}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, p2, p1}, Lbl/a;->a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbl/h;)Ljava/lang/String;

    move-result-object v7

    move-object v4, v3

    invoke-static/range {v0 .. v7}, Lbl/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lbl/h;Lbl/c;Lcom/google/googlenav/ai;)V
    .locals 8

    invoke-virtual {p0}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lbl/a;->a(Lbl/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lbl/c;->e()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2, p0}, Lbl/a;->a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbl/h;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lbl/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lbl/h;Lcom/google/googlenav/ai;)V
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cancel"

    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1, p0}, Lbl/a;->a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbl/h;)Ljava/lang/String;

    move-result-object v7

    move-object v4, v3

    invoke-static/range {v0 .. v7}, Lbl/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lbl/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lbl/h;->o()Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, p2

    invoke-static/range {v0 .. v7}, Lbl/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ai;Lbl/h;Z)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0, p1, p2}, Lbl/a;->a(Ljava/lang/String;Lbl/h;Z)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lbl/h;Lcom/google/googlenav/ai;)V
    .locals 8

    const-string v1, "apc"

    invoke-virtual {p1}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lbl/a;->a(Lbl/h;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, p2, p1}, Lbl/a;->a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbl/h;)Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    invoke-static/range {v0 .. v7}, Lbl/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lbl/h;Z)V
    .locals 8

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/16 v3, 0x54

    const-string v4, "ac"

    const/4 v2, 0x4

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "uf"

    aput-object v6, v5, v2

    const/4 v6, 0x1

    if-eqz p0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cid="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    aput-object v2, v5, v6

    const/4 v2, 0x2

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "qn="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    aput-object v1, v5, v2

    const/4 v1, 0x3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "qas="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_2

    const-string v0, "oninit"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    move-object v2, v1

    goto :goto_1

    :cond_2
    const-string v0, "useraction"

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x0

    const/16 v2, 0x6a

    const-string v3, "go"

    const/16 v0, 0x9

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "oi=uddq_experiment"

    aput-object v5, v4, v0

    const/4 v0, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "experimentId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "qn="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "value="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x5

    if-nez p7, :cond_0

    move-object v0, v1

    :goto_0
    aput-object v0, v4, v5

    const/4 v5, 0x6

    if-nez p3, :cond_1

    move-object v0, v1

    :goto_1
    aput-object v0, v4, v5

    const/4 v5, 0x7

    if-nez p5, :cond_2

    move-object v0, v1

    :goto_2
    aput-object v0, v4, v5

    const/16 v0, 0x8

    if-nez p4, :cond_3

    :goto_3
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "qvalue="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ph="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rli="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
