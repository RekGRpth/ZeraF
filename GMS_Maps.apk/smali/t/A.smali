.class Lt/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt/s;


# instance fields
.field private a:LA/c;

.field private b:Ljava/util/Queue;

.field private c:Ljava/util/Map;

.field private d:Lt/g;


# direct methods
.method public constructor <init>(LA/c;Lt/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt/A;->a:LA/c;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lt/A;->c:Ljava/util/Map;

    iput-object p2, p0, Lt/A;->d:Lt/g;

    return-void
.end method

.method static synthetic a(Lt/A;)LA/c;
    .locals 1

    iget-object v0, p0, Lt/A;->a:LA/c;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/C;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lt/C;-><init>(Lt/A;Lt/z;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/E;

    invoke-direct {v1, p0, p1}, Lt/E;-><init>(Lt/A;I)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(J)V
    .locals 3

    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/H;

    invoke-direct {v2, p0, v0}, Lt/H;-><init>(Lt/A;Lo/aq;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(JI)V
    .locals 3

    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v1

    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/F;

    invoke-direct {v2, p0, v0, p3}, Lt/F;-><init>(Lt/A;Lo/ap;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lo/ap;)V
    .locals 2

    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/B;

    iget-object v1, p0, Lt/A;->d:Lt/g;

    invoke-interface {v0, v1}, Lt/B;->a(Lt/g;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "SDCardTileCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tiles were not inserted into the disk cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    return-void
.end method

.method public b(JI)V
    .locals 3

    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/G;

    invoke-direct {v2, p0, v0, p3}, Lt/G;-><init>(Lt/A;Lo/aq;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/D;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lt/D;-><init>(Lt/A;Lt/z;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method
