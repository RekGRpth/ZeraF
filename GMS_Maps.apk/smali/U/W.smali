.class public LU/W;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/F;


# instance fields
.field private final a:Lbi/d;

.field private final b:LU/Y;

.field private c:Lbi/a;

.field private d:J


# direct methods
.method constructor <init>(Lbi/d;LU/Y;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LU/W;->a:Lbi/d;

    iput-object p2, p0, LU/W;->b:LU/Y;

    return-void
.end method

.method static a()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->w:I

    int-to-long v0, v0

    return-wide v0
.end method

.method private b()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LU/W;->b:LU/Y;

    invoke-virtual {v2}, LU/Y;->b()LU/z;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, LU/W;->b:LU/Y;

    invoke-virtual {v2}, LU/Y;->a()LU/z;

    move-result-object v2

    iget-object v2, v2, LU/z;->b:LU/A;

    sget-object v3, LU/A;->b:LU/A;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LU/W;->b:LU/Y;

    invoke-virtual {v2}, LU/Y;->b()LU/z;

    move-result-object v2

    iget-object v3, p0, LU/W;->a:Lbi/d;

    iget-object v2, v2, LU/z;->c:Lbi/a;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v3, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    sget-object v3, LU/X;->a:[I

    invoke-virtual {v2}, Lbi/h;->b()Lbi/q;

    move-result-object v2

    invoke-virtual {v2}, Lbi/q;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, LU/W;->b:LU/Y;

    invoke-virtual {v2}, LU/Y;->b()LU/z;

    move-result-object v2

    iget-object v2, v2, LU/z;->c:Lbi/a;

    iget v2, v2, Lbi/a;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :pswitch_1
    move v1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(LU/R;)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, LU/R;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LU/W;->b:LU/Y;

    invoke-virtual {v0}, LU/Y;->b()LU/z;

    move-result-object v0

    iget-object v0, v0, LU/z;->c:Lbi/a;

    iget-object v2, p0, LU/W;->a:Lbi/d;

    iget-object v3, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v3}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    iget v3, v0, Lbi/a;->b:F

    const/high16 v4, 0x3f000000

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    const/high16 v3, 0x3f800000

    iget v0, v0, Lbi/a;->b:F

    sub-float v0, v3, v0

    invoke-virtual {v2}, Lbi/h;->r()F

    move-result v2

    mul-float/2addr v0, v2

    :goto_0
    invoke-virtual {p1}, LU/R;->getSpeed()F

    move-result v2

    const/high16 v3, 0x40a00000

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    const/high16 v2, 0x41f00000

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget v0, v0, Lbi/a;->b:F

    invoke-virtual {v2}, Lbi/h;->r()F

    move-result v2

    mul-float/2addr v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(LU/R;)LU/G;
    .locals 15

    invoke-virtual/range {p1 .. p1}, LU/R;->getTime()J

    move-result-wide v3

    invoke-static {}, LU/W;->a()J

    move-result-wide v5

    iget-object v0, p0, LU/W;->c:Lbi/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/W;->b:LU/Y;

    invoke-virtual {v0}, LU/Y;->b()LU/z;

    move-result-object v0

    iget-object v0, v0, LU/z;->a:LU/R;

    invoke-virtual {v0}, LU/R;->getTime()J

    move-result-wide v0

    iget-wide v7, p0, LU/W;->d:J

    cmp-long v0, v0, v7

    if-lez v0, :cond_2

    :cond_0
    invoke-direct {p0}, LU/W;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LU/W;->b:LU/Y;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, LU/Y;->a(J)V

    new-instance v0, LU/G;

    sget-object v1, LU/H;->a:LU/H;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LU/W;->b:LU/Y;

    invoke-virtual {v0}, LU/Y;->b()LU/z;

    move-result-object v0

    iget-object v2, v0, LU/z;->c:Lbi/a;

    iget-object v0, p0, LU/W;->b:LU/Y;

    invoke-virtual {v0}, LU/Y;->b()LU/z;

    move-result-object v0

    iget-object v0, v0, LU/z;->a:LU/R;

    invoke-virtual {v0}, LU/R;->getTime()J

    move-result-wide v0

    iget-object v7, p0, LU/W;->b:LU/Y;

    invoke-virtual {v7, v3, v4}, LU/Y;->a(J)V

    :goto_1
    iget-object v7, p0, LU/W;->b:LU/Y;

    invoke-virtual {v7}, LU/Y;->e()J

    move-result-wide v7

    add-long/2addr v5, v7

    cmp-long v5, v5, v3

    if-gez v5, :cond_3

    new-instance v0, LU/G;

    sget-object v1, LU/H;->a:LU/H;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, LU/W;->c:Lbi/a;

    iget-wide v0, p0, LU/W;->d:J

    goto :goto_1

    :cond_3
    iget-object v5, p0, LU/W;->a:Lbi/d;

    iget-object v6, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v5, v6}, Lbi/d;->e(Lbi/t;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v0, LU/G;

    sget-object v1, LU/H;->a:LU/H;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    goto :goto_0

    :cond_4
    iget-object v5, p0, LU/W;->a:Lbi/d;

    iget-object v6, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v5, v6}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v5

    invoke-direct/range {p0 .. p1}, LU/W;->b(LU/R;)Z

    move-result v6

    invoke-virtual {v5}, Lbi/h;->p()Z

    move-result v7

    if-nez v7, :cond_5

    if-nez v6, :cond_5

    new-instance v0, LU/G;

    sget-object v1, LU/H;->a:LU/H;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v5}, Lbi/h;->m()I

    move-result v7

    int-to-long v7, v7

    const-wide/16 v9, 0x3e8

    invoke-virtual {v5}, Lbi/h;->q()I

    move-result v11

    int-to-long v11, v11

    sub-long/2addr v11, v7

    mul-long/2addr v9, v11

    iget v11, v2, Lbi/a;->b:F

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-nez v11, :cond_6

    iget-object v11, p0, LU/W;->b:LU/Y;

    invoke-virtual {v11}, LU/Y;->d()J

    move-result-wide v11

    sub-long v11, v3, v11

    const-wide/16 v13, 0x3e8

    mul-long/2addr v7, v13

    cmp-long v7, v11, v7

    if-gez v7, :cond_6

    const/4 v0, 0x0

    :goto_2
    iget v1, v2, Lbi/a;->b:F

    add-float/2addr v1, v0

    const/high16 v0, 0x3f800000

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_a

    iget-object v0, p0, LU/W;->a:Lbi/d;

    iget-object v1, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->g(Lbi/t;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, LU/G;

    sget-object v1, LU/H;->a:LU/H;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    goto/16 :goto_0

    :cond_6
    long-to-float v7, v9

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-lez v7, :cond_8

    sub-long v0, v3, v0

    if-eqz v6, :cond_7

    invoke-virtual/range {p1 .. p1}, LU/R;->getSpeed()F

    move-result v3

    long-to-float v0, v0

    mul-float/2addr v0, v3

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    invoke-virtual {v5}, Lbi/h;->r()F

    move-result v1

    div-float/2addr v0, v1

    goto :goto_2

    :cond_7
    long-to-float v0, v0

    long-to-float v1, v9

    div-float/2addr v0, v1

    goto :goto_2

    :cond_8
    const/high16 v0, 0x3f800000

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    new-instance v0, Lbi/v;

    iget-object v3, p0, LU/W;->a:Lbi/d;

    invoke-direct {v0, v3}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v0

    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    :goto_3
    new-instance v2, Lbi/a;

    invoke-direct {v2, v0, v1}, Lbi/a;-><init>(Lbi/t;F)V

    iput-object v2, p0, LU/W;->c:Lbi/a;

    invoke-virtual/range {p1 .. p1}, LU/R;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, LU/W;->d:J

    new-instance v0, LU/G;

    sget-object v1, LU/H;->d:LU/H;

    iget-object v2, p0, LU/W;->c:Lbi/a;

    invoke-direct {v0, v1, v2}, LU/G;-><init>(LU/H;Lbi/a;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, v2, Lbi/a;->a:Lbi/t;

    goto :goto_3
.end method
