.class public final LU/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/u;


# static fields
.field static final a:Lcom/google/common/collect/ImmutableSet;


# instance fields
.field private final b:LU/v;

.field private final c:Lcom/google/common/collect/ImmutableSet;

.field private d:LV/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "gps"

    const-string v1, "network"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, LU/w;->a:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;LU/l;)LU/b;
    .locals 2

    const-string v0, "location_recorder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LV/c;

    iget-object v1, p0, LU/w;->d:LV/m;

    invoke-direct {v0, v1}, LV/c;-><init>(LV/m;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1, p2}, LU/v;->a(Ljava/lang/String;LU/l;)LU/b;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/os/Handler$Callback;)LX/i;
    .locals 1

    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1}, LU/v;->a(Landroid/os/Handler$Callback;)LX/i;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, LU/w;->c:Lcom/google/common/collect/ImmutableSet;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    const-string v0, "location_recorder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LU/w;->a:Lcom/google/common/collect/ImmutableSet;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1}, LU/v;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method
