.class public final enum LU/S;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/S;

.field public static final enum b:LU/S;

.field public static final enum c:LU/S;

.field private static final synthetic e:[LU/S;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, LU/S;

    const-string v1, "GPS"

    const-string v2, "gps"

    invoke-direct {v0, v1, v3, v2}, LU/S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LU/S;->a:LU/S;

    new-instance v0, LU/S;

    const-string v1, "NETWORK"

    const-string v2, "network"

    invoke-direct {v0, v1, v4, v2}, LU/S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LU/S;->b:LU/S;

    new-instance v0, LU/S;

    const-string v1, "TIMER"

    const-string v2, "speed_provider"

    invoke-direct {v0, v1, v5, v2}, LU/S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LU/S;->c:LU/S;

    const/4 v0, 0x3

    new-array v0, v0, [LU/S;

    sget-object v1, LU/S;->a:LU/S;

    aput-object v1, v0, v3

    sget-object v1, LU/S;->b:LU/S;

    aput-object v1, v0, v4

    sget-object v1, LU/S;->c:LU/S;

    aput-object v1, v0, v5

    sput-object v0, LU/S;->e:[LU/S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, LU/S;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/S;
    .locals 1

    const-class v0, LU/S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/S;

    return-object v0
.end method

.method public static values()[LU/S;
    .locals 1

    sget-object v0, LU/S;->e:[LU/S;

    invoke-virtual {v0}, [LU/S;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/S;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LU/S;->d:Ljava/lang/String;

    return-object v0
.end method
