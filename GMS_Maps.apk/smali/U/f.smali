.class LU/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/F;


# static fields
.field private static a:F

.field private static b:F

.field private static c:F

.field private static d:F

.field private static e:F

.field private static f:F

.field private static g:F

.field private static h:F

.field private static i:F


# instance fields
.field private final j:Lbi/d;

.field private final k:Ljava/util/ArrayList;

.field private l:LU/h;


# direct methods
.method public constructor <init>(Lbi/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, LU/f;->l:LU/h;

    iput-object p1, p0, LU/f;->j:Lbi/d;

    invoke-direct {p0}, LU/f;->a()V

    return-void
.end method

.method private static a(F)F
    .locals 2

    sget v0, LU/f;->g:F

    mul-float/2addr v0, p0

    sget v1, LU/f;->h:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    sget v0, LU/f;->h:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, LU/f;->i:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    sget v0, LU/f;->i:F

    goto :goto_0
.end method

.method private static a(FJ)F
    .locals 2

    long-to-float v0, p1

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    sget v1, LU/f;->c:F

    mul-float/2addr v0, v1

    invoke-static {v0, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sget v1, LU/f;->d:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    sget v0, LU/f;->d:F

    :cond_0
    return v0
.end method

.method private a(LU/h;Ljava/util/ArrayList;)I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p1}, LU/h;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    if-eq p1, v0, :cond_1

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private a(LU/h;LU/R;)LU/h;
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, LU/f;->b(LU/h;LU/R;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v1

    iget v1, v1, Lbi/a;->b:F

    const/4 v6, 0x0

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_0

    iget-object v1, v0, LU/h;->b:Lbi/t;

    iget-object v6, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v1, v6}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move v1, v3

    :goto_1
    invoke-virtual {v0, v1}, LU/h;->a(Z)V

    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/h;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, LU/h;->c()LU/j;

    move-result-object v1

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, v4, p1}, LU/f;->a(Ljava/util/ArrayList;LU/h;)LU/h;

    move-result-object v0

    invoke-virtual {v0, p1}, LU/h;->c(LU/h;)LU/i;

    invoke-virtual {v0}, LU/h;->e()LU/i;

    move-result-object v1

    sget-object v4, LU/i;->c:LU/i;

    if-ne v1, v4, :cond_5

    :goto_2
    if-eqz v3, :cond_6

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, LU/j;->b:LU/j;

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    :cond_4
    :goto_3
    return-object v0

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    if-nez v3, :cond_4

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, LU/j;->c:LU/j;

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Ljava/util/ArrayList;LU/h;)LU/h;
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, v1, p2}, LU/f;->a(LU/h;LU/h;LU/h;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v1, p2}, LU/h;->a(LU/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, p2}, LU/h;->b(LU/h;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    return-object p2

    :cond_2
    move-object p2, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lbi/a;Lbi/a;)Lbi/a;
    .locals 2

    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LU/f;->j:Lbi/d;

    sget v1, LU/f;->a:F

    neg-float v1, v1

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private a(Lbi/t;LU/R;II)Ljava/util/ArrayList;
    .locals 11

    iget-object v0, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v0, p1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v9

    invoke-virtual {v9}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v10

    if-gez p4, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p4

    :cond_0
    invoke-virtual {v9, p3}, Lbi/h;->b(I)LaN/B;

    move-result-object v0

    move v3, p3

    move-object v2, v0

    :goto_0
    if-ge v3, p4, :cond_7

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v9, v0}, Lbi/h;->b(I)LaN/B;

    move-result-object v8

    invoke-static {p2}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v4

    invoke-virtual {v2, v8}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v9}, Lbi/h;->b()Lbi/q;

    move-result-object v0

    sget-object v1, Lbi/q;->e:Lbi/q;

    if-ne v0, v1, :cond_1

    invoke-static {v4, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v5

    invoke-static {v5, v9, v3}, LU/f;->a(FLbi/h;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LU/h;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, LU/h;-><init>(Lbi/d;Lbi/t;ILU/R;FFZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move-object v2, v8

    goto :goto_0

    :cond_2
    invoke-static {v4, v2, v8}, LX/g;->c(LaN/B;LaN/B;LaN/B;)F

    move-result v6

    invoke-static {v4, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    invoke-static {v4, v8}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v1

    invoke-static {v4, v2, v8}, LX/g;->b(LaN/B;LaN/B;LaN/B;)F

    move-result v5

    const/4 v2, 0x0

    cmpl-float v2, v6, v2

    if-ltz v2, :cond_4

    const/high16 v2, 0x3f800000

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_4

    :goto_2
    invoke-static {v5, v9, v3}, LU/f;->a(FLbi/h;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v6}, LU/f;->b(F)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    cmpg-float v0, v6, v0

    if-ltz v0, :cond_3

    const/high16 v0, 0x3f800000

    cmpl-float v0, v6, v0

    if-lez v0, :cond_6

    :cond_3
    const/4 v7, 0x1

    :goto_3
    new-instance v0, LU/h;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, LU/h;-><init>(Lbi/d;Lbi/t;ILU/R;FFZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    cmpg-float v2, v6, v2

    if-gez v2, :cond_5

    move v5, v0

    goto :goto_2

    :cond_5
    move v5, v1

    goto :goto_2

    :cond_6
    const/4 v7, 0x0

    goto :goto_3

    :cond_7
    return-object v10
.end method

.method private a()V
    .locals 3

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v1, v0, LX/q;->e:F

    iget v2, v0, LX/q;->d:F

    add-float/2addr v1, v2

    sput v1, LU/f;->a:F

    iget v1, v0, LX/q;->A:F

    sput v1, LU/f;->b:F

    iget v1, v0, LX/q;->j:F

    sput v1, LU/f;->c:F

    iget v1, v0, LX/q;->k:F

    sput v1, LU/f;->d:F

    iget v1, v0, LX/q;->l:F

    sput v1, LU/f;->e:F

    iget v1, v0, LX/q;->m:F

    sput v1, LU/f;->f:F

    iget v1, v0, LX/q;->n:F

    sput v1, LU/f;->g:F

    iget v1, v0, LX/q;->o:F

    sput v1, LU/f;->h:F

    iget v0, v0, LX/q;->p:F

    sput v0, LU/f;->i:F

    return-void
.end method

.method private a(LU/h;)V
    .locals 2

    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    sget-object v0, LU/j;->a:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/f;->j:Lbi/d;

    iget-object v1, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, LU/f;->c(LU/h;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p1}, LU/h;->g()Lbi/a;

    move-result-object v0

    invoke-virtual {p1, v0}, LU/h;->a(Lbi/a;)V

    sget-object v0, LU/j;->c:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    goto :goto_0

    :cond_2
    sget-object v0, LU/j;->a:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;LU/h;Lbi/a;)V
    .locals 3

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    if-eq v0, p2, :cond_0

    invoke-virtual {v0}, LU/h;->h()Lbi/a;

    move-result-object v0

    iget-object v2, p0, LU/f;->j:Lbi/d;

    invoke-static {v2, p3, v0}, LX/g;->a(Lbi/d;Lbi/a;Lbi/a;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v2, LU/f;->a:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(FLbi/h;I)Z
    .locals 1

    invoke-virtual {p1, p2}, Lbi/h;->d(I)F

    move-result v0

    invoke-static {v0}, LU/f;->a(F)F

    move-result v0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LU/h;LU/h;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v1

    invoke-virtual {p2}, LU/h;->h()Lbi/a;

    move-result-object v2

    iget-object v1, v1, Lbi/a;->a:Lbi/t;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/t;->a(Lbi/t;)I

    move-result v1

    if-lez v1, :cond_0

    invoke-direct {p0, p1}, LU/f;->b(LU/h;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method private a(LU/h;LU/h;LU/h;)Z
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, LU/h;->c()LU/j;

    move-result-object v2

    invoke-virtual {p2}, LU/h;->c()LU/j;

    move-result-object v3

    invoke-virtual {v2, v3}, LU/j;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v2

    invoke-virtual {p2}, LU/h;->h()Lbi/a;

    move-result-object v3

    if-eqz p3, :cond_4

    invoke-virtual {p3}, LU/h;->h()Lbi/a;

    move-result-object v4

    iget-object v5, v2, Lbi/a;->a:Lbi/t;

    iget-object v6, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v5, v6}, Lbi/t;->a(Lbi/t;)I

    move-result v5

    iget-object v6, v3, Lbi/a;->a:Lbi/t;

    iget-object v4, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v6, v4}, Lbi/t;->a(Lbi/t;)I

    move-result v4

    if-ltz v5, :cond_3

    if-ltz v4, :cond_0

    :cond_3
    if-gez v5, :cond_4

    if-ltz v4, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, LU/f;->b(LU/h;LU/h;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, p2, p1}, LU/f;->b(LU/h;LU/h;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-direct {p0, p1}, LU/f;->c(LU/h;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-direct {p0, p2}, LU/f;->c(LU/h;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {p1, p3}, LU/h;->b(LU/h;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {p2, p3}, LU/h;->b(LU/h;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_6
    invoke-virtual {v2, v3}, Lbi/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p1, p2}, LU/h;->a(LU/h;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p1, p2}, LU/h;->a(LU/h;)Z

    move-result v0

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method private b(Lbi/a;Lbi/a;)Lbi/a;
    .locals 2

    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LU/f;->j:Lbi/d;

    sget v1, LU/f;->a:F

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private b(LU/R;)Ljava/util/ArrayList;
    .locals 7

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, LU/f;->l:LU/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/f;->l:LU/h;

    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/R;)LU/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    iget-object v5, p0, LU/f;->l:LU/h;

    if-ne v0, v5, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/R;)LU/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1}, LU/f;->c(LU/R;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-direct {p0, v0}, LU/f;->a(LU/h;)V

    invoke-virtual {v0}, LU/h;->c()LU/j;

    move-result-object v5

    sget-object v6, LU/j;->a:LU/j;

    if-eq v5, v6, :cond_4

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    return-object v3
.end method

.method private b(LU/h;LU/R;)Ljava/util/ArrayList;
    .locals 10

    const/4 v9, 0x0

    const/4 v1, -0x1

    invoke-static {p2}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    iget-object v2, p1, LU/h;->d:LaN/B;

    invoke-static {v0, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    invoke-virtual {p2}, LU/R;->getTime()J

    move-result-wide v2

    iget-wide v4, p1, LU/h;->e:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, LU/f;->a(FJ)F

    move-result v0

    iget v2, p1, LU/h;->h:F

    add-float/2addr v0, v2

    invoke-virtual {p2}, LU/R;->getAccuracy()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {p1}, LU/h;->g()Lbi/a;

    move-result-object v2

    iget-object v3, p0, LU/f;->j:Lbi/d;

    neg-float v4, v0

    invoke-static {v3, v2, v4}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v3

    invoke-direct {p0, v2, v3}, LU/f;->a(Lbi/a;Lbi/a;)Lbi/a;

    move-result-object v3

    iget-object v4, p0, LU/f;->j:Lbi/d;

    invoke-static {v4, v2, v0}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    invoke-direct {p0, v2, v0}, LU/f;->b(Lbi/a;Lbi/a;)Lbi/a;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    new-instance v0, Lbi/v;

    iget-object v2, p0, LU/f;->j:Lbi/d;

    invoke-direct {v0, v2}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v2, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/v;->b(Lbi/t;)Lbi/v;

    move-result-object v6

    :cond_0
    invoke-virtual {v6}, Lbi/v;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6}, Lbi/v;->a()Lbi/t;

    move-result-object v7

    invoke-virtual {p2}, LU/R;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v0, v7}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, LU/R;->getSpeed()F

    move-result v0

    sget v2, LU/f;->b:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    :cond_1
    iget-object v0, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LU/f;->j:Lbi/d;

    iget-object v2, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    iget v2, v3, Lbi/a;->b:F

    invoke-virtual {v0, v2}, Lbi/h;->b(F)I

    move-result v0

    :goto_0
    iget-object v2, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v2}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LU/f;->j:Lbi/d;

    iget-object v8, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v8}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    iget v8, v4, Lbi/a;->b:F

    invoke-virtual {v2, v8}, Lbi/h;->b(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    :goto_1
    invoke-direct {p0, v7, p2, v0, v2}, LU/f;->a(Lbi/t;LU/R;II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-virtual {v0}, LU/h;->g()Lbi/a;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbi/a;->a(Lbi/a;)I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-virtual {v0}, LU/h;->g()Lbi/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbi/a;->a(Lbi/a;)I

    move-result v0

    if-gez v0, :cond_3

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_3
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    return-object v5

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;LU/h;)V
    .locals 1

    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object p2, p0, LU/f;->l:LU/h;

    return-void
.end method

.method private static b(F)Z
    .locals 2

    sget v0, LU/f;->e:F

    neg-float v0, v0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000

    sget v1, LU/f;->e:F

    add-float/2addr v0, v1

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LU/h;)Z
    .locals 2

    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LU/h;LU/h;)Z
    .locals 3

    iget v0, p1, LU/h;->f:F

    iget v1, p2, LU/h;->f:F

    sget v2, LU/f;->f:F

    sub-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LU/R;)Ljava/util/ArrayList;
    .locals 6

    const/4 v5, -0x1

    new-instance v0, Lbi/v;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    invoke-direct {v0, v1}, Lbi/v;-><init>(Lbi/d;)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lbi/v;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v2

    invoke-virtual {p1}, LU/R;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v3, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v3

    invoke-virtual {v3}, Lbi/h;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, LU/R;->getSpeed()F

    move-result v3

    sget v4, LU/f;->b:F

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    :cond_1
    invoke-direct {p0, v2, p1, v5, v5}, LU/f;->a(Lbi/t;LU/R;II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private c(LU/h;)Z
    .locals 2

    iget-boolean v0, p1, LU/h;->i:Z

    if-eqz v0, :cond_0

    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LU/R;)LU/G;
    .locals 7

    const/4 v1, 0x1

    const-wide/16 v5, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, LU/R;->getLatitude()D

    move-result-wide v3

    cmpl-double v0, v3, v5

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LU/R;->getLongitude()D

    move-result-wide v3

    cmpl-double v0, v3, v5

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Location should have both latitude and longitude"

    invoke-static {v0, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, LU/f;->b(LU/R;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LU/H;->b:LU/H;

    move-object v1, v0

    move-object v0, v2

    :goto_1
    invoke-direct {p0, v4, v2}, LU/f;->b(Ljava/util/ArrayList;LU/h;)V

    new-instance v2, LU/G;

    invoke-direct {v2, v1, v0}, LU/G;-><init>(LU/H;Lbi/a;)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LU/f;->l:LU/h;

    invoke-direct {p0, v4, v0}, LU/f;->a(Ljava/util/ArrayList;LU/h;)LU/h;

    move-result-object v3

    invoke-virtual {v3}, LU/h;->h()Lbi/a;

    move-result-object v0

    invoke-direct {p0, v4, v3, v0}, LU/f;->a(Ljava/util/ArrayList;LU/h;Lbi/a;)V

    sget-object v5, LU/g;->a:[I

    invoke-virtual {v3}, LU/h;->c()LU/j;

    move-result-object v6

    invoke-virtual {v6}, LU/j;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    sget-object v0, LU/H;->b:LU/H;

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    :pswitch_0
    sget-object v1, LU/H;->e:LU/H;

    move-object v2, v3

    goto :goto_1

    :pswitch_1
    invoke-direct {p0, v3, v4}, LU/f;->a(LU/h;Ljava/util/ArrayList;)I

    move-result v5

    if-ne v5, v1, :cond_2

    sget-object v1, LU/H;->e:LU/H;

    sget-object v2, LU/j;->d:LU/j;

    invoke-virtual {v3, v2}, LU/h;->a(LU/j;)V

    move-object v2, v3

    goto :goto_1

    :cond_2
    sget-object v1, LU/H;->c:LU/H;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
