.class public final enum LU/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/i;

.field public static final enum b:LU/i;

.field public static final enum c:LU/i;

.field private static final synthetic d:[LU/i;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LU/i;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v2}, LU/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/i;->a:LU/i;

    new-instance v0, LU/i;

    const-string v1, "NOT_SURE"

    invoke-direct {v0, v1, v3}, LU/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/i;->b:LU/i;

    new-instance v0, LU/i;

    const-string v1, "BACKWARD"

    invoke-direct {v0, v1, v4}, LU/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/i;->c:LU/i;

    const/4 v0, 0x3

    new-array v0, v0, [LU/i;

    sget-object v1, LU/i;->a:LU/i;

    aput-object v1, v0, v2

    sget-object v1, LU/i;->b:LU/i;

    aput-object v1, v0, v3

    sget-object v1, LU/i;->c:LU/i;

    aput-object v1, v0, v4

    sput-object v0, LU/i;->d:[LU/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/i;
    .locals 1

    const-class v0, LU/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/i;

    return-object v0
.end method

.method public static values()[LU/i;
    .locals 1

    sget-object v0, LU/i;->d:[LU/i;

    invoke-virtual {v0}, [LU/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/i;

    return-object v0
.end method
