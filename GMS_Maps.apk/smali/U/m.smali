.class LU/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/l;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/LinkedList;

.field private final c:LX/i;

.field private final d:Z

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final f:Z

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, LU/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LU/m;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(LU/u;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LU/m;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x1

    iput-boolean v0, p0, LU/m;->f:Z

    new-instance v0, LU/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LU/p;-><init>(LU/m;LU/n;)V

    invoke-interface {p1, v0}, LU/u;->a(Landroid/os/Handler$Callback;)LX/i;

    move-result-object v0

    iput-object v0, p0, LU/m;->c:LX/i;

    iput-boolean p2, p0, LU/m;->d:Z

    return-void
.end method

.method static synthetic a(LU/m;)V
    .locals 0

    invoke-direct {p0}, LU/m;->c()V

    return-void
.end method

.method static synthetic a(LU/m;LU/R;)V
    .locals 0

    invoke-direct {p0, p1}, LU/m;->b(LU/R;)V

    return-void
.end method

.method static synthetic a(LU/m;LU/o;)V
    .locals 0

    invoke-direct {p0, p1}, LU/m;->a(LU/o;)V

    return-void
.end method

.method static synthetic a(LU/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LU/m;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(LU/m;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LU/m;->b(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private a(LU/o;)V
    .locals 1

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b(LU/R;)V
    .locals 4

    invoke-direct {p0}, LU/m;->d()V

    invoke-virtual {p1}, LU/R;->getProvider()Ljava/lang/String;

    move-result-object v1

    const-string v0, "gps"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LU/m;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, LU/m;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LU/m;->g:I

    :cond_0
    return-void

    :cond_1
    iget v0, p0, LU/m;->g:I

    if-lez v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, LU/m;->g:I

    :cond_3
    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/o;

    invoke-virtual {v0}, LU/o;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, LU/o;->b()LU/T;

    move-result-object v0

    invoke-interface {v0, p1}, LU/T;->a(LU/R;)V

    goto :goto_0
.end method

.method static synthetic b(LU/m;LU/o;)V
    .locals 0

    invoke-direct {p0, p1}, LU/m;->b(LU/o;)V

    return-void
.end method

.method static synthetic b(LU/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LU/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(LU/o;)V
    .locals 3

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/o;

    invoke-virtual {v0}, LU/o;->b()LU/T;

    move-result-object v0

    invoke-virtual {p1}, LU/o;->b()LU/T;

    move-result-object v2

    if-ne v0, v2, :cond_0

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/o;

    invoke-virtual {v0}, LU/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LU/o;->b()LU/T;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c()V
    .locals 1

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, LU/m;->c:LX/i;

    invoke-interface {v0}, LX/i;->a()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/o;

    invoke-virtual {v0}, LU/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LU/o;->b()LU/T;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LU/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private d()V
    .locals 3

    iget-boolean v0, p0, LU/m;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Operation must be called on the location thread. Called on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, LU/m;->d()V

    iget-object v0, p0, LU/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/o;

    invoke-virtual {v0}, LU/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LU/o;->b()LU/T;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e()Z
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LU/m;->c:LX/i;

    invoke-interface {v1}, LX/i;->a()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LX/i;
    .locals 1

    iget-object v0, p0, LU/m;->c:LX/i;

    return-object v0
.end method

.method public a(LU/R;)V
    .locals 5

    const-wide v3, 0x3eb0c6f7a0b5ed8dL

    invoke-virtual {p1}, LU/R;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LU/m;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    :cond_0
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "network"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {p1}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    invoke-static {v0}, LaH/E;->e(LaN/B;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/E;->d(LaN/B;)LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-virtual {p1, v1, v2}, LU/R;->setLatitude(D)V

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    invoke-virtual {p1, v0, v1}, LU/R;->setLongitude(D)V

    :cond_2
    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, LU/m;->b(LU/R;)V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, LU/m;->c:LX/i;

    iget-object v1, p0, LU/m;->c:LX/i;

    const/4 v2, 0x3

    invoke-interface {v1, v2, p1}, LX/i;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/i;->a(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;LU/T;)V
    .locals 4

    new-instance v0, LU/o;

    invoke-direct {v0, p1, p2}, LU/o;-><init>(Ljava/lang/String;LU/T;)V

    iget-object v1, p0, LU/m;->c:LX/i;

    iget-object v2, p0, LU/m;->c:LX/i;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v0}, LX/i;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-interface {v1, v0}, LX/i;->a(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, LU/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/m;->c:LX/i;

    iget-object v1, p0, LU/m;->c:LX/i;

    const/4 v2, 0x5

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-interface {v1, v2, v3}, LX/i;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/i;->a(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, LU/m;->b(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/m;->c:LX/i;

    iget-object v1, p0, LU/m;->c:LX/i;

    const/4 v2, 0x6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v5

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-interface {v1, v2, p3, v5, v3}, LX/i;->a(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/i;->a(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 3

    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LU/m;->c()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/m;->c:LX/i;

    iget-object v1, p0, LU/m;->c:LX/i;

    const/4 v2, 0x7

    invoke-interface {v1, v2}, LX/i;->a(I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/i;->a(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, LU/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, LU/m;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LU/m;->c:LX/i;

    iget-object v1, p0, LU/m;->c:LX/i;

    const/4 v2, 0x4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-interface {v1, v2, v3}, LX/i;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/i;->a(Landroid/os/Message;)Z

    goto :goto_0
.end method
