.class public LU/d;
.super LU/b;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Ljava/lang/Runnable;

.field private final c:LX/i;


# direct methods
.method public constructor <init>(LU/l;)V
    .locals 1

    const-string v0, "gps_fixup_provider"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    const/4 v0, 0x1

    iput v0, p0, LU/d;->a:I

    new-instance v0, LU/e;

    invoke-direct {v0, p0}, LU/e;-><init>(LU/d;)V

    iput-object v0, p0, LU/d;->b:Ljava/lang/Runnable;

    invoke-interface {p1}, LU/l;->a()LX/i;

    move-result-object v0

    iput-object v0, p0, LU/d;->c:LX/i;

    return-void
.end method

.method public static a()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->r:I

    int-to-long v0, v0

    return-wide v0
.end method

.method private b(LU/R;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LU/d;->i()F

    move-result v0

    invoke-virtual {p1}, LU/R;->getAccuracy()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p1, v0}, LU/R;->setAccuracy(F)V

    :cond_0
    return-void
.end method

.method private c(LU/R;)V
    .locals 2

    invoke-virtual {p1}, LU/R;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LU/R;->getSpeed()F

    move-result v0

    invoke-static {}, LU/d;->g()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, LU/R;->removeSpeed()V

    :cond_0
    return-void
.end method

.method static g()F
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->j:F

    return v0
.end method

.method static h()F
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->A:F

    return v0
.end method

.method static i()F
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->z:F

    return v0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, LU/d;->c:LX/i;

    iget-object v1, p0, LU/d;->b:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LX/i;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private k()V
    .locals 4

    iget-object v0, p0, LU/d;->c:LX/i;

    iget-object v1, p0, LU/d;->b:Ljava/lang/Runnable;

    invoke-static {}, LU/d;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/i;->b(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public a(LU/R;)V
    .locals 3

    const/4 v2, 0x2

    invoke-virtual {p1}, LU/R;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LU/R;->getAccuracy()F

    move-result v0

    invoke-static {}, LU/d;->h()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, LU/d;->a:I

    if-eq v0, v2, :cond_1

    iput v2, p0, LU/d;->a:I

    const-string v0, "gps_fixup_provider"

    const-string v1, "gps"

    iget v2, p0, LU/d;->a:I

    invoke-virtual {p0, v0, v1, v2}, LU/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    invoke-direct {p0}, LU/d;->j()V

    invoke-direct {p0}, LU/d;->k()V

    new-instance v0, LU/R;

    invoke-direct {v0, p1}, LU/R;-><init>(Landroid/location/Location;)V

    invoke-direct {p0, v0}, LU/d;->b(LU/R;)V

    invoke-direct {p0, v0}, LU/d;->c(LU/R;)V

    invoke-super {p0, v0}, LU/b;->a(LU/R;)V

    goto :goto_0
.end method
