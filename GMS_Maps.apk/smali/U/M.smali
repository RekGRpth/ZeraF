.class public LU/M;
.super LU/b;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field private a:Landroid/location/LocationManager;

.field private b:LI/c;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .locals 1

    const-string v0, "network"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    iput-object p2, p0, LU/M;->c:Landroid/content/Context;

    iput-object p3, p0, LU/M;->d:Lcom/google/googlenav/common/a;

    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LU/M;->a:Landroid/location/LocationManager;

    return-void
.end method

.method private static a()I
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->c:I

    return v0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b()V
    .locals 7

    invoke-virtual {p0}, LU/M;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, LU/M;->a()I

    move-result v2

    iget-object v0, p0, LU/M;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    iget-object v0, p0, LU/M;->c:Landroid/content/Context;

    invoke-static {v0}, LU/M;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LU/M;->b:LI/c;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Already running!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, LI/c;

    iget-object v1, p0, LU/M;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2, p0, v6}, LI/c;-><init>(Landroid/content/Context;ILandroid/location/LocationListener;Landroid/os/Looper;)V

    iput-object v0, p0, LU/M;->b:LI/c;

    :goto_1
    invoke-super {p0}, LU/b;->b()V

    goto :goto_0

    :cond_3
    const-string v1, "network"

    iget-object v0, p0, LU/M;->a:Landroid/location/LocationManager;

    int-to-long v2, v2

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_1
.end method

.method public d()V
    .locals 1

    invoke-virtual {p0}, LU/M;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, LU/b;->d()V

    iget-object v0, p0, LU/M;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v0, p0, LU/M;->b:LI/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LU/M;->b:LI/c;

    invoke-virtual {v0}, LI/c;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, LU/M;->b:LI/c;

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 3

    new-instance v0, LU/R;

    invoke-direct {v0, p1}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v1, LU/S;->b:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    iget-object v1, p0, LU/M;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    invoke-super {p0, v0}, LU/b;->a(LU/R;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p1}, LU/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p1}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p1, p2}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
