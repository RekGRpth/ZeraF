.class public final enum LU/H;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/H;

.field public static final enum b:LU/H;

.field public static final enum c:LU/H;

.field public static final enum d:LU/H;

.field public static final enum e:LU/H;

.field private static final synthetic f:[LU/H;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LU/H;

    const-string v1, "UNAVAIL"

    invoke-direct {v0, v1, v2}, LU/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/H;->a:LU/H;

    new-instance v0, LU/H;

    const-string v1, "OFF_ROUTE"

    invoke-direct {v0, v1, v3}, LU/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/H;->b:LU/H;

    new-instance v0, LU/H;

    const-string v1, "NEEDS_MORE_FIXES"

    invoke-direct {v0, v1, v4}, LU/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/H;->c:LU/H;

    new-instance v0, LU/H;

    const-string v1, "ALMOST_SURE"

    invoke-direct {v0, v1, v5}, LU/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/H;->d:LU/H;

    new-instance v0, LU/H;

    const-string v1, "CERTAIN"

    invoke-direct {v0, v1, v6}, LU/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/H;->e:LU/H;

    const/4 v0, 0x5

    new-array v0, v0, [LU/H;

    sget-object v1, LU/H;->a:LU/H;

    aput-object v1, v0, v2

    sget-object v1, LU/H;->b:LU/H;

    aput-object v1, v0, v3

    sget-object v1, LU/H;->c:LU/H;

    aput-object v1, v0, v4

    sget-object v1, LU/H;->d:LU/H;

    aput-object v1, v0, v5

    sget-object v1, LU/H;->e:LU/H;

    aput-object v1, v0, v6

    sput-object v0, LU/H;->f:[LU/H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/H;
    .locals 1

    const-class v0, LU/H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/H;

    return-object v0
.end method

.method public static values()[LU/H;
    .locals 1

    sget-object v0, LU/H;->f:[LU/H;

    invoke-virtual {v0}, [LU/H;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/H;

    return-object v0
.end method
