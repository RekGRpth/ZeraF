.class LV/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LV/h;


# direct methods
.method constructor <init>(LV/h;)V
    .locals 0

    iput-object p1, p0, LV/i;->a:LV/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LV/g;)V
    .locals 4

    instance-of v0, p1, LV/d;

    if-eqz v0, :cond_1

    check-cast p1, LV/d;

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->d(LV/h;)LU/T;

    move-result-object v0

    iget-object v1, p1, LV/d;->a:LU/R;

    invoke-interface {v0, v1}, LU/T;->a(LU/R;)V

    :cond_0
    :goto_0
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->a(LV/h;)Lcom/google/common/collect/di;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/di;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->a(LV/h;)Lcom/google/common/collect/di;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/di;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LV/g;

    iget-wide v0, v0, LV/g;->c:J

    iget-object v2, p0, LV/i;->a:LV/h;

    invoke-static {v2}, LV/h;->c(LV/h;)LX/a;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/a;->a(J)V

    iget-object v2, p0, LV/i;->a:LV/h;

    invoke-static {v2}, LV/h;->e(LV/h;)LX/b;

    move-result-object v2

    iget-object v3, p0, LV/i;->a:LV/h;

    invoke-static {v3}, LV/h;->b(LV/h;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/b;->a(Ljava/lang/Runnable;J)Z

    :goto_1
    return-void

    :cond_1
    instance-of v0, p1, LV/e;

    if-eqz v0, :cond_3

    check-cast p1, LV/e;

    iget-boolean v0, p1, LV/e;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->d(LV/h;)LU/T;

    move-result-object v0

    iget-object v1, p1, LV/e;->a:Ljava/lang/String;

    iget-object v2, p1, LV/e;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->d(LV/h;)LU/T;

    move-result-object v0

    iget-object v1, p1, LV/e;->a:Ljava/lang/String;

    iget-object v2, p1, LV/e;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LU/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, LV/f;

    if-eqz v0, :cond_0

    check-cast p1, LV/f;

    iget v0, p1, LV/f;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->d(LV/h;)LU/T;

    move-result-object v0

    iget-object v1, p1, LV/f;->a:Ljava/lang/String;

    iget-object v2, p1, LV/f;->a:Ljava/lang/String;

    iget v3, p1, LV/f;->b:I

    invoke-interface {v0, v1, v2, v3}, LU/T;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->c(LV/h;)LX/a;

    move-result-object v0

    invoke-virtual {v0}, LX/a;->a()V

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->f(LV/h;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Reached end of log"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->a(LV/h;)Lcom/google/common/collect/di;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/di;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->b(LV/h;)Ljava/lang/Runnable;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-virtual {v0}, LV/h;->e()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LV/i;->a:LV/h;

    invoke-static {v0}, LV/h;->a(LV/h;)Lcom/google/common/collect/di;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/di;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LV/g;

    iget-object v2, p0, LV/i;->a:LV/h;

    invoke-static {v2}, LV/h;->c(LV/h;)LX/a;

    move-result-object v2

    iget-wide v3, v0, LV/g;->c:J

    invoke-virtual {v2, v3, v4}, LX/a;->b(J)V

    invoke-direct {p0, v0}, LV/i;->a(LV/g;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
