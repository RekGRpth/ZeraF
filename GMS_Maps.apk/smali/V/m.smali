.class public LV/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/LinkedList;

.field private final c:LV/j;

.field private final d:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, LV/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LV/m;->a:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, LV/m;->c:LV/j;

    iget-object v1, p0, LV/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, LV/j;->a(Ljava/util/List;)V

    iget-object v0, p0, LV/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-direct {p0}, LV/m;->c()V

    iget-object v0, p0, LV/m;->c:LV/j;

    invoke-virtual {v0}, LV/j;->a()V

    return-void
.end method

.method public a(LU/R;)V
    .locals 1

    new-instance v0, LV/d;

    invoke-direct {v0, p1}, LV/d;-><init>(LU/R;)V

    invoke-virtual {p0, v0}, LV/m;->a(LV/g;)V

    return-void
.end method

.method public a(LV/g;)V
    .locals 2

    iget-object v0, p0, LV/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, LV/m;->c()V

    :cond_0
    iget-object v0, p0, LV/m;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    new-instance v0, LV/e;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, LV/e;-><init>(JLjava/lang/String;Z)V

    invoke-virtual {p0, v0}, LV/m;->a(LV/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 3

    new-instance v0, LV/f;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2, p1, p2}, LV/f;-><init>(JLjava/lang/String;I)V

    invoke-virtual {p0, v0}, LV/m;->a(LV/g;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, LV/m;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, LV/m;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    new-instance v0, LV/e;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, p1, v3}, LV/e;-><init>(JLjava/lang/String;Z)V

    invoke-virtual {p0, v0}, LV/m;->a(LV/g;)V

    return-void
.end method
