.class public LV/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lorg/xmlpull/v1/XmlPullParserFactory;

.field private final b:Laa/e;

.field private c:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method public constructor <init>(Laa/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    iput-object v0, p0, LV/n;->a:Lorg/xmlpull/v1/XmlPullParserFactory;

    iput-object p1, p0, LV/n;->b:Laa/e;

    invoke-virtual {p0}, LV/n;->a()V

    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LV/g;
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1}, LV/n;->b(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "location-scan"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, LV/n;->d(Lorg/xmlpull/v1/XmlPullParser;)LV/d;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "provider-enabled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, LV/n;->e(Lorg/xmlpull/v1/XmlPullParser;)LV/e;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v2, "provider-status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, LV/n;->f(Lorg/xmlpull/v1/XmlPullParser;)LV/f;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "wifi-scan"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cell-scan"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "location-scan"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "provider-enabled"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "provider-status"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Lorg/xmlpull/v1/XmlPullParser;
    .locals 4

    iget-object v0, p0, LV/n;->a:Lorg/xmlpull/v1/XmlPullParserFactory;

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v0, p0, LV/n;->b:Laa/e;

    invoke-interface {v0}, Laa/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    return-object v1
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    :goto_0
    if-eq v1, v0, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LV/n;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;)J
    .locals 2

    const/4 v0, 0x0

    const-string v1, "time"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/f;->a(Ljava/lang/String;)Lcom/google/googlenav/common/util/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/f;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private d(Lorg/xmlpull/v1/XmlPullParser;)LV/d;
    .locals 6

    const/4 v5, 0x0

    const-string v0, "accuracy"

    invoke-interface {p1, v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "latitude"

    invoke-interface {p1, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "longitude"

    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "speed"

    invoke-interface {p1, v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "provider"

    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/location/Location;

    invoke-direct {v5, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/location/Location;->setAccuracy(F)V

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Landroid/location/Location;->removeSpeed()V

    :goto_0
    invoke-static {p1}, LV/n;->c(Lorg/xmlpull/v1/XmlPullParser;)J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setTime(J)V

    new-instance v0, LU/R;

    invoke-direct {v0, v5}, LU/R;-><init>(Landroid/location/Location;)V

    const-string v1, "gps"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    :cond_0
    :goto_1
    new-instance v1, LV/d;

    invoke-direct {v1, v0}, LV/d;-><init>(LU/R;)V

    return-object v1

    :cond_1
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/location/Location;->setSpeed(F)V

    goto :goto_0

    :cond_2
    const-string v1, "network"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LU/S;->b:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    goto :goto_1
.end method

.method private e(Lorg/xmlpull/v1/XmlPullParser;)LV/e;
    .locals 5

    const/4 v2, 0x0

    const-string v0, "provider"

    invoke-interface {p1, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "enabled"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    new-instance v2, LV/e;

    invoke-static {p1}, LV/n;->c(Lorg/xmlpull/v1/XmlPullParser;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4, v0, v1}, LV/e;-><init>(JLjava/lang/String;Z)V

    return-object v2
.end method

.method private f(Lorg/xmlpull/v1/XmlPullParser;)LV/f;
    .locals 5

    const/4 v2, 0x0

    const-string v0, "provider"

    invoke-interface {p1, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LV/f;

    invoke-static {p1}, LV/n;->c(Lorg/xmlpull/v1/XmlPullParser;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4, v0, v1}, LV/f;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 3

    invoke-static {p1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v2, p0, LV/n;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {p0, v2}, LV/n;->a(Lorg/xmlpull/v1/XmlPullParser;)LV/g;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-object v1

    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    invoke-direct {p0}, LV/n;->b()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    iput-object v0, p0, LV/n;->c:Lorg/xmlpull/v1/XmlPullParser;

    return-void
.end method
