.class public Lbh/j;
.super Lbh/a;
.source "SourceFile"


# instance fields
.field private c:[Lcom/google/googlenav/e;


# direct methods
.method public constructor <init>(Lbf/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lbh/a;-><init>(Lbf/i;)V

    return-void
.end method

.method private a(Z)Lcom/google/googlenav/e;
    .locals 2

    iget-object v0, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/googlenav/E;)I
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbh/j;->a(Z)Lcom/google/googlenav/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/e;->e()I

    move-result v0

    :cond_0
    return v0
.end method

.method public a()Z
    .locals 8

    const/16 v7, 0xf

    const/16 v6, 0xc

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0}, Lbh/a;->a()Z

    move-result v2

    iget-object v3, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    iget-object v4, p0, Lbh/j;->b:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bi;->S()[[Lcom/google/googlenav/e;

    move-result-object v4

    iget-object v5, p0, Lbh/j;->a:Lbf/i;

    invoke-virtual {v5}, Lbf/i;->aw()LaN/Y;

    move-result-object v5

    invoke-virtual {v5}, LaN/Y;->a()I

    move-result v5

    if-ge v5, v6, :cond_2

    aget-object v4, v4, v0

    iput-object v4, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    if-eq v3, v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    if-lt v5, v6, :cond_3

    if-ge v5, v7, :cond_3

    aget-object v4, v4, v1

    iput-object v4, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    goto :goto_0

    :cond_3
    if-lt v5, v7, :cond_4

    const/16 v6, 0x13

    if-ge v5, v6, :cond_4

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iput-object v4, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    goto :goto_0

    :cond_4
    const/4 v5, 0x3

    aget-object v4, v4, v5

    iput-object v4, p0, Lbh/j;->c:[Lcom/google/googlenav/e;

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbh/j;->a(Z)Lcom/google/googlenav/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/e;->f()I

    move-result v0

    :cond_0
    return v0
.end method

.method public b(I)Lcom/google/googlenav/e;
    .locals 1

    iget-object v0, p0, Lbh/j;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbh/j;->a(I)Z

    move-result v0

    invoke-direct {p0, v0}, Lbh/j;->a(Z)Lcom/google/googlenav/e;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
