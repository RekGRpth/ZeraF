.class public LaR/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:LaR/l;


# instance fields
.field private b:LaR/R;

.field private c:LaR/I;

.field private d:LaR/n;

.field private e:LaR/n;

.field private f:LaR/n;

.field private g:LaR/n;

.field private h:LaR/n;

.field private i:LaR/n;

.field private j:LaR/n;

.field private k:LaR/n;

.field private l:[LaR/n;

.field private m:Ljava/util/Timer;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, LaR/l;->c(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, LaR/l;->e:LaR/n;

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    iget-object v0, p0, LaR/l;->f:LaR/n;

    if-eqz v0, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, LaR/l;->f:LaR/n;

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    :cond_0
    return-void
.end method

.method static synthetic a(LaR/l;)LaR/I;
    .locals 1

    iget-object v0, p0, LaR/l;->c:LaR/I;

    return-object v0
.end method

.method public static a()LaR/l;
    .locals 1

    sget-object v0, LaR/l;->a:LaR/l;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ui/wizard/jv;)LaR/l;
    .locals 1

    sget-object v0, LaR/l;->a:LaR/l;

    if-nez v0, :cond_1

    const-string v0, "MyPlacesContext.MyPlacesContext"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    new-instance v0, LaR/l;

    invoke-direct {v0, p0}, LaR/l;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    sput-object v0, LaR/l;->a:LaR/l;

    const-string v0, "MyPlacesContext.MyPlacesContext"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    sget-object v0, LaR/l;->a:LaR/l;

    return-object v0

    :cond_1
    if-eqz p0, :cond_0

    sget-object v0, LaR/l;->a:LaR/l;

    invoke-direct {v0, p0}, LaR/l;->b(Lcom/google/googlenav/ui/wizard/jv;)V

    goto :goto_0
.end method

.method private a(JJ)V
    .locals 6

    iget-object v0, p0, LaR/l;->m:Ljava/util/Timer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LaR/l;->m:Ljava/util/Timer;

    iget-object v0, p0, LaR/l;->m:Ljava/util/Timer;

    new-instance v1, LaR/m;

    invoke-direct {v1, p0}, LaR/m;-><init>(LaR/l;)V

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :cond_0
    return-void
.end method

.method private b(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    iget-object v0, p0, LaR/l;->e:LaR/n;

    invoke-interface {v0, p1}, LaR/n;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    iget-object v0, p0, LaR/l;->f:LaR/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/l;->f:LaR/n;

    invoke-interface {v0, p1}, LaR/n;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    :cond_0
    iget-object v0, p0, LaR/l;->d:LaR/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaR/l;->d:LaR/n;

    invoke-interface {v0, p1}, LaR/n;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    :cond_1
    iget-object v0, p0, LaR/l;->b:LaR/R;

    check-cast v0, LaR/S;

    invoke-virtual {v0, p1}, LaR/S;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private c(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 7

    new-instance v0, LaR/S;

    invoke-direct {v0, p1}, LaR/S;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, LaR/l;->b:LaR/R;

    new-instance v0, LaR/I;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-direct {v0, v1}, LaR/I;-><init>(Lcom/google/googlenav/common/io/j;)V

    iput-object v0, p0, LaR/l;->c:LaR/I;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/gq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->e:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x3

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->f:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/16 v4, 0x8

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/et;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->d:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x7

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->i:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x5

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->g:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x4

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->h:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/4 v4, 0x6

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->j:LaR/n;

    new-instance v0, LaR/q;

    iget-object v2, p0, LaR/l;->b:LaR/R;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    const/16 v4, 0x9

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, LaR/l;->c:LaR/I;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LaR/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V

    iput-object v0, p0, LaR/l;->k:LaR/n;

    invoke-direct {p0}, LaR/l;->o()V

    const-wide/32 v0, 0x2bf20

    const-wide/32 v2, 0x5265c00

    invoke-direct {p0, v0, v1, v2, v3}, LaR/l;->a(JJ)V

    return-void
.end method

.method private o()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [LaR/n;

    const/4 v1, 0x0

    iget-object v2, p0, LaR/l;->e:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaR/l;->f:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LaR/l;->h:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LaR/l;->g:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LaR/l;->i:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LaR/l;->d:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LaR/l;->j:LaR/n;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, LaR/l;->k:LaR/n;

    aput-object v2, v0, v1

    iput-object v0, p0, LaR/l;->l:[LaR/n;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LaR/D;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LaR/l;->l:[LaR/n;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, LaR/l;->l:[LaR/n;

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    instance-of v2, v0, LaR/D;

    if-eqz v2, :cond_0

    check-cast v0, LaR/D;

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()LaR/aa;
    .locals 1

    iget-object v0, p0, LaR/l;->b:LaR/R;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, LaR/l;->l:[LaR/n;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, LaR/l;->l:[LaR/n;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v2}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-interface {v2, p1}, LaR/u;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public c()LaR/R;
    .locals 1

    iget-object v0, p0, LaR/l;->b:LaR/R;

    return-object v0
.end method

.method public d()[LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->l:[LaR/n;

    return-object v0
.end method

.method public e()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->d:LaR/n;

    return-object v0
.end method

.method public f()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->e:LaR/n;

    return-object v0
.end method

.method public g()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->f:LaR/n;

    return-object v0
.end method

.method public h()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->h:LaR/n;

    return-object v0
.end method

.method public i()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->g:LaR/n;

    return-object v0
.end method

.method public j()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->i:LaR/n;

    return-object v0
.end method

.method public k()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->j:LaR/n;

    return-object v0
.end method

.method public l()LaR/n;
    .locals 1

    iget-object v0, p0, LaR/l;->k:LaR/n;

    return-object v0
.end method

.method public m()LaR/I;
    .locals 1

    iget-object v0, p0, LaR/l;->c:LaR/I;

    return-object v0
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, LaR/l;->b:LaR/R;

    invoke-interface {v0}, LaR/R;->g()V

    return-void
.end method
