.class LaR/c;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:LaR/b;


# direct methods
.method constructor <init>(LaR/b;Las/c;)V
    .locals 0

    iput-object p1, p0, LaR/c;->a:LaR/b;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->a(LaR/b;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->a(LaR/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->b(LaR/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->c(LaR/b;)Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, LaR/c;->a:LaR/b;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LaR/b;->a(LaR/b;Z)Z

    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->d(LaR/b;)Z

    move-result v0

    iget-object v2, p0, LaR/c;->a:LaR/b;

    const/4 v3, 0x0

    invoke-static {v2, v3}, LaR/b;->b(LaR/b;Z)Z

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v1, "BackgroundPlaceDetailsFetcher.doCheck"

    invoke-static {v1}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    iget-object v1, p0, LaR/c;->a:LaR/b;

    invoke-static {v1}, LaR/b;->e(LaR/b;)I

    move-result v1

    if-eqz v0, :cond_2

    const-string v0, "BPDF1"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v0, "BackgroundPlaceDetailsFetcher.doCheck"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
