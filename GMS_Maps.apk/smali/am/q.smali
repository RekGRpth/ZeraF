.class public Lam/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lam/e;


# instance fields
.field private a:Lam/e;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Lam/e;IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual/range {p0 .. p5}, Lam/q;->a(Lam/e;IIII)V

    return-void
.end method

.method private b(I)I
    .locals 2

    iget v0, p0, Lam/q;->b:I

    add-int/2addr v0, p1

    iget v1, p0, Lam/q;->h:I

    add-int/2addr v0, v1

    return v0
.end method

.method private c(I)I
    .locals 2

    iget v0, p0, Lam/q;->c:I

    add-int/2addr v0, p1

    iget v1, p0, Lam/q;->i:I

    add-int/2addr v0, v1

    return v0
.end method

.method private d(I)I
    .locals 2

    iget v0, p0, Lam/q;->b:I

    sub-int v0, p1, v0

    iget v1, p0, Lam/q;->h:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private e(I)I
    .locals 2

    iget v0, p0, Lam/q;->c:I

    sub-int v0, p1, v0

    iget v1, p0, Lam/q;->i:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private f()Z
    .locals 5

    iget v0, p0, Lam/q;->l:I

    if-lez v0, :cond_0

    iget v0, p0, Lam/q;->m:I

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0}, Lam/e;->a()I

    move-result v0

    iput v0, p0, Lam/q;->n:I

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0}, Lam/e;->b()I

    move-result v0

    iput v0, p0, Lam/q;->o:I

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0}, Lam/e;->c()I

    move-result v0

    iput v0, p0, Lam/q;->p:I

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0}, Lam/e;->d()I

    move-result v0

    iput v0, p0, Lam/q;->q:I

    iget-object v0, p0, Lam/q;->a:Lam/e;

    iget v1, p0, Lam/q;->j:I

    iget v2, p0, Lam/q;->k:I

    iget v3, p0, Lam/q;->l:I

    iget v4, p0, Lam/q;->m:I

    invoke-interface {v0, v1, v2, v3, v4}, Lam/e;->a(IIII)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 5

    iget-object v0, p0, Lam/q;->a:Lam/e;

    iget v1, p0, Lam/q;->n:I

    iget v2, p0, Lam/q;->o:I

    iget v3, p0, Lam/q;->p:I

    iget v4, p0, Lam/q;->q:I

    invoke-interface {v0, v1, v2, v3, v4}, Lam/e;->a(IIII)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lam/q;->j:I

    invoke-direct {p0, v0}, Lam/q;->d(I)I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0, p1}, Lam/e;->a(I)V

    return-void
.end method

.method public a(IIII)V
    .locals 3

    invoke-direct {p0, p1}, Lam/q;->b(I)I

    move-result v0

    iput v0, p0, Lam/q;->j:I

    invoke-direct {p0, p2}, Lam/q;->c(I)I

    move-result v0

    iput v0, p0, Lam/q;->k:I

    iput p3, p0, Lam/q;->l:I

    iput p4, p0, Lam/q;->m:I

    iget v0, p0, Lam/q;->b:I

    iget v1, p0, Lam/q;->j:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lam/q;->l:I

    iget v1, p0, Lam/q;->b:I

    iget v2, p0, Lam/q;->j:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lam/q;->l:I

    iget v0, p0, Lam/q;->b:I

    iput v0, p0, Lam/q;->j:I

    :cond_0
    iget v0, p0, Lam/q;->c:I

    iget v1, p0, Lam/q;->k:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lam/q;->m:I

    iget v1, p0, Lam/q;->c:I

    iget v2, p0, Lam/q;->k:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lam/q;->m:I

    iget v0, p0, Lam/q;->c:I

    iput v0, p0, Lam/q;->k:I

    :cond_1
    iget v0, p0, Lam/q;->j:I

    iget v1, p0, Lam/q;->l:I

    add-int/2addr v0, v1

    iget v1, p0, Lam/q;->f:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lam/q;->f:I

    iget v1, p0, Lam/q;->j:I

    sub-int/2addr v0, v1

    iput v0, p0, Lam/q;->l:I

    :cond_2
    iget v0, p0, Lam/q;->k:I

    iget v1, p0, Lam/q;->m:I

    add-int/2addr v0, v1

    iget v1, p0, Lam/q;->g:I

    if-le v0, v1, :cond_3

    iget v0, p0, Lam/q;->g:I

    iget v1, p0, Lam/q;->k:I

    sub-int/2addr v0, v1

    iput v0, p0, Lam/q;->m:I

    :cond_3
    return-void
.end method

.method public a(Lam/d;)V
    .locals 1

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0, p1}, Lam/e;->a(Lam/d;)V

    return-void
.end method

.method public a(Lam/e;IIII)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lam/q;->a:Lam/e;

    iput p2, p0, Lam/q;->b:I

    iput p3, p0, Lam/q;->c:I

    iput p4, p0, Lam/q;->d:I

    iput p5, p0, Lam/q;->e:I

    iget v0, p0, Lam/q;->b:I

    iget v1, p0, Lam/q;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lam/q;->f:I

    iget v0, p0, Lam/q;->c:I

    iget v1, p0, Lam/q;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lam/q;->g:I

    iget v0, p0, Lam/q;->b:I

    iput v0, p0, Lam/q;->j:I

    iget v0, p0, Lam/q;->c:I

    iput v0, p0, Lam/q;->k:I

    iput p4, p0, Lam/q;->l:I

    iput p5, p0, Lam/q;->m:I

    iput v2, p0, Lam/q;->h:I

    iput v2, p0, Lam/q;->i:I

    return-void
.end method

.method public a(Lam/f;II)V
    .locals 3

    invoke-direct {p0}, Lam/q;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-direct {p0, p2}, Lam/q;->b(I)I

    move-result v1

    invoke-direct {p0, p3}, Lam/q;->c(I)I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lam/e;->a(Lam/f;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-direct {p0}, Lam/q;->g()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lam/q;->g()V

    throw v0
.end method

.method public a(Ljava/lang/String;II)V
    .locals 3

    invoke-direct {p0}, Lam/q;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-direct {p0, p2}, Lam/q;->b(I)I

    move-result v1

    invoke-direct {p0, p3}, Lam/q;->c(I)I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lam/e;->a(Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lam/q;->g()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lam/q;->g()V

    throw v0
.end method

.method public a(Lam/f;IIIIIIII)Z
    .locals 10

    invoke-direct {p0}, Lam/q;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-direct {p0, p2}, Lam/q;->b(I)I

    move-result v2

    invoke-direct {p0, p3}, Lam/q;->c(I)I

    move-result v3

    move-object v1, p1

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lam/e;->a(Lam/f;IIIIIIII)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-direct {p0}, Lam/q;->g()V

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lam/q;->g()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lam/q;->k:I

    invoke-direct {p0, v0}, Lam/q;->e(I)I

    move-result v0

    return v0
.end method

.method public b(IIII)V
    .locals 3

    invoke-direct {p0}, Lam/q;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-direct {p0, p1}, Lam/q;->b(I)I

    move-result v1

    invoke-direct {p0, p2}, Lam/q;->c(I)I

    move-result v2

    invoke-interface {v0, v1, v2, p3, p4}, Lam/e;->b(IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lam/q;->g()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lam/q;->g()V

    throw v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lam/q;->l:I

    return v0
.end method

.method public c(IIII)V
    .locals 3

    invoke-direct {p0}, Lam/q;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-direct {p0, p1}, Lam/q;->b(I)I

    move-result v1

    invoke-direct {p0, p2}, Lam/q;->c(I)I

    move-result v2

    invoke-interface {v0, v1, v2, p3, p4}, Lam/e;->c(IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lam/q;->g()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lam/q;->g()V

    throw v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lam/q;->m:I

    return v0
.end method

.method public e()Lam/d;
    .locals 1

    iget-object v0, p0, Lam/q;->a:Lam/e;

    invoke-interface {v0}, Lam/e;->e()Lam/d;

    move-result-object v0

    return-object v0
.end method
