.class public Lam/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lam/g;


# instance fields
.field private final a:[Lam/g;

.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lam/g;Lam/g;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Lam/g;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-direct {p0, v0}, Lam/a;-><init>([Lam/g;)V

    return-void
.end method

.method public constructor <init>([Lam/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lam/a;->b:Ljava/util/Map;

    iput-object p1, p0, Lam/a;->a:[Lam/g;

    invoke-direct {p0}, Lam/a;->b()V

    return-void
.end method

.method private b()V
    .locals 0

    return-void
.end method

.method private f(C)Lam/g;
    .locals 3

    iget-object v0, p0, Lam/a;->b:Ljava/util/Map;

    new-instance v1, Ljava/lang/Character;

    invoke-direct {v1, p1}, Ljava/lang/Character;-><init>(C)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/g;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lam/a;->a:[Lam/g;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lam/a;->a:[Lam/g;

    aget-object v2, v2, v1

    invoke-interface {v2, p1}, Lam/g;->a(C)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lam/a;->a:[Lam/g;

    aget-object v0, v0, v1

    iget-object v1, p0, Lam/a;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lam/a;->a:[Lam/g;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lam/a;->a:[Lam/g;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lam/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(C)Z
    .locals 1

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(CLam/e;II)Z
    .locals 1

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lam/g;->a(CLam/e;II)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(C)I
    .locals 2

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lam/g;->b(C)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lam/a;->a:[Lam/g;

    iget-object v1, p0, Lam/a;->a:[Lam/g;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-interface {v0, p1}, Lam/g;->b(C)I

    move-result v0

    goto :goto_0
.end method

.method public c(C)I
    .locals 2

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lam/g;->c(C)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lam/a;->a:[Lam/g;

    iget-object v1, p0, Lam/a;->a:[Lam/g;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-interface {v0, p1}, Lam/g;->c(C)I

    move-result v0

    goto :goto_0
.end method

.method public d(C)I
    .locals 2

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lam/g;->d(C)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lam/a;->a:[Lam/g;

    iget-object v1, p0, Lam/a;->a:[Lam/g;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-interface {v0, p1}, Lam/g;->d(C)I

    move-result v0

    goto :goto_0
.end method

.method public e(C)Lam/f;
    .locals 1

    invoke-direct {p0, p1}, Lam/a;->f(C)Lam/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lam/a;->a:[Lam/g;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v0

    iget-object v2, p0, Lam/a;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v2, v1

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v3, "CompositeIconProvider"

    invoke-direct {v1, v3, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    :goto_0
    iget-object v2, p0, Lam/a;->a:[Lam/g;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lam/a;->a:[Lam/g;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lam/g;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
