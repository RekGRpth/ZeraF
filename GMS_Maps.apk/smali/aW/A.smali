.class public LaW/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/ab;
.implements LaR/o;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/ViewGroup;

.field private c:Ljava/util/Set;

.field private d:LaR/n;

.field private e:LaR/n;

.field private f:Ljava/util/List;

.field private final g:Lcom/google/googlenav/ui/wizard/gj;

.field private h:Lcom/google/googlenav/aV;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/aV;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaW/A;->c:Ljava/util/Set;

    iput-object p1, p0, LaW/A;->g:Lcom/google/googlenav/ui/wizard/gj;

    iput-object p2, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaW/A;->f:Ljava/util/List;

    return-void
.end method

.method static synthetic a(LaW/A;)LaR/n;
    .locals 1

    iget-object v0, p0, LaW/A;->e:LaR/n;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    iget-object v1, p0, LaW/A;->f:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    iget-object v2, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aV;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, LaW/A;->f()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(LaW/A;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1}, LaW/A;->a(Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 5

    iget-object v1, p0, LaW/A;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0, p0}, LaR/n;->a(LaR/o;)V

    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0, p0}, LaR/n;->a(LaR/o;)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    iget-object v3, p0, LaW/A;->c:Ljava/util/Set;

    iget-object v4, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v3, v0, LaW/D;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, LaW/A;->d:LaR/n;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, LaR/n;->a(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v3, p0, LaW/A;->e:LaR/n;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, LaR/n;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private a(Z)V
    .locals 4

    new-instance v0, LaW/E;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaW/E;-><init>(LaW/A;LaW/B;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LaW/E;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(LaW/A;)LaR/n;
    .locals 1

    iget-object v0, p0, LaW/A;->d:LaR/n;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    iget-object v0, v0, LaW/D;->d:Lcom/google/googlenav/ai;

    iget-object v1, p0, LaW/A;->g:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/gj;->a(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method static synthetic c(LaW/A;)Lcom/google/googlenav/aV;
    .locals 1

    iget-object v0, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    return-object v0
.end method

.method static synthetic d(LaW/A;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(LaW/A;)V
    .locals 0

    invoke-direct {p0}, LaW/A;->f()V

    return-void
.end method

.method private f()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    invoke-virtual {p0, v0}, LaW/A;->a(LaW/D;)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    new-instance v0, LaW/C;

    invoke-direct {v0, p0}, LaW/C;-><init>(LaW/A;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaW/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public F_()V
    .locals 2

    new-instance v0, LaW/C;

    invoke-direct {v0, p0}, LaW/C;-><init>(LaW/A;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaW/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public a(LaW/D;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, LaW/A;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1000fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100020

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100199

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100044

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100335

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x3b1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f100334

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_0
    const/16 v2, 0x3b2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a()V
    .locals 1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    return-void
.end method

.method public a(LaR/P;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f100325

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/A;->a:Landroid/view/View;

    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const v1, 0x7f100333

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaW/A;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const v1, 0x7f100332

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x3b3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    iput-object v0, p0, LaW/A;->d:LaR/n;

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->h()LaR/n;

    move-result-object v0

    iput-object v0, p0, LaW/A;->e:LaR/n;

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->a(LaR/ab;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 2

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    new-instance v1, LaW/D;

    invoke-direct {v1, p0, p1}, LaW/D;-><init>(LaW/A;Lcom/google/googlenav/ai;)V

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, LaW/A;->a(I)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, LaW/A;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaW/A;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LaW/A;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0, v2}, LaR/n;->a(LaR/o;)V

    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0, v2}, LaR/n;->a(LaR/o;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    return-void
.end method

.method protected c()I
    .locals 1

    const v0, 0x7f04012b

    return v0
.end method

.method public e()Ljava/util/List;
    .locals 6

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    new-instance v3, LaW/D;

    invoke-virtual {v0}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5, v0}, LaW/D;-><init>(LaW/A;Ljava/lang/String;ZLaR/t;)V

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    new-instance v3, LaW/D;

    invoke-virtual {v0}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, p0, v4, v5, v0}, LaW/D;-><init>(LaW/A;Ljava/lang/String;ZLaR/t;)V

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f1000fc

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, LaW/A;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, LaW/A;->b(I)V

    goto :goto_0
.end method
