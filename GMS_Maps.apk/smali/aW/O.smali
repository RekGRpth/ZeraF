.class public LaW/O;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/googlenav/ui/wizard/gj;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaW/O;->e:Z

    iput-object p2, p0, LaW/O;->a:Ljava/lang/String;

    iput-boolean p1, p0, LaW/O;->d:Z

    iput-object p3, p0, LaW/O;->f:Lcom/google/googlenav/ui/wizard/gj;

    return-void
.end method

.method static synthetic a(LaW/O;)Z
    .locals 1

    iget-boolean v0, p0, LaW/O;->d:Z

    return v0
.end method

.method static synthetic b(LaW/O;)Lcom/google/googlenav/ui/wizard/gj;
    .locals 1

    iget-object v0, p0, LaW/O;->f:Lcom/google/googlenav/ui/wizard/gj;

    return-object v0
.end method

.method static synthetic c(LaW/O;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, LaW/O;->b:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f100336

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/O;->b:Landroid/view/View;

    const v0, 0x7f100337

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaW/O;->c:Landroid/widget/TextView;

    const v0, 0x7f100338

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v1, 0x3b9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-boolean v0, p0, LaW/O;->d:Z

    iget-object v1, p0, LaW/O;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LaW/O;->a(ZLjava/lang/String;)V

    iget-object v0, p0, LaW/O;->b:Landroid/view/View;

    new-instance v1, LaW/P;

    invoke-direct {v1, p0}, LaW/P;-><init>(LaW/O;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    iget-boolean v0, p0, LaW/O;->e:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, LaW/O;->e:Z

    if-eqz p1, :cond_2

    iget-object v0, p0, LaW/O;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1

    const v0, 0x7f05001c

    :goto_1
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v1, p0, LaW/O;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v1, LaW/Q;

    invoke-direct {v1, p0}, LaW/Q;-><init>(LaW/O;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f05001d

    goto :goto_1

    :cond_2
    iget-object v0, p0, LaW/O;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    iput-object p2, p0, LaW/O;->a:Ljava/lang/String;

    iput-boolean p1, p0, LaW/O;->d:Z

    iget-object v0, p0, LaW/O;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/O;->c:Landroid/widget/TextView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, LaW/O;->a(Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, LaW/O;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
