.class public LaW/F;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# static fields
.field static a:Lcom/google/googlenav/ui/wizard/gj;

.field static c:Z

.field static d:Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field b:Ljava/lang/String;

.field private final l:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaW/G;

    invoke-direct {v0}, LaW/G;-><init>()V

    sput-object v0, LaW/F;->d:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/gj;Ljava/lang/String;Z)V
    .locals 2

    const v0, 0x7f0f001b

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    sput-object p1, LaW/F;->a:Lcom/google/googlenav/ui/wizard/gj;

    iput-object p2, p0, LaW/F;->b:Ljava/lang/String;

    sput-boolean p3, LaW/F;->c:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, LaW/H;->values()[LaW/H;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LaW/F;->l:Ljava/util/List;

    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    sget-object v1, LaW/H;->a:LaW/H;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    sget-object v1, LaW/H;->b:LaW/H;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    sget-object v1, LaW/H;->c:LaW/H;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    sget-object v1, LaW/H;->d:LaW/H;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    sget-object v1, LaW/H;->e:LaW/H;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(LaW/F;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaW/F;->l:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaW/F;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_0
    return-void
.end method

.method public O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaW/F;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, LaW/F;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040172

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100340

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, LaW/F;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f1003f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v2, LaW/N;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LaW/N;-><init>(LaW/F;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v2, LaW/F;->d:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v1

    :cond_0
    iget-object v0, p0, LaW/F;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, LaW/F;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
