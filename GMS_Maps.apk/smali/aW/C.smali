.class public LaW/C;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:LaW/A;

.field private b:Ljava/util/Set;


# direct methods
.method public constructor <init>(LaW/A;)V
    .locals 0

    iput-object p1, p0, LaW/C;->a:LaW/A;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private a(LaW/D;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->l()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v2

    invoke-virtual {v2}, LaR/H;->c()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    iget-object v0, p0, LaW/C;->a:LaW/A;

    invoke-virtual {v0}, LaW/A;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    iget-object v2, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_3

    :cond_1
    iget-object v0, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, LaW/C;->a:LaW/A;

    iget-object v1, p0, LaW/C;->b:Ljava/util/Set;

    invoke-static {v0, v1}, LaW/A;->a(LaW/A;Ljava/util/Set;)V

    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    invoke-direct {p0, v0}, LaW/C;->a(LaW/D;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaW/C;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
