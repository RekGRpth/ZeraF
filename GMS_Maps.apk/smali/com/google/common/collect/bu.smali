.class Lcom/google/common/collect/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:I

.field b:Lcom/google/common/collect/bt;

.field c:Lcom/google/common/collect/bt;

.field d:Lcom/google/common/collect/bt;

.field final synthetic e:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;I)V
    .locals 2

    iput-object p1, p0, Lcom/google/common/collect/bu;->e:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/common/collect/bj;->c()I

    move-result v1

    invoke-static {p2, v1}, Lcom/google/common/base/J;->b(II)I

    div-int/lit8 v0, v1, 0x2

    if-lt p2, v0, :cond_0

    invoke-static {p1}, Lcom/google/common/collect/bj;->b(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iput v1, p0, Lcom/google/common/collect/bu;->a:I

    :goto_0
    add-int/lit8 v0, p2, 0x1

    if-ge p2, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/common/collect/bu;->b()Lcom/google/common/collect/bt;

    move p2, v0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    :goto_1
    add-int/lit8 v0, p2, -0x1

    if-lez p2, :cond_1

    invoke-virtual {p0}, Lcom/google/common/collect/bu;->a()Lcom/google/common/collect/bt;

    move p2, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/bt;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method public a(Lcom/google/common/collect/bt;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object p1, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic add(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/common/collect/bt;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bu;->b(Lcom/google/common/collect/bt;)V

    return-void
.end method

.method public b()Lcom/google/common/collect/bt;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/bt;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bu;->a()Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    return v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bu;->b()Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v1, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    :goto_1
    iget-object v0, p0, Lcom/google/common/collect/bu;->e:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    invoke-static {v0, v1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Lcom/google/common/collect/bt;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    goto :goto_1
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/common/collect/bt;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bu;->a(Lcom/google/common/collect/bt;)V

    return-void
.end method
