.class public abstract Lcom/google/common/collect/l;
.super Lcom/google/common/collect/dY;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/common/collect/n;

.field private b:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/common/collect/dY;-><init>()V

    sget-object v0, Lcom/google/common/collect/n;->b:Lcom/google/common/collect/n;

    iput-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    return-void
.end method

.method private c()Z
    .locals 2

    sget-object v0, Lcom/google/common/collect/n;->d:Lcom/google/common/collect/n;

    iput-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    invoke-virtual {p0}, Lcom/google/common/collect/l;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/l;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    sget-object v1, Lcom/google/common/collect/n;->c:Lcom/google/common/collect/n;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/common/collect/n;->a:Lcom/google/common/collect/n;

    iput-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/common/collect/n;->c:Lcom/google/common/collect/n;

    iput-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    sget-object v3, Lcom/google/common/collect/n;->d:Lcom/google/common/collect/n;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    sget-object v0, Lcom/google/common/collect/m;->a:[I

    iget-object v3, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    invoke-virtual {v3}, Lcom/google/common/collect/n;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0}, Lcom/google/common/collect/l;->c()Z

    move-result v2

    :goto_1
    :pswitch_0
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    move v2, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/l;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/n;->b:Lcom/google/common/collect/n;

    iput-object v0, p0, Lcom/google/common/collect/l;->a:Lcom/google/common/collect/n;

    iget-object v0, p0, Lcom/google/common/collect/l;->b:Ljava/lang/Object;

    return-object v0
.end method
