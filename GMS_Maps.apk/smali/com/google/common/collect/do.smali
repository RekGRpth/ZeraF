.class Lcom/google/common/collect/do;
.super Lcom/google/common/collect/aC;
.source "SourceFile"


# instance fields
.field final transient b:Lcom/google/common/collect/dn;


# direct methods
.method constructor <init>(Lcom/google/common/collect/dn;)V
    .locals 1

    invoke-static {p1}, Lcom/google/common/collect/dn;->a(Lcom/google/common/collect/dn;)[Lcom/google/common/collect/dq;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/common/collect/aC;-><init>([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/common/collect/do;->b:Lcom/google/common/collect/dn;

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/util/Map$Entry;

    iget-object v1, p0, Lcom/google/common/collect/do;->b:Lcom/google/common/collect/dn;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/collect/dn;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
