.class Lcom/google/common/collect/s;
.super Lcom/google/common/collect/da;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Map$Entry;

.field final synthetic b:Lcom/google/common/collect/r;


# direct methods
.method constructor <init>(Lcom/google/common/collect/r;Ljava/util/Map$Entry;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/s;->b:Lcom/google/common/collect/r;

    iput-object p2, p0, Lcom/google/common/collect/s;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, Lcom/google/common/collect/da;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/s;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/s;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/s;->b:Lcom/google/common/collect/r;

    iget-object v0, v0, Lcom/google/common/collect/r;->c:Lcom/google/common/collect/q;

    invoke-static {v0}, Lcom/google/common/collect/q;->a(Lcom/google/common/collect/q;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/common/collect/s;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
