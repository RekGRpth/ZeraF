.class Lcom/google/common/collect/dp;
.super Lcom/google/common/collect/aE;
.source "SourceFile"


# instance fields
.field final d:Lcom/google/common/collect/dn;


# direct methods
.method constructor <init>(Lcom/google/common/collect/dn;)V
    .locals 2

    invoke-static {p1}, Lcom/google/common/collect/dn;->a(Lcom/google/common/collect/dn;)[Lcom/google/common/collect/dq;

    move-result-object v0

    invoke-static {p1}, Lcom/google/common/collect/dn;->b(Lcom/google/common/collect/dn;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/common/collect/aE;-><init>([Ljava/lang/Object;I)V

    iput-object p1, p0, Lcom/google/common/collect/dp;->d:Lcom/google/common/collect/dn;

    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/dp;->a(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/util/Map$Entry;)Ljava/lang/Object;
    .locals 1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dp;->d:Lcom/google/common/collect/dn;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dn;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
