.class Lcom/google/common/collect/i;
.super Lcom/google/common/collect/aj;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Set;

.field final synthetic b:Lcom/google/common/collect/a;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/a;)V
    .locals 1

    iput-object p1, p0, Lcom/google/common/collect/i;->b:Lcom/google/common/collect/a;

    invoke-direct {p0}, Lcom/google/common/collect/aj;-><init>()V

    iget-object v0, p0, Lcom/google/common/collect/i;->b:Lcom/google/common/collect/a;

    invoke-static {v0}, Lcom/google/common/collect/a;->b(Lcom/google/common/collect/a;)Lcom/google/common/collect/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/a;->keySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/i;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/a;Lcom/google/common/collect/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/i;-><init>(Lcom/google/common/collect/a;)V

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/i;->a:Ljava/util/Set;

    return-object v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/i;->b:Lcom/google/common/collect/a;

    invoke-static {v0}, Lcom/google/common/collect/a;->a(Lcom/google/common/collect/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/j;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/j;-><init>(Lcom/google/common/collect/i;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/i;->e()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/i;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/i;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
