.class public final Lcom/google/common/collect/bE;
.super Lcom/google/common/collect/ak;
.source "SourceFile"


# instance fields
.field b:Z

.field c:I

.field d:I

.field e:I

.field f:Lcom/google/common/collect/cz;

.field g:Lcom/google/common/collect/cz;

.field h:J

.field i:J

.field j:Lcom/google/common/collect/bH;

.field k:Lcom/google/common/base/t;

.field l:Lcom/google/common/base/t;

.field m:Lcom/google/common/base/ae;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/common/collect/ak;-><init>()V

    iput v0, p0, Lcom/google/common/collect/bE;->c:I

    iput v0, p0, Lcom/google/common/collect/bE;->d:I

    iput v0, p0, Lcom/google/common/collect/bE;->e:I

    iput-wide v1, p0, Lcom/google/common/collect/bE;->h:J

    iput-wide v1, p0, Lcom/google/common/collect/bE;->i:J

    return-void
.end method


# virtual methods
.method b()Lcom/google/common/base/t;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bE;->k:Lcom/google/common/base/t;

    invoke-virtual {p0}, Lcom/google/common/collect/bE;->f()Lcom/google/common/collect/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/cz;->a()Lcom/google/common/base/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/t;

    return-object v0
.end method

.method c()Lcom/google/common/base/t;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bE;->l:Lcom/google/common/base/t;

    invoke-virtual {p0}, Lcom/google/common/collect/bE;->g()Lcom/google/common/collect/cz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/cz;->a()Lcom/google/common/base/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/t;

    return-object v0
.end method

.method d()I
    .locals 2

    iget v0, p0, Lcom/google/common/collect/bE;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/common/collect/bE;->c:I

    goto :goto_0
.end method

.method e()I
    .locals 2

    iget v0, p0, Lcom/google/common/collect/bE;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/common/collect/bE;->d:I

    goto :goto_0
.end method

.method f()Lcom/google/common/collect/cz;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bE;->f:Lcom/google/common/collect/cz;

    sget-object v1, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cz;

    return-object v0
.end method

.method g()Lcom/google/common/collect/cz;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bE;->g:Lcom/google/common/collect/cz;

    sget-object v1, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cz;

    return-object v0
.end method

.method h()J
    .locals 4

    iget-wide v0, p0, Lcom/google/common/collect/bE;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/common/collect/bE;->h:J

    goto :goto_0
.end method

.method i()J
    .locals 4

    iget-wide v0, p0, Lcom/google/common/collect/bE;->i:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/common/collect/bE;->i:J

    goto :goto_0
.end method

.method j()Lcom/google/common/base/ae;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/bE;->m:Lcom/google/common/base/ae;

    invoke-static {}, Lcom/google/common/base/ae;->b()Lcom/google/common/base/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/ae;

    return-object v0
.end method

.method public k()Ljava/util/concurrent/ConcurrentMap;
    .locals 4

    iget-boolean v0, p0, Lcom/google/common/collect/bE;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lcom/google/common/collect/bE;->d()I

    move-result v1

    const/high16 v2, 0x3f400000

    invoke-virtual {p0}, Lcom/google/common/collect/bE;->e()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/bE;->j:Lcom/google/common/collect/bH;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/common/collect/bP;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bP;-><init>(Lcom/google/common/collect/bE;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/common/collect/bG;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bG;-><init>(Lcom/google/common/collect/bE;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v3, -0x1

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    iget v1, p0, Lcom/google/common/collect/bE;->c:I

    if-eq v1, v3, :cond_0

    const-string v1, "initialCapacity"

    iget v2, p0, Lcom/google/common/collect/bE;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    :cond_0
    iget v1, p0, Lcom/google/common/collect/bE;->d:I

    if-eq v1, v3, :cond_1

    const-string v1, "concurrencyLevel"

    iget v2, p0, Lcom/google/common/collect/bE;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    :cond_1
    iget v1, p0, Lcom/google/common/collect/bE;->e:I

    if-eq v1, v3, :cond_2

    const-string v1, "maximumSize"

    iget v2, p0, Lcom/google/common/collect/bE;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    :cond_2
    iget-wide v1, p0, Lcom/google/common/collect/bE;->h:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_3

    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lcom/google/common/collect/bE;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_3
    iget-wide v1, p0, Lcom/google/common/collect/bE;->i:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_4

    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lcom/google/common/collect/bE;->i:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_4
    iget-object v1, p0, Lcom/google/common/collect/bE;->f:Lcom/google/common/collect/cz;

    if-eqz v1, :cond_5

    const-string v1, "keyStrength"

    iget-object v2, p0, Lcom/google/common/collect/bE;->f:Lcom/google/common/collect/cz;

    invoke-virtual {v2}, Lcom/google/common/collect/cz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_5
    iget-object v1, p0, Lcom/google/common/collect/bE;->g:Lcom/google/common/collect/cz;

    if-eqz v1, :cond_6

    const-string v1, "valueStrength"

    iget-object v2, p0, Lcom/google/common/collect/bE;->g:Lcom/google/common/collect/cz;

    invoke-virtual {v2}, Lcom/google/common/collect/cz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_6
    iget-object v1, p0, Lcom/google/common/collect/bE;->k:Lcom/google/common/base/t;

    if-eqz v1, :cond_7

    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_7
    iget-object v1, p0, Lcom/google/common/collect/bE;->l:Lcom/google/common/base/t;

    if-eqz v1, :cond_8

    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_8
    iget-object v1, p0, Lcom/google/common/collect/bE;->a:Lcom/google/common/collect/bN;

    if-eqz v1, :cond_9

    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    :cond_9
    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
