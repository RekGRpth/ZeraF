.class Lcom/google/common/collect/aO;
.super Lcom/google/common/collect/ImmutableSet;
.source "SourceFile"


# instance fields
.field final transient a:Lcom/google/common/collect/aK;


# direct methods
.method constructor <init>(Lcom/google/common/collect/aK;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/ImmutableSet;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/aO;->a:Lcom/google/common/collect/aK;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aO;->a:Lcom/google/common/collect/aK;

    invoke-virtual {v0}, Lcom/google/common/collect/aK;->d()Z

    move-result v0

    return v0
.end method

.method public b()Lcom/google/common/collect/dY;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aO;->a:Lcom/google/common/collect/aK;

    iget-object v0, v0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/util/Map$Entry;

    iget-object v1, p0, Lcom/google/common/collect/aO;->a:Lcom/google/common/collect/aK;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/collect/aK;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aO;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aO;->a:Lcom/google/common/collect/aK;

    invoke-virtual {v0}, Lcom/google/common/collect/aK;->size()I

    move-result v0

    return v0
.end method
