.class final Lcom/google/common/base/h;
.super Lcom/google/common/base/e;
.source "SourceFile"


# instance fields
.field final synthetic o:[C


# direct methods
.method constructor <init>([C)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/base/h;->o:[C

    invoke-direct {p0}, Lcom/google/common/base/e;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lcom/google/common/base/r;)V
    .locals 4

    iget-object v1, p0, Lcom/google/common/base/h;->o:[C

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-char v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/common/base/r;->a(C)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lcom/google/common/base/e;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public b(C)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/base/h;->o:[C

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([CC)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
