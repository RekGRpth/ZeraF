.class public Lcom/google/googlenav/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lam/f;

.field private final b:Lcom/google/googlenav/ui/bs;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lam/f;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    iput-object p2, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    iput-object p1, p0, Lcom/google/googlenav/ay;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    iput-boolean v3, p0, Lcom/google/googlenav/ay;->d:Z

    return-void
.end method


# virtual methods
.method public a()Lam/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 8

    const/4 v7, -0x1

    const/4 v6, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x55

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    return-object v1

    :cond_0
    if-ne p1, v7, :cond_2

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const/16 v0, 0x1000

    :try_start_0
    new-array v0, v0, [B

    :goto_1
    invoke-virtual {v3, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    if-eq v4, v7, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    throw v0

    :cond_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-static {v0}, Lbm/a;->a(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-static {v2, v0, p1}, Lbm/a;->a(Ljava/lang/String;II)[B

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ay;->d:Z

    return-void
.end method

.method public b()Lcom/google/googlenav/ui/bs;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ay;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ay;->d:Z

    return v0
.end method
