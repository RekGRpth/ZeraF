.class public Lcom/google/googlenav/networkinitiated/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/h;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/networkinitiated/c;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/networkinitiated/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/c;->h()V

    return-void
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    iget-object v2, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {v2, v4, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private h()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    iget-object v2, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {v2, v4, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "sender"

    const-string v2, "gmobilemaps@gmail.com"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public C_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/c;->h()V

    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    const-string v1, "network_initiated_prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "registration_id"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/networkinitiated/j;

    const/4 v4, 0x2

    invoke-direct {v3, v1, v4, v5}, Lcom/google/googlenav/networkinitiated/j;-><init>(Ljava/lang/String;ILcom/google/googlenav/networkinitiated/k;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/c;->g()V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "registration_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lal/b;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/networkinitiated/j;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/googlenav/networkinitiated/l;

    iget-object v4, p0, Lcom/google/googlenav/networkinitiated/c;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/googlenav/networkinitiated/l;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, p1, v2, v3}, Lcom/google/googlenav/networkinitiated/j;-><init>(Ljava/lang/String;ILcom/google/googlenav/networkinitiated/k;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public e()V
    .locals 2

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/networkinitiated/d;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/d;-><init>(Lcom/google/googlenav/networkinitiated/c;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    return-void
.end method
