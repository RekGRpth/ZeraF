.class public Lcom/google/googlenav/networkinitiated/j;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/googlenav/networkinitiated/k;

.field private final c:I

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/google/googlenav/networkinitiated/k;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/networkinitiated/j;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/networkinitiated/j;->b:Lcom/google/googlenav/networkinitiated/k;

    iput p2, p0, Lcom/google/googlenav/networkinitiated/j;->c:I

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/googlenav/networkinitiated/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/googlenav/networkinitiated/j;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bK;->K:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    const/4 v1, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->L:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/networkinitiated/j;->d:I

    return v1
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x5b

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/j;->b:Lcom/google/googlenav/networkinitiated/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/j;->b:Lcom/google/googlenav/networkinitiated/k;

    iget v1, p0, Lcom/google/googlenav/networkinitiated/j;->d:I

    iget-object v2, p0, Lcom/google/googlenav/networkinitiated/j;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/networkinitiated/k;->a(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
