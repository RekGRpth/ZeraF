.class public Lcom/google/googlenav/by;
.super Lcom/google/googlenav/ai;
.source "SourceFile"


# instance fields
.field private final c:Z


# direct methods
.method public constructor <init>(LaR/D;)V
    .locals 2

    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;)V

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->d(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(B)V

    invoke-virtual {p1}, LaR/D;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(Ljava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    return-void
.end method

.method public constructor <init>(LaR/D;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->d(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(B)V

    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->c(LaN/g;)V

    :cond_0
    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->g(Ljava/lang/String;)V

    invoke-virtual {p1}, LaR/D;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(Ljava/util/List;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    return-void
.end method


# virtual methods
.method public f()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    return v0
.end method
