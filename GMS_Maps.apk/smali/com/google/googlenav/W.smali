.class public Lcom/google/googlenav/W;
.super Lcom/google/googlenav/ai;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:J

.field private m:Ljava/lang/String;

.field private n:J

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-boolean v2, p0, Lcom/google/googlenav/W;->k:Z

    iput v1, p0, Lcom/google/googlenav/W;->o:I

    iput-object p2, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    iput v1, p0, Lcom/google/googlenav/W;->f:I

    iput v1, p0, Lcom/google/googlenav/W;->g:I

    iput v1, p0, Lcom/google/googlenav/W;->h:I

    iput v2, p0, Lcom/google/googlenav/W;->i:I

    iput v2, p0, Lcom/google/googlenav/W;->j:I

    iput-boolean v2, p0, Lcom/google/googlenav/W;->k:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/W;->l:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/googlenav/W;->n:J

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/layer/o;Lcom/google/googlenav/layer/j;)V
    .locals 4

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->a()Ljava/lang/String;

    move-result-object v0

    const-string v3, "msid:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->e()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;ILjava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/W;->k:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/W;->o:I

    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/layer/o;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/W;->f:I

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->e()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/W;->g:I

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->f()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/W;->h:I

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->b()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/W;->i:I

    invoke-virtual {p2}, Lcom/google/googlenav/layer/j;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/W;->j:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/W;->k:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/W;->l:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/googlenav/W;->n:J

    return-void

    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p3, p4}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/google/googlenav/W;->k:Z

    iput v1, p0, Lcom/google/googlenav/W;->o:I

    iput-object p1, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    iput v1, p0, Lcom/google/googlenav/W;->f:I

    iput v1, p0, Lcom/google/googlenav/W;->g:I

    iput v1, p0, Lcom/google/googlenav/W;->h:I

    iput v2, p0, Lcom/google/googlenav/W;->i:I

    iput v2, p0, Lcom/google/googlenav/W;->j:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/W;->k:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/W;->l:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/googlenav/W;->n:J

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/W;
    .locals 1

    new-instance v0, Lcom/google/googlenav/W;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/W;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->f:I

    return-void
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/googlenav/W;->l:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/W;->k:Z

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->g:I

    return-void
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/googlenav/W;->n:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/W;->m:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/W;->q:Z

    return-void
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->h:I

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/W;->p:Ljava/lang/String;

    return-void
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->i:I

    return-void
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->j:I

    return-void
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/W;->o:I

    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    const/16 v0, 0x35

    iget-object v1, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/W;->m:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/W;->m:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "LayerPlacemark"

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-super {p0}, Lcom/google/googlenav/ai;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/W;->d:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/W;->e:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->f:I

    return v0
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->g:I

    return v0
.end method

.method public n()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->h:I

    return v0
.end method

.method public o()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->i:I

    return v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->j:I

    return v0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/W;->k:Z

    return v0
.end method

.method public r()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/W;->l:J

    return-wide v0
.end method

.method public s()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/W;->n:J

    return-wide v0
.end method

.method public t()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/W;->o:I

    return v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/W;->p:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "l="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/googlenav/W;->t()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "z="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/W;->t()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/W;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/W;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/W;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public w()Z
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ai;->w()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/W;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/W;->c:Ljava/lang/String;

    const-string v1, "TransitStations"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
