.class public Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Lcom/google/googlenav/android/aa;

.field private c:Lcom/google/googlenav/ui/view/n;

.field private d:Lcom/google/googlenav/ui/view/o;

.field private e:Landroid/view/animation/Animation;

.field private f:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->setClickable(Z)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->setFocusable(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;)Lcom/google/googlenav/ui/view/n;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->c:Lcom/google/googlenav/ui/view/n;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    const v0, 0x7f1000c2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 1

    const v0, 0x7f1000c3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(I)Landroid/view/animation/Animation;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const p1, 0x7f0f0022

    :cond_0
    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/m;-><init>(Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->d:Lcom/google/googlenav/ui/view/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->d:Lcom/google/googlenav/ui/view/o;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/o;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->e:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->e:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    new-instance v1, Lcom/google/googlenav/ui/view/l;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->b:Lcom/google/googlenav/android/aa;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/googlenav/ui/view/l;-><init>(Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;Las/c;Lcom/google/googlenav/android/aa;Z)V

    iget v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->a:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lbm/j;->a(J)V

    invoke-virtual {v1}, Lbm/j;->g()V

    :cond_2
    return-void
.end method

.method public a(Lcom/google/googlenav/android/aa;Ljava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, -0x1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->b:Lcom/google/googlenav/android/aa;

    iput-object p7, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->c:Lcom/google/googlenav/ui/view/n;

    iput-object p8, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->d:Lcom/google/googlenav/ui/view/o;

    iput p6, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->a:I

    if-ne p4, v2, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->e:Landroid/view/animation/Animation;

    if-ne p5, v2, :cond_2

    :goto_1
    iput-object v1, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->f:Landroid/view/animation/Animation;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->a(Ljava/lang/CharSequence;)V

    sget v0, LaA/c;->a:I

    if-le p3, v0, :cond_0

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->a(I)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/view/k;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/k;-><init>(Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p5}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->b(I)Landroid/view/animation/Animation;

    move-result-object v1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->f:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->f:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;->setVisibility(I)V

    goto :goto_0
.end method
