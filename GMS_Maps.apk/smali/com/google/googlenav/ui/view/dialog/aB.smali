.class Lcom/google/googlenav/ui/view/dialog/aB;
.super Lvedroid/support/v4/view/x;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/ax;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/dialog/ax;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-direct {p0}, Lvedroid/support/v4/view/x;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/view/dialog/ax;Lcom/google/googlenav/ui/view/dialog/ay;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/aB;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 3

    check-cast p1, Lcom/google/googlenav/ui/view/dialog/aC;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/dialog/aC;->a(Lcom/google/googlenav/ui/view/dialog/aC;)Lcom/google/googlenav/friend/history/O;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/history/O;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/history/O;->a(Landroid/view/View;)V

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public a(I)Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aB;->a()I

    move-result v0

    if-lt p1, v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/O;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ax;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/O;->a(Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/aC;-><init>(Lcom/google/googlenav/friend/history/O;Landroid/view/View;)V

    return-object v2
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    check-cast p1, Lvedroid/support/v4/view/ViewPager;

    check-cast p3, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-static {p3}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lvedroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
