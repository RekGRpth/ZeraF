.class public Lcom/google/googlenav/ui/view/dialog/aX;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/dialog/aw;

.field b:Landroid/widget/TextView;

.field c:Lcom/google/googlenav/ui/android/ai;

.field private final d:Lbf/bU;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Lbf/bU;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/aX;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/aX;)Lbf/bU;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    return-object v0
.end method


# virtual methods
.method protected O_()V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/dialog/aX;->requestWindowFeature(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setUiOptions(I)V

    goto :goto_0
.end method

.method a(Lcom/google/googlenav/F;)Ljava/util/List;
    .locals 7

    const/16 v6, 0xfa1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v4

    if-lez v4, :cond_0

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {p1, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lbj/Y;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v2, v6}, Lbj/Y;-><init>(Lcom/google/googlenav/bZ;III)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v3

    :cond_2
    check-cast v0, Lcom/google/googlenav/cp;

    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    new-instance v5, Lbj/Y;

    invoke-direct {v5, v0, v1, v2, v6}, Lbj/Y;-><init>(Lcom/google/googlenav/bZ;III)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, -0x1

    const v0, 0x7f02021f

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    const/16 v0, 0x5d6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0401be

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f10045c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ai;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v3}, Lbf/bU;->a()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ai;Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v1, 0x13

    invoke-direct {v3, v5, v5, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {p1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LaA/h;->a(Landroid/app/ActionBar;Landroid/view/View;Landroid/app/ActionBar$LayoutParams;LaA/g;Landroid/content/Context;)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1004b4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lbf/bU;->a(IILjava/lang/Object;)Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->a(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Landroid/view/View;
    .locals 10

    const/4 v6, 0x0

    const/16 v5, 0x5d6

    const/4 v8, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0400fe

    invoke-virtual {v0, v1, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->as()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aX;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v0}, Lbf/bU;->bH()Z

    move-result v5

    const v0, 0x7f10019b

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->n()V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aw;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v2}, Lbf/bU;->bG()Lcom/google/googlenav/ui/view/u;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v6, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v6}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/dialog/aX;->a(Lcom/google/googlenav/F;)Ljava/util/List;

    move-result-object v6

    const v7, 0x7f100026

    invoke-virtual {v9, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/dialog/aw;-><init>(Lbf/i;Lcom/google/googlenav/ui/view/u;Landroid/view/LayoutInflater;Landroid/content/Context;ZLjava/util/List;Landroid/widget/ListView;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->a:Lcom/google/googlenav/ui/view/dialog/aw;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    return-object v9

    :cond_0
    const v1, 0x7f1002dd

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    const v2, 0x7f1001b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f1001b1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f02021f

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v1

    invoke-virtual {v1, v2, v6}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    const v2, 0x7f10001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    const v2, 0x7f100205

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/android/ai;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/aY;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/aY;-><init>(Lcom/google/googlenav/ui/view/dialog/aX;)V

    invoke-virtual {v1, v2, v3}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ai;Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    const v2, 0x7f1002de

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f0401b3

    invoke-virtual {v0, v2, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100433

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aZ;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/aZ;-><init>(Lcom/google/googlenav/ui/view/dialog/aX;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->l:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->c:Lcom/google/googlenav/ui/android/ai;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ai;->a()V

    :cond_0
    return-void
.end method

.method public l()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->a:Lcom/google/googlenav/ui/view/dialog/aw;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v1}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/dialog/aX;->a(Lcom/google/googlenav/F;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v2}, Lbf/bU;->bH()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/aw;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->a:Lcom/google/googlenav/ui/view/dialog/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->a:Lcom/google/googlenav/ui/view/dialog/aw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aw;->a()V

    :cond_0
    return-void
.end method

.method public n()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->d:Lbf/bU;

    invoke-virtual {v0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aX;->b:Landroid/widget/TextView;

    const/16 v2, 0x5cc

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aX;->b:Landroid/widget/TextView;

    const/16 v1, 0x5cd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/aX;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110017

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x2af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    goto :goto_0
.end method
