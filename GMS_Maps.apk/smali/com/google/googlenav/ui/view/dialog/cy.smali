.class public abstract Lcom/google/googlenav/ui/view/dialog/cy;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/Integer;

.field protected n:Landroid/webkit/WebView;

.field protected o:Landroid/webkit/WebViewClient;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;ILjava/lang/CharSequence;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/cy;->a:Ljava/lang/CharSequence;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->b:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;Ljava/lang/CharSequence;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/cy;->a:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/cy;->b:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->a:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f100014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->o:Landroid/webkit/WebViewClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cy;->o:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->requestFocus(I)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cz;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/cz;-><init>(Lcom/google/googlenav/ui/view/dialog/cy;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/webkit/WebViewClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cy;->o:Landroid/webkit/WebViewClient;

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cy;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cy;->l()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cy;->a(Landroid/view/View;)V

    return-object v0
.end method

.method protected l()I
    .locals 1

    const v0, 0x7f0401d2

    return v0
.end method

.method public o()Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cy;->n:Landroid/webkit/WebView;

    return-object v0
.end method

.method protected v()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    return-void
.end method
