.class public Lcom/google/googlenav/ui/view/dialog/cj;
.super Lcom/google/googlenav/ui/view/dialog/cc;
.source "SourceFile"


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final l:I

.field private final m:Ljava/lang/String;

.field private n:Z


# direct methods
.method public constructor <init>(IIIILjava/lang/String;Lcom/google/googlenav/ui/wizard/jb;)V
    .locals 1

    invoke-direct {p0, p6}, Lcom/google/googlenav/ui/view/dialog/cc;-><init>(Lcom/google/googlenav/ui/wizard/jb;)V

    iput p1, p0, Lcom/google/googlenav/ui/view/dialog/cj;->b:I

    iput p2, p0, Lcom/google/googlenav/ui/view/dialog/cj;->c:I

    iput p3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->d:I

    iput p4, p0, Lcom/google/googlenav/ui/view/dialog/cj;->l:I

    invoke-static {p5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->n:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->n:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x54e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->m:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iput-object p5, p0, Lcom/google/googlenav/ui/view/dialog/cj;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Landroid/view/View;II)Z
    .locals 7

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f1003f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1003f2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v4, 0x549

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x54a

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v4, v5}, Lcom/google/googlenav/ui/wizard/jb;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v3

    :goto_0
    if-eqz v4, :cond_2

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/16 v0, 0x547

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v5, 0x548

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3, v0, v5}, Lcom/google/googlenav/ui/wizard/jb;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v3

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    if-nez v4, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v3

    :goto_4
    return v0

    :cond_1
    move v4, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4
.end method

.method private h()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->d:I

    iget v3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->l:I

    add-int/2addr v0, v3

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->b:I

    iget v4, p0, Lcom/google/googlenav/ui/view/dialog/cj;->c:I

    add-int/2addr v3, v4

    if-lez v3, :cond_1

    move v3, v1

    :goto_1
    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->n:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_2

    const/16 v0, 0x561

    :goto_2
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    const/16 v0, 0x55a

    goto :goto_2

    :cond_3
    if-eqz v0, :cond_4

    const/16 v0, 0x560

    :goto_4
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    const/16 v0, 0x559

    goto :goto_4

    :cond_5
    if-eqz v0, :cond_6

    const/16 v0, 0x55d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 11

    const/16 v10, 0x8

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cj;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04017d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v0, 0x7f1003f3

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100403

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f100405

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f100404

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v3, 0x7f100406

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f100407

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v4, 0x7f100408

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/16 v8, 0x562

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Lcom/google/googlenav/ui/view/dialog/cj;->a(Landroid/view/View;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/cj;->h()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->b:I

    iget v8, p0, Lcom/google/googlenav/ui/view/dialog/cj;->c:I

    invoke-direct {p0, v6, v0, v8}, Lcom/google/googlenav/ui/view/dialog/cj;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x55f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->m:Ljava/lang/String;

    sget-object v1, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    :goto_1
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->d:I

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/cj;->l:I

    invoke-direct {p0, v7, v0, v1}, Lcom/google/googlenav/ui/view/dialog/cj;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x55e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/cj;->n:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x55c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const v0, 0x7f100409

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x558

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ck;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ck;-><init>(Lcom/google/googlenav/ui/view/dialog/cj;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v5

    :cond_0
    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    sget-object v9, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v8, v9}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    const/16 v0, 0x55b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/cj;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
