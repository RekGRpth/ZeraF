.class public Lcom/google/googlenav/ui/view/dialog/aR;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/dialog/aW;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/googlenav/ui/view/dialog/aV;

.field private final l:Lcom/google/googlenav/friend/af;

.field private final m:LaB/s;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/aW;ILjava/lang/String;Lcom/google/googlenav/ui/view/dialog/aV;Lcom/google/googlenav/friend/af;LaB/s;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    iput p2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->b:I

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/aR;->d:Lcom/google/googlenav/ui/view/dialog/aV;

    iput-object p5, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    iput-object p6, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/aR;)LaB/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/aR;)Lcom/google/googlenav/ui/view/dialog/aV;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->d:Lcom/google/googlenav/ui/view/dialog/aV;

    return-object v0
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->onBackPressed()V

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->b:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 7

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e9

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f100039

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f100092

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const v0, 0x7f1001bd

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/google/googlenav/ui/bs;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/bi;->e(I)I

    move-result v3

    invoke-direct {v2, v1, v3}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aS;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/googlenav/ui/view/dialog/aS;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;Landroid/widget/ImageView;Lcom/google/googlenav/ui/bs;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    invoke-virtual {v0, v2}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, LaB/p;->Q_()V

    :cond_0
    :goto_1
    const v0, 0x7f1001fe

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100093

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100231

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<head><meta name=\"viewport\" content=\"target-densitydpi=medium-dpi, user-scalable=no, initial-scale=1.0\" /></head><body style=\"padding: 0px; margin:0px;\"><img src=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" style=\"float:right;padding-left: 1em;padding-bottom: 1em;vertical-align:text-top\" />"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/dialog/aW;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</body>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    const-string v1, "file:///android_res/drawable/"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f1002be

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aT;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/aT;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aU;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aU;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aR;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-object v6

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->w_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aR;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    invoke-virtual {v0, v2, v1}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto/16 :goto_1

    :cond_3
    const v0, 0x7f1002bf

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<head><meta name=\"viewport\" content=\"target-densitydpi=medium-dpi, user-scalable=no, initial-scale=1.0\" /></head><body style=\"padding: 0px; margin:0px;\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/dialog/aW;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</body>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aW;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
