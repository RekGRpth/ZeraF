.class public Lcom/google/googlenav/ui/view/dialog/a;
.super Lcom/google/googlenav/ui/view/dialog/cy;
.source "SourceFile"


# static fields
.field private static p:Lcom/google/googlenav/ui/android/aF;


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/TextView;

.field d:Landroid/view/View;

.field l:Landroid/widget/ImageView;

.field m:Landroid/widget/ScrollView;

.field private q:Lcom/google/googlenav/ai;

.field private r:Lbf/m;

.field private s:Landroid/widget/ProgressBar;

.field private t:J

.field private final u:Lcom/google/googlenav/common/a;

.field private v:Z

.field private w:Z

.field private x:Landroid/view/View$OnFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/a;->p:Lcom/google/googlenav/ui/android/aF;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;ZZ)V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/dialog/a;-><init>(Lcom/google/googlenav/ai;Lbf/m;ZZLcom/google/googlenav/common/a;)V

    return-void
.end method

.method constructor <init>(Lcom/google/googlenav/ai;Lbf/m;ZZLcom/google/googlenav/common/a;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, v0}, Lcom/google/googlenav/ui/view/dialog/cy;-><init>(Lcom/google/googlenav/ui/e;Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->t:J

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/b;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->x:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    iput-object p5, p0, Lcom/google/googlenav/ui/view/dialog/a;->u:Lcom/google/googlenav/common/a;

    iput-boolean p3, p0, Lcom/google/googlenav/ui/view/dialog/a;->v:Z

    iput-boolean p4, p0, Lcom/google/googlenav/ui/view/dialog/a;->w:Z

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/c;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/webkit/WebViewClient;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/a;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private a(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/j;->a:Lcom/google/googlenav/ui/view/dialog/j;

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/j;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/j;->a:Lcom/google/googlenav/ui/view/dialog/j;

    invoke-static {p2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/j;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/j;->a:Lcom/google/googlenav/ui/view/dialog/j;

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/j;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/j;->a:Lcom/google/googlenav/ui/view/dialog/j;

    invoke-static {p2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/j;)V

    goto :goto_0
.end method

.method static a(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/j;)V
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/j;->a:Lcom/google/googlenav/ui/view/dialog/j;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v1, v2}, Ljava/text/Bidi;->requiresBidi([CII)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/j;->c:Lcom/google/googlenav/ui/view/dialog/j;

    if-ne p2, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x200f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x200e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/a;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/a;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/a;)Lbf/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->s:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->s:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/a;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->s:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/dialog/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/a;->w()V

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/dialog/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/a;->x()V

    return-void
.end method

.method private w()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lai/a;->a(Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    const/16 v1, 0x10

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    return-void
.end method

.method private x()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lai/a;->a(Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    const/16 v1, 0x25b

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/a;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method a(Landroid/webkit/WebView;IIIFF)I
    .locals 3

    if-le p2, p3, :cond_2

    sub-int v0, p2, p3

    int-to-float v0, v0

    div-float v1, p6, p5

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, p3

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p6

    float-to-int v1, v1

    add-int v2, v0, p4

    if-le v2, v1, :cond_0

    add-int v0, p3, v1

    sub-int/2addr v0, p4

    :cond_0
    if-gez v0, :cond_1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, p2

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cy;->a(Landroid/app/ActionBar;)V

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->h()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cy;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lai/a;->a(Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setScrollContainer(Z)V

    const v0, 0x7f10000e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->d:Landroid/view/View;

    const v0, 0x7f10000f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->a:Landroid/widget/ImageView;

    const v0, 0x7f100010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->b:Landroid/widget/ImageView;

    const v0, 0x7f100012

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->l:Landroid/widget/ImageView;

    const v0, 0x7f100011

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->c:Landroid/widget/TextView;

    const v0, 0x7f100013

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->s:Landroid/widget/ProgressBar;

    const v0, 0x7f10000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->m:Landroid/widget/ScrollView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/a;->a:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->b:Landroid/widget/ImageView;

    const v1, 0x7f0202d3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->l:Landroid/widget/ImageView;

    const v1, 0x7f0202e2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->b:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/e;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/e;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->l:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/f;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/f;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/a;->x:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->s:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/g;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/g;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->m()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->a:Landroid/widget/ImageView;

    const v1, 0x7f0202ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/a;->x()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/a;->w()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f100499
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()Landroid/view/View;
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/cy;->c()Landroid/view/View;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100017

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100018

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Landroid/widget/TextView;)V

    const v0, 0x7f10000c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10000b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/googlenav/ui/view/dialog/a;->v:Z

    if-eqz v3, :cond_1

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/h;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/h;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->w:Z

    if-eqz v0, :cond_2

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/i;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/i;-><init>(Lcom/google/googlenav/ui/view/dialog/a;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected h()Landroid/view/View;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040002

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const v1, 0x7f100017

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f100018

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {p0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/a;->a(Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-object v0
.end method

.method protected l()I
    .locals 1

    const v0, 0x7f040001

    return v0
.end method

.method m()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->u:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->t:J

    return-void
.end method

.method n()J
    .locals 4

    const-wide/16 v0, -0x1

    iget-wide v2, p0, Lcom/google/googlenav/ui/view/dialog/a;->t:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->u:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/view/dialog/a;->t:J

    sub-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->o()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/cy;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const v5, 0x7f10049a

    const v4, 0x7f100499

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/a;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v2}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const/high16 v3, 0x7f110000

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v2, v3}, Lbf/m;->k(Lcom/google/googlenav/ai;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/a;->r:Lbf/m;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v2, v3}, Lbf/m;->j(Lcom/google/googlenav/ai;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    return v0

    :cond_0
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected onStop()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/a;->q:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/a;->n()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lai/a;->a(Ljava/lang/String;IJ)Z

    return-void
.end method
