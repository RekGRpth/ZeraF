.class public Lcom/google/googlenav/ui/view/dialog/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/android/LoadingFooterView;

.field private final b:Landroid/widget/ListView;

.field private final c:Lbf/i;

.field private final d:Lcom/google/googlenav/ui/view/android/J;

.field private e:Lcom/google/googlenav/ui/view/u;


# direct methods
.method public constructor <init>(Lbf/i;Lcom/google/googlenav/ui/view/u;Landroid/view/LayoutInflater;Landroid/content/Context;ZLjava/util/List;Landroid/widget/ListView;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aw;->c:Lbf/i;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/aw;->e:Lcom/google/googlenav/ui/view/u;

    iput-object p7, p0, Lcom/google/googlenav/ui/view/dialog/aw;->b:Landroid/widget/ListView;

    const v0, 0x7f0400a4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p7, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/LoadingFooterView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    const v1, 0x7f100232

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/LoadingFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x25d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {p0, p5}, Lcom/google/googlenav/ui/view/dialog/aw;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {p7, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    invoke-virtual {p0, p4, p8, p6}, Lcom/google/googlenav/ui/view/dialog/aw;->a(Landroid/content/Context;ILjava/util/List;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->d:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aw;->a()V

    const/4 v0, 0x1

    invoke-virtual {p7, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;ILjava/util/List;)Lcom/google/googlenav/ui/view/android/J;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aw;->c:Lbf/i;

    invoke-direct {v0, p1, v1, p3, p2}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    return-object v0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aw;->c:Lbf/i;

    invoke-virtual {v1}, Lbf/i;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/dialog/aw;->a(Z)V

    return-void
.end method

.method protected a(Z)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/16 v3, 0xf

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aw;->e:Lcom/google/googlenav/ui/view/u;

    invoke-virtual {v0, v2, v4}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setViewportListener(Lcom/google/googlenav/ui/view/u;Lcom/google/googlenav/ui/view/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setVisibility(I)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v4, v4}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setViewportListener(Lcom/google/googlenav/ui/view/u;Lcom/google/googlenav/ui/view/a;)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aw;->a:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
