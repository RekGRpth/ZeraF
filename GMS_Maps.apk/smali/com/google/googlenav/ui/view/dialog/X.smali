.class public Lcom/google/googlenav/ui/view/dialog/X;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/android/J;

.field private final b:Lcom/google/googlenav/ui/view/dialog/ah;

.field private final c:Lcom/google/googlenav/friend/history/o;

.field private d:Landroid/view/View;

.field private l:Lbe/o;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/view/dialog/ah;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->w_()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020218

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/CharSequence;II)V

    return-void
.end method

.method private a(Ljava/lang/String;)Lbe/g;
    .locals 2

    new-instance v0, Lbe/g;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ad;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ad;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v0, p1, v1}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/X;)Lcom/google/googlenav/friend/history/o;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/X;)Lcom/google/googlenav/ui/view/dialog/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/X;)Lbe/o;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    return-object v0
.end method

.method private l()Ljava/util/List;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lbj/bv;

    const/4 v2, 0x0

    const v3, 0x7f0400cc

    const/16 v4, 0xdc

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/o;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lbe/n;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/n;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x14e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/String;)Lbe/g;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/o;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lbe/H;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x14f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/String;)Lbe/g;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v1, Lbe/j;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbe/g;

    const/16 v2, 0x284

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/aa;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/aa;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbe/g;

    const/16 v2, 0x28c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/ab;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/ab;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbe/g;

    const/16 v2, 0x288

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/ac;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/ac;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private m()Ljava/util/List;
    .locals 5

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lbe/u;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/u;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbj/bv;

    const/4 v2, 0x0

    const v3, 0x7f0400cc

    const/16 v4, 0x275

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbe/d;

    const/16 v2, 0x286

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020202

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/ae;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/dialog/ae;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3, v4}, Lbe/d;-><init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private n()Ljava/util/List;
    .locals 11

    const v6, 0x7f0400cc

    const/4 v10, 0x7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->m()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v3, Lbj/bv;

    const/16 v4, 0x27c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v6, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lbe/G;

    invoke-direct {v3, v2}, Lbe/G;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lbe/a;

    const/16 v4, 0x27b

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/ui/view/dialog/af;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/view/dialog/af;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v3, v4, v5}, Lbe/a;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lbj/bv;

    const/16 v4, 0x288

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v6, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    array-length v4, v3

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    invoke-virtual {v5, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    new-instance v6, Lbe/A;

    invoke-direct {v6, v5}, Lbe/A;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    new-instance v1, Lbe/d;

    const/16 v2, 0x285

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020202

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/ag;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/dialog/ag;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3, v4}, Lbe/d;-><init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/ah;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    return-void
.end method

.method public b(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/dialog/X;->c(Z)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/J;->insert(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040025

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->d:Landroid/view/View;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/friend/history/W;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    new-instance v3, Lbe/o;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v4}, Lcom/google/googlenav/friend/history/o;->t()Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v5}, Lcom/google/googlenav/friend/history/o;->k()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Lbe/o;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x7

    invoke-direct {v0, v3, v7, v2, v4}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/Y;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/Y;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->u()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/Z;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/Z;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-interface {v0, v2, v3}, Lcom/google/googlenav/ui/view/dialog/ah;->a(Ljava/lang/String;Lcom/google/googlenav/ui/wizard/bj;)V

    return-object v1
.end method

.method c(Z)Ljava/util/List;
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->l()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->n()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->p()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/X;->c(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/ah;->a()V

    :cond_0
    return-void
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0xdc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
