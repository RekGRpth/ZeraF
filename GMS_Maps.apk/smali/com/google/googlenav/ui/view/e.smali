.class public Lcom/google/googlenav/ui/view/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/Object;

.field private static g:Z

.field private static h:Lcom/google/googlenav/ui/view/e;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/google/googlenav/ui/view/d;

.field private final c:Lcom/google/googlenav/ui/android/BaseAndroidView;

.field private final d:Lcom/google/googlenav/ui/android/ButtonContainer;

.field private e:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/e;->f:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/googlenav/ui/view/e;->g:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/e;->a:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/e;->b:Lcom/google/googlenav/ui/view/d;

    :goto_0
    iput-object p3, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-void

    :cond_0
    new-instance v1, Lcom/google/googlenav/ui/view/android/K;

    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f10004f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidBubbleView;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/view/android/K;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/android/AndroidBubbleView;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/view/e;->b:Lcom/google/googlenav/ui/view/d;

    goto :goto_0
.end method

.method public static a(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    return-object p0
.end method

.method public static a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/c;
    .locals 0

    return-object p0
.end method

.method public static a()Lcom/google/googlenav/ui/view/e;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/e;->h:Lcom/google/googlenav/ui/view/e;

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/view/e;)V
    .locals 0

    sput-object p0, Lcom/google/googlenav/ui/view/e;->h:Lcom/google/googlenav/ui/view/e;

    return-void
.end method

.method private b(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020084

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020088

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020086

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02003f

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020041

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(Lcom/google/googlenav/ui/s;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0, v3, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/R;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/R;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    :cond_0
    const/16 v0, 0xb

    new-instance v1, Lcom/google/googlenav/ui/view/g;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/g;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    const/16 v0, 0x4f8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aW;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/e;->a(Ljava/util/List;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0xc

    new-instance v1, Lcom/google/googlenav/ui/view/h;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/h;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    :cond_1
    const/16 v0, 0xd

    new-instance v1, Lcom/google/googlenav/ui/view/i;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/i;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    const/16 v0, 0xe

    new-instance v1, Lcom/google/googlenav/ui/view/j;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/j;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    return-void
.end method


# virtual methods
.method public a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;
    .locals 11

    const/16 v1, 0x1aa

    const/16 v0, 0xf7

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, -0x1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown touchscreen button type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v5, 0x7f10006c

    const v1, 0x7f02008f

    const/16 v0, 0x623

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    :goto_0
    packed-switch p1, :pswitch_data_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    move-object v1, v0

    :goto_1
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-nez v9, :cond_2

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :pswitch_2
    const v5, 0x7f10006b

    const v1, 0x7f02008b

    const/16 v0, 0x624

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_3
    const v5, 0x7f100067

    const v1, 0x7f02007b

    const/16 v0, 0x3e8

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_4
    const v5, 0x7f100069

    const v1, 0x7f020071

    const/16 v0, 0x307

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_5
    const v5, 0x7f100070

    const v1, 0x7f02002d

    const/16 v0, 0x58

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_6
    const v5, 0x7f10006e

    const v1, 0x7f020060

    const/16 v0, 0x2b2

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_7
    const v5, 0x7f10006f

    const v1, 0x7f020061

    const/16 v0, 0x2b3

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto :goto_0

    :pswitch_8
    const v1, 0x7f100204

    const/16 v0, 0x4f6

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    goto :goto_0

    :pswitch_9
    const v1, 0x7f1001b0

    const/16 v0, 0x1e0

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    goto :goto_0

    :pswitch_a
    const v1, 0x7f100206

    const/16 v0, 0x38d

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    goto/16 :goto_0

    :pswitch_b
    const v1, 0x7f100208

    const/16 v0, 0x231

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    goto/16 :goto_0

    :pswitch_c
    const v1, 0x7f100209

    const/16 v0, 0x2c4

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    goto/16 :goto_0

    :pswitch_d
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/K;->an()Z

    move-result v5

    if-eqz v5, :cond_0

    const v7, 0x7f1002d5

    packed-switch p1, :pswitch_data_2

    const v0, 0x7f02004d

    move v1, v0

    move v0, v2

    :goto_3
    const v6, 0x7f1002d6

    const v5, 0x7f1002d4

    move v8, v1

    move v9, v7

    move v7, v6

    move v6, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_e
    const v1, 0x7f02005c

    const/16 v0, 0x4fa

    goto :goto_3

    :pswitch_f
    const v1, 0x7f020050

    goto :goto_3

    :pswitch_10
    const v0, 0x7f020055

    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_3

    :pswitch_11
    const v0, 0x7f02005e

    move v1, v0

    move v0, v2

    goto :goto_3

    :cond_0
    const v5, 0x7f100066

    packed-switch p1, :pswitch_data_3

    const v1, 0x7f02004d

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v2

    goto/16 :goto_0

    :pswitch_12
    const v1, 0x7f02005a

    const/16 v0, 0x4fa

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_13
    const v1, 0x7f02004e

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_14
    const v0, 0x7f020053

    move v6, v2

    move v7, v2

    move v8, v0

    move v9, v5

    move v5, v1

    goto/16 :goto_0

    :pswitch_15
    const v5, 0x7f100071

    const v1, 0x7f02006e

    const/16 v0, 0x2c4

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_16
    const v5, 0x7f100068

    const v1, 0x7f020078

    const/16 v0, 0x30c

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_17
    const v5, 0x7f100072

    const v1, 0x7f02003d

    const/16 v0, 0xdb

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    goto/16 :goto_0

    :pswitch_18
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :pswitch_19
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    move-object v1, v0

    goto/16 :goto_1

    :cond_2
    if-eq v8, v2, :cond_3

    invoke-virtual {v9, v8}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    if-eq v7, v2, :cond_4

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    if-eq v5, v2, :cond_5

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-static {p2}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/c;

    move-result-object v0

    if-ne v6, v2, :cond_7

    new-instance v2, Lcom/google/googlenav/ui/view/android/R;

    invoke-direct {v2, v9, v0}, Lcom/google/googlenav/ui/view/android/R;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/view/c;)V

    :goto_4
    if-ne p1, v4, :cond_9

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    :goto_5
    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    :goto_6
    move-object v0, v2

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v2, Lcom/google/googlenav/ui/view/android/R;

    invoke-direct {v2, v5, v0}, Lcom/google/googlenav/ui/view/android/R;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/view/c;)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_8
    move v0, v4

    goto :goto_5

    :cond_9
    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0xc

    if-ne p1, v0, :cond_6

    const v0, 0x7f100207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_d
        :pswitch_15
        :pswitch_5
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_19
        :pswitch_1
        :pswitch_1
        :pswitch_18
        :pswitch_1
        :pswitch_1
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xf
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xf
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public a(Lax/b;Lcom/google/googlenav/ui/e;Z)Lcom/google/googlenav/ui/view/d;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/view/android/x;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/googlenav/ui/view/android/x;-><init>(Landroid/view/ViewGroup;Lax/b;Lcom/google/googlenav/ui/e;Z)V

    return-object v0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100209

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/e;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    instance-of v0, v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setCompassMargin(II)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/aW;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f10006a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->G()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x64

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->aq()Lcom/google/googlenav/ui/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bE;->b()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    new-instance v1, Lcom/google/googlenav/ui/view/f;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/f;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/e;->b(Lcom/google/googlenav/ui/s;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    iget-object v0, v0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    if-ne v1, v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    return-object v0
.end method

.method public c()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->i()V

    :cond_0
    return-void
.end method

.method public d()Lcom/google/googlenav/ui/android/ButtonContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-object v0
.end method

.method public e()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public g()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a()V

    return-void
.end method

.method public l()Z
    .locals 3

    const v2, 0x7f020066

    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100071

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v1}, Lcom/google/googlenav/android/A;->k()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_1
    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_2
    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
