.class public Lcom/google/googlenav/ui/view/android/bC;
.super Landroid/app/Dialog;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/B;


# instance fields
.field private final a:Lcom/google/googlenav/android/BaseMapsActivity;

.field private b:Landroid/app/Dialog;

.field private c:Landroid/view/View;

.field private d:Lcom/google/googlenav/ui/e;

.field private e:Ljava/lang/CharSequence;

.field private f:Lcom/google/googlenav/ui/au;

.field private g:LaP/b;

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/BaseMapsActivity;)V
    .locals 1

    const v0, 0x7f0f0015

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    new-instance v0, LaP/b;

    invoke-direct {v0}, LaP/b;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/bC;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->d:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private a(Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110022

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {p1}, LaP/a;->a(Landroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/bC;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method private b(Landroid/view/Menu;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0}, LaP/b;->a()[LaP/c;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4}, LaP/c;->getItemId()I

    move-result v5

    const v6, 0x7f1004d1

    invoke-virtual {v4}, LaP/c;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {p1, v6, v5, v1, v7}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v5

    invoke-virtual {v4}, LaP/c;->isVisible()Z

    move-result v4

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/android/bC;->h:Z

    return-void
.end method

.method private e()Landroid/widget/LinearLayout;
    .locals 5

    const/4 v4, -0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401d6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f1003a5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bC;->e:Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f1000fc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/bD;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/bD;-><init>(Lcom/google/googlenav/ui/view/android/bC;)V

    new-instance v3, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v3, v4, v4}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :cond_0
    new-instance v1, Lcom/google/googlenav/ui/view/android/bE;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/bE;-><init>(Lcom/google/googlenav/ui/view/android/bC;)V

    new-instance v2, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v2, v4, v4}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    return-void
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/google/googlenav/ui/view/android/bC;->a(Landroid/app/Dialog;Landroid/view/View;Lcom/google/googlenav/ui/e;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/app/Dialog;Landroid/view/View;Lcom/google/googlenav/ui/e;Ljava/lang/CharSequence;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/bC;->c:Landroid/view/View;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/bC;->d:Lcom/google/googlenav/ui/e;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/bC;->e:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0}, LaP/b;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    instance-of v0, v0, LT/a;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bC;->h:Z

    const v0, 0x7f1002d7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bC;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1002d8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bC;->c:Landroid/view/View;

    if-nez v2, :cond_2

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bC;->e()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ui/au;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->closeOptionsMenu()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0}, LaP/b;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bC;->h:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 0

    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    const v1, 0x7f10009a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/BaseMapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/bC;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/googlenav/ui/au;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    check-cast v0, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v2, v0, p0, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    instance-of v0, v0, LT/a;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bC;->a(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110021

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {p1}, LaP/a;->a(Landroid/view/Menu;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bC;->b(Landroid/view/Menu;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    invoke-virtual {v0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->onBackPressed()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    invoke-virtual {v0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    invoke-interface {v0, p2}, Lcom/google/googlenav/ui/au;->a(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v1, :cond_2

    invoke-interface {p2}, Landroid/view/MenuItem;->getGroupId()I

    move-result v1

    const v2, 0x7f1004d1

    if-ne v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    invoke-virtual {v0, p1, p2}, Landroid/app/Dialog;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_3

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    goto :goto_0

    :cond_3
    const v3, 0x7f1004c6

    if-ne v2, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->c()V

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_4
    const v3, 0x7f10007a

    if-ne v2, v3, :cond_5

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "f"

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    goto :goto_0

    :cond_5
    const v3, 0x7f1004b7

    if-eq v2, v3, :cond_6

    const v3, 0x7f1004c1

    if-eq v2, v3, :cond_6

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->o()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->a()V

    :cond_6
    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/ui/as;->a(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->f:Lcom/google/googlenav/ui/au;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/au;->a(Landroid/view/Menu;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    instance-of v0, v0, LT/a;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bC;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0}, LaP/b;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    :cond_2
    const v0, 0x7f1004d1

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeGroup(I)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bC;->b(Landroid/view/Menu;)V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->g:LaP/b;

    invoke-virtual {v0}, LaP/b;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->b:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    :cond_4
    const v0, 0x7f1004be

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v3}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->az()Z

    move-result v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_5
    const v0, 0x7f1004c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v3, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v3}, Lcom/google/googlenav/android/A;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    sget-object v4, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->k()I

    move-result v4

    invoke-static {v3, v4}, LaP/a;->a(Lcom/google/googlenav/android/BaseMapsActivity;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bC;->a:Lcom/google/googlenav/android/BaseMapsActivity;

    check-cast v0, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v3

    const v0, 0x7f1004bf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_a

    if-eqz v3, :cond_a

    move v0, v1

    :goto_1
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_7
    const v0, 0x7f1004c0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v4

    if-eqz v4, :cond_8

    if-nez v3, :cond_8

    move v2, v1

    :cond_8
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_9
    const v0, 0x7f10007a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ag()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_1
.end method
