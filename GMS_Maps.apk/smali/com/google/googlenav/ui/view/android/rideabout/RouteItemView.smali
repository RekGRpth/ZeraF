.class public Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/google/googlenav/ui/view/android/rideabout/f;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(I)I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->a()I

    move-result v0

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->c()I

    move-result v1

    rem-int/2addr v0, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->d()I

    move-result v2

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->e()I

    move-result v2

    add-int/2addr v0, v2

    if-gez v0, :cond_0

    add-int/2addr v0, v1

    :cond_0
    add-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2, v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->b()V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->setMinimumHeight(I)V

    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->setMinimumHeight(I)V

    goto :goto_1
.end method

.method public setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V
    .locals 4

    const/4 v3, -0x1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->b:Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setExpandCommandView(Lcom/google/googlenav/ui/view/android/rideabout/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->c:Lcom/google/googlenav/ui/view/android/rideabout/f;

    return-void
.end method

.method public setIntermediateStop(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->d:Z

    return-void
.end method
