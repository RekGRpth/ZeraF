.class public Lcom/google/googlenav/ui/view/android/rideabout/a;
.super Landroid/text/style/ImageSpan;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/Bitmap;)V

    iput v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/Bitmap;)V

    iput v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->b:I

    iput p2, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    iput p3, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->b:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-virtual {p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int v2, v1, v0

    iget v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/a;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    iget v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    if-ne v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/a;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :goto_1
    add-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/a;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v4, v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    add-int v0, p7, v2

    add-int/lit8 v0, v0, -0x2

    int-to-float v0, v0

    invoke-virtual {p1, p5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/a;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->b:I

    goto :goto_1
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-super/range {p0 .. p5}, Landroid/text/style/ImageSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/a;->a:I

    goto :goto_0
.end method
