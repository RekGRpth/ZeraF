.class public Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;
.super Landroid/widget/RadioGroup;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/view/android/X;

.field private b:I

.field private c:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)Lcom/google/googlenav/ui/view/android/X;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a:Lcom/google/googlenav/ui/view/android/X;

    return-object v0
.end method

.method private a()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/ab;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/ab;-><init>(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;Lcom/google/googlenav/ui/view/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a:Lcom/google/googlenav/ui/view/android/X;

    new-instance v0, Lcom/google/googlenav/ui/view/android/ac;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/ac;-><init>(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;Lcom/google/googlenav/ui/view/android/aa;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    return-void
.end method

.method private a(I)V
    .locals 2

    iput p1, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->c:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->c:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    invoke-interface {v0, p0, v1}, Landroid/widget/RadioGroup$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/RadioGroup;I)V

    :cond_0
    return-void
.end method

.method private a(IZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    return v0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    instance-of v0, p1, Lcom/google/googlenav/ui/view/android/CheckableContainer;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/googlenav/ui/view/android/CheckableContainer;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a:Lcom/google/googlenav/ui/view/android/X;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->setOnCheckedChangeListener(Lcom/google/googlenav/ui/view/android/X;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->check(I)V

    :cond_0
    return-void
.end method

.method public check(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(IZ)V

    :cond_1
    if-eq p1, v1, :cond_2

    invoke-direct {p0, p1, v3}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(IZ)V

    :cond_2
    iput-boolean v2, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(I)V

    goto :goto_0
.end method

.method public getCheckedRadioButtonId()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/widget/RadioGroup;->onFinishInflate()V

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iput-boolean v2, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(IZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->d:Z

    iget v0, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->b:I

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(I)V

    :cond_0
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->c:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-void
.end method
