.class public Lcom/google/googlenav/ui/view/android/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/android/S;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/android/S;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    return-void
.end method

.method public static a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ui/aW;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/util/List;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;
    .locals 2

    invoke-virtual {p3, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez p1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(ILjava/util/List;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/ae;)Lcom/google/googlenav/ui/view/android/S;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/ui/android/ad;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/ag;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/ag;-><init>(Lcom/google/googlenav/ui/view/android/ae;I)V

    return-object v0
.end method

.method public a()V
    .locals 4

    const/4 v3, -0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    const v1, 0x7f1000fc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    iget-object v1, v1, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/af;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/view/android/af;-><init>(Lcom/google/googlenav/ui/view/android/ae;Lcom/google/googlenav/ui/e;)V

    new-instance v1, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v1, v3, v3}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :cond_0
    return-void
.end method
