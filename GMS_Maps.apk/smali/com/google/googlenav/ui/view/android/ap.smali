.class Lcom/google/googlenav/ui/view/android/ap;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/googlenav/ui/view/android/aq;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/ap;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/aq;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/view/android/aq;->a(Landroid/view/View;)V

    :goto_0
    return-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/ap;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/aq;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ap;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aq;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_0
.end method
