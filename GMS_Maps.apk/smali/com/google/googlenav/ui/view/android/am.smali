.class public Lcom/google/googlenav/ui/view/android/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/view/dialog/q;

.field private b:[Lcom/google/googlenav/ui/view/android/aq;

.field private final c:Landroid/view/View;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/googlenav/ui/e;

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/googlenav/ui/e;ZZII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/am;->c:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/am;->e:Lcom/google/googlenav/ui/e;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    iput p5, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    iput p6, p0, Lcom/google/googlenav/ui/view/android/am;->f:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/am;->d()V

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/am;->b(Z)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p4}, Lcom/google/googlenav/ui/view/android/am;->a(Z)V

    :cond_0
    return-void
.end method

.method private static a(II)Lcom/google/googlenav/ui/view/android/aq;
    .locals 6

    invoke-static {p0, p1}, Lax/b;->b(II)Z

    move-result v5

    packed-switch p0, :pswitch_data_0

    new-instance v0, Lcom/google/googlenav/ui/view/android/aq;

    if-eqz v5, :cond_3

    const v1, 0x7f0202ba

    :goto_0
    const/16 v2, 0x10a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xe8

    move v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/aq;-><init>(ILjava/lang/String;IIZ)V

    :goto_1
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/googlenav/ui/view/android/aq;

    if-eqz v5, :cond_0

    const v1, 0x7f0202c2

    :goto_2
    const/16 v2, 0x5d6

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xe9

    move v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/aq;-><init>(ILjava/lang/String;IIZ)V

    goto :goto_1

    :cond_0
    const v1, 0x7f0202c3

    goto :goto_2

    :pswitch_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/aq;

    if-eqz v5, :cond_1

    const v1, 0x7f0202ca

    :goto_3
    const/16 v2, 0x5df

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xea

    move v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/aq;-><init>(ILjava/lang/String;IIZ)V

    goto :goto_1

    :cond_1
    const v1, 0x7f0202cb

    goto :goto_3

    :pswitch_2
    new-instance v0, Lcom/google/googlenav/ui/view/android/aq;

    if-eqz v5, :cond_2

    const v1, 0x7f0202b8

    :goto_4
    const/16 v2, 0x232

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xeb

    move v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/aq;-><init>(ILjava/lang/String;IIZ)V

    goto :goto_1

    :cond_2
    const v1, 0x7f0202b9

    goto :goto_4

    :cond_3
    const v1, 0x7f0202bb

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/am;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/am;->b(I)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->c:Landroid/view/View;

    const v1, 0x7f10023d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/android/am;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    const v1, 0x7f10023e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x50b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->e:Lcom/google/googlenav/ui/e;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    return-void
.end method

.method private b(Z)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->c:Landroid/view/View;

    const v1, 0x7f10023b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    const/16 v1, 0xec

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/am;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    const v1, 0x7f10023c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x2fa

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->c:Landroid/view/View;

    const v1, 0x7f10023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const/16 v1, 0xed

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/am;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v1, p0, Lcom/google/googlenav/ui/view/android/am;->f:I

    iget v2, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/view/android/am;->a(II)Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/aq;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/am;->e()[Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->b:[Lcom/google/googlenav/ui/view/android/aq;

    return-void
.end method

.method private e()[Lcom/google/googlenav/ui/view/android/aq;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/googlenav/ui/view/android/aq;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    invoke-static {v2, v1}, Lcom/google/googlenav/ui/view/android/am;->a(II)Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    invoke-static {v3, v1}, Lcom/google/googlenav/ui/view/android/am;->a(II)Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v1

    aput-object v1, v0, v3

    iget v1, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    invoke-static {v4, v1}, Lcom/google/googlenav/ui/view/android/am;->a(II)Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lcom/google/googlenav/ui/view/android/am;->g:I

    invoke-static {v5, v1}, Lcom/google/googlenav/ui/view/android/am;->a(II)Lcom/google/googlenav/ui/view/android/aq;

    move-result-object v1

    aput-object v1, v0, v5

    return-object v0
.end method


# virtual methods
.method protected a(I)Lcom/google/googlenav/ui/android/ad;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/an;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/an;-><init>(Lcom/google/googlenav/ui/view/android/am;I)V

    return-object v0
.end method

.method public a()V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/q;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/am;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/am;->c:Landroid/view/View;

    const v4, 0x7f100240

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/view/android/ap;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/am;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/am;->b:[Lcom/google/googlenav/ui/view/android/aq;

    invoke-direct {v4, v5, v6}, Lcom/google/googlenav/ui/view/android/ap;-><init>(Landroid/content/Context;[Lcom/google/googlenav/ui/view/android/aq;)V

    new-instance v5, Lcom/google/googlenav/ui/view/android/ao;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/view/android/ao;-><init>(Lcom/google/googlenav/ui/view/android/am;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/ui/view/dialog/q;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/ListAdapter;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/dialog/q;->setOwnerActivity(Landroid/app/Activity;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/q;->show()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/q;->dismiss()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/am;->a:Lcom/google/googlenav/ui/view/dialog/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/q;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/am;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/am;->a()V

    goto :goto_0
.end method
