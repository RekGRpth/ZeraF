.class public Lcom/google/googlenav/ui/view/android/bn;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaB/p;


# static fields
.field private static final a:Ljava/lang/CharSequence;


# instance fields
.field private b:Landroid/widget/ListView;

.field c:Lcom/google/googlenav/ai;

.field protected final d:Lbf/m;

.field protected final l:Lcom/google/googlenav/ui/br;

.field protected m:Lcom/google/googlenav/ui/view/android/J;

.field protected n:Landroid/view/View;

.field protected o:Z

.field p:Landroid/view/View;

.field private q:Lcom/google/googlenav/ui/view/dialog/ba;

.field private r:Lcom/google/googlenav/common/a;

.field private s:J

.field private final t:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x605

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/view/android/bn;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;)V
    .locals 2

    const v0, 0x7f0f0018

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    new-instance v0, Lcom/google/googlenav/ui/view/android/bo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bo;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {p2}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private J()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    :cond_0
    return-void
.end method

.method private K()Lbj/H;
    .locals 13

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/view/android/bn;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->by()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v0, 0x5f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v9

    if-nez v9, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->d(Z)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x1f

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v10

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, v2}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :cond_2
    :goto_1
    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v11, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v11}, Lcom/google/googlenav/ai;->aj()Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->aP()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->aQ()Ljava/lang/String;

    move-result-object v2

    :cond_3
    new-instance v11, Lcom/google/googlenav/ui/view/android/bt;

    invoke-direct {v11}, Lcom/google/googlenav/ui/view/android/bt;-><init>()V

    sget-object v12, Lcom/google/googlenav/ui/aV;->aP:Lcom/google/googlenav/ui/aV;

    invoke-virtual {v11, v5, v12}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/googlenav/ui/view/android/bt;->a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/String;Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v5

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aK()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v4

    :goto_2
    invoke-virtual {v5, v4}, Lcom/google/googlenav/ui/view/android/bt;->a(Lai/d;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/googlenav/ui/view/android/bt;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/view/android/bt;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/googlenav/ui/view/android/bt;->a(LaN/B;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/googlenav/ui/view/android/bt;->a(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->b(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/googlenav/ui/view/android/bt;->c(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const v1, 0x7f0400c2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ar;)Lcom/google/googlenav/ui/view/android/bt;

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/googlenav/ap;->b()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    const/4 v0, 0x1

    :goto_3
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Lbf/aS;->b(Lcom/google/googlenav/ai;ZZ)Lcom/google/googlenav/ui/view/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v4, v0}, Lbf/aS;->b(Lcom/google/googlenav/ai;Z)V

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->L()Z

    move-result v4

    if-nez v4, :cond_5

    if-nez v0, :cond_6

    :cond_5
    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/view/android/bt;

    :cond_6
    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/bt;->a()Lcom/google/googlenav/ui/view/android/bs;

    move-result-object v0

    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Vector;

    const/4 v0, 0x3

    invoke-direct {v2, v0}, Ljava/util/Vector;-><init>(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bl()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->an()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {v2, p1}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/ba;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;-><init>(Lcom/google/googlenav/bZ;Lbf/i;Lcom/google/googlenav/ui/view/android/bn;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    :cond_0
    return-void
.end method

.method private e(Ljava/util/List;)V
    .locals 10

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v0}, Lbf/aS;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    sget-object v2, Lcom/google/googlenav/ui/view/android/bn;->a:Ljava/lang/CharSequence;

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v7, 0x19

    const/4 v8, -0x1

    invoke-direct {v6, v7, v8, v5}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bc()Lcom/google/googlenav/ac;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v2, v1, v9}, Lbf/aS;->a(Lcom/google/googlenav/ai;ZZ)Ljava/util/Vector;

    move-result-object v2

    invoke-static {v0}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Ljava/lang/CharSequence;Ljava/util/Vector;)V

    :cond_1
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v2, v0, v9}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Vector;Z)V

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aT;

    invoke-static {v0, v1}, Lbf/aS;->a(Lbf/aT;I)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method private f(Ljava/util/List;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->J()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->h:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->v()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->a(Ljava/util/List;)Ljava/util/List;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected A()Lbj/H;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x579

    :goto_0
    new-instance v1, Lbj/bv;

    const/16 v2, 0x1b

    const v3, 0x7f040119

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    return-object v1

    :cond_0
    const/16 v0, 0x577

    goto :goto_0
.end method

.method protected B()Ljava/util/List;
    .locals 2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    return-object v0
.end method

.method C()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected D()I
    .locals 1

    const v0, 0x7f040118

    return v0
.end method

.method protected E()I
    .locals 1

    const v0, 0x7f040116

    return v0
.end method

.method protected F()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v0

    return v0
.end method

.method protected G()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->a(Lcom/google/googlenav/ai;)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aR;->b(Lcom/google/googlenav/ai;)I

    move-result v1

    add-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    :cond_0
    return-void
.end method

.method public I()V
    .locals 8

    const-wide/high16 v6, -0x8000000000000000L

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lai/a;->a(Ljava/lang/String;IJ)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    iput-wide v6, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    :cond_0
    return-void
.end method

.method public I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public Q_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    :cond_0
    return-void
.end method

.method protected a(Lbf/aQ;)Lbj/H;
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->d(Lcom/google/googlenav/ai;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;
    .locals 10

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {p2, v4, v3}, Lbf/aR;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    new-instance v0, Lbj/aE;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    move-object v1, p2

    move-object v2, p3

    move-object v6, p1

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-static {v3}, Lcom/google/common/collect/aT;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/android/bq;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/view/android/bq;-><init>(Lcom/google/googlenav/ui/view/android/bn;Lbj/aE;)V

    invoke-virtual {v1, v2, v3}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_0
    return-object v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    const v2, 0x7f100217

    const v0, 0x7f1001ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    const v1, 0x7f1001ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(Lbf/aQ;Ljava/util/List;Z)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbj/ao;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v4, 0xe

    new-instance v5, Lcom/google/googlenav/bh;

    invoke-direct {v5}, Lcom/google/googlenav/bh;-><init>()V

    move-object v2, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lbj/ao;-><init>(Lcom/google/googlenav/ai;Lbf/aQ;Lbf/m;ILcom/google/googlenav/bh;Z)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->c(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->b(Ljava/lang/Runnable;)V

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cj()Lcom/google/googlenav/ao;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cj()Lcom/google/googlenav/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lbj/B;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v3}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbj/B;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/J;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected a(Ljava/util/List;Lbf/aQ;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->c(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lbf/aQ;->a()Z

    move-result v5

    new-instance v0, Lbj/bc;

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v2

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    invoke-direct/range {v0 .. v5}, Lbj/bc;-><init>(IZLjava/util/List;Lcom/google/googlenav/ui/br;Z)V

    invoke-virtual {v0}, Lbj/bc;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lbj/bv;

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->E()I

    move-result v3

    const/16 v4, 0x31d

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method protected a(Ljava/util/List;Lbf/aQ;Z)V
    .locals 7

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x61e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    const/16 v4, 0x14

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/CharSequence;Ljava/util/Vector;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    move v0, v1

    :cond_0
    if-nez p2, :cond_2

    const/16 v2, 0x601

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v3, 0xf

    const/16 v5, 0x14

    invoke-direct {v6, v3, v5, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    const/16 v2, 0x1e2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected a(Ljava/util/List;Z)V
    .locals 4

    const/4 v3, 0x2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lbj/bx;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v3, p2}, Lbj/bx;-><init>(Lcom/google/googlenav/ai;Lbf/i;IZ)V

    :goto_0
    invoke-virtual {v0}, Lbj/aR;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lbj/aR;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v3, p2}, Lbj/aR;-><init>(Lcom/google/googlenav/ai;Lbf/i;IZ)V

    goto :goto_0
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x258

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/16 v0, 0x4b1

    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v2, v0, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_0
    const/16 v0, 0x4b0

    goto :goto_1

    :sswitch_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x909

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x2bf

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x14

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x2c0

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    :cond_1
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x5dc

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x7f1002a8 -> :sswitch_4
        0x7f1004b2 -> :sswitch_0
        0x7f1004b8 -> :sswitch_1
        0x7f1004b9 -> :sswitch_2
        0x7f1004ba -> :sswitch_5
        0x7f1004bb -> :sswitch_6
        0x7f1004bc -> :sswitch_7
        0x7f1004cf -> :sswitch_3
    .end sparse-switch
.end method

.method protected b(Landroid/view/Menu;)V
    .locals 3

    const v0, 0x7f1004b8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x59a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_0
.end method

.method protected b(Ljava/util/List;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->g(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lbf/aT;

    const/4 v2, 0x0

    const/16 v3, 0x2c4

    invoke-direct {v1, v0, v2, v3, v4}, Lbf/aT;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {v1, v4}, Lbf/aS;->a(Lbf/aT;I)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected b(Ljava/util/List;Lbf/aQ;Z)V
    .locals 7

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bh()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x127

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    const/16 v4, 0x16

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->l()V

    return-void
.end method

.method public c()Landroid/view/View;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040114

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100353

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/View;)V

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/aC;->c(Lcom/google/googlenav/ai;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->e(Lcom/google/googlenav/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->f(Lcom/google/googlenav/ai;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/ViewGroup;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->m()Ljava/util/List;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v5, 0x24

    invoke-direct {v0, v3, v4, v2, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/google/googlenav/ui/view/android/br;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/br;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lbj/aV;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v2}, Lbj/aV;-><init>(Lcom/google/googlenav/ai;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbj/aV;->a(Landroid/view/View;Landroid/content/Context;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->l()V

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->J()V

    goto :goto_1
.end method

.method protected c(Landroid/view/Menu;)V
    .locals 7

    const/4 v1, 0x0

    const v0, 0x7f1004b9

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/16 v0, 0x530

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_1
.end method

.method protected c(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lbj/bv;

    const/16 v2, 0x1d

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->D()I

    move-result v3

    invoke-direct {v1, v2, v3, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    return-void
.end method

.method protected c(Ljava/util/List;Lbf/aQ;Z)V
    .locals 7

    const/4 v3, 0x1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bi()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    const/16 v4, 0x15

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->a()V

    :cond_0
    return-void
.end method

.method protected d(Landroid/view/Menu;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->z()I

    move-result v3

    if-ne v3, v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v1

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v0, v2

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->bm()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x255

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const v2, 0x7f1004cf

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_3
    return-void

    :cond_4
    const/16 v1, 0x257

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method protected d(Ljava/util/List;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bf()Lcom/google/googlenav/cx;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Lbj/bq;

    const/16 v1, 0x1a

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->be()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->T()I

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bg()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lbj/bq;-><init>(ILcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected e(Landroid/view/Menu;)V
    .locals 2

    const v0, 0x7f1004ba

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x23

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aS;->b(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    return-void
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected f(Landroid/view/Menu;)V
    .locals 2

    const v0, 0x7f1004bb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x34a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method protected g(Landroid/view/Menu;)V
    .locals 5

    const/4 v0, 0x1

    const v1, 0x7f1004bc

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/16 v1, 0x43b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bA()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->Z()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->q()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->Y()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbf/m;->c(LaN/B;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->m()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbj/aV;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1}, Lbj/aV;-><init>(Lcom/google/googlenav/ai;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbj/aV;->a(Landroid/view/View;Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected l()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->z()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->B()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->y()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected o()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/view/android/bn;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x37

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->x()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->b(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->c(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->d(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->e(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->f(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->g(Landroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public w()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected x()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()Ljava/util/List;
    .locals 8

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v2}, Lbf/m;->bs()Ljava/util/Hashtable;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Hashtable;Ljava/lang/Object;)Lbf/aQ;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v5

    invoke-virtual {p0, v6, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    invoke-virtual {p0, v0, v6, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Ljava/util/List;Z)V

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->f(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->c(Ljava/util/List;)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->d(Ljava/util/List;)V

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->e(Ljava/util/List;)V

    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Lbf/aQ;)V

    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->b(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->c(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->F()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lbj/aX;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3, v5}, Lbj/aX;-><init>(Lcom/google/googlenav/ai;IZ)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->G()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;)Lbj/H;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;)V

    new-instance v0, Lbj/bm;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v7, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4, v7}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lbj/bm;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbj/bi;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lbj/bi;-><init>(Lcom/google/googlenav/ai;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbj/al;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2, v5}, Lbj/al;-><init>(ILcom/google/googlenav/ai;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->b(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    check-cast v3, Lbf/bk;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v2

    invoke-virtual {v3}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    const/16 v1, 0x577

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x13

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/g;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v6

    goto/16 :goto_0
.end method

.method protected z()Ljava/util/List;
    .locals 3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->A()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/e;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->b(Ljava/util/List;)V

    return-object v0
.end method
