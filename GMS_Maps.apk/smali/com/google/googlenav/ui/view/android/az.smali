.class Lcom/google/googlenav/ui/view/android/az;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/android/au;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/android/au;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/az;->a:Lcom/google/googlenav/ui/view/android/au;

    const v0, 0x7f040070

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    check-cast p2, Landroid/widget/LinearLayout;

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/az;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/at;

    invoke-virtual {v0}, Lcom/google/googlenav/at;->a()Lcom/google/googlenav/ar;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_1
    const v1, 0x7f100198

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/az;->a:Lcom/google/googlenav/ui/view/android/au;

    sget-object v4, Lcom/google/googlenav/ui/aV;->bc:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v3, v1, v2}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/ui/view/android/au;Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    const v1, 0x7f100199

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/az;->a:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/at;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/ui/view/android/au;Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    const v1, 0x7f10019a

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x2bb

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->bb:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const v1, 0x7f100197

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/az;->a:Lcom/google/googlenav/ui/view/android/au;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/android/au;->c(Lcom/google/googlenav/ui/view/android/au;)LaB/s;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/az;->a:Lcom/google/googlenav/ui/view/android/au;

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/ui/view/android/au;Lcom/google/googlenav/at;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    invoke-virtual {v2, v0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-object p2

    :cond_1
    const v0, 0x7f040070

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto/16 :goto_0

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/az;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/at;

    invoke-virtual {v0}, Lcom/google/googlenav/at;->a()Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
