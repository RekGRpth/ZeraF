.class public Lcom/google/googlenav/ui/view/android/x;
.super Lcom/google/googlenav/ui/view/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lax/b;Lcom/google/googlenav/ui/e;Z)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/view/ViewGroup;Lax/b;Lcom/google/googlenav/ui/e;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method private static a(Landroid/view/ViewGroup;Lax/b;Lcom/google/googlenav/ui/e;Z)Landroid/view/View;
    .locals 8

    const v7, 0x7f100183

    const v3, 0x7f100179

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f100181

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const v0, 0x7f100058

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10017c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0xf1

    invoke-static {v0, p1, p2, v2}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/view/View;Lax/b;Lcom/google/googlenav/ui/e;I)V

    const v0, 0x7f10001f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    if-eqz p3, :cond_0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    new-instance v3, Lcom/google/googlenav/ui/view/android/z;

    invoke-direct {v3, p2}, Lcom/google/googlenav/ui/view/android/z;-><init>(Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100182

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x2f6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    if-ne v0, v5, :cond_1

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    new-instance v3, Lcom/google/googlenav/ui/view/android/A;

    invoke-direct {v3, p2}, Lcom/google/googlenav/ui/view/android/A;-><init>(Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100184

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x586

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100185

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lax/b;->Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    return-object v1

    :cond_0
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const v1, 0x7f0f00fd

    const v2, 0x7f0f00fe

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected static a()V
    .locals 0

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V
    .locals 4

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v1, p0, p3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x22

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method public static a(Landroid/view/View;Lax/b;Lcom/google/googlenav/ui/e;I)V
    .locals 7

    const v6, 0x7f10017f

    const v5, 0x7f0f00ff

    const/4 v4, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v4}, Landroid/view/View;->setClickable(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/y;

    invoke-direct {v0, p2, p3}, Lcom/google/googlenav/ui/view/android/y;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x5bf

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10017e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lax/b;->M()I

    move-result v0

    invoke-virtual {p1}, Lax/b;->ab()Lax/h;

    move-result-object v2

    invoke-virtual {v2}, Lax/h;->q()I

    move-result v2

    add-int/2addr v0, v2

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/bd;->a(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/x;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v2

    if-ne v2, v4, :cond_1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, " - "

    invoke-static {v1, v2, v0, v5}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->az()I

    move-result v3

    invoke-static {p1, v0, v3, v4}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/b;Lcom/google/googlenav/ui/bi;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    const v3, 0x7f0f002c

    invoke-static {v1, v2, v0, v3}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f100180

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    sget-char v3, Lcom/google/googlenav/ui/bi;->bn:C

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    :goto_2
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, " - "

    invoke-static {v1, v2, v0, v5}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V

    invoke-virtual {p1}, Lax/b;->am()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/x;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, " - "

    invoke-static {v1, v2, v0, v5}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V

    invoke-virtual {p1}, Lax/b;->aG()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0f0100

    invoke-static {v1, v2, v0, v3}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lax/b;->k()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    sget-char v3, Lcom/google/googlenav/ui/bi;->bl:C

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lax/b;->ab()Lax/h;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/view/android/B;->a:[I

    invoke-virtual {v1}, Lax/h;->r()Lax/f;

    move-result-object v1

    invoke-virtual {v1}, Lax/f;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    const v1, 0x7f02042b

    :goto_3
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :pswitch_0
    const v1, 0x7f02042d

    goto :goto_3

    :pswitch_1
    const v1, 0x7f02042c

    goto :goto_3

    :pswitch_2
    const v1, 0x7f02042e

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
