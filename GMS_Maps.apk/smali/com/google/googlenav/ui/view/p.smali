.class public Lcom/google/googlenav/ui/view/p;
.super Lcom/google/googlenav/ui/view/q;
.source "SourceFile"


# instance fields
.field public a:Lax/j;

.field public b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lax/j;I)V
    .locals 6

    const/4 v5, 0x0

    new-array v2, v5, [I

    new-array v3, v5, [I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/q;-><init>(Ljava/lang/String;[I[ILjava/util/List;Z)V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    iput p3, p0, Lcom/google/googlenav/ui/view/p;->b:I

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-virtual {v0}, Lax/j;->aq()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-virtual {v0}, Lax/j;->as()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/view/p;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
