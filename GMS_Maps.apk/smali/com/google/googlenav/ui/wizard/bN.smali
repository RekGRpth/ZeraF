.class public Lcom/google/googlenav/ui/wizard/bN;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Date;

.field private b:I

.field private c:Lcom/google/googlenav/ui/wizard/bO;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->c:Lcom/google/googlenav/ui/wizard/bO;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bN;I)I
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/bN;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bN;)Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bN;Ljava/util/Date;)Ljava/util/Date;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/bN;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bN;->b:I

    return v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bN;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    return v0
.end method

.method public a(Lax/l;)V
    .locals 1

    invoke-virtual {p1}, Lax/l;->c()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    invoke-virtual {p1}, Lax/l;->d()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/bN;->b:I

    return-void
.end method

.method public a(Lax/l;Lcom/google/googlenav/ui/wizard/bO;)V
    .locals 0

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/bN;->c:Lcom/google/googlenav/ui/wizard/bO;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/bN;->a(Lax/l;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    iput v3, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bN;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/bN;->b:I

    invoke-static {v1, v2}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/bN;->a(Lax/l;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bN;->c:Lcom/google/googlenav/ui/wizard/bO;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bN;->c:Lcom/google/googlenav/ui/wizard/bO;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bN;->e()Lax/l;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/ui/wizard/bO;->a(Lax/l;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/bN;->c:Lcom/google/googlenav/ui/wizard/bO;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bN;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->K()V

    iput v3, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bN;->h()V

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/bN;->g:I

    new-instance v0, Lcom/google/googlenav/ui/wizard/bP;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bP;-><init>(Lcom/google/googlenav/ui/wizard/bN;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method public e()Lax/l;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->a:Ljava/util/Date;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/bN;->b:I

    invoke-static {v0, v1}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bN;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    return-void
.end method
