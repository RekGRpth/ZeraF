.class public Lcom/google/googlenav/ui/wizard/hZ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/g;


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;

.field private final b:Lcom/google/googlenav/ui/wizard/hH;

.field private c:LaZ/a;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/hH;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method


# virtual methods
.method public B_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x459

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x45a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    return-void
.end method

.method public C_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    :cond_0
    return-void
.end method

.method public D_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    :cond_0
    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public a(LaZ/a;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hZ;->B_()V

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
