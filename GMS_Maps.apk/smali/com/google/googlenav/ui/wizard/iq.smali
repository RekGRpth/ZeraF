.class Lcom/google/googlenav/ui/wizard/iq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Landroid/widget/CheckBox;

.field final synthetic d:Lcom/google/googlenav/ui/wizard/io;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/io;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iq;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/iq;->b:Landroid/widget/CheckBox;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/iq;->c:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/EnumSet;Ljava/util/EnumSet;)V
    .locals 3

    invoke-virtual {p1, p2}, Ljava/util/EnumSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v1, 0x61

    const-string v2, "f"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ik;->o()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-virtual {v1}, Ljava/util/EnumSet;->clear()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/iq;->a(Ljava/util/EnumSet;Ljava/util/EnumSet;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/in;->a:Lcom/google/googlenav/ui/wizard/im;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/im;->a(Ljava/util/EnumSet;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/io;->b(Lcom/google/googlenav/ui/wizard/io;)Lcom/google/googlenav/ui/wizard/ix;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ix;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iq;->d:Lcom/google/googlenav/ui/wizard/io;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ik;->a()V

    goto/16 :goto_0
.end method
