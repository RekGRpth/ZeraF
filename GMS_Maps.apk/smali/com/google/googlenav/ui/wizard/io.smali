.class public Lcom/google/googlenav/ui/wizard/io;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/ik;

.field private b:Landroid/view/View;

.field private final c:Lcom/google/googlenav/ui/wizard/ix;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/ik;Lcom/google/googlenav/ui/e;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    const v0, 0x7f0f001b

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/io;->m()Lcom/google/googlenav/ui/wizard/ix;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/io;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ik;->a(Landroid/media/AudioManager;)Landroid/media/AudioManager;

    :cond_0
    return-void
.end method

.method private a(ILandroid/widget/SeekBar;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    invoke-virtual {p2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/io;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/io;->n()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/io;)Lcom/google/googlenav/ui/wizard/ix;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    return-object v0
.end method

.method static synthetic l()Lcom/google/googlenav/android/BaseMapsActivity;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/io;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    return-object v0
.end method

.method private m()Lcom/google/googlenav/ui/wizard/ix;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/wizard/iu;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/iu;-><init>(Lcom/google/googlenav/ui/wizard/io;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/it;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/it;-><init>(Lcom/google/googlenav/ui/wizard/io;)V

    goto :goto_0
.end method

.method private n()V
    .locals 7

    const/16 v3, 0x8

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f10044b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f10044c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    if-eqz v4, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    invoke-interface {v1, v5}, Lcom/google/googlenav/ui/wizard/ix;->a(I)V

    invoke-direct {p0, v5, v0}, Lcom/google/googlenav/ui/wizard/io;->a(ILandroid/widget/SeekBar;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f100449

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v4, 0x7f10044a

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    if-eqz v1, :cond_3

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    invoke-interface {v1, v6}, Lcom/google/googlenav/ui/wizard/ix;->a(I)V

    invoke-direct {p0, v6, v0}, Lcom/google/googlenav/ui/wizard/io;->a(ILandroid/widget/SeekBar;)V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method protected I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/io;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x4b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/ip;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ip;-><init>(Lcom/google/googlenav/ui/wizard/io;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/io;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/io;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto :goto_0
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/io;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 8

    const v7, 0x7f090007

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/io;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401b9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f100340

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x4b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f10044b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v2, 0x7f100448

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v3, 0x7f100449

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/16 v3, 0x4b3

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0x4b8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0x4b9

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v6}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v6

    iget-object v6, v6, Lcom/google/googlenav/ui/wizard/in;->c:Ljava/util/EnumSet;

    invoke-virtual {v3, v6}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setTextColor(I)V

    :cond_1
    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v6}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v6

    iget-object v6, v6, Lcom/google/googlenav/ui/wizard/in;->c:Ljava/util/EnumSet;

    invoke-virtual {v3, v6}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setTextColor(I)V

    :cond_2
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v3

    iget-object v6, v3, Lcom/google/googlenav/ui/wizard/in;->c:Ljava/util/EnumSet;

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v7

    iget-object v7, v7, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-virtual {v3, v7}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v3, v6}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v7

    iget-object v7, v7, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-virtual {v3, v7}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v3, v6}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;

    move-result-object v7

    iget-object v7, v7, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    invoke-virtual {v3, v7}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v3, v6}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    invoke-interface {v3}, Lcom/google/googlenav/ui/wizard/ix;->a()V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v4, 0x7f10044c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    new-instance v4, Lcom/google/googlenav/ui/wizard/iv;

    const/4 v6, 0x2

    invoke-direct {v4, p0, v6}, Lcom/google/googlenav/ui/wizard/iv;-><init>(Lcom/google/googlenav/ui/wizard/io;I)V

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v4, 0x7f10044a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    new-instance v4, Lcom/google/googlenav/ui/wizard/iv;

    const/4 v6, 0x3

    invoke-direct {v4, p0, v6}, Lcom/google/googlenav/ui/wizard/iv;-><init>(Lcom/google/googlenav/ui/wizard/io;I)V

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/ir;

    invoke-direct {v3, p0, v2, v0}, Lcom/google/googlenav/ui/wizard/ir;-><init>(Lcom/google/googlenav/ui/wizard/io;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v4, 0x7f100030

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const/16 v4, 0x4b1

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/google/googlenav/ui/wizard/iq;

    invoke-direct {v4, p0, v1, v2, v0}, Lcom/google/googlenav/ui/wizard/iq;-><init>(Lcom/google/googlenav/ui/wizard/io;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v1, 0x7f10002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    return-object v0

    :cond_3
    move v3, v5

    goto/16 :goto_0

    :cond_4
    move v3, v5

    goto/16 :goto_1

    :cond_5
    move v4, v5

    goto :goto_2
.end method

.method public h()Ljava/util/EnumSet;
    .locals 3

    const-class v0, Lcom/google/googlenav/ui/wizard/iw;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v2, 0x7f10044b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v2, 0x7f100448

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/io;->b:Landroid/view/View;

    const v2, 0x7f100449

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v4, 0x5

    const/4 v1, 0x0

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/io;->h()Ljava/util/EnumSet;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    :goto_0
    packed-switch p1, :pswitch_data_0

    move v0, v1

    :goto_1
    :sswitch_0
    return v0

    :sswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/io;->c:Lcom/google/googlenav/ui/wizard/ix;

    invoke-interface {v1}, Lcom/google/googlenav/ui/wizard/ix;->b()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/io;->a:Lcom/google/googlenav/ui/wizard/ik;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/ik;->h()V

    goto :goto_1

    :cond_0
    sget-object v3, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v2, v0, v4}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/io;->n()V

    goto :goto_1

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v1

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/io;->n()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x54 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
