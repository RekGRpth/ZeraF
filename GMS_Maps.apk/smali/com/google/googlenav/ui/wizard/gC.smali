.class public Lcom/google/googlenav/ui/wizard/gC;
.super Lf/a;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private final j:LaB/s;

.field private final k:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/wizard/gA;LaB/s;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lf/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gC;->j:LaB/s;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gE;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/gE;-><init>(Lcom/google/googlenav/ui/wizard/gC;Lcom/google/googlenav/ui/wizard/gD;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/gC;->a(Landroid/widget/FilterQueryProvider;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gC;)Lcom/google/googlenav/ui/wizard/gA;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gC;->d()Lcom/google/googlenav/ui/wizard/gA;

    move-result-object v0

    return-object v0
.end method

.method private d()Lcom/google/googlenav/ui/wizard/gA;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gC;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gA;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/friend/CircleListItem;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/friend/CircleListItem;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a(IZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/gC;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gA;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->c()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2

    instance-of v0, p1, Lcom/google/googlenav/ui/wizard/gA;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cursor must be a PlusoneAudienceCursor!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lf/a;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    check-cast p3, Lcom/google/googlenav/ui/wizard/gA;

    check-cast p1, Lcom/google/googlenav/ui/friend/CircleListItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gC;->j:LaB/s;

    invoke-virtual {p3}, Lcom/google/googlenav/ui/wizard/gA;->c()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/ui/friend/CircleListItem;->setPostTarget(LaB/s;Lcom/google/googlenav/friend/aD;)V

    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/aD;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    instance-of v0, p1, Lcom/google/googlenav/ui/wizard/gA;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cursor must be a PlusoneAudienceCursor!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lf/a;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gC;->k:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gC;->d()Lcom/google/googlenav/ui/wizard/gA;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gA;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gC;->d()Lcom/google/googlenav/ui/wizard/gA;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gA;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gC;->d()Lcom/google/googlenav/ui/wizard/gA;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
