.class Lcom/google/googlenav/ui/wizard/ij;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Spinner;

.field final synthetic b:Landroid/widget/EditText;

.field final synthetic c:Lcom/google/googlenav/ui/wizard/ii;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/ii;Landroid/widget/Spinner;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ij;->a:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ij;->b:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ii;->a:Lcom/google/googlenav/ui/wizard/ie;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ie;->o()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ii;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Please choose a log file."

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const/high16 v0, -0x40800000

    :try_start_0
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ij;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-gtz v2, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ii;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Please enter a time ratio greater than 0."

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {v0, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-gtz v2, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ii;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Please enter a time ratio greater than 0."

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ii;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Please enter a time ratio greater than 0."

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    throw v1

    :cond_3
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/ii;->a:Lcom/google/googlenav/ui/wizard/ie;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ie;->a(Lcom/google/googlenav/ui/wizard/ie;)Lcom/google/googlenav/ui/wizard/ih;

    move-result-object v2

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/ih;->a:Lcom/google/googlenav/ui/wizard/ig;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/ii;->a:Lcom/google/googlenav/ui/wizard/ie;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/ie;->a(Lcom/google/googlenav/ui/wizard/ie;)Lcom/google/googlenav/ui/wizard/ih;

    move-result-object v3

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/ih;->b:[Ljava/lang/String;

    aget-object v1, v3, v1

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/ui/wizard/ig;->a(Ljava/lang/String;F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ij;->c:Lcom/google/googlenav/ui/wizard/ii;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ii;->a:Lcom/google/googlenav/ui/wizard/ie;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ie;->a()V

    goto/16 :goto_0
.end method
