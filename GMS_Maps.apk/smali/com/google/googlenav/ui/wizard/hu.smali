.class Lcom/google/googlenav/ui/wizard/hu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/bG;


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hu;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hu;->b:Z

    return-void
.end method

.method private a(Z)V
    .locals 7

    if-eqz p1, :cond_0

    const-string v0, "s"

    :goto_0
    const/16 v1, 0x65

    const-string v2, "r"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "r="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "e"

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hu;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/hu;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hu;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x410

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/hu;->a(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hu;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x40a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
