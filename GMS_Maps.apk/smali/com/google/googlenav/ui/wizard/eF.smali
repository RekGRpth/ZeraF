.class public Lcom/google/googlenav/ui/wizard/eF;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaR/ab;
.implements Lvedroid/support/v4/view/ai;


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/B;

.field private b:LaR/R;

.field private c:Lcom/google/googlenav/ui/wizard/jv;

.field private d:Lvedroid/support/v4/view/ViewPager;

.field private l:Lcom/google/googlenav/ui/wizard/eL;

.field private m:I

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Ljava/util/List;

.field private q:Ljava/util/Map;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/googlenav/ui/view/dialog/bD;

.field private t:Ljava/lang/String;

.field private u:I


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/B;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 2

    const/4 v1, -0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    iput v1, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    iput v1, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->c()LaR/R;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->b:LaR/R;

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->a(LaR/ab;)V

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eF;)Lvedroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(Lvedroid/support/v4/view/PagerTitleStrip;)V
    .locals 4

    invoke-virtual {p1}, Lvedroid/support/v4/view/PagerTitleStrip;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Lvedroid/support/v4/view/PagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const v3, 0x7f0202ce

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    const/16 v3, 0xc8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMinimumWidth(I)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const v3, 0x7f0202cd

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f100086

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->n:Landroid/view/View;

    const v0, 0x7f100087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->n:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eN;

    const/4 v2, -0x1

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/eN;-><init>(Lcom/google/googlenav/ui/wizard/eF;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->o:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eN;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/eN;-><init>(Lcom/google/googlenav/ui/wizard/eF;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/eL;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 6

    const v5, 0x7f02021c

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x2dc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {p0, v0, v1, v5}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/CharSequence;II)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/eG;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/eG;-><init>(Lcom/google/googlenav/ui/wizard/eF;)V

    new-array v1, v4, [I

    const/16 v2, 0xbc0

    aput v2, v1, v3

    invoke-virtual {p0, v4, v5, v0, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(ZILaA/f;[I)V

    return-void
.end method

.method private c(I)Landroid/widget/ListView;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Lvedroid/support/v4/view/ViewPager;->b()Lvedroid/support/v4/view/x;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eL;->c(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eF;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/eF;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/eF;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    return v0
.end method

.method private n()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->b:LaR/R;

    sget-object v1, LaR/O;->c:LaR/O;

    invoke-interface {v0, v2, v1}, LaR/R;->a(ZLaR/O;)V

    const/16 v0, 0x2f3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    return-void
.end method


# virtual methods
.method public F_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->h()V

    return-void
.end method

.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/B;->h()V

    const/4 v0, 0x1

    return v0
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public a(IFI)V
    .locals 0

    return-void
.end method

.method public a(LaR/P;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/eJ;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iget v0, p1, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->c(I)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p1, Lcom/google/googlenav/ui/wizard/eJ;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eM;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eM;-><init>(Lcom/google/googlenav/ui/wizard/eF;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eF;->t:Ljava/lang/String;

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p3}, LaT/a;->a(LaT/m;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eK;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eK;-><init>(Lcom/google/googlenav/ui/wizard/eF;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->as()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/B;->h()V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/eF;->n()V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/B;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x1c

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1001e5 -> :sswitch_1
        0x7f1004b7 -> :sswitch_3
        0x7f1004c1 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 5

    const v4, 0x7f1001e5

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Lvedroid/support/v4/view/ViewPager;->b()Lvedroid/support/v4/view/x;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eL;

    const/16 v1, 0x35b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Lvedroid/support/v4/view/ViewPager;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/eL;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    return v3

    :cond_0
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public b_(I)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->invalidateOptionsMenu()V

    iput p1, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    iget v0, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    if-ne p1, v0, :cond_0

    const-string v0, "v"

    const-string v1, ""

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/16 v0, 0x7b

    const-string v1, "t"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ti="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eK;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eK;->b()V

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400f6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100201

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->b(Landroid/view/View;)V

    const v0, 0x7f10029d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/google/googlenav/ui/wizard/eL;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/eL;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    const v3, 0x7f1000e0

    invoke-virtual {v0, v3}, Lvedroid/support/v4/view/ViewPager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/view/PagerTitleStrip;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Lvedroid/support/v4/view/PagerTitleStrip;)V

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/eF;->a(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eK;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/googlenav/ui/wizard/eK;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/wizard/eL;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    iget-object v4, v0, Lcom/google/googlenav/ui/wizard/eK;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eK;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/ui/wizard/eL;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setAdapter(Lvedroid/support/v4/view/x;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->c(Ljava/lang/String;)V

    :cond_1
    return-object v2
.end method

.method public c_(I)V
    .locals 0

    return-void
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .locals 2

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    if-eqz v0, :cond_0

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-virtual {v0, v1}, LaT/a;->b(LaT/m;)V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/eH;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/eH;-><init>(Lcom/google/googlenav/ui/wizard/eF;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Lvedroid/support/v4/view/ViewPager;->c()I

    move-result v0

    return v0
.end method

.method public m()Lcom/google/googlenav/ui/wizard/eJ;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/eJ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/eJ;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Lvedroid/support/v4/view/ViewPager;->c()I

    move-result v1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    iget v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/eF;->c(I)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->b:I

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/B;->h()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/eF;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110016

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1001e5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x57f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1004c1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x526

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1004b7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x1d9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    const/4 v0, 0x1

    return v0
.end method
