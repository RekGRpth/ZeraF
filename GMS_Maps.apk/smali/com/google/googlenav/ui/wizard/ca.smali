.class public Lcom/google/googlenav/ui/wizard/ca;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dy;


# static fields
.field public static a:[I


# instance fields
.field protected b:I

.field protected c:I

.field protected i:I

.field protected j:Lax/j;

.field protected k:Lcom/google/googlenav/J;

.field private final l:Lax/z;

.field private m:Z

.field private final n:LaH/m;

.field private o:Lcom/google/googlenav/ui/view/p;

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaH/m;Lcom/google/googlenav/J;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->i()V

    return-void
.end method

.method public static H()I
    .locals 5

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->Q()I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "DIRECTIONS_MODE"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v3, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v0, 0x0

    aget-byte v0, v2, v0

    :goto_0
    return v0

    :cond_0
    const-string v2, "DIRECTIONS_MODE"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_0
.end method

.method private I()Lcom/google/googlenav/ui/view/android/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/j;

    return-object v0
.end method

.method private J()V
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/p;

    const/16 v1, 0xfc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget v3, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/p;-><init>(Ljava/lang/String;Lax/j;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    return-void
.end method

.method private K()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    return-void
.end method

.method private L()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    return-void
.end method

.method private M()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    const/16 v2, 0x3c6

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    const/16 v2, 0x3c5

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->g()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static N()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->y()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->A()Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    new-array v0, v0, [I

    sput-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    invoke-static {}, Lcom/google/googlenav/K;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    aput v2, v0, v2

    move v2, v1

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->y()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v3, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v0, v2, 0x1

    aput v1, v3, v2

    move v2, v0

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v1, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v0, v2, 0x1

    const/4 v3, 0x2

    aput v3, v1, v2

    move v2, v0

    :cond_5
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v1, v2, 0x1

    const/4 v1, 0x3

    aput v1, v0, v2

    :cond_6
    return-void

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method private O()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    invoke-static {v1, v0}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, v0}, Lax/j;->a(Lax/y;)V

    :cond_1
    return-void

    :cond_2
    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v0

    goto :goto_0
.end method

.method private P()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->h()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-static {v0}, Lax/y;->a(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    invoke-virtual {v1, v0}, Lax/j;->b(Lax/y;)V

    goto :goto_0
.end method

.method private static Q()I
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->N()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_0
.end method

.method private R()V
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/A;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/ah;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ah;->show()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)V
    .locals 5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "DIRECTIONS_MODE"

    const/4 v2, 0x1

    new-array v2, v2, [B

    const/4 v3, 0x0

    int-to-byte v4, p0

    aput-byte v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    return-void
.end method

.method private c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/j;->o()Lcom/google/googlenav/ui/view/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/j;->l()V

    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/j;-><init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/p;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected A()Ljava/lang/Boolean;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected B()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected C()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->aq()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method protected D()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected E()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->as()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method protected F()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->aq()Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v2}, Lax/j;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v1, v2}, Lax/j;->a(Lax/y;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, v0}, Lax/j;->b(Lax/y;)V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    return-void
.end method

.method public G()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    return-void
.end method

.method public S_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public T_()V
    .locals 0

    return-void
.end method

.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const-string v0, "b"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    return v0
.end method

.method protected a(I)V
    .locals 3

    const/4 v1, 0x3

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    iput p1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    const-string v0, "s"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    const-string v0, "e"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_5
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lax/b;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/b;)I

    move-result v0

    invoke-virtual {p1}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/j;I)V

    return-void
.end method

.method public a(Lax/b;Z)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(I)V

    goto :goto_0
.end method

.method public a(Lax/j;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->O()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    return-void
.end method

.method public a(Lax/j;I)V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    :cond_0
    iput p2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-static {p2}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/j;)V

    return-void
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p3, :cond_0

    const/16 v1, 0xb

    invoke-static {p3, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, p1}, Lax/j;->a(Lax/y;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "ss"

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, p1}, Lax/j;->b(Lax/y;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "es"

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    new-instance v0, Lax/j;

    invoke-direct {v0}, Lax/j;-><init>()V

    invoke-virtual {v0, p1}, Lax/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/j;)V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x4

    const-string v1, "q"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x4

    const-string v3, "q"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x1

    if-nez p2, :cond_0

    move-object v0, v1

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x2

    if-nez p3, :cond_1

    :goto_1
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "is_ad="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Directions"

    invoke-static {v1}, Laj/a;->b(Ljava/lang/String;)V

    :cond_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    const/16 v2, 0x1d

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Navigation"

    invoke-static {v1}, Laj/a;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->z()V

    goto :goto_0

    :sswitch_4
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->F()V

    goto :goto_0

    :sswitch_7
    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xd4 -> :sswitch_0
        0xd5 -> :sswitch_1
        0xd6 -> :sswitch_2
        0xde -> :sswitch_4
        0xdf -> :sswitch_5
        0xe4 -> :sswitch_6
        0xec -> :sswitch_3
        0xb56 -> :sswitch_7
    .end sparse-switch
.end method

.method protected b(Lax/b;)I
    .locals 1

    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lax/x;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lax/i;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    const/4 v0, 0x4

    const-string v1, "q"

    invoke-static {v0, v1, p1}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x0

    iput v3, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->m:Z

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-eq v2, v3, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    :goto_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-nez v0, :cond_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    const-string v1, "route"

    invoke-virtual {v0, v1}, Law/h;->e(Ljava/lang/String;)V

    :cond_0
    const-string v0, "a"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    return-void

    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lax/j;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->i()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->O()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->f()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->P()V

    if-eqz v0, :cond_0

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 9

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-ne v0, v5, :cond_3

    move v4, v5

    :goto_0
    if-eqz v4, :cond_4

    const/16 v0, 0xc5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    xor-int/lit8 v1, v1, 0x4

    :cond_2
    xor-int/lit8 v7, v1, 0x8

    if-eqz v4, :cond_5

    const/16 v1, 0x589

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    :goto_2
    if-eqz v4, :cond_6

    const/4 v1, 0x4

    move v2, v1

    :goto_3
    if-eqz v4, :cond_7

    const/16 v1, 0x5e4

    :goto_4
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v8, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v8}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-virtual {v8, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->b(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void

    :cond_3
    move v4, v6

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0xc3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    const/16 v1, 0x11a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_2

    :cond_6
    const/4 v1, 0x5

    move v2, v1

    goto :goto_3

    :cond_7
    const/16 v1, 0x5e3

    goto :goto_4
.end method

.method protected c()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->R()V

    return-void
.end method

.method public c(Lax/b;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    return-void
.end method

.method public d(Lax/b;)V
    .locals 1

    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lax/b;->aD()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lax/b;->aE()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public e()Lax/b;
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->f()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DirectionsWizard.getRequest() called too early!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    if-ne v0, v1, :cond_1

    new-instance v0, Lax/w;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ca;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    :goto_0
    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    if-nez v2, :cond_4

    :goto_1
    invoke-virtual {v0, v1}, Lax/b;->a(Z)V

    invoke-virtual {v0}, Lax/b;->aM()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    return-object v0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    new-instance v0, Lax/x;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/x;-><init>(Lax/k;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    new-instance v0, Lax/i;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/i;-><init>(Lax/k;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lax/s;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/s;-><init>(Lax/k;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public f()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    :cond_1
    return-void
.end method

.method public h()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, -0x1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "b"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->g()V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v1

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    :cond_1
    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected i()V
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lax/j;

    invoke-direct {v0}, Lax/j;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->N()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->G()V

    return-void
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->M()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "d"

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->A()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->I()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    return-void
.end method

.method protected z()V
    .locals 5

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "n"

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->A()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-static {v3}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "w"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    return-void
.end method
