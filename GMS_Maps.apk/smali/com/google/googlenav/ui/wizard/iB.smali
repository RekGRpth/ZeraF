.class public Lcom/google/googlenav/ui/wizard/iB;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lax/t;)Lax/t;
    .locals 3

    new-instance v0, Lax/t;

    invoke-direct {v0}, Lax/t;-><init>()V

    invoke-virtual {p0}, Lax/t;->E()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/t;->d(I)V

    invoke-virtual {p0}, Lax/t;->y()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lax/t;->a(J)V

    invoke-virtual {p0}, Lax/t;->v()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/t;->a(I)V

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/t;->b(I)V

    return-object v0
.end method

.method public static a(Lax/b;Lcom/google/googlenav/ui/bi;III)Lbj/by;
    .locals 7

    const/4 v3, 0x0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {p0, p2}, Lax/b;->l(I)Lax/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/h;)Ljava/util/Vector;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-virtual {v4, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1, p2, v3}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/b;Lcom/google/googlenav/ui/bi;IZ)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;Z)Ljava/lang/CharSequence;

    move-result-object v2

    :cond_1
    invoke-virtual {p0, p2}, Lax/b;->p(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    move-object v3, v0

    :goto_1
    new-instance v0, Lbj/by;

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v3

    move v4, p4

    move v5, p3

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lbj/by;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V

    return-object v0

    :cond_2
    move-object v3, v0

    goto :goto_1
.end method

.method public static a(Lax/b;Lcom/google/googlenav/ui/bi;IZ)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p2}, Lax/b;->l(I)Lax/h;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/iB;->c(Lax/h;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/t;

    invoke-static {v4, v2}, Lcom/google/googlenav/ui/wizard/iB;->a(Ljava/util/Vector;I)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v3, p3}, Lcom/google/googlenav/ui/wizard/iB;->a(Ljava/lang/StringBuilder;Z)V

    :cond_0
    invoke-static {v3, p1, v0, p3}, Lcom/google/googlenav/ui/wizard/iB;->a(Ljava/lang/StringBuilder;Lcom/google/googlenav/ui/bi;Lax/t;Z)Z

    move-result v0

    or-int/2addr v0, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    if-lez v5, :cond_2

    invoke-virtual {v4}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/t;

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_2

    invoke-static {v3, p1, v0, p3}, Lcom/google/googlenav/ui/wizard/iB;->a(Ljava/lang/StringBuilder;Lcom/google/googlenav/ui/bi;Lax/t;Z)Z

    move-result v0

    or-int/2addr v1, v0

    :cond_2
    sget-boolean v0, Lcom/google/googlenav/android/E;->e:Z

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    if-nez v1, :cond_3

    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lax/h;)Ljava/util/Vector;
    .locals 4

    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/iB;->b(Lax/h;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {p0}, Lax/h;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v2, Lcom/google/googlenav/ui/bi;->A:C

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, v0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method private static a(Ljava/lang/StringBuilder;Z)V
    .locals 1

    if-eqz p1, :cond_0

    sget-char v0, Lcom/google/googlenav/ui/bi;->h:C

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    sget-char v0, Lcom/google/googlenav/ui/bi;->g:C

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/google/googlenav/ui/bi;Lax/t;Z)Z
    .locals 3

    invoke-virtual {p2}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    if-eqz p3, :cond_0

    sget-char v0, Lcom/google/googlenav/ui/bi;->bl:C

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {p2}, Lax/t;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    sget-char v0, Lcom/google/googlenav/ui/bi;->bk:C

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    invoke-virtual {p2}, Lax/t;->y()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static a(Ljava/util/Vector;I)Z
    .locals 5

    invoke-virtual {p0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/t;

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/wizard/iB;->b(Ljava/util/Vector;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iB;->b(Lax/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lax/h;)Ljava/util/Vector;
    .locals 6

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/h;->s()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lax/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const/16 v3, 0x5be

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Lax/h;->l()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lax/h;->k()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->bS:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    const/16 v3, 0x377

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/google/googlenav/ui/aX;->a(Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/aW;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    :cond_1
    :goto_2
    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lax/h;->m()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lax/h;->o()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private static b(Lax/t;)Z
    .locals 2

    invoke-virtual {p0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v0

    const/16 v1, 0x1e0

    if-gt v0, v1, :cond_0

    invoke-virtual {p0}, Lax/t;->v()I

    move-result v0

    const/16 v1, 0x29a

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/Vector;I)Z
    .locals 3

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/t;

    if-ge p1, v1, :cond_0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lax/t;

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lax/h;)Ljava/util/Vector;
    .locals 8

    const/4 v7, 0x2

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/h;->e()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v2}, Lax/h;->b(I)Lax/t;

    move-result-object v4

    invoke-virtual {v4}, Lax/t;->E()I

    move-result v0

    if-ne v0, v7, :cond_3

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v5

    if-ne v5, v7, :cond_2

    invoke-virtual {v0}, Lax/t;->v()I

    move-result v5

    invoke-virtual {v4}, Lax/t;->v()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v0, v5}, Lax/t;->a(I)V

    invoke-virtual {v0}, Lax/t;->x()I

    move-result v5

    invoke-virtual {v4}, Lax/t;->x()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, Lax/t;->b(I)V

    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/t;

    goto :goto_2

    :cond_2
    invoke-static {v4}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/t;)Lax/t;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v1, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method
