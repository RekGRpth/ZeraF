.class public Lcom/google/googlenav/ui/wizard/iY;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;

.field final synthetic b:Lcom/google/googlenav/ui/wizard/iU;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/iU;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iY;->b:Lcom/google/googlenav/ui/wizard/iU;

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iY;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/iY;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    const/16 v3, 0x52e

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iY;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04016a

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iY;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    const v0, 0x7f100025

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iY;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v3, v1, v6}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iY;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbj/H;

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/iY;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/iX;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/iY;->b:Lcom/google/googlenav/ui/wizard/iU;

    invoke-direct {v1, v3, v7}, Lcom/google/googlenav/ui/wizard/iX;-><init>(Lcom/google/googlenav/ui/wizard/iU;Lcom/google/googlenav/ui/wizard/iV;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v2
.end method
