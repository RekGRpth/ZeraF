.class public Lcom/google/googlenav/ui/wizard/gJ;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Lcom/google/googlenav/ui/wizard/gI;

.field private f:LaB/s;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x3cb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->a:Ljava/lang/String;

    const v0, 0x7f020222

    iput v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->b:I

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/gJ;->g:Z

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/gJ;->h:Z

    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->b:I

    return-object p0
.end method

.method public a(LaB/s;)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->f:LaB/s;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->e:Lcom/google/googlenav/ui/wizard/gI;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->c:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->g:Z

    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->c:Ljava/util/List;

    return-object v0
.end method

.method public b()LaB/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->f:LaB/s;

    return-object v0
.end method

.method public b(Z)Lcom/google/googlenav/ui/wizard/gJ;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/gJ;->h:Z

    return-object p0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->g:Z

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->h:Z

    return v0
.end method

.method public e()Lcom/google/googlenav/ui/wizard/gI;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->e:Lcom/google/googlenav/ui/wizard/gI;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->d:Ljava/util/List;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/gJ;->b:I

    return v0
.end method
