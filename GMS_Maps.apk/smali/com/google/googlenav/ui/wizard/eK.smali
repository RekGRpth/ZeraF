.class Lcom/google/googlenav/ui/wizard/eK;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Lcom/google/googlenav/ui/wizard/eI;

.field d:Z

.field final synthetic e:Lcom/google/googlenav/ui/wizard/eF;

.field private f:Lcom/google/googlenav/ui/view/android/J;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/eF;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eK;->e:Lcom/google/googlenav/ui/wizard/eF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/eK;->d:Z

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eK;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/eK;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/eK;->c:Lcom/google/googlenav/ui/wizard/eI;

    return-void
.end method

.method private d()Lcom/google/googlenav/ui/view/android/J;
    .locals 3

    new-instance v1, Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eK;->e:Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x4

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eK;->c:Lcom/google/googlenav/ui/wizard/eI;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eI;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eK;->f:Lcom/google/googlenav/ui/view/android/J;

    return-object v1
.end method


# virtual methods
.method a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/eK;->d:Z

    return-void
.end method

.method b()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/eK;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eK;->f:Lcom/google/googlenav/ui/view/android/J;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eK;->c:Lcom/google/googlenav/ui/wizard/eI;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/eI;->b()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/eK;->d:Z

    :cond_0
    return-void
.end method

.method c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eK;->e:Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/eK;->d()Lcom/google/googlenav/ui/view/android/J;

    move-result-object v1

    const v2, 0x7f0400f9

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eK;->e:Lcom/google/googlenav/ui/wizard/eF;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/eF;->c(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eK;->e:Lcom/google/googlenav/ui/wizard/eF;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/eF;->c(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v2
.end method
