.class public Lcom/google/googlenav/ui/wizard/eO;
.super Lcom/google/googlenav/ui/wizard/B;
.source "SourceFile"


# instance fields
.field i:Lcom/google/googlenav/ui/wizard/dy;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/B;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private a(I)LaR/a;
    .locals 1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/E;

    invoke-virtual {v0, p1}, LaR/E;->a(I)LaR/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eO;->a:Lcom/google/googlenav/J;

    invoke-interface {v0, p1}, Lcom/google/googlenav/J;->e(Ljava/lang/String;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eO;->i:Lcom/google/googlenav/ui/wizard/dy;

    invoke-static {v0}, Lax/y;->c(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    invoke-interface {v2, v0, v1, v3, v3}, Lcom/google/googlenav/ui/wizard/dy;->a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/be;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/be;->f()Lcom/google/googlenav/ui/view/a;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaR/t;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eO;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eO;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->l()I

    move-result v0

    const/16 v1, 0x7b

    const-string v2, "s"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ti="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eO;->a()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eO;->a(I)LaR/a;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/t;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/dy;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eO;->a:Lcom/google/googlenav/J;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eO;->i:Lcom/google/googlenav/ui/wizard/dy;

    const-string v0, "stars"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/googlenav/ui/wizard/eO;->a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V

    return-void
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/eO;->a(Z)V

    return-void
.end method

.method protected b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eO;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eO;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    return-void
.end method
