.class public Lcom/google/googlenav/ui/wizard/bx;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field protected a:Lax/b;

.field protected b:Lcom/google/googlenav/bZ;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    return-void
.end method


# virtual methods
.method public a(Lax/b;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/bx;->b(Lax/b;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public a(Lcom/google/googlenav/bZ;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/bx;->b(Lcom/google/googlenav/bZ;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "al"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected b()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/bA;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bA;-><init>(Lcom/google/googlenav/ui/wizard/bx;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method public b(Lax/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    return-void
.end method

.method public b(Lcom/google/googlenav/bZ;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 4

    const-string v0, "ac"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {p1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bx;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bx;->j()V

    return-void
.end method
