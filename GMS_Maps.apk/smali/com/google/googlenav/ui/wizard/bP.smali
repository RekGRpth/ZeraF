.class public Lcom/google/googlenav/ui/wizard/bP;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/bN;

.field private b:Landroid/widget/Spinner;

.field private final c:Ljava/util/Calendar;

.field private d:Ljava/text/DateFormat;

.field private l:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/bN;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bP;->a:Lcom/google/googlenav/ui/wizard/bN;

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->l:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bP;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bP;->l:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bP;)Ljava/util/Calendar;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->d:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bP;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_0
    return-void
.end method

.method public O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bP;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public P_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(I)Landroid/app/Dialog;
    .locals 6

    const/4 v5, 0x1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    sget-object v1, Lcom/google/googlenav/ui/wizard/bP;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/TimePickerDialog;

    sget-object v1, Lcom/google/googlenav/ui/wizard/bP;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/K;->c()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(ILandroid/app/Dialog;)V
    .locals 4

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    check-cast p2, Landroid/app/DatePickerDialog;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p2, v0, v1, v2}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    :pswitch_1
    check-cast p2, Landroid/app/TimePickerDialog;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/app/TimePickerDialog;->updateTime(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(ILandroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 2

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 9

    const/16 v3, 0x5bd

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bP;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040058

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bP;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->a:Lcom/google/googlenav/ui/wizard/bN;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/bN;->a(Lcom/google/googlenav/ui/wizard/bN;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {v8}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->d:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->d:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->setCalendar(Ljava/util/Calendar;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->g:Lcom/google/googlenav/ui/view/android/ae;

    const v0, 0x7f100160

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bP;->l()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/bQ;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/bQ;-><init>(Lcom/google/googlenav/ui/wizard/bP;)V

    invoke-static {v0, v2, v3, v1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->g:Lcom/google/googlenav/ui/view/android/ae;

    const v0, 0x7f100161

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bP;->h()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/bR;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/bR;-><init>(Lcom/google/googlenav/ui/wizard/bP;)V

    invoke-static {v0, v2, v3, v1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->g:Lcom/google/googlenav/ui/view/android/ae;

    const v0, 0x7f100163

    const/16 v3, 0x5f5

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/wizard/bS;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/wizard/bS;-><init>(Lcom/google/googlenav/ui/wizard/bP;)V

    invoke-static {v0, v3, v4, v1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->g:Lcom/google/googlenav/ui/view/android/ae;

    const v0, 0x7f100164

    const/16 v3, 0x69

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/wizard/bT;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/wizard/bT;-><init>(Lcom/google/googlenav/ui/wizard/bP;)V

    invoke-static {v0, v3, v4, v1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->g:Lcom/google/googlenav/ui/view/android/ae;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ae;->a()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    const v0, 0x7f10015e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->b:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bP;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090008

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0xea

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x3c

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    const/16 v7, 0x20f

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v0, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v3, 0x1090009

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bP;->b:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->b:Landroid/widget/Spinner;

    new-instance v3, Lcom/google/googlenav/ui/wizard/bU;

    invoke-direct {v3, p0, v2}, Lcom/google/googlenav/ui/wizard/bU;-><init>(Lcom/google/googlenav/ui/wizard/bP;Landroid/widget/Button;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->b:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bP;->a:Lcom/google/googlenav/ui/wizard/bN;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/bN;->b(Lcom/google/googlenav/ui/wizard/bN;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    return-object v1

    :cond_1
    const v0, 0x7f100340

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p4}, Ljava/util/Calendar;->set(II)V

    const v1, 0x7f100160

    const v0, 0x7f10015f

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bP;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/bP;->a(ILandroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bP;->c:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    const v1, 0x7f100161

    const v0, 0x7f10015f

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bP;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/bP;->a(ILandroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    return-void
.end method
