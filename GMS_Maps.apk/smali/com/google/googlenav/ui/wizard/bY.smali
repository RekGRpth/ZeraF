.class public Lcom/google/googlenav/ui/wizard/bY;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/bV;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/bV;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/wizard/bY;->p()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public P_()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    if-ne v0, v5, :cond_0

    const/16 v0, 0x47a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    packed-switch v0, :pswitch_data_0

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    move-object v2, v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bY;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401a8

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->an()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    sget-object v4, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bY;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/bV;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/bZ;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    invoke-direct {v2, v4, v3}, Lcom/google/googlenav/ui/wizard/bZ;-><init>(Lcom/google/googlenav/ui/wizard/bV;Lcom/google/googlenav/ui/wizard/bW;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setEnabled(Z)V

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/bY;->a:Lcom/google/googlenav/ui/wizard/bV;

    iget-object v4, v4, Lcom/google/googlenav/ui/wizard/bV;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v4, v2}, Lcom/google/googlenav/ui/view/android/aL;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
