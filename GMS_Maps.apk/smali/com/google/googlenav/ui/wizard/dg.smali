.class public Lcom/google/googlenav/ui/wizard/dg;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/c;


# instance fields
.field private A:Lcom/google/googlenav/ui/view/d;

.field private B:LaN/B;

.field private C:LaN/B;

.field private D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private E:Ljava/util/List;

.field private F:Lcom/google/googlenav/ui/wizard/dy;

.field private G:Z

.field private H:Ljava/lang/String;

.field private I:Z

.field private J:Z

.field private K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private L:Z

.field private M:Z

.field private final N:Lcom/google/googlenav/aA;

.field private final O:Landroid/content/DialogInterface$OnCancelListener;

.field private volatile P:LaH/A;

.field private final Q:Lbf/az;

.field protected a:I

.field protected b:Ljava/lang/String;

.field protected c:B

.field protected final i:LaH/m;

.field protected j:Z

.field protected final k:Landroid/graphics/Point;

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private final v:LaN/p;

.field private final w:LaN/u;

.field private final x:Lcom/google/googlenav/J;

.field private final y:Lcom/google/googlenav/ui/ak;

.field private z:J


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaN/p;LaN/u;LaH/m;Lcom/google/googlenav/aA;Lcom/google/googlenav/J;Lcom/google/googlenav/ui/ak;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    new-instance v0, Lcom/google/googlenav/ui/wizard/dh;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dh;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    iput-object p6, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iput-object p7, p0, Lcom/google/googlenav/ui/wizard/dg;->y:Lcom/google/googlenav/ui/ak;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/dg;->N:Lcom/google/googlenav/aA;

    new-instance v0, Lbf/az;

    invoke-direct {v0, p0}, Lbf/az;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    return-void
.end method

.method private declared-synchronized B()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    invoke-interface {v0, v1}, LaH/m;->b(LaH/A;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private C()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/dC;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dC;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method private D()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/dG;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dG;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method private E()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/dM;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dM;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method private F()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->c()V

    return-void
.end method

.method private G()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x3

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->H()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->F()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->H()V

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    iput v3, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private H()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dy;->T_()V

    :cond_0
    return-void
.end method

.method private I()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->f()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/os/Handler;)Las/b;
    .locals 3

    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dp;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/dp;-><init>(Lcom/google/googlenav/ui/wizard/dg;Landroid/os/Handler;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;
    .locals 3

    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lax/y;->a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    return-object p1
.end method

.method private a(Lax/y;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dy;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/y;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dk;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dk;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->i(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;Lcom/google/googlenav/aZ;)V

    goto :goto_0
.end method

.method private a(Lax/y;Lcom/google/googlenav/aZ;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    invoke-virtual {v0}, LaN/p;->e()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2, p2}, Lcom/google/googlenav/ui/wizard/dy;->a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->e:Lcom/google/googlenav/ui/bi;

    iget-byte v1, p0, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(BC)Lcom/google/googlenav/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/dQ;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v8}, Lcom/google/googlenav/ui/wizard/dQ;->a(Z)V

    :goto_0
    return-void

    :cond_0
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x292

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dl;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/dl;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dQ;)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v8, v1}, LaH/A;->a(ILaH/m;)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/ui/wizard/dQ;)LaH/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1, v0}, LaH/m;->a(LaH/A;)V

    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/os/Handler;)Las/b;

    move-result-object v0

    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->G()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lax/y;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lax/y;Lcom/google/googlenav/aZ;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;Lcom/google/googlenav/aZ;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    return p1
.end method

.method private b(Lcom/google/googlenav/ui/wizard/dQ;)LaH/A;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/dm;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/dm;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dQ;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/dg;)LaH/A;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dr;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/dr;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ui/wizard/dF;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    iget v0, p1, Lcom/google/googlenav/ui/wizard/dF;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/dF;->a()V

    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->q:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    iget v0, p1, Lcom/google/googlenav/ui/wizard/dF;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    iget-byte v0, p1, Lcom/google/googlenav/ui/wizard/dF;->l:B

    iput-byte v0, p0, Lcom/google/googlenav/ui/wizard/dg;->c:B

    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->J:Z

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->r:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->M:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/dF;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/dF;->f:I

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    invoke-static {p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, p1, v3}, Lcom/google/googlenav/ui/wizard/dy;->a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    return-void
.end method

.method private c(Lcom/google/googlenav/ui/r;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v1, v2, v0}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v1, v2, v0}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/dg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->B()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->q:Ljava/lang/String;

    return-object v0
.end method

.method public static e()I
    .locals 1

    const/16 v0, 0x1f

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/dg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->M:Z

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/dg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/dg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    return v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/dg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/dg;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    return v0
.end method


# virtual methods
.method protected A()V
    .locals 3

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->y()Lcom/google/googlenav/ui/wizard/eO;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/eO;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/dy;)V

    return-void
.end method

.method public a(LaN/B;)I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v0, v1}, Lbf/az;->a(LaN/B;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    :cond_0
    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/a;)I
    .locals 3

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v0

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x32b

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(IILjava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    const v1, 0x1869f

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lat/b;)I
    .locals 3

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    return v0

    :pswitch_0
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    invoke-virtual {v0}, Lbf/az;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/dz;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/dz;-><init>(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v0, p1, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->e()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x53f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/ds;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/ds;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x11b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dt;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dt;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_1
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x53e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/du;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/du;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x540

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dv;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dv;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_3
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x2dc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/di;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/di;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_4
    return-object v0
.end method

.method protected a(II)V
    .locals 8

    const/4 v7, 0x3

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    iput p1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    if-eq p1, v7, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    :cond_2
    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->I()V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_4
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/dg;->z:J

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_5
    const-string v0, ""

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->H:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Ljava/lang/String;)V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x4f3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    :cond_4
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->D()V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->E()V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method protected a(LaN/B;I)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/bg;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/friend/bg;->f(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/dP;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/dP;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->J:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dF;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    const-class v2, Lcom/google/googlenav/ui/wizard/EnterAddressActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dB;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/wizard/dB;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dh;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void
.end method

.method protected a(I)Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->A()V

    move v0, v1

    goto :goto_0

    :sswitch_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    goto :goto_0

    :sswitch_2
    check-cast p3, Ljava/lang/String;

    invoke-static {p3}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    goto :goto_0

    :sswitch_3
    if-ne p2, v2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :sswitch_4
    check-cast p3, Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(Ljava/lang/String;)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_2

    :sswitch_5
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x9

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/ui/wizard/dg;->a(II)V

    move v0, v1

    goto :goto_0

    :sswitch_7
    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    :cond_3
    move v0, v1

    goto :goto_0

    :sswitch_9
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1f5 -> :sswitch_8
        0x1f6 -> :sswitch_9
        0x327 -> :sswitch_2
        0x328 -> :sswitch_3
        0x329 -> :sswitch_4
        0x32b -> :sswitch_0
        0x32c -> :sswitch_1
        0x32d -> :sswitch_5
        0x32e -> :sswitch_7
        0x32f -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    const v1, 0x1869f

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->D()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->C()V

    goto :goto_0
.end method

.method protected b(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(II)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lcom/google/googlenav/ui/r;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/ui/r;)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/r;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->H:Ljava/lang/String;

    invoke-static {p1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)Z
    .locals 5

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    iget-wide v1, p0, Lcom/google/googlenav/ui/wizard/dg;->z:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    invoke-static {v1, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected c()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->F()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->j()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lbf/by;->e(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->a()V

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    return-void
.end method

.method protected f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    return v0
.end method

.method protected g()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/dj;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dj;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dQ;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->G()V

    return-void
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    return-void
.end method

.method public k()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x8

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public p()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->N:Lcom/google/googlenav/aA;

    check-cast v0, Lcom/google/googlenav/android/d;

    new-instance v1, Lcom/google/googlenav/ui/wizard/dx;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/dx;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dh;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Lcom/google/googlenav/android/T;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    return-void
.end method

.method public z()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    return-void
.end method
