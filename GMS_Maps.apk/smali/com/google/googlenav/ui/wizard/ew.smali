.class public Lcom/google/googlenav/ui/wizard/ew;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private a:Lcom/google/googlenav/ui/view/dialog/bl;

.field private b:Lcom/google/googlenav/ui/view/android/aL;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ew;->a:Lcom/google/googlenav/ui/view/dialog/bl;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/android/aL;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->j()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->a()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->h()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3f9
        :pswitch_0
    .end packed-switch
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->a:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->n()V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->a()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
