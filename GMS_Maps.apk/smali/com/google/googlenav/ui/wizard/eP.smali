.class public Lcom/google/googlenav/ui/wizard/eP;
.super Lcom/google/googlenav/ui/wizard/B;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/googlenav/ui/wizard/eJ;


# instance fields
.field private j:Lcom/google/googlenav/ui/wizard/eX;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/B;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private a(LaR/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-nez p1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, LaR/a;->f()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {p1}, LaR/a;->g()I

    move-result v3

    mul-int/lit8 v3, v3, 0xa

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p1}, LaR/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;)Lcom/google/googlenav/ui/wizard/eX;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;Lcom/google/googlenav/ui/wizard/eX;)Lcom/google/googlenav/ui/wizard/eX;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    return-object p1
.end method

.method private a(I)V
    .locals 8

    const/4 v5, 0x1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x2dd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, LaR/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x2de

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/eU;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/ui/wizard/eU;-><init>(Lcom/google/googlenav/ui/wizard/eP;I)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_0
.end method

.method private a(LaR/D;)V
    .locals 4

    const/16 v3, 0x1c

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->e(Ljava/lang/String;)Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v0, v1, v3}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/ai;I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;ZI)V

    goto :goto_0
.end method

.method private a(LaR/D;Ljava/util/List;)V
    .locals 4

    invoke-virtual {p1}, LaR/D;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2e0

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    return-void

    :cond_0
    const/16 v0, 0x2dd

    goto :goto_0
.end method

.method private a(LaR/L;)V
    .locals 4

    const/16 v3, 0x1c

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, LaR/L;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "30"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method private a(LaR/h;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/h;->e()Lax/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lax/b;)V

    return-void
.end method

.method private a(LaR/t;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->d(Ljava/lang/String;)Lcom/google/googlenav/ag;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    return-void
.end method

.method private a(LaR/t;Ljava/util/List;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    new-instance v0, LaX/a;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eT;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/eT;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V

    invoke-direct {v0, p2, v1}, LaX/a;-><init>(Ljava/util/List;LaX/b;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(LaR/t;Ljava/util/List;Ljava/util/List;)V
    .locals 5

    invoke-virtual {p1}, LaR/t;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x2e1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "<br/>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "<br/>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .locals 4

    if-nez p2, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/wizard/eW;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/eW;-><init>(Lcom/google/googlenav/ui/wizard/eP;)V

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/wizard/iT;->a:Lcom/google/googlenav/ui/wizard/iT;

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/view/a;)V
    .locals 8

    const/4 v7, 0x2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->m()Lcom/google/googlenav/ui/wizard/eJ;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v2

    if-ne v1, v7, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->l()I

    move-result v0

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "at="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ai="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ti="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x7b

    const-string v3, "a"

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->f(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->b(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->e(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/D;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/D;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/t;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;Lcom/google/googlenav/ui/view/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/ui/view/a;)V

    return-void
.end method

.method private a(Ljava/lang/String;LaR/t;Ljava/util/List;)V
    .locals 8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x2de

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/eS;

    invoke-direct {v7, p0, p3, p2}, Lcom/google/googlenav/ui/wizard/eS;-><init>(Lcom/google/googlenav/ui/wizard/eP;Ljava/util/List;LaR/t;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eP;I)LaR/a;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 8

    invoke-static {p1}, LaR/a;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/eV;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/eV;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/n;)V

    sget-object v2, LaR/O;->d:LaR/O;

    invoke-interface {v0, v7, v1, v2}, LaR/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    return-void
.end method

.method private b(LaR/D;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/eR;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/eR;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    invoke-static {p1, v0}, LaX/c;->a(LaR/t;LaX/d;)LaX/c;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private b(LaR/t;)V
    .locals 7

    const-wide/16 v4, 0x0

    invoke-virtual {p1}, LaR/t;->j()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/t;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/eQ;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/eQ;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V

    invoke-static {p1, v0}, LaX/c;->a(LaR/t;LaX/d;)LaX/c;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/ui/view/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/D;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(LaR/t;)V

    return-void
.end method

.method private c(I)V
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->B_()V

    goto :goto_0
.end method

.method private c(LaR/D;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private c(LaR/t;)V
    .locals 4

    invoke-virtual {p1}, LaR/t;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2dd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    return-void
.end method

.method private c(Lcom/google/googlenav/ui/view/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/D;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/D;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(I)LaR/a;
    .locals 1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/E;

    invoke-virtual {v0, p1}, LaR/E;->a(I)LaR/a;

    move-result-object v0

    return-object v0
.end method

.method private d(LaR/t;)V
    .locals 5

    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->d()[LaR/n;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    if-eqz v4, :cond_0

    invoke-interface {v4}, LaR/n;->e()LaR/u;

    move-result-object v4

    invoke-interface {v4, v1}, LaR/u;->e(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->h()V

    :cond_2
    return-void
.end method

.method private d(Lcom/google/googlenav/ui/view/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/h;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e(Lcom/google/googlenav/ui/view/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/L;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f(Lcom/google/googlenav/ui/view/a;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->c(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(LaN/B;)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method protected a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->f()Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/J;)V
    .locals 2

    const-string v0, "stars"

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lcom/google/googlenav/ui/wizard/B;->a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V

    sget-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    sget-object v1, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(Lcom/google/googlenav/ui/wizard/eJ;)V

    :cond_0
    return-void
.end method

.method protected b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    const/4 v1, 0x0

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->f()Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    goto :goto_0

    :pswitch_0
    new-instance v1, Lcom/google/googlenav/ui/wizard/fc;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fc;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->show()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/google/googlenav/ui/wizard/fa;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fa;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_1

    :pswitch_2
    new-instance v1, Lcom/google/googlenav/ui/wizard/fb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fb;-><init>(Lcom/google/googlenav/ui/wizard/eP;I)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_1

    :pswitch_3
    new-instance v1, Lcom/google/googlenav/ui/wizard/eZ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/eZ;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/h;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_1

    :pswitch_4
    new-instance v1, Lcom/google/googlenav/ui/wizard/fd;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fd;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/L;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected c()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/B;->c()V

    return-void
.end method

.method public h()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->dismiss()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    :goto_0
    return-void

    :cond_0
    sput-object v1, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    goto :goto_0
.end method
