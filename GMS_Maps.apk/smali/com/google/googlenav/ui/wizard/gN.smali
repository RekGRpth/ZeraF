.class Lcom/google/googlenav/ui/wizard/gN;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/gT;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/gK;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/gK;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/gK;Lcom/google/googlenav/ui/wizard/gL;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gN;-><init>(Lcom/google/googlenav/ui/wizard/gK;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3cf

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/gU;->b(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x18a

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;)V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/gK;->c(Lcom/google/googlenav/ui/wizard/gK;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/gK;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/gK;->d(Lcom/google/googlenav/ui/wizard/gK;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/gU;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gN;->a:Lcom/google/googlenav/ui/wizard/gK;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gU;->l()V

    :cond_1
    return-void
.end method
