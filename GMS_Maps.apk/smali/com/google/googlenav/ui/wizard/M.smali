.class public Lcom/google/googlenav/ui/wizard/M;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/Q;

.field private b:Lcom/google/googlenav/ui/wizard/S;

.field private final c:Lcom/google/googlenav/ui/wizard/aF;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/aF;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/M;->c:Lcom/google/googlenav/ui/wizard/aF;

    return-void
.end method

.method private a(Landroid/app/ActionBar;)V
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f020216

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/wizard/P;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/P;-><init>(Lcom/google/googlenav/ui/wizard/M;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/M;Landroid/app/ActionBar;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/M;->a(Landroid/app/ActionBar;)V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/M;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/M;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/M;->g:I

    return v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/Q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public b()V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/S;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/Q;->a(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/Q;->a(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->m()Lbf/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v4}, Lcom/google/googlenav/ui/wizard/Q;->b(Lcom/google/googlenav/ui/wizard/Q;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/M;->c:Lcom/google/googlenav/ui/wizard/aF;

    new-instance v6, Lcom/google/googlenav/ui/wizard/N;

    invoke-direct {v6, p0}, Lcom/google/googlenav/ui/wizard/N;-><init>(Lcom/google/googlenav/ui/wizard/M;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/S;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;Lbf/a;ZLcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/ac;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    new-instance v0, Lcom/google/googlenav/ui/wizard/an;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/Q;->c(Lcom/google/googlenav/ui/wizard/Q;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/Q;->d(Lcom/google/googlenav/ui/wizard/Q;)Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/wizard/an;-><init>(Lcom/google/googlenav/ui/e;ZZ)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/an;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/S;->a()Lcom/google/googlenav/ui/wizard/aC;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(Lcom/google/googlenav/ui/wizard/aC;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/ui/wizard/an;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/O;-><init>(Lcom/google/googlenav/ui/wizard/M;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/Q;->e(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/h;)V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/ui/wizard/an;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(Lcom/google/googlenav/ui/wizard/aC;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/Q;->a(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    :cond_2
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->j()V

    return-void
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/Q;->f(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/wizard/R;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/Q;->f(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/wizard/R;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/R;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->a()V

    return-void
.end method

.method f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/Q;->f(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/wizard/R;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->a:Lcom/google/googlenav/ui/wizard/Q;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/Q;->f(Lcom/google/googlenav/ui/wizard/Q;)Lcom/google/googlenav/ui/wizard/R;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/R;->b()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->a()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/M;->b:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/S;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/M;->f()V

    const-string v0, "cy"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->d(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
