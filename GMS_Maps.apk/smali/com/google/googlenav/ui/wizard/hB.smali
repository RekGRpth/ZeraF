.class public Lcom/google/googlenav/ui/wizard/hB;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/hy;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hy;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    new-instance v2, Lcom/google/googlenav/ui/wizard/hD;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hB;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/google/googlenav/ui/wizard/hD;-><init>(Lcom/google/googlenav/ui/wizard/hy;Landroid/content/Context;Lcom/google/googlenav/ui/wizard/hz;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/ui/wizard/hy;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hy;->b(Lcom/google/googlenav/ui/wizard/hy;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hy;->c(Lcom/google/googlenav/ui/wizard/hy;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/hC;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hC;-><init>(Lcom/google/googlenav/ui/wizard/hB;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method


# virtual methods
.method public I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hB;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x436

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hB;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto :goto_0
.end method

.method public O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hB;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hB;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/ui/wizard/hy;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hB;->a:Lcom/google/googlenav/ui/wizard/hy;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/ui/wizard/hy;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040159

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x436

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/hB;->a(Landroid/view/View;)V

    return-object v1
.end method
