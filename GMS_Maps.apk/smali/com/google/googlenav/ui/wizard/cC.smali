.class public Lcom/google/googlenav/ui/wizard/cC;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private b:Lbf/am;

.field private final c:Lam/g;

.field private final i:Lan/f;

.field private j:Lcom/google/googlenav/ui/av;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v1, Lcom/google/googlenav/ui/bi;->ai:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->b:Lbf/am;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/ui/view/android/a;
    .locals 8

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/layer/m;->a(I)Lcom/google/googlenav/e;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    :goto_1
    check-cast v0, Lan/f;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-char v0, Lcom/google/googlenav/ui/bi;->aj:C

    :goto_2
    invoke-interface {v3, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    move-object v3, v0

    :goto_3
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x3fb

    move v6, v0

    :goto_4
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/googlenav/J;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_5
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, p1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    move-object v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    sget-char v0, Lcom/google/googlenav/ui/bi;->ad:C

    goto :goto_2

    :cond_4
    const/16 v0, 0x3f6

    move v6, v0

    goto :goto_4

    :cond_5
    move-object v4, v2

    goto :goto_5

    :cond_6
    move-object v3, v0

    goto :goto_3
.end method

.method private a(Lbf/i;)Z
    .locals 2

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    instance-of v0, p1, Lbf/y;

    if-eqz v0, :cond_2

    check-cast p1, Lbf/y;

    invoke-virtual {p1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/layer/m;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/layer/m;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LayerWikipedia"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x23a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v4, Lcom/google/googlenav/ui/bi;->af:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-interface {v4}, Lcom/google/googlenav/J;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_0
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3f0

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v4, v2

    goto :goto_0
.end method

.method private d(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x23d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v4, Lcom/google/googlenav/ui/bi;->ag:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-interface {v4}, Lcom/google/googlenav/J;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_0
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3f1

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v4, v2

    goto :goto_0
.end method

.method private e(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x23b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v4, Lcom/google/googlenav/ui/bi;->aA:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-interface {v4}, Lcom/google/googlenav/J;->b()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_0
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3f3

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v4, v2

    goto :goto_0
.end method

.method private f(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x23c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v4, Lcom/google/googlenav/ui/bi;->av:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-interface {v4}, Lcom/google/googlenav/J;->b()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_0
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3f4

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v4, v2

    goto :goto_0
.end method

.method private g(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x232

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->c:Lam/g;

    sget-char v4, Lcom/google/googlenav/ui/bi;->ax:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->a:Lcom/google/googlenav/J;

    invoke-interface {v4}, Lcom/google/googlenav/J;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    :goto_0
    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3f7

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v4, v2

    goto :goto_0
.end method

.method private h(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private i(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerWikipedia"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 12

    new-instance v10, Lcom/google/googlenav/ui/view/android/J;

    const/4 v0, 0x1

    invoke-direct {v10, p1, v0}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->b:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->J()Ljava/util/Vector;

    move-result-object v11

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v9, v0, :cond_2

    invoke-virtual {v11, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbf/i;

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/wizard/cC;->a(Lbf/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/4 v1, 0x0

    invoke-virtual {v4}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v4}, Lbf/i;->aL()Lam/f;

    move-result-object v4

    check-cast v4, Lan/f;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v7, 0x3e9

    const/4 v8, 0x0

    invoke-direct {v6, v7, v9, v8}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->aA:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->v:Lcom/google/googlenav/ui/aV;

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {v10, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    invoke-virtual {v4}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4}, Lbf/i;->aL()Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->i:Lan/f;

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3e9

    const/4 v7, 0x0

    invoke-direct {v5, v6, v9, v7}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v10, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->d(Lcom/google/googlenav/ui/view/android/J;)V

    :cond_3
    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->e(Lcom/google/googlenav/ui/view/android/J;)V

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->f(Lcom/google/googlenav/ui/view/android/J;)V

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->h(Lcom/google/googlenav/ui/view/android/J;)V

    invoke-virtual {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/view/android/J;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->c(Lcom/google/googlenav/ui/view/android/J;)V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->p()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/view/android/J;)V

    :cond_5
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->g(Lcom/google/googlenav/ui/view/android/J;)V

    :cond_6
    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/wizard/cC;->i(Lcom/google/googlenav/ui/view/android/J;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->b:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->K()Ljava/util/Vector;

    move-result-object v11

    const/4 v0, 0x0

    move v9, v0

    :goto_2
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v9, v0, :cond_9

    invoke-virtual {v11, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbf/i;

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/wizard/cC;->a(Lbf/i;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_2

    :cond_7
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/4 v1, 0x0

    invoke-virtual {v4}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v4}, Lbf/i;->aL()Lam/f;

    move-result-object v4

    check-cast v4, Lan/f;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v7, 0x3ea

    const/4 v8, 0x0

    invoke-direct {v6, v7, v9, v8}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->aA:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->v:Lcom/google/googlenav/ui/aV;

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {v10, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_3

    :cond_8
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    invoke-virtual {v4}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4}, Lbf/i;->aL()Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3ea

    const/4 v7, 0x0

    invoke-direct {v5, v6, v9, v7}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v10, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_3

    :cond_9
    return-object v10
.end method

.method protected a(Landroid/view/View;)V
    .locals 5

    const v1, 0x7f100031

    const v2, 0x7f100030

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0xd2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cC;->b:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->O()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    new-instance v1, Lcom/google/googlenav/ui/wizard/cD;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/cD;-><init>(Lcom/google/googlenav/ui/wizard/cC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/cE;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/cE;-><init>(Lcom/google/googlenav/ui/wizard/cC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public a(Lbf/am;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cC;->b:Lbf/am;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cC;->j()V

    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x2d7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cC;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v3

    sget-char v4, Lcom/google/googlenav/ui/bi;->ad:C

    invoke-interface {v3, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/cC;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v4

    sget-char v5, Lcom/google/googlenav/ui/bi;->aq:C

    invoke-interface {v4, v5}, Lam/g;->e(C)Lam/f;

    move-result-object v4

    check-cast v4, Lan/f;

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/16 v6, 0x3ed

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7, v2}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    return-void
.end method

.method b(Lbf/am;)Lcom/google/googlenav/ui/av;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->j:Lcom/google/googlenav/ui/av;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/av;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cC;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cC;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/googlenav/ui/av;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lbf/am;Lcom/google/googlenav/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->j:Lcom/google/googlenav/ui/av;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->j:Lcom/google/googlenav/ui/av;

    return-object v0
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/cF;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/cF;-><init>(Lcom/google/googlenav/ui/wizard/cC;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method b(Lcom/google/googlenav/ui/view/android/J;)V
    .locals 7

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v2

    const-string v0, "__LAYERS"

    invoke-virtual {v2, v0}, Lcom/google/googlenav/layer/f;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/layer/i;->b()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    invoke-virtual {v2, v4}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v6

    invoke-virtual {v2, v4}, Lcom/google/googlenav/layer/f;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v6}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v6}, Lcom/google/googlenav/layer/m;->l()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/layer/m;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected c()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cC;->j:Lcom/google/googlenav/ui/av;

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    return-void
.end method
