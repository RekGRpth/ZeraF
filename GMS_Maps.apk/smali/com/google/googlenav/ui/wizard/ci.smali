.class public Lcom/google/googlenav/ui/wizard/ci;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/googlenav/aw;

.field private final b:I

.field private final c:Z

.field private final d:Lcom/google/googlenav/android/aa;

.field private final l:Lcom/google/googlenav/ui/wizard/jv;

.field private m:Ljava/util/Timer;

.field private final n:Landroid/view/animation/Animation;

.field private final o:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>([Lcom/google/googlenav/aw;IZLcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/e;)V
    .locals 2

    const v0, 0x1030009

    invoke-direct {p0, p6, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->n:Landroid/view/animation/Animation;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050011

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->o:Landroid/view/animation/Animation;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ci;->a:[Lcom/google/googlenav/aw;

    iput p2, p0, Lcom/google/googlenav/ui/wizard/ci;->b:I

    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/ci;->c:Z

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/ci;->d:Lcom/google/googlenav/android/aa;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/ci;->l:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ci;->h()V

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->n:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->m:Ljava/util/Timer;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->m:Ljava/util/Timer;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cm;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/cm;-><init>(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ci;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ci;)[Lcom/google/googlenav/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->a:[Lcom/google/googlenav/aw;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/ci;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->l:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ci;->d:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/cn;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/cn;-><init>(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ci;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/ci;)Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->o:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 10

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ci;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04013b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f100365

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/Gallery;

    const v0, 0x7f100089

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f10001e

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v0, 0x7f100367

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v0, 0x7f100366

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v0, Lcom/google/googlenav/ui/wizard/co;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ci;->a:[Lcom/google/googlenav/aw;

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/ci;->d:Lcom/google/googlenav/android/aa;

    iget-boolean v9, p0, Lcom/google/googlenav/ui/wizard/ci;->c:Z

    invoke-direct {v0, v6, v1, v8, v9}, Lcom/google/googlenav/ui/wizard/co;-><init>(Landroid/widget/Gallery;[Lcom/google/googlenav/aw;Lcom/google/googlenav/android/aa;Z)V

    invoke-virtual {v6, v0}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ci;->b:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ci;->b:I

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ci;->a:[Lcom/google/googlenav/aw;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ci;->b:I

    invoke-virtual {v6, v0}, Landroid/widget/Gallery;->setSelection(I)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/cj;

    invoke-direct {v0, p0, v3}, Lcom/google/googlenav/ui/wizard/cj;-><init>(Lcom/google/googlenav/ui/wizard/ci;Landroid/widget/TextView;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/ck;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/wizard/ck;-><init>(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;)V

    invoke-virtual {v6, v0}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/cl;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/cl;-><init>(Lcom/google/googlenav/ui/wizard/ci;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v6, v0}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-object v7
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->m:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->m:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ci;->m:Ljava/util/Timer;

    :cond_0
    return-void
.end method
