.class public Lcom/google/googlenav/ui/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lam/g;


# static fields
.field static final a:C


# instance fields
.field final b:I

.field private c:Ljava/util/List;

.field private d:[J

.field private e:Ljava/util/Map;

.field private f:Ljava/util/Map;

.field private g:I

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const v0, 0xec32

    const v1, 0xecff

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-char v0, v0

    sput-char v0, Lcom/google/googlenav/ui/m;->a:C

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/m;->h:Z

    sget-char v0, Lcom/google/googlenav/ui/m;->a:C

    const v1, 0xec00

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/m;->b:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->b()V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/m;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bA;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz p0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->f()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->e()[J

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    const/4 v3, 0x3

    aget-wide v4, v2, v0

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-static {}, Lcom/google/googlenav/ui/m;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method private e(J)Z
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(J)V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-char v0, v0, Lcom/google/googlenav/ui/o;->d:C

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->f:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/D;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    return-object v0
.end method

.method private k()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/m;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->i()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(J)C
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0xec00

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/ui/o;->a(Lcom/google/googlenav/ui/o;)C

    move-result v0

    goto :goto_0
.end method

.method public a([BJ)Lcom/google/googlenav/ui/o;
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object p1, v0, Lcom/google/googlenav/ui/o;->a:[B

    move-object v5, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-char v4, v0

    new-instance v0, Lcom/google/googlenav/ui/o;

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/o;-><init>([BJCLcom/google/googlenav/ui/n;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->f:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Set;)Ljava/util/Set;
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->c()I

    move-result v0

    new-array v5, v0, [J

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v2, v7, v9

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->c()I

    move-result v2

    if-ge v3, v2, :cond_1

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    aput-wide v7, v5, v3

    move v0, v2

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :cond_2
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v3

    goto :goto_1

    :cond_3
    invoke-direct {p0, v6, v7}, Lcom/google/googlenav/ui/m;->e(J)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v6, v7}, Lcom/google/googlenav/ui/m;->f(J)V

    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/m;->d:[J

    array-length v0, v0

    if-ge v4, v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->d:[J

    aget-wide v6, v0, v4

    cmp-long v0, v6, v9

    if-nez v0, :cond_3

    :cond_6
    iput-object v5, p0, Lcom/google/googlenav/ui/m;->d:[J

    return-object v1

    :cond_7
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    array-length v0, v5

    if-ge v3, v0, :cond_8

    add-int/lit8 v0, v3, 0x1

    aput-wide v6, v5, v3

    move v3, v0

    goto :goto_2

    :cond_8
    invoke-direct {p0, v6, v7}, Lcom/google/googlenav/ui/m;->f(J)V

    goto :goto_2
.end method

.method public a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    iget v0, p0, Lcom/google/googlenav/ui/m;->g:I

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->b()V

    iput p1, p0, Lcom/google/googlenav/ui/m;->g:I

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V
    .locals 8

    const/4 v7, 0x1

    if-eqz p1, :cond_0

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/m;->a(I)V

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/m;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    if-eqz p1, :cond_3

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    if-eqz v1, :cond_1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v3

    invoke-virtual {p0, v3, v4, v5}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 8

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v0, 0x11

    iget v3, p0, Lcom/google/googlenav/ui/m;->g:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    aget-wide v3, v3, v0

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-nez v5, :cond_2

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    const/4 v3, 0x4

    if-le v0, v3, :cond_1

    const/4 v3, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v0, p1}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/m;->e(J)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v5, 0x2

    iget-wide v6, v3, Lcom/google/googlenav/ui/o;->c:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v5, 0x3

    iget-object v3, v3, Lcom/google/googlenav/ui/o;->a:[B

    invoke-virtual {v4, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(C)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/m;->f(C)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(CLam/e;II)Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/m;->f(C)Lcom/google/googlenav/ui/o;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v1}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v0

    invoke-interface {p2, v0, p3, p4}, Lam/e;->a(Lam/f;II)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(C)I
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/m;->f(C)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->b()I

    move-result v0

    goto :goto_0
.end method

.method public b(J)Lam/f;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    const v1, 0xec00

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/m;->g:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/m;->f:Ljava/util/Map;

    const/4 v0, 0x0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/googlenav/ui/m;->d:[J

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/m;->c()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/m;->c:Ljava/util/List;

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/googlenav/ui/m;->b:I

    add-int/2addr v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/m;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/googlenav/ui/m;->h:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    :try_start_0
    invoke-interface {v0, p1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/m;->g:I

    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [J

    iput-object v0, p0, Lcom/google/googlenav/ui/m;->d:[J

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    aput-wide v5, v3, v0

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/m;->b:I

    return v0
.end method

.method public c(C)I
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/m;->f(C)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    goto :goto_0
.end method

.method protected c(J)Lcom/google/googlenav/ui/o;
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->f:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/o;

    return-object v0
.end method

.method public d(C)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/ui/m;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public d(J)[B
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/m;->c(J)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/googlenav/ui/o;->a:[B

    goto :goto_0
.end method

.method public e(C)Lam/f;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/m;->f(C)Lcom/google/googlenav/ui/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/ui/o;->b(Lcom/google/googlenav/ui/o;)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public e()[J
    .locals 7

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    aget-wide v3, v3, v0

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    :cond_0
    new-array v4, v2, [J

    move v3, v1

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->d:[J

    aget-wide v5, v1, v3

    invoke-direct {p0, v5, v6}, Lcom/google/googlenav/ui/m;->e(J)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/googlenav/ui/m;->d:[J

    aget-wide v3, v3, v0

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/m;->e(J)Z

    move-result v3

    if-nez v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v0, 0x1

    aput-wide v5, v4, v0

    move v0, v1

    goto :goto_2

    :cond_4
    return-object v4
.end method

.method public f()I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    iget v0, p0, Lcom/google/googlenav/ui/m;->g:I

    return v0
.end method

.method protected f(C)Lcom/google/googlenav/ui/o;
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/m;->k()V

    iget-object v0, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/o;

    return-object v0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/m;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/o;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/o;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v2, "DownloadedIconProvider"

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public h()V
    .locals 1

    const-string v0, "SAVED_REMOTE_ICONS_DATA_BLOCK"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/m;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected i()V
    .locals 1

    const-string v0, "SAVED_REMOTE_ICONS_DATA_BLOCK"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/m;->b(Ljava/lang/String;)V

    return-void
.end method
