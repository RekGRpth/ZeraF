.class public Lcom/google/googlenav/ui/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaM/h;
.implements Lcom/google/googlenav/J;
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/ui/android/D;


# static fields
.field private static aE:Lbm/i;

.field private static aF:Lbm/i;

.field private static aj:Ljava/lang/String;


# instance fields
.field private final A:Lcom/google/googlenav/friend/j;

.field private final B:LaN/p;

.field private final C:LaH/m;

.field private final D:LaN/V;

.field private final E:LaN/k;

.field private final F:Lcom/google/googlenav/j;

.field private G:I

.field private H:J

.field private I:I

.field private J:J

.field private K:J

.field private L:J

.field private M:J

.field private N:Z

.field private final O:Lcom/google/googlenav/ui/X;

.field private P:Z

.field private final Q:Lcom/google/googlenav/ui/W;

.field private R:LaN/B;

.field private final S:Lcom/google/googlenav/ui/p;

.field private T:I

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:J

.field private Z:J

.field a:Z

.field private aA:Z

.field private aB:LaB/o;

.field private final aC:Lcom/google/googlenav/ui/ac;

.field private final aD:Lcom/google/googlenav/ui/S;

.field private final aG:Lbm/i;

.field private final aH:Lbm/i;

.field private final aI:Lcom/google/googlenav/friend/a;

.field private aJ:Lcom/google/googlenav/ui/android/r;

.field private final aa:Lcom/google/googlenav/A;

.field private final ab:Las/c;

.field private final ac:LaR/b;

.field private final ad:Lcom/google/googlenav/ui/wizard/jv;

.field private final ae:Lbf/am;

.field private final af:Lcom/google/googlenav/ui/wizard/z;

.field private final ag:Lav/a;

.field private final ah:Lcom/google/googlenav/L;

.field private final ai:Lcom/google/googlenav/mylocationnotifier/k;

.field private ak:LaN/B;

.field private final al:Landroid/graphics/Point;

.field private am:Z

.field private an:Lcom/google/googlenav/ui/R;

.field private final ao:Lat/c;

.field private ap:Z

.field private aq:Z

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Lcom/google/googlenav/ui/r;

.field private au:Lcom/google/googlenav/ui/b;

.field private final av:LaN/u;

.field private aw:Z

.field private ax:Ljava/lang/Boolean;

.field private final ay:LaN/v;

.field private final az:Lcom/google/googlenav/ui/aC;

.field protected b:Lcom/google/googlenav/ui/ak;

.field volatile c:Z

.field protected final d:Lcom/google/googlenav/offers/a;

.field protected volatile e:Z

.field public final f:Lbm/i;

.field private final g:Lcom/google/googlenav/ui/android/L;

.field private final h:Lcom/google/googlenav/android/aa;

.field private i:Z

.field private j:Z

.field private k:Las/d;

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private p:Lat/a;

.field private q:Z

.field private final r:Lcom/google/googlenav/aA;

.field private final s:Lcom/google/googlenav/common/io/j;

.field private t:Lcom/google/googlenav/ui/as;

.field private final u:Lcom/google/googlenav/ui/bE;

.field private final v:Lcom/google/googlenav/ui/bF;

.field private final w:Lcom/google/googlenav/ui/aD;

.field private final x:Lcom/google/googlenav/friend/J;

.field private final y:Lcom/google/googlenav/friend/p;

.field private final z:Lcom/google/googlenav/friend/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x16

    const-string v0, ""

    sput-object v0, Lcom/google/googlenav/ui/s;->aj:Ljava/lang/String;

    new-instance v0, Lbm/i;

    const-string v1, "latitude join"

    const-string v2, "lj"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/s;->aE:Lbm/i;

    new-instance v0, Lbm/i;

    const-string v1, "latitude show"

    const-string v2, "ls"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/s;->aF:Lbm/i;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/aC;LaN/p;LaN/u;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;LaH/m;Lbf/at;)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->i:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->j:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->q:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->I:I

    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->K:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->L:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->M:J

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->N:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->T:I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->U:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->V:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->W:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->X:Z

    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->Y:J

    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->Z:J

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->c:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ak:LaN/B;

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->al:Landroid/graphics/Point;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->am:Z

    new-instance v3, Lat/c;

    invoke-direct {v3}, Lat/c;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->aq:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->as:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->e:Z

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->aw:Z

    new-instance v3, Lcom/google/googlenav/ui/t;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/t;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ay:LaN/v;

    new-instance v3, Lbm/i;

    const-string v4, "searchprenetworkrequest"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    new-instance v3, Lbm/i;

    const-string v4, "searchpostnetworkrequest"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    new-instance v3, Lbm/i;

    const-string v4, "search-bubble"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    const-string v3, "GmmController.GmmController"

    invoke-static {v3}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->a()Lcom/google/googlenav/aA;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->e()Lcom/google/googlenav/android/aa;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->d()Lcom/google/googlenav/ui/android/L;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->b()Lcom/google/googlenav/ui/aD;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->i()Las/c;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->O()V

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->B:LaN/p;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->av:LaN/u;

    new-instance v3, Lcom/google/googlenav/ui/bE;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/bE;-><init>(LaN/u;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ay:LaN/v;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LaN/u;->a(LaN/v;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, LaN/u;->c(II)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/z;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/z;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u0001"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-char v4, Lcom/google/googlenav/ui/bi;->z:C

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lam/p;->a(Ljava/lang/String;)V

    new-instance v3, Lcom/google/googlenav/ui/W;

    invoke-direct {v3}, Lcom/google/googlenav/ui/W;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/L;->b()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/L;->a()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/ui/bi;->b(II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/s;->a(II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-static {v3, v4}, LaN/Y;->b(II)V

    invoke-virtual/range {p2 .. p2}, LaN/p;->c()LaN/Y;

    move-result-object v3

    invoke-static {v3}, LaN/Y;->c(LaN/Y;)LaN/Y;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LaN/u;->a(LaN/Y;)V

    new-instance v3, Lcom/google/googlenav/ui/ak;

    sget-object v8, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    move-object/from16 v4, p6

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/google/googlenav/ui/ak;-><init>(LaH/m;Lcom/google/googlenav/ui/s;LaN/u;LaN/p;Lcom/google/googlenav/android/A;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, LaN/p;->d(II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, LaN/u;->c(II)V

    invoke-static/range {p0 .. p0}, Lcom/google/googlenav/common/k;->b(Lcom/google/googlenav/common/h;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v11

    new-instance v3, Lcom/google/googlenav/ui/S;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ui/S;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aD:Lcom/google/googlenav/ui/S;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->aD:Lcom/google/googlenav/ui/S;

    invoke-virtual {v11, v3}, Law/h;->a(Law/q;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    new-instance v3, LaN/V;

    const-wide/32 v4, 0x1d4c0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-direct {v3, v4, v5, v6}, LaN/V;-><init>(JLas/c;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->c()Lcom/google/googlenav/ui/X;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-interface {v3, v4}, Lcom/google/googlenav/ui/X;->a(LaN/V;)V

    new-instance v3, LaN/k;

    invoke-direct {v3}, LaN/k;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    new-instance v3, Lcom/google/googlenav/j;

    invoke-direct {v3}, Lcom/google/googlenav/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v4, Lcom/google/googlenav/ui/F;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/googlenav/ui/F;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v3, v4}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/Q;)V

    new-instance v3, Lcom/google/googlenav/ui/K;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/K;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/q;)V

    new-instance v3, Lcom/google/googlenav/A;

    move-object/from16 v0, p6

    move-object/from16 v1, p3

    move-object/from16 v2, p0

    invoke-direct {v3, v0, v1, v2}, Lcom/google/googlenav/A;-><init>(LaH/m;LaN/u;Lcom/google/googlenav/J;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->i()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->j()Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Lcom/google/googlenav/friend/j;

    invoke-direct {v3}, Lcom/google/googlenav/friend/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    :goto_0
    new-instance v3, LaA/q;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LaA/q;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v3, v4}, LaA/h;->a(LaA/e;Lcom/google/googlenav/android/aa;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    move-object/from16 v9, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/googlenav/ui/aC;->a(LaN/p;LaN/u;LaH/m;ZLcom/google/googlenav/ui/wizard/z;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/j;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v3, Lcom/google/googlenav/ui/ac;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/googlenav/ui/ac;-><init>(Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/aA;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aC:Lcom/google/googlenav/ui/ac;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->i()Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Lcom/google/googlenav/friend/J;

    invoke-direct {v3}, Lcom/google/googlenav/friend/J;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    new-instance v3, Lcom/google/googlenav/friend/p;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    move-object/from16 v0, p0

    invoke-direct {v3, v4, v0}, Lcom/google/googlenav/friend/p;-><init>(Las/c;Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    new-instance v3, Lcom/google/googlenav/friend/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-direct {v3, v4}, Lcom/google/googlenav/friend/ag;-><init>(Lcom/google/googlenav/android/aa;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    :goto_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/V;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/googlenav/ui/V;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    invoke-virtual {v3, v4}, LaM/f;->a(LaM/h;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v3}, LaR/l;->a(Lcom/google/googlenav/ui/wizard/jv;)LaR/l;

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v3

    invoke-virtual {v3}, LaR/l;->f()LaR/n;

    move-result-object v3

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v4

    invoke-virtual {v4}, LaR/l;->g()LaR/n;

    move-result-object v4

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v5

    invoke-virtual {v5}, LaR/l;->m()LaR/I;

    move-result-object v5

    new-instance v6, LaR/b;

    invoke-direct {v6, v11, v5}, LaR/b;-><init>(Law/h;LaR/I;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-interface {v3}, LaR/n;->e()LaR/u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-interface {v3, v5}, LaR/u;->a(LaR/C;)V

    invoke-interface {v4}, LaR/n;->e()LaR/u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-interface {v3, v4}, LaR/u;->a(LaR/C;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v3}, LaR/b;->b()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/aC;->f()Lcom/google/googlenav/layer/r;

    move-result-object v15

    sget-object v16, Lcom/google/googlenav/offers/j;->a:Lcom/google/googlenav/offers/j;

    move-object/from16 v3, p7

    move-object/from16 v4, p0

    invoke-virtual/range {v3 .. v16}, Lbf/at;->a(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)Lbf/am;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3}, Lbf/am;->b()V

    new-instance v3, Lcom/google/googlenav/offers/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/googlenav/offers/a;-><init>(Lcom/google/googlenav/ui/s;LaN/u;Lcom/google/googlenav/ui/wizard/jv;Lbf/am;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v4}, Lcom/google/googlenav/offers/k;->a(Lcom/google/googlenav/android/aa;)Lcom/google/googlenav/offers/k;

    move-result-object v4

    invoke-virtual {v3, v4}, LaM/f;->a(LaM/h;)V

    :cond_1
    new-instance v3, Lav/a;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lav/a;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ag:Lav/a;

    new-instance v3, Lcom/google/googlenav/L;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v3, v4}, Lcom/google/googlenav/L;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ah:Lcom/google/googlenav/L;

    new-instance v3, Lcom/google/googlenav/mylocationnotifier/k;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/mylocationnotifier/k;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ai:Lcom/google/googlenav/mylocationnotifier/k;

    new-instance v3, Lcom/google/googlenav/ui/L;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1}, Lcom/google/googlenav/ui/L;-><init>(Lcom/google/googlenav/ui/s;LaN/u;)V

    new-instance v4, Lcom/google/googlenav/ui/M;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v4, v0, v1}, Lcom/google/googlenav/ui/M;-><init>(Lcom/google/googlenav/ui/s;LaH/m;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v3, v4, v5}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->ao()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Lcom/google/googlenav/ui/ar;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/ar;-><init>(LaN/p;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/w;)V

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->aD()V

    new-instance v3, Lcom/google/googlenav/friend/a;

    invoke-direct {v3}, Lcom/google/googlenav/friend/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->s()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->z()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->c:Z

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->x()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v3

    invoke-static {v3}, LaT/a;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, LaT/a;->i()V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/aV;->a()Lcom/google/googlenav/aV;

    move-result-object v3

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v4

    invoke-virtual {v4}, LaR/l;->b()LaR/aa;

    move-result-object v4

    invoke-interface {v4, v3}, LaR/aa;->a(LaR/ab;)V

    const-string v3, "GmmController.GmmController"

    invoke-static {v3}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_5
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    goto/16 :goto_1
.end method

.method private a(LaR/H;)Lcom/google/googlenav/ai;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaR/H;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v0

    goto :goto_0
.end method

.method static a(II)V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v0

    invoke-static {v0}, LaN/p;->a(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v1

    invoke-static {v1}, LaN/p;->a(I)I

    move-result v1

    mul-int/2addr v0, v1

    invoke-static {v0}, LaN/P;->a(I)V

    return-void
.end method

.method private static a(IILjava/lang/String;)V
    .locals 6

    const/4 v0, 0x4

    const-string v1, "ws"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "m="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lam/e;I)V
    .locals 7

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->t()I

    move-result v1

    invoke-virtual {v0}, Law/h;->s()I

    move-result v2

    invoke-virtual {v0}, Law/h;->r()I

    move-result v0

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  Z: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v6}, LaN/u;->d()LaN/Y;

    move-result-object v6

    invoke-virtual {v6}, LaN/Y;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  C: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/googlenav/ui/s;->I:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  S: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p2}, Lcom/google/googlenav/ui/s;->d(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mem: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->c()I

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/common/util/k;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/common/util/k;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NW: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    shr-int/lit8 v1, v1, 0xa

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "Kbytes/s"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "  R/S: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "K "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v0, v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "K"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->j()[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v1

    sget-object v2, Lcom/google/googlenav/ui/s;->aj:Ljava/lang/String;

    invoke-static {p1, v1, v3, v0, v2}, Lcom/google/googlenav/ui/aU;->a(Lam/e;I[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static a(Lcom/google/googlenav/aZ;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v1

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    new-instance v2, LaN/H;

    invoke-direct {v2, v0, v1, v4}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-static {v0, v2}, LaN/u;->a(LaN/H;LaN/H;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, LaN/u;->b(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->V()LaN/H;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->V()LaN/H;

    move-result-object v1

    invoke-static {v1, v2}, LaN/u;->a(LaN/H;LaN/H;)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-static {v1}, LaN/u;->b(I)I

    move-result v1

    sub-int/2addr v1, v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "s=c"

    aput-object v3, v2, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ts="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v1, "stat"

    invoke-static {v5, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "s=nc"

    aput-object v2, v1, v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/cg;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/cp;

    invoke-virtual {p1}, Lcom/google/googlenav/cg;->i()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/cp;-><init>(Lcom/google/googlenav/bZ;)V

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbf/am;->b(Lcom/google/googlenav/ui/r;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/b;->a(Lcom/google/googlenav/ui/r;)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ui/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/b;->b(Lcom/google/googlenav/ui/r;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->g()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->h()V

    :cond_3
    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-nez v0, :cond_8

    :cond_5
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1}, Lbf/i;->ab()Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->F()Z

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lcom/google/googlenav/ui/r;)V

    :cond_7
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lcom/google/googlenav/ui/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lcom/google/googlenav/ui/r;)V

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/ui/r;Lam/e;)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/aL;->a()Lcom/google/googlenav/ui/aL;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->a()Lam/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->o()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/aL;->a(Lam/e;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ui/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aD;->a()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->G()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bE;->a(Lcom/google/googlenav/ui/r;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private a(Lat/a;I)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq v2, v1, :cond_2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    :cond_2
    packed-switch v2, :pswitch_data_0

    move p2, v0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->c()LaN/B;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v4}, LaN/u;->d()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v0, p2, v4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/u;->c(LaN/B;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    neg-int p2, p2

    goto :goto_1

    :pswitch_2
    move v5, v0

    move v0, p2

    move p2, v5

    goto :goto_1

    :pswitch_3
    neg-int p2, p2

    move v5, v0

    move v0, p2

    move p2, v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->U:Z

    return p1
.end method

.method private aD()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    invoke-interface {v0, v1}, LaH/m;->a(LaH/A;)V

    :cond_0
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/x;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/x;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    return-void
.end method

.method private aE()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LastSessionTime"

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    return-void
.end method

.method private aF()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v1, "TRAFFIC_ON"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    aget-byte v0, v0, v3

    if-ne v0, v2, :cond_0

    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private aG()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->ag_()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lbf/i;->aZ()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private aH()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->J:J

    return-void
.end method

.method private aI()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ag()Z

    move-result v0

    return v0
.end method

.method private aJ()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->M:J

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->j()V

    return-void
.end method

.method private aK()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->p()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    :goto_1
    return-void

    :sswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->r(Z)V

    goto :goto_1

    :sswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->r(Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x8 -> :sswitch_2
        0xa -> :sswitch_3
    .end sparse-switch
.end method

.method private aL()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aN()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->e()V

    :cond_0
    return-void
.end method

.method private aM()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aN()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->f()V

    :cond_0
    return-void
.end method

.method private aN()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lbf/bK;

    invoke-virtual {v0}, Lbf/bK;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aO()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    :cond_0
    return-void
.end method

.method private aP()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->Q()V

    :cond_0
    return-void
.end method

.method private aQ()V
    .locals 2

    invoke-static {}, LaI/b;->c()I

    move-result v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->b(I)V

    return-void
.end method

.method private aR()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbf/i;->as()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    :cond_0
    return-void
.end method

.method private aS()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->S()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aT()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/l;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;
    .locals 1

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ai;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ai;

    if-eqz p2, :cond_0

    iget-object v2, p2, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2, v1}, LaN/u;->a(LaN/H;)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3, v1}, LaN/u;->b(LaN/H;)I

    move-result v3

    invoke-static {v0, v1, v2, v3, p2}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-static {v0, p2, v1, p3}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;Lcom/google/googlenav/bf;LaN/u;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/ui/bE;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    return-object v0
.end method

.method private b(Lam/e;)Lcom/google/googlenav/ui/r;
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v2

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->ag_()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->r()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->s()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->t()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->u()I

    move-result v6

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/googlenav/ui/r;->a(Lam/e;IIIIII)Lcom/google/googlenav/ui/r;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v1, v2}, Lcom/google/googlenav/ui/r;->a(Lam/e;II)Lcom/google/googlenav/ui/r;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lat/b;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->G()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bE;->a(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lat/b;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lat/b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaN/B;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lat/b;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0}, Lat/c;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0, p1}, Lat/c;->a(Lat/b;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->V:Z

    :cond_4
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/p;->a(Lat/b;)V

    :cond_5
    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lat/b;->m()Landroid/graphics/Point;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v1, p1}, Lat/c;->a(Lat/b;)V

    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/p;->d(II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v4}, LaN/u;->d()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v1, v0, v4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/u;->c(LaN/B;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0, p1}, Lat/c;->a(Lat/b;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0}, Lat/c;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->V:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v1}, Lat/c;->b()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v2}, Lat/c;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->a(II)V

    :cond_7
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    goto/16 :goto_0
.end method

.method private b(Lcom/google/googlenav/ai;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->E()V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    :cond_0
    new-array v0, v3, [Lcom/google/googlenav/ai;

    aput-object p1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0, v3}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(I)V

    invoke-virtual {v0}, Lbf/bk;->an()Z

    return-void
.end method

.method private b(Lcom/google/googlenav/ui/r;)V
    .locals 9

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v7

    invoke-virtual {v7}, Law/h;->g()V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->f()I

    move-result v3

    iget v4, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v5, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v3, v4, v5}, LaN/p;->a(IIII)V

    iget-object v8, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    check-cast v0, LaN/h;

    invoke-virtual {v0}, LaN/h;->g()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->f()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v7}, Law/h;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v3}, LaH/m;->g()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LaN/p;->a(Lam/e;ZZZZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    :cond_2
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v7}, Law/h;->h()V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/googlenav/ui/s;->am:Z

    invoke-virtual {v7}, Law/h;->h()V

    goto :goto_0

    :cond_3
    move v2, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v7}, Law/h;->h()V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v1, :cond_4

    iput-boolean v6, p0, Lcom/google/googlenav/ui/s;->am:Z

    invoke-virtual {v7}, Law/h;->h()V

    :cond_4
    throw v0
.end method

.method private b(Ljava/lang/Object;B)V
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/googlenav/aZ;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/googlenav/aZ;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->i()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    invoke-virtual {v2}, Lbf/i;->av()I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    invoke-virtual {v2}, Lbf/i;->av()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bk;

    :goto_1
    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, p2}, Lbf/bk;->a(B)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v1

    invoke-virtual {v1}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->a(I)V

    move-object v0, v1

    goto :goto_1
.end method

.method private b(Lcom/google/googlenav/aZ;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aB()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "##exp "

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "##exp "

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Laz/c;->a()Laz/c;

    move-result-object v3

    if-eqz v3, :cond_2

    if-gez v0, :cond_3

    neg-int v4, v0

    invoke-virtual {v3, v4}, Laz/c;->b(I)V

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Experiment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-gez v0, :cond_4

    const-string v0, "disabled"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v3, v0}, Laz/c;->a(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad Experiment id: \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_4
    :try_start_1
    const-string v0, "enabled"
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/googlenav/ui/s;)Las/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    return-object v0
.end method

.method private c(Law/g;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x4ee

    instance-of v1, p1, Lax/s;

    if-eqz v1, :cond_1

    const/16 v0, 0x1b7

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    instance-of v1, p1, Lax/w;

    if-eqz v1, :cond_2

    const/16 v0, 0x1b8

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lax/x;

    if-eqz v1, :cond_3

    const/16 v0, 0x1b9

    goto :goto_0

    :cond_3
    instance-of v1, p1, Lax/i;

    if-eqz v1, :cond_0

    const/16 v0, 0x1b6

    goto :goto_0
.end method

.method private c(Lam/e;)V
    .locals 10

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aH()V

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lam/e;)Lcom/google/googlenav/ui/r;

    move-result-object v7

    iput-object v7, p0, Lcom/google/googlenav/ui/s;->at:Lcom/google/googlenav/ui/r;

    const/4 v3, -0x1

    :try_start_0
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/r;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aG()Z

    move-result v2

    if-nez v2, :cond_a

    move v3, v5

    :goto_0
    if-nez v3, :cond_e

    move v4, v0

    :goto_1
    if-ne v3, v0, :cond_f

    iget v2, p0, Lcom/google/googlenav/ui/s;->T:I

    if-eq v2, v6, :cond_1

    iget v2, p0, Lcom/google/googlenav/ui/s;->T:I

    if-ne v2, v5, :cond_f

    :cond_1
    move v2, v0

    :goto_2
    or-int/2addr v4, v2

    if-ne v3, v0, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->d()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->e()Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_3
    move v2, v0

    :goto_3
    or-int/2addr v2, v4

    iput v3, p0, Lcom/google/googlenav/ui/s;->T:I

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    iget v8, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v9, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v4, v8, v9}, Lcom/google/googlenav/ui/p;->a(II)Lcom/google/googlenav/ui/r;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    :cond_4
    if-ne v3, v6, :cond_11

    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    :cond_5
    :goto_4
    invoke-direct {p0, v7, p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;Lam/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v2, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-eqz v2, :cond_6

    invoke-direct {p0, p1, v3}, Lcom/google/googlenav/ui/s;->a(Lam/e;I)V

    :cond_6
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->m()Z

    move-result v2

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->n()Z

    move-result v2

    if-nez v2, :cond_17

    :goto_5
    invoke-static {}, Lcom/google/googlenav/friend/p;->e()Z

    move-result v1

    if-nez v0, :cond_7

    if-eqz v1, :cond_8

    :cond_7
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lam/e;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    :cond_9
    return-void

    :cond_a
    :try_start_1
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->f()Z

    move-result v2

    if-eqz v2, :cond_b

    move v3, v0

    goto :goto_0

    :cond_b
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bF;->d()Z

    move-result v2

    if-nez v2, :cond_c

    iget v2, p0, Lcom/google/googlenav/ui/s;->I:I

    if-nez v2, :cond_d

    :cond_c
    move v3, v6

    goto/16 :goto_0

    :cond_d
    move v3, v1

    goto/16 :goto_0

    :cond_e
    move v4, v1

    goto/16 :goto_1

    :cond_f
    move v2, v1

    goto/16 :goto_2

    :cond_10
    move v2, v1

    goto :goto_3

    :cond_11
    if-eq v3, v5, :cond_5

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4, v7}, Lcom/google/googlenav/ui/p;->a(Lcom/google/googlenav/ui/r;)V

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/p;->j()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    :cond_12
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4, v7}, Lcom/google/googlenav/ui/p;->b(Lcom/google/googlenav/ui/r;)V

    if-nez v2, :cond_5

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    :catchall_0
    move-exception v2

    iget-boolean v4, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-eqz v4, :cond_13

    invoke-direct {p0, p1, v3}, Lcom/google/googlenav/ui/s;->a(Lam/e;I)V

    :cond_13
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v3}, LaN/p;->a()LaN/D;

    move-result-object v3

    invoke-virtual {v3}, LaN/D;->m()Z

    move-result v3

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v3}, LaN/p;->a()LaN/D;

    move-result-object v3

    invoke-virtual {v3}, LaN/D;->n()Z

    move-result v3

    if-nez v3, :cond_18

    :goto_6
    invoke-static {}, Lcom/google/googlenav/friend/p;->e()Z

    move-result v1

    if-nez v0, :cond_14

    if-eqz v1, :cond_15

    :cond_14
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lam/e;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    :cond_16
    throw v2

    :cond_17
    move v0, v1

    goto/16 :goto_5

    :cond_18
    move v0, v1

    goto :goto_6
.end method

.method private c(Lax/k;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v0

    :cond_2
    invoke-static {v1, v0}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1, v0}, Lax/k;->a(Lax/y;)V

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Lax/k;->b(Lax/y;)V

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ui/r;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aq:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x304

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->aq:Z

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->ap:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->a(LaN/p;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->b(LaN/p;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x317

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->ap:Z

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->aq:Z

    goto :goto_0
.end method

.method private c(Lat/b;)Z
    .locals 1

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v0

    return v0
.end method

.method private c(Lcom/google/googlenav/aZ;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "##dumpmem"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aT()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private static d(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "?"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "Map->Buf->Scr"

    goto :goto_0

    :pswitch_1
    const-string v0, "Buf->Scr"

    goto :goto_0

    :pswitch_2
    const-string v0, "Map->Scr"

    goto :goto_0

    :pswitch_3
    const-string v0, "No map"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private d(Lam/e;)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->z()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->x:C

    invoke-interface {v0, v1}, Lam/g;->c(C)I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/s;->n:I

    sub-int v0, v2, v0

    add-int/lit8 v0, v0, -0x3

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Lam/e;II)V

    return-void
.end method

.method private d(Lax/k;)V
    .locals 3

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    const-string v0, "0"

    const-string v1, "driving"

    instance-of v1, p1, Lax/w;

    if-eqz v1, :cond_1

    const-string v0, "1"

    const-string v1, "transit"

    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->d()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/s;->a(IILjava/lang/String;)V

    const/4 v1, 0x1

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->d()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/s;->a(IILjava/lang/String;)V

    const/4 v1, 0x4

    const-string v2, "m"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    instance-of v1, p1, Lax/x;

    if-eqz v1, :cond_2

    const-string v0, "2"

    const-string v1, "walking"

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lax/i;

    if-eqz v1, :cond_0

    const-string v0, "3"

    const-string v1, "bicycling"

    goto :goto_0
.end method

.method private d(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lat/a;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aC()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/r;->b()V

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lat/a;)Z

    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e(Lcom/google/googlenav/ui/s;)Lbf/am;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    return-object v0
.end method

.method private e(Lat/a;)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "8596494"

    iget v2, p0, Lcom/google/googlenav/ui/s;->l:I

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/google/googlenav/ui/s;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/s;->l:I

    iget v0, p0, Lcom/google/googlenav/ui/s;->l:I

    const-string v2, "8596494"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->a:Z

    iput v1, p0, Lcom/google/googlenav/ui/s;->l:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iput v1, p0, Lcom/google/googlenav/ui/s;->l:I

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aF()V

    return-void
.end method

.method private f(Lat/a;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->q:Z

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, p1}, Lbf/am;->a(Lat/a;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lat/a;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lat/a;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->q:Z

    iget-boolean v2, p0, Lcom/google/googlenav/ui/s;->q:Z

    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lat/a;)I

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->w()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x37

    if-ne v2, v3, :cond_4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Q()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x35

    if-ne v2, v3, :cond_6

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(I)Z

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v0

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private g(Lat/a;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/google/googlenav/ui/W;->a(Lat/a;Z)I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    if-gez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v3, v0}, LaN/u;->a(Z)Z

    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/s;)LaH/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    return-object v0
.end method

.method private h(Lat/a;)Z
    .locals 5

    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->L:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xfa

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/16 v0, 0xc

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/a;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/ak;->a(Lat/a;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/friend/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    return-object v0
.end method

.method private i(Lax/b;)V
    .locals 3

    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->ar()Lax/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax/z;->a(Lax/y;Lax/y;)V

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->at()Lax/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax/z;->a(Lax/y;Lax/y;)V

    return-void
.end method

.method private j(Lax/b;)V
    .locals 6

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Law/g;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    return-void
.end method

.method static synthetic j(Lcom/google/googlenav/ui/s;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/s;)Las/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/friend/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    return-object v0
.end method

.method private l(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/A;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/A;-><init>(Lcom/google/googlenav/ui/s;)V

    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    const/16 v0, 0x1b0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    return-void
.end method

.method private m(Ljava/lang/String;)LaR/H;
    .locals 2

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaR/H;->c()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1, p1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private r(Z)V
    .locals 3

    const/16 v2, 0xa

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    invoke-static {}, Lcom/google/googlenav/K;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Z)V

    const-string v0, "a"

    invoke-static {v2, v0}, Lbm/m;->a(ILjava/lang/String;)V

    sget-object v0, Lcom/google/googlenav/z;->d:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "d"

    invoke-static {v2, v0}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-static {}, Lbm/m;->a()Lbm/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbm/m;->a(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->as:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v2, "T_AND_C_ACCEPT"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->as:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->h()V

    :cond_0
    return-void
.end method

.method public C_()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/G;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/G;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public D()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D_()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/H;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/H;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public E()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->R()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->e:Z

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aI()Z

    move-result v0

    goto :goto_0
.end method

.method public G()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->u()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->e(Lbf/i;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->d()V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->f()V

    goto :goto_0

    :cond_1
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->i()V

    goto :goto_0
.end method

.method public I()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/bK;->d()V

    :cond_0
    return-void
.end method

.method public J()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/bK;->f()V

    :cond_0
    return-void
.end method

.method public K()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->h()V

    return-void
.end method

.method L()V
    .locals 5

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ac;->b()Lcom/google/googlenav/friend/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bi;->B_()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/am;->f(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lbf/X;->ax()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->e(Lbf/i;)V

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->a(Lbf/X;)V

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->aa()Lbf/a;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v4, Lcom/google/googlenav/ui/y;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/ui/y;-><init>(Lcom/google/googlenav/ui/s;Lbf/X;)V

    invoke-virtual {v1, v2, v3, v4}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-direct {v1}, Lcom/google/googlenav/aZ;-><init>()V

    invoke-virtual {v0, v1}, Lbf/am;->e(Lcom/google/googlenav/aZ;)Lbf/X;

    move-result-object v0

    goto :goto_1
.end method

.method public M()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    return-wide v0
.end method

.method public M_()V
    .locals 0

    return-void
.end method

.method public N()J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Z:J

    return-wide v0
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method protected O()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->Z:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LastSessionTime"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    return-void
.end method

.method public P()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->h()Z

    move-result v0

    return v0
.end method

.method public Q()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public R()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->r()V

    return-void
.end method

.method public S()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    return-void
.end method

.method public T()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->m:Z

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->k(Z)V

    :cond_0
    return-void
.end method

.method public U()V
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->X:Z

    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->X:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    if-nez v0, :cond_c

    :cond_2
    move v0, v2

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    if-eqz v3, :cond_e

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->L:J

    sub-long v3, v5, v3

    const-wide/16 v7, 0x64

    cmp-long v3, v3, v7

    if-lez v3, :cond_e

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-boolean v3, p0, Lcom/google/googlenav/ui/s;->q:Z

    if-eqz v3, :cond_e

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->K:J

    cmp-long v3, v3, v9

    if-nez v3, :cond_d

    const/4 v3, 0x6

    iput-wide v5, p0, Lcom/google/googlenav/ui/s;->K:J

    :goto_2
    const/16 v4, 0xc

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-boolean v4, p0, Lcom/google/googlenav/ui/s;->U:Z

    iget-object v7, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/ui/s;->a(Lat/a;I)Z

    move-result v3

    or-int/2addr v3, v4

    iput-boolean v3, p0, Lcom/google/googlenav/ui/s;->U:Z

    :goto_3
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->H:J

    cmp-long v3, v5, v3

    if-lez v3, :cond_14

    iget-boolean v3, p0, Lcom/google/googlenav/ui/s;->W:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v3

    if-eqz v3, :cond_f

    :cond_3
    const-wide/16 v3, 0x5dc

    :goto_4
    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/googlenav/ui/s;->H:J

    iget v3, p0, Lcom/google/googlenav/ui/s;->G:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/googlenav/ui/s;->G:I

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->h()Z

    move-result v3

    if-eqz v3, :cond_10

    move v0, v2

    move v3, v2

    :goto_5
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bF;->d()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    move v0, v2

    :cond_4
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/p;->i()Z

    move-result v4

    if-eqz v4, :cond_5

    move v0, v2

    :cond_5
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/ak;->a()V

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-boolean v7, p0, Lcom/google/googlenav/ui/s;->U:Z

    invoke-virtual {v4, v3}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v3

    or-int/2addr v3, v7

    iput-boolean v3, p0, Lcom/google/googlenav/ui/s;->U:Z

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->m()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    move v0, v2

    :cond_6
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3, v5, v6}, Lcom/google/googlenav/ui/ak;->a(J)V

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->J:J

    const-wide/16 v7, 0x1388

    add-long/2addr v3, v7

    cmp-long v3, v3, v5

    if-gtz v3, :cond_7

    move v0, v2

    :cond_7
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v3}, Lcom/google/googlenav/A;->b()Z

    move-result v3

    if-nez v3, :cond_8

    if-eqz v0, :cond_12

    :cond_8
    move v0, v2

    :goto_6
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->M:J

    const-wide/32 v5, 0x2932e0

    add-long/2addr v3, v5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_9

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/googlenav/ui/s;->M:J

    invoke-virtual {p0, v1, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    move v0, v2

    :cond_9
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    or-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    :cond_a
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->c:Z

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->c:Z

    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v0}, LaR/b;->a()V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_1

    :cond_d
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->K:J

    sub-long v3, v5, v3

    long-to-int v3, v3

    int-to-long v3, v3

    const-wide/16 v7, 0x6

    mul-long/2addr v3, v7

    long-to-int v4, v3

    div-int/lit8 v3, v4, 0x64

    rem-int/lit8 v4, v4, 0x64

    int-to-long v7, v4

    sub-long v7, v5, v7

    iput-wide v7, p0, Lcom/google/googlenav/ui/s;->K:J

    goto/16 :goto_2

    :cond_e
    iput-wide v9, p0, Lcom/google/googlenav/ui/s;->K:J

    goto/16 :goto_3

    :cond_f
    const-wide/16 v3, 0x1f4

    goto/16 :goto_4

    :cond_10
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    invoke-virtual {v3}, Law/h;->l()Z

    move-result v3

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->d()Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_11
    move v0, v2

    move v3, v2

    goto/16 :goto_5

    :cond_12
    move v0, v1

    goto/16 :goto_6

    :cond_13
    move v3, v2

    goto/16 :goto_5

    :cond_14
    move v3, v1

    goto/16 :goto_5
.end method

.method public V()V
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->G()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->L()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/aL;->a()Lcom/google/googlenav/ui/aL;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/aL;->a(LaN/p;Z)V

    :cond_1
    return-void
.end method

.method public W()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    return v0
.end method

.method public X()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->a()V

    return-void
.end method

.method public Y()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->c()V

    return-void
.end method

.method public declared-synchronized Z()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lbf/aJ;
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->d(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bN;)Lbf/bH;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lcom/google/googlenav/bN;)Lbf/bH;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/Y;)Lbf/bU;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/Y;Z)Lbf/bU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)Lbf/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1, p2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bf;)Lbf/bk;
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->c(Lcom/google/googlenav/aZ;)Lbf/bk;

    move-result-object v0

    return-object v0
.end method

.method public a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;
    .locals 5

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/aa;

    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->c(Lax/y;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lcom/google/googlenav/aa;-><init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V

    if-eqz p2, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/googlenav/aa;->a(Lax/y;)V

    :cond_0
    invoke-static {}, Lbf/bU;->bL()Lcom/google/googlenav/ab;

    move-result-object v2

    sget-object v3, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    aput v2, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/googlenav/aa;->a([I)V

    :cond_1
    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-object v1
.end method

.method public a(Lbf/bG;Lcom/google/googlenav/bU;Z)Lcom/google/googlenav/bW;
    .locals 2

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bW;

    invoke-direct {v1, p1, p3}, Lcom/google/googlenav/bW;-><init>(Lbf/bG;Z)V

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bW;->a(Lcom/google/googlenav/bU;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-object v1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    return-void
.end method

.method public a(ILaH/m;)V
    .locals 0

    return-void
.end method

.method public a(I[Lba/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(I[Lba/f;)V

    return-void
.end method

.method public a(LaH/m;LaN/B;ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {p1, p2, p3, p4, v0}, LaH/d;->a(LaH/m;LaN/B;ZLjava/lang/String;Las/c;)V

    return-void
.end method

.method public a(LaN/B;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1, p1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    const/4 v4, 0x1

    move-object v1, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/A;->b()Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Law/h;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aQ()V

    return-void
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v5

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v6

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v7

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v9, p5

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/ui/wizard/jv;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;LaN/H;IILcom/google/googlenav/ui/wizard/C;Z)V

    return-void
.end method

.method public a(Lam/e;)V
    .locals 9

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Landroid/os/Handler;JJLas/c;J)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Landroid/os/Handler;JJLas/c;J)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lam/e;)V

    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/s;->I:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0
.end method

.method public a(Lat/b;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lat/b;)V

    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->X:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto :goto_0
.end method

.method public a(Law/g;)V
    .locals 11

    const/4 v0, 0x0

    const/4 v4, 0x7

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v1, p1, Lcom/google/googlenav/aZ;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    check-cast p1, Lcom/google/googlenav/aZ;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->u()I

    move-result v2

    invoke-virtual {v1, v2}, LaM/f;->a(I)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->x()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->y()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lax/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ax()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aR()V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "s=po"

    aput-object v1, v0, v10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->p()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v2}, Lbm/i;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stat"

    invoke-static {v9, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/googlenav/bf;->G:Z

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aD()I

    move-result v1

    if-lez v1, :cond_7

    sget-object v2, LaA/b;->k:LaA/c;

    invoke-virtual {v2, v1}, LaA/c;->a(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/offers/a;->a(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/offers/a;->a(Lcom/google/googlenav/aZ;)V

    :cond_7
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x58

    const-string v4, "b"

    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "c="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v10

    if-nez v2, :cond_8

    :goto_3
    aput-object v0, v5, v8

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_9
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->n()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    if-ne v1, v4, :cond_a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->q()Z

    move-result v1

    if-nez v1, :cond_b

    :cond_a
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aO()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ak()I

    move-result v0

    if-ne v0, v8, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->S()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->T()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->d(II)V

    invoke-virtual {p0, v8}, Lcom/google/googlenav/ui/s;->q(Z)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1, v10}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    goto/16 :goto_1

    :cond_d
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lbf/am;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V

    invoke-static {p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    if-eq v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v2

    invoke-interface {v1, v2, v9, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    instance-of v0, p1, Lax/b;

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    check-cast p1, Lax/b;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    goto/16 :goto_2

    :cond_f
    instance-of v0, p1, Lcom/google/googlenav/aa;

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    check-cast p1, Lcom/google/googlenav/aa;

    invoke-virtual {p1}, Lcom/google/googlenav/aa;->k()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/google/googlenav/aa;->l()Lcom/google/googlenav/Y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->x()Lbf/bU;

    move-result-object v1

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lbf/bU;->ax()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    :cond_10
    invoke-virtual {v1, p1}, Lbf/bU;->a(Lcom/google/googlenav/aa;)V

    goto/16 :goto_2

    :cond_11
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->i()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/Y;)Lbf/bU;

    goto/16 :goto_2

    :cond_12
    instance-of v0, p1, Lcom/google/googlenav/bW;

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    check-cast p1, Lcom/google/googlenav/bW;

    invoke-virtual {p1}, Lcom/google/googlenav/bW;->i()Lcom/google/googlenav/bN;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_13

    invoke-virtual {p1}, Lcom/google/googlenav/bW;->k()V

    goto/16 :goto_2

    :cond_13
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bN;)Lbf/bH;

    goto/16 :goto_2

    :cond_14
    instance-of v0, p1, Lcom/google/googlenav/cg;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/google/googlenav/cg;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/cg;)V

    goto/16 :goto_2
.end method

.method public a(Lax/b;)V
    .locals 3

    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-direct {v0, v2, v1}, LaN/B;-><init>(II)V

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lax/y;->a(LaN/B;)Lax/y;

    move-result-object v0

    invoke-virtual {p1, v0}, Lax/b;->b(Lax/y;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lax/k;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lax/k;)V

    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    invoke-virtual {p1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-static {p1}, Lbf/O;->a(Lax/b;)Lax/b;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->j(Lax/b;)V

    goto :goto_1
.end method

.method public a(Lax/b;B)V
    .locals 1

    invoke-virtual {p1, p2}, Lax/b;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    return-void
.end method

.method public a(Lax/b;Ljava/util/List;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v3

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v4

    move-object v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;LaN/H;IILjava/util/List;)V

    return-void
.end method

.method public a(Lax/k;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/A;->a(Lax/k;)V

    return-void
.end method

.method public a(Lax/w;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->p(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/w;)Lbf/bK;

    return-void
.end method

.method public a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .locals 2

    invoke-static {p3}, LO/b;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;

    move-result-object v0

    invoke-static {p1, p2, v0, p4}, Lcom/google/android/maps/driveabout/app/bn;->a(Lax/y;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    new-instance v0, Lax/j;

    invoke-direct {v0, p1, p2, p3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/j;)V

    return-void
.end method

.method public a(Lbf/aU;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p0, p1}, Lbf/am;->b(Lcom/google/googlenav/J;Lbf/aU;)V

    return-void
.end method

.method public a(Lbf/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->h(Lbf/i;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/A;)V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v2

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v3

    invoke-virtual {v0}, Lbf/i;->aX()V

    invoke-virtual {v0, v2}, Lbf/i;->b(B)V

    invoke-interface {v1, v3}, Lcom/google/googlenav/F;->a(I)V

    invoke-virtual {v0}, Lbf/i;->aW()V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->d()V

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/google/googlenav/ui/s;->I:I

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3, v0, v1, v2}, Lbf/am;->a(ZZZ)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/N;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/N;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/O;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/P;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/P;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    :cond_4
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/Q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/Q;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/u;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    const-string v1, "GmmController-loadSavedState"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public a(Lcom/google/googlenav/aZ;IZ)V
    .locals 11

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {v0, v1, v2, v3, v4}, LaH/d;->a(Ljava/lang/String;ILaN/B;ILas/c;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    if-eqz p3, :cond_5

    const-string v0, "a"

    :goto_1
    new-array v1, v10, [Ljava/lang/String;

    const-string v2, "s=pr"

    aput-object v2, v1, v8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v2}, Lbm/i;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v7

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stat"

    invoke-static {v7, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, p2, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v0

    const-string v2, "19"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, ""

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gk;->B()I

    move-result v2

    if-ne v2, v7, :cond_2

    const-string v0, "26"

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_6

    const-string v2, "rf"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v3, "ro"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/googlenav/aZ;->b(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v6

    :cond_3
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "q="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    const-string v0, "c="

    aput-object v0, v3, v10

    const/4 v0, 0x4

    aput-object v2, v3, v0

    const/4 v0, 0x5

    aput-object v6, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->c()V

    iput-boolean v8, p0, Lcom/google/googlenav/ui/s;->ap:Z

    goto/16 :goto_0

    :cond_5
    const-string v0, "p"

    goto/16 :goto_1

    :cond_6
    move-object v2, v6

    goto :goto_2
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;BZ)V
    .locals 6

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    move-object v1, p1

    move v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Lcom/google/googlenav/aZ;)Lbf/x;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->d()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->d()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/bf;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0, p1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(LaN/B;)Lcom/google/googlenav/bg;

    :cond_0
    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->c(Z)V

    iget v0, p1, Lcom/google/googlenav/bf;->j:I

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    return-void
.end method

.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;)V
    .locals 8

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object v1, p0

    move-object v2, p1

    move v6, v5

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/as;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/s;->t:Lcom/google/googlenav/ui/as;

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;B)V

    return-void
.end method

.method public a(Ljava/lang/Object;B)V
    .locals 2

    check-cast p1, Lcom/google/googlenav/Y;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/Y;Z)Lbf/bU;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbf/bU;->b(I)V

    invoke-virtual {v0, p2}, Lbf/bU;->a(B)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aD;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/B;

    invoke-direct {v0, p0, p4, p2, p3}, Lcom/google/googlenav/ui/B;-><init>(Lcom/google/googlenav/ui/s;ZILjava/lang/String;)V

    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    const/16 v0, 0x1b0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;LaN/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;LaN/B;)V

    return-void
.end method

.method public a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/ai;)V
    .locals 11

    const-wide/16 v2, -0x1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/H;

    invoke-direct {v0, p0}, Lcom/google/googlenav/H;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {p1}, Lcom/google/googlenav/H;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/H;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    :goto_1
    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "http:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "https:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_1
    const/16 v5, 0x37

    const-string v6, "u"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "url="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v7, v8

    const/4 v1, 0x2

    if-eqz v4, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v7, v1

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1

    :cond_3
    :try_start_1
    const-string v0, ""

    goto :goto_2

    :cond_4
    const-string v0, ""

    goto :goto_3

    :cond_5
    const/16 v5, 0x37

    const-string v6, "x"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "url="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    aput-object v0, v7, v8

    const/4 v1, 0x2

    if-eqz v4, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    aput-object v0, v7, v1

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    const-string v0, ""

    goto :goto_5

    :cond_7
    const-string v0, ""
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x25a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/google/googlenav/W;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/googlenav/W;-><init>(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v3, 0x2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->e()Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;ZI)V
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/z;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/googlenav/ui/z;-><init>(Lcom/google/googlenav/ui/s;ZI)V

    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    const/16 v0, 0x1b0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Lo/S;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lo/S;)V

    return-void
.end method

.method public a(Z)V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GmmController.handleOutOfMemory("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aT()V

    :cond_0
    if-eqz p1, :cond_1

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x36b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Q()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5c2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/s;->aE:Lbm/i;

    :goto_0
    invoke-virtual {v0}, Lbm/i;->a()V

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/s;->c(I)V

    invoke-virtual {v0}, Lbm/i;->b()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/s;->aF:Lbm/i;

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 10

    const/16 v9, 0xd

    const/4 v8, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->w()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->P:Z

    if-eqz p1, :cond_4

    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lbf/i;->c(Z)I

    move-result v1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->q()I

    move-result v0

    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v5

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v6

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v4, v5, v6, v1, v0}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v1

    if-le v1, v9, :cond_1

    invoke-static {v9}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :cond_1
    new-instance v1, LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v5

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v4, v2}, LaN/B;-><init>(II)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2, v1, v0}, LaN/u;->e(LaN/B;LaN/Y;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->q()Lbf/bE;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_3

    const/16 v0, 0x2c2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_3
    iput-boolean v8, p0, Lcom/google/googlenav/ui/s;->ap:Z

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->r()V

    goto/16 :goto_0

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public a(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-eq v2, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0, p1}, LaN/u;->a(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->V()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    move v0, v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(ILat/a;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    if-eqz p2, :cond_0

    packed-switch p1, :pswitch_data_1

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    move v0, v1

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lbf/i;->a(B)V

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    move v0, v1

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Y()V

    move v0, v1

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/s;->c(Lat/a;)Z

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_7
    .end packed-switch
.end method

.method public a(LaD/q;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/p;->a(LaD/q;)Z

    move-result v0

    return v0
.end method

.method public a(Lat/a;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lat/a;)Z

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    return v0
.end method

.method public aA()Lcom/google/googlenav/offers/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    return-object v0
.end method

.method public aB()Lav/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ag:Lav/a;

    return-object v0
.end method

.method public aC()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    return v0
.end method

.method public ab()Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method public ac()Lcom/google/googlenav/mylocationnotifier/k;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ai:Lcom/google/googlenav/mylocationnotifier/k;

    return-object v0
.end method

.method public ad()Lcom/google/googlenav/aA;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    return-object v0
.end method

.method public ae()Lcom/google/googlenav/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    return-object v0
.end method

.method public af()Lcom/google/googlenav/ui/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    return-object v0
.end method

.method public ag()V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    move v0, v1

    move-object v1, v2

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Lbf/i;->a(B)V

    :cond_1
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->v()Lbf/O;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Lcom/google/googlenav/F;->a(I)V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->w()Lbf/bK;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-virtual {v2}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Lcom/google/googlenav/F;->a(I)V

    move-object v1, v2

    goto :goto_0

    :cond_2
    move v0, v1

    move-object v1, v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x1b -> :sswitch_1
    .end sparse-switch
.end method

.method public ah()Lcom/google/googlenav/ui/wizard/C;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    return-object v0
.end method

.method public ai()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->s()Z

    move-result v0

    return v0
.end method

.method public aj()Lcom/google/googlenav/A;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    return-object v0
.end method

.method public ak()Las/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    return-object v0
.end method

.method public al()Lbf/am;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    return-object v0
.end method

.method public am()LaR/n;
    .locals 1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public an()LaR/n;
    .locals 1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public ao()Lcom/google/googlenav/ui/R;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    return-object v0
.end method

.method public ap()Lcom/google/googlenav/ui/wizard/z;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    return-object v0
.end method

.method public aq()Lcom/google/googlenav/ui/bE;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    return-object v0
.end method

.method public ar()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/l;->a()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->d()V

    invoke-static {}, Lax/o;->b()V

    invoke-static {}, Lcom/google/googlenav/ui/wizard/gk;->C()V

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public as()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/googlenav/ui/s;->Z:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public at()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_0
    return-void
.end method

.method public au()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->p(Z)V

    return-void
.end method

.method public av()Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method public aw()Lcom/google/googlenav/friend/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method public ax()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public ay()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-static {v0, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->x()Lbf/bU;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lbf/bU;->ax()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    :cond_0
    invoke-virtual {v1}, Lbf/bU;->d()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;

    move-result-object v2

    const/16 v0, 0x4c9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    goto :goto_0
.end method

.method public az()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;I)V

    return-void
.end method

.method public b(II)V
    .locals 3

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/ui/s;->n:I

    iput p2, p0, Lcom/google/googlenav/ui/s;->o:I

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, LaN/p;->d(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, LaN/u;->c(II)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->b(II)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->V()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/p;->c(II)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0
.end method

.method public b(LaN/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(LaN/B;)V

    return-void
.end method

.method public b(Law/g;)V
    .locals 0

    invoke-interface {p1}, Law/g;->u_()V

    return-void
.end method

.method public b(Lax/b;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x4

    const-string v1, "rs"

    invoke-virtual {p1}, Lax/b;->y()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aF()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/o;->a([I)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->i(Lax/b;)V

    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->c(Lax/b;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lax/b;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    invoke-virtual {p1, v0}, Lax/b;->u(I)V

    invoke-virtual {p1}, Lax/b;->A()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lax/b;->aO()Lax/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/bN;->a()V

    :cond_2
    const/16 v1, 0x5c1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lax/b;->aQ()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lax/b;->aQ()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lax/b;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->i()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;)V

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aw()LaN/B;

    move-result-object v1

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aC()LaN/B;

    move-result-object v1

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->e(Lax/b;)Lbf/O;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Lbf/O;->c(Lcom/google/googlenav/F;)V

    goto :goto_1
.end method

.method public b(Lax/k;)V
    .locals 1

    instance-of v0, p1, Lax/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    check-cast p1, Lax/b;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    check-cast p1, Lax/j;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/j;)V

    goto :goto_0
.end method

.method public b(Lbf/aU;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lbf/aU;)V

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;B)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aD;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_0
.end method

.method public b(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/W;->a(Lat/a;)V

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->e(Lat/a;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->f(Lat/a;)Z

    move-result v0

    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v1

    if-nez v1, :cond_4

    iput-object p1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/googlenav/ui/s;->L:J

    :cond_4
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0
.end method

.method public c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->d(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    return-void
.end method

.method public c(Lax/b;)V
    .locals 1

    invoke-virtual {p1}, Lax/b;->t()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->e(Lax/b;)V

    return-void
.end method

.method public c(Lbf/aU;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1, p1}, Lbf/am;->a(Lcom/google/googlenav/ui/wizard/jv;Lbf/aU;)V

    return-void
.end method

.method public c(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lcom/google/googlenav/bl;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lbf/am;->a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;

    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v0, p1}, LaR/b;->a(Z)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    return v0
.end method

.method c(Lat/a;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->g(Lat/a;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->h(Lat/a;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c_(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1}, LaN/p;->b()LaN/H;

    move-result-object v1

    invoke-virtual {v1, p1}, LaN/H;->a(Z)LaN/H;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/H;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->m(Ljava/lang/String;)LaR/H;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaR/H;)Lcom/google/googlenav/ai;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->l(Ljava/lang/String;)V

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ag;->b:Lcom/google/googlenav/ag;

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ai;)V

    sget-object v0, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    goto :goto_0
.end method

.method public d(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->c(Lax/b;)V

    return-void
.end method

.method public d(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/16 v1, 0x14a

    invoke-virtual {v0, v1, p1}, Lbf/am;->a(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->aA:Z

    return-void
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->P:Z

    return v0
.end method

.method public e(Lax/b;)Lbf/O;
    .locals 1

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lax/b;)Lbf/O;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/ai;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->m(Ljava/lang/String;)LaR/H;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaR/H;)Lcom/google/googlenav/ai;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public e(Z)V
    .locals 1

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aC:Lcom/google/googlenav/ui/ac;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/ac;->a(Z)V

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->U()V

    return-void
.end method

.method public f(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->d(Lax/b;)V

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/cg;

    invoke-direct {v1, p1}, Lcom/google/googlenav/cg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public f(Z)V
    .locals 3

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lbf/am;->b(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const-string v2, "LayerTransit"

    invoke-virtual {v1, v2}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/am;->h(Lbf/i;)V

    goto :goto_0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x6

    new-array v3, v0, [Lcom/google/googlenav/common/util/n;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    aput-object v0, v3, v1

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x5

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    aput-object v4, v3, v0

    move v0, v1

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lcom/google/googlenav/common/util/n;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "GMM memory usage"

    invoke-direct {v0, v3, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method public g(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lax/b;)V

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    return-void
.end method

.method public g(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->P()Z

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1}, LaN/p;->b()LaN/H;

    move-result-object v1

    invoke-virtual {v1, p1}, LaN/H;->b(Z)LaN/H;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/H;)V

    goto :goto_0
.end method

.method public h()Lcom/google/googlenav/E;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->V()Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method

.method public h(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->g()Lcom/google/googlenav/ui/wizard/bI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bI;->a(Lax/b;)V

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/aZ;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public h(Z)V
    .locals 1

    const-string v0, "GmmController.showNotify"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->l(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->U()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "GmmController.showNotify"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0
.end method

.method public i()V
    .locals 5

    new-instance v0, Lcom/google/googlenav/layer/k;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->a()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->b()I

    move-result v3

    const-string v4, "__LAYERS"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/layer/k;-><init>(LaN/H;IILjava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    new-instance v1, Lcom/google/googlenav/ui/w;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/w;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/k;->a(Lcom/google/googlenav/layer/l;)V

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    return-void
.end method

.method public i(Z)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    invoke-virtual {v0}, Las/c;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->e()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->S()V

    if-eqz p1, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->j(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    invoke-virtual {v0}, LaN/k;->d()V

    invoke-static {}, Lcom/google/googlenav/friend/E;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/friend/E;->e()Lcom/google/googlenav/friend/E;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/E;->b(LaM/h;)V

    :cond_3
    invoke-static {}, Lcom/google/googlenav/friend/as;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/friend/as;->e()Lcom/google/googlenav/friend/as;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/as;->b(LaM/h;)V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/friend/W;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/friend/W;->e()Lcom/google/googlenav/friend/W;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/W;->b(LaM/h;)V

    :cond_5
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    :cond_6
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    goto :goto_0
.end method

.method public j()Lbf/by;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/by;->d(Ljava/lang/String;)V

    return-void
.end method

.method public j(Z)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/m;->h()V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bh;->h()V

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->L()V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->f()LaN/H;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/H;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->f()V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v2

    invoke-virtual {v2}, LaR/l;->n()V

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->b()V

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v3, "TRAFFIC_ON"

    new-array v4, v0, [B

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_0
    int-to-byte v0, v0

    aput-byte v0, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    invoke-static {v0}, Lbf/bU;->a(Lcom/google/googlenav/common/io/j;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    invoke-static {}, Lbm/m;->d()V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public k()LaB/o;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    if-nez v0, :cond_0

    new-instance v0, LaB/o;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-direct {v0, v1, v2}, LaB/o;-><init>(Lcom/google/googlenav/android/aa;Las/c;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    return-object v0
.end method

.method public k(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public declared-synchronized k(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aE()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->w()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->q()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->h()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aM()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()Lcom/google/googlenav/L;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ah:Lcom/google/googlenav/L;

    return-object v0
.end method

.method public declared-synchronized l(Z)V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->O()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/s;->I:I

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->y()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->c()V

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    :cond_1
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->r()V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->i()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    const/4 v0, 0x0

    new-instance v1, Lcom/google/googlenav/ui/C;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/googlenav/ui/C;-><init>(Lcom/google/googlenav/ui/s;Las/c;Lcom/google/googlenav/android/aa;Z)V

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Las/d;->c(J)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/D;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/D;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->x()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/E;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/E;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aL()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->a()V

    :cond_3
    sget-object v0, Lcom/google/googlenav/z;->e:Lcom/google/googlenav/z;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Las/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public m()Lbf/a;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->aa()Lbf/a;

    move-result-object v0

    return-object v0
.end method

.method public m(Z)V
    .locals 1

    if-nez p1, :cond_0

    const/16 v0, 0x2a5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public n()Lcom/google/googlenav/ui/as;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->t:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method public n(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->L()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/app/bn;->a(Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    return-void
.end method

.method public o()Lbf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    return-object v0
.end method

.method public o(Z)V
    .locals 8

    const-wide/16 v6, 0x1f4

    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->W:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    if-eqz p1, :cond_1

    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->H:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/google/googlenav/ui/s;->H:J

    add-long v4, v0, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    add-long/2addr v0, v6

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->H:J

    goto :goto_0
.end method

.method public p()Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method public p(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/i;->Z()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->f()V

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    :cond_2
    return-void
.end method

.method public q()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aH()V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aG()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->F()Z

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->G()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/L;->q()V

    goto :goto_0
.end method

.method public q(Z)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/I;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/I;-><init>(Lcom/google/googlenav/ui/s;Z)V

    new-instance v2, Lcom/google/googlenav/ui/J;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/J;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V

    new-instance v3, LaT/h;

    invoke-direct {v3}, LaT/h;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/MapsActivity;->activateAreaSelector(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)Lcom/google/googlenav/ui/android/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    return-void
.end method

.method public r()Lcom/google/googlenav/friend/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    return-object v0
.end method

.method public s()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/R;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/R;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/E;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/E;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(Lbf/am;)V

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/s;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/s;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/as;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/as;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/as;->a(LaM/h;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/ar;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/ar;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/W;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/W;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/W;->a(LaM/h;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/V;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/V;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/bm;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v2, Lcom/google/googlenav/friend/bu;

    invoke-direct {v2, v1}, Lcom/google/googlenav/friend/bu;-><init>(Lcom/google/googlenav/bm;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    goto/16 :goto_0
.end method

.method public t()LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    return-object v0
.end method

.method public u()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;)V

    return-void
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z

    return v0
.end method

.method public w()V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/j;->a([I)V

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public x()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->h()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    return-object v0
.end method

.method public y()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ui/s;)V

    return-void
.end method

.method public z()Z
    .locals 3

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aA()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz v1, :cond_2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->s()V

    goto :goto_0

    :cond_2
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->g()V

    goto :goto_0
.end method
