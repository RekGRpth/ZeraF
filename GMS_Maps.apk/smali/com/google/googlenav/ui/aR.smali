.class public Lcom/google/googlenav/ui/aR;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/googlenav/ui/aI;

.field private b:LaN/Y;

.field private c:[[[J


# direct methods
.method public constructor <init>([Lcom/google/googlenav/ui/aI;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iput-object p1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    return-void
.end method

.method private static a(IIII)I
    .locals 2

    const/4 v0, 0x0

    if-gez p2, :cond_1

    const/16 v1, 0x8

    :goto_0
    if-gez p3, :cond_3

    const/4 v0, 0x2

    :cond_0
    :goto_1
    or-int/2addr v0, v1

    return v0

    :cond_1
    if-le p2, p0, :cond_2

    const/4 v1, 0x4

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    if-le p3, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static a(J)I
    .locals 2

    const/16 v0, 0x20

    shr-long v0, p0, v0

    long-to-int v0, v0

    return v0
.end method

.method private static a(Lcom/google/googlenav/ui/aH;)I
    .locals 1

    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(II)J
    .locals 5

    const/16 v4, 0x20

    int-to-long v0, p0

    shl-long/2addr v0, v4

    int-to-long v2, p1

    shl-long/2addr v2, v4

    ushr-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method static a(IIII[I)V
    .locals 10

    const/16 v1, 0xfa0

    const/16 v2, -0xfa0

    sub-int v4, p0, p2

    sub-int v5, p1, p3

    if-gt p0, v1, :cond_0

    if-ge p0, v2, :cond_5

    :cond_0
    if-lez p0, :cond_2

    move v0, v1

    :goto_0
    sub-int v3, v0, p2

    int-to-long v6, v3

    int-to-long v8, v5

    mul-long/2addr v6, v8

    int-to-long v8, v4

    div-long/2addr v6, v8

    long-to-int v3, v6

    add-int p1, p3, v3

    move v3, v0

    move v0, p1

    :goto_1
    if-gt v0, v1, :cond_1

    if-ge v0, v2, :cond_4

    :cond_1
    add-int v0, v5, p3

    if-lez v0, :cond_3

    :goto_2
    sub-int v0, v1, p3

    int-to-long v2, v0

    int-to-long v6, v4

    mul-long/2addr v2, v6

    int-to-long v4, v5

    div-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v0, p2

    :goto_3
    const/4 v2, 0x0

    aput v0, p4, v2

    const/4 v0, 0x1

    aput v1, p4, v0

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    move v1, v0

    move v0, v3

    goto :goto_3

    :cond_5
    move v0, p1

    move v3, p0

    goto :goto_1
.end method

.method private a(LaN/Y;)V
    .locals 12

    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    if-ne p1, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    array-length v0, v0

    new-array v0, v0, [[[J

    iput-object v0, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v0, v0, v1

    instance-of v0, v0, Lcom/google/googlenav/ui/aH;

    if-nez v0, :cond_3

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v0, v0, v1

    check-cast v0, Lcom/google/googlenav/ui/aH;

    invoke-static {v0}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aH;)I

    move-result v4

    iget-object v2, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    new-array v3, v4, [[J

    aput-object v3, v2, v1

    invoke-static {v0}, Lcom/google/googlenav/ui/aR;->b(Lcom/google/googlenav/ui/aH;)[[LaN/B;

    move-result-object v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v6, v5, v3

    array-length v0, v6

    new-array v7, v0, [J

    const/4 v2, 0x1

    iput-object p1, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    const/4 v0, 0x0

    const/4 v8, 0x0

    aget-object v8, v6, v8

    iget-object v9, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    invoke-virtual {v8, v9}, LaN/B;->a(LaN/Y;)I

    move-result v8

    const/4 v9, 0x0

    aget-object v9, v6, v9

    iget-object v10, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    invoke-virtual {v9, v10}, LaN/B;->b(LaN/Y;)I

    move-result v9

    invoke-static {v8, v9}, Lcom/google/googlenav/ui/aR;->a(II)J

    move-result-wide v8

    aput-wide v8, v7, v0

    const/4 v0, 0x1

    :goto_2
    array-length v8, v6

    if-ge v0, v8, :cond_6

    aget-object v8, v6, v0

    iget-object v9, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    invoke-virtual {v8, v9}, LaN/B;->a(LaN/Y;)I

    move-result v8

    aget-object v9, v6, v0

    iget-object v10, p0, Lcom/google/googlenav/ui/aR;->b:LaN/Y;

    invoke-virtual {v9, v10}, LaN/B;->b(LaN/Y;)I

    move-result v9

    add-int/lit8 v10, v2, -0x1

    aget-wide v10, v7, v10

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v10

    sub-int v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    const/4 v11, 0x2

    if-gt v10, v11, :cond_4

    add-int/lit8 v10, v2, -0x1

    aget-wide v10, v7, v10

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v10

    sub-int v10, v9, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    const/4 v11, 0x2

    if-gt v10, v11, :cond_4

    array-length v10, v6

    add-int/lit8 v10, v10, -0x1

    if-ne v0, v10, :cond_5

    :cond_4
    invoke-static {v8, v9}, Lcom/google/googlenav/ui/aR;->a(II)J

    move-result-wide v8

    aput-wide v8, v7, v2

    add-int/lit8 v2, v2, 0x1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v0, v0, v1

    new-array v6, v2, [J

    aput-object v6, v0, v3

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v6, v6, v1

    aget-object v6, v6, v3

    const/4 v8, 0x0

    invoke-static {v7, v0, v6, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method private a(Lcom/google/googlenav/ui/aS;IIIILcom/google/googlenav/ui/aG;LaN/Y;)V
    .locals 8

    const/4 v1, -0x1

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->k()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->m()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->c()LaN/B;

    move-result-object v0

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->b()I

    move-result v1

    invoke-virtual {p7, v1}, LaN/Y;->a(I)I

    move-result v3

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->a()I

    move-result v1

    invoke-virtual {p7, v1}, LaN/Y;->a(I)I

    move-result v4

    invoke-virtual {v0, p7}, LaN/B;->a(LaN/Y;)I

    move-result v1

    sub-int/2addr v1, p2

    invoke-virtual {v0, p7}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int v2, v0, p3

    div-int/lit8 v0, v3, 0x2

    sub-int v0, v1, v0

    div-int/lit8 v5, v4, 0x2

    sub-int v5, v2, v5

    invoke-static {p4, p5, v0, v5}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v0

    div-int/lit8 v5, v3, 0x2

    add-int/2addr v5, v1

    div-int/lit8 v6, v4, 0x2

    add-int/2addr v6, v2

    invoke-static {p4, p5, v5, v6}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v5

    and-int/2addr v0, v5

    if-nez v0, :cond_0

    invoke-interface {p6, p7}, Lcom/google/googlenav/ui/aG;->a(LaN/Y;)I

    move-result v5

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->k()I

    move-result v6

    invoke-interface {p6}, Lcom/google/googlenav/ui/aG;->m()I

    move-result v7

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/google/googlenav/ui/aS;->a(IIIIIII)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/aS;IIII[JLcom/google/googlenav/ui/aH;LaN/Y;)V
    .locals 14

    const/4 v2, 0x2

    new-array v7, v2, [I

    const/4 v2, 0x2

    new-array v8, v2, [I

    const/4 v2, 0x2

    new-array v9, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-wide v3, p6, v3

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v3

    sub-int v3, v3, p2

    aput v3, v8, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aget-wide v3, p6, v3

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v3

    sub-int v3, v3, p3

    aput v3, v8, v2

    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v3, 0x1

    aget v3, v8, v3

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p6

    array-length v6, v0

    if-ge v2, v6, :cond_6

    const/4 v6, 0x0

    aget-wide v10, p6, v2

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v10

    sub-int v10, v10, p2

    aput v10, v7, v6

    const/4 v6, 0x1

    aget-wide v11, p6, v2

    invoke-static {v11, v12}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v11

    sub-int v11, v11, p3

    aput v11, v7, v6

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v10, v11}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v6

    and-int/2addr v5, v6

    if-nez v5, :cond_5

    if-nez v3, :cond_0

    invoke-interface/range {p7 .. p7}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v3

    invoke-interface/range {p7 .. p8}, Lcom/google/googlenav/ui/aH;->a(LaN/Y;)I

    move-result v5

    invoke-interface/range {p7 .. p7}, Lcom/google/googlenav/ui/aH;->d()I

    move-result v10

    invoke-interface {p1, v3, v5, v10}, Lcom/google/googlenav/ui/aS;->a(III)V

    const/4 v3, 0x1

    :cond_0
    invoke-static {v7}, Lcom/google/googlenav/ui/aR;->a([I)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v10, 0x0

    aget v10, v7, v10

    const/4 v11, 0x1

    aget v11, v7, v11

    const/4 v12, 0x0

    aget v12, v8, v12

    const/4 v13, 0x1

    aget v13, v8, v13

    invoke-static {v10, v11, v12, v13, v9}, Lcom/google/googlenav/ui/aR;->a(IIII[I)V

    :cond_1
    invoke-static {v8}, Lcom/google/googlenav/ui/aR;->a([I)Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v10, 0x0

    aget v10, v8, v10

    const/4 v11, 0x1

    aget v11, v8, v11

    const/4 v12, 0x0

    aget v12, v7, v12

    const/4 v13, 0x1

    aget v13, v7, v13

    invoke-static {v10, v11, v12, v13, v8}, Lcom/google/googlenav/ui/aR;->a(IIII[I)V

    :cond_2
    if-eqz v5, :cond_3

    invoke-interface {p1, v7, v8, v4}, Lcom/google/googlenav/ui/aS;->a([I[IZ)V

    :goto_1
    if-nez v5, :cond_4

    const/4 v4, 0x1

    :goto_2
    const/4 v5, 0x0

    const/4 v10, 0x0

    aget v10, v7, v10

    aput v10, v8, v5

    const/4 v5, 0x1

    const/4 v10, 0x1

    aget v10, v7, v10

    aput v10, v8, v5

    add-int/lit8 v2, v2, 0x1

    move v5, v6

    goto :goto_0

    :cond_3
    invoke-interface {p1, v9, v8, v4}, Lcom/google/googlenav/ui/aS;->a([I[IZ)V

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    if-eqz v3, :cond_7

    invoke-interface {p1}, Lcom/google/googlenav/ui/aS;->a()V

    :cond_7
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/aS;IIII[[JLcom/google/googlenav/ui/aH;LaN/Y;)V
    .locals 9

    const/4 v4, 0x0

    invoke-static/range {p7 .. p7}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aH;)I

    move-result v5

    new-array v6, v5, [[J

    const/4 v0, 0x0

    const/4 v1, 0x0

    aget-object v1, p6, v1

    invoke-static {p2, p3, v1}, Lcom/google/googlenav/ui/aR;->a(II[J)[J

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x0

    aget-object v0, v6, v0

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v0

    const/4 v1, 0x0

    aget-object v1, v6, v1

    const/4 v2, 0x0

    aget-wide v1, v1, v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v1

    invoke-static {p4, p5, v0, v1}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v1

    const/4 v0, 0x1

    move v2, v1

    :goto_0
    const/4 v3, 0x0

    aget-object v3, v6, v3

    array-length v3, v3

    if-ge v0, v3, :cond_4

    const/4 v3, 0x0

    aget-object v3, v6, v3

    aget-wide v7, v3, v0

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v3

    const/4 v7, 0x0

    aget-object v7, v6, v7

    aget-wide v7, v7, v0

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v7

    invoke-static {p4, p5, v3, v7}, Lcom/google/googlenav/ui/aR;->a(IIII)I

    move-result v3

    and-int/2addr v2, v3

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0xf

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-ge v0, v5, :cond_2

    aget-object v1, p6, v0

    invoke-static {p2, p3, v1}, Lcom/google/googlenav/ui/aR;->a(II[J)[J

    move-result-object v1

    aput-object v1, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    or-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-interface/range {p7 .. p7}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v0

    invoke-interface/range {p7 .. p8}, Lcom/google/googlenav/ui/aH;->a(LaN/Y;)I

    move-result v1

    invoke-interface/range {p7 .. p7}, Lcom/google/googlenav/ui/aH;->m()I

    move-result v2

    invoke-interface {p1, v6, v0, v1, v2}, Lcom/google/googlenav/ui/aS;->a([[JIII)V

    :cond_3
    return-void

    :cond_4
    move v0, v4

    goto :goto_1
.end method

.method protected static a([I)Z
    .locals 5

    const/16 v4, 0xfa0

    const/16 v3, -0xfa0

    const/4 v0, 0x1

    const/4 v1, 0x0

    aget v2, p0, v1

    if-gt v2, v4, :cond_0

    aget v2, p0, v1

    if-lt v2, v3, :cond_0

    aget v2, p0, v0

    if-gt v2, v4, :cond_0

    aget v2, p0, v0

    if-lt v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static a(II[J)[J
    .locals 5

    array-length v0, p2

    new-array v1, v0, [J

    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    aget-wide v2, p2, v0

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v2

    sub-int/2addr v2, p0

    aget-wide v3, p2, v0

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v3

    sub-int/2addr v3, p1

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aR;->a(II)J

    move-result-wide v2

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static b(J)I
    .locals 2

    const-wide v0, 0xffffffffL

    and-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method

.method private static b(Lcom/google/googlenav/ui/aH;)[[LaN/B;
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v1

    if-nez v1, :cond_0

    new-array v0, v0, [[LaN/B;

    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aH;)I

    move-result v1

    new-array v1, v1, [[LaN/B;

    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v2

    aput-object v2, v1, v3

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    invoke-interface {p0}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v3, v3, v0

    invoke-interface {v3}, Lcom/google/googlenav/ui/aI;->h()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    return v1

    :cond_0
    mul-int/lit8 v2, v2, 0x1d

    iget-object v3, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v3, v3, v0

    invoke-interface {v3}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ui/aS;IIIILaN/Y;)V
    .locals 12

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aR;->a()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p6

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/aR;->a(LaN/Y;)V

    const/4 v1, 0x0

    move v10, v1

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    array-length v1, v1

    if-ge v10, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v1, v1, v10

    instance-of v1, v1, Lcom/google/googlenav/ui/aG;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v7, v1, v10

    check-cast v7, Lcom/google/googlenav/ui/aG;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aS;IIIILcom/google/googlenav/ui/aG;LaN/Y;)V

    :cond_2
    :goto_1
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v8, v1, v10

    check-cast v8, Lcom/google/googlenav/ui/aH;

    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v1, v1, v10

    invoke-interface {v1}, Lcom/google/googlenav/ui/aI;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v7, v1, v10

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aS;IIII[[JLcom/google/googlenav/ui/aH;LaN/Y;)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    move v11, v1

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v1, v1, v10

    array-length v1, v1

    if-ge v11, v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v1, v1, v10

    aget-object v7, v1, v11

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aS;IIII[JLcom/google/googlenav/ui/aH;LaN/Y;)V

    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_2
.end method

.method public a(Lcom/google/googlenav/ui/aI;LaN/Y;)[J
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/aR;->a(LaN/Y;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/aR;->a:[Lcom/google/googlenav/ui/aI;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/aR;->c:[[[J

    aget-object v0, v2, v0

    aget-object v0, v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
