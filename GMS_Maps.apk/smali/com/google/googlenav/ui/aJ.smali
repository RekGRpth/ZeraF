.class public Lcom/google/googlenav/ui/aJ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/aH;


# instance fields
.field private final a:Lax/w;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(Lax/w;III)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/aJ;->e:I

    iput-object p1, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    iput p2, p0, Lcom/google/googlenav/ui/aJ;->b:I

    iput p3, p0, Lcom/google/googlenav/ui/aJ;->c:I

    iput p4, p0, Lcom/google/googlenav/ui/aJ;->d:I

    return-void
.end method

.method public static a(Z)I
    .locals 1

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Z)I
    .locals 1

    if-eqz p0, :cond_0

    const v0, -0x57f0f0f1

    :goto_0
    return v0

    :cond_0
    const v0, -0x57ffaf01

    goto :goto_0
.end method


# virtual methods
.method public a(LaN/Y;)I
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/aF;->b(LaN/Y;)I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/aJ;->e:I

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    invoke-virtual {v0}, Lax/w;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    invoke-virtual {v0}, Lax/w;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lo/D;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aJ;->e:I

    return v0
.end method

.method public h()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aJ;->a()Z

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    invoke-virtual {v0}, Lax/w;->K()I

    move-result v0

    return v0
.end method

.method public j()[LaN/B;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/aJ;->a:Lax/w;

    iget v1, p0, Lcom/google/googlenav/ui/aJ;->c:I

    iget v2, p0, Lcom/google/googlenav/ui/aJ;->d:I

    invoke-virtual {v0, v1, v2}, Lax/w;->a(II)[LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/aJ;->b:I

    return v0
.end method

.method public l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    const-string v0, "transit directions"

    return-object v0
.end method

.method public o()[[LaN/B;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, [[LaN/B;

    return-object v0
.end method
