.class public Lcom/google/googlenav/ui/android/AndroidVectorView;
.super Lcom/google/googlenav/ui/android/BaseAndroidView;
.source "SourceFile"


# instance fields
.field private f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private g:Lcom/google/android/maps/driveabout/vector/j;

.field private h:Lcom/google/android/maps/driveabout/vector/av;

.field private i:Lcom/google/googlenav/ui/android/ab;

.field private j:LaO/e;

.field private final k:Lcom/google/android/maps/driveabout/vector/bz;

.field private final l:Lcom/google/android/maps/driveabout/vector/by;

.field private final m:LaM/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/googlenav/ui/android/f;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/f;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->k:Lcom/google/android/maps/driveabout/vector/bz;

    new-instance v0, Lcom/google/googlenav/ui/android/g;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/g;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->l:Lcom/google/android/maps/driveabout/vector/by;

    new-instance v0, Lcom/google/googlenav/ui/android/h;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/h;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/AndroidVectorView;)LaO/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    return-object v0
.end method

.method private a(LA/c;Lr/H;I)Lr/D;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/bf;->a(LA/c;Landroid/content/Context;Landroid/content/res/Resources;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/D;

    invoke-virtual {v0, p2}, Lr/D;->a(Lr/H;)V

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Lr/D;->a(J)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LA/c;)V

    return-object v0
.end method

.method private a(LA/c;)V
    .locals 1

    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(LA/c;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f()V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/m;->b:Lcom/google/android/maps/driveabout/vector/m;

    :goto_0
    new-instance v1, Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/ButtonContainer;->d()Lcom/google/googlenav/ui/android/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ac;->a()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    new-instance v1, Lcom/google/googlenav/ui/android/i;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/android/i;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/android/ac;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->i:Lcom/google/googlenav/ui/android/ab;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->i:Lcom/google/googlenav/ui/android/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ac;->a(Lcom/google/googlenav/ui/android/ab;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const v2, 0x7f0b004c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b004d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/j;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    new-instance v1, Lcom/google/googlenav/ui/android/j;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/j;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/j;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    return-void

    :cond_1
    sget-object v0, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const v2, 0x7f0b0053

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/AndroidVectorView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->q()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method private n()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->p()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->q()V

    goto :goto_0
.end method

.method private o()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->r()V

    return-void
.end method

.method private p()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->c()I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/clientparam/k;->b()Z

    move-result v1

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/k;->a()J

    move-result-wide v2

    invoke-static {v0, v1}, Lr/D;->a(IZ)Lr/H;

    move-result-object v0

    sget-object v1, LA/c;->q:LA/c;

    long-to-int v2, v2

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;Lr/H;I)Lr/D;

    return-void
.end method

.method private q()V
    .locals 1

    sget-object v0, LA/c;->q:LA/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;)V

    return-void
.end method

.method private r()V
    .locals 1

    sget-object v0, LA/c;->r:LA/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;)V

    return-void
.end method

.method private s()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->a()J

    move-result-wide v0

    sget-object v2, LA/c;->s:LA/c;

    invoke-static {}, Lr/D;->n()Lr/H;

    move-result-object v3

    long-to-int v0, v0

    invoke-direct {p0, v2, v3, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;Lr/H;I)Lr/D;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lr/D;->b(Z)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IFFLcom/google/android/maps/driveabout/vector/aX;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->x()LG/a;

    move-result-object v1

    invoke-static {v1, p1, p2, p3, p4}, LG/a;->a(LG/a;IFFLcom/google/android/maps/driveabout/vector/aX;)LG/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTheme(LG/a;)V

    return-void
.end method

.method public a(LaN/B;Landroid/view/View;Lcom/google/googlenav/ui/view/d;Lcom/google/googlenav/ui/view/c;)V
    .locals 9

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    new-instance v0, LF/H;

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v3

    invoke-static {v1, v3}, Lo/T;->b(II)Lo/T;

    move-result-object v1

    move-object v3, v2

    move v5, v4

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v2, Lcom/google/googlenav/ui/android/k;

    invoke-direct {v2, p0, p4, p3}, Lcom/google/googlenav/ui/android/k;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/d;)V

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v2, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v2, p2}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .locals 7

    const/4 v6, 0x1

    const-string v0, "AndroidVectorView.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    new-instance v0, Lcom/google/googlenav/ui/android/b;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->g()LaN/p;

    move-result-object v4

    check-cast v4, LaO/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v0, Lcom/google/googlenav/ui/android/l;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/l;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;Lcom/google/googlenav/ui/android/f;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->k:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->l:Lcom/google/android/maps/driveabout/vector/by;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setInterceptingOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/by;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v4, v0}, LaO/a;->a(Lcom/google/android/maps/driveabout/vector/bA;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v4, v0}, LaO/a;->a(Lo/C;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/aq;

    move-result-object v0

    invoke-virtual {v4, v0}, LaO/a;->a(Lcom/google/android/maps/driveabout/vector/aq;)V

    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    check-cast v0, LaO/e;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v1

    invoke-virtual {v0, v1}, LaO/e;->a(F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    invoke-virtual {v4}, LaO/a;->d()LaN/B;

    move-result-object v1

    invoke-virtual {v4}, LaO/a;->c()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaO/e;->e(LaN/B;LaN/Y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    const/high16 v1, 0x42480000

    invoke-virtual {v0, v1}, LaO/e;->b(F)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->n()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->o()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->s()V

    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v1

    invoke-static {v0, v1}, LaO/e;->a(LaN/Y;F)F

    move-result v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(F)V

    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, LS/e;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LS/e;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, LS/d;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LS/d;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setFocusable(Z)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setFocusableInTouchMode(Z)V

    sget-object v0, LaE/d;->a:LaE/d;

    invoke-virtual {v0}, LaE/d;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/av;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/av;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->h:Lcom/google/android/maps/driveabout/vector/av;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->h:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_2
    const-string v0, "AndroidVectorView.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 2

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    invoke-virtual {v0, v1}, LaM/f;->b(LaM/h;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b()V

    return-void
.end method

.method public c()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, Lcom/google/googlenav/ui/android/m;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/android/m;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/android/f;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aG;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n_()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    invoke-virtual {v0, v1}, LaM/f;->a(LaM/h;)V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    invoke-virtual {v0}, Lbg/b;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbg/b;->e(Lbf/i;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    invoke-virtual {v0}, Lbg/b;->ab()V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o_()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i()V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->n()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->s()V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    :cond_0
    return-void
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->w()V

    return-void
.end method

.method public l()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    return-void
.end method

.method public setCompassMargin(II)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    :cond_0
    return-void
.end method
