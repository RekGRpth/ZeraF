.class Lcom/google/googlenav/ui/android/multilinetextview/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/method/TransformationMethod;


# static fields
.field static a:Lcom/google/googlenav/ui/android/multilinetextview/j;

.field private static final b:Lcom/google/common/base/A;

.field private static final c:Lcom/google/common/base/S;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/common/base/A;->a(C)Lcom/google/common/base/A;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->b:Lcom/google/common/base/A;

    invoke-static {v1}, Lcom/google/common/base/S;->a(C)Lcom/google/common/base/S;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->c:Lcom/google/common/base/S;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/ui/android/multilinetextview/j;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->a:Lcom/google/googlenav/ui/android/multilinetextview/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/j;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/multilinetextview/j;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->a:Lcom/google/googlenav/ui/android/multilinetextview/j;

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->a:Lcom/google/googlenav/ui/android/multilinetextview/j;

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)Ljava/lang/CharSequence;
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/j;->c:Lcom/google/common/base/S;

    invoke-virtual {v0, p1}, Lcom/google/common/base/S;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/android/multilinetextview/j;->a(Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)Lcom/google/common/base/x;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/android/multilinetextview/j;->b:Lcom/google/common/base/A;

    invoke-static {v0}, Lcom/google/common/collect/aT;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/common/base/A;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)I
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->getMaxLines()I

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)Lcom/google/common/base/x;
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/j;->b(Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/a;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/android/multilinetextview/a;-><init>(Landroid/widget/TextView;II)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/g;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/multilinetextview/g;-><init>(I)V

    goto :goto_0
.end method

.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    check-cast p2, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/multilinetextview/j;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 0

    return-void
.end method
