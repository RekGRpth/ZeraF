.class public Lcom/google/googlenav/ui/android/ElevationSeriesView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Path;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Path;

.field private e:Landroid/graphics/Paint;

.field private f:Lcom/google/googlenav/ui/android/E;

.field private g:Lcom/google/googlenav/ui/android/E;

.field private h:Ljava/util/List;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/android/ElevationSeriesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v2, 0x7f090076

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a:Landroid/graphics/Path;

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->setChartColor(II)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->setWillNotDraw(Z)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 18

    const/4 v7, 0x0

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    const/4 v3, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lay/a;

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a:Landroid/graphics/Path;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v11

    invoke-virtual {v2, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v11

    invoke-virtual {v2, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    move v2, v3

    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v10

    cmpl-double v3, v10, v5

    if-lez v3, :cond_5

    :cond_1
    int-to-float v6, v2

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v4

    const/4 v3, 0x1

    :goto_2
    move v7, v6

    move v14, v2

    move-object v2, v1

    move v15, v3

    move v3, v14

    move-wide/from16 v16, v4

    move-wide/from16 v5, v16

    move v4, v15

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lay/a;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v1}, Lay/a;->a()LaN/B;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v2

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->f:Lcom/google/googlenav/ui/android/E;

    int-to-double v11, v2

    invoke-virtual {v10, v11, v12}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->f:Lcom/google/googlenav/ui/android/E;

    int-to-double v11, v2

    invoke-virtual {v10, v11, v12}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->f:Lcom/google/googlenav/ui/android/E;

    int-to-double v3, v3

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->d:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->e:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->i:Z

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->c:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->f:Lcom/google/googlenav/ui/android/E;

    float-to-double v2, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    invoke-virtual {v2, v5, v6}, Lcom/google/googlenav/ui/android/E;->a(D)F

    move-result v2

    const/high16 v3, 0x40400000

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_4
    return-void

    :cond_5
    move v3, v4

    move-wide v14, v5

    move-wide v4, v14

    move v6, v7

    goto/16 :goto_2
.end method


# virtual methods
.method a(Lcom/google/googlenav/ui/android/E;Lcom/google/googlenav/ui/android/E;)V
    .locals 1

    const-string v0, "missing horizontal axis"

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/E;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->f:Lcom/google/googlenav/ui/android/E;

    const-string v0, "missing vertical axis"

    invoke-static {p2, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/E;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->g:Lcom/google/googlenav/ui/android/E;

    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->h:Ljava/util/List;

    iput-boolean p2, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->i:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->h:Ljava/util/List;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setChartColor(II)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ElevationSeriesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->c:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ElevationSeriesView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method
