.class public Lcom/google/googlenav/ui/android/LoadingFooterView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/view/u;

.field private b:Lcom/google/googlenav/ui/view/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/LoadingFooterView;->a:Lcom/google/googlenav/ui/view/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/LoadingFooterView;->a:Lcom/google/googlenav/ui/view/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/LoadingFooterView;->b:Lcom/google/googlenav/ui/view/a;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/u;->a(Lcom/google/googlenav/ui/view/t;)Z

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method public setViewportListener(Lcom/google/googlenav/ui/view/u;Lcom/google/googlenav/ui/view/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/LoadingFooterView;->a:Lcom/google/googlenav/ui/view/u;

    iput-object p2, p0, Lcom/google/googlenav/ui/android/LoadingFooterView;->b:Lcom/google/googlenav/ui/view/a;

    return-void
.end method
