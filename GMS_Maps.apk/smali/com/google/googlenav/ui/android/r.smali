.class public Lcom/google/googlenav/ui/android/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/android/D;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final s:LE/o;

.field private static final t:LE/d;

.field private static final u:LE/d;

.field private static final v:LE/d;

.field private static final w:LE/o;

.field private static final x:LE/d;


# instance fields
.field private final A:LE/i;

.field private final B:Lcom/google/android/maps/driveabout/vector/aV;

.field private final C:Lcom/google/googlenav/ui/android/w;

.field private D:LC/a;

.field private E:Lbf/ah;

.field private volatile F:Z

.field private G:Z

.field private volatile e:Landroid/graphics/Point;

.field private volatile f:Landroid/graphics/Point;

.field private g:Lcom/google/googlenav/ui/android/y;

.field private h:Lcom/google/googlenav/ui/android/B;

.field private i:Lcom/google/googlenav/ui/android/A;

.field private j:Lcom/google/googlenav/ui/android/x;

.field private k:Lcom/google/googlenav/ui/au;

.field private l:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private m:Lcom/google/android/maps/MapsActivity;

.field private n:LaN/u;

.field private o:Z

.field private p:I

.field private volatile q:I

.field private volatile r:I

.field private y:[F

.field private z:LD/b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x6

    const/4 v3, 0x2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->a:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->b:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->c:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->d:I

    new-instance v0, LE/d;

    invoke-direct {v0, v4}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    invoke-virtual {v0, v5, v6}, LE/d;->a(SS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v6, v1}, LE/d;->a(SS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LE/d;->a(SS)V

    new-instance v0, LE/d;

    invoke-direct {v0, v5}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v5}, LE/d;->a(S)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v6}, LE/d;->a(S)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v4}, LE/d;->a(S)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, LE/d;->a(S)V

    new-instance v0, LE/d;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x8

    const/16 v2, 0x9

    invoke-virtual {v0, v7, v1, v2}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x9

    const/4 v2, 0x1

    invoke-virtual {v0, v7, v1, v2}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v5, v6}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6, v3}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x9

    const/16 v2, 0xa

    invoke-virtual {v0, v4, v1, v2}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xa

    const/4 v2, 0x7

    invoke-virtual {v0, v4, v1, v2}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xa

    const/16 v2, 0xb

    invoke-virtual {v0, v3, v1, v2}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xb

    const/4 v2, 0x3

    invoke-virtual {v0, v3, v1, v2}, LE/d;->a(SSS)V

    new-instance v0, LE/d;

    invoke-direct {v0, v4}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v7, v3, v1}, LE/d;->a(SSS)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v3}, LE/d;->a(SSS)V

    new-instance v0, LE/o;

    invoke-direct {v0, v5}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->w:LE/o;

    new-instance v0, LE/o;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/MapsActivity;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->A:LE/i;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    new-instance v0, Lcom/google/googlenav/ui/android/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/android/w;-><init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;I)I
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/android/r;->p:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;LC/a;)LC/a;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;LD/b;)LD/b;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->z:LD/b;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;)Lbf/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;[F)[F
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    return-object p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->t()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/r;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->s()V

    return-void
.end method

.method static synthetic d()LE/o;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    return-object v0
.end method

.method static synthetic e()LE/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/MapsActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/r;->a:I

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/android/r;)LC/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    return-object v0
.end method

.method static synthetic g()LE/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/android/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->u()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/android/r;)LD/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->z:LD/b;

    return-object v0
.end method

.method static synthetic h()LE/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    return-object v0
.end method

.method static synthetic i()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/r;->d:I

    return v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/driveabout/vector/aV;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/android/r;)LE/i;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->A:LE/i;

    return-object v0
.end method

.method static synthetic j()LE/o;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->w:LE/o;

    return-object v0
.end method

.method static synthetic k()LE/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/android/r;)[F
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    return-object v0
.end method

.method static synthetic l()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/r;->c:I

    return v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/android/r;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    return v0
.end method

.method static synthetic m()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/r;->b:I

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/android/r;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/r;->p:I

    return v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    return-object v0
.end method

.method private n()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->t()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowRotateGesture(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowTiltGesture(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowLongPressGesture(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowSingleTapGesture(Z)V

    return-void
.end method

.method static synthetic o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    return-object v0
.end method

.method private o()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->u()V

    return-void
.end method

.method static synthetic p(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/x;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    return-object v0
.end method

.method private p()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/ButtonContainer;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->p()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(LS/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->b(Lcom/google/googlenav/ui/android/D;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/view/android/bC;->a(Lcom/google/googlenav/ui/au;)V

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->o()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/x;->c()V

    iput-object v4, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/android/s;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/android/s;-><init>(Lcom/google/googlenav/ui/android/r;Las/c;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/s;->g()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->k:Lcom/google/googlenav/ui/au;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->j()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBottomBar()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f100202

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f10003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private q()V
    .locals 7

    const v6, 0x7f020222

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x349

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v6, v1, v2, v5}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/actionbar/a;->a(Z)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v6, v1, v2, v5}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f100202

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f10003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic q(Lcom/google/googlenav/ui/android/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->v()V

    return-void
.end method

.method static synthetic r(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/AndroidVectorView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    return-object v0
.end method

.method private r()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBottomBar()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f100030

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/16 v2, 0x348

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v2, v2, Lcom/google/googlenav/ui/android/w;->i:Z

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v2, Lcom/google/googlenav/ui/android/t;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/android/t;-><init>(Lcom/google/googlenav/ui/android/r;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f100031

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/google/googlenav/ui/android/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/u;-><init>(Lcom/google/googlenav/ui/android/r;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private s()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->p()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->h:Lcom/google/googlenav/ui/android/B;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/B;->a()V

    return-void
.end method

.method private t()V
    .locals 6

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->p()V

    const-string v0, "s"

    const-string v1, ""

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, LJ/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->i:Lcom/google/googlenav/ui/android/A;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->w()LaN/B;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->x()LaN/B;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/googlenav/ui/android/A;->a(LaN/B;LaN/B;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private u()V
    .locals 8

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/google/googlenav/ui/android/r;->r:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/google/googlenav/ui/android/r;->r:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LE/o;->a(LD/a;)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5, v7}, LE/o;->a(FFF)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v0

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v2

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->q:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v0

    int-to-float v6, v1

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v2

    int-to-float v1, v1

    invoke-virtual {v4, v5, v1, v7}, LE/o;->a(FFF)V

    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v4, v0

    int-to-float v5, v3

    invoke-virtual {v1, v4, v5, v7}, LE/o;->a(FFF)V

    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v4, v2

    int-to-float v3, v3

    invoke-virtual {v1, v4, v3, v7}, LE/o;->a(FFF)V

    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    invoke-virtual {v1, v7, v7, v7}, LE/o;->a(FFF)V

    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v0, v0

    invoke-virtual {v1, v0, v7, v7}, LE/o;->a(FFF)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v1, v2

    invoke-virtual {v0, v1, v7, v7}, LE/o;->a(FFF)V

    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v1, p0, Lcom/google/googlenav/ui/android/r;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v7, v7}, LE/o;->a(FFF)V

    goto :goto_0
.end method

.method private v()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/android/v;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/v;-><init>(Lcom/google/googlenav/ui/android/r;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method private w()LaN/B;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method private x()LaN/B;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->h:Lcom/google/googlenav/ui/android/B;

    iput-object p2, p0, Lcom/google/googlenav/ui/android/r;->i:Lcom/google/googlenav/ui/android/A;

    iput-object p3, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbf/bk;->bI()Lbf/ah;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    invoke-virtual {v0, v1}, Lbf/ah;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->E()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbf/am;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v6}, Lbf/am;->a(Ljava/lang/String;)Lbf/aM;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->a()Lcom/google/googlenav/ui/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->k:Lcom/google/googlenav/ui/au;

    new-instance v0, Lcom/google/googlenav/ui/android/z;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/z;-><init>(Lcom/google/googlenav/ui/android/r;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bC;->a(Lcom/google/googlenav/ui/au;)V

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->q()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->r()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(Lcom/google/googlenav/ui/android/D;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v0}, LaN/u;->i()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    const/16 v1, 0x35c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    sget v4, Lcom/google/googlenav/ui/android/r;->d:I

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v0}, LaN/u;->n()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v1}, LaN/u;->o()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/android/r;->b(II)V

    iput-boolean v5, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->setVisibility(I)V

    new-instance v0, Lcom/google/googlenav/ui/android/y;

    invoke-direct {v0, p0, v6}, Lcom/google/googlenav/ui/android/y;-><init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    const/16 v0, 0x34d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/r;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    move-object v0, v6

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    return v0
.end method

.method public b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->s()V

    return-void
.end method

.method public b(II)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iput p2, p0, Lcom/google/googlenav/ui/android/r;->r:I

    iput p1, p0, Lcom/google/googlenav/ui/android/r;->q:I

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->ar()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    int-to-float v0, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    sub-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v2, p2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v4, v4, v1

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    :goto_1
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x46

    :goto_2
    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    new-instance v3, Landroid/graphics/Point;

    iget v4, p0, Lcom/google/googlenav/ui/android/r;->q:I

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    sub-int/2addr v5, v2

    add-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    new-instance v3, Landroid/graphics/Point;

    iget v4, p0, Lcom/google/googlenav/ui/android/r;->q:I

    add-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    add-int/2addr v2, v5

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v3, v4, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    float-to-int v2, v2

    sub-int/2addr v0, v2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v3, v3, v1

    float-to-int v3, v3

    add-int/2addr v2, v3

    sub-int v2, p2, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v0, v0, v1

    float-to-int v0, v0

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    float-to-int v2, v2

    sub-int v2, p2, v2

    goto :goto_1

    :cond_3
    const/16 v2, 0x5a

    goto :goto_2
.end method

.method public c()V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v6, v0, Lcom/google/googlenav/ui/android/w;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->w()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->x()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v3

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    invoke-virtual {v4}, LC/a;->r()F

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/ui/android/x;->a(LaN/B;LaN/B;IFLcom/google/googlenav/ui/android/w;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/android/w;->i:Z

    if-eq v6, v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->v()V

    :cond_0
    return-void
.end method
