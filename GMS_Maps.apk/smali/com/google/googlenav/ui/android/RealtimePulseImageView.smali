.class public Lcom/google/googlenav/ui/android/RealtimePulseImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    iput p2, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Landroid/widget/ImageView;->getBaseline()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->a:I

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    new-instance v0, Lcom/google/googlenav/ui/android/at;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/at;-><init>(Lcom/google/googlenav/ui/android/RealtimePulseImageView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimePulseImageView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
