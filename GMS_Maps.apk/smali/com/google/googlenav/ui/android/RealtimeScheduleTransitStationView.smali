.class public Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;
.super Lcom/google/googlenav/ui/android/TransitStationView;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ci;


# static fields
.field public static final a:I

.field private static final i:I

.field private static final j:I

.field private static final k:I

.field private static final l:I

.field private static final m:I


# instance fields
.field private final n:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0xc

    const/16 v2, 0xa

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->i:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->j:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->k:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->l:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->m:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/TransitStationView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/googlenav/ui/android/au;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/au;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->n:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/TransitStationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/googlenav/ui/android/au;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/au;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->n:Ljava/util/Comparator;

    return-void
.end method

.method private a(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/widget/TextView;
    .locals 2

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Ljava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f0401a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;)Lcom/google/googlenav/android/c;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->j()Lcom/google/googlenav/android/c;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V
    .locals 6

    const v5, 0x7f10046e

    const v4, 0x7f10046d

    const/16 v3, 0x8

    const/4 v1, 0x0

    const v0, 0x7f10046f

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->n()I

    move-result v2

    if-lez v2, :cond_0

    const/16 v2, 0x2fc

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v5, v2}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f10046c

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->n()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/bZ;->c(I)Lcom/google/googlenav/bZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f090019

    new-instance v5, Lcom/google/googlenav/ui/android/ax;

    invoke-direct {v5, p0, v2, p2}, Lcom/google/googlenav/ui/android/ax;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Landroid/view/ViewGroup;Ljava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/googlenav/cj;Landroid/widget/LinearLayout;)V
    .locals 5

    const/4 v4, -0x2

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->c:I

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020438

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    sget v3, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->i:I

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const v0, 0x7f090006

    const v2, 0x7f090001

    invoke-virtual {p1}, Lcom/google/googlenav/cj;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/cj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/cj;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/cj;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/cj;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f090024

    new-instance v3, Lcom/google/googlenav/ui/android/aw;

    invoke-direct {v3, p0, p1}, Lcom/google/googlenav/ui/android/aw;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;Lcom/google/googlenav/cj;)V

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Landroid/view/ViewGroup;Ljava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;

    :cond_0
    return-void
.end method

.method private b(Lcom/google/googlenav/bZ;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->c(Lcom/google/googlenav/bZ;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->d(Lcom/google/googlenav/bZ;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->e(Lcom/google/googlenav/bZ;)V

    return-void
.end method

.method private c(Lcom/google/googlenav/bZ;)V
    .locals 7

    const v6, 0x7f10045f

    const/16 v5, 0x8

    const/4 v2, 0x0

    const v0, 0x7f10045d

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f10045e

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->l()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    const/16 v3, 0x38

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->l()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bZ;->b(I)Lcom/google/googlenav/cj;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Lcom/google/googlenav/cj;Landroid/widget/LinearLayout;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic d()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->k:I

    return v0
.end method

.method private d(Lcom/google/googlenav/bZ;)V
    .locals 9

    const v8, 0x7f100466

    const v7, 0x7f100462

    const v6, 0x7f100460

    const/16 v5, 0x8

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->k()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    move v0, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->k()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->n:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const v0, 0x7f100465

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/SelfMeasuredListView;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f100467

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    const/16 v3, 0x5d4

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f100463

    invoke-virtual {p0, v4, v3}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    const v3, 0x7f100464

    const/16 v4, 0x1fb

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    const/16 v3, 0x213

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v8, v3}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    new-instance v3, Lcom/google/googlenav/ui/android/ay;

    invoke-direct {v3, p0, v1}, Lcom/google/googlenav/ui/android/ay;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVerticalFadingEdgeEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVisibility(I)V

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setSelector(I)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setDividerHeight(I)V

    const v0, 0x7f100461

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    new-instance v1, Lcom/google/googlenav/ui/android/RealtimePulseImageView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->m:I

    invoke-direct {v1, v3, v4}, Lcom/google/googlenav/ui/android/RealtimePulseImageView;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setId(I)V

    const v3, 0x7f020364

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    sget v4, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->l:I

    sget v5, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->m:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f100467

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v8}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic e()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->j:I

    return v0
.end method

.method private e(Lcom/google/googlenav/bZ;)V
    .locals 8

    const v7, 0x7f10046b

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->k()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->k()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->d()I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const v0, 0x7f10046a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/SelfMeasuredListView;

    const v3, 0x7f100468

    const/16 v4, 0x5d5

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    new-instance v3, Lcom/google/googlenav/ui/android/az;

    invoke-direct {v3, p0, v2}, Lcom/google/googlenav/ui/android/az;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVerticalFadingEdgeEnabled(Z)V

    const v2, 0x106000d

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setSelector(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVisibility(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setDividerHeight(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const v1, 0x7f100469

    const/16 v2, 0x4f2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/android/SelfMeasuredListView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->k()I

    move-result v0

    if-ge v0, v5, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->n()I

    move-result v0

    if-ge v0, v5, :cond_2

    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->h:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/google/googlenav/android/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/co;)Ljava/lang/CharSequence;
    .locals 9

    const/4 v0, 0x0

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v1, v0

    :goto_0
    const/4 v3, 0x3

    if-ge v0, v3, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/co;->d()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/co;->a(I)Lcom/google/googlenav/cc;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cc;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    if-lez v0, :cond_0

    const-string v4, ", "

    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3}, Lcom/google/googlenav/cc;->c()J

    move-result-wide v5

    const-wide/16 v7, 0x3c

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0f00b9

    invoke-direct {v3, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->h:Lcom/google/googlenav/bZ;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->b(Lcom/google/googlenav/bZ;)V

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method protected c()V
    .locals 2

    const v0, 0x7f10046c

    const/16 v1, 0x5d3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(ILjava/lang/String;)V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/z;->e:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/android/av;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/av;-><init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->h:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->g()V

    goto :goto_0
.end method

.method public setTransitStation(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/android/TransitStationView;->setTransitStation(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V

    const v0, 0x7f100356

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p0}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/ci;)V

    const/16 v0, 0x54

    const-string v1, "ts"

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->b(Lcom/google/googlenav/bZ;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Lcom/google/googlenav/bZ;)V

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;->a(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V

    return-void
.end method
