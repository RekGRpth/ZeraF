.class Lcom/google/googlenav/ui/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/ak;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v2, 0x0

    const/high16 v1, -0x40800000

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    iget-object v3, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v3}, Lcom/google/googlenav/ui/ak;->b(Lcom/google/googlenav/ui/ak;)LaH/m;

    move-result-object v3

    invoke-interface {v3}, LaH/m;->s()LaH/h;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/ak;->a(Lcom/google/googlenav/ui/ak;LaH/h;)LaH/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->f()V

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->c(Lcom/google/googlenav/ui/ak;)LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->a(LaH/h;)LaN/B;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v0

    invoke-virtual {v0}, Lo/S;->k()V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->e(Lcom/google/googlenav/ui/ak;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->b(Lcom/google/googlenav/ui/ak;)LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->c(Lcom/google/googlenav/ui/ak;)LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->b(Landroid/location/Location;)I

    move-result v0

    int-to-float v0, v0

    :goto_1
    invoke-static {v3}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v4}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v4

    invoke-virtual {v4, v3}, Lo/S;->a(Lo/T;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v3}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v3

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v3, v1}, Lo/S;->a(Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo/S;->a(F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->c(Lcom/google/googlenav/ui/ak;)LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v1

    const v3, 0x1869f

    if-ne v0, v3, :cond_3

    :goto_3
    invoke-virtual {v1, v2}, Lo/S;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->d(Lcom/google/googlenav/ui/ak;)Lo/S;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/am;->a:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->c(Lcom/google/googlenav/ui/ak;)LaH/h;

    move-result-object v1

    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/D;)V

    goto :goto_0

    :cond_1
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v2, v0

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method
