.class public Lcom/google/googlenav/ui/aW;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/googlenav/ui/aV;

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/google/googlenav/ui/view/t;

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;Z)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/googlenav/ui/aW;->h:I

    iput v0, p0, Lcom/google/googlenav/ui/aW;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/aW;->j:I

    iput v0, p0, Lcom/google/googlenav/ui/aW;->k:I

    if-eqz p1, :cond_1

    if-eqz p6, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/aW;->a:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    :goto_1
    iput-object p2, p0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    iput-boolean p3, p0, Lcom/google/googlenav/ui/aW;->d:Z

    iput-boolean p4, p0, Lcom/google/googlenav/ui/aW;->e:Z

    iput-object p5, p0, Lcom/google/googlenav/ui/aW;->f:Lcom/google/googlenav/ui/view/t;

    return-void

    :cond_0
    iput-object v1, p0, Lcom/google/googlenav/ui/aW;->a:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/google/googlenav/ui/aW;->a:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;Z)V

    return-void
.end method

.method public static a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v0, Lcom/google/googlenav/ui/aW;

    move-object v1, p0

    move-object v2, p1

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;Z)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;
    .locals 6

    const/4 v3, 0x0

    new-instance v0, Lcom/google/googlenav/ui/aW;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/aW;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/aW;
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/aW;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/aW;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/aW;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;ZZLcom/google/googlenav/ui/view/t;)V

    return-object v0
.end method


# virtual methods
.method public a(IIII)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/aW;->h:I

    iput p2, p0, Lcom/google/googlenav/ui/aW;->i:I

    iput p3, p0, Lcom/google/googlenav/ui/aW;->j:I

    iput p4, p0, Lcom/google/googlenav/ui/aW;->k:I

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/aW;->g:Ljava/lang/Object;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aW;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aW;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    move v1, v0

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    check-cast p1, Lcom/google/googlenav/ui/aW;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aW;->a()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/aW;->a()Z

    move-result v3

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/googlenav/ui/aW;->d:Z

    iget-boolean v3, p1, Lcom/google/googlenav/ui/aW;->d:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/googlenav/ui/aW;->e:Z

    iget-boolean v3, p1, Lcom/google/googlenav/ui/aW;->e:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    iget-object v3, p1, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    if-ne v2, v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/googlenav/ui/aW;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/googlenav/ui/aW;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aW;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    return-object v0
.end method
