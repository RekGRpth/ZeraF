.class public Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;
.super Lcom/google/googlenav/actionbar/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;


# static fields
.field private static final a:[I

.field public static final b:Landroid/graphics/drawable/ColorDrawable;

.field public static final c:Landroid/graphics/drawable/ColorDrawable;

.field public static final d:Landroid/graphics/drawable/ColorDrawable;

.field public static final e:Landroid/graphics/drawable/ColorDrawable;

.field static final synthetic g:Z


# instance fields
.field protected f:Landroid/app/ActionBar;

.field private h:Landroid/widget/SearchView;

.field private i:Lcom/google/googlenav/ui/s;

.field private j:Landroid/app/SearchManager;

.field private k:Lcom/google/android/maps/MapsActivity;

.field private l:Lcom/google/googlenav/actionbar/b;

.field private m:Landroid/view/MenuItem;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:I

.field private q:LaA/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, -0x67000000

    const-class v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    new-array v0, v1, [I

    const v1, 0x10102eb

    aput v1, v0, v2

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a:[I

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->c:Landroid/graphics/drawable/ColorDrawable;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x34000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0xd6d6d7

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->e:Landroid/graphics/drawable/ColorDrawable;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/a;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)Lcom/google/googlenav/actionbar/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method private a(Ljava/lang/String;LaN/B;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v1

    invoke-virtual {v1, p2}, LaN/u;->c(LaN/B;)V

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1}, Lcom/google/googlenav/actionbar/b;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p1, p2}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1, p1}, Lcom/google/googlenav/actionbar/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1, p1}, Lcom/google/googlenav/actionbar/b;->a(Ljava/lang/String;)Lcom/google/googlenav/bf;

    move-result-object v1

    if-eqz v0, :cond_5

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2, v1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->b(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method private a(ILbb/w;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {p2}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lbb/o;->i()V

    invoke-static {v1}, Lau/b;->i(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Lbb/o;->a(III)V

    const-string v2, "s"

    sget-object v3, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-virtual {v0, v1, v2, v3}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    const-string v1, "stars"

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v0, p2}, Lcom/google/googlenav/actionbar/b;->a(Lbb/w;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbf/am;->a(Lbb/w;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lbb/w;->h()LaN/B;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Ljava/lang/String;LaN/B;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private p()V
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb/o;->c(I)Lbb/w;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0, v2, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(ILbb/w;)Z

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()LaA/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/actionbar/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/actionbar/e;-><init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)V

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setIcon(I)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    :cond_2
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1, p2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    sget-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    if-nez v0, :cond_3

    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iput-object p3, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    check-cast p1, Landroid/widget/SearchView;

    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iput-object p2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const/16 v1, 0x4f8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setMaxWidth(I)V

    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    invoke-virtual {v2, v0}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/googlenav/actionbar/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/actionbar/d;-><init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)V

    invoke-interface {p2, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/s;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/16 v1, 0x2a8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSplitBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    iput-boolean v3, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_1
.end method

.method protected a(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/s;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->k:Lcom/google/android/maps/MapsActivity;

    iput-object p2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    const-string v0, "search"

    invoke-virtual {p1, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/google/googlenav/actionbar/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n()Landroid/content/Context;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LaA/h;->a(Landroid/app/ActionBar;Landroid/view/View;Landroid/app/ActionBar$LayoutParams;LaA/g;Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/google/googlenav/actionbar/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    :cond_0
    return-void
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    return v0
.end method

.method public j()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    goto :goto_0
.end method

.method public k()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a:[I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_2
    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public l()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    return-void
.end method

.method public m()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LaA/h;->a(Landroid/content/Context;Landroid/view/View;LaA/g;)V

    :cond_0
    return-void
.end method

.method public n()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->k:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->o()Lbf/i;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/n;->a(Lcom/google/googlenav/ui/wizard/C;Lbf/i;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbb/o;->d(I)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lbb/o;->j()V

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->i()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Ljava/lang/String;LaN/B;)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p()V

    goto :goto_0
.end method

.method public onSuggestionClick(I)Z
    .locals 1

    :try_start_0
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb/o;->c(I)Lbb/w;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(ILbb/w;)Z

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
