.class public Lcom/google/googlenav/co;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[Lcom/google/googlenav/cc;

.field private final c:Z

.field private d:Lcom/google/googlenav/cm;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cm;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v2, v3, [Lcom/google/googlenav/cc;

    iput-object v2, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v4, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    new-instance v5, Lcom/google/googlenav/cc;

    invoke-virtual {p1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/googlenav/cc;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v5, v4, v2

    iget-object v4, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    aget-object v4, v4, v2

    invoke-static {v4}, Lcom/google/googlenav/cc;->a(Lcom/google/googlenav/cc;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iput-boolean v0, p0, Lcom/google/googlenav/co;->c:Z

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/co;->a:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/co;->e:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/co;->d:Lcom/google/googlenav/cm;

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;[Lcom/google/googlenav/cc;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/co;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    iput-boolean p3, p0, Lcom/google/googlenav/co;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/co;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/co;->c:Z

    return v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/cc;
    .locals 8

    iget-object v2, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-virtual {v0}, Lcom/google/googlenav/cc;->c()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(I)Lcom/google/googlenav/cc;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Lcom/google/googlenav/cc;
    .locals 6

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/googlenav/co;->c:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    invoke-virtual {v1}, Lcom/google/googlenav/cc;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/google/googlenav/cc;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/co;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/co;->b:[Lcom/google/googlenav/cc;

    array-length v0, v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/co;->c:Z

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/co;->e:Ljava/lang/String;

    return-object v0
.end method
