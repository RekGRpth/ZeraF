.class public Lcom/google/googlenav/bN;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/F;


# instance fields
.field private a:B

.field private final b:Lbf/bG;

.field private final c:[Lcom/google/googlenav/bT;

.field private final d:[Lcom/google/googlenav/bS;

.field private e:I

.field private final f:Lcom/google/common/collect/ImmutableList;

.field private final g:I

.field private final h:Z


# direct methods
.method public constructor <init>(Lbf/bG;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 19

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/bN;->b:Lbf/bG;

    const/4 v2, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v4, v3, [Lcom/google/googlenav/cp;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    new-instance v5, Lcom/google/googlenav/bZ;

    const/4 v6, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    sget-object v7, Lcom/google/wireless/googlenav/proto/j2me/hF;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/cd;)V

    new-instance v6, Lcom/google/googlenav/cp;

    invoke-direct {v6, v5}, Lcom/google/googlenav/cp;-><init>(Lcom/google/googlenav/bZ;)V

    aput-object v6, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/googlenav/bN;->f:Lcom/google/common/collect/ImmutableList;

    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v13

    invoke-static {}, Lcom/google/common/collect/bj;->a()Lcom/google/common/collect/bj;

    move-result-object v14

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/googlenav/cm;->f()[Lcom/google/googlenav/cb;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v15}, Lcom/google/googlenav/cm;->f()[Lcom/google/googlenav/cb;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/googlenav/bN;->h:Z

    const/4 v2, 0x0

    move v12, v2

    :goto_2
    if-ge v12, v13, :cond_7

    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v16

    const/4 v2, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v17

    move/from16 v0, v17

    new-array v0, v0, [Lcom/google/googlenav/bV;

    move-object/from16 v18, v0

    const/4 v8, 0x0

    const/4 v2, 0x0

    move v11, v2

    :goto_3
    move/from16 v0, v17

    if-ge v11, v0, :cond_5

    const/4 v2, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v2, 0x2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    if-nez v8, :cond_c

    move-object v9, v4

    :goto_4
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    new-array v6, v5, [I

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v5, :cond_3

    const/4 v7, 0x1

    invoke-virtual {v3, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v7

    aput v7, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/google/googlenav/bV;

    invoke-virtual {v15, v4}, Lcom/google/googlenav/cm;->a(Ljava/lang/String;)Lcom/google/googlenav/co;

    move-result-object v5

    const/4 v7, 0x3

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x3

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    :goto_6
    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/googlenav/bV;-><init>(Lcom/google/googlenav/bN;Ljava/lang/String;Lcom/google/googlenav/co;[III)V

    aput-object v2, v18, v11

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move-object v8, v9

    goto :goto_3

    :cond_4
    const/4 v7, -0x1

    goto :goto_6

    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/util/List;

    move-result-object v5

    const/4 v2, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/googlenav/bN;->h:Z

    if-nez v2, :cond_6

    invoke-virtual {v15, v8}, Lcom/google/googlenav/cm;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_6
    new-instance v2, Lcom/google/googlenav/bT;

    add-int/lit8 v9, v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, v18

    move v7, v10

    invoke-direct/range {v2 .. v7}, Lcom/google/googlenav/bT;-><init>(Lcom/google/googlenav/bN;[Lcom/google/googlenav/bV;Ljava/util/List;Ljava/util/List;I)V

    invoke-virtual {v14, v8, v2}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move v2, v9

    :goto_7
    add-int/lit8 v3, v12, 0x1

    move v12, v3

    move v10, v2

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v14}, Lcom/google/common/collect/bj;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v14}, Lcom/google/common/collect/bj;->c()I

    move-result v3

    new-array v3, v3, [Lcom/google/googlenav/bT;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/googlenav/bT;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/googlenav/bN;->h:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/googlenav/bN;->a([Lcom/google/googlenav/bT;)Ljava/util/Collection;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/googlenav/bS;

    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/googlenav/bS;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    invoke-virtual/range {p1 .. p1}, Lbf/bG;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/googlenav/bN;->g:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/googlenav/bN;->g:I

    if-ltz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    array-length v2, v2

    if-lez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/bN;->g:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/googlenav/bS;->a()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/googlenav/bT;

    invoke-virtual {v2}, Lcom/google/googlenav/bT;->f()I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/bN;->a(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/bN;->b()Lcom/google/googlenav/bV;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bV;->c()I

    move-result v2

    if-gez v2, :cond_8

    const/4 v2, 0x0

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bN;->a(I)V

    :cond_9
    return-void

    :cond_a
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v14}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/cm;Lcom/google/common/collect/cV;)Ljava/util/List;

    move-result-object v2

    goto :goto_8

    :cond_b
    move v2, v10

    goto/16 :goto_7

    :cond_c
    move-object v9, v8

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/google/googlenav/bN;Ljava/util/List;[I[Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;I)I
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/googlenav/bN;->a(Ljava/util/List;[I[Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;I)I

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;[I[Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;I)I
    .locals 14

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bY;

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/google/googlenav/bY;->b(Lcom/google/googlenav/bY;)[Lcom/google/googlenav/bX;

    move-result-object v6

    array-length v7, v6

    const/4 v2, 0x0

    move v13, v2

    move v2, v3

    move v3, v13

    :goto_1
    if-ge v3, v7, :cond_1

    aget-object v4, v6, v3

    :goto_2
    move-object/from16 v0, p2

    array-length v8, v0

    if-ge v2, v8, :cond_0

    move-object/from16 v0, p3

    array-length v8, v0

    if-ge v2, v8, :cond_0

    invoke-static {v4}, Lcom/google/googlenav/bX;->b(Lcom/google/googlenav/bX;)I

    move-result v8

    aget v9, p2, v2

    if-eq v8, v9, :cond_0

    aget-object v8, p4, p5

    new-instance v9, Lcom/google/googlenav/bO;

    const-string v10, "-"

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Lcom/google/googlenav/bO;-><init>(Ljava/lang/String;Z)V

    aput-object v9, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move-object/from16 v0, p3

    array-length v8, v0

    if-ge v2, v8, :cond_1

    move-object/from16 v0, p2

    array-length v8, v0

    if-lt v2, v8, :cond_2

    :cond_1
    :goto_3
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_2
    aget-object v8, p3, v2

    invoke-virtual {v8}, Lcom/google/googlenav/ui/view/android/aT;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v1}, Lcom/google/googlenav/bY;->c(Lcom/google/googlenav/bY;)I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/google/googlenav/bN;->d(I)Lcom/google/googlenav/bT;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/bT;->e()Ljava/lang/String;

    move-result-object v8

    aget-object v9, p3, v2

    invoke-virtual {v9, v8}, Lcom/google/googlenav/ui/view/android/aT;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    aget-object v1, p4, p5

    new-instance v3, Lcom/google/googlenav/bP;

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/google/googlenav/ui/bd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    invoke-direct {v3, v6, v4, v8}, Lcom/google/googlenav/bP;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    aput-object v3, v1, v2

    goto :goto_3

    :cond_3
    aget-object v9, p4, p5

    new-instance v10, Lcom/google/googlenav/bP;

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v11

    const/4 v12, 0x1

    invoke-static {v11, v12}, Lcom/google/googlenav/ui/bd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/String;

    move-result-object v11

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    invoke-direct {v10, v11, v4, v8}, Lcom/google/googlenav/bP;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    aput-object v10, v9, v2

    :goto_4
    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto/16 :goto_1

    :cond_4
    aget-object v8, p4, p5

    new-instance v9, Lcom/google/googlenav/bO;

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/bd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/String;

    move-result-object v10

    invoke-static {v4}, Lcom/google/googlenav/bX;->c(Lcom/google/googlenav/bX;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    invoke-direct {v9, v10, v4}, Lcom/google/googlenav/bO;-><init>(Ljava/lang/String;Z)V

    aput-object v9, v8, v2

    goto :goto_4

    :cond_5
    return p5
.end method

.method static synthetic a(Lcom/google/googlenav/bN;)Lcom/google/common/collect/ImmutableList;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bN;->f:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method private a([Lcom/google/googlenav/bT;)Ljava/util/Collection;
    .locals 4

    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bS;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/bS;

    invoke-direct {v0, p0, v3}, Lcom/google/googlenav/bS;-><init>(Lcom/google/googlenav/bN;Ljava/lang/String;)V

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    aget-object v3, p1, v1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bS;->a(Lcom/google/googlenav/bT;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/cm;Lcom/google/common/collect/cV;)Ljava/util/List;
    .locals 9

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->f()[Lcom/google/googlenav/cb;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_3

    aget-object v0, v3, v1

    new-instance v4, Lcom/google/googlenav/bS;

    invoke-virtual {v0}, Lcom/google/googlenav/cb;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/google/googlenav/bS;-><init>(Lcom/google/googlenav/bN;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/cb;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ce;

    invoke-virtual {v0}, Lcom/google/googlenav/ce;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/common/collect/cV;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4}, Lcom/google/googlenav/bS;->a(Lcom/google/googlenav/bS;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bT;->d(I)V

    invoke-virtual {v4, v0}, Lcom/google/googlenav/bS;->a(Lcom/google/googlenav/bT;)V

    goto :goto_1

    :cond_2
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-object v2
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/util/List;
    .locals 5

    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/bY;

    invoke-direct {v4, v3, p3}, Lcom/google/googlenav/bY;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private a(Lcom/google/googlenav/bS;Ljava/util/List;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Lcom/google/googlenav/bR;

    const/16 v0, 0x4ab

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p1}, Lcom/google/googlenav/bS;->a(Lcom/google/googlenav/bS;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/googlenav/bS;->b(Lcom/google/googlenav/bS;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->f()I

    move-result v0

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/bR;-><init>(Ljava/lang/String;I)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/google/googlenav/bS;->b(Lcom/google/googlenav/bS;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bT;

    new-instance v2, Lcom/google/googlenav/bR;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->f()I

    move-result v0

    invoke-direct {v2, v3, v0}, Lcom/google/googlenav/bR;-><init>(Ljava/lang/String;I)V

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->b()Lcom/google/googlenav/bV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bV;->c()I

    move-result v0

    return v0
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/bN;->a:B

    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bT;->b(I)V

    return-void
.end method

.method public a(II)V
    .locals 2

    iput p1, p0, Lcom/google/googlenav/bN;->e:I

    iget-object v0, p0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    iget v1, p0, Lcom/google/googlenav/bN;->e:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bT;->c(I)V

    return-void
.end method

.method public b(I)Lcom/google/googlenav/E;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bT;->a(I)Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/googlenav/bV;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->d()Lcom/google/googlenav/bV;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->c()I

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 0

    return p1
.end method

.method public d()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/bN;->a:B

    return v0
.end method

.method public d(I)Lcom/google/googlenav/bT;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    array-length v0, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/google/googlenav/E;
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/bN;->a:B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->a()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bT;->b()I

    move-result v0

    return v0
.end method

.method public g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bN;->f:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public h()Lbf/bG;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bN;->b:Lbf/bG;

    return-object v0
.end method

.method public i()Lcom/google/googlenav/bT;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    array-length v0, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bN;->c:[Lcom/google/googlenav/bT;

    iget v1, p0, Lcom/google/googlenav/bN;->e:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Ljava/util/List;
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/googlenav/bN;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    if-ne v1, v0, :cond_1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bT;->a(Lcom/google/googlenav/bT;)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, v2}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bS;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    aget-object v3, v3, v1

    invoke-direct {p0, v3, v2}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bS;Ljava/util/List;)V

    goto :goto_2

    :cond_2
    return-object v2
.end method

.method public k()Lcom/google/googlenav/bS;
    .locals 6

    iget-object v2, p0, Lcom/google/googlenav/bN;->d:[Lcom/google/googlenav/bS;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-static {v0}, Lcom/google/googlenav/bS;->b(Lcom/google/googlenav/bS;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->i()Lcom/google/googlenav/bT;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public l()[Lcom/google/googlenav/ui/view/android/aT;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/bN;->k()Lcom/google/googlenav/bS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bS;->b()[Lcom/google/googlenav/ui/view/android/aT;

    move-result-object v0

    return-object v0
.end method

.method public m()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bN;->b:Lbf/bG;

    invoke-virtual {v0}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->l()I

    move-result v0

    return v0
.end method
