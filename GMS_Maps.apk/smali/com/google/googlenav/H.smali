.class public Lcom/google/googlenav/H;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private final e:Lcom/google/googlenav/ui/s;

.field private f:I

.field private final g:Lcom/google/googlenav/A;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "=@latlon:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/google/googlenav/H;->b:I

    const-string v0, "..."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/google/googlenav/H;->c:I

    const-string v0, "&"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/google/googlenav/H;->d:I

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "application/vnd.google.gmm"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "application/vnd.google-earth.kml+xml"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "application/vnd.google-earth.kmz"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/H;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/H;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/H;->h:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/googlenav/H;->e:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/H;->g:Lcom/google/googlenav/A;

    return-void
.end method

.method static a(Ljava/lang/String;I)Lcom/google/googlenav/c;
    .locals 5

    const/4 v4, 0x3

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/googlenav/c;

    invoke-direct {v0, p1}, Lcom/google/googlenav/c;-><init>(I)V

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "@map"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/googlenav/c;

    invoke-direct {v0, v4}, Lcom/google/googlenav/c;-><init>(I)V

    goto :goto_0

    :cond_2
    const-string v0, "@gps"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/googlenav/c;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/googlenav/c;-><init>(I)V

    goto :goto_0

    :cond_3
    const-string v0, "@latlon:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "@latlon:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    new-instance v0, Lcom/google/googlenav/c;

    invoke-direct {v0, v4}, Lcom/google/googlenav/c;-><init>(I)V

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/c;

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lau/b;->b(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x6

    invoke-static {v1, v3}, Lau/b;->b(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/c;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/googlenav/c;

    invoke-direct {v0, v4}, Lcom/google/googlenav/c;-><init>(I)V

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/google/googlenav/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/c;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v3, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/gmm/x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, ".kml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".kmz"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".gmml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".kml?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, ".kmz?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, ".gmml?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, ".kml#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, ".kmz#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, ".gmml#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    const-string v2, "http://maps.google.com/maps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "output=kml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 2

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/H;->e(Ljava/lang/String;)I

    move-result v0

    :goto_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/googlenav/H;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    const-string v2, "=@latlon:"

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_0

    return-object p0

    :cond_0
    sget v2, Lcom/google/googlenav/H;->b:I

    add-int/2addr v2, v0

    const-string v0, "&"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "..."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-lez v3, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    sget v0, Lcom/google/googlenav/H;->c:I

    add-int/2addr v0, v2

    sget v2, Lcom/google/googlenav/H;->d:I

    add-int/2addr v0, v2

    goto :goto_0

    :cond_1
    move-object p0, v0

    goto :goto_1
.end method

.method static d(Ljava/lang/String;)Ljava/util/Map;
    .locals 7

    const/16 v4, 0x3f

    const/4 v1, 0x0

    const/4 v6, -0x1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-eq v2, v6, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x26

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v2

    :goto_1
    :try_start_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget-object v3, v2, v1

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-eq v4, v6, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/googlenav/common/io/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/H;->e:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->h(Ljava/lang/String;)V

    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/google/googlenav/H;->f:I

    if-nez v0, :cond_0

    iput p1, p0, Lcom/google/googlenav/H;->f:I

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "/gmm/x"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/google/googlenav/H;->a(Ljava/lang/String;ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZZ)V
    .locals 3

    const/16 v2, 0x34

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_2

    invoke-static {p1}, Lcom/google/googlenav/H;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/H;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "x"

    invoke-static {v2, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/H;->a(I)V

    const-string v1, "k"

    invoke-static {v2, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    iput-object p1, p0, Lcom/google/googlenav/H;->h:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/H;->j:Z

    iput-boolean p2, p0, Lcom/google/googlenav/H;->i:Z

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/H;->e:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/H;->a(I)V

    const-string v1, "u"

    invoke-static {v2, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method b(Ljava/lang/String;)I
    .locals 11

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v10, 0x1

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-static {p1}, Lcom/google/googlenav/H;->d(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    const-string v0, "view"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "MAPV"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/H;->e:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/s;->a(I)Z

    :cond_0
    :goto_0
    const-string v0, "span"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_7

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v6, :cond_3

    move v5, v6

    :goto_1
    const-string v0, "action"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "BUSI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "q"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v0, "a"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/google/googlenav/H;->a(Ljava/lang/String;I)Lcom/google/googlenav/c;

    move-result-object v4

    iget-object v0, p0, Lcom/google/googlenav/H;->g:Lcom/google/googlenav/A;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v8, v7

    move v9, v7

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/c;IIZZZ)V

    move v7, v10

    :cond_1
    :goto_2
    if-eqz v7, :cond_6

    const/4 v0, 0x5

    :goto_3
    return v0

    :cond_2
    const-string v1, "SATV"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/H;->e:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v10}, Lcom/google/googlenav/ui/s;->a(I)Z

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lau/b;->b(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    add-int/lit8 v1, v1, 0x1

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lau/b;->b(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    goto :goto_1

    :cond_4
    const-string v1, "LOCN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "a"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/google/googlenav/H;->a(Ljava/lang/String;I)Lcom/google/googlenav/c;

    move-result-object v1

    const-string v0, "title"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v0, "description"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v0, "phone"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/H;->g:Lcom/google/googlenav/A;

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/A;->a(Lcom/google/googlenav/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v7

    goto :goto_2

    :cond_5
    const-string v1, "ROUT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "start"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/googlenav/H;->a(Ljava/lang/String;I)Lcom/google/googlenav/c;

    move-result-object v1

    const-string v0, "end"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/googlenav/H;->a(Ljava/lang/String;I)Lcom/google/googlenav/c;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v2

    new-instance v3, Lax/j;

    invoke-virtual {v1}, Lcom/google/googlenav/c;->d()Lax/y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/c;->d()Lax/y;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Lax/j;-><init>(Lax/y;Lax/y;)V

    packed-switch v2, :pswitch_data_0

    new-instance v0, Lax/s;

    invoke-direct {v0, v3}, Lax/s;-><init>(Lax/k;)V

    :goto_4
    iget-object v1, p0, Lcom/google/googlenav/H;->g:Lcom/google/googlenav/A;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/A;->a(Lax/k;)V

    move v7, v10

    goto/16 :goto_2

    :pswitch_0
    new-instance v0, Lax/x;

    invoke-direct {v0, v3}, Lax/x;-><init>(Lax/k;)V

    goto :goto_4

    :pswitch_1
    new-instance v0, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    goto :goto_4

    :pswitch_2
    new-instance v0, Lax/i;

    invoke-direct {v0, v3}, Lax/i;-><init>(Lax/k;)V

    goto :goto_4

    :cond_6
    const/4 v0, 0x7

    goto/16 :goto_3

    :catch_0
    move-exception v0

    move v5, v6

    goto/16 :goto_1

    :catch_1
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v5, v6

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/H;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/H;->h:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/googlenav/H;->i:Z

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/H;->b(Ljava/lang/String;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/H;->h:Ljava/lang/String;

    goto :goto_0
.end method
