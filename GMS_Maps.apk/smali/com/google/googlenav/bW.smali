.class public Lcom/google/googlenav/bW;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lbf/bG;

.field private b:Lcom/google/googlenav/bN;

.field private c:Lcom/google/googlenav/bU;

.field private d:Z


# direct methods
.method public constructor <init>(Lbf/bG;Z)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/bW;->a:Lbf/bG;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/bU;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bW;->c:Lcom/google/googlenav/bU;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/bW;->a:Lbf/bG;

    invoke-virtual {v0}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->d()I

    move-result v6

    if-ge v0, v6, :cond_1

    invoke-virtual {v3, v0}, Lcom/google/googlenav/cm;->a(I)Lcom/google/googlenav/co;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/co;->f()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v2, v8, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->b()[Lcom/google/googlenav/bZ;

    move-result-object v0

    array-length v4, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v6, v0, v1

    invoke-virtual {v6}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->e()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bN;

    iget-object v2, p0, Lcom/google/googlenav/bW;->a:Lbf/bG;

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/bN;-><init>(Lbf/bG;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, Lcom/google/googlenav/bW;->b:Lcom/google/googlenav/bN;

    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized a_()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/bW;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x7f

    return v0
.end method

.method public declared-synchronized d_()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/googlenav/bW;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Lcom/google/googlenav/bN;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bW;->b:Lcom/google/googlenav/bN;

    return-object v0
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bW;->c:Lcom/google/googlenav/bU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bW;->c:Lcom/google/googlenav/bU;

    iget-object v1, p0, Lcom/google/googlenav/bW;->a:Lbf/bG;

    invoke-interface {v0, v1}, Lcom/google/googlenav/bU;->a(Lbf/bG;)V

    :cond_0
    return-void
.end method
