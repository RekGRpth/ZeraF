.class public Lcom/google/googlenav/cs;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Lcom/google/googlenav/ct;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ct;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cs;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    return-void
.end method

.method private static a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-virtual {p1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-virtual {p2, p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->Y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/googlenav/cs;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v2, p0, Lcom/google/googlenav/cs;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v5, v2, v1}, Lcom/google/googlenav/cs;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v2, p0, Lcom/google/googlenav/cs;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v4, v2, v1}, Lcom/google/googlenav/cs;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v2, p0, Lcom/google/googlenav/cs;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v6, v2, v1}, Lcom/google/googlenav/cs;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v2, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v2, v2, Lcom/google/googlenav/ct;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v2, :cond_0

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v3, v3, Lcom/google/googlenav/ct;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_4

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v2, v2, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v1, v1, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/googlenav/cs;->b:Lcom/google/googlenav/ct;

    iget-object v2, v2, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_5
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    const/4 v0, 0x1

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->Z:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x5e

    return v0
.end method
