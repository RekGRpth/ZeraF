.class public Lcom/google/googlenav/bT;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/bN;

.field private final b:[Lcom/google/googlenav/bV;

.field private final c:[Ljava/lang/String;

.field private d:I

.field private e:I

.field private final f:I

.field private g:I

.field private final h:Ljava/util/List;

.field private final i:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bN;[Lcom/google/googlenav/bV;Ljava/util/List;Ljava/util/List;I)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/bT;->a:Lcom/google/googlenav/bN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    iput-object p3, p0, Lcom/google/googlenav/bT;->h:Ljava/util/List;

    iput-object p4, p0, Lcom/google/googlenav/bT;->i:Ljava/util/List;

    iput p5, p0, Lcom/google/googlenav/bT;->f:I

    iget-object v0, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bT;->c:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/bT;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/bT;->c:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/bV;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bT;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bT;->g:I

    return v0
.end method

.method private e(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/google/googlenav/bV;->c()I

    move-result v0

    return v0
.end method

.method private k()Lcom/google/googlenav/bV;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/E;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/bT;->a:Lcom/google/googlenav/bN;

    invoke-static {v0}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    iget v2, p0, Lcom/google/googlenav/bT;->e:I

    aget-object v1, v1, v2

    iget v2, p0, Lcom/google/googlenav/bT;->d:I

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bV;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/E;

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/E;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/bT;->a:Lcom/google/googlenav/bN;

    invoke-static {v0}, Lcom/google/googlenav/bN;->a(Lcom/google/googlenav/bN;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    iget v2, p0, Lcom/google/googlenav/bT;->e:I

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bV;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/E;

    return-object v0
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    iget v1, p0, Lcom/google/googlenav/bT;->e:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/googlenav/bV;->a()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bT;->d:I

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bT;->d:I

    return v0
.end method

.method public c(I)V
    .locals 1

    iput p1, p0, Lcom/google/googlenav/bT;->e:I

    invoke-direct {p0, p1}, Lcom/google/googlenav/bT;->e(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput v0, p0, Lcom/google/googlenav/bT;->d:I

    return-void
.end method

.method public d()Lcom/google/googlenav/bV;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/bT;->b:[Lcom/google/googlenav/bV;

    iget v1, p0, Lcom/google/googlenav/bT;->e:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bT;->g:I

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/bT;->k()Lcom/google/googlenav/bV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bV;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/bT;->f:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/bT;->k()Lcom/google/googlenav/bV;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bV;->b(I)Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bT;->h:Ljava/util/List;

    return-object v0
.end method

.method public i()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bT;->i:Ljava/util/List;

    return-object v0
.end method

.method public j()[I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/bT;->k()Lcom/google/googlenav/bV;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bV;->a(Lcom/google/googlenav/bV;)[I

    move-result-object v0

    return-object v0
.end method
