.class public Lcom/google/googlenav/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/E;


# static fields
.field public static final a:Lcom/google/common/base/x;

.field private static final c:Lcom/google/googlenav/au;

.field private static final d:[Lcom/google/googlenav/ap;

.field private static final e:[Lcom/google/googlenav/aq;


# instance fields
.field private A:Z

.field private B:Lcom/google/googlenav/av;

.field private C:Z

.field private D:Z

.field private E:Lcom/google/googlenav/ax;

.field private F:Lcom/google/googlenav/ax;

.field private G:Lcom/google/googlenav/ay;

.field private H:[Lcom/google/googlenav/aw;

.field private I:Z

.field private J:Ljava/lang/Boolean;

.field private K:Ljava/lang/String;

.field private L:Lbl/o;

.field private final M:Ljava/lang/Object;

.field private N:Lcom/google/googlenav/cx;

.field private O:Lcom/google/googlenav/ar;

.field private P:Lcom/google/googlenav/ao;

.field private Q:Lcom/google/googlenav/cx;

.field protected b:Lcom/google/googlenav/cu;

.field private final f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Ljava/util/List;

.field private h:LaN/g;

.field private i:LaN/g;

.field private j:Ljava/lang/String;

.field private k:B

.field private l:I

.field private m:Z

.field private n:Ljava/util/Map;

.field private o:Lcom/google/googlenav/e;

.field private p:Z

.field private q:[Lcom/google/googlenav/ai;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Lcom/google/googlenav/ac;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:J

.field private x:Ljava/lang/ref/SoftReference;

.field private final y:Ljava/lang/Object;

.field private final z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v5, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/google/googlenav/au;

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/au;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/ai;->c:Lcom/google/googlenav/au;

    new-array v0, v1, [Lcom/google/googlenav/ap;

    sput-object v0, Lcom/google/googlenav/ai;->d:[Lcom/google/googlenav/ap;

    new-array v0, v1, [Lcom/google/googlenav/aq;

    sput-object v0, Lcom/google/googlenav/ai;->e:[Lcom/google/googlenav/aq;

    new-instance v0, Lcom/google/googlenav/aj;

    invoke-direct {v0}, Lcom/google/googlenav/aj;-><init>()V

    sput-object v0, Lcom/google/googlenav/ai;->a:Lcom/google/common/base/x;

    return-void
.end method

.method protected constructor <init>(LaN/g;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;ILjava/util/List;)V

    return-void
.end method

.method public constructor <init>(LaN/g;Ljava/lang/String;B)V
    .locals 7

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    return-void
.end method

.method protected constructor <init>(LaN/g;Ljava/lang/String;ILjava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->m:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->n:Ljava/util/Map;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->p:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    iput-object v1, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->y:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->A:Z

    sget-object v0, Lcom/google/googlenav/av;->c:Lcom/google/googlenav/av;

    iput-object v0, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->C:Z

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->D:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->M:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    iput-object v1, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    iput-object v1, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    invoke-static {p4}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->g:Ljava/util/List;

    iput-object p1, p0, Lcom/google/googlenav/ai;->h:LaN/g;

    iput-object p2, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-byte v2, p0, Lcom/google/googlenav/ai;->k:B

    iput p3, p0, Lcom/google/googlenav/ai;->l:I

    return-void
.end method

.method public constructor <init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 12

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->n:Ljava/util/Map;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->p:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->y:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->A:Z

    sget-object v0, Lcom/google/googlenav/av;->c:Lcom/google/googlenav/av;

    iput-object v0, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->C:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->D:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->M:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->p:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ai;->c(LaN/g;)V

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-direct {p0, p3}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-direct {p0, p4}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    invoke-direct {p0, p5}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-byte p6, p0, Lcom/google/googlenav/ai;->k:B

    invoke-static {p7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x12

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    invoke-static {p8}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, p8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-static {p9}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x18

    invoke-virtual {v0, v1, p9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    packed-switch p6, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/googlenav/ai;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x16

    if-eqz p6, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-wide/16 v0, -0x1

    cmp-long v0, p10, v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x77

    invoke-virtual {v0, v1, p10, p11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    :cond_3
    new-instance v0, Lcom/google/googlenav/ax;

    invoke-direct {v0}, Lcom/google/googlenav/ax;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->E:Lcom/google/googlenav/ax;

    new-instance v0, Lcom/google/googlenav/ax;

    invoke-direct {v0}, Lcom/google/googlenav/ax;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->F:Lcom/google/googlenav/ax;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->g:Ljava/util/List;

    return-void

    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x7b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    const/16 v5, 0xe

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->m:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->n:Ljava/util/Map;

    iput-boolean v3, p0, Lcom/google/googlenav/ai;->p:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    iput-object v1, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->y:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->A:Z

    sget-object v0, Lcom/google/googlenav/av;->c:Lcom/google/googlenav/av;

    iput-object v0, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->C:Z

    iput-boolean v2, p0, Lcom/google/googlenav/ai;->D:Z

    iput-object v1, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->M:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    iput-object v1, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    iput-object v1, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    iput-object p1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ai;->w:J

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->h:LaN/g;

    invoke-static {p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ai;->l:I

    const/16 v0, 0x90

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    const/16 v0, 0x8d

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-nez v0, :cond_1

    iput-byte v2, p0, Lcom/google/googlenav/ai;->k:B

    :goto_0
    iget-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    if-eq v0, v5, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ai;->i()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bF()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ai;->s:I

    new-instance v0, Lcom/google/googlenav/ax;

    invoke-direct {v0}, Lcom/google/googlenav/ax;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->E:Lcom/google/googlenav/ax;

    new-instance v0, Lcom/google/googlenav/ax;

    invoke-direct {v0}, Lcom/google/googlenav/ax;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ai;->F:Lcom/google/googlenav/ax;

    const/16 v0, 0xa9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->g:Ljava/util/List;

    return-void

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    packed-switch v0, :pswitch_data_0

    iput-byte v3, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_0
    const/16 v0, 0xc

    iput-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/google/googlenav/ai;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xf

    iput-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :cond_2
    iput-byte v3, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_3
    iput-byte v4, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xb

    iput-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    :pswitch_5
    iput-byte v5, p0, Lcom/google/googlenav/ai;->k:B

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ai;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/googlenav/ai;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->p:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    check-cast p0, Ljava/io/InputStream;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaN/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    :try_start_0
    invoke-interface {p0}, LaN/g;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p0, LaN/B;

    invoke-static {p0}, LaN/C;->b(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "PROTO"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    const/4 v2, 0x4

    :try_start_1
    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p0, LaN/B;

    invoke-static {p0}, LaN/C;->a(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p0, LaN/M;

    invoke-virtual {p0}, LaN/M;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p0, LaN/N;

    invoke-virtual {p0}, LaN/N;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fx;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x4

    invoke-direct {p0, p1}, Lcom/google/googlenav/ai;->b(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    return-object p1
.end method

.method private a(Ljava/util/Set;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aY;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static a(Lcom/google/googlenav/au;)[Lcom/google/googlenav/aw;
    .locals 12

    const/4 v0, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, LbK/aP;->b:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {v0, v10}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v5, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v10, v9}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v5

    if-eqz v7, :cond_2

    new-instance v8, Lcom/google/googlenav/aw;

    invoke-direct {v8, v7, v0, v6, v5}, Lcom/google/googlenav/aw;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/aw;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/aw;

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/g;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unsupported Geometry"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "PROTO"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x2

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v1, 0xe

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/M;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/M;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/N;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/N;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private b(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const/4 v5, 0x3

    const/16 v0, 0x20

    sget-object v1, LbK/C;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    if-ne v4, p1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v2, -0x1

    if-nez p0, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "cid="

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v2, :cond_2

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private c(I)Lcom/google/googlenav/ui/bs;
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x6

    invoke-direct {p0, p1}, Lcom/google/googlenav/ai;->b(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/bs;

    sget v2, Lcom/google/googlenav/ui/bi;->bz:I

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 6

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-nez p0, :cond_1

    :cond_0
    return-object v0

    :cond_1
    const/4 v1, 0x5

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected static g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    const/16 v0, 0x7b

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method private i()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ap()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-interface {v1, v0, v2, v3}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ai()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/google/googlenav/e;->b(Lam/f;)Lcom/google/googlenav/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/google/googlenav/e;->c(Lam/f;)Lcom/google/googlenav/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    goto :goto_0
.end method

.method private static i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    if-eqz p0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 4

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDouble(I)D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private j()[Lcom/google/googlenav/ap;
    .locals 11

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v3, 0x97

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    if-nez v5, :cond_0

    sget-object v0, Lcom/google/googlenav/ai;->d:[Lcom/google/googlenav/ap;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v5, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    new-array v3, v6, [Lcom/google/googlenav/ap;

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_1

    invoke-virtual {v5, v10, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v8, "http://www.google.com/streetview"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    new-instance v8, Lcom/google/googlenav/ap;

    const/4 v9, 0x0

    invoke-direct {v8, v7, v0, v9}, Lcom/google/googlenav/ap;-><init>(Ljava/lang/String;I[B)V

    aput-object v8, v3, v4

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move-object v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private k()Lcom/google/googlenav/ap;
    .locals 5

    invoke-direct {p0}, Lcom/google/googlenav/ai;->j()[Lcom/google/googlenav/ap;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_2

    aget-object v1, v2, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ap;->b()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/ap;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public static l(I)Ljava/lang/String;
    .locals 3

    if-gtz p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v0, v2, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x2ee

    if-lt p0, v2, :cond_1

    sget-char v2, Lcom/google/googlenav/ui/bi;->m:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit16 p0, p0, -0x3e8

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/16 v2, 0xfa

    if-lt p0, v2, :cond_2

    sget-char v2, Lcom/google/googlenav/ui/bi;->n:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    sget-char v2, Lcom/google/googlenav/ui/bi;->o:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".google."

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    const-string v0, "/gmf/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/16 v3, 0x5e

    const/16 v2, 0x5a

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    const/4 v0, 0x0

    const/16 v2, 0x5a

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0
.end method

.method private o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v0, 0x0

    const/16 v3, 0x5e

    invoke-direct {p0}, Lcom/google/googlenav/ai;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0
.end method

.method private p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    const/16 v1, 0x15

    sget-object v0, LbK/i;->f:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, LbK/i;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method static synthetic p(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/ai;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/google/googlenav/ar;
    .locals 11

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v8, 0x1

    const/16 v2, 0x2d

    sget-object v3, LbK/W;->a:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v4, 0x7

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_0

    invoke-virtual {v3, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    if-ne v6, v9, :cond_2

    invoke-virtual {v1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    :goto_2
    if-nez v1, :cond_4

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_2

    :cond_4
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, LbK/bj;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    :try_start_0
    invoke-virtual {v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-eqz v6, :cond_2

    invoke-static {v6, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    if-eqz v6, :cond_2

    new-instance v1, Lcom/google/googlenav/ar;

    invoke-direct {v1, v6}, Lcom/google/googlenav/ar;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v1}, Lcom/google/googlenav/ar;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v4}, Lcom/google/googlenav/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ar;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v7, "PLACE_PAGE-Placemark"

    invoke-static {v7, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method private r()Lcom/google/googlenav/ao;
    .locals 2

    const/16 v0, 0x50

    sget-object v1, LbK/X;->c:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ao;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ao;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method


# virtual methods
.method public A()[Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    return-object v0
.end method

.method public C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public D()LaN/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->h:LaN/g;

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/d;->a(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/d;->a(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public G()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x97

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public K()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aR()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aT()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x76

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Lcom/google/googlenav/aq;
    .locals 3

    const/4 v1, 0x0

    const/16 v0, 0xe

    sget-object v2, LbK/aa;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aq;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->a()[Lcom/google/googlenav/an;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->a()[Lcom/google/googlenav/an;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_1

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public N()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->M()Lcom/google/googlenav/aq;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/aq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public O()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x11

    sget-object v1, LbK/aP;->b:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public P()[Lcom/google/googlenav/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->H:[Lcom/google/googlenav/aw;

    if-nez v0, :cond_0

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->i(I)Lcom/google/googlenav/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/au;)[Lcom/google/googlenav/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->H:[Lcom/google/googlenav/aw;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->H:[Lcom/google/googlenav/aw;

    return-object v0
.end method

.method public Q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ai;->I:Z

    return v0
.end method

.method public R()Ljava/util/Vector;
    .locals 7

    const/4 v6, 0x1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/16 v1, 0x24

    sget-object v2, LbK/q;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/al;

    invoke-direct {v5, v4}, Lcom/google/googlenav/al;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public S()Ljava/util/Vector;
    .locals 6

    const/4 v5, 0x1

    const/16 v0, 0x33

    sget-object v1, LbK/ae;->b:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public T()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x76

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public U()Ljava/util/List;
    .locals 6

    const/4 v5, 0x1

    const/16 v0, 0x31

    sget-object v1, LbK/m;->b:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public V()J
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x77

    const-wide/16 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public W()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public X()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x94

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Y()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ai;->l:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()LaN/B;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LaN/g;->b()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ai;->i(I)Lcom/google/googlenav/au;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/ai;->k:B

    return-void
.end method

.method public a(Lcom/google/googlenav/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/au;)[Lcom/google/googlenav/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->H:[Lcom/google/googlenav/aw;

    iput-boolean p2, p0, Lcom/google/googlenav/ai;->I:Z

    return-void
.end method

.method public a(Lcom/google/googlenav/cx;)V
    .locals 6

    const/4 v4, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/googlenav/ai;->Q:Lcom/google/googlenav/cx;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    :cond_1
    iput v1, p0, Lcom/google/googlenav/ai;->s:I

    iget-object v0, p1, Lcom/google/googlenav/cx;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cy;

    iget-boolean v3, v0, Lcom/google/googlenav/cy;->e:Z

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/google/googlenav/cy;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz p1, :cond_0

    const/16 v1, 0xb

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    const/16 v3, 0xa9

    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ai;->g:Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    iget-object v2, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lo/D;)V
    .locals 1

    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->a(Ljava/util/List;)V

    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public a([Lcom/google/googlenav/ai;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    return-void
.end method

.method public aA()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public aB()Ljava/lang/String;
    .locals 3

    const/16 v2, 0x12

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const-string v0, ""

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aC()[Lcom/google/googlenav/ap;
    .locals 5

    const/16 v4, 0x64

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ai;->d:[Lcom/google/googlenav/ap;

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [Lcom/google/googlenav/ap;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ap;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ap;

    move-result-object v3

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public aD()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x47

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aE()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8c

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aF()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x48

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aG()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x49

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aH()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x73

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aI()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x74

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aJ()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aM()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aK()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ai;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aL()Lai/d;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aK()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ai;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v0, Lai/d;

    invoke-direct {v0, v1}, Lai/d;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public aM()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0xa7

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public aN()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public aO()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public aP()Ljava/lang/String;
    .locals 6

    const/16 v5, 0x5b

    const/16 v1, 0x5a

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v3, -0x1

    if-eq v0, v4, :cond_0

    const-string v4, " > "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public aQ()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x5a

    const/16 v2, 0x5c

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aR()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x5a

    const/16 v2, 0x5d

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aS()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x5a

    const/16 v2, 0x5f

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aT()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ai;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aU()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aV()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ai;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aW()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aX()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aX()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".youtube."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aY()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aZ()I
    .locals 2

    const/16 v0, 0x32

    sget-object v1, LbK/at;->f:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    goto :goto_0
.end method

.method public aa()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ad()Z
    .locals 2

    iget-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ae()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ai;->m:Z

    return-void
.end method

.method public af()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ai;->m:Z

    return v0
.end method

.method public ag()Z
    .locals 5

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    if-eq v1, v0, :cond_0

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    const/16 v2, 0xc

    if-eq v1, v2, :cond_0

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ah()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ai()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ai;->l:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aj()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ah()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ak()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->c()B

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->K()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public al()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bp()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public am()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/16 v4, 0x9d

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v2, v1, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public an()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v0

    goto :goto_0
.end method

.method public ao()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    invoke-virtual {v0}, Lcom/google/googlenav/e;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v0

    goto :goto_0
.end method

.method public ap()[B
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aC()[Lcom/google/googlenav/ap;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ap;->b()I

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ap;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    aget-object v0, v1, v0

    invoke-virtual {v0}, Lcom/google/googlenav/ap;->c()[B

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public aq()Lcom/google/googlenav/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->E:Lcom/google/googlenav/ax;

    return-object v0
.end method

.method public ar()Lcom/google/googlenav/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->F:Lcom/google/googlenav/ax;

    return-object v0
.end method

.method public as()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->F:Lcom/google/googlenav/ax;

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ai;->F:Lcom/google/googlenav/ax;

    invoke-virtual {v1}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v1

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ay;

    iput-object v0, p0, Lcom/google/googlenav/ai;->G:Lcom/google/googlenav/ay;

    :cond_0
    return-void
.end method

.method public at()Lcom/google/googlenav/ay;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->G:Lcom/google/googlenav/ay;

    return-object v0
.end method

.method public au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x97

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public av()Lcom/google/googlenav/bZ;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/bZ;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aw()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ax()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/googlenav/ap;->a(Lcom/google/googlenav/ap;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ay()Lcom/google/googlenav/ap;
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v0, 0x23

    sget-object v1, LbK/I;->b:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/googlenav/ap;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/googlenav/ap;-><init>(Ljava/lang/String;I[B)V

    :goto_1
    return-object v0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/googlenav/ai;->J:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/google/googlenav/ai;->k()Lcom/google/googlenav/ap;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public az()Ljava/util/Map;
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/ai;->y:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->x:Ljava/lang/ref/SoftReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->x:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->g:Ljava/util/List;

    return-object v0
.end method

.method public b(LaN/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ai;->i:LaN/g;

    return-void
.end method

.method public bA()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x98

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public bB()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8b

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bC()Z
    .locals 5

    const/4 v0, 0x0

    iget-byte v1, p0, Lcom/google/googlenav/ai;->k:B

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x8d

    const/16 v3, 0x8f

    const/4 v4, -0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;III)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bD()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    const/16 v0, 0x27

    sget-object v1, LbK/i;->g:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ai;->K:Ljava/lang/String;

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public bE()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bF()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x99

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public bG()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bH()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x99

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    const/16 v2, 0x99

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bH()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fx;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public bJ()[Lcom/google/googlenav/ak;
    .locals 6

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v3, v2, [Lcom/google/googlenav/ak;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/ak;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ak;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public bK()[I
    .locals 8

    const/4 v7, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v0, 0x2

    new-array v4, v0, [I

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    aget v5, v4, v1

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v1

    goto :goto_1

    :pswitch_2
    const/4 v5, 0x1

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5

    goto :goto_1

    :cond_0
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bL()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ai;->s:I

    return v0
.end method

.method public bM()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bN()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    const/16 v2, 0x9b

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bO()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    const-string v2, "www."

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x4

    :cond_0
    const-string v2, "/"

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bP()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9f

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bQ()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    return-object v0
.end method

.method public bR()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    sget-object v1, Lcom/google/googlenav/av;->a:Lcom/google/googlenav/av;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bS()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->B:Lcom/google/googlenav/av;

    sget-object v1, Lcom/google/googlenav/av;->c:Lcom/google/googlenav/av;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bT()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ai;->C:Z

    return v0
.end method

.method public bU()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ai;->D:Z

    return v0
.end method

.method public bV()Ljava/lang/String;
    .locals 2

    const/16 v1, 0x8d

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x8e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bW()Ljava/util/Set;
    .locals 6

    const/4 v5, 0x1

    const/16 v0, 0x1c

    sget-object v1, LbK/O;->f:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/bm;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/google/googlenav/ai;->a(Ljava/util/Set;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bi()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v2, v0}, Lcom/google/googlenav/ai;->a(Ljava/util/Set;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bh()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v2, v0}, Lcom/google/googlenav/ai;->a(Ljava/util/Set;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_2

    :cond_3
    return-object v2
.end method

.method public bX()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->cd()Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bY()Lcom/google/googlenav/ui/bs;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->c(I)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    return-object v0
.end method

.method public bZ()Lcom/google/googlenav/ui/bs;
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->c(I)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    return-object v0
.end method

.method public ba()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bb()I
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0xd

    invoke-static {v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public bc()Lcom/google/googlenav/ac;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    if-nez v0, :cond_0

    const/16 v0, 0xf

    sget-object v1, LbK/az;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/googlenav/ac;

    iget-wide v2, p0, Lcom/google/googlenav/ai;->w:J

    invoke-direct {v1, v0, v2, v3}, Lcom/google/googlenav/ac;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    iput-object v1, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->t:Lcom/google/googlenav/ac;

    return-object v0
.end method

.method public bd()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ai;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    return-object v0
.end method

.method public be()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ai;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    return-object v0

    :cond_1
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->v:Ljava/lang/String;

    goto :goto_0
.end method

.method public bf()Lcom/google/googlenav/cx;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    if-nez v0, :cond_0

    const/16 v0, 0x4e

    sget-object v1, LbK/bs;->c:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/googlenav/cx;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->N:Lcom/google/googlenav/cx;

    return-object v0
.end method

.method public bg()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x4f

    sget-object v1, LbK/K;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bh()Ljava/util/List;
    .locals 2

    const/16 v0, 0x56

    sget-object v1, LbK/x;->g:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bi()Ljava/util/List;
    .locals 2

    const/16 v0, 0x4a

    sget-object v1, LbK/x;->f:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bj()Lcom/google/googlenav/cu;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x4b

    sget-object v1, LbK/x;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/cu;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aC;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->l()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/aC;->a(Ljava/lang/String;)Lcom/google/googlenav/aF;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/cu;->a(Lcom/google/googlenav/aF;)V

    goto :goto_0
.end method

.method public bk()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ai;->b:Lcom/google/googlenav/cu;

    return-void
.end method

.method public bl()Lcom/google/googlenav/cx;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->Q:Lcom/google/googlenav/cx;

    return-object v0
.end method

.method public bm()Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->T()I

    move-result v0

    if-lez v0, :cond_1

    if-ne v0, v2, :cond_0

    const/16 v0, 0x481

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v1, 0x480

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bn()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bf()Lcom/google/googlenav/cx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bo()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bf()Lcom/google/googlenav/cx;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/cx;->a()Lcom/google/googlenav/cy;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public bp()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aQ()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bq()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public br()Lcom/google/googlenav/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    return-object v0
.end method

.method public bs()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bt()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bu()Z
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->b(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public bv()I
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/googlenav/ai;->b(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    return v0
.end method

.method public bw()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public bx()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/googlenav/ai;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public by()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x7e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public bz()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x95

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public c()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/ai;->k:B

    return v0
.end method

.method public c(LaN/g;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ai;->h:LaN/g;

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {p1}, Lcom/google/googlenav/ai;->a(LaN/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/16 v7, 0x92

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lez v1, :cond_0

    new-array v0, v1, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ai;->h:LaN/g;

    const/4 v5, 0x2

    invoke-static {v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    const/16 v4, 0x90

    invoke-static {v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ai;->p:Z

    return-void
.end method

.method public ca()Lcom/google/googlenav/ar;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v1, 0x1f

    sget-object v2, LbK/aj;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/googlenav/ar;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ar;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public cb()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x1f

    sget-object v1, LbK/aj;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public cc()Ljava/util/Vector;
    .locals 6

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x1f

    sget-object v1, LbK/aj;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ar;

    invoke-direct {v4, v3}, Lcom/google/googlenav/ar;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public cd()Ljava/util/Vector;
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/16 v1, 0x1f

    sget-object v2, LbK/aj;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/as;

    invoke-direct {v5, p0, v4}, Lcom/google/googlenav/as;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public ce()Lbl/o;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ai;->M:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    if-nez v0, :cond_1

    new-instance v0, Lbl/o;

    const/16 v2, 0x4c

    sget-object v3, LbK/bl;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {}, Lbl/j;->a()Lbl/j;

    move-result-object v3

    invoke-direct {v0, v2, p0, v3}, Lbl/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ai;Lbl/j;)V

    iput-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->L:Lbl/o;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cf()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xad

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cg()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ch()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb0

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public ci()Lcom/google/googlenav/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ai;->q()Lcom/google/googlenav/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    iget-object v0, p0, Lcom/google/googlenav/ai;->O:Lcom/google/googlenav/ar;

    goto :goto_0
.end method

.method public cj()Lcom/google/googlenav/ao;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ai;->r()Lcom/google/googlenav/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    iget-object v0, p0, Lcom/google/googlenav/ai;->P:Lcom/google/googlenav/ao;

    goto :goto_0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d(Z)Ljava/lang/String;
    .locals 4

    const/16 v1, 0x76

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bm()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9e

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->az()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/p;->a(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ai;->c:Lcom/google/googlenav/au;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x90

    iget-object v2, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1, p1}, Lcom/google/googlenav/d;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x46

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ai;->C:Z

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ai;->p:Z

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-direct {p0, p1}, Lcom/google/googlenav/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    return-void
.end method

.method public f(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ai;->D:Z

    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v2, "proto"

    iget-object v4, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed()I

    move-result v4

    invoke-direct {v0, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v2, "image"

    iget-object v4, p0, Lcom/google/googlenav/ai;->o:Lcom/google/googlenav/e;

    invoke-virtual {v4}, Lcom/google/googlenav/e;->g()I

    move-result v4

    invoke-direct {v0, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    if-eqz v0, :cond_3

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v4, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v4

    iget v4, v4, Lcom/google/googlenav/common/util/l;->b:I

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v4, "rbl results"

    invoke-direct {v0, v4, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->az()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/au;

    sget-object v5, Lcom/google/googlenav/ai;->c:Lcom/google/googlenav/au;

    if-eq v0, v5, :cond_8

    invoke-virtual {v0}, Lcom/google/googlenav/au;->a()I

    move-result v0

    add-int/2addr v0, v2

    :goto_2
    move v2, v0

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v4, "place page stories - soft ref"

    invoke-direct {v0, v4, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ai;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/ai;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v1

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "Placemark"

    invoke-direct {v1, v2, v0, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v1

    :cond_8
    move v0, v2

    goto :goto_2
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ai;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public g(I)[Lcom/google/googlenav/aq;
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v1, 0x1

    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    :cond_0
    const-string v0, "getJustifications"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/googlenav/ai;->e:[Lcom/google/googlenav/aq;

    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0xe

    sget-object v4, LbK/aa;->d:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v4}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    if-nez v4, :cond_2

    sget-object v0, Lcom/google/googlenav/ai;->e:[Lcom/google/googlenav/aq;

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/google/googlenav/aq;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    if-nez v5, :cond_4

    sget-object v0, Lcom/google/googlenav/ai;->e:[Lcom/google/googlenav/aq;

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/googlenav/aq;->b(I)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2
    new-array v3, v0, [Lcom/google/googlenav/aq;

    move v4, v2

    :goto_3
    add-int v6, v2, v0

    if-ge v4, v6, :cond_6

    sub-int v6, v4, v2

    invoke-virtual {v5, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lcom/google/googlenav/aq;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/aq;

    move-result-object v7

    aput-object v7, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v0, -0x3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v3

    goto :goto_2

    :cond_6
    move-object v0, v3

    goto :goto_0
.end method

.method public h(I)Lcom/google/googlenav/ap;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/googlenav/ap;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/ap;-><init>(Ljava/lang/String;I[B)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x99

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bF()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ai;->n(I)V

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x12

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i(I)Lcom/google/googlenav/au;
    .locals 10

    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    const/16 v8, 0x9e

    iget-object v2, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    invoke-static {p1}, Lcom/google/googlenav/common/util/p;->a(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ai;->y:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v2, p0, Lcom/google/googlenav/ai;->x:Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ai;->x:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    move-object v2, v0

    :goto_1
    if-nez v2, :cond_3

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_1

    iget-object v6, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v7, 0x9e

    invoke-virtual {v6, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/common/util/p;->a(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/googlenav/ai;->c:Lcom/google/googlenav/au;

    invoke-virtual {v2, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/googlenav/ai;->x:Ljava/lang/ref/SoftReference;

    move-object v2, v0

    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :goto_3
    if-ge v0, v3, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    if-ne v5, p1, :cond_4

    invoke-static {v1}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/au;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/au;

    sget-object v6, Lcom/google/googlenav/ai;->c:Lcom/google/googlenav/au;

    if-eq v0, v6, :cond_2

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/au;

    goto :goto_0

    :cond_6
    move-object v2, v0

    goto :goto_1
.end method

.method public i(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ai;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public j(I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/view/android/bi;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ai;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    const/4 v2, 0x2

    const/16 v0, 0x32

    sget-object v1, LbK/at;->f:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-ltz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lt p1, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0
.end method

.method public k(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ai;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public m(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x99

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1}, Lcom/google/common/base/aa;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x9

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public n(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ai;->s:I

    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-void
.end method

.method public o(I)V
    .locals 2

    invoke-static {p1}, Lcom/google/googlenav/common/util/p;->a(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ai;->z:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public o(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9f

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    return v0
.end method

.method public x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ai;->i:LaN/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ai;->i:LaN/g;

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(LaN/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ai;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ai;->q:[Lcom/google/googlenav/ai;

    array-length v0, v0

    goto :goto_0
.end method
