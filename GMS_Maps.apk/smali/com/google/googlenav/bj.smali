.class Lcom/google/googlenav/bj;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Lcom/google/googlenav/bi;

.field private i:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Law/a;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/bj;->b:J

    iput v2, p0, Lcom/google/googlenav/bj;->d:I

    iput v2, p0, Lcom/google/googlenav/bj;->g:I

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bB()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/bj;->b:J

    iput p2, p0, Lcom/google/googlenav/bj;->a:I

    iput-object p3, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    return-void
.end method

.method public static a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .locals 2

    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1, p2}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    iput p1, v0, Lcom/google/googlenav/bj;->g:I

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .locals 2

    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .locals 2

    const/4 v1, 0x1

    new-instance v0, Lcom/google/googlenav/bj;

    invoke-direct {v0, p0, v1, p2}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    iput p1, v0, Lcom/google/googlenav/bj;->d:I

    if-ne p1, v1, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .locals 2

    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    return-object v0
.end method

.method public static c(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .locals 2

    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, -0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gf;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/googlenav/bj;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v1, p0, Lcom/google/googlenav/bj;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/googlenav/bj;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget v1, p0, Lcom/google/googlenav/bj;->d:I

    if-eq v1, v5, :cond_2

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/googlenav/bj;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/bj;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/googlenav/bj;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    iget v1, p0, Lcom/google/googlenav/bj;->g:I

    if-eq v1, v5, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/googlenav/bj;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gf;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    packed-switch v1, :pswitch_data_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/bj;->i:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x4e

    return v0
.end method

.method public d_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/bj;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    invoke-interface {v0}, Lcom/google/googlenav/bi;->a()V

    :cond_0
    return-void
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
