.class public Lcom/google/googlenav/settings/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/settings/B;

.field private final b:Lcom/google/googlenav/friend/j;

.field private final c:Lcom/google/googlenav/settings/C;

.field private final d:Landroid/os/Handler;

.field private e:Lcom/google/googlenav/friend/reporting/s;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/j;Landroid/os/Handler;Lcom/google/googlenav/friend/reporting/s;Lcom/google/googlenav/settings/B;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/settings/s;->b:Lcom/google/googlenav/friend/j;

    iput-object p4, p0, Lcom/google/googlenav/settings/s;->a:Lcom/google/googlenav/settings/B;

    new-instance v0, Lcom/google/googlenav/settings/C;

    invoke-direct {v0}, Lcom/google/googlenav/settings/C;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/settings/s;->c:Lcom/google/googlenav/settings/C;

    iput-object p2, p0, Lcom/google/googlenav/settings/s;->d:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/googlenav/settings/s;->e:Lcom/google/googlenav/friend/reporting/s;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/s;)Lcom/google/googlenav/settings/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->a:Lcom/google/googlenav/settings/B;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/settings/s;)Lcom/google/googlenav/friend/reporting/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->e:Lcom/google/googlenav/friend/reporting/s;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->b:Lcom/google/googlenav/friend/j;

    new-instance v1, Lcom/google/googlenav/settings/w;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/w;-><init>(Lcom/google/googlenav/settings/s;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/j;->b(Lcom/google/googlenav/friend/k;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/settings/s;)Lcom/google/googlenav/settings/C;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->c:Lcom/google/googlenav/settings/C;

    return-object v0
.end method

.method private c()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/settings/y;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/settings/y;-><init>(Lcom/google/googlenav/settings/s;Las/c;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/settings/s;)Lcom/google/googlenav/friend/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->b:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->c:Lcom/google/googlenav/settings/C;

    invoke-virtual {v0}, Lcom/google/googlenav/settings/C;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->a:Lcom/google/googlenav/settings/B;

    iget-object v1, p0, Lcom/google/googlenav/settings/s;->c:Lcom/google/googlenav/settings/C;

    invoke-interface {v0, v1}, Lcom/google/googlenav/settings/B;->a(Lcom/google/googlenav/settings/C;)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/settings/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/settings/s;->d()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/settings/s;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/s;->c:Lcom/google/googlenav/settings/C;

    invoke-virtual {v0}, Lcom/google/googlenav/settings/C;->d()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/settings/s;->b()V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/settings/s;->c()V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/i;)V
    .locals 4

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/settings/t;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/i;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3, p1}, Lcom/google/googlenav/settings/t;-><init>(Lcom/google/googlenav/settings/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ct;Lcom/google/googlenav/friend/i;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Z)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/settings/u;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/settings/u;-><init>(Lcom/google/googlenav/settings/s;Z)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/friend/aM;

    invoke-direct {v3, v1, v0}, Lcom/google/googlenav/friend/aM;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/aN;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    return-void
.end method
