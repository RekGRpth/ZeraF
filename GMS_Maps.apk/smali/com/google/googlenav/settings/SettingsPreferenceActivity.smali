.class public Lcom/google/googlenav/settings/SettingsPreferenceActivity;
.super Lcom/google/googlenav/settings/GmmPreferenceActivity;
.source "SourceFile"


# instance fields
.field private b:Landroid/preference/PreferenceScreen;

.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/ListPreference;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/PreferenceCategory;

.field private h:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, LaM/j;->a()LaM/j;

    move-result-object v0

    invoke-virtual {v0}, LaM/j;->d()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-void
.end method

.method private c()V
    .locals 7

    const/4 v6, 0x1

    const/4 v3, 0x0

    const-string v0, "settings_preference"

    invoke-virtual {p0, v0, v3}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "settings_version"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v6, v1, :cond_2

    if-nez v1, :cond_1

    const-string v1, "cache_settings_preference"

    invoke-virtual {p0, v1, v3}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "cache_settings_prefetch_over_mobile_networks"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "map_tile_settings_prefetching_over_mobile_networks"

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/K;->b(Z)V

    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "settings_version"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    return-void
.end method

.method private d()Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.BUG_REPORT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v1, :cond_2

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onBackPressed()V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    const-string v1, "stars"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x1c -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/16 v2, 0x5a1

    invoke-super {p0, p1}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f060009

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->addPreferencesFromResource(I)V

    const/16 v0, 0x526

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "settings_preference"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->c()V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "display_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0xfe

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "sign_in"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->c:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->c:Landroid/preference/Preference;

    const/16 v1, 0x53b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "switch_account"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b()V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    new-instance v1, Lcom/google/googlenav/settings/R;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/R;-><init>(Lcom/google/googlenav/settings/SettingsPreferenceActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->e:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->e:Landroid/preference/Preference;

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    :goto_1
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "location_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->f:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->f:Landroid/preference/Preference;

    const/16 v1, 0x29d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->f:Landroid/preference/Preference;

    new-instance v1, Lcom/google/googlenav/settings/S;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/S;-><init>(Lcom/google/googlenav/settings/SettingsPreferenceActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "labs_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x20b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "info"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    const/16 v1, 0x1e4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    const-string v1, "info_whats_new"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x619

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    const-string v1, "info_legal"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x252

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    const-string v1, "info_feedback"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->h:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->h:Landroid/preference/Preference;

    const/16 v1, 0x12e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    const-string v1, "info_about"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_1
    const/16 v0, 0x63

    goto/16 :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/16 v5, 0x10

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "display_settings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/googlenav/settings/DisplaySettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=71"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v3, "sign_in"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->finish()V

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=23"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x1b2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->ao()Lcom/google/googlenav/ui/R;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    goto :goto_0

    :cond_1
    const-string v3, "switch_account"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=62"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "map_tile_settings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=69"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/settings/MapTileSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v3, "location_settings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=70"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/googlenav/friend/as;->k()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v3, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    const-string v3, "labs_settings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=58"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/settings/LabsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "info_whats_new"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=66"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/googlenav/aA;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "info_legal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=51"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/settings/LegalActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    const-string v3, "info_feedback"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=18"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->i()V

    goto/16 :goto_0

    :cond_9
    const-string v3, "info_about"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "a=s"

    aput-object v3, v2, v1

    const-string v1, "i=53"

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "s"

    invoke-static {v5, v2, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/settings/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onResume()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "sign_in"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_0
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aE()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "switch_account"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_1
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    :goto_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "location_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    :goto_3
    invoke-direct {p0}, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "info_feedback"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->h:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_4
    :goto_4
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->g:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/googlenav/settings/SettingsPreferenceActivity;->h:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4
.end method
