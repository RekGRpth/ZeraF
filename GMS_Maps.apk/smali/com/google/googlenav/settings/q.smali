.class Lcom/google/googlenav/settings/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

.field private b:Lcom/google/googlenav/friend/aQ;

.field private c:Lcom/google/googlenav/friend/ag;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Lcom/google/googlenav/friend/ag;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/settings/r;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/settings/r;-><init>(Lcom/google/googlenav/settings/q;Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    iput-object v0, p0, Lcom/google/googlenav/settings/q;->b:Lcom/google/googlenav/friend/aQ;

    iput-object p2, p0, Lcom/google/googlenav/settings/q;->c:Lcom/google/googlenav/friend/ag;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "automatic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->c:Lcom/google/googlenav/friend/ag;

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->b:Lcom/google/googlenav/friend/aQ;

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/friend/ag;->a(ZLcom/google/googlenav/friend/aQ;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "manual"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/googlenav/android/M;->g:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "start_activity_on_complete"

    const-class v2, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v0, "off"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->e(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/q;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/settings/q;->d(Z)V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->c:Lcom/google/googlenav/friend/ag;

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->b:Lcom/google/googlenav/friend/aQ;

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/friend/ag;->b(ZLcom/google/googlenav/friend/aQ;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {p1}, Lcom/google/googlenav/friend/as;->a(Z)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/settings/q;->c:Lcom/google/googlenav/friend/ag;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ag;->a()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/googlenav/android/M;->h:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/friend/j;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/friend/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/j;->k()Lcom/google/googlenav/friend/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/i;->a(Z)V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/s;->a(Lcom/google/googlenav/friend/i;)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/googlenav/android/M;->d:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "start_activity_on_complete"

    const-class v2, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Z)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->c:Lcom/google/googlenav/friend/ag;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/settings/q;->b:Lcom/google/googlenav/friend/aQ;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/googlenav/friend/ag;->a(ZZILcom/google/googlenav/friend/aQ;)V

    return-void
.end method

.method public e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/friend/j;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/friend/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/j;->k()Lcom/google/googlenav/friend/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/i;->b(Z)V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/google/googlenav/settings/q;->a:Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-static {v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/settings/s;->a(Lcom/google/googlenav/friend/i;)V

    goto :goto_0
.end method
