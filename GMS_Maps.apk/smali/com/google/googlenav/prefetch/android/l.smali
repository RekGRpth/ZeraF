.class public Lcom/google/googlenav/prefetch/android/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;


# static fields
.field private static c:[I

.field private static d:[I


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x9

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/prefetch/android/l;->c:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/prefetch/android/l;->d:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x4
        0x6
        0x8
        0xa
        0xb
        0xc
        0xd
        0xe
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x2
        0x2
        0x2
        0x3
        0x4
        0x4
        0x6
        0x8
    .end array-data
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/l;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/prefetch/android/l;->b:I

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    sget-object v3, Lcom/google/googlenav/prefetch/android/l;->d:[I

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/l;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    sget-object v5, Lcom/google/googlenav/prefetch/android/l;->c:[I

    invoke-static {v0, v3, v4, v1, v5}, Lcom/google/googlenav/prefetch/android/l;->a(Lo/aq;[ILjava/util/Queue;Ljava/util/Set;[I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(II)I
    .locals 1

    if-ltz p0, :cond_0

    rem-int v0, p0, p1

    :goto_0
    return v0

    :cond_0
    rem-int v0, p0, p1

    add-int/2addr v0, p1

    goto :goto_0
.end method

.method private static a(Lo/aq;[ILjava/util/Queue;Ljava/util/Set;[I)V
    .locals 16

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->c()Lcom/google/googlenav/clientparam/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/clientparam/e;->a()I

    move-result v7

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    move-object/from16 v0, p4

    array-length v3, v0

    if-ge v1, v3, :cond_3

    aget v8, p4, v1

    aget v6, p1, v1

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lo/aq;->b()I

    move-result v3

    sub-int v9, v8, v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->c()I

    move-result v3

    invoke-static {v3, v9}, Lcom/google/googlenav/prefetch/android/l;->b(II)I

    move-result v3

    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lo/aq;->c()I

    move-result v5

    invoke-static {v5, v9}, Lcom/google/googlenav/prefetch/android/l;->b(II)I

    move-result v5

    add-int/2addr v5, v6

    add-int/lit8 v10, v5, -0x1

    invoke-virtual/range {p0 .. p0}, Lo/aq;->d()I

    move-result v5

    invoke-static {v5, v9}, Lcom/google/googlenav/prefetch/android/l;->b(II)I

    move-result v5

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {p0 .. p0}, Lo/aq;->d()I

    move-result v11

    invoke-static {v11, v9}, Lcom/google/googlenav/prefetch/android/l;->b(II)I

    move-result v9

    add-int/2addr v6, v9

    add-int/lit8 v9, v6, -0x1

    const/4 v6, 0x1

    shl-int v11, v6, v8

    move v6, v3

    move v3, v4

    :goto_1
    if-gt v6, v10, :cond_2

    move v4, v5

    move v15, v3

    move v3, v2

    move v2, v15

    :goto_2
    if-gt v4, v9, :cond_1

    invoke-static {v6, v11}, Lcom/google/googlenav/prefetch/android/l;->a(II)I

    move-result v12

    invoke-static {v4, v11}, Lcom/google/googlenav/prefetch/android/l;->a(II)I

    move-result v13

    new-instance v14, Lo/aq;

    invoke-direct {v14, v8, v12, v13}, Lo/aq;-><init>(III)V

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->size()I

    move-result v12

    if-ge v12, v7, :cond_0

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v15, v2

    move v2, v3

    move v3, v15

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private static b(II)I
    .locals 6

    int-to-double v0, p0

    const-wide/high16 v2, 0x4000000000000000L

    int-to-double v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a(Lo/aq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Lo/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/l;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    return-object v0
.end method

.method public d()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "should not be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/l;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/prefetch/android/l;->b:I

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
