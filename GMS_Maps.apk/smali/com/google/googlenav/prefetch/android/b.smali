.class public Lcom/google/googlenav/prefetch/android/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/googlenav/prefetch/android/d;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/googlenav/prefetch/android/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->a:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/googlenav/prefetch/android/b;->b:Ljava/util/List;

    iput-object p2, p0, Lcom/google/googlenav/prefetch/android/b;->c:Lcom/google/googlenav/prefetch/android/d;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/prefetch/android/b;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/prefetch/android/b;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/prefetch/android/b;)Lcom/google/googlenav/prefetch/android/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->c:Lcom/google/googlenav/prefetch/android/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/util/t;->b()V

    new-instance v0, Lcom/google/googlenav/prefetch/android/c;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/prefetch/android/c;-><init>(Lcom/google/googlenav/prefetch/android/b;Las/c;)V

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/c;->g()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/16 v5, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Reverse Geocode Tiles"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/b;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "coords: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/b;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/prefetch/android/e;

    if-eqz v0, :cond_0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "location: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
