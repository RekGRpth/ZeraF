.class public Lcom/google/googlenav/prefetch/android/PrefetcherService;
.super Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
.source "SourceFile"


# static fields
.field private static final d:Lcom/google/googlenav/prefetch/android/a;


# instance fields
.field private e:Lcom/google/googlenav/ui/android/al;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/prefetch/android/a;

    const-class v1, Lcom/google/googlenav/prefetch/android/PrefetcherService;

    invoke-direct {v0, v1}, Lcom/google/googlenav/prefetch/android/a;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/prefetch/android/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;JJ)V
    .locals 6

    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/prefetch/android/a;->a(Landroid/content/Context;JJ)V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/prefetch/android/a;->b(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->a(Landroid/content/Context;)V

    return-void
.end method

.method protected b()V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->b(Landroid/content/Context;)V

    return-void
.end method

.method protected c()V
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, LaE/f;->a()LaE/f;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, LaF/b;->a:LaF/b;

    invoke-virtual {v0}, LaF/b;->d()I

    move-result v0

    invoke-static {v0}, LaE/f;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Z)V

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/al;

    invoke-virtual {v0, v1}, LaT/a;->b(LaT/m;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/c;->f()V

    return-void
.end method

.method protected e()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e()V

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    invoke-static {v0}, LaT/a;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/android/al;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/al;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/al;

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/al;

    invoke-virtual {v0, v1}, LaT/a;->a(LaT/m;)V

    :cond_1
    return-void
.end method
