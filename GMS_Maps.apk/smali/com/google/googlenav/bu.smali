.class public Lcom/google/googlenav/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field public final i:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field public j:Z

.field public final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/googlenav/bu;->a:Z

    iput-object p2, p0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/googlenav/bu;->j:Z

    iput-object p5, p0, Lcom/google/googlenav/bu;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/googlenav/bu;->e:Ljava/lang/String;

    iput p7, p0, Lcom/google/googlenav/bu;->f:I

    iput p8, p0, Lcom/google/googlenav/bu;->g:I

    iput-object p9, p0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/googlenav/bu;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p11, p0, Lcom/google/googlenav/bu;->i:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bu;
    .locals 13

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    new-instance v0, Lcom/google/googlenav/friend/U;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v0, v8}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v6, :cond_2

    if-eqz v7, :cond_0

    :cond_2
    invoke-static {v0, v5}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {v0, v10}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    if-eqz v6, :cond_4

    invoke-virtual {v6, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/bv;

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/google/googlenav/bv;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v9

    move-object v11, v4

    move-object v10, v4

    move-object v6, v4

    move v12, v5

    move-object v5, v4

    move v4, v12

    :goto_2
    if-eqz v0, :cond_5

    iget v7, v0, Lcom/google/googlenav/bv;->c:I

    :goto_3
    if-eqz v0, :cond_3

    iget v8, v0, Lcom/google/googlenav/bv;->a:I

    :cond_3
    new-instance v0, Lcom/google/googlenav/bu;

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/bu;-><init>(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move-object v4, v0

    goto :goto_0

    :cond_4
    if-eqz v7, :cond_7

    invoke-virtual {v7, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v5, Lcom/google/googlenav/bv;

    const/4 v0, 0x4

    invoke-virtual {v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/bv;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v5}, Lcom/google/googlenav/bv;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v7, v10}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x5

    invoke-virtual {v7, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    const/4 v9, 0x6

    invoke-virtual {v7, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v11

    move-object v9, v4

    move v4, v8

    move-object v12, v5

    move-object v5, v0

    move-object v0, v12

    goto :goto_2

    :cond_5
    move v7, v8

    goto :goto_3

    :cond_6
    move-object v0, v4

    goto :goto_4

    :cond_7
    move-object v0, v4

    move-object v9, v4

    move-object v11, v4

    move-object v10, v4

    move-object v6, v4

    move-object v5, v4

    move v4, v8

    goto :goto_2

    :cond_8
    move-object v3, v4

    move-object v2, v4

    goto :goto_1
.end method

.method public static a(Lcom/google/googlenav/bu;)V
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/bu;->a(Lcom/google/googlenav/bu;I)V

    return-void
.end method

.method protected static a(Lcom/google/googlenav/bu;I)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    invoke-static {}, Lcom/google/googlenav/bu;->d()V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Laq/b;

    invoke-direct {v3}, Laq/b;-><init>()V

    :try_start_0
    invoke-virtual {v3, p1}, Laq/b;->writeInt(I)V

    iget-boolean v2, p0, Lcom/google/googlenav/bu;->a:Z

    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Laq/b;->writeUTF(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v2, v0

    :goto_2
    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    invoke-virtual {v3, v2}, Laq/b;->writeUTF(Ljava/lang/String;)V

    :cond_2
    iget-boolean v2, p0, Lcom/google/googlenav/bu;->j:Z

    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v2, v0

    :goto_3
    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/bu;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Laq/b;->writeUTF(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/google/googlenav/bu;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v2, v0

    :goto_4
    invoke-virtual {v3, v2}, Laq/b;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/googlenav/bu;->e:Ljava/lang/String;

    invoke-virtual {v3, v2}, Laq/b;->writeUTF(Ljava/lang/String;)V

    :cond_4
    iget v2, p0, Lcom/google/googlenav/bu;->f:I

    invoke-virtual {v3, v2}, Laq/b;->writeInt(I)V

    iget v2, p0, Lcom/google/googlenav/bu;->g:I

    invoke-virtual {v3, v2}, Laq/b;->writeInt(I)V

    iget-object v2, p0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    if-eqz v2, :cond_a

    :goto_5
    invoke-virtual {v3, v0}, Laq/b;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    invoke-virtual {v3, v0}, Laq/b;->writeUTF(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SPICY_BOWL_FEATURE_PROVIDER_OPT_IN_SETTING"

    invoke-virtual {v3}, Laq/b;->a()[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    goto/16 :goto_0

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    move v2, v1

    goto :goto_3

    :cond_9
    move v2, v1

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_5

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public static c()Lcom/google/googlenav/bu;
    .locals 13

    const/4 v12, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SPICY_BOWL_FEATURE_PROVIDER_OPT_IN_SETTING"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v12

    :goto_0
    return-object v0

    :cond_0
    new-instance v9, Laq/a;

    invoke-direct {v9, v0}, Laq/a;-><init>([B)V

    :try_start_0
    invoke-virtual {v9}, Laq/a;->readInt()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/bu;->d()V

    move-object v0, v12

    goto :goto_0

    :cond_1
    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v1

    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v9}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v9}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v4

    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v9}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v9}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v6

    :goto_4
    invoke-virtual {v9}, Laq/a;->readInt()I

    move-result v7

    invoke-virtual {v9}, Laq/a;->readInt()I

    move-result v8

    invoke-virtual {v9}, Laq/a;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v9}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v9

    :goto_5
    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v0, Lcom/google/googlenav/bu;

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/bu;-><init>(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v12

    goto :goto_0

    :cond_2
    move-object v9, v12

    goto :goto_5

    :cond_3
    move-object v6, v12

    goto :goto_4

    :cond_4
    move-object v5, v12

    goto :goto_3

    :cond_5
    move-object v3, v12

    goto :goto_2

    :cond_6
    move-object v2, v12

    goto :goto_1
.end method

.method public static d()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SPICY_BOWL_FEATURE_PROVIDER_OPT_IN_SETTING"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bu;->j:Z

    return-void
.end method

.method public a()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/bu;->j:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/bu;->g:I

    iget v1, p0, Lcom/google/googlenav/bu;->f:I

    add-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/bu;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gplus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/bu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSpicy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/bu;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " needsMigration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/bu;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
