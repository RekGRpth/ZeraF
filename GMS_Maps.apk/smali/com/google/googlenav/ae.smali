.class public Lcom/google/googlenav/ae;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/googlenav/af;

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;ILjava/lang/String;Lcom/google/googlenav/af;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    iput p2, p0, Lcom/google/googlenav/ae;->b:I

    iput-object p3, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, 0x1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x6

    iget v1, p0, Lcom/google/googlenav/ae;->b:I

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, Lcom/google/googlenav/ae;->b:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    invoke-virtual {v2, v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ay;

    invoke-virtual {v0}, Lcom/google/googlenav/ay;->d()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/googlenav/ae;->b:I

    if-ne v1, v5, :cond_3

    const/4 v1, -0x1

    :goto_2
    const/4 v4, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ay;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ay;->a(Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_3
    const v1, 0x124f80

    goto :goto_2

    :cond_4
    const/4 v0, 0x5

    invoke-virtual {v2, v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_0

    iput-boolean v2, p0, Lcom/google/googlenav/ae;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ae;->g:I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ae;->g:I

    iget v0, p0, Lcom/google/googlenav/ae;->g:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/googlenav/ae;->e:Z

    iget v0, p0, Lcom/google/googlenav/ae;->g:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/google/googlenav/ae;->f:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x75

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 8

    const/4 v7, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ae;->e:Z

    if-eqz v0, :cond_1

    const-string v0, "s"

    :goto_0
    const/16 v2, 0x65

    iget v1, p0, Lcom/google/googlenav/ae;->b:I

    if-nez v1, :cond_2

    const-string v1, "p"

    :goto_1
    new-array v3, v7, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "r="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ae;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    invoke-interface {v0, v7}, Lcom/google/googlenav/af;->b(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    const-string v0, "e"

    goto :goto_0

    :cond_2
    const-string v1, "n"

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    iget-boolean v1, p0, Lcom/google/googlenav/ae;->e:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/af;->a(Z)V

    goto :goto_2
.end method

.method public u_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/af;->b(Z)V

    :cond_0
    return-void
.end method
