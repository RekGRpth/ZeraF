.class public Lcom/google/googlenav/android/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/clientparam/i;


# static fields
.field private static final s:Ljava/util/List;


# instance fields
.field private final a:Lcom/google/googlenav/android/AndroidGmmApplication;

.field private final b:LaN/p;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/ui/s;

.field private final e:Law/h;

.field private final f:Lcom/google/googlenav/ui/android/L;

.field private final g:Lcom/google/googlenav/android/aa;

.field private final h:Lcom/google/googlenav/layer/r;

.field private final i:Lcom/google/googlenav/android/d;

.field private final j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

.field private final k:Lcom/google/googlenav/prefetch/android/g;

.field private final l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

.field private m:Landroid/view/Menu;

.field private n:Lcom/google/android/maps/MapsActivity;

.field private o:Lcom/google/android/maps/A;

.field private p:Z

.field private final q:Laz/b;

.field private final r:[Laz/b;

.field private t:Ljava/util/Locale;

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/AndroidGmmApplication;Las/c;Law/h;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->m:Landroid/view/Menu;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->o:Lcom/google/android/maps/A;

    new-instance v0, Laz/l;

    const v1, 0x13d655

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laz/l;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->q:Laz/b;

    const/4 v0, 0x4

    new-array v0, v0, [Laz/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/android/i;->q:Laz/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Laz/i;->a:Laz/i;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Laz/g;->a:Laz/g;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Laz/h;->a:Laz/h;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/googlenav/android/i;->r:[Laz/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    const-string v0, "AndroidState.AndroidState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/googlenav/android/i;->s()V

    iput-object p1, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v8

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/android/i;->q()V

    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    :cond_0
    invoke-static {p0}, Lcom/google/googlenav/clientparam/f;->a(Lcom/google/googlenav/clientparam/i;)V

    new-instance v0, Lcom/google/googlenav/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/d;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    iput-object p3, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/j;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/googlenav/android/j;-><init>(Lcom/google/googlenav/android/i;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    const/16 v0, 0xb

    invoke-static {v0}, Lbm/m;->a(I)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/i;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/google/googlenav/ui/android/L;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/L;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    new-instance v0, Lcom/google/googlenav/android/aa;

    invoke-direct {v0}, Lcom/google/googlenav/android/aa;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LaO/a;

    invoke-virtual {p1}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x100000

    const/high16 v3, 0x80000

    const/high16 v4, 0x100000

    invoke-virtual {v6}, Lcom/google/googlenav/K;->g()LaN/B;

    move-result-object v5

    invoke-virtual {v6}, Lcom/google/googlenav/K;->h()LaN/Y;

    move-result-object v6

    const/16 v7, 0x190

    invoke-direct/range {v0 .. v7}, LaO/a;-><init>(Landroid/content/res/Resources;IIILaN/B;LaN/Y;I)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v0, v1}, LaN/p;->a(Lat/d;)V

    new-instance v0, Lcom/google/googlenav/layer/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/layer/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->h:Lcom/google/googlenav/layer/r;

    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/n;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/n;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    new-instance v0, Lcom/google/googlenav/prefetch/android/g;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-direct {v0, v1}, Lcom/google/googlenav/prefetch/android/g;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    const-class v1, Lcom/google/googlenav/prefetch/android/PrefetcherService;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/o;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/o;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->r()V

    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-static {p1, v0}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    new-instance v0, LaV/a;

    new-instance v1, Lcom/google/googlenav/android/p;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/p;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v8}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaV/a;-><init>(LaV/e;Lcom/google/googlenav/common/a;)V

    invoke-static {v0}, LaV/h;->a(LaV/h;)V

    new-instance v1, Lcom/google/googlenav/android/w;

    invoke-direct {v1, p0, p2}, Lcom/google/googlenav/android/w;-><init>(Lcom/google/googlenav/android/i;Las/c;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v7, Lbg/i;

    invoke-direct {v7}, Lbg/i;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    check-cast v0, LaO/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v2, LaO/e;

    invoke-virtual {v0}, LaO/a;->w()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    iget-object v6, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v6}, LaN/p;->b()LaN/H;

    move-result-object v6

    iget-object v8, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-direct {v2, v3, v0, v6, v8}, LaO/e;-><init>(Lcom/google/android/maps/driveabout/vector/bk;LaO/a;LaN/H;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    :goto_2
    new-instance v0, Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/s;-><init>(Lcom/google/googlenav/ui/aC;LaN/p;LaN/u;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;LaH/m;Lbf/at;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    iget-object v0, p0, Lcom/google/googlenav/android/i;->r:[Laz/b;

    invoke-static {p3, v0}, Laz/c;->a(Law/h;[Laz/b;)V

    new-instance v0, Lcom/google/googlenav/android/y;

    invoke-direct {v0}, Lcom/google/googlenav/android/y;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/android/y;->a()V

    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/ui/s;)V

    const-string v0, "AndroidState.AndroidState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_1
    const/high16 v1, 0x100000

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_3
    mul-int/2addr v1, v0

    new-instance v0, LaN/p;

    const/4 v2, -0x1

    const/high16 v3, 0x100000

    invoke-virtual {v6}, Lcom/google/googlenav/K;->g()LaN/B;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/googlenav/K;->h()LaN/Y;

    move-result-object v5

    const/16 v6, 0x190

    invoke-direct/range {v0 .. v6}, LaN/p;-><init>(IIILaN/B;LaN/Y;I)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/s;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/r;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/r;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    goto/16 :goto_1

    :cond_4
    new-instance v7, Lbf/at;

    invoke-direct {v7}, Lbf/at;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/aC;->a(LaN/p;)Lcom/google/googlenav/ui/bF;

    move-result-object v4

    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/aC;->b(LaN/p;)Lcom/google/googlenav/ui/p;

    move-result-object v5

    const/4 v0, 0x1

    new-instance v2, LaN/h;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-direct {v2, v3, v4, v5, v0}, LaN/h;-><init>(LaN/p;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;Z)V

    iput-object v2, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    goto :goto_2
.end method

.method private a(Lcom/google/googlenav/ui/android/a;)Landroid/app/ProgressDialog;
    .locals 3

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d013e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/android/k;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/android/k;-><init>(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/a;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    const/4 v1, 0x1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcom/google/googlenav/android/i;->b(Landroid/content/Context;)Z

    move-result v3

    new-instance v4, LaJ/a;

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v4, v0}, LaJ/a;-><init>(Z)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_2

    new-instance v0, LaH/z;

    invoke-direct {v0, p1}, LaH/z;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->t()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, LaK/h;

    invoke-direct {v3, p1, v0}, LaK/h;-><init>(Landroid/content/Context;LaH/F;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, LaH/o;

    invoke-direct {v0, v1, v2}, LaH/o;-><init>(ZLjava/util/List;)V

    invoke-static {v0}, LaH/o;->a(LaH/o;)V

    new-instance v0, LaI/a;

    invoke-direct {v0}, LaI/a;-><init>()V

    invoke-static {v0}, LaI/b;->a(LaI/c;)V

    new-instance v0, LaL/a;

    invoke-direct {v0}, LaL/a;-><init>()V

    invoke-static {v0}, LaL/d;->a(LaL/e;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v3, LaH/a;

    invoke-direct {v3, p1, v0}, LaH/a;-><init>(Landroid/content/Context;LaH/F;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v0, "nlp_state"

    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/AndroidGmmApplication;)V
    .locals 8

    const-class v6, Lcom/google/googlenav/android/i;

    monitor-enter v6

    :try_start_0
    const-string v0, "AndroidState.setUpSharedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->al()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/K;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    const-string v3, "GMM"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    :cond_1
    invoke-static {p0}, Lcom/google/googlenav/android/F;->a(Landroid/app/Application;)V

    const-string v0, "AndroidState.setUpSharedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V
    .locals 8

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    new-instance v0, Lcom/google/googlenav/android/v;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/android/v;-><init>(Lcom/google/googlenav/android/i;Landroid/app/ProgressDialog;ZLcom/google/googlenav/android/x;Law/q;)V

    invoke-virtual {v7, v0, v6}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/s;)V
    .locals 0

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1}, Law/h;->y()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/google/googlenav/android/a;->e()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unavailable. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Check that the server is up and that you have a network connection."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_1
    const v1, 0x7f0d011e

    new-instance v2, Lcom/google/googlenav/android/l;

    invoke-direct {v2, p0}, Lcom/google/googlenav/android/l;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/google/googlenav/android/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/m;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wrong remote strings version, or no network connection, or the GMM Server is down.  GMM Server must be hosting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    :cond_4
    const v1, 0x7f0d0155

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/googlenav/android/x;Z)V
    .locals 7

    new-instance v3, Lcom/google/googlenav/ui/android/a;

    invoke-direct {v3, p1}, Lcom/google/googlenav/ui/android/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    invoke-direct {p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/ui/android/a;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-static {v5}, Lcom/google/googlenav/ui/android/G;->b(Landroid/app/Dialog;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->f()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->h()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0, v3}, Law/h;->a(Law/q;)V

    new-instance v0, Lcom/google/googlenav/android/u;

    const-string v2, "RemoteStrings"

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/android/u;-><init>(Lcom/google/googlenav/android/i;Ljava/lang/String;Lcom/google/googlenav/ui/android/a;Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/android/u;->start()V

    :goto_1
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/android/a;->a(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/Config;->a([Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/android/i;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaJ/a;->u()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/android/i;)Lcom/google/android/maps/MapsActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/AndroidGmmApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/android/i;)Law/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/android/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/layer/r;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->h:Lcom/google/googlenav/layer/r;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/android/i;)LaN/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    return-object v0
.end method

.method private p()[LaE/e;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, LaE/g;->a:LaE/g;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaE/a;->a:LaE/a;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, LaE/b;->a:LaE/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LaE/d;->a:LaE/d;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LaE/e;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaE/e;

    return-object v0

    :cond_2
    sget-object v0, LaF/b;->a:LaF/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, LaE/i;->a:LaE/i;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, LaE/c;->a:LaE/c;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private q()V
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->L()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "UserAgentPref"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    check-cast v0, Lap/a;

    invoke-virtual {v0, v1}, Lap/a;->a(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/r;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/googlenav/android/r;-><init>(Lcom/google/googlenav/android/i;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private r()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->p()Lam/b;

    move-result-object v0

    check-cast v0, Lam/k;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lam/k;->a(Lam/l;)V

    new-instance v0, Lbf/al;

    invoke-direct {v0}, Lbf/al;-><init>()V

    invoke-static {v0}, Lbf/al;->a(Lbf/al;)V

    return-void
.end method

.method private s()V
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-le v1, v0, :cond_1

    const-string v0, "AndroidState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "******************** WARNING **** =====>  Memory leak detected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AndroidState instances !!! <==== **** WARNING ********************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private t()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ar()V

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lcom/google/googlenav/android/q;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/googlenav/android/q;-><init>(Lcom/google/googlenav/android/i;Las/c;Lcom/google/googlenav/android/aa;Z)V

    invoke-virtual {v1}, Lcom/google/googlenav/android/q;->g()V

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    if-nez v0, :cond_1

    const-string v0, "onConfigurationChanged"

    const-string v1, "currentLocale should not be null"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->a([Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/A;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/i;->o:Lcom/google/android/maps/A;

    return-void
.end method

.method public a(Lcom/google/android/maps/MapsActivity;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/content/Context;)V

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    check-cast v0, LaV/a;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LaV/a;->a(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    if-nez p2, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->initMapUi()V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    :cond_0
    if-eqz p4, :cond_1

    if-nez p3, :cond_1

    :cond_1
    iput-object p1, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    iput-boolean v1, p0, Lcom/google/googlenav/android/i;->u:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    invoke-virtual {p0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    iput-boolean v1, p0, Lcom/google/googlenav/android/i;->p:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p4}, Lcom/google/googlenav/android/i;->a(Ljava/lang/String;Lcom/google/googlenav/android/x;Z)V

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    goto :goto_0
.end method

.method public b()Lcom/google/googlenav/prefetch/android/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    return-object v0
.end method

.method public c()V
    .locals 5

    invoke-static {}, LaE/f;->a()LaE/f;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/android/i;->p()[LaE/e;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    instance-of v4, v0, LaF/a;

    if-eqz v4, :cond_0

    check-cast v0, LaF/a;

    iget-object v4, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v4}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0, v4}, LaF/a;->a(Landroid/content/Context;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-static {v0, v2}, LaE/f;->a(Lcom/google/googlenav/ui/s;[LaE/e;)V

    :cond_2
    return-void
.end method

.method public d()Lcom/google/googlenav/android/AndroidGmmApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method public e()Lcom/google/googlenav/android/d;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    return-object v0
.end method

.method public f()Lcom/google/android/maps/MapsActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method public g()LaN/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    return-object v0
.end method

.method public h()LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    return-object v0
.end method

.method public i()Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public j()Lcom/google/googlenav/ui/android/L;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    return-object v0
.end method

.method public k()Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method public l()V
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/googlenav/android/s;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/android/s;-><init>(Lcom/google/googlenav/android/i;Landroid/os/Handler;)V

    invoke-static {}, LR/o;->f()V

    iget-object v1, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-static {v1, v3, v2, v0}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public m()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    return v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    return v0
.end method
