.class public Lcom/google/googlenav/android/AndroidGmmApplication;
.super Landroid/app/Application;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/android/b;

.field private b:Lcom/google/googlenav/android/b;

.field private c:Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/android/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/android/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    return-void
.end method

.method public b()Lcom/google/googlenav/android/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    return-object v0
.end method

.method public b(Lcom/google/googlenav/android/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/b;->a(Landroid/content/res/Configuration;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/b;->a(Landroid/content/res/Configuration;)V

    :cond_1
    return-void
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {p0}, Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;->b(Landroid/content/Context;)Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->c:Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->e()V

    :cond_1
    return-void
.end method

.method public onTerminate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->a:Lcom/google/googlenav/android/b;

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->b:Lcom/google/googlenav/android/b;

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->d()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/android/AndroidGmmApplication;->c:Lcom/google/googlenav/friend/OptInSyncBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
