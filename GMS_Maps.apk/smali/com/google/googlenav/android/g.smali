.class Lcom/google/googlenav/android/g;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/android/d;

.field private final b:Lcom/google/googlenav/aB;

.field private final c:Ljava/lang/String;

.field private d:J

.field private e:J


# direct methods
.method private constructor <init>(Lcom/google/googlenav/android/d;Ljava/lang/String;Lcom/google/googlenav/aB;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/android/g;->a:Lcom/google/googlenav/android/d;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/android/g;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/android/g;->b:Lcom/google/googlenav/aB;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/android/d;Ljava/lang/String;Lcom/google/googlenav/aB;Lcom/google/googlenav/android/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/android/g;-><init>(Lcom/google/googlenav/android/d;Ljava/lang/String;Lcom/google/googlenav/aB;)V

    return-void
.end method

.method private a()Landroid/telephony/TelephonyManager;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/android/g;->a:Lcom/google/googlenav/android/d;

    invoke-static {v0}, Lcom/google/googlenav/android/d;->a(Lcom/google/googlenav/android/d;)Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private a(J)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/android/g;->a()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v1, p0, Lcom/google/googlenav/android/g;->b:Lcom/google/googlenav/aB;

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v1, p1, p2, v0}, Lcom/google/googlenav/aB;->a(JZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/android/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/android/g;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/android/g;->d:J

    invoke-direct {p0}, Lcom/google/googlenav/android/g;->a()Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, p0, v1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 8

    const-wide/16 v6, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    iget-wide v2, p0, Lcom/google/googlenav/android/g;->d:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x61a8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-direct {p0, v6, v7}, Lcom/google/googlenav/android/g;->a(J)V

    :cond_0
    iput-wide v0, p0, Lcom/google/googlenav/android/g;->e:J

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-nez p1, :cond_1

    iget-wide v2, p0, Lcom/google/googlenav/android/g;->e:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/googlenav/android/g;->d:J

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/android/g;->a(J)V

    goto :goto_0
.end method
