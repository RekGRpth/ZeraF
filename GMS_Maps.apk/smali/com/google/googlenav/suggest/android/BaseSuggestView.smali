.class public abstract Lcom/google/googlenav/suggest/android/BaseSuggestView;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/graphics/drawable/AnimationDrawable;

.field protected b:Z

.field protected final c:Landroid/view/inputmethod/InputMethodManager;

.field protected final d:Ljava/lang/Runnable;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private final g:Ljava/lang/Runnable;

.field private final h:Ljava/lang/Runnable;

.field private i:Lcom/google/googlenav/suggest/android/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b:Z

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    new-instance v0, Lcom/google/googlenav/suggest/android/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/b;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/googlenav/suggest/android/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/c;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/googlenav/suggest/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/d;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b:Z

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    new-instance v0, Lcom/google/googlenav/suggest/android/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/b;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/googlenav/suggest/android/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/c;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/googlenav/suggest/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/d;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Lcom/google/googlenav/suggest/android/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->i:Lcom/google/googlenav/suggest/android/h;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/BaseSuggestView;Ljava/lang/CharSequence;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->performFiltering(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 10

    const/4 v9, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d()V

    new-instance v0, Lcom/google/googlenav/suggest/android/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/e;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v0, Lcom/google/googlenav/suggest/android/f;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/f;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/google/googlenav/suggest/android/g;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/g;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b()Lam/g;

    move-result-object v5

    invoke-interface {v5}, Lam/g;->a()Ljava/lang/String;

    move-result-object v6

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-interface {v5, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-le v0, v4, :cond_0

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    :cond_0
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-le v0, v3, :cond_1

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    const/16 v8, 0x64

    invoke-virtual {v0, v7, v8}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2, v2, v4, v3}, Landroid/graphics/drawable/AnimationDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {p0, v9, v9, v0, v9}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a(Z)V

    return-void
.end method

.method protected final a(Z)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz p1, :cond_1

    const/16 v0, 0xff

    :goto_1
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/AnimationDrawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/AnimationDrawable;->setVisible(ZZ)Z

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->invalidateSelf()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected abstract b()Lam/g;
.end method

.method protected final c()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method protected convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract d()V
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public onFilterComplete(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-lez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->showDropDown()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->dismissDropDown()V

    goto :goto_0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isPerformingCompletion()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AutoCompleteTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/i;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->i:Lcom/google/googlenav/suggest/android/h;

    return-void
.end method
