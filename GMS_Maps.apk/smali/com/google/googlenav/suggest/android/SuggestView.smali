.class public Lcom/google/googlenav/suggest/android/SuggestView;
.super Lcom/google/googlenav/suggest/android/BaseSuggestView;
.source "SourceFile"


# instance fields
.field protected e:Z

.field protected f:Z

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/suggest/android/BaseSuggestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->h:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->i:I

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->e:Z

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/suggest/android/BaseSuggestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->h:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->i:I

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->e:Z

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/SuggestView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->i:I

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/suggest/android/SuggestView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->h:I

    return v0
.end method


# virtual methods
.method protected b()Lam/g;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->z()Lam/g;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Lcom/google/googlenav/suggest/android/l;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/google/googlenav/suggest/android/l;-><init>(Lcom/google/googlenav/suggest/android/SuggestView;Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public enoughToFilter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->g:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_0
    return-void
.end method

.method public setEnoughToFilter(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->g:Z

    return-void
.end method

.method public setFeatureTypeRestrict(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->h:I

    return-void
.end method

.method public setInputIndex(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->i:I

    return-void
.end method

.method public setSuggestEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/suggest/android/SuggestView;->e:Z

    return-void
.end method

.method public showDropDown()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->e:Z

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->showDropDown()V

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestView;->f:Z

    :cond_1
    return-void
.end method
