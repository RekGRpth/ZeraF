.class public Lcom/google/googlenav/cq;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Lcom/google/googlenav/cr;

.field private d:Z

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;IJJLcom/google/googlenav/cr;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cq;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/googlenav/cq;->b:I

    iput-wide p3, p0, Lcom/google/googlenav/cq;->e:J

    iput-wide p5, p0, Lcom/google/googlenav/cq;->f:J

    iput-object p7, p0, Lcom/google/googlenav/cq;->c:Lcom/google/googlenav/cr;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 8

    const-wide/high16 v6, -0x8000000000000000L

    const/4 v5, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ib;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v2, p0, Lcom/google/googlenav/cq;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/googlenav/cq;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v2, p0, Lcom/google/googlenav/cq;->e:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/google/googlenav/cq;->e:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-wide v2, p0, Lcom/google/googlenav/cq;->f:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/google/googlenav/cq;->f:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    const/4 v1, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ib;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/cq;->d:Z

    iget-boolean v0, p0, Lcom/google/googlenav/cq;->d:Z

    if-nez v0, :cond_0

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x5c

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cq;->c:Lcom/google/googlenav/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/cq;->c:Lcom/google/googlenav/cr;

    iget-boolean v1, p0, Lcom/google/googlenav/cq;->d:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/cr;->a(Z)V

    :cond_0
    return-void
.end method
