.class public Lcom/google/googlenav/M;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected static a(Ljava/lang/String;Lcom/google/googlenav/P;)Lcom/google/googlenav/O;
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "6.9.2"

    invoke-static {p0, v0}, Lcom/google/googlenav/K;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {p0}, Lcom/google/googlenav/M;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/P;->c()Z

    move-result v3

    :goto_1
    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/P;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    if-eqz v4, :cond_4

    :goto_3
    new-instance v2, Lcom/google/googlenav/O;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v4, v1, v3}, Lcom/google/googlenav/O;-><init>(ZZZLcom/google/googlenav/N;)V

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/P;->b()Z

    move-result v3

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "6.8.0"

    invoke-static {p0, v0}, Lcom/google/googlenav/K;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/googlenav/O;
    .locals 1

    new-instance v0, Lcom/google/googlenav/P;

    invoke-direct {v0}, Lcom/google/googlenav/P;-><init>()V

    invoke-static {p0, v0}, Lcom/google/googlenav/M;->a(Ljava/lang/String;Lcom/google/googlenav/P;)Lcom/google/googlenav/O;

    move-result-object v0

    return-object v0
.end method
