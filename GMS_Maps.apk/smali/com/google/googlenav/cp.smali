.class public Lcom/google/googlenav/cp;
.super Lcom/google/googlenav/ai;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/googlenav/bZ;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bZ;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->j()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x17

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    iput-object p1, p0, Lcom/google/googlenav/cp;->c:Lcom/google/googlenav/bZ;

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cp;->c:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->j()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/cp;->h:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/cp;->f:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/cp;->e:Z

    return-void
.end method

.method public av()Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cp;->c:Lcom/google/googlenav/bZ;

    return-object v0
.end method

.method public aw()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cp;->c:Lcom/google/googlenav/bZ;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public bV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cp;->c:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "TransitStation"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/cp;->d:Z

    return-void
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cp;->d:Z

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cp;->e:Z

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cp;->f:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cp;->g:Z

    return v0
.end method

.method public n()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/cp;->h:I

    return v0
.end method
