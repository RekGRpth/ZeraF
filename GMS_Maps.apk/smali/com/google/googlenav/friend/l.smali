.class public Lcom/google/googlenav/friend/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/googlenav/friend/m;

.field private g:Z

.field private h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/friend/m;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput p1, p0, Lcom/google/googlenav/friend/l;->a:I

    iput p2, p0, Lcom/google/googlenav/friend/l;->b:I

    iput-object p3, p0, Lcom/google/googlenav/friend/l;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/friend/l;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/googlenav/friend/l;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/googlenav/friend/l;->f:Lcom/google/googlenav/friend/m;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gl;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v2, p0, Lcom/google/googlenav/friend/l;->a:I

    mul-int/lit8 v2, v2, 0xa

    iget v3, p0, Lcom/google/googlenav/friend/l;->b:I

    mul-int/lit8 v3, v3, 0xa

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/friend/l;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/friend/l;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/friend/l;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/googlenav/friend/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gl;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/l;->g:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x74

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/l;->f:Lcom/google/googlenav/friend/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/l;->f:Lcom/google/googlenav/friend/m;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/l;->g:Z

    iget-object v2, p0, Lcom/google/googlenav/friend/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/friend/m;->a(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method
