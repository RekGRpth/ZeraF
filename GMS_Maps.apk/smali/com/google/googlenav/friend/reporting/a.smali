.class public abstract Lcom/google/googlenav/friend/reporting/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:I

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/googlenav/friend/reporting/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/reporting/a;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/reporting/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/friend/reporting/c;-><init>(Lcom/google/googlenav/friend/reporting/a;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->g:Lcom/google/googlenav/friend/reporting/c;

    iput-object p2, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    iput p4, p0, Lcom/google/googlenav/friend/reporting/a;->d:I

    iput-object p3, p0, Lcom/google/googlenav/friend/reporting/a;->c:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/googlenav/friend/reporting/a;->e:J

    iput-object p7, p0, Lcom/google/googlenav/friend/reporting/a;->f:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/reporting/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method private a(JLandroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    invoke-virtual {p3, v0, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    const-string v0, "SELECT min(%s) FROM %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/a;->c:Ljava/lang/String;

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v0

    new-array v2, v3, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    const-string v1, "%s = ?"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/googlenav/friend/reporting/a;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT count(*) FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/reporting/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/content/ContentValues;
.end method

.method protected abstract a(Landroid/database/Cursor;)Ljava/lang/Object;
.end method

.method public a(J)V
    .locals 3

    sget-object v1, Lcom/google/googlenav/friend/reporting/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->g:Lcom/google/googlenav/friend/reporting/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/friend/reporting/a;->a(JLandroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/googlenav/friend/reporting/b;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/reporting/b;-><init>(Ljava/lang/Exception;)V

    throw v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract a()Z
.end method

.method public b()Ljava/util/List;
    .locals 5

    sget-object v1, Lcom/google/googlenav/friend/reporting/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/googlenav/friend/reporting/a;->d()V

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->g:Lcom/google/googlenav/friend/reporting/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select * from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/reporting/a;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_1
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v2, Lcom/google/googlenav/friend/reporting/b;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/reporting/b;-><init>(Ljava/lang/Exception;)V

    throw v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_1
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    return-object v3

    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 8

    const-wide/16 v6, -0x1

    sget-object v1, Lcom/google/googlenav/friend/reporting/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/googlenav/friend/reporting/a;->d()V

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/a;->g:Lcom/google/googlenav/friend/reporting/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/a;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const-wide/16 v2, -0x1

    invoke-direct {p0, v2, v3, v0}, Lcom/google/googlenav/friend/reporting/a;->a(JLandroid/database/sqlite/SQLiteDatabase;)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/reporting/a;->b(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v2

    iget v4, p0, Lcom/google/googlenav/friend/reporting/a;->d:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/reporting/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lcom/google/googlenav/friend/reporting/b;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/reporting/b;-><init>(Ljava/lang/Exception;)V

    throw v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/googlenav/friend/reporting/a;->e:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/google/googlenav/friend/reporting/a;->a(JLandroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/a;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/a;->a(Ljava/lang/Object;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v0, v2, v6

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/friend/reporting/a;->a(J)V

    return-void
.end method
