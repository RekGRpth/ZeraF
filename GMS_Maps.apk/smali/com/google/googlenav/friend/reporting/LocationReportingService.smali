.class public Lcom/google/googlenav/friend/reporting/LocationReportingService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lak/a;

.field private b:Landroid/net/ConnectivityManager;

.field private c:Lcom/google/googlenav/friend/reporting/g;

.field private d:Landroid/os/PowerManager$WakeLock;

.field private e:Lcom/google/googlenav/friend/reporting/n;

.field private f:Lcom/google/googlenav/friend/reporting/s;

.field private g:Lcom/google/googlenav/friend/reporting/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "LocationReportingService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method a(Lcom/google/googlenav/friend/f;)Lcom/google/googlenav/friend/h;
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/f;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/h;

    return-object v0
.end method

.method a(Landroid/content/Intent;)V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a:Lak/a;

    invoke-virtual {v0}, Lak/a;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->c:Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/g;->b()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->g:Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/d;->b()Ljava/util/List;
    :try_end_0
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/reporting/k;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/k;->e()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->c:Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/g;->c()V

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->g:Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/d;->c()V
    :try_end_1
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/friend/reporting/s;->b(Z)Ljava/util/List;

    move-result-object v0

    new-instance v5, Lcom/google/googlenav/friend/f;

    iget-object v6, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v6}, Lcom/google/googlenav/friend/reporting/s;->b()I

    move-result v6

    invoke-direct {v5, v1, v0, v2, v6}, Lcom/google/googlenav/friend/f;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v5}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a(Lcom/google/googlenav/friend/f;)Lcom/google/googlenav/friend/h;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v0, v5, v0

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/ad;->a(J)V

    if-eqz v2, :cond_0

    iget-boolean v0, v2, Lcom/google/googlenav/friend/h;->a:Z

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->c:Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/friend/reporting/g;->a(J)V

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->g:Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/friend/reporting/d;->a(J)V
    :try_end_2
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_2 .. :try_end_2} :catch_1

    iget-object v0, v2, Lcom/google/googlenav/friend/h;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->e:Lcom/google/googlenav/friend/reporting/n;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/reporting/n;->b()V

    :cond_3
    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_4
    :goto_1
    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/reporting/s;->d()Z

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/reporting/s;->a(Z)V

    goto/16 :goto_0

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/friend/aH;->h()V

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->e:Lcom/google/googlenav/friend/reporting/n;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/reporting/n;->b()V

    goto :goto_1

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/friend/aH;->g()V

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->e:Lcom/google/googlenav/friend/reporting/n;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/reporting/n;->b()V

    goto :goto_1

    :catch_1
    move-exception v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    return v0
.end method

.method b()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a:Lak/a;

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a:Lak/a;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lak/a;->a(Landroid/content/Context;)V

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->b:Landroid/net/ConnectivityManager;

    new-instance v0, Lcom/google/googlenav/friend/reporting/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    invoke-static {p0, v0}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    new-instance v1, Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/googlenav/friend/reporting/g;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;Lcom/google/googlenav/friend/reporting/s;)V

    iput-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->c:Lcom/google/googlenav/friend/reporting/g;

    new-instance v0, Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/friend/reporting/d;-><init>(Landroid/content/Context;Lbm/c;Lcom/google/googlenav/friend/reporting/s;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->g:Lcom/google/googlenav/friend/reporting/d;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "LocationReportingService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/n;->a(Landroid/content/Context;)Lcom/google/googlenav/friend/reporting/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->e:Lcom/google/googlenav/friend/reporting/n;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/LocationReportingService;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReportingService;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
