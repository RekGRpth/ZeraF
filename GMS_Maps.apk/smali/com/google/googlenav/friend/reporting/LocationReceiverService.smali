.class public Lcom/google/googlenav/friend/reporting/LocationReceiverService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/friend/reporting/g;

.field private b:Lcom/google/googlenav/friend/reporting/d;

.field private c:Lcom/google/googlenav/android/F;

.field private d:Lcom/google/googlenav/friend/reporting/h;

.field private e:Lcom/google/googlenav/friend/reporting/o;

.field private f:Lcom/google/googlenav/friend/reporting/s;

.field private g:Landroid/os/PowerManager$WakeLock;

.field private h:Lcom/google/googlenav/friend/reporting/e;

.field private i:Lcom/google/googlenav/friend/reporting/n;

.field private j:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "LocationReceiverService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    const-string v0, "cell"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "gps"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/s;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->b:Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/reporting/d;->b(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Landroid/location/Location;I)Z
    .locals 2

    invoke-virtual {p0, p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/location/Location;)Lcom/google/googlenav/friend/reporting/m;
    .locals 6

    const-wide v4, 0x416312d000000000L

    new-instance v0, Lcom/google/googlenav/friend/reporting/m;

    invoke-direct {v0}, Lcom/google/googlenav/friend/reporting/m;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/m;->a(II)Lcom/google/googlenav/friend/reporting/m;

    move-result-object v0

    invoke-static {p0}, LaJ/a;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/reporting/m;->a(I)Lcom/google/googlenav/friend/reporting/m;

    move-result-object v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/m;->a(J)Lcom/google/googlenav/friend/reporting/m;

    move-result-object v0

    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/reporting/m;->a(F)Lcom/google/googlenav/friend/reporting/m;

    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/reporting/m;->b(F)Lcom/google/googlenav/friend/reporting/m;

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/m;->a(D)Lcom/google/googlenav/friend/reporting/m;

    :cond_2
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/reporting/m;->c(F)Lcom/google/googlenav/friend/reporting/m;

    :cond_3
    return-object v0
.end method

.method private b()Z
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->j:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/reporting/s;->c()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    sub-long v2, v0, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(J)V

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/googlenav/friend/reporting/LocationReportingService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method a(Landroid/content/Intent;)V
    .locals 8

    const/4 v0, 0x0

    const-string v1, "activity"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "providerEnabled"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->h:Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/reporting/e;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->i:Lcom/google/googlenav/friend/reporting/n;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/n;->b()V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Intent;)Lcom/google/android/location/clientlib/c;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/location/clientlib/c;->a:Landroid/location/Location;

    if-eqz v2, :cond_0

    const-string v3, "gps"

    invoke-virtual {v2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->j:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Landroid/location/Location;->setTime(J)V

    :cond_4
    iget v3, v1, Lcom/google/android/location/clientlib/c;->b:I

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Landroid/location/Location;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Landroid/location/Location;)V

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/reporting/s;->b(Z)Ljava/util/List;

    move-result-object v5

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/reporting/s;->a()Landroid/location/Location;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->d:Lcom/google/googlenav/friend/reporting/h;

    invoke-virtual {v4, v2, v3, v5}, Lcom/google/googlenav/friend/reporting/h;->a(Landroid/location/Location;Landroid/location/Location;Ljava/util/List;)Lcom/google/googlenav/friend/reporting/j;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/friend/reporting/j;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->b(Landroid/location/Location;)Lcom/google/googlenav/friend/reporting/m;

    move-result-object v3

    iget v4, v1, Lcom/google/android/location/clientlib/c;->b:I

    const/4 v7, -0x1

    if-eq v4, v7, :cond_5

    iget v4, v1, Lcom/google/android/location/clientlib/c;->b:I

    invoke-virtual {v3, v4}, Lcom/google/googlenav/friend/reporting/m;->b(I)Lcom/google/googlenav/friend/reporting/m;

    :cond_5
    iget-object v4, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->c:Lcom/google/googlenav/android/F;

    invoke-virtual {v4}, Lcom/google/googlenav/android/F;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/friend/reporting/m;->c(I)Lcom/google/googlenav/friend/reporting/m;

    iget-object v4, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->c:Lcom/google/googlenav/android/F;

    invoke-virtual {v4}, Lcom/google/googlenav/android/F;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/friend/reporting/m;->a(Z)Lcom/google/googlenav/friend/reporting/m;

    invoke-virtual {v6}, Lcom/google/googlenav/friend/reporting/j;->a()Z

    move-result v4

    if-nez v4, :cond_6

    const/4 v0, 0x1

    :cond_6
    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/reporting/m;->b(Z)Lcom/google/googlenav/friend/reporting/m;

    iget-object v0, v1, Lcom/google/android/location/clientlib/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/google/android/location/clientlib/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/reporting/m;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/reporting/m;

    :cond_7
    iget-object v0, v1, Lcom/google/android/location/clientlib/c;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    iget-object v0, v1, Lcom/google/android/location/clientlib/c;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/reporting/m;->d(I)Lcom/google/googlenav/friend/reporting/m;

    :cond_8
    invoke-virtual {v3}, Lcom/google/googlenav/friend/reporting/m;->a()Lcom/google/googlenav/friend/reporting/k;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a:Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/reporting/g;->b(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/googlenav/friend/reporting/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/location/Location;)Z

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->e:Lcom/google/googlenav/friend/reporting/o;

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->j:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/reporting/s;->e()J

    move-result-wide v3

    invoke-virtual {v6}, Lcom/google/googlenav/friend/reporting/j;->a()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/friend/reporting/o;->a(JJLjava/util/List;Z)Lcom/google/googlenav/friend/reporting/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/q;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/s;->d(J)Z

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method a(Landroid/location/Location;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "location"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method a(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/reporting/f;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->j:Lcom/google/googlenav/common/a;

    invoke-static {p0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    new-instance v0, Lcom/google/googlenav/friend/reporting/g;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-direct {v0, v2, v1, v3}, Lcom/google/googlenav/friend/reporting/g;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;Lcom/google/googlenav/friend/reporting/s;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a:Lcom/google/googlenav/friend/reporting/g;

    new-instance v0, Lcom/google/googlenav/friend/reporting/d;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->f:Lcom/google/googlenav/friend/reporting/s;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/friend/reporting/d;-><init>(Landroid/content/Context;Lbm/c;Lcom/google/googlenav/friend/reporting/s;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->b:Lcom/google/googlenav/friend/reporting/d;

    new-instance v0, Lcom/google/googlenav/android/F;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/android/F;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->c:Lcom/google/googlenav/android/F;

    new-instance v0, Lcom/google/googlenav/friend/reporting/h;

    invoke-direct {v0}, Lcom/google/googlenav/friend/reporting/h;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->d:Lcom/google/googlenav/friend/reporting/h;

    new-instance v0, Lcom/google/googlenav/friend/reporting/o;

    invoke-direct {v0}, Lcom/google/googlenav/friend/reporting/o;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->e:Lcom/google/googlenav/friend/reporting/o;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/n;->a(Landroid/content/Context;)Lcom/google/googlenav/friend/reporting/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->i:Lcom/google/googlenav/friend/reporting/n;

    new-instance v0, Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->h:Lcom/google/googlenav/friend/reporting/e;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "LocationReceiverService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->g:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationReceiverService;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
