.class Lcom/google/googlenav/friend/c;
.super Lcom/google/googlenav/friend/S;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/List;

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    iput-object p2, p0, Lcom/google/googlenav/friend/c;->c:Ljava/util/List;

    iput-object p3, p0, Lcom/google/googlenav/friend/c;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public d_()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/friend/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/b;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/c;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/friend/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_1
    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lcom/google/googlenav/friend/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/googlenav/friend/c;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/friend/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_2
    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/friend/c;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-super {p0}, Lcom/google/googlenav/friend/S;->d_()V

    return-void
.end method
