.class Lcom/google/googlenav/friend/aE;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/googlenav/friend/aE;


# instance fields
.field private final b:Lcom/google/googlenav/friend/aD;

.field private final c:Lcom/google/googlenav/friend/aD;

.field private final d:Lcom/google/googlenav/friend/aD;

.field private final e:Lcom/google/googlenav/friend/aD;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/aE;

    invoke-direct {v0}, Lcom/google/googlenav/friend/aE;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/aE;->a:Lcom/google/googlenav/friend/aE;

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/aD;

    const-wide/16 v1, 0x1

    const/16 v3, 0x3d3

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f0202a1

    const v8, 0x7f0200bd

    sget-object v9, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aE;->b:Lcom/google/googlenav/friend/aD;

    new-instance v0, Lcom/google/googlenav/friend/aD;

    const-wide/16 v1, 0x2

    const/16 v3, 0x9c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f020220

    const v8, 0x7f020221

    sget-object v9, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aE;->c:Lcom/google/googlenav/friend/aD;

    new-instance v0, Lcom/google/googlenav/friend/aD;

    const-wide v1, -0x411daa5aff29b7b0L

    const/16 v3, 0x3e1

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f02029c

    const v8, 0x7f0200bb

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aE;->d:Lcom/google/googlenav/friend/aD;

    new-instance v0, Lcom/google/googlenav/friend/aD;

    const-wide v1, -0x35c1067b89114543L

    const/16 v3, 0x3d4

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f02029d

    const v8, 0x7f0200bc

    sget-object v9, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    invoke-direct/range {v0 .. v9}, Lcom/google/googlenav/friend/aD;-><init>(JLjava/lang/String;Ljava/lang/String;JIILcom/google/googlenav/friend/aG;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/aE;->e:Lcom/google/googlenav/friend/aD;

    return-void
.end method

.method static a()Lcom/google/googlenav/friend/aE;
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/aE;->a:Lcom/google/googlenav/friend/aE;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/googlenav/friend/aD;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->d:Lcom/google/googlenav/friend/aD;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->e:Lcom/google/googlenav/friend/aD;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->b:Lcom/google/googlenav/friend/aD;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->c:Lcom/google/googlenav/friend/aD;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method b()Lcom/google/googlenav/friend/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->b:Lcom/google/googlenav/friend/aD;

    return-object v0
.end method

.method c()Lcom/google/googlenav/friend/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->c:Lcom/google/googlenav/friend/aD;

    return-object v0
.end method

.method d()Lcom/google/googlenav/friend/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->e:Lcom/google/googlenav/friend/aD;

    return-object v0
.end method

.method e()Lcom/google/googlenav/friend/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aE;->d:Lcom/google/googlenav/friend/aD;

    return-object v0
.end method
