.class public Lcom/google/googlenav/friend/aH;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(I)V
    .locals 1

    const-string v0, "LOCATION_REPORTER_PRIVACY_SETTING"

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aU;->b(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(ILcom/google/googlenav/friend/bc;)V
    .locals 1

    const-string v0, "LOCATION_REPORTER_PRIVACY_SETTING"

    invoke-static {v0, p0, p1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/friend/ae;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/ae;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->b(Z)V

    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/friend/aH;->f()V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/friend/aH;->g()V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/google/googlenav/friend/aH;->h()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/googlenav/friend/bc;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aH;->a(ILcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static a(Z)V
    .locals 2

    const-string v0, "LOCATION_SHARING"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ZLcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static a(ZLcom/google/googlenav/friend/bc;)V
    .locals 1

    const-string v0, "LOCATION_SHARING"

    invoke-static {v0, p0, p1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ZLcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static a()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/aH;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/googlenav/friend/ae;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/ae;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(Z)V

    return-void

    :pswitch_0
    invoke-static {v1}, Lcom/google/googlenav/friend/aH;->a(Lcom/google/googlenav/friend/bc;)V

    goto :goto_0

    :pswitch_1
    invoke-static {v1}, Lcom/google/googlenav/friend/aH;->b(Lcom/google/googlenav/friend/bc;)V

    goto :goto_0

    :pswitch_2
    invoke-static {v1}, Lcom/google/googlenav/friend/aH;->c(Lcom/google/googlenav/friend/bc;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Lcom/google/googlenav/friend/bc;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aH;->a(ILcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static b(Z)V
    .locals 1

    const-string v0, "LOCATION_SHARING"

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aU;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public static b()Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/googlenav/friend/aH;->d()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/google/googlenav/friend/bc;)V
    .locals 1

    const/4 v0, 0x2

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aH;->a(ILcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static c()Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/aH;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()I
    .locals 2

    const-string v0, "LOCATION_REPORTER_PRIVACY_SETTING"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static d(Lcom/google/googlenav/friend/bc;)V
    .locals 1

    const-string v0, "LOCATION_REPORTER_PRIVACY_SETTING"

    invoke-static {v0, p0}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;Lcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method public static e()Z
    .locals 2

    const-string v0, "LOCATION_SHARING"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static f()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(I)V

    return-void
.end method

.method public static g()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(I)V

    return-void
.end method

.method public static h()V
    .locals 1

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(I)V

    return-void
.end method
