.class public Lcom/google/googlenav/friend/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/h;
.implements Lcom/google/googlenav/bb;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Lcom/google/googlenav/friend/o;

.field private static final b:Ljava/lang/Runnable;

.field private static d:Lcom/google/googlenav/ui/s;

.field private static volatile g:Z


# instance fields
.field private final c:Las/d;

.field private e:Z

.field private volatile f:Z

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/o;

    invoke-direct {v0}, Lcom/google/googlenav/friend/o;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    new-instance v0, Lcom/google/googlenav/friend/q;

    invoke-direct {v0}, Lcom/google/googlenav/friend/q;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/p;->b:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Las/c;Lcom/google/googlenav/ui/s;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    new-instance v0, Las/d;

    invoke-direct {v0, p1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    sput-object p2, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    return-void
.end method

.method public static a(Lcom/google/googlenav/bb;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/friend/r;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/r;-><init>(Lcom/google/googlenav/bb;)V

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-static {}, LaN/H;->i()LaN/H;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(Z)Lcom/google/googlenav/bg;

    :cond_0
    sget-object v1, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/o;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->g(I)Lcom/google/googlenav/bg;

    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    invoke-static {v3}, Lcom/google/googlenav/friend/p;->b(Z)V

    return-void
.end method

.method static synthetic a(Z)V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/friend/p;->b(Z)V

    return-void
.end method

.method private static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/google/googlenav/friend/p;->g:Z

    invoke-static {}, Lcom/google/googlenav/friend/p;->l()V

    return-void
.end method

.method public static e()Z
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/friend/p;->g:Z

    return v0
.end method

.method public static g()Lcom/google/googlenav/friend/o;
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    return-object v0
.end method

.method static synthetic h()Lcom/google/googlenav/ui/s;
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private i()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    return-void
.end method

.method private j()V
    .locals 3

    const-wide/32 v1, 0x1d4c0

    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1, v2}, Las/d;->c(J)V

    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    return-void
.end method

.method private k()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1}, Las/c;->b(Las/a;)I

    return-void
.end method

.method private static l()V
    .locals 3

    sget-object v0, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    sget-object v1, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/friend/p;->b:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public C_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->i()V

    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->j()V

    goto :goto_0
.end method

.method public M_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    return-void
.end method

.method public N_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->j()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/bb;J)V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1, v1}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/friend/p;->h:J

    sub-long v2, v0, v2

    cmp-long v2, v2, p2

    if-ltz v2, :cond_2

    iput-wide v0, p0, Lcom/google/googlenav/friend/p;->h:J

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p1, v0}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    :cond_3
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .locals 0

    return-void
.end method

.method public run()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    return-void
.end method
