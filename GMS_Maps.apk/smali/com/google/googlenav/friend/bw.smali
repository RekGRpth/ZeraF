.class public Lcom/google/googlenav/friend/bw;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:Z

.field private final b:Lcom/google/googlenav/common/f;

.field private c:Ljava/util/Vector;

.field private d:Ljava/util/Vector;

.field private e:Lcom/google/googlenav/common/f;

.field private f:Lcom/google/googlenav/common/f;

.field private g:Lcom/google/googlenav/common/f;

.field private h:Lcom/google/googlenav/common/f;

.field private i:Lcom/google/googlenav/common/f;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/f;Ljava/util/Vector;Ljava/util/Vector;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/bw;->b:Lcom/google/googlenav/common/f;

    iput-object p2, p0, Lcom/google/googlenav/friend/bw;->c:Ljava/util/Vector;

    iput-object p3, p0, Lcom/google/googlenav/friend/bw;->d:Ljava/util/Vector;

    iput-object p4, p0, Lcom/google/googlenav/friend/bw;->e:Lcom/google/googlenav/common/f;

    iput-object p5, p0, Lcom/google/googlenav/friend/bw;->f:Lcom/google/googlenav/common/f;

    iput-object p6, p0, Lcom/google/googlenav/friend/bw;->g:Lcom/google/googlenav/common/f;

    iput-object p7, p0, Lcom/google/googlenav/friend/bw;->h:Lcom/google/googlenav/common/f;

    iput-object p8, p0, Lcom/google/googlenav/friend/bw;->i:Lcom/google/googlenav/common/f;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 7

    const/4 v1, 0x0

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->b:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/googlenav/friend/bw;->b:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->c:Ljava/util/Vector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    const/4 v5, 0x2

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->c:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->d:Ljava/util/Vector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->d:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->e:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->e:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_3
    if-ge v0, v2, :cond_3

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/googlenav/friend/bw;->e:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->f:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->f:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_4
    if-ge v0, v2, :cond_4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/googlenav/friend/bw;->f:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->g:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->g:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_5
    if-ge v0, v2, :cond_5

    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/googlenav/friend/bw;->g:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->h:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->h:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_6
    if-ge v0, v2, :cond_6

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/googlenav/friend/bw;->h:Lcom/google/googlenav/common/f;

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->i:Lcom/google/googlenav/common/f;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/friend/bw;->i:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->b()I

    move-result v2

    move v0, v1

    :goto_7
    if-ge v0, v2, :cond_7

    const/16 v1, 0x8

    iget-object v4, p0, Lcom/google/googlenav/friend/bw;->i:Lcom/google/googlenav/common/f;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v4

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput-boolean v5, p0, Lcom/google/googlenav/friend/bw;->a:Z

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :goto_1
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/bw;->a:Z

    invoke-virtual {p0, v3}, Lcom/google/googlenav/friend/bw;->a(Z)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/google/googlenav/friend/bw;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x2f

    return v0
.end method
