.class public Lcom/google/googlenav/friend/bs;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lcom/google/googlenav/friend/bt;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/googlenav/friend/bt;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/bs;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/bs;->z_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    invoke-interface {v0}, Lcom/google/googlenav/friend/bt;->a()V

    :cond_0
    invoke-super {p0}, Law/a;->Z()V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eC;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eC;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/bs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    return v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x47

    return v0
.end method

.method public d_()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    invoke-interface {v1, v0}, Lcom/google/googlenav/friend/bt;->a(I)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    iget-object v1, p0, Lcom/google/googlenav/friend/bs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/bt;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    invoke-interface {v0}, Lcom/google/googlenav/friend/bt;->b()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/googlenav/friend/bs;->b:Lcom/google/googlenav/friend/bt;

    invoke-interface {v0}, Lcom/google/googlenav/friend/bt;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
