.class public Lcom/google/googlenav/friend/checkins/CheckinNotificationService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/friend/bv;

.field private b:Landroid/net/wifi/WifiManager;

.field private c:Lcom/google/googlenav/friend/android/a;

.field private d:Lak/a;

.field private e:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "CheckinNotificationService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    const/4 v2, 0x6

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/location/Location;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const-wide v4, 0x412e848000000000L

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->B:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {p1}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->c(Landroid/location/Location;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private static c(Landroid/location/Location;)I
    .locals 2

    invoke-static {p0}, LaJ/a;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaJ/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LaJ/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/location/Location;)Lcom/google/googlenav/friend/checkins/c;
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Lcom/google/googlenav/friend/checkins/c;

    invoke-direct {v3}, Lcom/google/googlenav/friend/checkins/c;-><init>()V

    iput-object p1, v3, Lcom/google/googlenav/friend/checkins/c;->a:Landroid/location/Location;

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/friend/bv;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bv;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->b(Landroid/location/Location;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, v3, Lcom/google/googlenav/friend/checkins/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v3, Lcom/google/googlenav/friend/checkins/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    iget-object v4, v3, Lcom/google/googlenav/friend/checkins/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/friend/bv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {p1}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->c(Landroid/location/Location;)I

    move-result v0

    iput v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    iget v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    if-ne v0, v1, :cond_1

    iget-object v0, v3, Lcom/google/googlenav/friend/checkins/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    invoke-static {p0, v0}, Lcom/google/googlenav/friend/checkins/a;->a(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->c:Z

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    iget v4, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    invoke-virtual {v0, v4}, Lcom/google/googlenav/friend/bv;->a(I)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->d:Z

    iget v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    iget-boolean v4, v3, Lcom/google/googlenav/friend/checkins/c;->d:Z

    invoke-static {p0, v0, v4}, Lcom/google/googlenav/friend/checkins/a;->a(Landroid/content/Context;IZ)V

    iget-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->d:Z

    if-eqz v0, :cond_2

    iget v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    if-eq v0, v5, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->e:Z

    iget-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->c:Z

    if-eqz v0, :cond_3

    iget-boolean v0, v3, Lcom/google/googlenav/friend/checkins/c;->d:Z

    if-nez v0, :cond_3

    iget v0, v3, Lcom/google/googlenav/friend/checkins/c;->g:I

    if-eq v0, v5, :cond_3

    :goto_2
    iput-boolean v1, v3, Lcom/google/googlenav/friend/checkins/c;->f:Z

    move-object v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method a(Lcom/google/googlenav/friend/checkins/c;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->c:Lcom/google/googlenav/friend/android/a;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/android/a;->a(Lcom/google/googlenav/friend/checkins/c;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->d:Lak/a;

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->d:Lak/a;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lak/a;->a(Landroid/content/Context;)V

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->b:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/google/googlenav/friend/checkins/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/checkins/b;-><init>()V

    new-instance v1, Lcom/google/googlenav/friend/android/a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/googlenav/friend/android/a;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/android/p;Lcom/google/googlenav/friend/checkins/b;)V

    iput-object v1, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->c:Lcom/google/googlenav/friend/android/a;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "CheckinsNotificationService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->e:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->d:Lak/a;

    invoke-virtual {v0}, Lak/a;->a()V

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a(Landroid/location/Location;)Lcom/google/googlenav/friend/checkins/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a(Lcom/google/googlenav/friend/checkins/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->a:Lcom/google/googlenav/friend/bv;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bv;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/checkins/CheckinNotificationService;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
