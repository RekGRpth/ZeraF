.class public Lcom/google/googlenav/friend/aM;
.super Law/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/aN;

.field private b:Lcom/google/googlenav/friend/aO;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/aN;)V
    .locals 1

    invoke-direct {p0}, Law/b;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/aO;

    invoke-direct {v0}, Lcom/google/googlenav/friend/aO;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    iput-object p2, p0, Lcom/google/googlenav/friend/aM;->a:Lcom/google/googlenav/friend/aN;

    iput-object p1, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_0
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private o()V
    .locals 4

    const/4 v3, 0x7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Setting users preferences <has>/<value>:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " HISTORY_ENABLED "

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/friend/aM;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v1, " HOME_LOCATION "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " WORK_LOCATION "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " MARINER_OPT_IN "

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/friend/aM;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v1, " SHARING_ENABLED "

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/friend/aM;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    const-string v2, " PREFERENCE_CHANGE_CAUSE "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :pswitch_0
    const-string v1, "USER_SETTING"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v1, "GMM_CLIENT_UPGRADE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const-string v1, "MARINER_OPT_IN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_3
    const-string v1, "MARINER_OPT_OUT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/friend/aM;->o()V

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->ah:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/friend/aM;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->ai:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/googlenav/friend/aO;->a(Lcom/google/googlenav/friend/aO;Z)Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    invoke-static {v0, v3}, Lcom/google/googlenav/friend/aO;->a(Lcom/google/googlenav/friend/aO;Z)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x69

    return v0
.end method

.method public d_()V
    .locals 1

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->a:Lcom/google/googlenav/friend/aN;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    invoke-static {v0}, Lcom/google/googlenav/friend/aO;->a(Lcom/google/googlenav/friend/aO;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->a:Lcom/google/googlenav/friend/aN;

    invoke-interface {v0}, Lcom/google/googlenav/friend/aN;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->a:Lcom/google/googlenav/friend/aN;

    invoke-interface {v0}, Lcom/google/googlenav/friend/aN;->b()V

    goto :goto_0
.end method

.method public synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aM;->n()Lcom/google/googlenav/friend/aO;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/googlenav/friend/aO;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aM;->b:Lcom/google/googlenav/friend/aO;

    return-object v0
.end method
