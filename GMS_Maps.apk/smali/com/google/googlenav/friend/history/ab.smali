.class public Lcom/google/googlenav/friend/history/ab;
.super Lcom/google/googlenav/friend/history/V;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/friend/history/ab;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/V;-><init>()V

    iput p1, p0, Lcom/google/googlenav/friend/history/ab;->b:I

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/googlenav/friend/history/ab;->b:I

    const/16 v2, 0x41

    if-ne v0, v2, :cond_0

    const/16 v0, 0x283

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v2, 0x3e

    if-ne v0, v2, :cond_1

    const/16 v0, 0x282

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/text/DateFormatSymbols;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v2}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v3

    move v2, v0

    move v0, v1

    :goto_1
    const/4 v4, 0x7

    if-ge v0, v4, :cond_3

    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_2

    const/16 v2, 0x281

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    sget-object v5, Lcom/google/googlenav/friend/history/ab;->a:[I

    aget v0, v5, v0

    aget-object v0, v3, v0

    aput-object v0, v4, v1

    invoke-static {v2, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    ushr-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid input for dayOfWeek. Value: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/ab;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
