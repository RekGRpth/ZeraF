.class public Lcom/google/googlenav/friend/history/d;
.super Law/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:Z

.field private final c:Lcom/google/googlenav/friend/history/b;

.field private final d:Lcom/google/googlenav/friend/history/b;

.field private final e:I

.field private final f:I

.field private final g:Z

.field private final h:Lcom/google/googlenav/friend/history/e;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;IIZLcom/google/googlenav/friend/history/e;)V
    .locals 0

    invoke-direct {p0}, Law/b;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/friend/history/d;->c:Lcom/google/googlenav/friend/history/b;

    iput-object p2, p0, Lcom/google/googlenav/friend/history/d;->d:Lcom/google/googlenav/friend/history/b;

    iput p3, p0, Lcom/google/googlenav/friend/history/d;->e:I

    iput p4, p0, Lcom/google/googlenav/friend/history/d;->f:I

    iput-boolean p5, p0, Lcom/google/googlenav/friend/history/d;->g:Z

    iput-object p6, p0, Lcom/google/googlenav/friend/history/d;->h:Lcom/google/googlenav/friend/history/e;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->at:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/aN;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/googlenav/friend/history/d;->e:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    const/4 v2, 0x4

    iget v3, p0, Lcom/google/googlenav/friend/history/d;->f:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    iget-object v2, p0, Lcom/google/googlenav/friend/history/d;->c:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/b;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/googlenav/friend/history/d;->d:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/b;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBool(IZ)V

    const/4 v2, 0x5

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/googlenav/friend/history/d;->g:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBool(IZ)V

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->au:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/friend/history/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/d;->b:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/history/d;->b:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x82

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/history/d;->h:Lcom/google/googlenav/friend/history/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/d;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/d;->h:Lcom/google/googlenav/friend/history/e;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/history/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/history/d;->h:Lcom/google/googlenav/friend/history/e;

    invoke-interface {v0}, Lcom/google/googlenav/friend/history/e;->a()V

    goto :goto_0
.end method

.method protected synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/d;->n()Lcom/google/googlenav/friend/history/f;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lcom/google/googlenav/friend/history/f;
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/history/f;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/history/d;->b:Z

    iget-object v2, p0, Lcom/google/googlenav/friend/history/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/history/f;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public s_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
