.class public Lcom/google/googlenav/friend/as;
.super Lcom/google/googlenav/friend/bi;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/googlenav/friend/as;


# direct methods
.method constructor <init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/e;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/e;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/friend/bi;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/d;)V

    return-void
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/as;
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/as;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/googlenav/friend/as;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/friend/as;-><init>(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    sput-object v0, Lcom/google/googlenav/friend/as;->a:Lcom/google/googlenav/friend/as;

    sget-object v0, Lcom/google/googlenav/friend/as;->a:Lcom/google/googlenav/friend/as;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Z)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/friend/as;->z()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/friend/as;->y()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p0, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/friend/as;->q()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_1
.end method

.method public static declared-synchronized b(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/as;
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/as;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/friend/as;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1, p2}, Lcom/google/googlenav/friend/as;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/as;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/friend/as;->e()Lcom/google/googlenav/friend/as;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized e()Lcom/google/googlenav/friend/as;
    .locals 2

    const-class v0, Lcom/google/googlenav/friend/as;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/friend/as;->a:Lcom/google/googlenav/friend/as;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized i()Z
    .locals 2

    const-class v1, Lcom/google/googlenav/friend/as;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/friend/as;->a:Lcom/google/googlenav/friend/as;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static j()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/as;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/as;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k()Z
    .locals 1

    const-string v0, "LOCATION_REPORTING_TERMS_ACCEPTED_SETTING"

    invoke-static {v0}, Lcom/google/googlenav/friend/as;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static l()V
    .locals 3

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/googlenav/friend/as;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "LOCATION_REPORTING_TERMS_ACCEPTED_SETTING"

    invoke-static {}, Lcom/google/googlenav/friend/as;->q()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_0
.end method

.method public static o()Ljava/lang/String;
    .locals 1

    const-string v0, "LOCATION_REPORTING_TERMS_ACCEPTED_SETTING"

    return-object v0
.end method

.method public static q()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private static x()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/as;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static y()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/friend/as;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LOCATION_REPORTING_DEVICE_ENABLED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static z()Ljava/lang/String;
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public B_()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/as;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/as;->c:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x192

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1b0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    :cond_0
    return-void
.end method

.method protected declared-synchronized G_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/googlenav/friend/bi;->G_()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/as;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized M_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/googlenav/friend/bi;->M_()V

    new-instance v0, Lcom/google/googlenav/friend/at;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/at;-><init>(Lcom/google/googlenav/friend/as;)V

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->d(Lcom/google/googlenav/friend/bc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)Lcom/google/googlenav/friend/br;
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/au;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/googlenav/friend/au;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/bi;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/googlenav/friend/bi;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, Lcom/google/googlenav/friend/ae;

    new-instance v1, Lcom/google/googlenav/friend/U;

    invoke-direct {v1, p1}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/aH;->a(ZLcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/ae;

    new-instance v1, Lcom/google/googlenav/friend/U;

    invoke-direct {v1, p1}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->b()Z

    move-result v0

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected m()V
    .locals 3

    const/16 v0, 0x3d

    const-string v1, "a"

    const-string v2, "sr"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected p()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/as;->q()I

    move-result v0

    return v0
.end method
