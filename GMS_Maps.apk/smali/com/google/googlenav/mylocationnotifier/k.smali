.class public Lcom/google/googlenav/mylocationnotifier/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements Lcom/google/googlenav/friend/bf;


# static fields
.field static a:I

.field private static f:I

.field private static g:I


# instance fields
.field b:LaH/h;

.field c:Lcom/google/googlenav/ai;

.field d:LaH/h;

.field e:Z

.field private h:J

.field private final i:Lcom/google/googlenav/ui/s;

.field private j:Lcom/google/googlenav/mylocationnotifier/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x64

    const/4 v0, 0x6

    sput v0, Lcom/google/googlenav/mylocationnotifier/k;->f:I

    sput v1, Lcom/google/googlenav/mylocationnotifier/k;->g:I

    sput v1, Lcom/google/googlenav/mylocationnotifier/k;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->h:J

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/k;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    return-void
.end method


# virtual methods
.method public a(ILaH/m;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    return-void
.end method

.method public a(J)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->e:Z

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->e()V

    :cond_0
    return-void
.end method

.method a(LaH/h;Ljava/lang/String;Lcom/google/googlenav/friend/bf;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->e:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/google/googlenav/mylocationnotifier/k;->e:Z

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/k;->d:LaH/h;

    const/16 v0, 0x76

    const-string v1, ""

    invoke-static {v0, p4, v1}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/bg;

    invoke-direct {v1}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/friend/bg;->a(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/friend/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;

    :cond_1
    invoke-virtual {p1}, LaH/h;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, LaH/h;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->f(I)Lcom/google/googlenav/friend/bg;

    :cond_2
    invoke-virtual {p1}, LaH/h;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, LaH/h;->b()Lo/D;

    move-result-object v2

    invoke-virtual {v2}, Lo/D;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->c(I)Lcom/google/googlenav/friend/bg;

    :cond_3
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v2

    invoke-virtual {v2}, LaM/f;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    :cond_4
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v1

    invoke-virtual {v2, v1}, Law/h;->c(Law/g;)V

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 1

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;)Z

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->d:LaH/h;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->b:LaH/h;

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/k;->c:Lcom/google/googlenav/ai;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->e:Z

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    invoke-virtual {p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v1, p2, v0}, Lcom/google/googlenav/mylocationnotifier/a;->a(Ljava/util/List;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/mylocationnotifier/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    iget-object v0, v0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    iget-object v0, v0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->setShouldDeactivateOnDetach(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->l()V

    :goto_0
    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->h()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaH/h;)Z
    .locals 6

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    sget v2, Lcom/google/googlenav/mylocationnotifier/k;->a:I

    if-ge v0, v2, :cond_7

    invoke-static {p1}, LaH/h;->c(Landroid/location/Location;)I

    move-result v0

    sget v2, Lcom/google/googlenav/mylocationnotifier/k;->f:I

    if-ge v0, v2, :cond_6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->b:LaH/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->b:LaH/h;

    invoke-virtual {p1, v0}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v0

    sget v3, Lcom/google/googlenav/mylocationnotifier/k;->g:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    const-string v0, "o"

    invoke-virtual {p0, p1, v2, p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;Ljava/lang/String;Lcom/google/googlenav/friend/bf;Ljava/lang/String;)V

    :cond_2
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    instance-of v0, v0, Lcom/google/googlenav/mylocationnotifier/i;

    if-eqz v0, :cond_4

    const-string v0, "m"

    invoke-virtual {p0, p1, v2, p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;Ljava/lang/String;Lcom/google/googlenav/friend/bf;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    instance-of v0, v0, Lcom/google/googlenav/mylocationnotifier/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    check-cast v0, Lcom/google/googlenav/mylocationnotifier/l;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/l;->a()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->d:LaH/h;

    invoke-virtual {p1, v0}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v0

    sget v3, Lcom/google/googlenav/mylocationnotifier/k;->g:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    :cond_5
    const-string v0, "m"

    invoke-virtual {p0, p1, v2, p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;Ljava/lang/String;Lcom/google/googlenav/friend/bf;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-wide v2, p0, Lcom/google/googlenav/mylocationnotifier/k;->h:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/googlenav/mylocationnotifier/k;->h:J

    goto :goto_1

    :cond_9
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/googlenav/mylocationnotifier/k;->h:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/16 v0, 0x2c9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->i()Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    iget-object v0, v0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    iget-object v0, v0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->setShouldDeactivateOnDetach(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    :cond_1
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->h:J

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0, p0}, LaH/m;->b(LaH/A;)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(Lcom/google/googlenav/mylocationnotifier/a;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->k()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->j:Lcom/google/googlenav/mylocationnotifier/a;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/a;->l()V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->c:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/mylocationnotifier/l;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/google/googlenav/mylocationnotifier/l;-><init>(Ljava/util/List;Lcom/google/googlenav/mylocationnotifier/k;Ljava/lang/String;Z)V

    new-instance v1, Lcom/google/googlenav/mylocationnotifier/d;

    iget-object v2, p0, Lcom/google/googlenav/mylocationnotifier/k;->c:Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v0, v3}, Lcom/google/googlenav/mylocationnotifier/d;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/mylocationnotifier/k;Lcom/google/googlenav/mylocationnotifier/l;Z)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/mylocationnotifier/k;->a(Lcom/google/googlenav/mylocationnotifier/a;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/mylocationnotifier/i;

    invoke-direct {v0, p0, v3}, Lcom/google/googlenav/mylocationnotifier/i;-><init>(Lcom/google/googlenav/mylocationnotifier/k;Z)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(Lcom/google/googlenav/mylocationnotifier/a;)V

    goto :goto_0

    :cond_1
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/mylocationnotifier/k;->a(LaH/h;)Z

    goto :goto_0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->b:LaH/h;

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->c:Lcom/google/googlenav/ai;

    return-void
.end method

.method i()Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/k;->i:Lcom/google/googlenav/ui/s;

    return-object v0
.end method
