.class public abstract Lcom/google/googlenav/mylocationnotifier/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static a:Lcom/google/android/maps/MapsActivity;


# instance fields
.field protected final b:Lcom/google/googlenav/mylocationnotifier/k;

.field protected final c:Lcom/google/googlenav/ui/s;

.field protected d:Landroid/widget/ListPopupWindow;

.field protected e:Lcom/google/googlenav/mylocationnotifier/HeaderView;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/mylocationnotifier/k;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/mylocationnotifier/a;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {p1}, Lcom/google/googlenav/mylocationnotifier/k;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->c:Lcom/google/googlenav/ui/s;

    return-void
.end method

.method public static a(Lcom/google/android/maps/MapsActivity;)V
    .locals 0

    sput-object p0, Lcom/google/googlenav/mylocationnotifier/a;->a:Lcom/google/android/maps/MapsActivity;

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;I)Landroid/widget/TextView;
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method abstract a(Ljava/util/List;Ljava/lang/String;)V
.end method

.method protected abstract b()V
.end method

.method protected abstract c()Landroid/widget/ListAdapter;
.end method

.method protected abstract d()Landroid/widget/AdapterView$OnItemClickListener;
.end method

.method e()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method h()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->b()V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->b:Lcom/google/googlenav/mylocationnotifier/k;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->setMyLocationNotifierManager(Lcom/google/googlenav/mylocationnotifier/k;)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const v1, 0x7f020222

    invoke-virtual {v0, v1, v6, v6, v5}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/widget/ListPopupWindow;

    sget-object v2, Lcom/google/googlenav/mylocationnotifier/a;->a:Lcom/google/android/maps/MapsActivity;

    invoke-direct {v1, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->d()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->f()V

    return-void
.end method

.method i()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->l()V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/mylocationnotifier/b;

    invoke-direct {v2, p0}, Lcom/google/googlenav/mylocationnotifier/b;-><init>(Lcom/google/googlenav/mylocationnotifier/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {p0}, Lcom/google/googlenav/mylocationnotifier/a;->g()V

    return-void
.end method

.method j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->e:Lcom/google/googlenav/mylocationnotifier/HeaderView;

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/HeaderView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/mylocationnotifier/a;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/mylocationnotifier/c;

    invoke-direct {v2, p0}, Lcom/google/googlenav/mylocationnotifier/c;-><init>(Lcom/google/googlenav/mylocationnotifier/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method l()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/mylocationnotifier/a;->d:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_0
    return-void
.end method
