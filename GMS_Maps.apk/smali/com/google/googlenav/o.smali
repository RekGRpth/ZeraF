.class public Lcom/google/googlenav/o;
.super Law/b;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lcom/google/googlenav/h;

.field private final c:Ljava/util/List;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/googlenav/r;

.field private final f:J

.field private g:Lcom/google/googlenav/a;

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/google/googlenav/r;)V
    .locals 2

    invoke-direct {p0}, Law/b;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    iput-object p2, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    iput-object p3, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    iput-object p5, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/o;->f:J

    return-void
.end method

.method private declared-synchronized n()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/o;->i:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ib;->u:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lcom/google/googlenav/o;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    const/4 v1, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ib;->v:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/o;->h:Z

    iget-boolean v0, p0, Lcom/google/googlenav/o;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/a;

    invoke-direct {v2, v0}, Lcom/google/googlenav/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v2, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x65

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 5

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    iget-boolean v1, p0, Lcom/google/googlenav/o;->h:Z

    iget-object v2, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    iget-object v3, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    iget-object v4, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/r;->a(ZLcom/google/googlenav/a;Lcom/google/googlenav/h;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v2, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbO/t;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/googlenav/o;->f:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    invoke-virtual {v2}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/google/googlenav/q;->a(Ljava/util/List;)Lcom/google/googlenav/q;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0}, Lcom/google/googlenav/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/o;->i:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/googlenav/o;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    return-object v1
.end method

.method protected j()Lcom/google/googlenav/s;
    .locals 2

    new-instance v0, Lcom/google/googlenav/s;

    invoke-direct {v0}, Lcom/google/googlenav/s;-><init>()V

    iget-boolean v1, p0, Lcom/google/googlenav/o;->h:Z

    iput-boolean v1, v0, Lcom/google/googlenav/s;->a:Z

    iget-object v1, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    iput-object v1, v0, Lcom/google/googlenav/s;->b:Lcom/google/googlenav/a;

    iget-object v1, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    iput-object v1, v0, Lcom/google/googlenav/s;->c:Lcom/google/googlenav/h;

    iget-object v1, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    iput-object v1, v0, Lcom/google/googlenav/s;->d:Ljava/util/List;

    return-object v0
.end method

.method protected synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/o;->j()Lcom/google/googlenav/s;

    move-result-object v0

    return-object v0
.end method
