.class public Lcom/google/googlenav/intersectionexplorer/GestureOverlay;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/intersectionexplorer/b;

.field private b:D

.field private c:D

.field private d:Lcom/google/googlenav/intersectionexplorer/a;

.field private e:I

.field private f:Z

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->e:I

    iput-boolean v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    iput v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/intersectionexplorer/b;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->e:I

    iput-boolean v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    iput-object p2, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    iput v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->i:I

    return-void
.end method

.method private a(DD)Lcom/google/googlenav/intersectionexplorer/a;
    .locals 11

    iget v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->e:I

    int-to-float v0, v0

    const-wide v1, 0x3fd0c152382d7365L

    iget-boolean v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->g:I

    int-to-double v3, v3

    cmpg-double v3, p1, v3

    if-gez v3, :cond_0

    iget-wide v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->b:D

    iget v5, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->g:I

    int-to-double v5, v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_0

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->j:Lcom/google/googlenav/intersectionexplorer/a;

    :goto_0
    return-object v0

    :cond_0
    iget v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->h:I

    int-to-double v3, v3

    cmpl-double v3, p1, v3

    if-lez v3, :cond_1

    iget-wide v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->b:D

    iget v5, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->h:I

    int-to-double v5, v5

    cmpl-double v3, v3, v5

    if-lez v3, :cond_1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->k:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    :cond_2
    iget-wide v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->b:D

    sub-double/2addr v3, p1

    iget-wide v5, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->c:D

    sub-double/2addr v5, p3

    mul-double v7, v3, v3

    mul-double v9, v5, v5

    add-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    float-to-double v9, v0

    cmpg-double v0, v7, v9

    if-gez v0, :cond_3

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->e:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_3
    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    const-wide/16 v5, 0x0

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_4

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->d:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_4
    const-wide v5, 0x3fe921fb54442d18L

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_5

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->a:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_5
    const-wide v5, 0x3ff921fb54442d18L

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_6

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->b:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_6
    const-wide v5, 0x4002d97c7f3321d2L

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_7

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->c:Lcom/google/googlenav/intersectionexplorer/a;

    goto :goto_0

    :cond_7
    const-wide v5, -0x3ffd268380ccde2eL

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_8

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->i:Lcom/google/googlenav/intersectionexplorer/a;

    goto/16 :goto_0

    :cond_8
    const-wide v5, -0x4006de04abbbd2e8L

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_9

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->h:Lcom/google/googlenav/intersectionexplorer/a;

    goto/16 :goto_0

    :cond_9
    const-wide v5, -0x4016de04abbbd2e8L

    sub-double v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    cmpg-double v0, v5, v1

    if-gez v0, :cond_a

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->g:Lcom/google/googlenav/intersectionexplorer/a;

    goto/16 :goto_0

    :cond_a
    const-wide v5, 0x400921fb54442d18L

    sub-double/2addr v5, v1

    cmpl-double v0, v3, v5

    if-gtz v0, :cond_b

    const-wide v5, -0x3ff6de04abbbd2e8L

    add-double v0, v5, v1

    cmpg-double v0, v3, v0

    if-gez v0, :cond_c

    :cond_b
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->f:Lcom/google/googlenav/intersectionexplorer/a;

    goto/16 :goto_0

    :cond_c
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->l:Lcom/google/googlenav/intersectionexplorer/a;

    goto/16 :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->i:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->i:I

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sget-object v3, Lcom/google/googlenav/intersectionexplorer/a;->l:Lcom/google/googlenav/intersectionexplorer/a;

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    float-to-double v3, v1

    float-to-double v1, v2

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a(DD)Lcom/google/googlenav/intersectionexplorer/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->l:Lcom/google/googlenav/intersectionexplorer/a;

    if-ne v1, v2, :cond_4

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    :cond_0
    :goto_0
    return v5

    :sswitch_0
    float-to-double v3, v1

    iput-wide v3, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->b:D

    float-to-double v2, v2

    iput-wide v2, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->c:D

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a()I

    move-result v0

    div-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->g:I

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a()I

    move-result v2

    div-int/lit8 v2, v2, 0x7

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->h:I

    iget v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->g:I

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->j:Lcom/google/googlenav/intersectionexplorer/a;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    iput-boolean v5, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    invoke-interface {v0, v1}, Lcom/google/googlenav/intersectionexplorer/b;->a(Lcom/google/googlenav/intersectionexplorer/a;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->h:I

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->k:Lcom/google/googlenav/intersectionexplorer/a;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    iput-boolean v5, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/a;->e:Lcom/google/googlenav/intersectionexplorer/a;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->f:Z

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    float-to-double v3, v1

    float-to-double v1, v2

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a(DD)Lcom/google/googlenav/intersectionexplorer/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->l:Lcom/google/googlenav/intersectionexplorer/a;

    if-ne v1, v2, :cond_3

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    invoke-interface {v0, v1}, Lcom/google/googlenav/intersectionexplorer/b;->c(Lcom/google/googlenav/intersectionexplorer/a;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->d:Lcom/google/googlenav/intersectionexplorer/a;

    invoke-interface {v0, v1}, Lcom/google/googlenav/intersectionexplorer/b;->b(Lcom/google/googlenav/intersectionexplorer/a;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x9 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public setGestureListener(Lcom/google/googlenav/intersectionexplorer/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->a:Lcom/google/googlenav/intersectionexplorer/b;

    return-void
.end method

.method public setMinimumRadius(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->e:I

    return-void
.end method

.method public setTestWidth(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->i:I

    return-void
.end method
