.class Lcom/google/commerce/wireless/topiary/e;
.super Lcom/google/commerce/wireless/topiary/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/commerce/wireless/topiary/d;


# direct methods
.method constructor <init>(Lcom/google/commerce/wireless/topiary/d;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/e;->b:Lcom/google/commerce/wireless/topiary/d;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/e;->a:Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/e;->a:Landroid/content/Intent;

    const-string v1, "topiary.EXTRA_TOKEN_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/e;->b:Lcom/google/commerce/wireless/topiary/d;

    invoke-static {v1}, Lcom/google/commerce/wireless/topiary/d;->a(Lcom/google/commerce/wireless/topiary/d;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/e;->b:Lcom/google/commerce/wireless/topiary/d;

    invoke-static {v2}, Lcom/google/commerce/wireless/topiary/d;->a(Lcom/google/commerce/wireless/topiary/d;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    monitor-enter v2

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
