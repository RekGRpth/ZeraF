.class public Lcom/google/commerce/wireless/topiary/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/accounts/Account;

.field private final b:Lcom/google/commerce/wireless/topiary/D;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/f;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    return-void
.end method

.method private b(Lcom/google/commerce/wireless/topiary/T;Z)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/g;

    invoke-interface {v0}, Lcom/google/commerce/wireless/topiary/g;->a()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/g;->a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z

    move-result v0

    or-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/g;

    invoke-interface {v0}, Lcom/google/commerce/wireless/topiary/g;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/g;->a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z

    goto :goto_2

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->d(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .locals 4

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    if-nez v0, :cond_0

    const-string v0, "AuthState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/commerce/wireless/topiary/W;

    invoke-direct {v0, p1}, Lcom/google/commerce/wireless/topiary/W;-><init>(Lcom/google/commerce/wireless/topiary/T;)V

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private e(Lcom/google/commerce/wireless/topiary/T;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/D;->e()Lcom/google/commerce/wireless/topiary/B;

    move-result-object v2

    iget-object v3, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/commerce/wireless/topiary/B;->a(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->f(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private f(Lcom/google/commerce/wireless/topiary/T;)Z
    .locals 7

    const/4 v1, 0x1

    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/U;

    iget v3, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    if-lez v3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v4

    sget-object v5, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-eq v4, v5, :cond_1

    const-string v2, "AuthState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cookie "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/U;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " requires service "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/google/commerce/wireless/topiary/T;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to be authenticated in this session"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/W;->b()J

    move-result-wide v3

    iget v5, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const-string v2, "AuthState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cookie "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/commerce/wireless/topiary/U;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " allows max age of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sec. but it has been "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " secs."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/W;->a:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v1

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/W;->a(J)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v1

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->e(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    goto :goto_0
.end method

.method a(Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/T;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    :cond_0
    return-void
.end method

.method a(Lcom/google/commerce/wireless/topiary/T;Z)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AuthState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing auth completion for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/T;Z)V

    return-void

    :cond_0
    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    goto :goto_0
.end method

.method a(Lcom/google/commerce/wireless/topiary/g;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->c:Lcom/google/commerce/wireless/topiary/V;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->e(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v2}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lcom/google/commerce/wireless/topiary/T;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->f(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->e()Lcom/google/commerce/wireless/topiary/B;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/commerce/wireless/topiary/T;->d:Z

    iget-object v2, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/B;->a(ZLjava/util/List;)V

    :cond_0
    return-void
.end method

.method b(Lcom/google/commerce/wireless/topiary/g;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public c()Z
    .locals 3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
