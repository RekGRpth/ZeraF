.class public Lcom/google/commerce/wireless/topiary/w;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field a:Lcom/google/commerce/wireless/topiary/HybridWebView;


# direct methods
.method public constructor <init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/w;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/w;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(Ljava/lang/String;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/w;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/w;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/w;->a:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
