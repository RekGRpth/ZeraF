.class public Lcom/google/commerce/wireless/topiary/HybridWebView;
.super Landroid/webkit/WebView;
.source "SourceFile"

# interfaces
.implements Lcom/google/commerce/wireless/topiary/g;


# static fields
.field private static o:Ljava/lang/reflect/Method;


# instance fields
.field a:Lcom/google/commerce/wireless/topiary/v;

.field private b:Ljava/lang/String;

.field private final c:[I

.field private d:I

.field private e:Lcom/google/commerce/wireless/topiary/D;

.field private f:Lcom/google/commerce/wireless/topiary/f;

.field private g:Lcom/google/commerce/wireless/topiary/T;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/commerce/wireless/topiary/T;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Landroid/os/Handler;

.field private m:Ljava/util/Hashtable;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x0

    sput-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    :try_start_0
    const-class v0, Landroid/webkit/WebView;

    const-string v1, "removeJavascriptInterface"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    const-string v0, "HybridWebView"

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iput v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-direct {p0, p1, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILjava/lang/String;I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne v0, p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-preloading "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aput v3, v0, v3

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setPreloading(Z)V

    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setScrollBarStyle(I)V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    const-wide/32 v1, 0x800000

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "appcache"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    if-nez p2, :cond_1

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->e()Landroid/webkit/WebViewClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->f()Landroid/webkit/WebChromeClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/commerce/wireless/topiary/T;)V
    .locals 6

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting authentication for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v1, p1, v0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/T;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->d()Lcom/google/commerce/wireless/topiary/a;

    move-result-object v2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/f;->a()Landroid/accounts/Account;

    move-result-object v3

    iget-object v5, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v0, Lcom/google/commerce/wireless/topiary/p;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/commerce/wireless/topiary/p;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/a;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/p;->b()V

    return-void
.end method

.method private a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processing token for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_2

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p1, v3}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    const/4 v0, 0x2

    const-string v1, ""

    invoke-direct {p0, v0, v1, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-ne v0, p1, :cond_3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/T;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Loading token into web view"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    invoke-virtual {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not loading token since pending service is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "null"

    goto :goto_1
.end method

.method private a(Z)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v1, v0, p1}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    :cond_1
    return-void
.end method

.method private a(ZILjava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v2, p1}, Lcom/google/commerce/wireless/topiary/T;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iget-boolean v2, v2, Lcom/google/commerce/wireless/topiary/T;->c:Z

    if-eqz v2, :cond_0

    if-eqz p2, :cond_3

    :cond_0
    move v3, v0

    :goto_0
    if-eqz p2, :cond_4

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/commerce/wireless/topiary/h;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Auth speeedbump detected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-nez v3, :cond_2

    if-eqz v2, :cond_5

    :cond_2
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Detected auth completion for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/r;

    invoke-direct {v2, p0}, Lcom/google/commerce/wireless/topiary/r;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_2
    return v0

    :cond_3
    move v3, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not an auth completion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_2
.end method

.method private b(Lcom/google/commerce/wireless/topiary/T;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "auth ok for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Now loading "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "auth ok for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". No url to load, so done."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Cleanup pending state"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Z)V

    invoke-virtual {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No account so can\'t do auth redirects "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/google/commerce/wireless/topiary/h;->b(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not an auth redirect: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Potential auth redirect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "We are in the middle of auth - let redirects happen"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "passive"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "false"

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_8

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/f;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Passive ServiceLogin likely to succeed - letting it proceed"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to process auth redirect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "service"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No service specified - picking one based on param "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v3, v2}, Lcom/google/commerce/wireless/topiary/f;->a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    :cond_5
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    if-nez v2, :cond_6

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "No service found for auth - letting redirect happen..."

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Will do auth for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    :cond_7
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "After auth will load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;)V

    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v2, v1

    goto/16 :goto_1
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    if-eqz v0, :cond_0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v1, v1, v2

    aput v1, v0, v3

    iput v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    :cond_1
    iget v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "State history too long"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    :cond_2
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aput p1, v0, v1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "State transition "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceivedError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/16 v1, 0x65

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    invoke-direct {p0, v0, p2, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ILjava/lang/String;I)V

    return-void

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xe
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method a(Lcom/google/commerce/wireless/topiary/D;Landroid/accounts/Account;)V
    .locals 1

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {p1, p2}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/g;)V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PageStarted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageStarted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->j:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    :cond_0
    iput-boolean v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    const/4 v0, -0x1

    invoke-direct {p0, v3, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->b(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUrlAuthenticated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUrlAuthenticated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/u;->a:[I

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/X;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p2, p1}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "will do auth for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decided not to do auth for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", policy was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/V;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_0

    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    const/4 v0, -0x1

    invoke-direct {p0, v3, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "auth in progress for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "already authenticated for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    return v0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Preloading view is abandoning load"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got auth, will now load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/s;

    invoke-direct {v2, p0, p1}, Lcom/google/commerce/wireless/topiary/s;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Auth failed or cancelled, will re-load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Auth failed or cancelled in this web view - exiting"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x1e

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    if-gt v0, v2, :cond_1

    if-lez v0, :cond_0

    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/lang/String;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ShouldOverride?: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v2, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Overriding loading of: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v2, p1}, Lcom/google/commerce/wireless/topiary/D;->a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Overrideing load of : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " will do auth load for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Detected non auth redirect, setting desiredUrl to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProceedingToLoad: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0
.end method

.method public c()I
    .locals 2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    return v0
.end method

.method c(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageFinished: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PageFinished: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageFinished - detected auth completion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->c(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/commerce/wireless/topiary/T;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clearing pending load for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-nez v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->g()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOT clearing pending load for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", desired = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public c(I)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    if-gt v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public destroy()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Destroying..."

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->stopLoading()V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->h()V

    invoke-direct {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->freeMemory()V

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->destroyDrawingCache()V

    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p0}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/g;)V

    :cond_1
    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Destroyed"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected e()Landroid/webkit/WebViewClient;
    .locals 1

    new-instance v0, Lcom/google/commerce/wireless/topiary/w;

    invoke-direct {v0, p0}, Lcom/google/commerce/wireless/topiary/w;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-object v0
.end method

.method protected f()Landroid/webkit/WebChromeClient;
    .locals 1

    new-instance v0, Lcom/google/commerce/wireless/topiary/o;

    invoke-direct {v0, p0}, Lcom/google/commerce/wireless/topiary/o;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-object v0
.end method

.method g()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Load finished - showing WebView"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->clearHistory()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/t;

    invoke-direct {v2, p0, v0}, Lcom/google/commerce/wireless/topiary/t;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring progress done "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, " no service"

    goto :goto_1
.end method

.method h()V
    .locals 2

    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    return-void
.end method

.method public setHybridWebViewUiClient(Lcom/google/commerce/wireless/topiary/v;)V
    .locals 0

    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    return-void
.end method

.method public setPreloading(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
