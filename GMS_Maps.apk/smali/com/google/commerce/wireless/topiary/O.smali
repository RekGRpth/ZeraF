.class public abstract Lcom/google/commerce/wireless/topiary/O;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/commerce/wireless/topiary/J;

.field private final b:[Lcom/google/commerce/wireless/topiary/M;

.field private final c:Landroid/accounts/Account;


# direct methods
.method protected constructor <init>(Lcom/google/commerce/wireless/topiary/R;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_0
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/commerce/wireless/topiary/i;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->g()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GoogleLogin auth="

    invoke-direct {v0, v1, v2, v3}, Lcom/google/commerce/wireless/topiary/i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/R;->a(Lcom/google/commerce/wireless/topiary/M;)Lcom/google/commerce/wireless/topiary/R;

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/commerce/wireless/topiary/Z;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/Z;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/R;->a(Lcom/google/commerce/wireless/topiary/M;)Lcom/google/commerce/wireless/topiary/R;

    :cond_1
    new-instance v0, Lcom/google/commerce/wireless/topiary/J;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->g()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/google/commerce/wireless/topiary/J;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/O;->a:Lcom/google/commerce/wireless/topiary/J;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->h()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/commerce/wireless/topiary/M;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/commerce/wireless/topiary/M;

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/O;->b:[Lcom/google/commerce/wireless/topiary/M;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->c()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/O;->c:Landroid/accounts/Account;

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/commerce/wireless/topiary/i;

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->g()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/R;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Bearer "

    invoke-direct {v0, v1, v2, v3}, Lcom/google/commerce/wireless/topiary/i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/R;->a(Lcom/google/commerce/wireless/topiary/M;)Lcom/google/commerce/wireless/topiary/R;

    goto :goto_1

    :cond_3
    move-object p2, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;[BLbH/p;)LbH/p;
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/google/commerce/wireless/topiary/N;->a:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p2, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/O;->a:Lcom/google/commerce/wireless/topiary/J;

    invoke-virtual {v1, p2, p1}, Lcom/google/commerce/wireless/topiary/J;->a(Lcom/google/commerce/wireless/topiary/K;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Lcom/google/commerce/wireless/topiary/n; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    :try_start_1
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/O;->a:Lcom/google/commerce/wireless/topiary/J;

    invoke-virtual {v1, p2, v2, p3}, Lcom/google/commerce/wireless/topiary/J;->a(Lcom/google/commerce/wireless/topiary/K;Ljava/net/HttpURLConnection;[B)[B

    move-result-object v1

    invoke-interface {p4}, LbH/p;->j()LbH/q;

    move-result-object v3

    invoke-interface {v3, v1}, LbH/q;->b([B)LbH/q;

    move-result-object v1

    invoke-interface {v1}, LbH/q;->f()LbH/p;
    :try_end_1
    .catch Lcom/google/commerce/wireless/topiary/n; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v2, v0

    :goto_1
    :try_start_2
    invoke-virtual {v1}, Lcom/google/commerce/wireless/topiary/n;->a()I

    move-result v3

    const/16 v4, 0x191

    if-ne v3, v4, :cond_1

    sget-object v1, Lcom/google/commerce/wireless/topiary/N;->f:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p2, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v3, Lcom/google/commerce/wireless/topiary/N;->c:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p2, v3, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0

    :catch_1
    move-exception v1

    move-object v2, v0

    :goto_4
    :try_start_4
    sget-object v3, Lcom/google/commerce/wireless/topiary/Q;->a:[I

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/K;->d()Lcom/google/commerce/wireless/topiary/N;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/commerce/wireless/topiary/N;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v3, Lcom/google/commerce/wireless/topiary/N;->e:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p2, v3, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :pswitch_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/google/commerce/wireless/topiary/K;)Z
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/google/commerce/wireless/topiary/N;->b:Lcom/google/commerce/wireless/topiary/N;

    invoke-virtual {p1, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/N;)V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->a()Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/O;->c:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/O;->c:Landroid/accounts/Account;

    invoke-virtual {p1, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/K;

    :cond_0
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/O;->b:[Lcom/google/commerce/wireless/topiary/M;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lcom/google/commerce/wireless/topiary/M;->a(Lcom/google/commerce/wireless/topiary/K;)V

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->d()Lcom/google/commerce/wireless/topiary/N;

    move-result-object v4

    sget-object v5, Lcom/google/commerce/wireless/topiary/N;->b:Lcom/google/commerce/wireless/topiary/N;

    if-eq v4, v5, :cond_1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private b(Lcom/google/commerce/wireless/topiary/K;)V
    .locals 4

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/O;->b:[Lcom/google/commerce/wireless/topiary/M;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/google/commerce/wireless/topiary/M;->b(Lcom/google/commerce/wireless/topiary/K;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;LbH/p;LbH/p;)LbH/p;
    .locals 3

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/O;->a(Lcom/google/commerce/wireless/topiary/K;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p3}, LbH/p;->r()[B

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/commerce/wireless/topiary/O;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;[BLbH/p;)LbH/p;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/O;->b(Lcom/google/commerce/wireless/topiary/K;)V

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/K;->d()Lcom/google/commerce/wireless/topiary/N;

    move-result-object v1

    sget-object v2, Lcom/google/commerce/wireless/topiary/N;->a:Lcom/google/commerce/wireless/topiary/N;

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/K;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/commerce/wireless/topiary/O;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;LbH/p;LbH/p;)LbH/p;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;LbH/p;LbH/p;Lcom/google/commerce/wireless/topiary/I;)V
    .locals 7

    new-instance v0, Lcom/google/commerce/wireless/topiary/P;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/commerce/wireless/topiary/P;-><init>(Lcom/google/commerce/wireless/topiary/O;Ljava/lang/String;Lcom/google/commerce/wireless/topiary/K;LbH/p;LbH/p;Lcom/google/commerce/wireless/topiary/I;)V

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/P;->b()V

    return-void
.end method
