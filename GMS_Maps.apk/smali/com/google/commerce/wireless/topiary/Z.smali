.class public Lcom/google/commerce/wireless/topiary/Z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/commerce/wireless/topiary/M;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/Z;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/commerce/wireless/topiary/K;)V
    .locals 2

    const-string v0, "X-Security-Token"

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/Z;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/commerce/wireless/topiary/K;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/google/commerce/wireless/topiary/K;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->d()Lcom/google/commerce/wireless/topiary/N;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/N;->c:Lcom/google/commerce/wireless/topiary/N;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/K;->e()Ljava/lang/Exception;

    move-result-object v0

    instance-of v1, v0, Lcom/google/commerce/wireless/topiary/n;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/commerce/wireless/topiary/n;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/n;->a()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    const-string v0, "X-Security-Token"

    invoke-virtual {p1, v0}, Lcom/google/commerce/wireless/topiary/K;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/Z;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/Z;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/commerce/wireless/topiary/K;->a(Lcom/google/commerce/wireless/topiary/M;)V

    :cond_0
    return-void
.end method
