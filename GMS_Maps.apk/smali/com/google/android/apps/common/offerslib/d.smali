.class public Lcom/google/android/apps/common/offerslib/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/net/Uri;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Lcom/google/android/apps/common/offerslib/t;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/apps/common/offerslib/e;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.nfc"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/common/offerslib/d;->e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/location/Location;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public j(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .locals 0

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->t()V

    return-void
.end method
