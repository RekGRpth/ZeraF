.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private b:I

.field private c:I

.field private d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

.field private e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;ILcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c:I

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput-wide p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    return v0
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    return-void
.end method

.method public d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c:I

    return v0
.end method

.method public f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f:J

    return-wide v0
.end method
