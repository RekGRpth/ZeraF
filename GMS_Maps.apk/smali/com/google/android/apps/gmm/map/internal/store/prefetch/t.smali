.class public abstract Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Li/f;


# instance fields
.field protected final a:Lcom/google/googlenav/android/F;

.field protected final b:Lr/I;

.field protected final c:Landroid/net/wifi/WifiManager$WifiLock;

.field protected final d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field protected final e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

.field private volatile f:Z

.field private final g:Li/g;

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->a:Lcom/google/googlenav/android/F;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b:Lr/I;

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->c:Landroid/net/wifi/WifiManager$WifiLock;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/v;

    invoke-interface {v0}, Lr/v;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    invoke-interface {v0}, Li/g;->a()V

    :cond_1
    return-void
.end method

.method public a(Lr/v;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    return v0
.end method

.method public abstract c()Z
.end method

.method protected abstract d()Z
.end method

.method public e()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    return-object v0
.end method
