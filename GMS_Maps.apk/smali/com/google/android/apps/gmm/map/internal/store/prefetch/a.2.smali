.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;
.super Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    return-void
.end method

.method private h()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v0}, Lcom/google/googlenav/android/F;->b()I

    move-result v0

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v0}, Lcom/google/googlenav/android/F;->b()I

    move-result v0

    const/16 v1, 0x35

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->av()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public c()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->h()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->b:Lr/I;

    invoke-virtual {v4}, Lr/I;->d()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v5}, Lcom/google/googlenav/android/F;->d()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k()Z

    move-result v6

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-nez v5, :cond_7

    :cond_0
    if-eqz v6, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_3

    const-string v0, "r"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_4

    const-string v0, "b"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v4, :cond_5

    const-string v0, "c"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v5, :cond_6

    const-string v0, "d"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v4, "n"

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j()V

    :cond_1
    :goto_5
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    const-string v0, ""

    goto :goto_3

    :cond_6
    const-string v0, ""

    goto :goto_4

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->j()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v0}, Lcom/google/googlenav/android/F;->c()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v3, "n"

    const-string v4, "e"

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j()V

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->c:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    move v0, v2

    :goto_6
    int-to-long v3, v0

    sget-wide v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_a

    invoke-static {}, Lcom/google/googlenav/android/F;->e()Z

    move-result v3

    if-eqz v3, :cond_9

    move v2, v1

    goto :goto_5

    :cond_9
    const-wide/16 v3, 0x7d0

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v3, "n"

    const-string v4, "w"

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->c:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_5

    :cond_b
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v3, "n"

    const-string v4, "n"

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j()V

    goto :goto_5

    :cond_c
    move v2, v1

    goto :goto_5

    :catch_0
    move-exception v3

    goto :goto_7
.end method

.method protected d()Z
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v0}, Lcom/google/googlenav/android/F;->a()Z

    move-result v3

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/F;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->i()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->b:Lr/I;

    invoke-virtual {v5}, Lr/I;->d()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->a:Lcom/google/googlenav/android/F;

    invoke-virtual {v6}, Lcom/google/googlenav/android/F;->d()Z

    move-result v6

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    if-nez v6, :cond_2

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v3, :cond_4

    const-string v2, "p"

    :goto_1
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_5

    const-string v0, "n"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v4, :cond_6

    const-string v0, "d"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v5, :cond_7

    const-string v0, "c"

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v6, :cond_8

    const-string v0, "b"

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v4, "u"

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const-string v2, ""

    goto :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_2

    :cond_6
    const-string v0, ""

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4

    :cond_8
    const-string v0, ""

    goto :goto_5
.end method
