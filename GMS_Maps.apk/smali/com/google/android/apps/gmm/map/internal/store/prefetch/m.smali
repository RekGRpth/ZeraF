.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->c:I

    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->b:I

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->h:I

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lo/aq;ILo/ap;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    const/4 v3, 0x3

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    if-eq p2, v3, :cond_0

    packed-switch p2, :pswitch_data_0

    :goto_1
    :try_start_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->b:I

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->e()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->b:I

    add-int/2addr v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(II)V

    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    if-ne v2, v3, :cond_2

    if-eq p2, v0, :cond_2

    if-ne p2, v6, :cond_6

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    :cond_2
    if-nez p3, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->g:I

    :goto_3
    if-nez p2, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->a(Lo/aq;)V

    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "n="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "c="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "s="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    const-string v3, "a"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/e;->h()J

    move-result-wide v0

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-static {v2, v3, v4, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILjava/lang/Object;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_0
    :try_start_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->d:I

    goto/16 :goto_1

    :pswitch_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->e:I

    goto/16 :goto_1

    :pswitch_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->f:I

    goto/16 :goto_1

    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->h:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;->i:Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    :cond_5
    const-wide/16 v0, 0x3e8

    goto :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
