.class public final Lcom/google/android/apps/maps/R$styleable;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final GridFlowLayout:[I

.field public static final GridFlowLayout_horizontalGridSize:I = 0x0

.field public static final GridFlowLayout_horizontalGridSpacing:I = 0x2

.field public static final GridFlowLayout_verticalGridSize:I = 0x1

.field public static final GridFlowLayout_verticalGridSpacing:I = 0x3

.field public static final ListPopupSpinnerSdk5:[I

.field public static final ListPopupSpinnerSdk5_arrowOnLeft:I = 0x1

.field public static final ListPopupSpinnerSdk5_dialogResource:I = 0x0

.field public static final MaxWidthForSpinner:[I

.field public static final MaxWidthForSpinner_spinner_max_width:I = 0x0

.field public static final SqueezedLabelView:[I

.field public static final SqueezedLabelView_desiredTextSize:I = 0x0

.field public static final SqueezedLabelView_minTextSize:I = 0x1

.field public static final da_SqueezedLabelView:[I

.field public static final da_SqueezedLabelView_da_desiredTextSize:I = 0x0

.field public static final da_SqueezedLabelView_da_minTextSize:I = 0x1

.field public static final da_TileButton:[I

.field public static final da_TileButton_icon:I = 0x1

.field public static final da_TileButton_title:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->GridFlowLayout:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->ListPopupSpinnerSdk5:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010009

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->MaxWidthForSpinner:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->SqueezedLabelView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->da_SqueezedLabelView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->da_TileButton:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
    .end array-data

    :array_1
    .array-data 4
        0x7f010007
        0x7f010008
    .end array-data

    :array_2
    .array-data 4
        0x7f010000
        0x7f010001
    .end array-data

    :array_3
    .array-data 4
        0x7f01000a
        0x7f01000b
    .end array-data

    :array_4
    .array-data 4
        0x7f01000c
        0x7f01000d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
