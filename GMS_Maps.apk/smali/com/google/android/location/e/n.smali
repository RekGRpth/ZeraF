.class public Lcom/google/android/location/e/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/n$a;
    }
.end annotation


# instance fields
.field private final a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-virtual {v4}, Lcom/google/android/location/e/n$a;->ordinal()I

    move-result v6

    aget v5, v5, v6

    if-lez v5, :cond_0

    iget v4, v4, Lcom/google/android/location/e/n$a;->e:I

    invoke-virtual {v1, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/e/n;->b()V

    return-object v1
.end method

.method public a(Lcom/google/android/location/e/n$a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/e/n$a;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-void
.end method

.method public b()V
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method
