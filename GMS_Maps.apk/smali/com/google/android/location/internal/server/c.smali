.class Lcom/google/android/location/internal/server/c;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;
.implements Lcom/google/android/location/os/real/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/c$a;
    }
.end annotation


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Landroid/location/LocationManager;

.field private final d:Lcom/google/android/location/internal/d;

.field private e:Landroid/database/Cursor;

.field private f:Landroid/content/ContentQueryMap;

.field private g:Lcom/google/android/location/internal/server/c$a;

.field private h:Z

.field private i:Z

.field private final j:Ljava/lang/Object;

.field private k:Lcom/google/android/location/os/e;

.field private final l:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/location/os/real/h;

.field private p:Lcom/google/android/location/b/e;

.field private q:J

.field private final r:Lcom/google/android/location/internal/server/b;

.field private s:Lcom/google/android/location/os/real/g;

.field private t:J


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 5

    const-wide/16 v3, -0x1

    const/16 v2, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->h:Z

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->i:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/location/internal/server/c$1;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/internal/server/c$1;-><init>(Lcom/google/android/location/internal/server/c;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->m:Z

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->n:Z

    iput-wide v3, p0, Lcom/google/android/location/internal/server/c;->q:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    iput-wide v3, p0, Lcom/google/android/location/internal/server/c;->t:J

    iput-object p1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    sget-object v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/internal/e;

    invoke-static {v0, p1}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    new-instance v0, Lcom/google/android/location/internal/server/b;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget v1, v1, Lcom/google/android/location/internal/d;->d:I

    invoke-direct {v0, v1}, Lcom/google/android/location/internal/server/b;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    return-void
.end method

.method private a(Lcom/google/android/location/e/o;)Landroid/location/Location;
    .locals 6

    const-wide v4, 0x416312d000000000L

    new-instance v0, Landroid/location/Location;

    const-string v1, "network"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget v2, v1, Lcom/google/android/location/e/w;->a:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    iget v2, v1, Lcom/google/android/location/e/w;->b:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    int-to-float v1, v1

    const/high16 v2, 0x447a0000

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    iget-wide v1, p1, Lcom/google/android/location/e/o;->e:J

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    return-object v0
.end method

.method private a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p2, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v1, v2, :cond_2

    const-string v1, "networkLocationSource"

    const-string v2, "server"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    if-eq p3, v1, :cond_1

    const-string v1, "travelState"

    invoke-virtual {p3}, Lcom/google/android/location/e/B;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v0

    :cond_2
    if-eqz p4, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v3, "dbgProtoBuf"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_3
    const-string v2, "networkLocationSource"

    const-string v3, "cached"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v1, v2, :cond_4

    const-string v1, "networkLocationType"

    const-string v2, "cell"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v1, v2, :cond_0

    const-string v1, "networkLocationType"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_5

    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget-object v1, v1, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, "levelId"

    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v2, v2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget-object v2, v2, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    const-string v1, "levelNumberE3"

    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v2, v2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v2, v2, Lcom/google/android/location/e/w;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private a(J)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/g;->f()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/location/os/real/h;->G()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gtz v1, :cond_1

    new-instance v1, Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    new-instance v1, Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/internal/server/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->f()V

    return-void
.end method

.method private a(Z)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/b;->a()I

    move-result v1

    if-nez p1, :cond_0

    int-to-long v2, v1

    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->q:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/os/real/h;->a(IZ)V

    :cond_1
    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->t:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    iput-wide v2, p0, Lcom/google/android/location/internal/server/c;->t:J

    :cond_2
    :goto_0
    int-to-long v0, v1

    iput-wide v0, p0, Lcom/google/android/location/internal/server/c;->q:J

    return-void

    :cond_3
    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->t:J

    sub-long v4, v2, v4

    iput-wide v2, p0, Lcom/google/android/location/internal/server/c;->t:J

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v3, 0xa

    if-ge v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long v3, v4, v6

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private final a(Landroid/content/Context;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final b(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_opt_in"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/c;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 4

    sget-object v0, Lcom/google/android/location/internal/e;->a:Lcom/google/android/location/internal/e;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget v2, v0, Lcom/google/android/location/internal/d;->d:I

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget v3, v3, Lcom/google/android/location/internal/d;->d:I

    if-lt v2, v3, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/c;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_1
.end method

.method private f()V
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/google/android/location/internal/server/c;->m:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/c;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v6, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/c;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    move v7, v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    if-nez v7, :cond_0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-eq v3, v7, :cond_0

    iput-boolean v7, p0, Lcom/google/android/location/internal/server/c;->h:Z

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v7}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Z)V

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_a

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-nez v2, :cond_a

    iget-object v8, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v8

    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/location/internal/server/c;->m:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/location/internal/e;->a:Lcom/google/android/location/internal/e;

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/internal/d;->a:Lcom/google/android/location/internal/e;

    sget-object v3, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/internal/e;

    if-eq v2, v3, :cond_8

    move v2, v0

    :goto_2
    sget-object v3, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/internal/e;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/internal/d;->a:Lcom/google/android/location/internal/e;

    sget-object v4, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/internal/e;

    if-ne v3, v4, :cond_9

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->n:Z

    new-instance v0, Lcom/google/android/location/os/real/h;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->k:Lcom/google/android/location/os/e;

    invoke-direct {v0, v1, v3, p0, v2}, Lcom/google/android/location/os/real/h;-><init>(Landroid/content/Context;Lcom/google/android/location/os/e;Lcom/google/android/location/os/real/h$a;Z)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-static {v0}, Lcom/google/android/location/b/e;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->p:Lcom/google/android/location/b/e;

    new-instance v0, Lcom/google/android/location/q;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/h;->D()Lcom/google/android/location/os/h;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/real/h;->C()Lcom/google/android/location/i/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->p:Lcom/google/android/location/b/e;

    iget-boolean v5, p0, Lcom/google/android/location/internal/server/c;->n:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/q;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/b/e;Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    :cond_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    :goto_4
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    if-eqz v7, :cond_3

    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-eq v0, v7, :cond_3

    iput-boolean v7, p0, Lcom/google/android/location/internal/server/c;->h:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, v7}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.location.internal.server.ACTION_RESTARTED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    iget-boolean v0, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->p:Lcom/google/android/location/b/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->p:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->c()V

    :cond_4
    if-nez v6, :cond_5

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_5
    return-void

    :cond_6
    move v6, v1

    goto/16 :goto_0

    :cond_7
    move v7, v1

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_8
    move v2, v1

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto :goto_3

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_a
    if-nez v7, :cond_2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, v7}, Lcom/google/android/location/os/real/h;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/h;->E()V

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/internal/server/c;->q:J

    monitor-exit v1

    goto :goto_4

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v1}, Lcom/google/android/location/internal/server/b;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v2}, Lcom/google/android/location/internal/server/b;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/os/real/h;->a(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)Ljava/lang/Object;
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x2

    :try_start_0
    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/app/PendingIntent;I)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v0, v2, p1, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/app/PendingIntent;I)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/app/PendingIntent;IZ)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    const/4 v2, 0x5

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Lcom/google/android/location/clientlib/NlpActivity;)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lcom/google/android/location/e/t;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/h;->a(Lcom/google/android/location/e/t;)V

    return-void
.end method

.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V
    .locals 9

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/e/o;)Landroid/location/Location;

    move-result-object v0

    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v0, p1, p2, v3}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const/4 v0, 0x0

    invoke-direct {v4, v1, v5, v6, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iput-object v4, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    new-instance v1, Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v4}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v4

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/location/internal/server/c;->a(ZLjava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/b;->a(Lcom/google/android/location/internal/ILocationListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;II)V
    .locals 7

    mul-int/lit16 v0, p4, 0x3e8

    int-to-long v0, v0

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    const/4 v3, -0x1

    if-eq p4, v3, :cond_0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/internal/server/c;->a(J)V

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v5}, Lcom/google/android/location/os/real/g;->f()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v0, v3, v0

    if-gtz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/internal/server/b;

    const/4 v1, 0x5

    invoke-static {p3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/internal/server/b;->a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    monitor-exit v2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/location/os/e;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/internal/server/c;->k:Lcom/google/android/location/os/e;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 10

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NLP-Period is currently "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/location/internal/server/c;->q:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->q:J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/location/internal/server/c;->t:J

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NLP-Period interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const v6, 0x7fffffff

    if-ne v5, v6, :cond_1

    const-string v0, "<no-client>"

    :cond_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", duration was "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GMM CollectionEnabled="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/location/internal/server/c;->n:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_0

    const-string v0, "RealOs stats:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/h;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/h;->a(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method a(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B
    .locals 9

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->n:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz p3, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->Q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez p2, :cond_3

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    :goto_0
    const/4 v2, 0x7

    :try_start_0
    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    const/4 v1, 0x6

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    instance-of v2, p2, Lcom/google/android/location/e/t;

    if-eqz v2, :cond_7

    check-cast p2, Lcom/google/android/location/e/t;

    iget-object v2, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v3, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v2, v3, :cond_6

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    :goto_2
    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v2

    iget-object v4, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-eqz v4, :cond_5

    iget-object v4, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v4, v4, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    if-eqz v4, :cond_5

    invoke-virtual {v4, v0, v2, v3, p4}, Lcom/google/android/location/e/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V

    :cond_5
    iget-object v4, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-eqz v4, :cond_2

    iget-object v4, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v4, v4, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    if-eqz v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_6
    iget-object v2, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v3, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v2, v3, :cond_4

    const/4 v2, 0x6

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2

    :cond_7
    instance-of v2, p2, Lcom/google/android/location/e/o;

    if-eqz v2, :cond_8

    const/4 v2, 0x6

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_8
    instance-of v2, p2, Landroid/location/Location;

    if-eqz v2, :cond_2

    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 8

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->addListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->m:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "network_location_opt_in"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/ContentQueryMap;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    const-string v2, "name"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, p0}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    new-instance v0, Lcom/google/android/location/internal/server/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/internal/server/c$a;-><init>(Lcom/google/android/location/internal/server/c;Lcom/google/android/location/internal/server/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->removeListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->m:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    invoke-virtual {v0, v2}, Landroid/content/ContentQueryMap;->deleteObserver(Ljava/util/Observer;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    invoke-virtual {v0}, Landroid/content/ContentQueryMap;->close()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/os/real/h;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/h;->a(Z)V

    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Lcom/google/android/location/os/g;
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->s:Lcom/google/android/location/os/real/g;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/internal/server/c;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->i:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->b(Landroid/content/Context;)V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->f()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
