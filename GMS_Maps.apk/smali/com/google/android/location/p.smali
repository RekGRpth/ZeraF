.class public Lcom/google/android/location/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final E:Lcom/google/android/location/e/f;


# instance fields
.field A:J

.field final B:Lcom/google/android/location/g;

.field final C:Lcom/google/android/location/e/d;

.field final D:Lcom/google/android/location/h;

.field private F:Lcom/google/android/location/os/g;

.field final a:Lcom/google/android/location/os/i;

.field final b:Lcom/google/android/location/b/f;

.field final c:Lcom/google/android/location/y;

.field final d:Lcom/google/android/location/e/n;

.field final e:Lcom/google/android/location/b/g;

.field final f:Lcom/google/android/location/a/n;

.field g:J

.field h:J

.field final i:Lcom/google/android/location/g/e;

.field j:Lcom/google/android/location/e/t;

.field k:Z

.field l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field m:J

.field n:J

.field o:Lcom/google/android/location/e/E;

.field p:Z

.field q:J

.field r:J

.field s:Lcom/google/android/location/e/f;

.field t:J

.field u:Z

.field v:Z

.field w:J

.field x:Lcom/google/android/location/e/E;

.field y:Lcom/google/android/location/g/l;

.field z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/location/e/f;

    invoke-direct {v0}, Lcom/google/android/location/e/f;-><init>()V

    sput-object v0, Lcom/google/android/location/p;->E:Lcom/google/android/location/e/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g/l;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/y;Lcom/google/android/location/e/d;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lcom/google/android/location/p;->g:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    new-instance v0, Lcom/google/android/location/e/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/e/t;-><init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V

    iput-object v0, p0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->k:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->m:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->n:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->p:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->r:J

    new-instance v0, Lcom/google/android/location/e/f;

    invoke-direct {v0}, Lcom/google/android/location/e/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->t:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->u:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->v:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->w:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->z:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->A:J

    iput-object p1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    iput-object p3, p0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    iput-object p4, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    new-instance v0, Lcom/google/android/location/g/e;

    iget-object v1, p2, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v2, p2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-direct {v0, v1, v2, p4, p1}, Lcom/google/android/location/g/e;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;Lcom/google/android/location/g/l;Lcom/google/android/location/os/i;)V

    iput-object v0, p0, Lcom/google/android/location/p;->i:Lcom/google/android/location/g/e;

    iput-object p6, p0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    iput-object p7, p0, Lcom/google/android/location/p;->c:Lcom/google/android/location/y;

    iput-object p5, p0, Lcom/google/android/location/p;->B:Lcom/google/android/location/g;

    iput-object p8, p0, Lcom/google/android/location/p;->C:Lcom/google/android/location/e/d;

    new-instance v0, Lcom/google/android/location/a/n;

    iget-object v1, p2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-direct {v0, v1, p1}, Lcom/google/android/location/a/n;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V

    iput-object v0, p0, Lcom/google/android/location/p;->f:Lcom/google/android/location/a/n;

    new-instance v0, Lcom/google/android/location/h;

    invoke-direct {v0}, Lcom/google/android/location/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    return-void
.end method

.method static a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)D
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget-object v1, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L

    mul-double/2addr v2, v4

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    int-to-double v4, v0

    sub-double/2addr v2, v4

    iget v0, v1, Lcom/google/android/location/e/w;->c:I

    int-to-double v0, v0

    sub-double v0, v2, v0

    iget-wide v2, p0, Lcom/google/android/location/e/o;->e:J

    iget-wide v4, p1, Lcom/google/android/location/e/o;->e:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_1
    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method static a(JLcom/google/android/location/os/g;Lcom/google/android/location/os/g;)Lcom/google/android/location/e/w;
    .locals 10

    const-wide/32 v4, 0x15f90

    const/4 v1, 0x0

    const-wide v8, 0x416312d000000000L

    if-eqz p2, :cond_5

    invoke-interface {p2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p0, v2

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    move-object v0, p2

    :goto_0
    if-eqz p3, :cond_4

    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p0, v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    invoke-interface {p3}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/location/os/g;->a()F

    move-result v3

    float-to-double v4, v2

    float-to-double v2, v3

    const-wide v6, 0x3feccccccccccccdL

    mul-double/2addr v2, v6

    cmpg-double v2, v4, v2

    if-gez v2, :cond_4

    :cond_0
    :goto_1
    if-nez p3, :cond_1

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_1
    invoke-interface {p3}, Lcom/google/android/location/os/g;->a()F

    move-result v0

    float-to-double v1, v0

    const-wide v3, 0x408f400000000000L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_2

    const/high16 v0, 0x457a0000

    :goto_3
    new-instance v1, Lcom/google/android/location/e/w;

    invoke-interface {p3}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v2

    mul-double/2addr v2, v8

    double-to-int v2, v2

    invoke-interface {p3}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    const/high16 v4, 0x447a0000

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/e/w;-><init>(III)V

    move-object v0, v1

    goto :goto_2

    :cond_2
    float-to-double v0, v0

    const-wide v2, 0x40c3880000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    const v0, 0x47435000

    goto :goto_3

    :cond_3
    const v0, 0x47c35000

    goto :goto_3

    :cond_4
    move-object p3, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/location/e/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2, v1, v2, v3}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->w:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v3, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/p;->c:Lcom/google/android/location/y;

    iget-object v2, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/y;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method

.method private a(J)V
    .locals 4

    iput-wide p1, p0, Lcom/google/android/location/p;->r:J

    iget-wide v0, p0, Lcom/google/android/location/p;->q:J

    const-wide/16 v2, 0x1

    sub-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    return-void
.end method

.method private a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V
    .locals 26

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :cond_0
    if-eqz p5, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->A:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/b/f;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/g/l;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v3}, Lcom/google/android/location/b/g;->g()V

    :cond_2
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->A:J

    :cond_3
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->q:J

    sub-long v20, p1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-nez v3, :cond_2a

    const-wide/16 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v5}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->h:J

    cmp-long v5, p1, v5

    if-ltz v5, :cond_2b

    const/4 v5, 0x1

    move/from16 v19, v5

    :goto_1
    if-eqz v7, :cond_2c

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->i()Z

    move-result v5

    if-eqz v5, :cond_2c

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v5

    sub-long v5, p1, v5

    const-wide/32 v8, 0xafc8

    cmp-long v5, v5, v8

    if-gez v5, :cond_2c

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-eqz v6, :cond_2d

    const-wide/32 v8, 0xafc8

    cmp-long v6, v3, v8

    if-gez v6, :cond_2d

    const/4 v6, 0x1

    move/from16 v18, v6

    :goto_3
    if-eqz v5, :cond_2e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->C:Lcom/google/android/location/e/d;

    invoke-virtual {v6}, Lcom/google/android/location/e/d;->b()Z

    move-result v6

    if-eqz v6, :cond_2e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    invoke-virtual {v6}, Lcom/google/android/location/e/t;->b()Lcom/google/android/location/e/e;

    move-result-object v6

    if-eq v7, v6, :cond_2e

    const/4 v6, 0x1

    move/from16 v17, v6

    :goto_4
    if-eqz v18, :cond_2f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    invoke-virtual {v8}, Lcom/google/android/location/e/t;->a()Lcom/google/android/location/e/E;

    move-result-object v8

    if-eq v6, v8, :cond_2f

    const/4 v6, 0x1

    move/from16 v16, v6

    :goto_5
    if-eqz v19, :cond_30

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->u:Z

    if-eqz v6, :cond_30

    const/4 v6, 0x1

    move v15, v6

    :goto_6
    if-eqz v19, :cond_31

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->v:Z

    if-eqz v6, :cond_31

    if-eqz v16, :cond_4

    const-wide/32 v8, 0xafc8

    sub-long v3, v8, v3

    const-wide/16 v8, 0xc8

    cmp-long v3, v3, v8

    if-gez v3, :cond_31

    :cond_4
    const/4 v3, 0x1

    move v14, v3

    :goto_7
    if-nez v15, :cond_5

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    const-wide/16 v8, -0x1

    cmp-long v3, v3, v8

    if-eqz v3, :cond_32

    if-eqz v7, :cond_5

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v6

    cmp-long v3, v3, v6

    if-lez v3, :cond_32

    :cond_5
    const/4 v3, 0x1

    move v13, v3

    :goto_8
    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->w:J

    const-wide/16 v6, -0x1

    cmp-long v3, v3, v6

    if-eqz v3, :cond_33

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/p;->v:Z

    if-eqz v3, :cond_33

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->w:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    iget-wide v6, v6, Lcom/google/android/location/e/E;->a:J

    cmp-long v3, v3, v6

    if-lez v3, :cond_33

    :cond_6
    const/4 v3, 0x1

    move v12, v3

    :goto_9
    if-eqz v13, :cond_34

    if-nez v15, :cond_7

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    sub-long v3, p1, v3

    const-wide/16 v6, 0x1388

    cmp-long v3, v3, v6

    if-gez v3, :cond_34

    :cond_7
    const/4 v3, 0x1

    move v4, v3

    :goto_a
    if-eqz v12, :cond_35

    if-nez v14, :cond_8

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/p;->w:J

    sub-long v6, p1, v6

    const-wide/16 v8, 0x1388

    cmp-long v3, v6, v8

    if-gez v3, :cond_35

    :cond_8
    const/4 v3, 0x1

    :goto_b
    if-nez v4, :cond_9

    if-eqz v3, :cond_36

    :cond_9
    const/4 v3, 0x1

    :goto_c
    if-nez v17, :cond_a

    if-eqz v16, :cond_b

    :cond_a
    if-eqz v3, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/p;->v:Z

    if-nez v3, :cond_37

    if-eqz v5, :cond_37

    if-nez v4, :cond_37

    :cond_c
    const/4 v3, 0x1

    :goto_d
    if-nez p5, :cond_e

    if-nez p6, :cond_e

    if-nez v19, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/p;->k:Z

    if-eqz v4, :cond_38

    :cond_d
    if-eqz v3, :cond_38

    :cond_e
    const/4 v3, 0x1

    move v11, v3

    :goto_e
    const/4 v3, 0x0

    if-eqz v11, :cond_4d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->F:Lcom/google/android/location/os/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/location/p;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/os/g;)Lcom/google/android/location/e/w;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->i:Lcom/google/android/location/g/e;

    if-eqz v17, :cond_39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v3}, Lcom/google/android/location/e/f;->a()Lcom/google/android/location/e/f;

    move-result-object v3

    move-object v4, v3

    :goto_f
    if-eqz v18, :cond_3a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :goto_10
    move-object/from16 v0, p4

    invoke-virtual {v6, v4, v3, v0, v5}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;Lcom/google/android/location/e/g;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/t;

    move-result-object v3

    move-object v10, v3

    :goto_11
    if-eqz v10, :cond_3b

    iget-object v3, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v3, :cond_3b

    const/4 v3, 0x1

    move v5, v3

    :goto_12
    if-eqz v10, :cond_3c

    iget-object v3, v10, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v3, v3, Lcom/google/android/location/e/c;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_3c

    const/4 v3, 0x1

    move v4, v3

    :goto_13
    if-eqz v10, :cond_3d

    iget-object v3, v10, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v3, v3, Lcom/google/android/location/e/D;->d:Lcom/google/android/location/e/o$a;

    sget-object v6, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    if-ne v3, v6, :cond_3d

    const/4 v3, 0x1

    :goto_14
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v6, :cond_3e

    if-nez v19, :cond_f

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    if-eqz v6, :cond_3e

    :cond_f
    if-eqz v4, :cond_10

    if-nez v17, :cond_11

    :cond_10
    if-eqz v3, :cond_3e

    if-eqz v16, :cond_3e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    invoke-virtual {v3}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-lez v3, :cond_3e

    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/location/h;->a(J)Z

    move-result v3

    if-eqz v3, :cond_3e

    const/4 v3, 0x1

    move v9, v3

    :goto_15
    if-nez v9, :cond_12

    if-eqz v5, :cond_3f

    :cond_12
    const/4 v3, 0x1

    move v8, v3

    :goto_16
    if-eqz v5, :cond_40

    if-eqz v9, :cond_13

    iget-object v3, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/o;J)Z

    move-result v3

    if-eqz v3, :cond_40

    :cond_13
    const/4 v3, 0x1

    :goto_17
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v6

    if-nez v6, :cond_41

    if-eqz v19, :cond_41

    const/4 v4, 0x1

    :goto_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/p;->m:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/p;->q:J

    move-wide/from16 v24, v0

    cmp-long v5, v22, v24

    if-gez v5, :cond_15

    :cond_14
    if-eqz v9, :cond_42

    :cond_15
    const/4 v5, 0x1

    :goto_19
    if-nez v13, :cond_16

    if-nez v12, :cond_16

    if-eqz v5, :cond_43

    :cond_16
    const/4 v5, 0x1

    :goto_1a
    if-eqz v6, :cond_44

    if-nez v19, :cond_44

    const-wide/16 v22, 0x1388

    cmp-long v7, v20, v22

    if-gez v7, :cond_17

    if-nez v5, :cond_44

    :cond_17
    const/4 v5, 0x1

    move v7, v5

    :goto_1b
    if-eqz v6, :cond_45

    if-eqz v19, :cond_45

    if-nez v7, :cond_45

    const/4 v5, 0x1

    :goto_1c
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->k:Z

    if-nez v6, :cond_18

    if-eqz v19, :cond_46

    :cond_18
    if-nez v11, :cond_46

    const/4 v6, 0x1

    :goto_1d
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/location/p;->k:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    if-nez v6, :cond_19

    if-eqz v19, :cond_47

    :cond_19
    if-nez v8, :cond_47

    const/4 v6, 0x1

    :goto_1e
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    if-eqz v4, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/p;->c()J

    move-result-wide p1

    :cond_1a
    if-eqz v5, :cond_1b

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->q:J

    :cond_1b
    if-eqz v19, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->a:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/p;->g:J

    add-long v4, v4, p1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/location/p;->h:J

    :cond_1c
    if-eqz v15, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->i()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->t:J

    :cond_1d
    if-eqz v14, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->n()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->w:J

    :cond_1e
    if-eqz v9, :cond_1f

    if-eqz v17, :cond_48

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    move-object v5, v4

    :goto_1f
    if-eqz v16, :cond_49

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    invoke-virtual {v4}, Lcom/google/android/location/e/E;->a()I

    move-result v4

    if-lez v4, :cond_49

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :goto_20
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->m:J

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/p;->o:Lcom/google/android/location/e/E;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v4, v5}, Lcom/google/android/location/os/i;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1f
    if-eqz v11, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    invoke-virtual {v4}, Lcom/google/android/location/g/l;->a()V

    :cond_20
    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->f:Lcom/google/android/location/a/n;

    if-eqz v18, :cond_4a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :goto_21
    invoke-virtual {v4, v10, v3}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/a/n$b;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v4

    if-eqz v4, :cond_21

    invoke-interface {v4}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/location/p;->n:J

    cmp-long v4, v8, v4

    if-lez v4, :cond_4b

    :cond_21
    :goto_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    iget-object v4, v4, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v4, :cond_22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    iget-object v4, v4, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v5, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    invoke-static {v4, v5}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)D

    move-result-wide v4

    const-wide v8, 0x407544a3d70a3d71L

    cmpl-double v4, v4, v8

    if-lez v4, :cond_22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->d:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    :cond_22
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    iget-object v5, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iput-object v5, v4, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->b:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    if-nez v3, :cond_4c

    const/4 v3, 0x0

    :goto_23
    invoke-interface {v4, v5, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V

    :cond_23
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->h:J

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v5

    if-eqz v5, :cond_24

    if-nez v7, :cond_24

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->q:J

    const-wide/16 v8, 0x1388

    add-long/2addr v5, v8

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    :cond_24
    const-wide v5, 0x7fffffffffffffffL

    cmp-long v5, v3, v5

    if-gez v5, :cond_25

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v6, 0x0

    invoke-interface {v5, v6, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_25
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->z:J

    sub-long v5, p1, v5

    const-wide/32 v8, 0x5265c0

    cmp-long v8, v5, v8

    if-gtz v8, :cond_26

    const-wide/32 v8, 0x2932e0

    cmp-long v5, v5, v8

    if-lez v5, :cond_28

    sub-long v3, v3, p1

    const-wide/16 v5, 0x2710

    cmp-long v3, v3, v5

    if-lez v3, :cond_28

    if-nez v13, :cond_28

    if-nez v12, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v3, :cond_28

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v3

    if-eqz v3, :cond_28

    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v3, v4}, Lcom/google/android/location/b/f;->b(Lcom/google/android/location/os/i;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    invoke-virtual {v3}, Lcom/google/android/location/g/l;->b()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    if-eqz v3, :cond_27

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v3}, Lcom/google/android/location/b/g;->c()V

    :cond_27
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->B:Lcom/google/android/location/g;

    invoke-virtual {v3}, Lcom/google/android/location/g;->b()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->z:J

    :cond_28
    if-eqz v7, :cond_29

    invoke-direct/range {p0 .. p2}, Lcom/google/android/location/p;->a(J)V

    :cond_29
    return-void

    :cond_2a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    iget-wide v3, v3, Lcom/google/android/location/e/E;->a:J

    sub-long v3, p1, v3

    goto/16 :goto_0

    :cond_2b
    const/4 v5, 0x0

    move/from16 v19, v5

    goto/16 :goto_1

    :cond_2c
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_2d
    const/4 v6, 0x0

    move/from16 v18, v6

    goto/16 :goto_3

    :cond_2e
    const/4 v6, 0x0

    move/from16 v17, v6

    goto/16 :goto_4

    :cond_2f
    const/4 v6, 0x0

    move/from16 v16, v6

    goto/16 :goto_5

    :cond_30
    const/4 v6, 0x0

    move v15, v6

    goto/16 :goto_6

    :cond_31
    const/4 v3, 0x0

    move v14, v3

    goto/16 :goto_7

    :cond_32
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_8

    :cond_33
    const/4 v3, 0x0

    move v12, v3

    goto/16 :goto_9

    :cond_34
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_a

    :cond_35
    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_36
    const/4 v3, 0x0

    goto/16 :goto_c

    :cond_37
    const/4 v3, 0x0

    goto/16 :goto_d

    :cond_38
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_e

    :cond_39
    sget-object v3, Lcom/google/android/location/p;->E:Lcom/google/android/location/e/f;

    move-object v4, v3

    goto/16 :goto_f

    :cond_3a
    const/4 v3, 0x0

    goto/16 :goto_10

    :cond_3b
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_12

    :cond_3c
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_13

    :cond_3d
    const/4 v3, 0x0

    goto/16 :goto_14

    :cond_3e
    const/4 v3, 0x0

    move v9, v3

    goto/16 :goto_15

    :cond_3f
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_16

    :cond_40
    const/4 v3, 0x0

    goto/16 :goto_17

    :cond_41
    const/4 v4, 0x0

    goto/16 :goto_18

    :cond_42
    const/4 v5, 0x0

    goto/16 :goto_19

    :cond_43
    const/4 v5, 0x0

    goto/16 :goto_1a

    :cond_44
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_1b

    :cond_45
    const/4 v5, 0x0

    goto/16 :goto_1c

    :cond_46
    const/4 v6, 0x0

    goto/16 :goto_1d

    :cond_47
    const/4 v6, 0x0

    goto/16 :goto_1e

    :cond_48
    const/4 v4, 0x0

    move-object v5, v4

    goto/16 :goto_1f

    :cond_49
    const/4 v4, 0x0

    goto/16 :goto_20

    :cond_4a
    const/4 v3, 0x0

    goto/16 :goto_21

    :cond_4b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->c:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    goto/16 :goto_22

    :cond_4c
    invoke-virtual {v3}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v3

    goto/16 :goto_23

    :cond_4d
    move-object v10, v3

    goto/16 :goto_11
.end method

.method private a(JZZ)V
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/location/p;->u:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean p3, p0, Lcom/google/android/location/p;->u:Z

    iput-boolean p4, p0, Lcom/google/android/location/p;->v:Z

    iget-boolean v2, p0, Lcom/google/android/location/p;->u:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v2, :cond_4

    :cond_1
    :goto_1
    if-eq v0, v1, :cond_2

    if-eqz v1, :cond_5

    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    :cond_2
    :goto_2
    move-object v0, p0

    move-wide v1, p1

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    return-void

    :cond_3
    move v0, v5

    goto :goto_0

    :cond_4
    move v1, v5

    goto :goto_1

    :cond_5
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    goto :goto_2
.end method

.method private a(Lcom/google/android/location/e/o;J)Z
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget v2, v2, Lcom/google/android/location/e/w;->c:I

    const v3, 0x30d40

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-wide v3, p1, Lcom/google/android/location/e/o;->e:J

    invoke-interface {v2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/location/p;->g:J

    const-wide/32 v7, 0xdbba0

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    iget-object v3, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget v3, v3, Lcom/google/android/location/e/w;->c:I

    int-to-float v3, v3

    invoke-interface {v2}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    const/high16 v4, 0x447a0000

    mul-float/2addr v2, v4

    const v4, 0x47c35000

    add-float/2addr v2, v4

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private c()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/p;->r:J

    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    return-wide v0
.end method


# virtual methods
.method public a(IZ)V
    .locals 10

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    int-to-long v1, p1

    const-wide/16 v6, 0x3e8

    mul-long/2addr v1, v6

    iput-wide v1, p0, Lcom/google/android/location/p;->g:J

    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iget-boolean v4, p0, Lcom/google/android/location/p;->u:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v4, :cond_2

    :cond_0
    move v4, v0

    :goto_0
    if-eqz v4, :cond_1

    if-eqz p2, :cond_3

    const-wide/16 v6, 0x0

    :goto_1
    iget-wide v8, p0, Lcom/google/android/location/p;->h:J

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/p;->h:J

    :cond_1
    iget-object v4, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-wide v6, p0, Lcom/google/android/location/p;->g:J

    const-wide/16 v8, 0x4e20

    cmp-long v6, v6, v8

    if-gtz v6, :cond_4

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/location/g/l;->a(Z)V

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    return-void

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    iget-wide v6, p0, Lcom/google/android/location/p;->g:J

    add-long/2addr v6, v1

    goto :goto_1

    :cond_4
    move v0, v5

    goto :goto_2
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .locals 7

    const/4 v5, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->t:J

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/p;->F:Lcom/google/android/location/os/g;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 9

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/location/p;->o:Lcom/google/android/location/e/E;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/location/e/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/e/E;J)Lcom/google/android/location/e/g;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    iget-object v3, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    invoke-virtual {v0, p1, v5, v7, v8}, Lcom/google/android/location/b/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    iput-wide v1, p0, Lcom/google/android/location/p;->n:J

    iget-object v0, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-object v3, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    invoke-virtual {v0, p1, v7, v8}, Lcom/google/android/location/g/l;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    iget-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/location/h;->a(JZ)V

    return-void

    :cond_0
    move v5, v6

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/location/p;->v:Z

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/location/p;->a(JZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ZZI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/h;->a(ZZI)V

    return-void
.end method

.method a()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/p;->q:J

    iget-wide v2, p0, Lcom/google/android/location/p;->r:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/g/l;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/location/p;->u:Z

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/location/p;->a(JZZ)V

    return-void
.end method
