.class Lcom/google/android/location/c/B$d;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/B;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/Handler;",
        "Lcom/google/android/location/c/h",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        "Lcom/google/android/location/c/D$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/B;

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Ljava/util/concurrent/locks/Condition;

.field private volatile d:I

.field private volatile e:Z

.field private final f:Lcom/google/android/location/c/B$a;

.field private final g:Lcom/google/android/location/c/n;

.field private volatile h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/c/B;Landroid/os/Looper;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    iput v3, p0, Lcom/google/android/location/c/B$d;->d:I

    iput-boolean v3, p0, Lcom/google/android/location/c/B$d;->e:Z

    new-instance v0, Lcom/google/android/location/c/B$a;

    const/high16 v1, 0x43960000

    invoke-direct {v0, v1}, Lcom/google/android/location/c/B$a;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    new-instance v0, Lcom/google/android/location/c/n;

    const-wide/32 v1, 0xea60

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/n;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    iput-boolean v3, p0, Lcom/google/android/location/c/B$d;->h:Z

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/location/c/B;->a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v1}, Lcom/google/android/location/c/B;->d(Lcom/google/android/location/c/B;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Will not send to MASF: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    if-eqz v1, :cond_2

    const-string v1, "Too many server errors."

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/location/c/D$a;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v1, "Interrupted by client."

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v1}, Lcom/google/android/location/c/B;->e(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/t;

    move-result-object v1

    invoke-virtual {v1, v2, p0}, Lcom/google/android/location/c/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/h;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/location/c/D$a;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v3, "Can not send to MASF."

    invoke-direct {v1, v4, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    goto :goto_1
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .locals 2

    const/4 v0, 0x2

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/B$d;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    move-result v0

    const-string v1, "There is pending result before handler thread exits."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    return-void
.end method

.method private b()Z
    .locals 1

    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/c/B$d;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/B$d;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "pending requests are not 0 before quiting."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->d:Lcom/google/android/location/c/H;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/H;)V

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/c/s;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v0, v0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v0, v0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->i()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/c/B$d;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "session ID should not be null in asynchronized mode."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .locals 4

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/c/B$d;->d:I

    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    invoke-virtual {v1}, Lcom/google/android/location/c/n;->a()V

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    invoke-virtual {v1}, Lcom/google/android/location/c/n;->b()I

    move-result v1

    if-le v1, v2, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v0, v2}, Lcom/google/android/location/c/B;->a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget v1, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/c/B$d;->d:I

    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B$d;->e:Z

    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B$d;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p2, Lcom/google/android/location/c/D$a;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    return-void
.end method

.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->e:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    :try_start_3
    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_5
    iget v1, p0, Lcom/google/android/location/c/B$d;->d:I

    const/16 v2, 0x14

    if-ge v1, v2, :cond_3

    const/4 v1, 0x1

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->b(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/B$d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    invoke-virtual {v3}, Lcom/google/android/location/c/B$a;->a()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/location/c/B$d;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v2, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/location/c/B$d;->d:I

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    invoke-virtual {v2}, Lcom/google/android/location/c/B$a;->b()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_2
    :try_start_6
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p3, :cond_4

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :cond_4
    :try_start_7
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_8
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/c/D$a;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/B$d;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
