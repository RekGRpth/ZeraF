.class public Lcom/google/android/location/c/K;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/location/c/K;->a:I

    iput v0, p0, Lcom/google/android/location/c/K;->b:I

    iput v0, p0, Lcom/google/android/location/c/K;->c:I

    iput v0, p0, Lcom/google/android/location/c/K;->d:I

    iput v0, p0, Lcom/google/android/location/c/K;->e:I

    iput v0, p0, Lcom/google/android/location/c/K;->f:I

    return-void
.end method


# virtual methods
.method a(Lcom/google/android/location/c/K;)V
    .locals 2

    iget v0, p0, Lcom/google/android/location/c/K;->a:I

    iget v1, p1, Lcom/google/android/location/c/K;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->a:I

    iget v0, p0, Lcom/google/android/location/c/K;->b:I

    iget v1, p1, Lcom/google/android/location/c/K;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->b:I

    iget v0, p0, Lcom/google/android/location/c/K;->c:I

    iget v1, p1, Lcom/google/android/location/c/K;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->c:I

    iget v0, p0, Lcom/google/android/location/c/K;->d:I

    iget v1, p1, Lcom/google/android/location/c/K;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->d:I

    iget v0, p0, Lcom/google/android/location/c/K;->e:I

    iget v1, p1, Lcom/google/android/location/c/K;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->e:I

    iget v0, p0, Lcom/google/android/location/c/K;->f:I

    iget v1, p1, Lcom/google/android/location/c/K;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/c/K;->f:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadSummary [numFileOpenSucc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numFileOpenFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numFileOpenInterrupted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numInvalidFileFormat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numGLocUploadSucc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numGLocUploadFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/K;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
