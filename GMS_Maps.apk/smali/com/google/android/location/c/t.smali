.class public Lcom/google/android/location/c/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/t$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/location/h/h$a;


# instance fields
.field private final b:Lcom/google/android/location/d/c;

.field private final c:Lcom/google/android/location/k/a/c;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Lcom/google/android/location/c/t$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/h",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/D$a;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/location/h/h$a;

    invoke-direct {v0}, Lcom/google/android/location/h/h$a;-><init>()V

    sput-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "https://www.google.com/loc/m/api"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    sget-object v1, Lcom/google/android/location/c/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->d(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "android"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->e(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    invoke-static {p1, v0}, Lcom/google/android/location/d/c;->a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V

    invoke-static {}, Lcom/google/android/location/d/c;->a()Lcom/google/android/location/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->c:Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/c/L;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->d:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/location/c/L;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v1, v2, v0, p2}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1, v0}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/h/b/m;
    .locals 3

    :try_start_0
    invoke-virtual {p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/location/c/v;-><init>(Ljava/lang/String;I[B)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/b/m;->b(I)V

    invoke-virtual {v1, p0}, Lcom/google/android/location/h/b/m;->a(Lcom/google/android/location/h/b/m$a;)V

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/c/t;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/t$a;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/c/t$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/c/h;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/c/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/location/c/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Server error, RC="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/c/t;->a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_1
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "Failed to read data from MASF: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->ai:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v1, Ljava/io/IOException;

    const-string v2, "isValid returned after parsing reply"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "GLS error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "Failed to send data to MASF: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/location/c/t;->a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/h;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/h",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/D$a;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v2, "g:loc/uil"

    invoke-direct {p0, v2, p1}, Lcom/google/android/location/c/t;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/h/b/m;

    move-result-object v2

    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move v0, v1

    :cond_1
    const-string v4, "Duplicated request."

    invoke-static {v0, v4}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/d/c;->a(Lcom/google/android/location/h/b/m;Z)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
