.class abstract Lcom/google/android/location/c/D;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/D$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/android/location/c/l;

.field protected final b:Lcom/google/android/location/k/a/c;

.field protected volatile c:Z

.field protected final d:Lcom/google/android/location/c/H;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z

    iput-object p1, p0, Lcom/google/android/location/c/D;->a:Lcom/google/android/location/c/l;

    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/D;->b:Lcom/google/android/location/k/a/c;

    iput-object p3, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
.end method

.method public declared-synchronized b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/c/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z

    invoke-virtual {p0}, Lcom/google/android/location/c/D;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
