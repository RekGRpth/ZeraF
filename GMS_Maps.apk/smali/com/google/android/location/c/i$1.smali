.class Lcom/google/android/location/c/i$1;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/i;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-virtual {v0}, Lcom/google/android/location/c/i;->h()V

    iget-object v0, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-static {v0, p1}, Lcom/google/android/location/c/i;->a(Lcom/google/android/location/c/i;Landroid/telephony/CellLocation;)V

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 0

    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-virtual {v0}, Lcom/google/android/location/c/i;->h()V

    iget-object v1, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/location/c/i;->a(Lcom/google/android/location/c/i;I)I

    iget-object v0, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    iget-object v1, p0, Lcom/google/android/location/c/i$1;->a:Lcom/google/android/location/c/i;

    invoke-static {v1}, Lcom/google/android/location/c/i;->a(Lcom/google/android/location/c/i;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/i;->a(Lcom/google/android/location/c/i;Landroid/telephony/CellLocation;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    goto :goto_0
.end method
