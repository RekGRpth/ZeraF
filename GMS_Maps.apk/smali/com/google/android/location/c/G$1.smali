.class Lcom/google/android/location/c/G$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/G;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/G;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/G;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([FI)F
    .locals 1

    array-length v0, p1

    if-ge p2, v0, :cond_0

    if-gez p2, :cond_1

    :cond_0
    const v0, 0x7f7fffff

    :goto_0
    return v0

    :cond_1
    aget v0, p1, p2

    goto :goto_0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->h()V

    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v4, v2}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v4, v3}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v3

    const/4 v5, 0x2

    invoke-direct {p0, v4, v5}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v4

    iget v5, p1, Landroid/hardware/SensorEvent;->accuracy:I

    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/location/c/k;->a(IFFFIJJ)V

    goto :goto_0
.end method
