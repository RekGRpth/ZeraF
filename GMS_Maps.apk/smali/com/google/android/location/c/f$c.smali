.class Lcom/google/android/location/c/f$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/f;

.field private final b:Lcom/google/android/location/c/f$a;

.field private final c:Lcom/google/android/location/c/g;

.field private volatile d:Z

.field private volatile e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/f$c;->d:Z

    invoke-static {}, Lcom/google/android/location/c/L;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    iput-object p3, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;Lcom/google/android/location/c/f$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/c/f$c;-><init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/f$c;)Lcom/google/android/location/c/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/f$a;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/f$c;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/f$a;->d()Lcom/google/android/location/c/K;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/f$c;->d:Z

    iget-object v1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->h(Lcom/google/android/location/c/f;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v1, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/c/f$c$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/location/c/f$c$4;-><init>(Lcom/google/android/location/c/f$c;Ljava/lang/String;Lcom/google/android/location/c/K;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$c$2;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/location/c/f$c$2;-><init>(Lcom/google/android/location/c/f$c;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->g(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/f$a;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, p1}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Z)Z

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$c$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/location/c/f$c$1;-><init>(Lcom/google/android/location/c/f$c;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-static {v1, p2}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;Ljava/lang/String;)Lcom/google/android/location/c/K;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v2, p2}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v3}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/c/f$c$3;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/google/android/location/c/f$c$3;-><init>(Lcom/google/android/location/c/f$c;ILjava/lang/String;Lcom/google/android/location/c/K;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f$c;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ZZ)V
    .locals 0

    return-void
.end method

.method public a_(I)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public declared-synchronized i()V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method
