.class Lcom/google/android/location/g$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/g;->q()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/g;


# direct methods
.method constructor <init>(Lcom/google/android/location/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/g$1;->a:Lcom/google/android/location/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 6

    const-wide/16 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v3, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    :goto_0
    if-eqz v5, :cond_0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    :cond_0
    cmp-long v5, v3, v1

    if-nez v5, :cond_3

    const/4 v0, 0x0

    :cond_1
    :goto_1
    return v0

    :cond_2
    move-wide v3, v1

    goto :goto_0

    :cond_3
    cmp-long v1, v3, v1

    if-gez v1, :cond_1

    const/4 v0, -0x1

    goto :goto_1
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/g$1;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    return v0
.end method
