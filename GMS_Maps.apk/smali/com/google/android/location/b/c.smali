.class public Lcom/google/android/location/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/io/File;

.field private final c:Lcom/google/android/location/c/a;

.field private final d:[B

.field private final e:Lcom/google/android/location/os/f;

.field private final f:Lcom/google/android/location/b/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/location/b/c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/c$a",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/location/b/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/k$b",
            "<TK;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/location/e/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/x",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/e/x;Lcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/location/e/x",
            "<TV;>;",
            "Lcom/google/android/location/b/k$b",
            "<TK;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "[B",
            "Lcom/google/android/location/os/f;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lt p1, p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Memory capacity is expected to be larger than disk capacity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p4, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    iput-object p6, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    iput-object p7, p0, Lcom/google/android/location/b/c;->d:[B

    if-nez p7, :cond_1

    iput-object v1, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    :goto_0
    iput-object p3, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/location/b/j;

    invoke-direct {v0, p1}, Lcom/google/android/location/b/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/location/b/c$a;

    invoke-direct {v0, p2, p3, p6}, Lcom/google/android/location/b/c$a;-><init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    :goto_1
    iput-object p5, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    iput-object p8, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    return-void

    :cond_1
    invoke-static {p7, v1}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    goto :goto_0

    :cond_2
    iput-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    return-object v0
.end method

.method static synthetic a(Ljava/io/Closeable;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/c;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    return-object v0
.end method

.method private static b(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/b/c;)[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->d:[B

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    return-object v0
.end method

.method private d()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    iget-object v1, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v1, p1}, Lcom/google/android/location/b/c$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/A;

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v2, :cond_0

    invoke-virtual {v1, p2, p3}, Lcom/google/android/location/e/A;->b(J)V

    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/A;->a(J)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move-object v1, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    if-eqz v1, :cond_4

    const-string v0, ""

    invoke-virtual {v1}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/location/e/A;

    invoke-virtual {v1}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v3

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0

    move-object v0, v2

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/location/b/c$1;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/android/location/b/c$1;-><init>(Lcom/google/android/location/b/c;Ljava/lang/Object;Lcom/google/android/location/e/A;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-object v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method public declared-synchronized a()V
    .locals 7

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    const-string v1, "lru.cache"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_0
    :try_start_4
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0}, Lcom/google/android/location/b/j;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v1}, Lcom/google/android/location/b/c$a;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v3}, Lcom/google/android/location/b/c$a;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v4}, Lcom/google/android/location/b/j;->a()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/c$a;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    if-ge v0, v3, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    iget-object v4, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_3
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_4
    :try_start_8
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/c$a;->clear()V

    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_5
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    throw v0

    :cond_3
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3
.end method

.method public declared-synchronized a(JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/j;->a(JJ)V

    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/c$a;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/e/A;

    iget-object v1, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    invoke-virtual {v1, p2}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    iget-object v1, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c$a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/location/e/A;

    iget-object v0, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    invoke-virtual {v0, p2}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    new-instance v2, Lcom/google/android/location/e/A;

    if-nez p2, :cond_2

    const-string v0, ""

    :goto_1
    invoke-direct {v2, v0, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/location/b/c$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_3

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/location/b/c$2;

    invoke-direct {v1, p0, v2, p2, p1}, Lcom/google/android/location/b/c$2;-><init>(Lcom/google/android/location/b/c;Lcom/google/android/location/e/A;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c$a;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    const-string v1, "lru.cache"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    iget-object v1, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v3, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    iget-object v4, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-interface {v3, v4, v0}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/OutputStream;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v3, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method
