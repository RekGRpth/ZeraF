.class public Lcom/google/android/location/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/g$a;,
        Lcom/google/android/location/b/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/K",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/b/h;

.field private final b:Lcom/google/android/location/b/g$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/g$a",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b/g$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/location/os/i;

.field private final d:Lcom/google/android/location/os/h;

.field private e:J

.field private f:Z

.field private final g:Lcom/google/android/location/os/j;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/location/b/g;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/location/b/g;-><init>(Lcom/google/android/location/os/i;Ljava/io/File;Lcom/google/android/location/os/h;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Ljava/io/File;Lcom/google/android/location/os/h;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/b/h;

    invoke-direct {v0}, Lcom/google/android/location/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    new-instance v0, Lcom/google/android/location/b/g$a;

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/b/g$a;-><init>(ILcom/google/android/location/b/h;)V

    iput-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    iput-object p1, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    iput-object p3, p0, Lcom/google/android/location/b/g;->d:Lcom/google/android/location/os/h;

    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x2

    invoke-interface {p1}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/j/a;->N:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    iput-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    invoke-direct {p0}, Lcom/google/android/location/b/g;->i()V

    return-void
.end method

.method private static a(Lcom/google/android/location/os/f;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/location/os/f;->f()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 13

    const/4 v12, 0x3

    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    add-long v9, v3, v1

    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    sub-long v3, v7, v5

    cmp-long v0, v9, v7

    if-lez v0, :cond_1

    :goto_0
    sub-long/2addr v7, v3

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-virtual {p0, v7, v8, v0}, Lcom/google/android/location/b/g;->a(JZ)V

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    invoke-virtual {p1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v7, v0, :cond_2

    invoke-virtual {p1, v12, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/google/android/location/b/g;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/location/b/g$b;

    invoke-direct {v0, v8}, Lcom/google/android/location/b/g$b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;JJJ)V

    invoke-virtual {v8, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    iget-object v10, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v8, v0}, Lcom/google/android/location/b/g$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    move-wide v7, v9

    goto :goto_0

    :cond_2
    return-void
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->N:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const/4 v3, 0x3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-static {v0}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/location/b/g;->e:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/android/location/b/g;->f:Z

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method private i()V
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4194997000000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    iget-object v2, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/g;->a(JZ)V

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->clear()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)F
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    iget-object v2, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    if-nez v0, :cond_1

    const/high16 v0, -0x40800000

    :goto_1
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->d()F

    move-result v0

    goto :goto_1
.end method

.method a(JJ)Lcom/google/android/location/b/g$b;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->r:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/android/location/b/g$b;

    invoke-direct {v1, v0}, Lcom/google/android/location/b/g$b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/b/g$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->c()V

    :cond_0
    move-object v0, v1

    :cond_1
    invoke-static {v0, p3, p4}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;J)V

    return-object v0
.end method

.method public a()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/b/g;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/android/location/b/g;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/location/b/g;->i()V

    goto :goto_0
.end method

.method public a(JZ)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/location/b/g;->e:J

    iput-boolean p3, p0, Lcom/google/android/location/b/g;->f:Z

    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .locals 5

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/google/android/location/b/g;->a(JJ)Lcom/google/android/location/b/g$b;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 11

    invoke-static {p1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/location/b/g;->d:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->e()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {v6, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    :goto_1
    const-wide/16 v9, -0x1

    cmp-long v0, v2, v9

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v4, v5, v7}, Lcom/google/android/location/b/g$b;->a(JI)V

    const/4 v2, 0x4

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->a(F)V

    :goto_2
    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->b(F)V

    :cond_2
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/e/F;->a(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->e()V

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->g()V

    goto :goto_3
.end method

.method public synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Long;)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-nez v0, :cond_0

    const/high16 v0, -0x40800000

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->f()F

    move-result v0

    goto :goto_0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b/g$b;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/location/e/E;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {p1, v1}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-eqz v0, :cond_2

    const/high16 v2, 0x3f800000

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->a(F)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/Long;)J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/location/b/g;->g()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    invoke-direct {p0}, Lcom/google/android/location/b/g;->h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c(Lcom/google/android/location/e/E;)V
    .locals 5

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/google/android/location/b/g;->a(JJ)Lcom/google/android/location/b/g$b;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/google/android/location/b/g$b;->b(Lcom/google/android/location/b/g$b;J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/b/g;->e:J

    return-wide v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/b/g;->f:Z

    return v0
.end method

.method public f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    iget-object v1, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v1}, Lcom/google/android/location/b/g$a;->a(Lcom/google/android/location/b/g$a;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v2}, Lcom/google/android/location/b/g$a;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/b/h;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 12

    const-wide/32 v10, 0x5265c00

    const v9, 0x7fffffff

    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    sub-long v2, v0, v2

    const-wide/32 v4, 0x240c8400

    sub-long v4, v0, v4

    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->c()I

    move-result v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->h()I

    move-result v1

    if-ne v7, v9, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    int-to-long v7, v7

    mul-long/2addr v7, v10

    cmp-long v7, v7, v4

    if-gez v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    goto :goto_0

    :cond_2
    if-eq v1, v9, :cond_0

    int-to-long v7, v1

    mul-long/2addr v7, v10

    cmp-long v1, v7, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v1}, Lcom/google/android/location/b/h;->b()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-static {v0}, Lcom/google/android/location/b/g$b;->b(Lcom/google/android/location/b/g$b;)V

    goto :goto_0

    :cond_3
    return-void
.end method
