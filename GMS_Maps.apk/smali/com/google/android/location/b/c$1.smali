.class Lcom/google/android/location/b/c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcom/google/android/location/e/A;

.field final synthetic c:Lcom/google/android/location/b/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/b/c;Ljava/lang/Object;Lcom/google/android/location/e/A;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    iput-object p2, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Lcom/google/android/location/b/c;)Ljava/io/File;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_1
    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v4}, Lcom/google/android/location/b/c;->c(Lcom/google/android/location/b/c;)[B

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v5}, Lcom/google/android/location/b/c;->d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/e/x;->a()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v5

    invoke-static {}, Lcom/google/common/base/Predicates;->alwaysTrue()Lcom/google/common/base/K;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v8}, Lcom/google/android/location/b/c;->e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/e/A;

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v2}, Lcom/google/android/location/b/c;->d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-virtual {v2}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :catch_0
    move-exception v0

    :try_start_6
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception v0

    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_a
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    :try_start_d
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3
.end method
