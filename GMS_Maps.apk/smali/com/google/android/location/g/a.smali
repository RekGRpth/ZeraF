.class public Lcom/google/android/location/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/g/d;

.field private final b:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/location/os/c;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;",
            "Lcom/google/android/location/os/c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/g/d;

    invoke-direct {v0}, Lcom/google/android/location/g/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    iput-object p1, p0, Lcom/google/android/location/g/a;->b:Lcom/google/android/location/b/i;

    iput-object p2, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    return-void
.end method

.method private a(Lcom/google/android/location/e/e;Ljava/util/Map;J)Lcom/google/android/location/b/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/e;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;J)",
            "Lcom/google/android/location/b/a",
            "<",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/location/e/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/g/a;->b:Lcom/google/android/location/b/i;

    invoke-virtual {v0, v1, p3, p4}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/f;)Lcom/google/android/location/e/c;
    .locals 14

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->b()J

    move-result-wide v7

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v0

    move-object v13, v0

    move-object v0, v2

    move-object v2, v13

    :goto_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->i()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/location/e/c;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/c;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V

    :goto_1
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v5}, Lcom/google/android/location/g/d;->a()V

    invoke-direct {p0, v0, v6, v7, v8}, Lcom/google/android/location/g/a;->a(Lcom/google/android/location/e/e;Ljava/util/Map;J)Lcom/google/android/location/b/a;

    move-result-object v5

    if-nez v5, :cond_2

    new-instance v0, Lcom/google/android/location/e/c;

    sget-object v2, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/c;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-virtual {v0}, Lcom/google/android/location/e/w;->a()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/location/e/c;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/c;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v5}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-virtual {v9, v0}, Lcom/google/android/location/g/d;->a(Lcom/google/android/location/e/w;)V

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e;

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v9

    sub-long v9, v3, v9

    const-wide/16 v11, 0x7530

    cmp-long v5, v9, v11

    if-gez v5, :cond_4

    invoke-direct {p0, v0, v6, v7, v8}, Lcom/google/android/location/g/a;->a(Lcom/google/android/location/e/e;Ljava/util/Map;J)Lcom/google/android/location/b/a;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-virtual {v0}, Lcom/google/android/location/e/w;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v9, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v5}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-virtual {v9, v0}, Lcom/google/android/location/g/d;->a(Lcom/google/android/location/e/w;)V

    goto :goto_2

    :cond_5
    new-instance v5, Lcom/google/android/location/e/w;

    iget-object v0, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v0}, Lcom/google/android/location/g/d;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/g/c;->b(D)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v2}, Lcom/google/android/location/g/d;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/g/c;->b(D)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v3}, Lcom/google/android/location/g/d;->e()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/location/g/c;->c(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/g/d;

    invoke-virtual {v4}, Lcom/google/android/location/g/d;->d()I

    move-result v4

    invoke-direct {v5, v0, v2, v3, v4}, Lcom/google/android/location/e/w;-><init>(IIII)V

    invoke-static {v5}, Lcom/google/android/location/g/c;->c(Lcom/google/android/location/e/w;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/location/e/c;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v1, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v5

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/c;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_6
    new-instance v0, Lcom/google/android/location/e/c;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/a;->c:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/c;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_7
    move-object v2, v1

    move-object v0, v1

    goto/16 :goto_0
.end method
