.class public Lcom/google/android/location/g/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/i$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/g/j;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/g/j;)V
    .locals 2

    const/high16 v0, -0x80000000

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/g/i;-><init>(Lcom/google/android/location/g/j;II)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/g/j;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/g/i;->a:Lcom/google/android/location/g/j;

    iput p2, p0, Lcom/google/android/location/g/i;->b:I

    iput p3, p0, Lcom/google/android/location/g/i;->c:I

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lcom/google/android/location/g/i$a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/i$a;"
        }
    .end annotation

    new-instance v2, Lcom/google/android/location/g/i$a;

    iget v0, p0, Lcom/google/android/location/g/i;->c:I

    invoke-direct {v2, v0}, Lcom/google/android/location/g/i$a;-><init>(I)V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v5, p0, Lcom/google/android/location/g/i;->b:I

    if-le v1, v5, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/g/i;->a:Lcom/google/android/location/g/j;

    invoke-interface {v0, v3}, Lcom/google/android/location/g/j;->a(Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/android/location/g/i$a;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/location/g/i$a;->c()V

    return-object v2
.end method

.method public a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p2}, Lcom/google/android/location/g/i;->a(Ljava/util/Map;)Lcom/google/android/location/g/i$a;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/location/g/i$a;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/g/i;->a:Lcom/google/android/location/g/j;

    invoke-interface {v1, v5}, Lcom/google/android/location/g/j;->a(Ljava/lang/String;)Lcom/google/android/location/e/l;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/android/location/e/l;->a(Ljava/util/Map;)Lcom/google/android/location/e/l$a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/location/e/l$a;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/location/g/i;->a:Lcom/google/android/location/g/j;

    invoke-interface {v1, v6}, Lcom/google/android/location/g/j;->b(Ljava/lang/String;)Lcom/google/android/location/e/j;

    move-result-object v7

    if-eqz v7, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v0}, Lcom/google/android/location/e/j;->a(Ljava/util/List;)Lcom/google/android/location/e/j$a;

    move-result-object v3

    new-instance v0, Lcom/google/android/location/e/w;

    iget v1, v3, Lcom/google/android/location/e/j$a;->a:I

    iget v2, v3, Lcom/google/android/location/e/j$a;->b:I

    iget v3, v3, Lcom/google/android/location/e/j$a;->c:I

    invoke-virtual {v4}, Lcom/google/android/location/e/l$a;->b()F

    move-result v4

    const/high16 v9, 0x41200000

    mul-float/2addr v4, v9

    const/high16 v9, 0x3f800000

    sub-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    invoke-interface {v7}, Lcom/google/android/location/e/j;->a()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/e/w;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/location/g/n$a;

    invoke-virtual {v8}, Lcom/google/android/location/g/i$a;->b()I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    move-object v0, v1

    goto :goto_0
.end method
