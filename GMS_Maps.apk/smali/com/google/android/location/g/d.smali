.class public Lcom/google/android/location/g/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:D

.field private b:D

.field private c:I

.field private d:I

.field private e:D

.field private f:D

.field private g:[D

.field private h:[D

.field private i:[I


# direct methods
.method constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0xa

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    iput-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    iput v3, p0, Lcom/google/android/location/g/d;->c:I

    iput v3, p0, Lcom/google/android/location/g/d;->d:I

    iput-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    iput-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    new-array v0, v2, [D

    iput-object v0, p0, Lcom/google/android/location/g/d;->g:[D

    new-array v0, v2, [D

    iput-object v0, p0, Lcom/google/android/location/g/d;->h:[D

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    invoke-virtual {p0}, Lcom/google/android/location/g/d;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    const/4 v1, 0x0

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/google/android/location/g/d;->a:D

    iput-wide v3, p0, Lcom/google/android/location/g/d;->b:D

    iput v1, p0, Lcom/google/android/location/g/d;->c:I

    iput v1, p0, Lcom/google/android/location/g/d;->d:I

    iput-wide v3, p0, Lcom/google/android/location/g/d;->e:D

    iput-wide v3, p0, Lcom/google/android/location/g/d;->f:D

    move v0, v1

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/g/d;->g:[D

    aput-wide v3, v2, v0

    iget-object v2, p0, Lcom/google/android/location/g/d;->h:[D

    aput-wide v3, v2, v0

    iget-object v2, p0, Lcom/google/android/location/g/d;->i:[I

    aput v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(DDII)V
    .locals 2

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    const/16 v0, 0x1388

    if-gt p5, v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    iget-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    add-double/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    iget v0, p0, Lcom/google/android/location/g/d;->d:I

    if-le p6, v0, :cond_0

    iput p6, p0, Lcom/google/android/location/g/d;->d:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/g/d;->g:[D

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput-wide p1, v0, v1

    iget-object v0, p0, Lcom/google/android/location/g/d;->h:[D

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput-wide p3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput p5, v0, v1

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/g/d;->c:I

    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/location/e/w;)V
    .locals 7

    iget v0, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v1

    iget v0, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v3

    iget v0, p1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v5

    iget v6, p1, Lcom/google/android/location/e/w;->d:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/g/d;->a(DDII)V

    return-void
.end method

.method public b()D
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    iget v2, p0, Lcom/google/android/location/g/d;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    return-wide v0
.end method

.method public c()D
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    iget v2, p0, Lcom/google/android/location/g/d;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    return-wide v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/g/d;->d:I

    return v0
.end method

.method public e()I
    .locals 14

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/g/d;->b()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/location/g/d;->c()D

    move-result-wide v2

    const/4 v8, 0x0

    const/16 v7, 0x1388

    const/16 v6, 0x1388

    const/4 v4, 0x1

    new-array v13, v4, [F

    const/4 v5, 0x0

    const/4 v4, 0x0

    move v9, v5

    move v10, v6

    move v11, v7

    move v12, v8

    move v8, v4

    :goto_1
    iget v4, p0, Lcom/google/android/location/g/d;->c:I

    if-ge v8, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/location/g/d;->g:[D

    aget-wide v4, v4, v8

    iget-object v6, p0, Lcom/google/android/location/g/d;->h:[D

    aget-wide v6, v6, v8

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    const/4 v4, 0x0

    aget v4, v13, v4

    float-to-int v4, v4

    add-int/2addr v12, v4

    const/4 v4, 0x0

    aget v4, v13, v4

    iget-object v5, p0, Lcom/google/android/location/g/d;->i:[I

    aget v5, v5, v8

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    const/4 v5, 0x1

    :goto_2
    iget-object v4, p0, Lcom/google/android/location/g/d;->i:[I

    aget v4, v4, v8

    if-ge v4, v11, :cond_4

    iget-object v4, p0, Lcom/google/android/location/g/d;->i:[I

    aget v7, v4, v8

    const/4 v4, 0x0

    aget v4, v13, v4

    float-to-int v6, v4

    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v9, v5

    move v10, v6

    move v11, v7

    goto :goto_1

    :cond_2
    if-eqz v9, :cond_3

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    div-int v0, v12, v0

    goto :goto_0

    :cond_3
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_4
    move v6, v10

    move v7, v11

    goto :goto_3

    :cond_5
    move v5, v9

    goto :goto_2
.end method
