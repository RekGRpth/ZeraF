.class public Lcom/google/android/location/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/x$1;,
        Lcom/google/android/location/x$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/os/i;

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lcom/google/android/location/x$a;

.field private g:J

.field private h:J

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private j:J

.field private k:Lcom/google/android/location/c/q;

.field private final l:Lcom/google/android/location/g;

.field private final m:Lcom/google/android/location/u;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/g;Lcom/google/android/location/u;)V
    .locals 9

    const-wide/16 v3, 0x0

    const-wide/16 v1, -0x1

    const/4 v8, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Lcom/google/android/location/x;->b:J

    iput-boolean v8, p0, Lcom/google/android/location/x;->c:Z

    iput-boolean v8, p0, Lcom/google/android/location/x;->d:Z

    iput-boolean v8, p0, Lcom/google/android/location/x;->e:Z

    sget-object v0, Lcom/google/android/location/x$a;->a:Lcom/google/android/location/x$a;

    iput-object v0, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    iput-wide v1, p0, Lcom/google/android/location/x;->g:J

    iput-wide v3, p0, Lcom/google/android/location/x;->h:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    iput-wide v3, p0, Lcom/google/android/location/x;->j:J

    iput-object p1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    iput-object p2, p0, Lcom/google/android/location/x;->l:Lcom/google/android/location/g;

    iput-object p3, p0, Lcom/google/android/location/x;->m:Lcom/google/android/location/u;

    invoke-virtual {p2}, Lcom/google/android/location/g;->e()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    sub-long v4, v2, v0

    const-wide/32 v6, 0x6cf2a0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    const-wide/32 v0, 0x6ddd00

    sub-long v0, v2, v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    :cond_0
    invoke-direct {p0, v0, v1, v8}, Lcom/google/android/location/x;->a(JZ)V

    return-void
.end method

.method private a(J)V
    .locals 5

    iget-wide v0, p0, Lcom/google/android/location/x;->j:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput-wide p1, p0, Lcom/google/android/location/x;->j:J

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v3}, Lcom/google/android/location/x;->a(Ljava/io/File;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 4

    iput-wide p1, p0, Lcom/google/android/location/x;->g:J

    iget-wide v0, p0, Lcom/google/android/location/x;->g:J

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->l:Lcom/google/android/location/g;

    iget-object v1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v1

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/g;->a(J)V

    :cond_0
    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/x;->b(Ljava/io/File;J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/x;->a(Ljava/io/File;J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/io/File;)Z

    :cond_1
    return-void
.end method

.method private a(Ljava/io/File;J)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-direct {p0, v4, p2, p3}, Lcom/google/android/location/x;->b(Ljava/io/File;J)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/x;->m:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    sget-object v1, Lcom/google/android/location/x$a;->a:Lcom/google/android/location/x$a;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/x;->a(J)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    sget-object v2, Lcom/google/android/location/x$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    invoke-virtual {v3}, Lcom/google/android/location/x$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    if-eq v1, v2, :cond_3

    :cond_3
    if-nez v0, :cond_2

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/x;->c()Z

    move-result v0

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/location/x;->d()Z

    move-result v0

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/location/x;->e()Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/x;->h:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/io/File;J)Z
    .locals 6

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    sub-long v2, p2, v0

    const-wide/32 v4, 0x240c8400

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    const-wide/32 v4, 0x36ee80

    add-long/2addr v4, p2

    cmp-long v0, v0, v4

    if-gtz v0, :cond_0

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/location/x;->c(Ljava/io/File;J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/x;->b:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/x;->b:J

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/location/x;->b:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_0
    return-void
.end method

.method private c()Z
    .locals 9

    const-wide/32 v7, 0x6ddd00

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/x;->g:J

    add-long/2addr v3, v7

    invoke-direct {p0}, Lcom/google/android/location/x;->h()Z

    move-result v5

    if-eqz v5, :cond_3

    cmp-long v5, v1, v3

    if-ltz v5, :cond_3

    invoke-direct {p0}, Lcom/google/android/location/x;->i()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/x;->a(JZ)V

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/google/android/location/x;->b(J)Z

    move-result v3

    if-eqz v3, :cond_2

    const-wide/16 v3, 0x1388

    add-long/2addr v3, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/location/x;->c(J)V

    :cond_1
    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/google/android/location/x;->a(J)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/x;->k()V

    sget-object v1, Lcom/google/android/location/x$a;->b:Lcom/google/android/location/x$a;

    iput-object v1, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    iget-object v1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/x;->a(JZ)V

    goto :goto_0

    :cond_3
    cmp-long v0, v3, v1

    if-gtz v0, :cond_5

    iget-wide v3, p0, Lcom/google/android/location/x;->b:J

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_4

    iget-wide v3, p0, Lcom/google/android/location/x;->b:J

    cmp-long v0, v3, v1

    if-gez v0, :cond_1

    :cond_4
    add-long v3, v1, v7

    invoke-direct {p0, v3, v4}, Lcom/google/android/location/x;->c(J)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v3, v4}, Lcom/google/android/location/x;->c(J)V

    goto :goto_1
.end method

.method private c(Ljava/io/File;J)Z
    .locals 2

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".lck"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x36ee80

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/location/x;->h()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/x;->g:J

    const-wide/16 v6, 0x3a98

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/x;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/location/x$a;->c:Lcom/google/android/location/x$a;

    iput-object v2, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/x;->f()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v4, v5}, Lcom/google/android/location/x;->c(J)V

    move v0, v1

    goto :goto_0
.end method

.method private e()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/location/x;->g:J

    const-wide/32 v5, 0x124f80

    add-long/2addr v3, v5

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/location/x;->h()Z

    move-result v5

    invoke-direct {p0}, Lcom/google/android/location/x;->j()Z

    move-result v6

    if-nez v0, :cond_0

    if-eqz v5, :cond_0

    if-nez v6, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    move v2, v1

    :goto_1
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3, v4}, Lcom/google/android/location/x;->c(J)V

    goto :goto_1
.end method

.method private f()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/location/x;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/x;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    sget-object v1, Lcom/google/android/location/x$a;->c:Lcom/google/android/location/x$a;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/c/C;->c(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SensorUploader"

    invoke-interface {v1, v0, p0, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Lcom/google/android/location/c/g;Ljava/lang/String;)Lcom/google/android/location/c/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    iget-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    invoke-interface {v0}, Lcom/google/android/location/c/q;->a()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    invoke-interface {v0}, Lcom/google/android/location/c/q;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/x;->k:Lcom/google/android/location/c/q;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/x;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/google/android/location/x;->l()V

    sget-object v0, Lcom/google/android/location/x$a;->a:Lcom/google/android/location/x$a;

    iput-object v0, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/x$a;

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/x;->c(J)V

    return-void
.end method

.method private h()Z
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/location/x;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->m:Lcom/google/android/location/u;

    invoke-virtual {v0, v1}, Lcom/google/android/location/u;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private i()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/x;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/x;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/x;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    iget-boolean v0, p0, Lcom/google/android/location/x;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->x()Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/x;->e:Z

    :cond_1
    return-void
.end method

.method private l()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/x;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/x;->e:Z

    :cond_0
    return-void
.end method

.method private m()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/x;->h()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/location/x;->j()Z

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/x;->g()V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/x;->b()V

    return-void
.end method

.method a(I)V
    .locals 2

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/x;->b:J

    invoke-direct {p0}, Lcom/google/android/location/x;->b()V

    :cond_0
    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/x;->m()V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/x;->m()V

    return-void
.end method

.method public a(ILjava/lang/String;Lcom/google/android/location/c/K;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/x;->f()V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/location/c/K;)V
    .locals 1

    iget v0, p2, Lcom/google/android/location/c/K;->b:I

    if-nez v0, :cond_0

    iget v0, p2, Lcom/google/android/location/c/K;->f:I

    if-nez v0, :cond_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/x;->f()V

    return-void
.end method

.method a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/x;->c:Z

    invoke-direct {p0}, Lcom/google/android/location/x;->b()V

    return-void
.end method

.method a(ZZI)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/location/x;->d:Z

    iget-object v0, p0, Lcom/google/android/location/x;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/x;->h:J

    invoke-direct {p0}, Lcom/google/android/location/x;->b()V

    return-void
.end method
