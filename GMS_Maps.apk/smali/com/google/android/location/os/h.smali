.class public Lcom/google/android/location/os/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/h$a;
    }
.end annotation


# static fields
.field static final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# instance fields
.field final b:Lcom/google/android/location/os/c;

.field final c:Lcom/google/android/location/os/f;

.field volatile d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field volatile e:J

.field private final f:Lcom/google/android/location/k/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/os/h;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->R:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/c;Lcom/google/android/location/os/f;Lcom/google/android/location/k/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/os/c;",
            "Lcom/google/android/location/os/f;",
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/os/h;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/os/h;->e:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    iput-object p2, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    iput-object p3, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    return-void
.end method

.method private a(J)J
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v2

    sub-long v2, v0, v2

    cmp-long v4, p1, v0

    if-lez v4, :cond_0

    move-wide p1, v0

    :cond_0
    sub-long v0, p1, v2

    return-wide v0
.end method

.method private a(Lcom/google/android/location/os/f;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lcom/google/android/location/os/f;->g()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_params"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(IZ)Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result p2

    :cond_0
    return p2
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    add-long/2addr v1, p2

    iget-object v3, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private s()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne v1, v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x2

    :try_start_2
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v2, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-wide v3, p0, Lcom/google/android/location/os/h;->e:J

    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->c()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v2, p0, Lcom/google/android/location/os/h;->e:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/os/h;->u()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized u()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    sget-object v2, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v2

    sget-object v3, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v3, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/google/android/location/os/h;->e:J

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/h;->a(Ljava/io/DataInputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/location/os/h;->u()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    int-to-long v2, v0

    invoke-static {p1}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    int-to-long v4, v0

    iput-object p1, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/os/h;->e:J

    cmp-long v0, v4, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Ljava/io/DataInputStream;)V
    .locals 4

    :try_start_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/os/h;->u()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(J)J

    move-result-wide v0

    sget-object v2, Lcom/google/android/location/j/a;->R:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p1, v2}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iput-object v2, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide v0, p0, Lcom/google/android/location/os/h;->e:J

    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    :cond_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v0

    :try_start_5
    invoke-direct {p0}, Lcom/google/android/location/os/h;->u()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_2
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/location/os/h;->u()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public b()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 2

    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 2

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 4

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public l()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public m()Lcom/google/android/location/os/h$a;
    .locals 9

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x16

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-long v1, v1

    const/16 v3, 0x22

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public n()Lcom/google/android/location/os/h$a;
    .locals 9

    const-wide/16 v7, 0x3e8

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x15

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-long v1, v1

    mul-long/2addr v1, v7

    const/16 v3, 0x1f

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v3, v7

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public o()Lcom/google/android/location/os/h$a;
    .locals 7

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x17

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x24

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public p()Lcom/google/android/location/os/h$a;
    .locals 7

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x18

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x26

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x27

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public q()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method
