.class public Lcom/google/android/location/os/real/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/d$1;,
        Lcom/google/android/location/os/real/d$a;,
        Lcom/google/android/location/os/real/d$b;
    }
.end annotation


# static fields
.field static a:Z

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static f:Ljava/io/File;


# instance fields
.field private e:Z

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/location/os/real/c;

.field private final i:Lcom/google/android/location/os/h;

.field private final j:Lcom/google/android/location/os/real/d$a;

.field private final k:Lcom/google/android/location/os/real/d$a;

.field private final l:Lcom/google/android/location/os/real/d$a;

.field private final m:Lcom/google/android/location/os/real/d$a;

.field private final n:Lcom/google/android/location/i/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/location/os/real/d;->a:Z

    const-string v0, ""

    sput-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/os/real/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/os/real/d;->e:Z

    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->j:Lcom/google/android/location/os/real/d$a;

    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->c:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->k:Lcom/google/android/location/os/real/d$a;

    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->l:Lcom/google/android/location/os/real/d$a;

    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->d:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->m:Lcom/google/android/location/os/real/d$a;

    iput-object p1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/d;->i:Lcom/google/android/location/os/h;

    iput-object p4, p0, Lcom/google/android/location/os/real/d;->h:Lcom/google/android/location/os/real/c;

    iput-object p3, p0, Lcom/google/android/location/os/real/d;->n:Lcom/google/android/location/i/a;

    invoke-static {p1}, Lcom/google/android/location/os/real/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->i:Lcom/google/android/location/os/h;

    return-object v0
.end method

.method static synthetic a(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-static {p0}, Lcom/google/android/location/os/real/d;->b(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a()V
    .locals 4

    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/internal/e;

    invoke-static {v0, p0}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    iget v0, v0, Lcom/google/android/location/internal/d;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/location/os/real/d;->b(Landroid/content/Context;)Lcom/google/android/location/h/h$a;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/d/c;->a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    const-string v0, "android"

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/os/real/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/d;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    sput-object p0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Lcom/google/android/location/h/h$a;
    .locals 4

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    const-string v0, "https://www.google.com/loc/m/api"

    sget-boolean v2, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "url:google_location_server"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    const-string v2, " "

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    aget-object v2, v1, v2

    const-string v3, "rewrite"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    aget-object v0, v1, v0

    :cond_1
    new-instance v1, Lcom/google/android/location/h/h$a;

    invoke-direct {v1}, Lcom/google/android/location/h/h$a;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->a(Ljava/lang/String;)V

    const-string v0, "location"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->c(Ljava/lang/String;)V

    const-string v0, "android"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->d(Ljava/lang/String;)V

    const-string v0, "gmm"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->e(Ljava/lang/String;)V

    return-object v1
.end method

.method private static b(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->W:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-static {}, Lcom/google/android/location/os/real/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-object v0
.end method

.method static declared-synchronized b()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget-object v0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v4, "nlp_GlsPlatformKey"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    sput-object v3, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/os/real/d;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/os/real/d;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->n:Lcom/google/android/location/i/a;

    return-object v0
.end method

.method private c()Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_provider_debug"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v1, "verbose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->h:Lcom/google/android/location/os/real/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/location/os/real/d;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v1, 0x3

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/location/os/real/d;->e:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-boolean v0, p0, Lcom/google/android/location/os/real/d;->e:Z

    :cond_0
    sget-boolean v2, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network_location_provider_debug"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_2

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    const/4 v4, 0x5

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->j:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->l:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->k:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/d;->m:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method
