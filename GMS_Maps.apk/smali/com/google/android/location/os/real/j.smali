.class public abstract Lcom/google/android/location/os/real/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/location/os/real/j;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    const-string v0, "com.google.android.location.os.real.SdkSpecific17"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    const-string v0, "com.google.android.location.os.real.SdkSpecific11"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_0

    :cond_2
    const/16 v1, 0x9

    if-lt v0, v1, :cond_3

    const-string v0, "com.google.android.location.os.real.SdkSpecific9"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_0

    :cond_3
    const/16 v1, 0x8

    if-lt v0, v1, :cond_4

    const-string v0, "com.google.android.location.os.real.SdkSpecific8"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported SDK"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/location/os/real/j;
    .locals 1

    sget-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/location/os/real/j;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/real/j;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract a(Landroid/telephony/gsm/GsmCellLocation;)I
.end method

.method public abstract a(Landroid/location/Location;)J
.end method

.method public abstract a(Landroid/net/wifi/ScanResult;)J
.end method

.method public abstract a(Landroid/telephony/TelephonyManager;J)Lcom/google/android/location/e/e;
.end method

.method public abstract a(Ljava/io/File;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method
