.class Lcom/google/android/location/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "IIIIII)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;"
        }
    .end annotation

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    const/4 v2, 0x0

    :goto_1
    move/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    if-ge v2, v5, :cond_2

    move/from16 v0, p2

    invoke-virtual {v8, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v5, 0x3

    new-array v9, v5, [F

    const/4 v5, 0x0

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    const/4 v5, 0x1

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    const/4 v5, 0x2

    move/from16 v0, p7

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    :goto_2
    move/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_1

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    :goto_3
    int-to-long v10, v5

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v3, v10

    int-to-long v5, v6

    add-long/2addr v3, v5

    new-instance v5, Lcom/google/android/location/a/j;

    invoke-direct {v5, v3, v4, v9}, Lcom/google/android/location/a/j;-><init>(J[F)V

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    goto :goto_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-object v7
.end method


# virtual methods
.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/F;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/F;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_0

    const/4 v2, 0x3

    const/4 v3, 0x7

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_1

    const/4 v2, 0x4

    const/4 v3, 0x7

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_2

    const/4 v2, 0x5

    const/16 v3, 0x8

    const/16 v4, 0x9

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported scanner type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
