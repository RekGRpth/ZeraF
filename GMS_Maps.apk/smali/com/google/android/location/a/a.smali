.class Lcom/google/android/location/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lcom/google/android/location/a/e;J)Lcom/google/android/location/clientlib/NlpActivity;
    .locals 10

    const/16 v9, 0x3a

    const/4 v8, 0x1

    const/16 v7, 0xa

    const-wide v5, 0x3fc3333333333333L

    const/16 v4, 0x64

    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fbe4bfd2e946801L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3f9a84be40420f6fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3f924d8fd5cb7910L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23879c4113c686L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f201f31f46ed246L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f121682f944241cL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f14b599aa60913aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_0

    :cond_1
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd64bf8fcd67fd4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe901c92ddbdb5eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ce6c093d96638L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fa13be22e5de15dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c92ddbdb5d895L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f58e757928e0c9eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22599ed7c6fbd2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_a
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4024a2fd976ff3aeL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_c
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_d
    const-wide v0, 0x3fefae147ae147aeL

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402a5d3bf2f55582L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f46a4873365881aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_e
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fb954003254e6e2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0x33

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e03f705857affL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_12
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd16af038e29f9dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dd40ee06d938L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_15
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa5171e29b6b2afL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa8f3eccc469510L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd12bb6672fba02L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40005a5ebb7739f3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002bf2cb641700dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f593b3a68b19a41L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021cde3fbbd7b20L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2f969e3c968944L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_16
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f97af640639d5e5L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1d
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f91737542a23c00L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd44dbdf8f47304L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fcec8e68e3ef284L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020623bbc6eb0b8L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3feb905186db50f4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67aeddce7cd035L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1e
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40287909aed56b01L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_1f
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4422036006d0dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_20
    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4016e28134480629L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_21
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f616872b020c49cL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_22
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_24
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022fd1e53a81dc1L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5d4738a3b57c4eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f45f45e0b4e11dcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_25
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f71b60ae9680e06L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_27
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e060fe47991bcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4019e81beb18116fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4fd9ba1b1960faL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_28
    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ffb480a5accd5L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_29
    const/16 v0, 0x25

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a79fec99f1ae3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2a
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021b79acffa7eb7L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2c
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2e
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_2f
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb82a23bff8a8f4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd55553ef6b5d46L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4028a45069a4df48L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_31
    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f253bd1676640a7L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_32
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fc7eb313be22e5eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_34
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402189d6f112fd33L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f500a393ee5eeddL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f680d3cff64cf8dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4908e581cf7879L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_35
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_36
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_37
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_38
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f548ba83f4eca68L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_39
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5e42e126202539L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3c
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd054bcf0b6b6e1L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc1b645a1cac083L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f804ab606b7aa26L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400560a2877ee4e2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_3f
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4035922281344806L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_40
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_41
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide/high16 v2, 0x3fda000000000000L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61d9b1b79d909fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_44
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff68a9cdc443915L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f805e1c15097c81L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_45
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe2cba94bbe4474L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_46
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_47
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3feea88ce703afb8L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff677fae3608d09L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9cf5f4e4430b18L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_48
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_49
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91b25f633ce63aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4a
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40135cbc48f10a9aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4b
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020f01d5c31593eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7a5119ce075f70L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400044cc6822ff09L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40316eba9d1f6018L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4017d312b1b36bd3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401a6ce2089e3433L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4e
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_4f
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0

    :cond_52
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_0
.end method
