.class Lcom/google/android/location/a/b$a;
.super Lcom/google/android/location/a/b$d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/a/b;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/clientlib/NlpActivity;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z

.field private f:Lcom/google/android/location/a/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/b;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/b$d;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    iput v2, p0, Lcom/google/android/location/a/b$a;->d:I

    iput-boolean v2, p0, Lcom/google/android/location/a/b$a;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$a;-><init>(Lcom/google/android/location/a/b;)V

    return-void
.end method

.method private b(Lcom/google/android/location/clientlib/NlpActivity;)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    if-eq v2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/android/location/a/b$a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/a/b$a;->d:I

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/c;->a(I)V

    return-void
.end method

.method private h()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/location/a/b$a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    :cond_0
    return-void
.end method

.method private i()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->i(Lcom/google/android/location/a/b;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x15f90

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->b()D

    move-result-wide v0

    const-wide v2, 0x3fe6666666666666L

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/android/location/a/c$a;)Lcom/google/android/location/a/c;
    .locals 3

    new-instance v0, Lcom/google/android/location/a/c;

    iget-object v1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/a/k;

    invoke-direct {v2}, Lcom/google/android/location/a/k;-><init>()V

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/location/a/c;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/a/c$a;Lcom/google/android/location/a/k;)V

    return-object v0
.end method

.method protected a()V
    .locals 4

    invoke-virtual {p0, p0}, Lcom/google/android/location/a/b$a;->a(Lcom/google/android/location/a/c$a;)Lcom/google/android/location/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    invoke-virtual {p0}, Lcom/google/android/location/a/b$a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->g()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v2

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$a;->b(Lcom/google/android/location/clientlib/NlpActivity;)I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    if-lt v2, v6, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v3, v4, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v0, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/B;->a:Lcom/google/android/location/e/B;

    if-ne v0, v1, :cond_4

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V

    goto :goto_2

    :cond_5
    iget v0, p0, Lcom/google/android/location/a/b$a;->d:I

    rsub-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    const/16 v2, 0x8

    if-le v0, v2, :cond_6

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_7

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v6, :cond_8

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    iget-object v1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;J)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/location/a/b$d;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->h()V

    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/location/a/b$d;->a(ZLjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->h()V

    return-void
.end method

.method public a_()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/location/a/b$d;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/a/c;->a()V

    return-void
.end method

.method protected c()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->g()V

    return-void
.end method
