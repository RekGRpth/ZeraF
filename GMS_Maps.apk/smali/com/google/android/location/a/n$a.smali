.class Lcom/google/android/location/a/n$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/e/E;

.field private final b:Lcom/google/android/location/e/t;


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/E;Lcom/google/android/location/e/t;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/a/n$a;->a:Lcom/google/android/location/e/E;

    iput-object p2, p0, Lcom/google/android/location/a/n$a;->b:Lcom/google/android/location/e/t;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/n$a;->a:Lcom/google/android/location/e/E;

    return-object v0
.end method

.method public b()Lcom/google/android/location/e/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/n$a;->b:Lcom/google/android/location/e/t;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/a/n$a;->b:Lcom/google/android/location/e/t;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ClassificationSignals [wifiScan="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/a/n$a;->a:Lcom/google/android/location/e/E;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", networkLocation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/n$a;->b:Lcom/google/android/location/e/t;

    invoke-virtual {v0}, Lcom/google/android/location/e/t;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
