.class public Lcom/google/android/location/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/b$1;,
        Lcom/google/android/location/a/b$d;,
        Lcom/google/android/location/a/b$a;,
        Lcom/google/android/location/a/b$c;,
        Lcom/google/android/location/a/b$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/os/i;

.field private final b:Z

.field private c:Lcom/google/android/location/a/b$d;

.field private d:J

.field private e:Lcom/google/android/location/clientlib/NlpActivity;

.field private f:J

.field private g:Lcom/google/android/location/a/n$b;

.field private h:J

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private n:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;)V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/a/b$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/a/b$b;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    iput-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    iput-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    iput-object v1, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    iput-wide v2, p0, Lcom/google/android/location/a/b;->f:J

    iput-object v1, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    iput-wide v2, p0, Lcom/google/android/location/a/b;->h:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/a/b;->i:Z

    iput-boolean v4, p0, Lcom/google/android/location/a/b;->j:Z

    iput v4, p0, Lcom/google/android/location/a/b;->k:I

    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/a/b;->m:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    iput-object p1, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {p1}, Lcom/google/android/location/os/i;->A()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    iget-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method private a()V
    .locals 4

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    iput-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/a/b;->d:J

    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    const/16 v1, 0x8

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/location/a/b$d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->b()V

    iput-object p1, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a/b;->a(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b$d;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    return-void
.end method

.method private a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/a/b;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    return-object v0
.end method

.method private b(Z)V
    .locals 6

    const-wide/16 v2, 0x3e8

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/location/a/b;->m:I

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    :goto_0
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/a/b;->n:J

    const-wide/16 v4, 0xe10

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    iget v0, p0, Lcom/google/android/location/a/b;->k:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->e()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/location/a/b;->m:I

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->d()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/location/a/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/a/b;->j:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/location/a/b;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/location/a/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/a/b;->a()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/location/a/b;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/a/b;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->f:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/a/b;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->h:J

    return-wide v0
.end method

.method static synthetic j(Lcom/google/android/location/a/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/a/b;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/location/a/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/a/b;->j:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 6

    const-wide/16 v4, -0x1

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    iput-wide v4, p0, Lcom/google/android/location/a/b;->d:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/a/b;->a(J)V

    goto :goto_0

    :cond_2
    iput-wide v4, p0, Lcom/google/android/location/a/b;->d:J

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->c()V

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    iget v0, p0, Lcom/google/android/location/a/b;->k:I

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/location/a/b;->k:I

    iput p2, p0, Lcom/google/android/location/a/b;->m:I

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b$d;->a(Lcom/google/android/location/a/n$b;)V

    iput-object p1, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a/b;->h:J

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b$d;->a(Z)V

    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b$d;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    return-void
.end method

.method public c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    return-void
.end method
