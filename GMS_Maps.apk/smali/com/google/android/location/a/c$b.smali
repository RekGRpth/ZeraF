.class Lcom/google/android/location/a/c$b;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/a/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/c;Lcom/google/android/location/a/c$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/a/c$b;-><init>(Lcom/google/android/location/a/c;)V

    return-void
.end method

.method private a(Lcom/google/android/location/a/e;)Z
    .locals 6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/location/a/e;->d(I)D

    move-result-wide v2

    const-wide/high16 v4, 0x4008000000000000L

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0}, Lcom/google/android/location/a/c;->a(Lcom/google/android/location/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0, v5}, Lcom/google/android/location/a/c;->a(Lcom/google/android/location/a/c;Z)Z

    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0}, Lcom/google/android/location/a/c;->b(Lcom/google/android/location/a/c;)Lcom/google/android/location/os/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v2}, Lcom/google/android/location/a/c;->c(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/k;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    invoke-virtual {v2, p1, v3}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/F;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x55

    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0}, Lcom/google/android/location/a/c;->d(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/c$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/a/c$a;->a_()V

    goto :goto_0

    :cond_1
    const/16 v3, 0xa

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v3}, Lcom/google/android/location/a/c;->e(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/location/a/d;->a(Ljava/util/List;)Lcom/google/android/location/a/e;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/location/a/c$b;->a(Lcom/google/android/location/a/e;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v2}, Lcom/google/android/location/a/c;->d(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/c$a;

    move-result-object v2

    new-instance v3, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v4, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v3, v4, v5, v0, v1}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    invoke-interface {v2, v3}, Lcom/google/android/location/a/c$a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v3}, Lcom/google/android/location/a/c;->f(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/a;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/location/a/a;->a(Lcom/google/android/location/a/e;J)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v1}, Lcom/google/android/location/a/c;->d(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/c$a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/a/c$a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0}, Lcom/google/android/location/a/c;->a(Lcom/google/android/location/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/a/c;->a(Lcom/google/android/location/a/c;Z)Z

    iget-object v0, p0, Lcom/google/android/location/a/c$b;->a:Lcom/google/android/location/a/c;

    invoke-static {v0}, Lcom/google/android/location/a/c;->d(Lcom/google/android/location/a/c;)Lcom/google/android/location/a/c$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/location/a/c$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
