.class Lcom/google/android/location/f/b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/f/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/b$a$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/location/f/b$a$a;

.field private b:I

.field private c:F

.field private d:I

.field private e:I

.field private f:Lcom/google/android/location/f/c;


# direct methods
.method constructor <init>(IFII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/f/b$a$a;->b:Lcom/google/android/location/f/b$a$a;

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    iput p1, p0, Lcom/google/android/location/f/b$a;->b:I

    iput p2, p0, Lcom/google/android/location/f/b$a;->c:F

    iput p3, p0, Lcom/google/android/location/f/b$a;->d:I

    iput p4, p0, Lcom/google/android/location/f/b$a;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/f/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    iput-object p1, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/b$a$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    return-object v0
.end method

.method static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b$a;
    .locals 5

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-static {}, Lcom/google/android/location/f/b$a$a;->values()[Lcom/google/android/location/f/b$a$a;

    move-result-object v1

    aget-object v0, v1, v0

    sget-object v1, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/location/f/c;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/c;

    move-result-object v1

    new-instance v0, Lcom/google/android/location/f/b$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/f/b$a;-><init>(Lcom/google/android/location/f/c;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v2

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    new-instance v0, Lcom/google/android/location/f/b$a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/f/b$a;-><init>(IFII)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/f/b$a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/f/b$a;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/f/b$a;)F
    .locals 1

    iget v0, p0, Lcom/google/android/location/f/b$a;->c:F

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/f/b$a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/f/b$a;->d:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/f/b$a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/f/b$a;->e:I

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/f/b$a;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/f/b$a;

    iget-object v2, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    iget-object v3, p1, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/location/f/b$a;->b:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/location/f/b$a;->c:F

    iget v3, p1, Lcom/google/android/location/f/b$a;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/location/f/b$a;->d:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/location/f/b$a;->e:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    iget-object v3, p1, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v2, v3}, Lcom/google/android/location/f/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    invoke-virtual {v0}, Lcom/google/android/location/f/b$a$a;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v0}, Lcom/google/android/location/f/c;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    sget-object v1, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Leaf: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v1}, Lcom/google/android/location/f/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[%d] <= %f (%d) : (%d)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/location/f/b$a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/location/f/b$a;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/location/f/b$a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/location/f/b$a;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
