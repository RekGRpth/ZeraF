.class public Lcom/google/android/location/i/c$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/i/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/google/android/location/i/c;

.field private c:J


# direct methods
.method private constructor <init>(Lcom/google/android/location/i/c;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/i/c$a;->b:Lcom/google/android/location/i/c;

    iput-wide p2, p0, Lcom/google/android/location/i/c$a;->a:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/i/c$a;->c:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/i/c;JLcom/google/android/location/i/c$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/i/c$a;-><init>(Lcom/google/android/location/i/c;J)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(J)J
    .locals 6

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/location/i/c$a;->a:J

    iget-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    add-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/android/location/i/c$a;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    iget-object v2, p0, Lcom/google/android/location/i/c$a;->b:Lcom/google/android/location/i/c;

    invoke-static {v2, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/i/c;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
