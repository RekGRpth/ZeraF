.class Lcom/google/android/location/i/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i/b$a;
    }
.end annotation


# instance fields
.field final a:J

.field private final b:Lcom/google/android/location/i/c;

.field private final c:Lcom/google/android/location/i/c;

.field private final d:Lcom/google/android/location/i/c;

.field private final e:Lcom/google/android/location/i/c;

.field private final f:Lcom/google/android/location/os/i;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    iput-wide p6, p0, Lcom/google/android/location/i/b;->a:J

    iput-object p2, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    iput-object p3, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    iput-object p4, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    iput-object p5, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    invoke-direct {p0, p8, p9}, Lcom/google/android/location/i/b;->c(J)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;JJ)V
    .locals 14

    new-instance v0, Lcom/google/android/location/i/c;

    const-string v1, "bandwidth"

    const-wide/16 v2, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->m()Lcom/google/android/location/os/h$a;

    move-result-object v4

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v1, Lcom/google/android/location/i/c;

    const-string v2, "general-gps"

    const-wide/16 v3, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->n()Lcom/google/android/location/os/h$a;

    move-result-object v5

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v2, Lcom/google/android/location/i/c;

    const-string v3, "sensor-gps"

    const-wide/16 v4, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->o()Lcom/google/android/location/os/h$a;

    move-result-object v6

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    invoke-direct/range {v2 .. v10}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v3, Lcom/google/android/location/i/c;

    const-string v4, "burst-gps"

    const-wide/16 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->p()Lcom/google/android/location/os/h$a;

    move-result-object v7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    invoke-direct/range {v3 .. v11}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    move-object v4, p0

    move-object v5, p1

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    invoke-direct/range {v4 .. v13}, Lcom/google/android/location/i/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;JJ)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    return-object v0
.end method

.method private declared-synchronized a(JLjava/io/ByteArrayOutputStream;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/location/i/b;->a:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    const/4 v3, 0x3

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v2, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    const/4 v3, 0x4

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v2, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    const/4 v3, 0x5

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v2, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    const/4 v3, 0x6

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/i/b;JLjava/io/ByteArrayOutputStream;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/i/b;->a(JLjava/io/ByteArrayOutputStream;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    return-void
.end method

.method private static a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/i/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized c(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/android/location/i/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method a(J)V
    .locals 6

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->e()Ljava/io/File;

    move-result-object v3

    const-string v4, "cp_state"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/location/i/b;->a(Ljava/io/InputStream;J)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0, v0}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-direct {p0, v0}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method a(Ljava/io/InputStream;J)V
    .locals 11

    const/4 v2, 0x1

    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    sget-object v1, Lcom/google/android/location/j/a;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    const-wide/16 v1, 0x0

    cmp-long v1, v5, v1

    if-gtz v1, :cond_1

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    :try_start_2
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    move-wide v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v10

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public b()Lcom/google/android/location/i/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method public declared-synchronized b(J)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/i/b$a;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/i/b$a;-><init>(Lcom/google/android/location/i/b;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lcom/google/android/location/i/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method public d()Lcom/google/android/location/i/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    return-object v0
.end method
