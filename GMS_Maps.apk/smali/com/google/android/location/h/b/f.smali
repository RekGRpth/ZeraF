.class public Lcom/google/android/location/h/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/location/h/b/n;

.field private b:Ljava/io/DataInputStream;

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/h/b/n;)V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/h/b/f;->a:Lcom/google/android/location/h/b/n;

    invoke-virtual {p1}, Lcom/google/android/location/h/b/n;->g()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/h/b/f;->g:I

    invoke-virtual {p1}, Lcom/google/android/location/h/b/n;->b_()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/h/b/f;->f:I

    new-instance v1, Ljava/io/DataInputStream;

    invoke-virtual {p1}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/google/android/location/h/b/f;->b:Ljava/io/DataInputStream;

    instance-of v1, p1, Lcom/google/android/location/h/b/h;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/location/h/b/h;

    invoke-virtual {p1}, Lcom/google/android/location/h/b/h;->e()Lcom/google/android/location/h/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/h/b/f;->e:I

    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/h/b/f;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/h/b/f;->d:[Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/location/h/b/f;->c:[Ljava/lang/String;

    aput-object v0, v4, v1

    iget-object v4, p0, Lcom/google/android/location/h/b/f;->d:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/location/h/b/f;->f:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/h/b/f;->b:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/google/android/location/h/b/f;->e:I

    iget v1, p0, Lcom/google/android/location/h/b/f;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/h/b/f;->f:I

    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/h/b/f;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/h/b/f;->d:[Ljava/lang/String;

    :goto_1
    iget v1, p0, Lcom/google/android/location/h/b/f;->e:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/h/b/f;->b:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/h/b/f;->c:[Ljava/lang/String;

    aput-object v1, v2, v0

    iget v2, p0, Lcom/google/android/location/h/b/f;->f:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/android/location/h/b/f;->f:I

    iget-object v1, p0, Lcom/google/android/location/h/b/f;->b:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/h/b/f;->d:[Ljava/lang/String;

    aput-object v1, v2, v0

    iget v2, p0, Lcom/google/android/location/h/b/f;->f:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/android/location/h/b/f;->f:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/h/b/f;->g:I

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/h/b/f;->f:I

    return v0
.end method

.method public c()Ljava/io/DataInputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/f;->b:Ljava/io/DataInputStream;

    return-object v0
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/f;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/f;->d:[Ljava/lang/String;

    return-object v0
.end method
