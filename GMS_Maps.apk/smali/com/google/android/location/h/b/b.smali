.class public Lcom/google/android/location/h/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# instance fields
.field private final a:Lcom/google/android/location/h/g;

.field private b:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/h/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/h/b/b;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;
    .locals 9

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readShort()S

    move-result v1

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/google/android/location/h/b/g;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataInputStream;)Ljava/util/Hashtable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " => "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Lcom/google/android/location/h/f;

    invoke-direct {v6, v3, v0}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    new-instance v0, Lcom/google/android/location/h/b/c;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/location/h/b/c;-><init>(Ljava/lang/String;ILcom/google/android/location/h/f;)V

    invoke-virtual {v6}, Lcom/google/android/location/h/f;->b()V

    new-instance v1, Lcom/google/android/location/h/b/b;

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/h/b/b;-><init>(Ljava/lang/String;Lcom/google/android/location/h/g;)V

    iput-object v5, v1, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    iput-object v4, v1, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;

    return-object v1

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method

.method private declared-synchronized h()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/util/Hashtable;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/b;->e:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->a()V

    return-void
.end method

.method public b_()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/h/b/b;->h()V

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->e:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v1}, Lcom/google/android/location/h/g;->b_()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/location/h/b/b;->h()V

    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->e:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public declared-synchronized d()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/google/android/location/h/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    return-object v0
.end method

.method public declared-synchronized g()Ljava/util/Hashtable;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
