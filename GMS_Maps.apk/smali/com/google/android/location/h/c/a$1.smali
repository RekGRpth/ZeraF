.class Lcom/google/android/location/h/c/a$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/h/c/a;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/h/c/a;


# direct methods
.method constructor <init>(Lcom/google/android/location/h/c/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .locals 6

    invoke-static {}, Lcom/google/android/location/h/c/a;->a()Lcom/google/android/location/h/c/a;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->b_()I

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v0

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v0}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v0}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v2}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v3, Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v4}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .locals 3

    invoke-static {}, Lcom/google/android/location/h/c/a;->a()Lcom/google/android/location/h/c/a;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
