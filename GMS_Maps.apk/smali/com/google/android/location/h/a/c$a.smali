.class public Lcom/google/android/location/h/a/c$a;
.super Lcom/google/android/location/h/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/a/b;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/h/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:Lcom/google/android/location/h/a/c;

.field private c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

.field private d:Ljava/io/InputStream;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/io/DataInputStream;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:I

.field private n:Lcom/google/android/location/h/g;

.field private o:[B

.field private p:I

.field private q:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/h/a/c;Ljava/lang/String;I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/h/a/a;-><init>()V

    const-wide/16 v0, 0x4e20

    iput-wide v0, p0, Lcom/google/android/location/h/a/c$a;->a:J

    const-string v0, "GET"

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    iput-object p2, p0, Lcom/google/android/location/h/a/c$a;->g:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/location/h/a/c$a;->p:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/a/c$a;->i:Z

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V
    .locals 0

    return-void
.end method

.method private a(Ljava/io/OutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v3, v0, v2

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-interface {p1, v3, v0}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    array-length v0, v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/location/h/a/c$a;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/io/InputStream;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    invoke-super {p0}, Lcom/google/android/location/h/a/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Las/a;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/location/h/a/c$a;->a(Las/a;)V

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v0, p0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;Lcom/google/android/location/h/a/c$a;)Z

    return-void
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d_()I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    iget v0, p0, Lcom/google/android/location/h/a/c$a;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()J
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected i()V
    .locals 2

    iget v0, p0, Lcom/google/android/location/h/a/c$a;->p:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/a/c$a;->q:J

    invoke-super {p0}, Lcom/google/android/location/h/a/a;->i()V

    :cond_0
    return-void
.end method

.method public declared-synchronized j()Ljava/io/DataInputStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->q:J

    return-wide v0
.end method

.method public declared-synchronized n()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized o()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->notifyTimeout()V

    :cond_0
    new-instance v0, Lcom/google/android/location/h/a/e;

    invoke-direct {v0}, Lcom/google/android/location/h/a/e;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/a/c$a;->a(Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 11

    const/4 v10, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;)Las/c;

    move-result-object v0

    new-instance v4, Lcom/google/android/location/h/a/c$b;

    invoke-direct {v4, v0}, Lcom/google/android/location/h/a/c$b;-><init>(Las/c;)V

    invoke-virtual {v4, p0}, Lcom/google/android/location/h/a/c$b;->a(Lcom/google/android/location/h/a/c$a;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->n()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Lcom/google/android/location/h/a/c$b;->a(J)V

    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->g()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f_()V

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;

    const-string v2, "POST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    iget-object v3, p0, Lcom/google/android/location/h/a/c$a;->g:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    move-result-object v3

    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f()Z

    move-result v0

    if-nez v0, :cond_2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_1
    :try_start_7
    iget-object v5, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v5}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/io/g;->c()V

    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :try_start_8
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v5

    if-ne v5, v10, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/a/c$a;->a(Ljava/lang/Exception;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    goto :goto_0

    :cond_2
    :try_start_9
    iput-object v3, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, "Content-Type"

    iget-object v2, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-direct {p0, v3}, Lcom/google/android/location/h/a/c$a;->b(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/location/h/a/c$a;->i:Z

    if-eqz v0, :cond_3

    const-string v0, "Content-Length"

    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->q()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a()Ljava/io/DataOutputStream;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v2

    :goto_3
    :try_start_b
    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->p()Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-result v0

    if-eqz v0, :cond_4

    :try_start_c
    invoke-direct {p0, v2}, Lcom/google/android/location/h/a/c$a;->a(Ljava/io/OutputStream;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    :cond_4
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->c()I

    move-result v0

    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->e()J

    move-result-wide v5

    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->d()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->b()Ljava/io/DataInputStream;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    invoke-static {v3}, Lcom/google/android/location/h/a/c$a;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    const/16 v8, 0xc8

    if-ne v0, v8, :cond_6

    iget-object v8, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v8}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Lcom/google/googlenav/common/io/g;->a(Z)Z

    :goto_4
    monitor-enter p0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :try_start_e
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f()Z

    move-result v8

    if-nez v8, :cond_7

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    throw v0

    :catch_1
    move-exception v0

    move-object v2, v1

    goto/16 :goto_1

    :cond_5
    const-string v0, "Content-Type"

    const-string v2, "application/binary"

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v2, v1

    :goto_5
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    throw v0

    :catchall_3
    move-exception v0

    :try_start_11
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    throw v0

    :catch_2
    move-exception v0

    goto/16 :goto_1

    :cond_6
    iget-object v8, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v8}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/googlenav/common/io/g;->c()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    goto :goto_4

    :catchall_4
    move-exception v0

    goto :goto_5

    :cond_7
    :try_start_12
    iput v0, p0, Lcom/google/android/location/h/a/c$a;->m:I

    iput-wide v5, p0, Lcom/google/android/location/h/a/c$a;->l:J

    iput-object v7, p0, Lcom/google/android/location/h/a/c$a;->k:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->g_()V

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    :try_start_14
    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :catchall_6
    move-exception v0

    :try_start_15
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    :try_start_16
    throw v0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    :catchall_7
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_5

    :cond_8
    move-object v2, v1

    goto/16 :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
