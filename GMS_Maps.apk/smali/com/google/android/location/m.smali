.class public Lcom/google/android/location/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/K",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/j;

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/g;


# direct methods
.method public constructor <init>(Lcom/google/android/location/g;Lcom/google/android/location/os/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/j;

    invoke-direct {v0}, Lcom/google/android/location/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/m;->a:Lcom/google/android/location/j;

    iput-object p2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    iput-object p1, p0, Lcom/google/android/location/m;->c:Lcom/google/android/location/g;

    return-void
.end method

.method private a(Ljava/util/TimeZone;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v0

    :goto_1
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_4

    invoke-virtual {p2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v1, v0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->E:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private a(JILcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    const/4 v7, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->O:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v7, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    invoke-virtual {v1, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x3

    iget-object v0, p4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/4 v2, 0x4

    iget-object v0, p4, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide/high16 v5, 0x4059000000000000L

    mul-double/2addr v3, v5

    double-to-int v0, v3

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-virtual {v1, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    if-eqz p6, :cond_2

    const/4 v0, 0x6

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v0, 0x7

    invoke-virtual {v1, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/location/m;Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/m;->b(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a(Ljava/util/Calendar;Lcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x2

    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/j/a;->y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v6}, Lcom/google/android/location/os/i;->w()Ljava/io/File;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/m;->a(Ljava/util/TimeZone;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v7

    if-gez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    move-object v4, p0

    move-object v8, p2

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/m;->a(JILcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/location/m;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    :try_start_1
    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/android/location/m;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private b(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/16 v3, 0x9

    const/4 v2, 0x4

    if-eqz p2, :cond_0

    invoke-virtual {p2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/m;->a:Lcom/google/android/location/j;

    invoke-virtual {v3, v1}, Lcom/google/android/location/j;->a(Ljava/util/List;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-static {v2}, Lcom/google/android/location/f;->b(Ljava/util/List;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/location/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/android/location/m;->a(Ljava/util/Calendar;Lcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    new-instance v1, Lcom/google/android/location/n;

    sget-object v0, Lcom/google/android/location/w;->i:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-direct {v1, v0}, Lcom/google/android/location/n;-><init>(Lcom/google/android/location/k/b;)V

    invoke-virtual {v1, p1}, Lcom/google/android/location/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/m;->c:Lcom/google/android/location/g;

    iget-object v2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/location/g;->a(JLjava/util/Map;)V

    return-void
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    const/16 v2, 0x3e8

    if-le v0, v2, :cond_0

    const/16 v2, 0xc

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Integer;
    .locals 9

    const/4 v5, 0x0

    const/16 v8, 0x10

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v7, 0x8

    if-eqz p1, :cond_0

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v5

    :goto_0
    return-object v0

    :cond_1
    move v2, v1

    move v3, v0

    :goto_1
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v3

    cmpl-float v6, v3, v2

    if-ltz v6, :cond_5

    move v1, v2

    move v2, v3

    move v3, v4

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    if-lt v3, v0, :cond_4

    add-float v0, v2, v1

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    const/high16 v1, 0x41200000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v5

    goto :goto_0

    :cond_5
    move v3, v4

    goto :goto_2
.end method

.method public a(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/m$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/m$1;-><init>(Lcom/google/android/location/m;Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/m;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
