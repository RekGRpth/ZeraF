.class public Lcom/google/android/location/v;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/v$a;
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/k/b;

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/location/v$a;

.field private final d:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/v$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/k/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/v$a;",
            "Z",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            "J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/v;->a:Lcom/google/android/location/k/b;

    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/location/v;->d:J

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v6

    const/4 v4, 0x0

    if-eqz p2, :cond_1

    sget-object v2, Lcom/google/android/location/k/b;->c:Ljava/util/Comparator;

    invoke-static {p2, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/k/b;

    invoke-virtual {v2, p1}, Lcom/google/android/location/k/b;->a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;

    move-result-object v5

    if-eqz v5, :cond_6

    if-eqz v3, :cond_6

    iget-wide v8, v5, Lcom/google/android/location/k/b;->a:J

    iget-wide v10, v3, Lcom/google/android/location/k/b;->b:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_6

    iget-wide v8, v3, Lcom/google/android/location/k/b;->b:J

    iget-wide v10, v5, Lcom/google/android/location/k/b;->b:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_0

    new-instance v2, Lcom/google/android/location/k/b;

    iget-wide v10, v5, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/google/android/location/k/b;-><init>(JJ)V

    :goto_1
    if-eqz v2, :cond_5

    invoke-virtual {v6, v2}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    add-int/lit8 v3, v4, 0x1

    :goto_2
    move v4, v3

    move-object v3, v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    if-nez v4, :cond_2

    invoke-virtual {v6, p1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    :cond_2
    invoke-virtual {v6}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/k/b;

    invoke-virtual {v2, p1}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object p3, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v$a;

    :cond_3
    iput-object p3, p0, Lcom/google/android/location/v;->c:Lcom/google/android/location/v$a;

    if-eqz p4, :cond_4

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/v;->b:Ljava/util/List;

    :goto_3
    return-void

    :cond_4
    iput-object v3, p0, Lcom/google/android/location/v;->b:Ljava/util/List;

    goto :goto_3

    :cond_5
    move-object v2, v3

    move v3, v4

    goto :goto_2

    :cond_6
    move-object v2, v5

    goto :goto_1
.end method

.method private a(JJLjava/util/List;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/k/b;->d(J)J

    move-result-wide v2

    cmp-long v4, v2, p3

    if-gtz v4, :cond_0

    sub-long/2addr p3, v2

    goto :goto_0

    :cond_0
    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long/2addr v0, p3

    :goto_1
    return-wide v0

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_1
.end method

.method private a(Ljava/util/Calendar;JLjava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iget-object v1, p0, Lcom/google/android/location/v;->a:Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    new-instance v1, Ljava/util/Random;

    iget-wide v2, p0, Lcom/google/android/location/v;->d:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    xor-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    long-to-double v2, p2

    mul-double/2addr v0, v2

    double-to-long v3, v0

    iget-object v0, p0, Lcom/google/android/location/v;->a:Lcom/google/android/location/k/b;

    iget-wide v1, v0, Lcom/google/android/location/k/b;->a:J

    move-object v0, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/v;->a(JJLjava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    const-wide/16 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0}, Lcom/google/android/location/k/b;->a()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method private a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/google/android/location/v;->a(Ljava/util/List;)J

    move-result-wide v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    invoke-direct {p0, p1, v0, v1, p3}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;JLjava/util/List;)J

    move-result-wide v2

    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/location/v;->a:Lcom/google/android/location/k/b;

    invoke-virtual {v0, p2}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gez v4, :cond_3

    move-wide v1, v0

    :goto_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/k/b;->b(J)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Lcom/google/android/location/k/b;

    iget-wide v6, v0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v5, v1, v2, v6, v7}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-virtual {v3, v5}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    goto :goto_1

    :cond_1
    invoke-virtual {v3, v0}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0

    :cond_3
    move-wide v1, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->b(Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()Lcom/google/android/location/v$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/v;->c:Lcom/google/android/location/v$a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SensorCollectionTimeSpan [targetTimeSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/v;->a:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/v;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpanType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/v;->c:Lcom/google/android/location/v$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
