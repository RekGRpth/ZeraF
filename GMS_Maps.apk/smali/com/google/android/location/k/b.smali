.class public Lcom/google/android/location/k/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/location/k/b$1;

    invoke-direct {v0}, Lcom/google/android/location/k/b$1;-><init>()V

    sput-object v0, Lcom/google/android/location/k/b;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 4

    const/4 v2, 0x0

    invoke-static {p1, p2, v2, v2}, Lcom/google/android/location/k/c;->a(IIII)J

    move-result-wide v0

    invoke-static {p3, p4, v2, v2}, Lcom/google/android/location/k/c;->a(IIII)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/k/b;-><init>(JJ)V

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/k/b;->a:J

    iput-wide p3, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, p3, p1

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid time span."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private e(J)Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;
    .locals 7

    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v5, p1, Lcom/google/android/location/k/b;->b:J

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/location/k/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/k/b;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/k/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)V"
        }
    .end annotation

    iget-wide v0, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-wide v0, p1, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Calendar;)Z
    .locals 3

    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/k/b;->e(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/util/Calendar;)Z
    .locals 2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->a(J)Z

    move-result v0

    return v0
.end method

.method public c(J)Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/util/Calendar;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v0

    return v0
.end method

.method public d(J)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/k/b;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    sub-long/2addr v0, p1

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/location/k/b;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/location/k/b;

    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    const-string v0, "TimeSpan: [%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/location/k/b;->a:J

    invoke-static {v3, v4}, Lcom/google/android/location/k/c;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-static {v3, v4}, Lcom/google/android/location/k/c;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
