.class public Lcom/google/android/maps/PlacesRefinementLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/ui/wizard/gj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/PlacesRefinementLinearLayout;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ae_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x57

    const-string v1, "ref"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->a:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPlacesWizardActions(Lcom/google/googlenav/ui/wizard/gj;)V
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gj;

    iput-object v0, p0, Lcom/google/android/maps/PlacesRefinementLinearLayout;->b:Lcom/google/googlenav/ui/wizard/gj;

    return-void
.end method
