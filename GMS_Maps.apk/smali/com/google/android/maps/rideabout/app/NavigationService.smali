.class public Lcom/google/android/maps/rideabout/app/NavigationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static h:Lcom/google/android/maps/rideabout/app/o;

.field private static final o:Landroid/content/IntentFilter;


# instance fields
.field private b:Lcom/google/android/maps/rideabout/app/k;

.field private c:Lbi/d;

.field private d:LU/q;

.field private e:LU/x;

.field private f:Lcom/google/android/maps/rideabout/view/c;

.field private g:LV/m;

.field private i:Ljava/util/EnumSet;

.field private j:Z

.field private final k:Landroid/os/IBinder;

.field private l:Z

.field private m:Z

.field private final n:Lcom/google/android/maps/rideabout/app/h;

.field private final p:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/maps/rideabout/app/NavigationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/rideabout/app/NavigationService;->a:Ljava/lang/String;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.maps.driveabout.app.STARTING_NAVIGATION_INTENT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/maps/rideabout/app/NavigationService;->o:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/maps/rideabout/app/i;

    invoke-direct {v0, p0}, Lcom/google/android/maps/rideabout/app/i;-><init>(Lcom/google/android/maps/rideabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->k:Landroid/os/IBinder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->m:Z

    new-instance v0, Lcom/google/android/maps/rideabout/app/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/rideabout/app/h;-><init>(Lcom/google/android/maps/rideabout/app/NavigationService;Lcom/google/android/maps/rideabout/app/g;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->n:Lcom/google/android/maps/rideabout/app/h;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.googlenav.STOP_TRANSIT_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->p:Landroid/content/Intent;

    return-void
.end method

.method private a(Laa/e;DLbi/d;)LU/x;
    .locals 7

    new-instance v6, LV/l;

    invoke-direct {v6, p1}, LV/l;-><init>(Laa/e;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v6}, LV/l;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, LV/l;->b()LV/g;

    move-result-object v0

    iget-wide v2, v0, LV/g;->c:J

    :cond_0
    new-instance v0, LX/a;

    new-instance v1, Lal/a;

    invoke-direct {v1}, Lal/a;-><init>()V

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, LX/a;-><init>(Lcom/google/googlenav/common/a;JD)V

    new-instance v1, LX/b;

    invoke-direct {v1, v0}, LX/b;-><init>(LX/a;)V

    new-instance v2, LU/Q;

    invoke-direct {v2, p0, v6, v0}, LU/Q;-><init>(Landroid/content/Context;LV/l;LX/a;)V

    new-instance v3, LU/q;

    invoke-direct {v3, v2}, LU/q;-><init>(LU/u;)V

    iput-object v3, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    new-instance v2, LU/C;

    iget-object v3, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    invoke-direct {v2, p4, v3, v0, v1}, LU/C;-><init>(Lbi/d;LU/q;Lcom/google/googlenav/common/a;LX/i;)V

    return-object v2
.end method

.method static synthetic a(Lcom/google/android/maps/rideabout/app/NavigationService;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->p:Landroid/content/Intent;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "com.google.android.maps.rideabout.START_SIMULATION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "event_log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "event_log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "mock_playback"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->h()V

    goto :goto_0
.end method

.method private a(Lax/w;)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v1}, Lax/w;->b(Ljava/io/DataOutput;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v1, "directions"

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No log file provided. Received \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, LV/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Specified log file \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LV/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' does not exist."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-static {p1}, LV/a;->a(Ljava/lang/String;)Laa/e;

    move-result-object v0

    const-wide/high16 v1, 0x3ff0000000000000L

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Laa/e;D)V

    goto :goto_0
.end method

.method private b(Lbi/d;)LU/x;
    .locals 2

    new-instance v0, LU/P;

    invoke-direct {v0}, LU/P;-><init>()V

    new-instance v1, LU/q;

    invoke-direct {v1, v0}, LU/q;-><init>(LU/u;)V

    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    new-instance v0, LU/I;

    sget-object v1, LU/L;->b:LU/L;

    invoke-direct {v0, p1, v1, p0}, LU/I;-><init>(Lbi/d;LU/L;Landroid/content/Context;)V

    invoke-virtual {v0}, LU/I;->b()V

    return-object v0
.end method

.method private b(Lax/w;Lbi/d;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->k()V

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/rideabout/app/NavigationService;->c(Lax/w;Lbi/d;)LU/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->j()V

    invoke-static {}, Lcom/google/android/maps/rideabout/app/q;->z()LaS/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/rideabout/app/q;->z()LaS/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaS/a;->a(Z)V

    :cond_0
    return-void
.end method

.method private c(Lax/w;Lbi/d;)LU/x;
    .locals 4

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    new-instance v1, LU/v;

    invoke-direct {v1, p0, v0}, LU/v;-><init>(Landroid/content/Context;Lcom/google/googlenav/common/a;)V

    new-instance v2, LU/q;

    invoke-direct {v2, v1}, LU/q;-><init>(LU/u;)V

    iput-object v2, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    new-instance v1, LU/C;

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    new-instance v3, LX/h;

    invoke-direct {v3}, LX/h;-><init>()V

    invoke-direct {v1, p2, v2, v0, v3}, LU/C;-><init>(Lbi/d;LU/q;Lcom/google/googlenav/common/a;LX/i;)V

    return-object v1
.end method

.method private h()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Lbi/d;)V

    :cond_0
    return-void
.end method

.method private i()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "Transit navigation should be running before performing simulation"

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->stopSelf()V

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    iget-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    iget-object v2, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->b:Lcom/google/android/maps/rideabout/app/k;

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/rideabout/view/c;->a(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)Lcom/google/android/maps/rideabout/view/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->f:Lcom/google/android/maps/rideabout/view/c;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    invoke-virtual {v0}, LU/q;->a()V

    return-void
.end method

.method private k()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    invoke-interface {v0}, LU/x;->a()V

    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->f:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->f:Lcom/google/android/maps/rideabout/view/c;

    invoke-static {v0}, Lcom/google/android/maps/rideabout/view/c;->a(Lcom/google/android/maps/rideabout/view/c;)V

    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->f:Lcom/google/android/maps/rideabout/view/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    invoke-virtual {v0}, LU/q;->b()V

    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->d:LU/q;

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->g:LV/m;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->g:LV/m;

    invoke-virtual {v0}, LV/m;->b()V

    :cond_3
    iput-object v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->g:LV/m;

    :cond_4
    return-void
.end method

.method private l()Lax/w;
    .locals 4

    const/4 v0, 0x0

    const-string v1, "directions"

    const-string v2, ""

    invoke-static {p0, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-static {v1}, Lcom/google/googlenav/common/util/c;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2, v0}, Lax/w;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    check-cast v0, Lax/w;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/maps/rideabout/view/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->f:Lcom/google/android/maps/rideabout/view/c;

    return-object v0
.end method

.method a(Landroid/content/Context;)Ljava/util/EnumSet;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->i:Ljava/util/EnumSet;

    if-nez v0, :cond_4

    const-class v0, Lcom/google/googlenav/ui/wizard/iw;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    const-string v1, "NotificationVibration"

    invoke-static {p1, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v1, "NotificationVoice"

    invoke-static {p1, v1}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "NotificationVoice"

    invoke-static {p1, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v1, "NotificationRingtone"

    invoke-static {p1, v1, v2}, LX/m;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_3
    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->i:Ljava/util/EnumSet;

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->i:Ljava/util/EnumSet;

    return-object v0
.end method

.method public a(Laa/e;D)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->m:Z

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->k()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Laa/e;DLbi/d;)LU/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->j()V

    return-void
.end method

.method a(Landroid/content/Context;Ljava/util/EnumSet;)V
    .locals 2

    iput-object p2, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->i:Ljava/util/EnumSet;

    const-string v0, "NotificationVibration"

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p2}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    invoke-static {p1, v0, v1}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    const-string v0, "NotificationVoice"

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p2}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    invoke-static {p1, v0, v1}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    const-string v0, "NotificationRingtone"

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v1, p2}, Lcom/google/googlenav/ui/wizard/iw;->a(Ljava/util/EnumSet;)Z

    move-result v1

    invoke-static {p1, v0, v1}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Lax/w;Lbi/d;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->l:Z

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Lax/w;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/rideabout/app/NavigationService;->b(Lax/w;Lbi/d;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lbi/d;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->c:Lbi/d;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->m:Z

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->k()V

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/NavigationService;->b(Lbi/d;)LU/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->e:LU/x;

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->j()V

    return-void
.end method

.method a(Lcom/google/android/maps/rideabout/app/y;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->f()Lcom/google/android/maps/rideabout/app/o;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/rideabout/app/o;->a(Lcom/google/android/maps/rideabout/app/y;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->l:Z

    return v0
.end method

.method public c()V
    .locals 2

    const-string v0, "directions"

    const-string v1, ""

    invoke-static {p0, v0, v1}, LX/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->j:Z

    return v0
.end method

.method e()Ljava/util/EnumSet;
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/iw;->c:Lcom/google/googlenav/ui/wizard/iw;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->j:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->a:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->f()Lcom/google/android/maps/rideabout/app/o;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/maps/rideabout/app/o;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/googlenav/ui/wizard/iw;->b:Lcom/google/googlenav/ui/wizard/iw;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method f()Lcom/google/android/maps/rideabout/app/o;
    .locals 1

    sget-object v0, Lcom/google/android/maps/rideabout/app/NavigationService;->h:Lcom/google/android/maps/rideabout/app/o;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/rideabout/app/p;->g()Lcom/google/android/maps/rideabout/app/p;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/rideabout/app/NavigationService;->h:Lcom/google/android/maps/rideabout/app/o;

    :cond_0
    sget-object v0, Lcom/google/android/maps/rideabout/app/NavigationService;->h:Lcom/google/android/maps/rideabout/app/o;

    return-object v0
.end method

.method g()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->j:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->j:Z

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->k:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/maps/rideabout/app/k;->a(Landroid/app/Service;)Lcom/google/android/maps/rideabout/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->b:Lcom/google/android/maps/rideabout/app/k;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->b:Lcom/google/android/maps/rideabout/app/k;

    const/16 v1, 0x4aa

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x4ea

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/maps/rideabout/app/k;->a(ZZLjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->b:Lcom/google/android/maps/rideabout/app/k;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/k;->b()V

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->g()V

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->l()Lax/w;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    new-instance v1, Lbi/e;

    invoke-direct {v1, v0}, Lbi/e;-><init>(Lax/w;)V

    invoke-virtual {v1}, Lbi/e;->a()Lbi/d;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/rideabout/app/NavigationService;->b(Lax/w;Lbi/d;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->n:Lcom/google/android/maps/rideabout/app/h;

    sget-object v1, Lcom/google/android/maps/rideabout/app/NavigationService;->o:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/rideabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/app/NavigationService;->k()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->b:Lcom/google/android/maps/rideabout/app/k;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/k;->a()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/app/NavigationService;->n:Lcom/google/android/maps/rideabout/app/h;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Lcom/google/android/maps/rideabout/app/q;->z()LaS/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/rideabout/app/q;->z()LaS/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaS/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/app/NavigationService;->a(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0
.end method
