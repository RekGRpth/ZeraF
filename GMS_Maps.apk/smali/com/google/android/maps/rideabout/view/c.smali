.class public Lcom/google/android/maps/rideabout/view/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/y;
.implements Lcom/google/googlenav/ui/view/android/rideabout/i;


# instance fields
.field private final a:LW/h;

.field private final b:LU/x;


# direct methods
.method private constructor <init>(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LW/h;

    invoke-direct {v0, p1, p2, p3}, LW/h;-><init>(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)V

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    iput-object p2, p0, Lcom/google/android/maps/rideabout/view/c;->b:LU/x;

    return-void
.end method

.method public static a(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)Lcom/google/android/maps/rideabout/view/c;
    .locals 2

    new-instance v0, Lcom/google/android/maps/rideabout/view/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/rideabout/view/c;-><init>(Lbi/d;LU/x;Lcom/google/android/maps/rideabout/app/j;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/c;)V

    invoke-interface {p1, v0}, LU/x;->a(LU/y;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/maps/rideabout/view/c;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->g()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->b:LU/x;

    invoke-interface {v0, p0}, LU/x;->b(LU/y;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x61

    const-string v1, "a"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->h()V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LW/k;->a(II)V

    return-void
.end method

.method public a(LU/z;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0, p1}, LW/h;->a(LU/z;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/maps/rideabout/view/d;->a:[I

    iget-object v1, p1, LU/z;->b:LU/A;

    invoke-virtual {v1}, LU/A;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->a(LU/z;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->b(LU/z;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->c(LU/z;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ZIIII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->j()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->i()V

    return-void
.end method

.method public c()V
    .locals 1

    const-string v0, "c"

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/view/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->p()V

    return-void
.end method

.method public d()V
    .locals 1

    const-string v0, "o"

    invoke-direct {p0, v0}, Lcom/google/android/maps/rideabout/view/c;->a(Ljava/lang/String;)V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->x()V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->p()V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->n()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->d()V

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    invoke-virtual {v0}, LW/k;->c()V

    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->b()LW/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LW/k;->a(Z)V

    return-void
.end method

.method public h()LW/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    return-object v0
.end method

.method public i()LU/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/c;->a:LW/h;

    invoke-virtual {v0}, LW/h;->c()LW/j;

    move-result-object v0

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    return-object v0
.end method
