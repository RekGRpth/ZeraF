.class public Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;
.super Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;
.source "SourceFile"


# instance fields
.field private h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f10047e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->h:Landroid/widget/TextView;

    const/16 v1, 0x4d4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->g()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->g()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionView;->b(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/maps/rideabout/view/TransitStepDescriptionLandView;->g()V

    return-void
.end method

.method protected e()I
    .locals 1

    const v0, 0x7f0401c2

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "x %s"

    return-object v0
.end method
