.class public Lcom/google/android/maps/driveabout/vector/VectorMapView;
.super Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/ay;
.implements Lcom/google/android/maps/driveabout/vector/bA;
.implements Lcom/google/android/maps/driveabout/vector/bv;
.implements Lcom/google/android/maps/driveabout/vector/w;
.implements Lo/C;


# instance fields
.field protected a:Lcom/google/android/maps/driveabout/vector/bk;

.field private final b:Lcom/google/android/maps/driveabout/vector/s;

.field private final c:Landroid/content/res/Resources;

.field private d:Lcom/google/android/maps/driveabout/vector/aK;

.field private e:LaD/g;

.field private f:Lcom/google/android/maps/driveabout/vector/bz;

.field private g:Lcom/google/android/maps/driveabout/vector/by;

.field private h:Lcom/google/android/maps/driveabout/vector/bt;

.field private i:Lcom/google/android/maps/driveabout/vector/D;

.field private j:Z

.field private k:LC/b;

.field private l:J

.field private m:Z

.field private n:LG/a;

.field private o:Lcom/google/android/maps/driveabout/vector/u;

.field private p:Lz/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/s;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->C()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/s;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    return-void
.end method

.method private C()V
    .locals 11

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setPreserveEGLContextOnPause(Z)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bt;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/bt;-><init>(Lcom/google/android/maps/driveabout/vector/bv;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    new-instance v0, LaD/g;

    invoke-direct {v0}, LaD/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, v1, v2}, LaD/g;->a(Landroid/content/Context;LaD/m;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setFocusable(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v8, v0, Landroid/util/DisplayMetrics;->density:F

    const/16 v9, 0x100

    const/16 v10, 0x100

    new-instance v0, Lcom/google/android/maps/driveabout/vector/u;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/u;-><init>(Lcom/google/android/maps/driveabout/vector/w;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/16 v6, 0x8

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    const/16 v2, 0x8

    const/16 v3, 0x8

    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/O;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/O;->a(Z)V

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/maps/driveabout/vector/N;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/O;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/vector/N;-><init>([Lcom/google/android/maps/driveabout/vector/O;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    const/4 v7, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p:Lz/k;

    new-instance v0, LC/a;

    sget-object v1, LC/a;->d:LC/b;

    const/4 v5, 0x0

    move v2, v9

    move v3, v10

    move v4, v8

    invoke-direct/range {v0 .. v5}, LC/a;-><init>(LC/b;IIFLjava/lang/Thread;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c()LA/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v5

    new-instance v1, Lcom/google/android/maps/driveabout/vector/aK;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p:Lz/k;

    move-object v4, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/maps/driveabout/vector/aK;-><init>(Lcom/google/android/maps/driveabout/vector/u;Landroid/content/res/Resources;LC/a;Lcom/google/android/maps/driveabout/vector/aZ;Lz/k;Lz/t;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setRenderer(Lcom/google/android/maps/driveabout/vector/ac;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setRenderMode(I)V

    return-void
.end method


# virtual methods
.method public A()Lcom/google/android/maps/driveabout/vector/aq;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public B()Ln/n;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aq;->i()Ln/n;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    return-object v0
.end method

.method public a(LaN/B;)Lo/D;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/T;)Lo/D;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(FF)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v2, v0, v1, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/maps/driveabout/vector/D;->a(FFLC/a;)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(FFF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;FFF)V

    :cond_0
    return-void
.end method

.method protected a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(I)V

    return-void
.end method

.method public a(LS/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1}, LS/b;->a(LS/a;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/D;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aG;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aG;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    return-void
.end method

.method public a(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    return-void
.end method

.method public a(Landroid/view/MotionEvent;FF)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0}, LS/b;->j_()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v0, v4, v5, v1}, LS/b;->b(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_1
    if-ltz v4, :cond_4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->j_()Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez v1, :cond_2

    new-instance v1, LC/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v9

    invoke-direct {v1, v6, v7, v8, v9}, LC/a;-><init>(LC/b;IIF)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v0, v6, v7, v1}, Lcom/google/android/maps/driveabout/vector/D;->b(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->g()V

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b()V

    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->p()LD/a;

    move-result-object v1

    invoke-virtual {v0, v1}, LB/a;->a(LD/a;)V

    :cond_0
    return-void
.end method

.method public b(LS/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1}, LS/b;->b(LS/a;)V

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/D;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d()V

    :cond_0
    return-void
.end method

.method public b(FF)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v2

    invoke-virtual {v2}, LS/b;->j_()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, p1, p2, v1, v1}, LS/b;->d(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v4

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move-object v2, v1

    :goto_1
    if-ltz v5, :cond_4

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->j_()Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v2, :cond_2

    new-instance v2, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v9

    invoke-direct {v2, v1, v7, v8, v9}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v2, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/D;->d(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v4

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public c()LA/c;
    .locals 1

    sget-object v0, LA/c;->a:LA/c;

    return-object v0
.end method

.method public c(Z)Lcom/google/android/maps/driveabout/vector/aA;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    return-object v0
.end method

.method public c(FF)V
    .locals 10

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v3

    invoke-direct {v4, v0, v1, v2, v3}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v4, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v3

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, LS/b;->a_(FFLo/T;LC/a;)Z

    move-result v0

    :cond_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, Lcom/google/android/maps/driveabout/vector/x;->a_(FFLo/T;LC/a;)Z

    move-result v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    invoke-interface {v0, p0, v3}, Lcom/google/android/maps/driveabout/vector/by;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z

    move-result v0

    :cond_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v1, v1, -0x1

    move v6, v0

    :goto_1
    if-nez v6, :cond_5

    if-ltz v1, :cond_5

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->l_()Z

    move-result v9

    if-eqz v9, :cond_4

    check-cast v0, Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v6

    :goto_2
    add-int/lit8 v1, v1, -0x1

    move v6, v0

    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v9}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v9

    if-eq v0, v9, :cond_a

    invoke-virtual {v0, p1, p2, v3, v4}, Lcom/google/android/maps/driveabout/vector/D;->a_(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v7

    goto :goto_2

    :cond_5
    if-nez v6, :cond_8

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->k:LC/b;

    invoke-virtual {v0, v1}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setShouldUpdateFeatureCluster(Z)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/s;->a(FFLo/T;LC/a;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v6, v7

    :cond_8
    if-nez v6, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, v3}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->k:LC/b;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto/16 :goto_0

    :cond_a
    move v0, v6

    goto :goto_2
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->k_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    :cond_0
    return-void
.end method

.method public d(FF)Z
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v1, v0}, Lcom/google/android/maps/driveabout/vector/x;->b(FFLo/T;LC/a;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->d()V

    :cond_0
    return-void
.end method

.method public e(FF)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v3, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v4, v3}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLo/T;LC/a;)Z

    move-result v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    invoke-interface {v0, p0, v4}, Lcom/google/android/maps/driveabout/vector/by;->b(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v6

    if-eq v0, v6, :cond_4

    invoke-virtual {v0, p1, p2, v4, v3}, Lcom/google/android/maps/driveabout/vector/D;->c(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_2
    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, v4}, Lcom/google/android/maps/driveabout/vector/bz;->b(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    return-void
.end method

.method public f(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;FF)V

    :cond_0
    return-void
.end method

.method public g()LS/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    return-object v0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Ljava/util/List;)V

    return-void
.end method

.method public i()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aK;->c(Z)V

    :goto_0
    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aK;->c(Z)V

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public j()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/E;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    :cond_0
    return-void
.end method

.method public k()Lcom/google/android/maps/driveabout/vector/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->j()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    return-object v0
.end method

.method public l()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public m()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    return-void
.end method

.method public n()LC/a;
    .locals 5

    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    return-object v0
.end method

.method public n_()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->n_()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->f()V

    return-void
.end method

.method public o()Lo/aQ;
    .locals 5

    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->B()Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method public o_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->b()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->onFinishInflate()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->C()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    invoke-virtual {v0, p1}, LaD/g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->onWindowFocusChanged(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->k()V

    return-void
.end method

.method public p_()Lcom/google/android/maps/driveabout/vector/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    return-object v0
.end method

.method public q()LF/H;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->l()LF/H;

    move-result-object v0

    return-object v0
.end method

.method public r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->q()V

    :cond_0
    return-void
.end method

.method public s()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public setAllowLongPressGesture(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->f(Z)V

    return-void
.end method

.method public setAllowRotateGesture(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->e(Z)V

    return-void
.end method

.method public setAllowScroll(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->a(Z)V

    return-void
.end method

.method public setAllowSingleTapGesture(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->g(Z)V

    return-void
.end method

.method public setAllowTiltGesture(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->d(Z)V

    return-void
.end method

.method public setAllowZoomGestures(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->b(Z)V

    return-void
.end method

.method public setBaseDistancePenaltyFactorForLabelOverlay(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(I)V

    return-void
.end method

.method public setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/y;)V

    return-void
.end method

.method public setController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/aU;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lw/c;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/b;)V

    return-void
.end method

.method public setCopyrightPadding(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aK;->a(II)V

    return-void
.end method

.method public setDefaultLabelTheme(LG/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n:LG/a;

    return-void
.end method

.method public setDoubleTapZoomsAboutCenter(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->c(Z)V

    return-void
.end method

.method public setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/q;)V

    return-void
.end method

.method public setEventBus(LZ/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LZ/a;)V

    return-void
.end method

.method public setImportantLabelFeatures(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Ljava/util/List;)V

    return-void
.end method

.method public setInterceptingOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/by;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    return-void
.end method

.method public setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    return-void
.end method

.method public setLabelTheme(LG/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LG/a;)V

    return-void
.end method

.method public setModelChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->e()V

    return-void
.end method

.method public setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    return-void
.end method

.method public setShouldUpdateFeatureCluster(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/s;->a(Z)V

    return-void
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bt;->a()V

    return-void
.end method

.method public u()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bt;->b()V

    return-void
.end method

.method public v()Lcom/google/android/maps/driveabout/vector/aK;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    return-object v0
.end method

.method public w()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTheme(LG/a;)V

    return-void
.end method

.method public x()LG/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n:LG/a;

    return-object v0
.end method

.method public y()LaD/g;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    return-object v0
.end method
