.class public Lcom/google/android/maps/driveabout/vector/aZ;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final M:Lg/c;

.field private static final d:LR/a;

.field private static final e:LR/a;


# instance fields
.field private volatile A:Z

.field private final B:Ljava/util/Set;

.field private final C:LR/h;

.field private final D:Lo/T;

.field private E:LC/b;

.field private F:J

.field private G:J

.field private H:J

.field private I:Z

.field private final J:Z

.field private final K:Z

.field private final L:Lg/c;

.field private N:I

.field private O:J

.field private final P:Ljava/util/Set;

.field private final Q:LA/a;

.field private R:Ljava/lang/ref/WeakReference;

.field protected volatile a:Lcom/google/android/maps/driveabout/vector/aU;

.field protected volatile b:Z

.field protected final c:Ly/b;

.field private final f:I

.field private final g:I

.field private final h:Lcom/google/android/maps/driveabout/vector/E;

.field private i:Z

.field private final j:I

.field private final k:I

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:LA/c;

.field private final q:Lu/d;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:[I

.field private final u:Ljava/util/ArrayList;

.field private final v:[I

.field private final w:Lcom/google/android/maps/driveabout/vector/bc;

.field private x:Lg/a;

.field private y:Lk/f;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    invoke-static {v0}, LR/a;->a([I)LR/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    sget-object v0, LF/Y;->a:LR/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    invoke-static {v0, v1}, LR/a;->a(LR/a;LR/a;)LR/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->e:LR/a;

    new-instance v0, Lg/b;

    invoke-direct {v0}, Lg/b;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    return-void
.end method

.method constructor <init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/bc;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/vector/bc;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    new-instance v1, LR/h;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, LR/h;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->C:LR/h;

    new-instance v1, Lo/T;

    invoke-direct {v1}, Lo/T;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    invoke-static {}, Lcom/google/common/collect/dA;->d()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    iput p4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    iput p5, p0, Lcom/google/android/maps/driveabout/vector/aZ;->g:I

    iput-object p7, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    iput p8, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    iput p9, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    iput-boolean p10, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->l:Z

    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->m:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->n:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v1, v2}, Lu/d;->a(LA/b;)V

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LA/c;->h()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ly/b;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aZ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ly/b;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    new-array v1, p4, [I

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p6, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p6, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0xfa0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    new-instance v2, Lcom/google/android/maps/driveabout/vector/ba;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/vector/ba;-><init>(Lcom/google/android/maps/driveabout/vector/aZ;)V

    invoke-virtual {v1, v2}, Lu/d;->a(Lu/i;)V

    return-void
.end method

.method private a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v1, :cond_1

    move v1, v0

    :goto_0
    if-ge p3, p4, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    invoke-interface {v0, p1, p2}, LF/T;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    aput v0, v2, p3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    aget v0, v0, p3

    or-int/2addr v0, v1

    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v0, v1, p2}, LA/c;->a(ILcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public static a(Landroid/content/res/Resources;I)I
    .locals 3

    const v1, 0x64140

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v2

    if-ge v0, v1, :cond_0

    move v0, v1

    :cond_0
    div-int/lit16 v2, p1, 0x100

    int-to-float v2, v2

    mul-float/2addr v2, v2

    mul-int/lit8 v0, v0, 0x18

    div-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v0, v0

    return v0
.end method

.method public static a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 2

    const/4 v1, 0x1

    sget-object v0, LA/c;->a:LA/c;

    if-eq p0, v0, :cond_0

    sget-object v0, LA/c;->b:LA/c;

    if-eq p0, v0, :cond_0

    sget-object v0, LA/c;->c:LA/c;

    if-ne p0, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {p0, v0, v1, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    if-nez p1, :cond_0

    sget-object v1, LA/c;->j:LA/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v13, 0x1

    :goto_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    if-nez v13, :cond_1

    sget-object v1, LA/c;->o:LA/c;

    move-object/from16 v0, p0

    if-eq v0, v1, :cond_1

    sget-object v1, LA/c;->p:LA/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_3

    :cond_1
    const/16 v16, 0x1

    :goto_1
    const/4 v4, 0x0

    if-eqz p1, :cond_4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aZ;->e:LR/a;

    move-object v2, v1

    :goto_2
    if-eqz v13, :cond_5

    if-eqz v16, :cond_5

    const/4 v1, 0x1

    :goto_3
    new-instance v7, Lu/a;

    const/16 v3, 0x8

    invoke-direct {v7, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    new-instance v3, Lu/d;

    new-instance v1, LB/e;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v4, v2}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v1, v7, v2}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    new-instance v4, Lk/e;

    invoke-direct {v4}, Lk/e;-><init>()V

    const/16 v7, 0x8

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->c:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p0

    move v14, v13

    move/from16 v15, p2

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1

    :cond_2
    const/4 v13, 0x0

    goto :goto_0

    :cond_3
    const/16 v16, 0x0

    goto :goto_1

    :cond_4
    sget-object v1, LR/a;->a:LR/a;

    move-object v2, v1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ln/s;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 10

    const/16 v7, 0x100

    const/4 v5, 0x4

    invoke-static {p1, v7}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v0

    mul-int/lit8 v3, v0, 0x2

    mul-int/lit8 v4, v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lu/a;

    invoke-direct {v2, v5, v4, v1, v0}, Lu/a;-><init>(IIZZ)V

    new-instance v1, Lu/d;

    sget-object v0, LA/c;->n:LA/c;

    invoke-direct {v1, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aq;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/E;->g:Lcom/google/android/maps/driveabout/vector/E;

    move-object v8, p0

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/maps/driveabout/vector/aq;-><init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;ILandroid/content/Context;Ln/s;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aq;->e()V

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v1, 0x0

    new-instance v2, Lu/a;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    new-instance v3, Lu/d;

    sget-object v1, LA/c;->m:LA/c;

    invoke-direct {v3, v1, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v2, LA/c;->m:LA/c;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->i:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method public static a(Lg/c;LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v4, Lu/a;

    const/4 v3, 0x0

    invoke-direct {v4, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    new-instance v3, Lu/d;

    new-instance v1, LB/e;

    sget-object v7, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2, v7}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1, v4, v2}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/i;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->j:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p1

    move-object/from16 v4, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/i;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method public static a(Landroid/content/res/Resources;LA/c;)Lcom/google/android/maps/driveabout/vector/bd;
    .locals 13

    const/16 v7, 0x100

    const/4 v5, 0x0

    invoke-static {p0, v7}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v3

    mul-int/lit8 v4, v3, 0x2

    const/4 v8, 0x1

    new-instance v0, Lu/a;

    invoke-direct {v0, v5, v4, v5, v8}, Lu/a;-><init>(IIZZ)V

    new-instance v1, Lu/d;

    invoke-direct {v1, p1, v0}, Lu/d;-><init>(LA/c;Lu/a;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/bd;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/E;->f:Lcom/google/android/maps/driveabout/vector/E;

    move v9, v5

    move v10, v5

    move v11, v5

    move v12, v5

    invoke-direct/range {v0 .. v12}, Lcom/google/android/maps/driveabout/vector/bd;-><init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IZZZZZ)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aZ;)Lu/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    return-object v0
.end method

.method private a(LC/a;Ljava/util/Collection;ILjava/util/Set;)V
    .locals 8

    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v4

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->c()V

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-gt v3, p3, :cond_3

    if-ne v3, p3, :cond_4

    const/4 v0, 0x0

    move-object v2, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v1, v0}, Lu/d;->a(Lo/aq;)LF/T;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LD/a;

    invoke-interface {v6, p1, v1}, LF/T;->a(LC/a;LD/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-interface {v6, v1}, LF/T;->a(Z)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v7, v1, v3

    add-int/lit8 v7, v7, 0x1

    aput v7, v1, v3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v7, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    if-ne v1, v7, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    if-eq v0, v1, :cond_3

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_8

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->d()V

    monitor-exit v4

    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move-object v2, v0

    goto :goto_1

    :cond_5
    if-eqz v6, :cond_6

    invoke-interface {v6}, LF/T;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_6
    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    invoke-interface {v1, v0, v6}, Lg/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    if-nez v3, :cond_0

    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object p2, v2

    goto/16 :goto_0
.end method

.method private a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;Lo/aq;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/high16 v1, 0x40000000

    invoke-virtual {p4}, Lo/aq;->b()I

    move-result v2

    shr-int/2addr v1, v2

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p4}, Lo/aq;->g()Lo/T;

    move-result-object v2

    int-to-float v1, v1

    invoke-static {p1, p2, v2, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    invoke-static {p3}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v1

    aget v2, v1, v6

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    invoke-interface {v0, v2, v3, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method public static b(Landroid/content/res/Resources;I)I
    .locals 4

    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    add-int/lit8 v0, v0, 0x2

    mul-int/2addr v0, v1

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/aZ;)LR/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->C:LR/h;

    return-object v0
.end method

.method public static b(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 17

    const/16 v1, 0x100

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/16 v10, 0x180

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v1, 0x0

    new-instance v2, Lu/a;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    new-instance v3, Lu/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x4

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method private b(I)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget v3, v3, LA/c;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "n="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 17

    const/16 v10, 0x14c

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v1, 0x0

    new-instance v2, Lu/a;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    new-instance v3, Lu/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x2

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aZ;->e()V

    return-void
.end method

.method private e()V
    .locals 4

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/16 v0, 0x6e

    const-string v1, "l"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    :cond_0
    return-void
.end method

.method private h()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v2}, Lu/d;->k()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v0, v2}, LF/T;->a(Lcom/google/googlenav/common/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->i()Lo/ad;

    move-result-object v3

    invoke-virtual {p1, v3}, Lo/aQ;->b(Lo/ae;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    invoke-interface {v0, v3}, LF/T;->a(Ly/b;)V

    invoke-interface {v0, p2}, LF/T;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->b()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_2
    instance-of v3, v0, LF/Y;

    if-eqz v3, :cond_0

    check-cast v0, LF/Y;

    invoke-virtual {v0, p3}, LF/Y;->a(Ljava/util/Set;)Z

    goto :goto_0

    :cond_3
    return v1
.end method

.method public a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LA/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LA/c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    return-void
.end method

.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, p2, v0, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    invoke-interface {v0, p1, v2, p3}, LF/T;->a(LC/a;ILjava/util/Collection;)V

    invoke-interface {v0, p1, p4}, LF/T;->a(LC/a;Ljava/util/Collection;)V

    invoke-interface {v0}, LF/T;->h()I

    move-result v0

    if-le v0, v1, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    array-length v0, p5

    if-lez v0, :cond_1

    aput v1, p5, v4

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a(LC/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 22

    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v3

    if-lez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v14

    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v15

    new-instance v16, Lcom/google/android/maps/driveabout/vector/aT;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/r;)V

    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v3, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/driveabout/vector/aZ;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, LF/T;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_1

    :cond_4
    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lu/d;->a(Ljava/util/List;)V

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->m:Z

    if-eqz v3, :cond_b

    sget-object v3, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v14, v3, :cond_7

    sget-object v3, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v14, v3, :cond_b

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_b

    :cond_8
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->c()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    move v5, v3

    :goto_2
    invoke-virtual/range {p1 .. p1}, LD/a;->I()I

    move-result v3

    if-lez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v3}, LA/c;->h()Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    if-eqz v3, :cond_c

    invoke-virtual/range {p1 .. p1}, LD/a;->J()Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    move v13, v3

    :goto_3
    if-eqz v13, :cond_9

    invoke-virtual/range {p1 .. p1}, LD/a;->w()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v6, 0x0

    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->d()Z

    move-result v17

    move v12, v3

    :goto_4
    if-ltz v12, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v3, v3, v12

    if-lez v3, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v3, v3, v12

    sub-int v7, v8, v3

    if-eqz v5, :cond_d

    move v4, v7

    :goto_5
    if-ge v4, v8, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    invoke-interface {v3}, LF/T;->f()Z

    move-result v9

    if-nez v9, :cond_a

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v9

    invoke-virtual {v9}, Lo/aq;->b()I

    move-result v9

    const/16 v10, 0xe

    if-lt v9, v10, :cond_a

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;Lo/aq;)V

    :cond_a
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    :cond_b
    const/4 v3, 0x0

    move v5, v3

    goto :goto_2

    :cond_c
    const/4 v3, 0x0

    move v13, v3

    goto :goto_3

    :cond_d
    if-eqz v13, :cond_f

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e01

    const/16 v9, 0x1e01

    const/16 v10, 0x1e01

    invoke-interface {v3, v4, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x7f

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    move v4, v7

    :goto_6
    if-ge v4, v8, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    const/16 v10, 0x200

    add-int/lit8 v11, v4, 0x1

    const/16 v18, 0x7f

    move/from16 v0, v18

    invoke-interface {v9, v10, v11, v0}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->i()Lo/ad;

    move-result-object v3

    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v9

    invoke-virtual {v3}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v9, v3}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-static/range {p1 .. p2}, LF/U;->a(LD/a;LC/a;)V

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    :cond_e
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e00

    const/16 v9, 0x1e00

    const/16 v10, 0x1e00

    invoke-interface {v3, v4, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v14, v7, v8}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I

    move-result v4

    const/4 v3, 0x0

    move v10, v3

    move v11, v4

    :goto_7
    if-eqz v11, :cond_17

    and-int/lit8 v3, v11, 0x1

    if-eqz v3, :cond_16

    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    const/4 v3, 0x1

    shl-int v18, v3, v10

    move v9, v7

    :goto_8
    if-ge v9, v8, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v4

    invoke-virtual {v4}, Lo/aq;->k()Lo/aB;

    move-result-object v19

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    move-object/from16 v20, v0

    sget-object v21, LA/c;->n:LA/c;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_11

    if-eqz v19, :cond_11

    sget-object v4, Lo/av;->c:Lo/av;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v4

    check-cast v4, Lo/E;

    if-nez v4, :cond_10

    const/4 v4, 0x0

    :goto_9
    invoke-virtual {v15, v4}, Lcom/google/android/maps/driveabout/vector/aH;->a(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v4

    if-nez v4, :cond_11

    move v3, v6

    :goto_a
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    move v6, v3

    goto :goto_8

    :cond_10
    invoke-virtual {v4}, Lo/E;->b()Lo/r;

    move-result-object v4

    goto :goto_9

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    move-object/from16 v19, v0

    aget v19, v19, v9

    and-int v19, v19, v18

    if-eqz v19, :cond_1b

    if-eqz v13, :cond_12

    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    const/16 v19, 0x202

    add-int/lit8 v20, v9, 0x1

    const/16 v21, 0x7f

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v6, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    :cond_12
    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v6

    invoke-virtual {v6}, Lo/aq;->i()Lo/ad;

    move-result-object v6

    invoke-virtual {v6}, Lo/ad;->d()Lo/T;

    move-result-object v6

    if-eqz v4, :cond_13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v4, v0, v1, v2, v6}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    :cond_13
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    if-eqz v4, :cond_14

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_14
    const/4 v3, 0x1

    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    goto :goto_a

    :cond_15
    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    :cond_16
    add-int/lit8 v3, v10, 0x1

    ushr-int/lit8 v4, v11, 0x1

    move v10, v3

    move v11, v4

    goto/16 :goto_7

    :cond_17
    if-eqz v17, :cond_1a

    if-eqz v6, :cond_1a

    :cond_18
    if-eqz v13, :cond_19

    invoke-virtual/range {p1 .. p1}, LD/a;->x()V

    :cond_19
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lu/d;->b(Ljava/util/List;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    goto/16 :goto_0

    :cond_1a
    move v3, v7

    :goto_b
    add-int/lit8 v4, v12, -0x1

    move v12, v4

    move v8, v3

    goto/16 :goto_4

    :cond_1b
    move v3, v6

    goto/16 :goto_a

    :cond_1c
    move v3, v8

    goto :goto_b
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 5

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LD/a;)V

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->a:Lcom/google/android/maps/driveabout/vector/aU;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-interface {v0, v1, v2, v3, v4}, Lg/c;->a(LA/c;IZLA/b;)Lg/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-interface {v0, v1, v2, v3}, Lg/c;->a(LA/c;ZLA/b;)Lk/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    instance-of v0, v0, Lk/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    check-cast v0, Lk/f;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad out-of-bounds coord generator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected a(Lg/a;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    return-void
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    return-void
.end method

.method public a(Lo/at;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v0, p1}, LA/a;->a(Lo/at;)Z

    move-result v0

    return v0
.end method

.method b(Lo/T;)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    if-nez v0, :cond_0

    const/high16 v0, 0x41a80000

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0, p1}, Lg/a;->a(Lo/T;)F

    move-result v0

    goto :goto_0
.end method

.method protected b(LC/a;)Ljava/util/Set;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->i:Z

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 11

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    invoke-virtual {p1, v0}, LC/a;->a(Lo/T;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0, p1}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-virtual {p1}, LC/a;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bc;->a(Lo/T;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    int-to-float v0, v0

    invoke-virtual {p1}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v8, v0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->h()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    if-eqz v0, :cond_2

    new-instance v3, LC/a;

    invoke-virtual {p1}, LC/a;->k()I

    move-result v1

    invoke-virtual {p1}, LC/a;->k()I

    move-result v2

    invoke-virtual {p1}, LC/a;->m()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, LC/a;-><init>(LC/b;IIF)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    new-instance v2, Lo/T;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v6

    invoke-direct {v2, v6}, Lo/T;-><init>(Lo/T;)V

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v6, v3}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v3

    iget-boolean v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    invoke-virtual/range {v0 .. v6}, Lu/d;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->b()I

    move-result v1

    invoke-direct {p0, p1, v7, v9, v10}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Ljava/util/Collection;ILjava/util/Set;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->i:Z

    if-eqz v0, :cond_7

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    const/4 v3, 0x0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    new-instance v5, LF/j;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, LF/j;-><init>(Lo/aq;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->e()V

    goto :goto_0

    :cond_3
    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    invoke-virtual {p1}, LC/a;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v3}, Lg/a;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v3}, LA/a;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_1

    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;)Ljava/util/Set;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    new-instance v2, Lo/T;

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-direct {v2, v3}, Lo/T;-><init>(Lo/T;)V

    iget-boolean v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    move-object v3, v7

    invoke-virtual/range {v0 .. v6}, Lu/d;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    invoke-virtual {p1}, LC/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0}, Lg/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v0}, LA/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    invoke-virtual {v0, p1}, Lk/f;->b(LC/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    const/4 v3, 0x0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    new-instance v5, LF/j;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, LF/j;-><init>(Lo/aq;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->b()I

    move-result v0

    sub-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v1, :cond_a

    if-nez v0, :cond_a

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bb;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_5
    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bb;->a(Z)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LF/T;->a(Z)V

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->a:Lcom/google/android/maps/driveabout/vector/aU;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    return-void
.end method

.method public i_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->g()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    return-void
.end method

.method public j()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lu/d;->a(J)V

    return-void
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    return v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    return v0
.end method

.method public m()Lu/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    return-object v0
.end method

.method public n()LA/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    return v0
.end method

.method public r()Lg/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    return-object v0
.end method

.method public s()LB/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->f()LB/e;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "tileType"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isBase"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTilesPerView"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTilesToFetch"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "drawOrder"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "fetchMissingAncestorTiles"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->l:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "prefetchAncestors"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "tileSize"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isContributingLabels"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTileSize"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->a:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method
