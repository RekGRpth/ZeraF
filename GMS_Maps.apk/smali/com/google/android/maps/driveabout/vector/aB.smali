.class Lcom/google/android/maps/driveabout/vector/aB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# static fields
.field static final a:Lz/L;


# instance fields
.field final b:Lz/r;

.field final c:Ljava/util/List;

.field final d:Ljava/lang/ref/WeakReference;

.field final e:Landroid/content/res/Resources;

.field f:Lz/c;

.field g:LC/a;

.field final h:Lh/l;

.field i:Z

.field final j:Lo/S;

.field k:Lcom/google/android/maps/driveabout/vector/aC;

.field l:I

.field final m:Ljava/util/Map;

.field n:F

.field o:F

.field p:F

.field q:F

.field r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lz/L;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v2, v2, v1}, Lz/L;-><init>(FFF)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aB;->a:Lz/L;

    return-void
.end method


# virtual methods
.method declared-synchronized a(LD/a;I)LD/b;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    new-instance v0, LD/b;

    invoke-direct {v0, p1, v1}, LD/b;-><init>(LD/a;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->e:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, LD/b;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method declared-synchronized a(FFF)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aB;->n:F

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aB;->o:F

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/aB;->p:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lz/c;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    sget-object v0, Lz/c;->a:Lz/P;

    invoke-interface {p1, p0, v0}, Lz/c;->a(Lz/b;Lz/O;)V

    return-void
.end method

.method public declared-synchronized b(Lz/c;)V
    .locals 8

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->h:Lh/l;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lh/l;->a(J)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    :cond_2
    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->r:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v4, v4, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v5, v5, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    if-eq v4, v5, :cond_4

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v2}, Lo/S;->j()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    :goto_2
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    if-eq v3, v2, :cond_3

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    invoke-virtual {p0, v0, v3}, Lcom/google/android/maps/driveabout/vector/aB;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lz/r;->a(Lz/o;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->h:Lh/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-interface {v0, v2}, Lh/l;->a(Lo/S;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->g:LC/a;

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v0}, Lo/S;->c()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v2, v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/r;

    invoke-virtual {v0, v3, v2}, Lz/r;->b(Lo/T;F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    goto :goto_2

    :cond_6
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->g:LC/a;

    invoke-virtual {v2, v3, v0}, LC/a;->a(Lo/T;Z)F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->o:F

    move v2, v0

    :goto_4
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->q:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    mul-float/2addr v0, v4

    mul-float/2addr v0, v2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    invoke-virtual {v1, v3, v0}, Lz/r;->a(Lo/T;F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v1}, Lo/S;->b()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lz/r;->a(F)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    sget-object v1, Lz/k;->a:Lz/P;

    invoke-interface {v0, p0, v1}, Lz/c;->a(Lz/b;Lz/O;)V

    goto/16 :goto_0

    :cond_8
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->n:F

    move v2, v0

    goto :goto_4

    :cond_9
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->p:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method
