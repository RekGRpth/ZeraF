.class Lcom/google/android/maps/driveabout/vector/B;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# instance fields
.field a:LD/a;

.field b:Lz/c;

.field c:LC/a;

.field d:Ljava/util/List;

.field final synthetic e:Lcom/google/android/maps/driveabout/vector/A;


# direct methods
.method private a(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LF/H;->b(I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(LF/H;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {p1, v0}, LF/H;->c(LD/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lz/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/B;->b:Lz/c;

    return-void
.end method

.method public declared-synchronized b(Lz/c;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/B;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/B;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {v0, v3}, LF/H;->a(LD/a;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {v0, v3}, LF/H;->b(LD/a;)V

    const/16 v3, 0xff

    invoke-virtual {v0, v3}, LF/H;->b(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void
.end method
