.class public Lcom/google/android/maps/driveabout/vector/aV;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lo/ao;

.field public static final b:Lcom/google/android/maps/driveabout/vector/aX;

.field public static final c:Lcom/google/android/maps/driveabout/vector/aX;

.field public static final d:Lcom/google/android/maps/driveabout/vector/aX;


# instance fields
.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Path;

.field private h:LD/c;

.field private i:Z

.field private final j:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aX;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aX;-><init>(Lcom/google/android/maps/driveabout/vector/aW;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aX;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aX;-><init>(Lcom/google/android/maps/driveabout/vector/aW;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aV;->c:Lcom/google/android/maps/driveabout/vector/aX;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aX;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aX;-><init>(Lcom/google/android/maps/driveabout/vector/aW;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/aX;

    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->g:Landroid/graphics/Path;

    new-instance v0, LD/c;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LD/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    const v0, 0x40066666

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    return-void
.end method

.method private a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIIIZ)LD/b;
    .locals 12

    move-object/from16 v0, p4

    invoke-direct {p0, p3, v0}, Lcom/google/android/maps/driveabout/vector/aV;->a(Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;)V

    const/high16 v1, 0x3fc00000

    mul-float v1, v1, p5

    float-to-int v1, v1

    int-to-float v5, v1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    if-eqz p9, :cond_4

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    move v8, v1

    :goto_0
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v6, p9

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v1, v2

    const v3, 0x3f820c4a

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v9, v2, 0x1

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v10, v1

    const/16 v1, 0x8

    invoke-static {v9, v1}, LD/b;->c(II)I

    move-result v2

    const/16 v1, 0x8

    invoke-static {v10, v1}, LD/b;->c(II)I

    move-result v1

    invoke-virtual {p1}, LD/a;->K()I

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {p1}, LD/a;->K()I

    move-result v3

    if-le v1, v3, :cond_8

    :cond_0
    const-string v3, "TextGenerator texture too large"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because of string "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, LD/a;->K()I

    move-result v2

    invoke-virtual {p1}, LD/a;->K()I

    move-result v1

    move v3, v2

    move v2, v1

    :goto_1
    const/high16 v1, -0x1000000

    move/from16 v0, p6

    if-eq v0, v1, :cond_1

    const/4 v1, -0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_5

    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    :goto_2
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v4

    invoke-virtual {v4, v3, v2, v1}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    move/from16 v0, p8

    invoke-virtual {v11, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v2, v11}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v1, v1

    add-float/2addr v1, v8

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v1, v3

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000

    mul-float/2addr v4, v8

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz p7, :cond_6

    const/4 v3, 0x0

    cmpl-float v3, v8, v3

    if-lez v3, :cond_6

    const/4 v6, 0x1

    :goto_3
    if-eqz p6, :cond_7

    const/4 v7, 0x1

    :goto_4
    float-to-double v3, v8

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-float v3, v3

    float-to-int v3, v3

    int-to-float v4, v3

    int-to-float v5, v1

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/maps/driveabout/vector/aV;->a(Landroid/graphics/Canvas;Ljava/lang/String;FFZZ)V

    new-instance v1, LD/b;

    invoke-direct {v1, p1}, LD/b;-><init>(LD/a;)V

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aV;->i:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LD/b;->e(Z)V

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LD/b;->c(Z)V

    invoke-virtual {v1, v11, v9, v10}, LD/b;->a(Landroid/graphics/Bitmap;II)V

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aV;->i:Z

    if-nez v2, :cond_3

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    return-object v1

    :cond_4
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_0

    :cond_5
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_3

    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    :cond_8
    move v3, v2

    move v2, v1

    goto/16 :goto_1
.end method

.method private a(Landroid/graphics/Canvas;Ljava/lang/String;FFZZ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aV;->g:Landroid/graphics/Path;

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->g:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    if-eqz p6, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->g:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;)V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/aX;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lo/ao;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {p2}, Lo/ao;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;F)F
    .locals 1

    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/driveabout/vector/aV;->a(Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;
    .locals 11

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aY;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/aY;-><init>(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v1, v0}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LD/b;

    if-nez v1, :cond_1

    if-nez p7, :cond_0

    if-eqz p8, :cond_2

    :cond_0
    const/4 v10, 0x1

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIIIZ)LD/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v2, v0, v1}, LD/c;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v1}, LD/b;->f()V

    return-object v1

    :cond_2
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;
    .locals 8

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aY;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/aY;-><init>(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v1, v0}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LD/b;->f()V

    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v0}, LD/c;->f()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    new-instance v0, LD/c;

    invoke-direct {v0, p1}, LD/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F
    .locals 7

    const/high16 v6, 0x3f800000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZF)[F

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZF)[F
    .locals 7

    const/high16 v6, 0x40000000

    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/driveabout/vector/aV;->a(Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v1, v3

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-float v3, v3

    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v1, v4

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float v0, v4, v0

    const/high16 v4, 0x3f800000

    sub-float v4, p6, v4

    mul-float/2addr v4, v3

    if-eqz p5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_0

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    add-float/2addr v1, v5

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    add-float/2addr v0, v5

    :cond_0
    add-float v5, v1, v0

    add-float/2addr v3, v5

    div-float v5, v4, v6

    sub-float/2addr v1, v5

    div-float/2addr v4, v6

    sub-float/2addr v0, v4

    const/4 v4, 0x4

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v2, 0x1

    aput v3, v4, v2

    const/4 v2, 0x2

    aput v1, v4, v2

    const/4 v1, 0x3

    aput v0, v4, v1

    return-object v4
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aV;->h:LD/c;

    invoke-virtual {v1}, LD/c;->f()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    const/16 v2, 0x8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, LD/c;->a(I)V

    return-void
.end method

.method public b(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [F

    array-length v0, v4

    if-nez v0, :cond_0

    move-object v0, v4

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/driveabout/vector/aV;->a(Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    if-eqz p5, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    :goto_1
    move v2, v0

    move v0, v1

    :goto_2
    array-length v3, v4

    if-ge v0, v3, :cond_2

    aget v3, v4, v0

    add-float/2addr v3, v2

    aput v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    if-eqz p5, :cond_3

    aget v0, v4, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    sub-float/2addr v0, v2

    aput v0, v4, v1

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget v1, v4, v0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aV;->j:F

    add-float/2addr v1, v2

    aput v1, v4, v0

    :cond_3
    move-object v0, v4

    goto :goto_0
.end method
