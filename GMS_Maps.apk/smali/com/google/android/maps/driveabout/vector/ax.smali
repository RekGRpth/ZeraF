.class public Lcom/google/android/maps/driveabout/vector/ax;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/ArrayList;

.field private e:Ly/d;

.field private final f:I


# direct methods
.method public constructor <init>(ILcom/google/android/maps/driveabout/vector/x;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    invoke-virtual {v0}, Ly/d;->d()Ly/g;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Ly/g;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ly/g;->a()LF/m;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v0, v1, :cond_0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->p()V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/ax;->e()V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-virtual {p1}, LD/a;->A()V

    invoke-virtual {v0, p1, p2, p3}, LF/m;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    invoke-virtual {p1}, LD/a;->B()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/List;FFLo/T;LC/a;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    instance-of v2, v0, Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v2

    if-ge v2, p6, :cond_0

    new-instance v3, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v3, v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public a(Ly/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->p:Lcom/google/android/maps/driveabout/vector/E;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->l:Lcom/google/android/maps/driveabout/vector/E;

    goto :goto_0
.end method
