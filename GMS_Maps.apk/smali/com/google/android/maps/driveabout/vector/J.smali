.class public Lcom/google/android/maps/driveabout/vector/J;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/maps/driveabout/vector/E;


# instance fields
.field private final b:Lcom/google/android/maps/driveabout/vector/K;

.field private c:Lcom/google/android/maps/driveabout/vector/K;

.field private d:Lcom/google/android/maps/driveabout/vector/K;

.field private final e:Ljava/util/List;

.field private f:Lo/ad;

.field private g:F

.field private h:F

.field private i:I

.field private final j:LE/o;

.field private final k:LE/d;

.field private final l:LE/i;

.field private m:Lcom/google/android/maps/driveabout/vector/E;

.field private final n:F

.field private final o:I

.field private p:Z

.field private q:Z

.field private final r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->k:Lcom/google/android/maps/driveabout/vector/E;

    sput-object v0, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method public constructor <init>(Lo/X;II[IF)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/K;

    invoke-direct {v0, p1, p4}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    iput p5, p0, Lcom/google/android/maps/driveabout/vector/J;->n:F

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    new-array v0, v2, [Lo/X;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, v2}, Lx/h;->a(Ljava/util/List;Z)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    invoke-static {v0, v2}, Lx/h;->b(Ljava/util/List;Z)I

    move-result v0

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/J;->o:I

    new-instance v1, LE/o;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    invoke-direct {v1, v2}, LE/o;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    new-instance v1, LE/i;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    invoke-direct {v1, v2}, LE/i;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    new-instance v1, LE/d;

    invoke-direct {v1, v0}, LE/d;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method static a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v0, p1, p2}, Lo/X;->a(FI)[Z

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    aget v4, v4, v0

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    if-eq v4, v5, :cond_0

    aput-boolean v1, v3, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    move v1, v2

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_3

    aget-boolean v4, v3, v0

    if-eqz v4, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-ne v1, v0, :cond_4

    :goto_2
    return-object p0

    :cond_4
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-eqz v4, :cond_5

    new-array v0, v1, [I

    :cond_5
    mul-int/lit8 v1, v1, 0x3

    new-array v4, v1, [I

    new-instance v5, Lo/T;

    invoke-direct {v5}, Lo/T;-><init>()V

    move v1, v2

    :goto_3
    array-length v6, v3

    if-ge v2, v6, :cond_8

    aget-boolean v6, v3, v2

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v6, v2, v5}, Lo/X;->a(ILo/T;)V

    invoke-virtual {v5, v4, v1}, Lo/T;->a([II)V

    if-eqz v0, :cond_6

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    aget v6, v6, v2

    aput v6, v0, v1

    :cond_6
    add-int/lit8 v1, v1, 0x1

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    new-instance p0, Lcom/google/android/maps/driveabout/vector/K;

    invoke-static {v4}, Lo/X;->a([I)Lo/X;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    goto :goto_2
.end method

.method private a(LD/a;LC/a;)V
    .locals 4

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-virtual {p1}, LD/a;->p()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    invoke-static {p1, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/K;LC/a;)V
    .locals 10

    const/4 v9, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v5

    invoke-virtual {p2}, LC/a;->y()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/J;->n:F

    mul-float v2, v0, v1

    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    const/high16 v6, 0x3f800000

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    if-eqz v1, :cond_0

    iget-object v5, p1, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    :goto_0
    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v1}, Lo/X;->b()I

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/J;->o:I

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    move-object v1, v0

    invoke-virtual/range {v1 .. v6}, Lx/h;->a(IZI[ILE/k;)V

    return-void

    :cond_0
    move-object v5, v9

    goto :goto_0
.end method

.method private a(LC/a;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa00000

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    monitor-exit p0

    :goto_0
    return v1

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(LD/a;LC/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0, p1}, LE/o;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual {v0, p1}, LE/d;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/K;

    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;LC/a;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    return-void
.end method

.method private b(LC/a;)Z
    .locals 4

    const/high16 v3, 0x40000000

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    div-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->c()Lo/ae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/ad;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)I
    .locals 2

    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method private c(LC/a;)V
    .locals 8

    const v7, 0x1fffffff

    const/high16 v6, -0x20000000

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/J;->a(F)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lo/aQ;->b()Lo/ad;

    move-result-object v1

    invoke-virtual {v1}, Lo/ad;->g()I

    move-result v3

    invoke-virtual {v1}, Lo/ad;->h()I

    move-result v4

    const v5, 0x71c71c7

    if-gt v3, v5, :cond_0

    if-le v4, v5, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lo/ad;

    new-instance v1, Lo/T;

    invoke-direct {v1, v6, v6}, Lo/T;-><init>(II)V

    new-instance v3, Lo/T;

    invoke-direct {v3, v7, v7}, Lo/T;-><init>(II)V

    invoke-direct {v0, v1, v3}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    :cond_1
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/K;

    int-to-float v4, v1

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Lo/T;

    mul-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, v4, 0x4

    invoke-direct {v5, v3, v4}, Lo/T;-><init>(II)V

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v5}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {v1}, Lo/ad;->e()Lo/T;

    move-result-object v1

    invoke-virtual {v1, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {v3, v3}, Lo/T;->j(Lo/T;)V

    invoke-virtual {v1, v1}, Lo/T;->j(Lo/T;)V

    new-instance v4, Lo/ad;

    invoke-direct {v4, v3, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v4, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    new-instance v1, Lo/h;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-direct {v1, v3}, Lo/h;-><init>(Lo/ae;)V

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-nez v3, :cond_3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v1, v0, v3}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    new-instance v5, Lcom/google/android/maps/driveabout/vector/K;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    invoke-virtual {v1, v3, v0, v4, v5}, Lo/h;->a(Lo/X;[ILjava/util/List;Ljava/util/List;)V

    move v3, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    new-instance v7, Lcom/google/android/maps/driveabout/vector/K;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    invoke-direct {v7, v0, v1}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    return-void
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    int-to-float v1, v1

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->d:Lcom/google/android/maps/driveabout/vector/K;

    :cond_0
    return-void
.end method


# virtual methods
.method a(F)Lcom/google/android/maps/driveabout/vector/K;
    .locals 1

    const/high16 v0, 0x41200000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/J;->e()V

    const/high16 v0, 0x40c00000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->d:Lcom/google/android/maps/driveabout/vector/K;

    goto :goto_0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/J;->c(LC/a;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/J;->b(LD/a;LC/a;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(LD/a;LC/a;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->c(LC/a;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    monitor-exit p0

    :cond_0
    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->a(LD/a;)V

    return-void
.end method

.method public declared-synchronized c(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
