.class public Lcom/google/android/maps/driveabout/vector/az;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:LD/b;

.field private final c:Lh/k;

.field private d:Lo/T;

.field private e:F

.field private final f:I

.field private final g:I

.field private final h:I

.field private volatile i:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->g:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->h:I

    new-instance v0, Lh/k;

    invoke-direct {v0}, Lh/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->c:Lh/k;

    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    if-nez v0, :cond_2

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v0, v5}, LD/b;->c(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v2, 0x7f020174

    invoke-virtual {v0, v1, v2}, LD/b;->a(Landroid/content/res/Resources;I)V

    :cond_2
    invoke-virtual {p1}, LD/a;->p()V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v1, 0x303

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v1, p1, LD/a;->d:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->e:F

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->c:Lh/k;

    invoke-virtual {v1, p1}, Lh/k;->a(LD/a;)F

    move-result v1

    const/high16 v2, 0x3f800000

    invoke-interface {v0, v1, v4, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v1, p1, LD/a;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 3

    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->g:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->h:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    invoke-virtual {p1, v2, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    invoke-virtual {p1, v1, v0}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->e:F

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->z:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
