.class public Lcom/google/android/maps/driveabout/vector/a;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static i:LE/o;

.field private static j:LE/d;

.field private static k:LE/o;

.field private static l:LE/d;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lo/T;

.field private c:Lo/r;

.field private d:Lo/ad;

.field private e:I

.field private f:F

.field private g:I

.field private h:I

.field private m:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x64

    new-instance v0, LE/o;

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    new-instance v0, LE/d;

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    new-instance v0, LE/o;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    new-instance v0, LE/d;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    invoke-static {v0, v1}, Lx/r;->a(LE/q;LE/e;)V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    invoke-static {v0, v1}, Lx/r;->b(LE/q;LE/e;)V

    return-void
.end method

.method public constructor <init>(Lo/T;IIILo/r;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/a;->e()V

    iput p3, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    iput p4, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    iput-object p5, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    iput-object p6, p0, Lcom/google/android/maps/driveabout/vector/a;->a:Ljava/lang/String;

    return-void
.end method

.method private static a(ILo/T;)F
    .locals 3

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    int-to-float v0, p0

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method private a(LD/a;F)V
    .locals 3

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, p2, p2, p2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v2}, LE/d;->a(LD/a;I)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    return-void
.end method

.method private e()V
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(ILo/T;)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->d:Lo/ad;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    return-void
.end method

.method public a(LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->d:Lo/ad;

    invoke-virtual {v0, v1}, Lo/aR;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    if-eqz v1, :cond_2

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    invoke-virtual {v1, v2}, Ln/s;->e(Lo/r;)Ln/k;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-virtual {v1, p2, v3}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lo/T;->b(I)V

    :cond_2
    invoke-virtual {p2}, LC/a;->y()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-static {p1, p2, v2, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    div-float v1, v2, v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;F)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Lo/T;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/a;->e()V

    :cond_1
    return-void
.end method

.method public a(Lo/r;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    return-void
.end method

.method public c(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;)V

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
