.class public Lcom/google/android/maps/driveabout/power/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LO/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LO/j;)Z
    .locals 1

    invoke-virtual {p1}, LO/j;->a()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/power/a;->b()Lcom/google/android/maps/driveabout/power/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->c(Lcom/google/android/maps/driveabout/power/a;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILO/g;LO/s;)V
    .locals 0

    return-void
.end method

.method public a(LO/j;I)V
    .locals 1

    if-lez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/power/g;->a(LO/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Guidance event"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(LO/s;)V
    .locals 0

    return-void
.end method

.method public b(LO/j;I)V
    .locals 1

    const/16 v0, 0x1f4

    if-gt p2, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/power/g;->a(LO/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Approaching guidance event"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(LO/s;)V
    .locals 0

    return-void
.end method

.method public c(LO/s;)V
    .locals 2

    invoke-static {}, Lcom/google/android/maps/driveabout/power/a;->b()Lcom/google/android/maps/driveabout/power/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Lcom/google/android/maps/driveabout/power/a;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, LO/s;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "User not on route"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, LO/s;->b()I

    move-result v0

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_0

    const-string v0, "User on a long step"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->c(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d(LO/s;)V
    .locals 0

    return-void
.end method

.method public e(LO/s;)V
    .locals 0

    return-void
.end method

.method public f(LO/s;)V
    .locals 0

    return-void
.end method

.method public g(LO/s;)V
    .locals 0

    return-void
.end method
