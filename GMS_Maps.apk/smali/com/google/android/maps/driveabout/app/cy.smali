.class Lcom/google/android/maps/driveabout/app/cy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/NavigationService;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    check-cast p2, Lcom/google/android/maps/driveabout/publisher/d;

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/publisher/d;->a()Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->d(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a()LO/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/t;->a(LO/q;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/t;->b(LO/q;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;

    return-void
.end method
