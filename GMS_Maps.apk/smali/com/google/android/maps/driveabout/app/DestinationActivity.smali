.class public Lcom/google/android/maps/driveabout/app/DestinationActivity;
.super Landroid/app/ListActivity;
.source "SourceFile"


# static fields
.field private static final a:[Lcom/google/android/maps/driveabout/app/P;


# instance fields
.field private b:[Lcom/google/android/maps/driveabout/app/P;

.field private c:Lcom/google/android/maps/driveabout/app/P;

.field private d:[LO/b;

.field private e:Landroid/os/Handler;

.field private f:Lcom/google/android/maps/driveabout/app/af;

.field private g:Landroid/location/LocationManager;

.field private h:Landroid/location/LocationListener;

.field private i:LaH/h;

.field private j:Lcom/google/android/maps/driveabout/app/an;

.field private k:Lcom/google/android/maps/driveabout/app/T;

.field private l:Lcom/google/android/maps/driveabout/app/bR;

.field private final m:Lcom/google/android/maps/driveabout/app/l;

.field private final n:Ljava/lang/Runnable;

.field private final o:LR/b;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

.field private u:Z

.field private v:LO/f;

.field private w:Lcom/google/android/maps/driveabout/app/Q;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x1

    new-array v0, v5, [Lcom/google/android/maps/driveabout/app/P;

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f020139

    const v3, 0x7f0d00de

    invoke-direct {v1, v2, v3, v6, v4}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f02013c

    const v3, 0x7f0d00df

    invoke-direct {v1, v2, v3, v7, v5}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f020136

    const v3, 0x7f0d00e0

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/P;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    new-instance v0, Lcom/google/android/maps/driveabout/app/l;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    new-instance v0, Lcom/google/android/maps/driveabout/app/r;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/r;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/maps/driveabout/app/A;

    const-string v1, "DestinationActivityIdleHandler"

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/A;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/Y;)I
    .locals 1

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/Y;)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/location/Location;)J
    .locals 4

    if-nez p0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;LaH/h;)LaH/h;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "TravelMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "TravelMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    sget-object v4, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v5, v4

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v1, v4, v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v6

    if-ne v6, v0, :cond_1

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_0
    const-string v0, "PickerTravelMode"

    invoke-static {p0, v0, v3}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v0, v0, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/P;)Lcom/google/android/maps/driveabout/app/P;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/T;)Lcom/google/android/maps/driveabout/app/T;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/bR;)Lcom/google/android/maps/driveabout/app/bR;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    return-object p1
.end method

.method private a(I)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->onSearchRequested()Z

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0d00cd
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private a(LO/U;I)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v3, 0x6

    if-ne p2, v3, :cond_2

    invoke-static {p0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    invoke-virtual {v0}, LO/M;->b()LO/a;

    move-result-object v3

    invoke-virtual {v0}, LO/M;->d()V

    invoke-virtual {v3}, LO/a;->a()LO/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/g;->f()[LO/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    invoke-virtual {v3}, LO/a;->d()[LO/U;

    move-result-object v0

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object p1, v0, v3

    aget-object v0, v0, v1

    move v1, v2

    :cond_2
    const-string v3, "PickerTravelMode"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v4

    invoke-static {p0, v3, v4}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    const-string v3, "D"

    invoke-static {v3, p2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v4, "TravelMode"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "ForceNewDestination"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x2

    if-ne p2, v2, :cond_4

    const-string v2, "Target"

    const-string v4, "Contact"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    :goto_1
    const/high16 v2, 0x10020000

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/bo;

    invoke-direct {v4}, Lcom/google/android/maps/driveabout/app/bo;-><init>()V

    invoke-virtual {v4, p1}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/maps/driveabout/app/bo;->b(LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/bo;->a(I)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/bo;->a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bo;->a(Z)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/bo;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bo;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bo;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x3

    if-ne p2, v2, :cond_3

    const-string v2, "Target"

    const-string v4, "StarredItem"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;LO/U;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(LO/U;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;[LO/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a([LO/b;)V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/P;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/P;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/j;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    invoke-virtual {v0}, LaH/h;->r()LaN/B;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1, v1}, Lcom/google/android/maps/driveabout/app/P;->a(Lcom/google/googlenav/j;LaN/B;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    sget-object v4, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v4, v4, v0

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/app/P;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibleTravelModes([Lcom/google/android/maps/driveabout/app/P;)V

    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Contact"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0d00cf

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "StarredItem"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d00d0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_0

    :cond_2
    const-string v0, "Speak"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0d00ce

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    const-string v0, "Search"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d00cd

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, LO/U;

    invoke-direct {v0, p1, v1, v1, v1}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(LO/U;I)V

    return-void
.end method

.method private a([LO/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    return p1
.end method

.method private static b(Lcom/google/android/maps/driveabout/app/Y;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/Y;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x6

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Target"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Target"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    if-eqz v1, :cond_0

    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    invoke-virtual {v1, v2}, LO/c;->b([LO/b;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    if-eq v1, v0, :cond_1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/P;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/P;)V

    return-void
.end method

.method private b(Lcom/google/android/maps/driveabout/app/P;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v0

    invoke-static {v0}, LO/c;->b(I)[LO/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a([LO/b;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/bR;->a_(I)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/t;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/t;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/u;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/u;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    return p1
.end method

.method private b(Lcom/google/googlenav/j;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    sget-object v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/P;->a(Lcom/google/android/maps/driveabout/app/P;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/j;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r()V

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d006e

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/L;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/L;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l()LaH/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e()V

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ad;->a(Landroid/content/Context;)V

    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    sget-object v0, Lcom/google/googlenav/z;->b:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DestinationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to start activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    return-object v0
.end method

.method private d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-nez v0, :cond_0

    const-string v0, "Show Disclaimer"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    goto :goto_0
.end method

.method private e()V
    .locals 7

    const-wide/16 v2, 0x2710

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v0, "gps"

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_0
    const-string v0, "network"

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/T;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    return-object v0
.end method

.method private f()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "http://maps.google.com/?myl=saddr&dirflg=d&daddr="

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    return-object v0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_1
    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v1, "PickerTravelMode"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v2

    invoke-static {p0, v1, v2}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private h()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/DestinationActivity;)LaH/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    return-object v0
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v2, v0, v1}, Lcom/google/android/maps/driveabout/app/eR;->a(Landroid/app/Activity;ILjava/lang/String;Z)V

    return-void
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/bR;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    return-object v0
.end method

.method private k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;LaH/h;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private l()LaH/h;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v0, "gps"

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :goto_0
    const-string v3, "network"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    :goto_1
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, v0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v1

    :cond_0
    :goto_2
    return-object v1

    :cond_1
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    :goto_3
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v2

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, v0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_3

    :cond_3
    move-object v2, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v2, v1

    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic l(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method private m()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n()V

    return-void
.end method

.method private n()V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/ci;->h()Lcom/google/googlenav/j;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/googlenav/j;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    array-length v3, v3

    if-eqz v3, :cond_2

    const-string v2, "e"

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/an;->b()V

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    array-length v2, v2

    if-eq v2, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/Q;->a(Z)V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/googlenav/j;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v2}, Lcom/google/googlenav/j;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p()V

    goto :goto_1
.end method

.method private o()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d0003

    const v2, 0x7f0d0004

    const v3, 0x7f0d0048

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/maps/driveabout/app/H;

    invoke-direct {v5, p0}, Lcom/google/android/maps/driveabout/app/H;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private p()V
    .locals 7

    new-instance v5, Lcom/google/android/maps/driveabout/app/I;

    invoke-direct {v5, p0}, Lcom/google/android/maps/driveabout/app/I;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    new-instance v6, Lcom/google/android/maps/driveabout/app/s;

    invoke-direct {v6, p0}, Lcom/google/android/maps/driveabout/app/s;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d005c

    const v2, 0x7f0d00c8

    const v3, 0x7f0d0086

    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private q()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/v;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/v;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/w;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/w;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method private r()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/x;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/x;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/y;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/y;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    new-instance v2, Lcom/google/android/maps/driveabout/app/z;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/z;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    return-void
.end method

.method public handleContactsClick(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    return-void
.end method

.method public handleDriveHomeClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Lcom/google/android/maps/driveabout/app/G;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/G;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    const-string v1, "HomeAddress"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LR/s;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/aG;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x7

    invoke-direct {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public handleFreeDriveClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    return-void
.end method

.method public handleSpeakDestinationClick(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    return-void
.end method

.method public handleStarredItemsClick(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    return-void
.end method

.method public handleTypeDestinationClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->onSearchRequested()Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/E;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/E;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-static {p0, p2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/eR;->a(Landroid/content/Context;ILandroid/content/Intent;Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/eU;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a()V

    :cond_0
    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const v3, 0x7f04002c

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/l;->a(Landroid/app/Activity;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setDefaultKeyMode(I)V

    invoke-static {v6}, Lcom/google/android/maps/driveabout/app/dr;->a(Z)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f040031

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/O;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/O;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    const-string v3, "DriveAbout"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/maps/driveabout/app/af;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    new-instance v0, Lcom/google/android/maps/driveabout/app/an;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ForceNewDestination"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/maps/driveabout/app/K;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/K;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/maps/driveabout/app/J;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/J;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/maps/driveabout/app/N;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/N;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    :goto_2
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/maps/driveabout/app/Q;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/Q;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/B;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/B;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->a()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/C;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/C;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    invoke-static {v0}, LO/c;->a(LO/f;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/M;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/M;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Speak"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v0, 0x7f0d00ce

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1

    :cond_5
    const-string v1, "Show Disclaimer"

    invoke-static {p0, v1, v6}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v1, v2}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/af;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    invoke-static {v0}, LO/c;->b(LO/f;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->a()V

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onNewIntent(Landroid/content/Intent;)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-eq v0, v1, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a()V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/aG;)V

    goto :goto_0

    :sswitch_4
    const-string v1, "A"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/maps/driveabout/app/SettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_5
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/googlenav/K;->ae()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f1000f6 -> :sswitch_4
        0x7f1000f7 -> :sswitch_5
        0x7f10049d -> :sswitch_0
        0x7f10049e -> :sswitch_2
        0x7f10049f -> :sswitch_3
        0x7f1004a1 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->c()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->m()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    :cond_2
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const v1, 0x7f10049d

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/Q;->a(Landroid/view/Menu;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/maps/driveabout/app/W;

    if-eqz v0, :cond_2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const v0, 0x7f1004a0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    new-instance v1, Lcom/google/android/maps/driveabout/app/F;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/F;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setTravelModeChangedListener(Lcom/google/android/maps/driveabout/widgets/f;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibleTravelModes([Lcom/google/android/maps/driveabout/app/P;)V

    :cond_0
    const v0, 0x7f10049e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    return v2

    :cond_2
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->b()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e()V

    :cond_1
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    goto :goto_0
.end method
