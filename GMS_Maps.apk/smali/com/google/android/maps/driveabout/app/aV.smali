.class Lcom/google/android/maps/driveabout/app/aV;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/b;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/aQ;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aZ;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/aZ;-><init>(Lcom/google/android/maps/driveabout/app/aV;LM/C;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    check-cast p1, LaH/h;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aW;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/aW;-><init>(Lcom/google/android/maps/driveabout/app/aV;LaH/h;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aX;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aX;-><init>(Lcom/google/android/maps/driveabout/app/aV;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2

    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aY;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aY;-><init>(Lcom/google/android/maps/driveabout/app/aV;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
