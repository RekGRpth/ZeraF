.class public Lcom/google/android/maps/driveabout/app/cq;
.super Lcom/google/android/maps/driveabout/vector/bk;
.source "SourceFile"


# instance fields
.field private b:I

.field private c:I

.field private d:F

.field private e:I

.field private f:Z

.field private g:I

.field private h:Lcom/google/android/maps/driveabout/app/k;

.field private i:Ljava/util/HashMap;

.field private j:Lcom/google/android/maps/driveabout/app/bT;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;-><init>(Landroid/content/res/Resources;)V

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/cq;->k()V

    iput v3, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v3, v2, v2}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/k;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    const/high16 v0, 0x3fc00000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/cq;->d(F)V

    return-void
.end method

.method private a(Lo/ad;)Lo/ad;
    .locals 10

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b002c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    sub-int v4, v0, v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v0, v2, 0x3

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    mul-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    sub-int/2addr v3, v2

    sub-int/2addr v3, v0

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    mul-int/lit8 v7, v5, 0x2

    sub-int/2addr v6, v7

    sub-int/2addr v6, v4

    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v7

    div-int v3, v7, v3

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v7

    div-int v6, v7, v6

    if-le v3, v6, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0030

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    move v2, v1

    :goto_0
    add-int/2addr v2, v5

    add-int v3, v1, v5

    add-int/2addr v4, v5

    add-int/2addr v5, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;IIII)Lo/ad;

    move-result-object v0

    return-object v0

    :cond_0
    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_0
.end method

.method private a(Lo/ad;FFFF)Lo/ad;
    .locals 6

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v1, v1

    sub-float/2addr v1, p2

    sub-float/2addr v1, p3

    div-float/2addr v0, v1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v2, v2

    sub-float/2addr v2, p2

    sub-float/2addr v2, p3

    div-float/2addr v1, v2

    mul-float/2addr v1, p3

    float-to-int v1, v1

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v3, v3

    sub-float/2addr v3, p4

    sub-float/2addr v3, p5

    div-float/2addr v2, v3

    mul-float/2addr v2, p4

    float-to-int v2, v2

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v4, v4

    sub-float/2addr v4, p4

    sub-float/2addr v4, p5

    div-float/2addr v3, v4

    mul-float/2addr v3, p5

    float-to-int v3, v3

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    new-instance v5, Lo/T;

    invoke-direct {v5, v0, v3}, Lo/T;-><init>(II)V

    invoke-virtual {v4, v5}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v3

    new-instance v4, Lo/T;

    invoke-direct {v4, v1, v2}, Lo/T;-><init>(II)V

    invoke-virtual {v3, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    new-instance v2, Lo/ad;

    invoke-direct {v2, v0, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v2
.end method

.method private a(Lo/ad;IIII)Lo/ad;
    .locals 6

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    int-to-float v5, p5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;FFFF)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/ad;Landroid/location/Location;)Lo/ad;
    .locals 5

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Lo/T;->a(D)D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v0, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    invoke-virtual {p1, v0}, Lo/ad;->a(Lo/ad;)Lo/ad;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a(Lo/ad;Lo/u;)Lo/ad;
    .locals 7

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lo/u;->a()I

    move-result v0

    invoke-virtual {p2}, Lo/u;->b()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Lo/T;IIII)Lo/ad;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Lo/u;)Lo/ad;

    move-result-object p2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Landroid/location/Location;)Lo/ad;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;)Lo/ad;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    const/16 v5, 0x320

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;IIFI)V

    return-void
.end method

.method private k()V
    .locals 15

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/high16 v12, 0x42820000

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c0000

    const/high16 v4, 0x41840000

    const/high16 v5, 0x41740000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x41860000

    const/high16 v5, 0x41780000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c0000

    const/high16 v4, 0x41840000

    const/high16 v5, 0x41740000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x418c0000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41880000

    const/high16 v4, 0x41920000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x418c0000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c0000

    const/high16 v4, 0x41840000

    const/high16 v5, 0x41740000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x41860000

    const/high16 v5, 0x41780000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c0000

    const/high16 v4, 0x41840000

    const/high16 v5, 0x41740000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x418c0000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41880000

    const/high16 v4, 0x41920000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x41860000

    const/high16 v4, 0x418c0000

    const/high16 v5, 0x41820000

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v5, Lq/e;

    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-direct {v5, v0}, Lq/e;-><init>(Lr/z;)V

    :cond_0
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41860000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x418e0000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41980000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41880000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x41920000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41980000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cU;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41920000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x41920000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41a00000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cU;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41940000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x41940000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41a00000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41860000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x418e0000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41980000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x41880000

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x41920000

    const/high16 v4, 0x425c0000

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x41780000

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41980000

    const/high16 v9, 0x42960000

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(F)F
    .locals 1

    const/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(FI)F

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/cq;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nMapWidth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nMapHeight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->d:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenDensity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->e:I

    if-eq v1, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nBottomMargin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->g:I

    if-eq v1, v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nCameraType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nCameraPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lo/aQ;
    .locals 5

    new-instance v0, LC/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method a(Lo/ad;Lo/T;IIII)Lo/ad;
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {p2, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    int-to-float v2, p5

    cmpg-float v2, v3, v2

    if-gez v2, :cond_3

    int-to-float v2, p5

    sub-float/2addr v2, v3

    :goto_0
    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    sub-int v5, p3, p5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    int-to-float v4, p3

    int-to-float v5, p5

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v5, v5

    sub-float v3, v5, v3

    sub-float v3, v4, v3

    :goto_1
    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v4, v4

    sub-float/2addr v4, v1

    int-to-float v5, p6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    int-to-float v4, p6

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v5, v5

    sub-float/2addr v5, v1

    sub-float/2addr v4, v5

    :goto_2
    sub-int v5, p4, p6

    int-to-float v5, v5

    cmpg-float v5, v1, v5

    if-gez v5, :cond_0

    int-to-float v0, p4

    int-to-float v5, p6

    sub-float/2addr v0, v5

    sub-float v5, v0, v1

    :goto_3
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;FFFF)Lo/ad;

    move-result-object v0

    return-object v0

    :cond_0
    move v5, v0

    goto :goto_3

    :cond_1
    move v4, v0

    goto :goto_2

    :cond_2
    move v3, v0

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/k;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    return-void
.end method

.method public a(IIF)V
    .locals 1

    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iput p2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iput p3, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    return-void
.end method

.method public a(ILcom/google/android/maps/driveabout/vector/q;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/maps/driveabout/app/cr;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {v4, p1, v0, p3}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/k;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    if-eq v0, v3, :cond_0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v3, 0x7f0c0003

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v4, 0x7f0c0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :goto_2
    const/4 v3, 0x2

    if-ne p3, v3, :cond_3

    :goto_3
    int-to-float v0, v0

    const/high16 v2, 0x42c80000

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/k;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(I)V

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Failed to load chevron walking margin resource."

    invoke-static {v3, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "NavigationMapController"

    const-string v3, "Failed to load da_mylocation_chevron_margin_walking_percent resource"

    invoke-static {v0, v3}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v4, "Failed to load chevron driving margin resource."

    invoke-static {v4, v3}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v3, "NavigationMapController"

    const-string v4, "Failed to load da_mylocation_chevron_margin_percent resource"

    invoke-static {v3, v4}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public a(LC/c;)V
    .locals 1

    const/16 v0, 0x320

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    return-void
.end method

.method public a(LO/N;ZZ)V
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v1, v0, p1, p3}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LO/N;Z)LC/b;

    move-result-object v1

    if-eqz p2, :cond_2

    const/16 v0, 0x320

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(LaH/h;)V
    .locals 6

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;IIF)LC/b;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_0
.end method

.method public a(LaH/h;Lo/am;)V
    .locals 7

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;Lo/am;IIF)LC/b;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 1

    const/16 v0, 0x320

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;I)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;FZ)V
    .locals 11

    const/16 v10, 0x1f4

    const/high16 v9, 0x41700000

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bT;->c()Lo/S;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v4

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v7, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v8, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move v5, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;

    move-result-object v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_2

    const/high16 v1, -0x40800000

    cmpl-float v1, p2, v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v2}, LaH/h;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LC/b;->a()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_5

    new-instance v3, LC/b;

    invoke-virtual {v0}, LC/b;->c()Lo/T;

    move-result-object v4

    invoke-virtual {v0}, LC/b;->d()F

    move-result v6

    invoke-virtual {v0}, LC/b;->e()F

    move-result v7

    invoke-virtual {v0}, LC/b;->f()F

    move-result v8

    move v5, v9

    invoke-direct/range {v3 .. v8}, LC/b;-><init>(Lo/T;FFFF)V

    move-object v4, v3

    :goto_1
    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v0

    const v1, 0x3f8ccccd

    mul-float/2addr v0, v1

    invoke-virtual {v2}, LaH/h;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Lo/T;->a(D)D

    move-result-wide v5

    double-to-float v1, v5

    mul-float v5, v0, v1

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v7, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v8, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;FIIF)LC/b;

    move-result-object v0

    move-object v1, v0

    :goto_2
    const/4 v3, 0x0

    invoke-virtual {v2}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v2

    double-to-float v0, v2

    :goto_3
    const/high16 v2, 0x3fc00000

    mul-float/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;FIIF)LC/b;

    move-result-object v0

    :cond_2
    if-eqz p3, :cond_3

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    new-instance v1, Lcom/google/android/maps/driveabout/app/cs;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-direct {v1, v0, v2, p1}, Lcom/google/android/maps/driveabout/app/cs;-><init>(LC/b;Lcom/google/android/maps/driveabout/app/bT;Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {p0, v1, v10}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Lcom/google/android/maps/driveabout/app/ct;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/ct;-><init>(LC/b;Lcom/google/android/maps/driveabout/app/bT;)V

    invoke-virtual {p0, v1, v10}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    move-object v4, v0

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto :goto_2
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {v2}, LaH/h;->getBearing()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;F)LC/b;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;F)LC/b;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bT;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    const/16 v1, 0x320

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/T;I)V

    return-void
.end method

.method public a_(LC/a;)I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bT;->a()I

    move-result v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a_(LC/a;)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public b()Lo/T;
    .locals 5

    new-instance v0, LC/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->h()Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 4

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v1

    const/4 v0, 0x1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-virtual {v3}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->a()Lo/ad;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/ad;->a(Lo/ad;)Lo/ad;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V

    goto :goto_0
.end method

.method public c()LC/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/high16 v0, -0x40800000

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a(F)Lo/am;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lo/am;->f()Lo/ad;

    move-result-object v0

    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v0

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/cq;

    if-eqz v2, :cond_2

    check-cast p1, Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/cq;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    add-int/2addr v0, v1

    return v0
.end method
