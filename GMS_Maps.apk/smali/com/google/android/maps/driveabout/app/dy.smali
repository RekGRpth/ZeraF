.class public Lcom/google/android/maps/driveabout/app/dy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:F


# instance fields
.field private final b:F

.field private c:J

.field private d:Landroid/location/Location;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x3ed18d26

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/app/dy;->a:F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const v0, -0x42a988b0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dy;-><init>(F)V

    return-void
.end method

.method public constructor <init>(F)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dy;->e:I

    invoke-static {p1}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dy;->b:F

    return-void
.end method


# virtual methods
.method a(FFJ)I
    .locals 11

    const-wide v6, 0xdc6d62da00L

    const/4 v0, 0x0

    const/high16 v10, 0x40000000

    const/4 v1, 0x1

    const v9, 0x40c90fdb

    float-to-double v2, p1

    const-wide v4, -0x3ff6de04abbbd2e8L

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    float-to-double v2, p1

    const-wide v4, 0x400921fb54442d18L

    cmpl-double v2, v2, v4

    if-gez v2, :cond_0

    const v2, -0x3f36f025

    cmpg-float v2, p2, v2

    if-ltz v2, :cond_0

    cmpl-float v2, p2, v9

    if-gtz v2, :cond_0

    cmp-long v2, p3, v6

    if-gez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    sub-long v2, p3, v6

    long-to-float v2, v2

    const v3, 0x4ca4cb80

    div-float/2addr v2, v3

    const v3, 0x40c7ae92

    mul-float v4, v2, v9

    const v5, 0x43b6a0d1

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    const v4, 0x3d08e2fe

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v3

    const v5, 0x39b702d8

    mul-float v6, v10, v3

    invoke-static {v6}, Landroid/util/FloatMath;->sin(F)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const v5, 0x36afb0e6

    const/high16 v6, 0x40400000

    mul-float/2addr v6, v3

    invoke-static {v6}, Landroid/util/FloatMath;->sin(F)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const v5, 0x3fe5f6fd

    add-float/2addr v4, v5

    const v5, 0x40490fdb

    add-float/2addr v4, v5

    invoke-static {v4}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    sget v6, Lcom/google/android/maps/driveabout/app/dy;->a:F

    mul-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->asin(D)D

    move-result-wide v5

    double-to-float v5, v5

    iget v6, p0, Lcom/google/android/maps/driveabout/app/dy;->b:F

    invoke-static {p1}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    invoke-static {v5}, Landroid/util/FloatMath;->sin(F)F

    move-result v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    invoke-static {p1}, Landroid/util/FloatMath;->cos(F)F

    move-result v7

    invoke-static {v5}, Landroid/util/FloatMath;->cos(F)F

    move-result v5

    mul-float/2addr v5, v7

    div-float v5, v6, v5

    const/high16 v6, 0x3f800000

    cmpl-float v6, v5, v6

    if-gez v6, :cond_1

    const/high16 v6, -0x40800000

    cmpg-float v6, v5, v6

    if-gtz v6, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->acos(D)D

    move-result-wide v5

    double-to-float v5, v5

    neg-float v6, p2

    div-float/2addr v6, v9

    const v7, 0x3a6bedfa

    add-float/2addr v6, v7

    const v7, 0x3badab9f

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    mul-float/2addr v3, v7

    add-float/2addr v3, v6

    const v6, -0x441de69b

    mul-float/2addr v4, v10

    invoke-static {v4}, Landroid/util/FloatMath;->sin(F)F

    move-result v4

    mul-float/2addr v4, v6

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    neg-float v3, v5

    div-float/2addr v3, v9

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_1

    div-float v3, v5, v9

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Landroid/location/Location;)I
    .locals 10

    const/high16 v9, 0x43b40000

    const v8, 0x40c90fdb

    const-wide v6, 0x3fb999999999999aL

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/dy;->c:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0x1d4c0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dy;->d:Landroid/location/Location;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/dy;->d:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v2, v2, v6

    if-gez v2, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/dy;->d:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v2, v2, v6

    if-gez v2, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dy;->e:I

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dy;->d:Landroid/location/Location;

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/dy;->c:J

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, v8

    div-float/2addr v2, v9

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    double-to-float v3, v3

    mul-float/2addr v3, v8

    div-float/2addr v3, v9

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/google/android/maps/driveabout/app/dy;->a(FFJ)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dy;->e:I

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dy;->e:I

    goto :goto_0
.end method
