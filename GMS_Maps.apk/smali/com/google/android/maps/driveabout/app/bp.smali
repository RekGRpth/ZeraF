.class public Lcom/google/android/maps/driveabout/app/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cQ;Lo/o;)V
    .locals 4

    if-eqz p1, :cond_0

    const v0, 0x7f0d0072

    invoke-interface {p1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(I)V

    :cond_0
    instance-of v0, p2, Lo/s;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/layer/p;

    const-string v1, "TrafficIncident"

    invoke-virtual {p2}, Lo/o;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/maps/driveabout/app/bq;

    invoke-direct {v3, p1, p0}, Lcom/google/android/maps/driveabout/app/bq;-><init>(Lcom/google/android/maps/driveabout/app/cQ;Landroid/content/Context;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/layer/q;)V

    new-instance v1, Ll/g;

    const-string v2, "addRequest"

    invoke-direct {v1, v2, v0}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v1}, Ll/f;->b(Ll/j;)V

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v1

    invoke-interface {v1, v0}, Law/p;->c(Law/g;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lo/o;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/K;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not launch internal item details"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
