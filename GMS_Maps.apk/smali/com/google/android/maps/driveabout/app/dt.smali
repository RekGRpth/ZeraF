.class public Lcom/google/android/maps/driveabout/app/dt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LO/N;

.field private b:J

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 1

    const v0, 0x30d40

    div-int v0, p0, v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private a(LO/z;LO/N;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dt;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/dt;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LO/z;->d()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "|M="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, LO/z;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string v3, "|T="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, LO/N;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "|L="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lo/T;->a()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/dt;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lo/T;->c()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dt;->a(I)I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "|m="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, LO/N;->e()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "|a="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "|p="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LO/N;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "|t="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dt;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, LO/z;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "|R=1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v0, 0x7c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "c"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dt;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static a(LO/N;LO/N;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    if-nez p1, :cond_4

    if-eqz p0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, LO/N;->a()Lo/T;

    move-result-object v2

    invoke-virtual {p1}, LO/N;->a()Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, LO/N;->f()I

    move-result v2

    invoke-virtual {p1}, LO/N;->f()I

    move-result v3

    if-ne v2, v3, :cond_5

    invoke-virtual {p0}, LO/N;->e()I

    move-result v2

    invoke-virtual {p1}, LO/N;->e()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private static b(LO/z;LO/N;)I
    .locals 4

    invoke-virtual {p1}, LO/N;->k()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->z()I

    move-result v0

    invoke-virtual {p0, v0}, LO/z;->b(I)D

    move-result-wide v0

    invoke-virtual {p1}, LO/N;->z()I

    move-result v2

    invoke-virtual {p0, v2}, LO/z;->b(I)D

    move-result-wide v2

    invoke-virtual {p0, v0, v1}, LO/z;->b(D)I

    move-result v0

    invoke-virtual {p0, v2, v3}, LO/z;->b(D)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method a()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LO/z;LO/N;LO/N;LaH/h;)V
    .locals 10

    const/4 v1, 0x1

    const-wide v8, 0x408f400000000000L

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dt;->a:LO/N;

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dt;->a(LO/N;LO/N;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, LO/N;->k()LO/N;

    move-result-object v0

    if-ne v0, p2, :cond_0

    invoke-virtual {p3}, LO/N;->z()I

    move-result v0

    invoke-virtual {p1}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v3

    if-lt v0, v3, :cond_2

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/dt;->d:Z

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/dt;->a:LO/N;

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v0

    invoke-virtual {p4, v0}, LaH/h;->a(Lo/T;)F

    move-result v0

    const/high16 v3, 0x42c80000

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_4

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/app/dt;->d:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dt;->a(LO/z;LO/N;)V

    :cond_3
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/dt;->a:LO/N;

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/dt;->d:Z

    if-eqz v0, :cond_1

    invoke-virtual {p3}, LO/N;->e()I

    move-result v0

    const/16 v2, 0x12c

    if-lt v0, v2, :cond_1

    invoke-virtual {p2}, LO/N;->z()I

    move-result v0

    invoke-virtual {p1, v0}, LO/z;->c(I)D

    move-result-wide v2

    invoke-virtual {p1}, LO/z;->p()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {p3}, LO/N;->z()I

    move-result v0

    invoke-virtual {p1, v0}, LO/z;->c(I)D

    move-result-wide v6

    sub-double/2addr v4, v6

    cmpl-double v0, v2, v8

    if-lez v0, :cond_1

    cmpl-double v0, v4, v8

    if-lez v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/dt;->d:Z

    invoke-static {p1, p3}, Lcom/google/android/maps/driveabout/app/dt;->b(LO/z;LO/N;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dt;->c:I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dt;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/dt;->b:J

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
