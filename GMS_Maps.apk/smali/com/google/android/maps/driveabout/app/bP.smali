.class public Lcom/google/android/maps/driveabout/app/bP;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/ar;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Lo/T;

.field private final f:I

.field private final g:I

.field private final h:D

.field private final i:D

.field private j:Lo/aq;

.field private k:Lo/aq;

.field private l:I


# direct methods
.method public constructor <init>(Lo/T;Lo/T;II)V
    .locals 8

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bP;->e:Lo/T;

    iput p3, p0, Lcom/google/android/maps/driveabout/app/bP;->a:I

    if-ge p4, v0, :cond_0

    move p4, v0

    :cond_0
    invoke-virtual {p1}, Lo/T;->f()I

    move-result v1

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v2

    invoke-static {p3, v1, v2, v4}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v1

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v2

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v3

    invoke-static {p3, v2, v3, v4}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v2

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v3

    invoke-virtual {p1}, Lo/T;->f()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v4

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v6, v5, :cond_1

    iput v7, p0, Lcom/google/android/maps/driveabout/app/bP;->f:I

    iput p4, p0, Lcom/google/android/maps/driveabout/app/bP;->g:I

    iput v7, p0, Lcom/google/android/maps/driveabout/app/bP;->c:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bP;->d:I

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v0

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    :goto_0
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bP;->i:D

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bP;->h:D

    :goto_1
    iput v7, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    return-void

    :cond_1
    iput p4, p0, Lcom/google/android/maps/driveabout/app/bP;->f:I

    iput v7, p0, Lcom/google/android/maps/driveabout/app/bP;->g:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bP;->c:I

    iput v7, p0, Lcom/google/android/maps/driveabout/app/bP;->d:I

    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v0

    invoke-virtual {v1}, Lo/aq;->d()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    goto :goto_0

    :cond_2
    int-to-double v0, v3

    iget v2, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bP;->h:D

    int-to-double v0, v4

    iget v2, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bP;->i:D

    goto :goto_1
.end method

.method public static a(Lo/T;II)Lcom/google/android/maps/driveabout/app/bP;
    .locals 4

    new-instance v0, Lo/T;

    invoke-virtual {p0}, Lo/T;->f()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-virtual {p0}, Lo/T;->g()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    new-instance v1, Lo/T;

    invoke-virtual {p0}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {p0}, Lo/T;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lo/T;-><init>(II)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/bP;

    invoke-direct {v2, v0, v1, p1, p2}, Lcom/google/android/maps/driveabout/app/bP;-><init>(Lo/T;Lo/T;II)V

    return-object v2
.end method


# virtual methods
.method public c()Lo/aq;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bP;->k:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bP;->k:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v2

    if-ge v1, v2, :cond_2

    :cond_0
    new-instance v0, Lo/aq;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bP;->a:I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/app/bP;->c:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->d:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lo/aq;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    :cond_1
    return-object v0

    :cond_2
    iget v1, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    iget v2, p0, Lcom/google/android/maps/driveabout/app/bP;->b:I

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bP;->e:Lo/T;

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    int-to-double v1, v1

    iget v3, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    int-to-double v3, v3

    iget-wide v5, p0, Lcom/google/android/maps/driveabout/app/bP;->h:D

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bP;->e:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    int-to-double v2, v2

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/google/android/maps/driveabout/app/bP;->i:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/app/bP;->a:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->f:I

    sub-int v4, v1, v4

    iget v5, p0, Lcom/google/android/maps/driveabout/app/bP;->g:I

    add-int/2addr v5, v2

    invoke-static {v3, v4, v5, v0}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/maps/driveabout/app/bP;->j:Lo/aq;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/bP;->a:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->f:I

    add-int/2addr v1, v4

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->c:I

    sub-int/2addr v1, v4

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->g:I

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bP;->d:I

    add-int/2addr v2, v4

    invoke-static {v3, v1, v2, v0}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bP;->k:Lo/aq;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bP;->l:I

    goto :goto_0
.end method
