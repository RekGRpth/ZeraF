.class Lcom/google/android/maps/driveabout/app/dW;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/maps/driveabout/app/dO;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/dO;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dW;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dW;->a:Z

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dW;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/dW;->a:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v1, v0

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/dW;->a:Z

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dW;->b:Lcom/google/android/maps/driveabout/app/dO;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Lcom/google/android/maps/driveabout/app/dO;)LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    goto :goto_0

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dW;->a:Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dW;->a(Landroid/view/View;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    if-ltz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    if-ge v0, v3, :cond_3

    if-ltz v2, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lt v2, v0, :cond_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dW;->a(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
