.class public Lcom/google/android/maps/driveabout/app/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private b:Landroid/content/Context;

.field private final c:Landroid/content/BroadcastReceiver;

.field private d:Z

.field private final e:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.app.action.ENTER_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/l;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/m;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/m;-><init>(Lcom/google/android/maps/driveabout/app/l;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->c:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    new-instance v0, Lcom/google/android/maps/driveabout/app/n;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/n;-><init>(Lcom/google/android/maps/driveabout/app/l;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->e:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/l;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/l;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->c:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/l;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, LJ/a;->g()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    :try_start_0
    const-string v0, "Show Disclaimer"

    const/4 v2, 0x1

    invoke-static {p1, v0, v2}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/l;->e:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/maps/driveabout/app/l;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/l;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.googlenav.suggest.android.SuggestContentProvider.SUGGEST_PROVIDER_CREATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-static {p1, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "UnitsSetting"

    invoke-virtual {v0, v1}, LR/u;->a(Ljava/lang/String;)LR/u;

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
.end method

.method public b()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/l;->e:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/maps/driveabout/app/l;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/l;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/l;->d:Z

    :cond_0
    return-void
.end method
