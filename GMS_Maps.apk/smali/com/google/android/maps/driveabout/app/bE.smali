.class public Lcom/google/android/maps/driveabout/app/bE;
.super Lcom/google/android/maps/driveabout/app/bI;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:I

.field private d:Landroid/graphics/Bitmap;

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:B

.field private i:Lo/J;

.field private j:F

.field private k:F


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZIZI)V
    .locals 2

    const/4 v1, 0x0

    const/high16 v0, -0x40800000

    invoke-direct {p0, p2, p3, v1}, Lcom/google/android/maps/driveabout/app/bI;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/bD;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bE;->j:F

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bE;->k:F

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bE;->a()Lcom/google/android/maps/driveabout/app/bE;

    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/app/bE;->a:Z

    iput-boolean p5, p0, Lcom/google/android/maps/driveabout/app/bE;->b:Z

    iput p6, p0, Lcom/google/android/maps/driveabout/app/bE;->c:I

    iput-boolean p7, p0, Lcom/google/android/maps/driveabout/app/bE;->f:Z

    if-lez p8, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p8}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->d:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bE;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/bE;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    new-instance v0, Lo/J;

    invoke-direct {v0, p1}, Lo/J;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    invoke-virtual {v0}, Lo/J;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/bE;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    invoke-virtual {v0}, Lo/J;->b()[Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bE;->f:Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bE;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bE;->a:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/bE;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/bE;->e:Z

    return p1
.end method


# virtual methods
.method a()Lcom/google/android/maps/driveabout/app/bE;
    .locals 1

    const/4 v0, -0x1

    iput-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    return-object p0
.end method

.method a([I)Lcom/google/android/maps/driveabout/app/bE;
    .locals 3

    const/4 v0, 0x0

    array-length v1, p1

    if-nez v1, :cond_1

    const/4 v0, -0x1

    iput-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    :cond_0
    return-object p0

    :cond_1
    iput-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/bE;->b(I)Lcom/google/android/maps/driveabout/app/bE;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(FF)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/bE;->j:F

    iput p2, p0, Lcom/google/android/maps/driveabout/app/bE;->k:F

    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bE;->d:Landroid/graphics/Bitmap;

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bE;->g:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/bE;->e:Z

    return-void
.end method

.method public a(F)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bE;->j:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bE;->k:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bE;->j:F

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bE;->k:F

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    invoke-virtual {v0, p1}, Lo/J;->a(F)Z

    move-result v0

    goto :goto_0
.end method

.method a(I)Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Lcom/google/android/maps/driveabout/app/bE;
    .locals 1

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    return-object p0
.end method

.method b(I)Lcom/google/android/maps/driveabout/app/bE;
    .locals 2

    iget-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    or-int/2addr v0, v1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/google/android/maps/driveabout/app/bE;->h:B

    return-object p0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/bE;->c:I

    return v0
.end method

.method public c(I)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->d:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->d:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    if-eqz v1, :cond_0

    if-gez p1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    invoke-virtual {v1}, Lo/J;->a()I

    move-result p1

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    invoke-virtual {v1}, Lo/J;->b()[Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-ge p1, v2, :cond_0

    aget-object v0, v1, p1

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bE;->f:Z

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bE;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bE;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bE;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bE;->e:Z

    return v0
.end method

.method public i()Lo/J;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bE;->i:Lo/J;

    return-object v0
.end method
