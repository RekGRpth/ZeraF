.class public Lcom/google/android/maps/driveabout/app/P;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/maps/driveabout/app/P;->a:I

    iput p2, p0, Lcom/google/android/maps/driveabout/app/P;->b:I

    iput p3, p0, Lcom/google/android/maps/driveabout/app/P;->c:I

    iput p4, p0, Lcom/google/android/maps/driveabout/app/P;->d:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/P;)I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/P;->d:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/P;->c:I

    return v0
.end method

.method public a(Lcom/google/googlenav/j;LaN/B;)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/P;->c:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    invoke-static {}, LO/T;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/maps/driveabout/app/P;->d:I

    invoke-virtual {p1, v1, p2, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/P;->a:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/P;->b:I

    return v0
.end method
