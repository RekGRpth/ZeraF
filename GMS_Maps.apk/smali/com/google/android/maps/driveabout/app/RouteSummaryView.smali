.class public Lcom/google/android/maps/driveabout/app/RouteSummaryView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/maps/driveabout/app/dz;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(LO/z;)Ljava/util/ArrayList;
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, LO/z;->d()I

    move-result v0

    if-ne v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d00e2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-virtual {p1}, LO/z;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d00ed

    const/4 v3, 0x1

    invoke-static {v0, v2, v4, v3}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;IIZ)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, LO/z;->s()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/S;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, LO/S;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, LO/z;->d()I

    move-result v0

    if-ne v0, v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d00e3

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, LO/z;->D()[LO/b;

    move-result-object v0

    const/4 v2, 0x5

    invoke-static {v0, v2}, LO/c;->a([LO/b;I)Z

    move-result v0

    invoke-virtual {p1}, LO/z;->E()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v0, :cond_4

    const-string v0, "!"

    invoke-static {v0, v6}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    :cond_4
    :goto_2
    invoke-virtual {p1}, LO/z;->D()[LO/b;

    move-result-object v0

    const/4 v2, 0x4

    invoke-static {v0, v2}, LO/c;->a([LO/b;I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d00ea

    const v3, 0x7f0200da

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;IIZ)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    return-object v1

    :cond_6
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d00e9

    const v3, 0x7f0200db

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;IIZ)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v0, "!"

    invoke-static {v0, v5}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->a:Lcom/google/android/maps/driveabout/app/dz;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04004f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f10013c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->b:Landroid/widget/ImageView;

    const v0, 0x7f10013d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->c:Landroid/widget/TextView;

    const v0, 0x7f10013e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->d:Landroid/widget/TextView;

    const v0, 0x7f10013f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->e:Landroid/widget/TextView;

    const v0, 0x7f100140

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->f:Landroid/widget/TextView;

    const v0, 0x7f100141

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->g:Landroid/view/View;

    const v0, 0x7f100142

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->h:Landroid/view/View;

    const v0, 0x7f100143

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->i:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public setRoute(LO/z;)V
    .locals 7

    const/16 v6, 0x8

    const/4 v2, 0x0

    invoke-virtual {p1}, LO/z;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->b:Landroid/widget/ImageView;

    const v1, 0x7f02013b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d00de

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->a:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {p1}, LO/z;->p()I

    move-result v3

    invoke-virtual {p1}, LO/z;->o()I

    move-result v4

    invoke-virtual {p1}, LO/z;->q()I

    move-result v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(III)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, LO/U;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    invoke-virtual {v1}, LO/V;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/RelativeSizeSpan;

    const/high16 v3, 0x3fa00000

    invoke-direct {v1, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :goto_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->a(LO/z;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->b:Landroid/widget/ImageView;

    const v1, 0x7f02013e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d00df

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->b:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d00e0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_0
    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    invoke-virtual {v1}, LO/V;->a()I

    move-result v1

    const/4 v3, 0x1

    if-gt v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d0006

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v1, v2

    :goto_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    if-lez v1, :cond_4

    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_4
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_6
    move-object v0, v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
