.class public Lcom/google/android/maps/driveabout/app/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/app/ProgressDialog;

.field private c:Landroid/content/DialogInterface$OnCancelListener;

.field private d:Landroid/app/Dialog;

.field private e:Lcom/google/android/maps/driveabout/app/bt;

.field private f:Landroid/app/Dialog;

.field private g:Landroid/app/Dialog;

.field private h:Landroid/app/Dialog;

.field private i:Landroid/app/Dialog;

.field private j:Landroid/app/Dialog;

.field private k:Landroid/app/Dialog;

.field private l:Landroid/widget/CheckBox;

.field private m:Landroid/app/AlertDialog;

.field private n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

.field private o:Landroid/widget/CheckBox;

.field private p:Landroid/app/Dialog;

.field private q:Landroid/app/Dialog;

.field private r:Lcom/google/android/maps/driveabout/app/dH;

.field private s:Landroid/app/Dialog;

.field private t:Landroid/app/Dialog;

.field private u:Landroid/app/Dialog;

.field private v:Landroid/app/Dialog;

.field private w:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/RecordingLevelsView;)Lcom/google/android/maps/driveabout/app/RecordingLevelsView;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/an;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/an;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    return-object v0
.end method

.method private u()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dH;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_e
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()Landroid/app/AlertDialog$Builder;
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method private x()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/app/Dialog;
    .locals 1

    const-string v0, "loading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "destinations"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    goto :goto_0

    :cond_1
    const-string v0, "layers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    goto :goto_0

    :cond_2
    const-string v0, "fatal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    goto :goto_0

    :cond_3
    const-string v0, "exitconfirmation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    goto :goto_0

    :cond_4
    const-string v0, "routeoptions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->b()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->g()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->i()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->l()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->j()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->o()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->c()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->e()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->f()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->q()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->x()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->s()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->t()V

    return-void
.end method

.method public a(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->v()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(Landroid/content/Context;I)Landroid/text/Spannable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ao;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ao;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method public a(IF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;->setSample(IF)V

    :cond_0
    return-void
.end method

.method public a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3, p5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    if-eqz p4, :cond_1

    if-eqz p6, :cond_1

    const v1, 0x7f0d0080

    new-instance v2, Lcom/google/android/maps/driveabout/app/aA;

    invoke-direct {v2, p0, p6}, Lcom/google/android/maps/driveabout/app/aA;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    if-eqz p6, :cond_2

    invoke-virtual {v0, p6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    :goto_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public a(IILandroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->e()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10012b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;->setNumSamples(I)V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f10012c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const-string v3, "RmiMail"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-static {v2, v3, v4}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d009b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0081

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0080

    new-instance v2, Lcom/google/android/maps/driveabout/app/aE;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/aE;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ap;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ap;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public a(LO/U;Lcom/google/android/maps/driveabout/app/cQ;)V
    .locals 8

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->p()V

    invoke-virtual {p1}, LO/U;->h()Lbl/h;

    move-result-object v5

    invoke-virtual {v5}, Lbl/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/c;

    invoke-virtual {v0}, Lbl/c;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/CharSequence;

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbl/c;

    invoke-virtual {v1}, Lbl/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LO/U;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lbl/a;->a(Lbl/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lbl/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v0, Lcom/google/android/maps/driveabout/app/aq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aq;-><init>(Lcom/google/android/maps/driveabout/app/an;LO/U;Lcom/google/android/maps/driveabout/app/cQ;Ljava/util/List;Lbl/h;)V

    invoke-virtual {v7, v6, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    goto :goto_0
.end method

.method a(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    return-void
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->f()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0078

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0083

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007f

    new-instance v2, Lcom/google/android/maps/driveabout/app/aC;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/aC;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/aB;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aB;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->k()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0076

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->j()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    if-eqz p3, :cond_1

    const v1, 0x7f0d007f

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aG;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->t()V

    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setGravity(I)V

    const v1, 0x7f0d004e

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v2, 0x1010041

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const-string v2, "HomeAddress"

    invoke-static {v1, v2, v4}, LR/s;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/av;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/av;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d004d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f020110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d0049

    new-instance v3, Lcom/google/android/maps/driveabout/app/aw;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/maps/driveabout/app/aw;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/widget/EditText;Lcom/google/android/maps/driveabout/app/aG;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V
    .locals 9

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->g()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bt;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bt;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->show()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/app/dL;)V
    .locals 6

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->c()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dH;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/dH;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    invoke-direct {v1, v3}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dH;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v2, 0x7f0d00d7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f020465

    const v5, 0x7f0d0085

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/dH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dH;->show()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1, p2}, Lcom/google/android/maps/driveabout/app/dH;->a(ILcom/google/android/maps/driveabout/app/dL;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    :goto_0
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method

.method public a([LO/U;ILcom/google/android/maps/driveabout/app/cR;)V
    .locals 8

    const/16 v7, 0x21

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    const v3, 0x7f0d00a3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v5, 0x1030042

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v6, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const v4, 0x7f0d00a5

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v5, 0x7f0f0003

    invoke-direct {v1, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_1
    new-instance v1, Lcom/google/android/maps/driveabout/app/am;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    array-length v3, p1

    invoke-direct {v1, v2, p1, v3, v6}, Lcom/google/android/maps/driveabout/app/am;-><init>(Landroid/content/Context;[LO/U;IZ)V

    const v2, 0x7f020156

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/google/android/maps/driveabout/app/ax;

    invoke-direct {v2, p0, p3}, Lcom/google/android/maps/driveabout/app/ax;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/google/android/maps/driveabout/app/ay;

    invoke-direct {v2, p0, p3, p1}, Lcom/google/android/maps/driveabout/app/ay;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;[LO/U;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    const v1, 0x7f0d0080

    new-instance v2, Lcom/google/android/maps/driveabout/app/az;

    invoke-direct {v2, p0, p3}, Lcom/google/android/maps/driveabout/app/az;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_2
    const v1, 0x7f0d00a4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V
    .locals 10

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->q()V

    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v4

    array-length v0, p1

    new-array v5, v0, [LO/b;

    array-length v0, p1

    invoke-static {p1, v1, v5, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    invoke-virtual {v2}, LO/b;->b()I

    move-result v2

    invoke-virtual {v4, v2}, LO/c;->a(I)LO/e;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, LO/e;->b()I

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v7, v0, [Z

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v8, v0, [Ljava/lang/String;

    move v2, v1

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v9, p1, v0

    invoke-virtual {v9}, LO/b;->c()I

    move-result v0

    if-ne v0, v3, :cond_3

    move v0, v3

    :goto_3
    aput-boolean v0, v7, v2

    invoke-virtual {v9}, LO/b;->b()I

    move-result v0

    invoke-virtual {v4, v0}, LO/c;->a(I)LO/e;

    move-result-object v0

    invoke-virtual {v0}, LO/e;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/google/android/maps/driveabout/app/ar;

    invoke-direct {v0, p0, v6, v5, v8}, Lcom/google/android/maps/driveabout/app/ar;-><init>(Lcom/google/android/maps/driveabout/app/an;Ljava/util/ArrayList;[LO/b;[Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/as;

    invoke-direct {v1, p0, p2, v5}, Lcom/google/android/maps/driveabout/app/as;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/aH;[LO/b;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0d00da

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v8, v7, v0}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d007e

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public b(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->o()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d005f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0080

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    goto :goto_0
.end method

.method public b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040035

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1000eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    const v0, 0x7f1000ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0d0033

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0082

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0084

    new-instance v2, Lcom/google/android/maps/driveabout/app/aD;

    invoke-direct {v2, p0, p2}, Lcom/google/android/maps/driveabout/app/aD;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dH;->cancel()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dH;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dH;

    :cond_0
    return-void
.end method

.method public c(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->s()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/at;

    invoke-direct {v0, p0, p2}, Lcom/google/android/maps/driveabout/app/at;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d00e6

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d00e7

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d007e

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d0080

    new-instance v3, Lcom/google/android/maps/driveabout/app/au;

    invoke-direct {v3, p0, v0}, Lcom/google/android/maps/driveabout/app/au;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->i()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d009a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {}, LJ/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public l()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    :cond_0
    return-void
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public q()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public r()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->x()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d00e4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d00e5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public s()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    :cond_0
    return-void
.end method
