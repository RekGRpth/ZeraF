.class Lcom/google/android/maps/driveabout/app/eM;
.super Landroid/os/CountDownTimer;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/eL;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/eL;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/eM;->a:Lcom/google/android/maps/driveabout/app/eL;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public declared-synchronized onFinish()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/eM;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eM;->a:Lcom/google/android/maps/driveabout/app/eL;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/app/eL;->c:Lcom/google/android/maps/driveabout/app/eG;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/eG;->a()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/eM;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onTick(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/eM;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eM;->a:Lcom/google/android/maps/driveabout/app/eL;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/app/eL;->c:Lcom/google/android/maps/driveabout/app/eG;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/eG;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
