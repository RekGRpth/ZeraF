.class public Lcom/google/android/maps/driveabout/app/dD;
.super LR/c;
.source "SourceFile"

# interfaces
.implements LO/q;
.implements Lcom/google/android/maps/driveabout/vector/ay;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/dG;

.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Looper;

.field private final d:Lcom/google/android/maps/driveabout/app/dB;

.field private e:Z

.field private f:LO/z;

.field private g:LO/N;

.field private h:I

.field private i:Z

.field private final j:Lr/t;

.field private k:Z

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Lr/t;Lcom/google/android/maps/driveabout/app/dB;)V
    .locals 2

    const-string v0, "TilePrefetcher"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dG;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/dG;-><init>(Lcom/google/android/maps/driveabout/app/dD;Lcom/google/android/maps/driveabout/app/dE;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->a:Lcom/google/android/maps/driveabout/app/dG;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dD;->j:Lr/t;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dD;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->j:Lr/t;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dE;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dE;-><init>(Lcom/google/android/maps/driveabout/app/dD;)V

    invoke-interface {v0, v1}, Lr/t;->a(Lr/A;)V

    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private a(I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(IILjava/lang/Object;)V

    return-void
.end method

.method private a(IILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private a(ILjava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/dD;->a(IILjava/lang/Object;)V

    return-void
.end method

.method private a(J)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->e:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->k:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x4

    cmp-long v0, p1, v0

    if-gtz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->j()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->k:Z

    goto :goto_0
.end method

.method private a(LO/N;I)V
    .locals 3

    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dD;->g:LO/N;

    iput p2, p0, Lcom/google/android/maps/driveabout/app/dD;->h:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->g:LO/N;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dD;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/N;I)V

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->w()I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    :cond_0
    return-void
.end method

.method private a(LO/z;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dD;->f:LO/z;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->f:LO/z;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/z;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->f()V

    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dD;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dD;IILjava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/dD;->a(IILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dD;LO/N;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dD;->a(LO/N;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dD;LO/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dD;->a(LO/z;)V

    return-void
.end method

.method private b(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dD;->n:I

    :cond_0
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dD;->n:I

    if-nez v1, :cond_1

    invoke-virtual {v0}, LR/m;->v()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    return-void

    :cond_1
    invoke-virtual {v0}, LR/m;->w()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->e()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dD;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dD;->b(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/dD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->g()V

    return-void
.end method

.method private d()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->e:Z

    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/dD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->h()V

    return-void
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->e:Z

    return-void
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/dD;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->i()V

    return-void
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dB;->c()Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->j:Lr/t;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dD;->a:Lcom/google/android/maps/driveabout/app/dG;

    sget-object v3, Lr/c;->d:Lr/c;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lr/t;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->f:LO/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->g:LO/N;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->h:I

    if-ltz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dB;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->f:LO/z;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/z;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dD;->g:LO/N;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dD;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/N;I)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->f()V

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->k:Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->j()V

    return-void
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    return-void
.end method

.method private j()V
    .locals 4

    const-wide/16 v2, 0x190

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->j:Lr/t;

    invoke-interface {v0}, Lr/t;->j()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dD;->k()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v2, v3}, Lcom/google/android/maps/driveabout/app/dD;->a(J)V

    goto :goto_0
.end method

.method private k()V
    .locals 7

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/maps/driveabout/app/dD;->n:I

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->u()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dD;->d:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/dB;->c()Lo/aq;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dD;->i:Z

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/dD;->j:Lr/t;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dD;->a:Lcom/google/android/maps/driveabout/app/dG;

    sget-object v6, Lr/c;->d:Lr/c;

    invoke-interface {v4, v3, v5, v6, v1}, Lr/t;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/maps/driveabout/app/dD;->m:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(ILO/g;LO/s;)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method public a(LO/j;I)V
    .locals 0

    return-void
.end method

.method public a(LO/s;)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method public b(LO/j;I)V
    .locals 0

    return-void
.end method

.method public b(LO/s;)V
    .locals 3

    invoke-virtual {p1}, LO/s;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dD;->l:I

    const/4 v0, 0x5

    invoke-virtual {p1}, LO/s;->e()I

    move-result v1

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dD;->a(IILjava/lang/Object;)V

    return-void
.end method

.method public c(LO/s;)V
    .locals 3

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dD;->l:I

    invoke-virtual {p1}, LO/s;->b()I

    move-result v1

    sub-int/2addr v0, v1

    const/16 v1, 0x7530

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, LO/s;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dD;->l:I

    const/4 v0, 0x5

    invoke-virtual {p1}, LO/s;->e()I

    move-result v1

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dD;->a(IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public d(LO/s;)V
    .locals 2

    const/4 v0, 0x4

    invoke-virtual {p1}, LO/s;->g()LO/z;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dD;->a(ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/dD;->b(LO/s;)V

    return-void
.end method

.method public e(LO/s;)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method public f(LO/s;)V
    .locals 0

    return-void
.end method

.method public g(LO/s;)V
    .locals 0

    return-void
.end method

.method public l()V
    .locals 4

    const/16 v0, 0xa

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->c:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/maps/driveabout/app/dF;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dF;-><init>(Lcom/google/android/maps/driveabout/app/dD;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->b:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "TilePrefetcherThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public n_()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dD;->a(I)V

    return-void
.end method

.method public o_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->c:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dD;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    return-void
.end method
