.class public Lcom/google/android/maps/driveabout/app/RouteFetcherService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:LO/t;

.field private b:Lcom/google/android/maps/driveabout/app/dc;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->b:Lcom/google/android/maps/driveabout/app/dc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->b:Lcom/google/android/maps/driveabout/app/dc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dc;->d()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->b:Lcom/google/android/maps/driveabout/app/dc;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->a:LO/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->a:LO/t;

    invoke-virtual {v0}, LO/t;->a()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->a:LO/t;

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-virtual {p0, p3}, Lcom/google/android/maps/driveabout/app/RouteFetcherService;->stopSelf(I)V

    const/4 v0, 0x2

    return v0
.end method
