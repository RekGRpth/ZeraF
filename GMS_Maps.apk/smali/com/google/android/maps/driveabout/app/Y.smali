.class public Lcom/google/android/maps/driveabout/app/Y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Ljava/util/Comparator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:I

.field private final e:I

.field private final f:LO/U;

.field private final g:I

.field private h:F

.field private i:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/Z;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/Z;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/Y;->j:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(IIZIILO/U;I)V
    .locals 1

    const/high16 v0, -0x40800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    iput v0, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->a:I

    iput p2, p0, Lcom/google/android/maps/driveabout/app/Y;->b:I

    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/Y;->c:Z

    iput p4, p0, Lcom/google/android/maps/driveabout/app/Y;->d:I

    iput p5, p0, Lcom/google/android/maps/driveabout/app/Y;->e:I

    iput-object p6, p0, Lcom/google/android/maps/driveabout/app/Y;->f:LO/U;

    iput p7, p0, Lcom/google/android/maps/driveabout/app/Y;->g:I

    return-void
.end method

.method static a()Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x4

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(I)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x1

    const/4 v6, 0x0

    move v2, p0

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(II)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x2

    const/4 v6, 0x0

    move v2, p0

    move v4, p1

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x3

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v6, p0

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static b(I)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v1, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v6, 0x0

    move v2, p0

    move v3, v1

    move v5, v4

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static c(I)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x5

    const/4 v6, 0x0

    move v2, p0

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static d(I)Lcom/google/android/maps/driveabout/app/Y;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x6

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, p0

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static synthetic k()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/app/Y;->j:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public a(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->a:I

    return v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->b:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/Y;->c:Z

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->d:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->e:I

    return v0
.end method

.method public g()LO/U;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Y;->f:LO/U;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->g:I

    return v0
.end method

.method public i()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    return v0
.end method

.method public j()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    return v0
.end method
