.class public Lcom/google/android/maps/driveabout/app/aQ;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:LO/t;

.field private B:Lcom/google/android/maps/driveabout/app/dO;

.field private C:Lcom/google/android/maps/driveabout/app/af;

.field private D:Lcom/google/android/maps/driveabout/app/a;

.field private E:Ljava/lang/Runnable;

.field private F:Ljava/lang/Runnable;

.field private G:LaH/h;

.field private H:Z

.field private I:LaH/h;

.field private J:F

.field private final K:Lcom/google/android/maps/driveabout/app/dt;

.field private final L:Lcom/google/android/maps/driveabout/app/dy;

.field private M:I

.field private N:LL/G;

.field private O:I

.field protected a:LM/n;

.field protected b:Lcom/google/android/maps/driveabout/app/NavigationService;

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:LaH/h;

.field protected g:F

.field protected h:F

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field protected p:Z

.field protected q:Z

.field protected r:I

.field protected s:I

.field protected t:J

.field protected u:LO/s;

.field protected v:[LO/U;

.field protected w:[LO/b;

.field protected x:Ljava/lang/String;

.field protected y:LO/a;

.field protected z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    const v3, 0x927c0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    iput v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->c:I

    const/16 v0, 0x7530

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->d:I

    iput v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->e:I

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    new-instance v0, Lcom/google/android/maps/driveabout/app/dt;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dt;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->K:Lcom/google/android/maps/driveabout/app/dt;

    new-instance v0, Lcom/google/android/maps/driveabout/app/dy;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dy;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->L:Lcom/google/android/maps/driveabout/app/dy;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    iput v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    return-void
.end method

.method private static a(ZI)V
    .locals 0

    invoke-static {p0}, LF/V;->a(Z)V

    return-void
.end method

.method private ah()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private ai()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    :cond_0
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    return-void
.end method

.method private d(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/aR;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aR;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    int-to-long v1, p1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method


# virtual methods
.method public A()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    return v0
.end method

.method public B()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    return-void
.end method

.method public C()LO/U;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public E()[LO/U;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    return-object v0
.end method

.method public F()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    return v0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->x:Ljava/lang/String;

    return-object v0
.end method

.method public H()[LO/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    return-object v0
.end method

.method protected I()LQ/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    return-object v0
.end method

.method protected J()V
    .locals 2

    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    invoke-virtual {v0, v1}, LM/n;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->h()V

    return-void
.end method

.method public K()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->L:Lcom/google/android/maps/driveabout/app/dy;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/dy;->a(Landroid/location/Location;)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public L()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    return-void
.end method

.method public M()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MockLocationManager is already enabled!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, LL/G;

    invoke-direct {v0}, LL/G;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    invoke-virtual {v0, v1}, LM/n;->a(LL/G;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, LL/O;->a(Landroid/content/Context;)LL/k;

    move-result-object v1

    invoke-virtual {v0, v1}, LL/G;->a(Ll/f;)V

    return-void
.end method

.method public N()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->l()V

    return-void
.end method

.method public O()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->j()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->S()V

    goto :goto_0
.end method

.method public P()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->i()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ah()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    return-void
.end method

.method public Q()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    return v0
.end method

.method public R()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/aS;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aS;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->c:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method public S()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->X()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    return-void
.end method

.method public T()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->c()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->d()V

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    :cond_0
    return-void
.end method

.method public U()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->aa()V

    :cond_0
    return-void
.end method

.method public V()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    :cond_0
    return-void
.end method

.method public W()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    :cond_0
    return-void
.end method

.method protected X()V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->g()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->d()V

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    :cond_0
    return-void
.end method

.method protected Y()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->j()V

    return-void
.end method

.method public Z()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    return-void
.end method

.method public a(F)Lo/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0, p1}, LO/s;->a(F)Lo/am;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-void
.end method

.method public a(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->g()V

    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/aQ;->a(ZI)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0, p1}, LM/n;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->h()V

    return-void
.end method

.method protected a(ILO/g;LO/s;)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    if-nez p1, :cond_1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p3}, LO/s;->l()Z

    move-result v2

    if-nez v2, :cond_3

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->aa()V

    goto :goto_1

    :cond_2
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p2}, LQ/s;->b(LO/g;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(LM/C;)V
    .locals 1

    invoke-virtual {p1}, LM/C;->c()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    invoke-virtual {p1}, LM/C;->d()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->h:F

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->K()V

    return-void
.end method

.method protected a(LM/n;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aV;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aV;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {v0, v1}, LM/n;->a(LM/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0, v1}, LM/n;->a(LM/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aT;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/aT;-><init>(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/aR;)V

    invoke-virtual {v0, v1}, LM/n;->a(LM/s;)V

    return-void
.end method

.method protected a(LO/s;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ac()V

    return-void
.end method

.method protected a(LO/t;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    new-instance v0, Lcom/google/android/maps/driveabout/app/ba;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ba;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {p1, v0}, LO/t;->a(LO/q;)V

    return-void
.end method

.method protected a(LaH/h;)V
    .locals 8

    const/4 v1, 0x0

    const/high16 v3, 0x42c80000

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v2, v0}, LM/n;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/ci;->a(LaH/h;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ac()V

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v2, 0x1

    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v4

    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Lo/T;->a(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    if-eqz v0, :cond_4

    const/high16 v0, 0x42a00000

    :goto_0
    float-to-double v6, v0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_7

    move v0, v1

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_5

    const-string v1, "o"

    :goto_2
    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    invoke-virtual {v1}, LaH/h;->q()Lo/u;

    move-result-object v1

    invoke-virtual {v0, v1}, LaH/h;->b(Lo/u;)F

    move-result v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->J()V

    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, v3

    goto :goto_0

    :cond_5
    const-string v1, "O"

    goto :goto_2

    :cond_6
    const-string v1, "driveabout_base_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/dO;LO/t;LM/n;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/a;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/aQ;->C:Lcom/google/android/maps/driveabout/app/af;

    iput-object p6, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {p0, p3}, Lcom/google/android/maps/driveabout/app/aQ;->a(LO/t;)V

    invoke-virtual {p0, p4}, Lcom/google/android/maps/driveabout/app/aQ;->a(LM/n;)V

    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->g(Z)V

    return-void
.end method

.method public a([LO/U;I[LO/b;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V

    return-void
.end method

.method public a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/aQ;->a(ZI)V

    iput p2, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/aQ;->x:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->J()V

    return-void
.end method

.method protected a([LO/W;)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-object v2, p1, v0

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-nez v2, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/U;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/U;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    invoke-static {v0}, LF/V;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-virtual {v0, v1, v2}, LO/c;->a(Landroid/content/Context;[LO/b;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->C:Lcom/google/android/maps/driveabout/app/af;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->l()LO/U;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v2}, LO/z;->m()LO/U;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/af;->a(LO/U;LO/U;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationService;->a([LO/U;I[LO/b;)V

    return-void
.end method

.method protected aa()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ad()V

    return-void
.end method

.method protected ab()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ac()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ad()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ae()V

    goto :goto_0
.end method

.method protected ad()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Y()V

    return-void
.end method

.method protected ae()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Z()V

    return-void
.end method

.method public af()I
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    return v0
.end method

.method public ag()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    return v0
.end method

.method public b()LaH/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    return-void
.end method

.method protected b(LO/s;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    if-lez v2, :cond_0

    invoke-virtual {v1}, LO/N;->i()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LQ/s;->b(LO/N;LO/N;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->K:Lcom/google/android/maps/driveabout/app/dt;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v3}, LO/s;->g()LO/z;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/maps/driveabout/app/dt;->a(LO/z;LO/N;LO/N;LaH/h;)V

    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dO;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    return v0
.end method

.method protected c(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    return-void
.end method

.method protected c(LO/s;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LO/s;->d()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1}, LO/s;->d()I

    move-result v0

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->e:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->d(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    return-void
.end method

.method protected d(LO/s;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Ljava/lang/String;)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v0, v1}, LM/n;->a(LO/z;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->c()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LQ/s;->b(LO/z;[LO/z;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LQ/s;->b(LO/N;LO/N;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->v()[LO/W;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/W;)V

    :cond_1
    return-void
.end method

.method public d()Z
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    return-void
.end method

.method protected e(LO/s;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ae()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ah()V

    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->d(I)V

    :cond_0
    return-void
.end method

.method public f()LO/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    return-object v0
.end method

.method protected f(LO/s;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v1}, LO/s;->k()I

    move-result v1

    invoke-virtual {v0, v1}, LQ/s;->a(I)V

    return-void
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->c()I

    move-result v0

    return v0
.end method

.method protected g(LO/s;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->R()V

    return-void
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->b()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->d()I

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    return v0
.end method

.method public k()LO/N;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    return-object v0
.end method

.method public l()LO/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    return-object v0
.end method

.method public m()[LO/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v0

    return-object v0
.end method

.method public n()LO/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->i()LO/z;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->j()Z

    move-result v0

    return v0
.end method

.method public p()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->k()I

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    return v0
.end method

.method public r()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    return v0
.end method

.method public s()I
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->t:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public t()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->t:J

    return-void
.end method

.method public u()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    return-void
.end method

.method public v()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    return v0
.end method

.method public x()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
