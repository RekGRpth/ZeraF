.class public LQ/z;
.super LQ/E;
.source "SourceFile"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LQ/E;-><init>()V

    return-void
.end method

.method static synthetic a(LQ/z;Z)Z
    .locals 0

    iput-boolean p1, p0, LQ/z;->c:Z

    return p1
.end method


# virtual methods
.method public F()Z
    .locals 1

    iget-boolean v0, p0, LQ/z;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/z;->c:Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LQ/E;->F()Z

    move-result v0

    goto :goto_0
.end method

.method public a()V
    .locals 5

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->f()LO/a;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v1}, LO/t;->a(LO/a;)V

    :goto_0
    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/z;->b()V

    return-void

    :cond_0
    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->f()LO/t;

    move-result-object v1

    iget-object v2, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->E()[LO/U;

    move-result-object v2

    iget-object v3, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    iget-object v4, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->H()[LO/b;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, LO/t;->a(LaH/h;[LO/U;I[LO/b;)V

    const-string v1, "i"

    invoke-virtual {v0}, LaH/h;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "F"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(LO/g;)V
    .locals 5

    const v4, 0x7f0d00c4

    const/4 v3, 0x1

    invoke-virtual {p1}, LO/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-virtual {p1}, LO/g;->g()[LO/U;

    move-result-object v1

    new-instance v2, LQ/A;

    invoke-direct {v2, p0}, LQ/A;-><init>(LQ/z;)V

    invoke-interface {v0, v1, v3, v3, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a([LO/U;ZZLcom/google/android/maps/driveabout/app/cR;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->G()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->G()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_1
    :goto_1
    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->c()V

    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/app/cQ;->j()V

    if-nez v0, :cond_2

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-virtual {p0, p1}, LQ/z;->c(LO/g;)I

    move-result v1

    invoke-interface {v0, v4, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(II)V

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, LQ/z;->c:Z

    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-virtual {p0, p1}, LQ/z;->c(LO/g;)I

    move-result v2

    invoke-interface {v1, v4, v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(IILandroid/content/Intent;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method protected a(LO/z;[LO/z;)V
    .locals 3

    iget-object v0, p0, LQ/z;->a:LQ/p;

    sget-object v1, LQ/x;->e:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method protected ai()V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    const/4 v0, 0x2

    if-ne v3, v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v4, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/maps/driveabout/app/a;->b(Z)V

    if-eqz v0, :cond_4

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->getLatitude()D

    move-result-wide v4

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v4

    sget-object v5, LA/c;->b:LA/c;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bE;->b()I

    move-result v0

    const/16 v4, 0x10

    if-gt v0, v4, :cond_2

    move v0, v1

    :goto_1
    iget-object v4, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->b()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v4

    const-string v5, "SatelliteImagery"

    invoke-virtual {v4, v5, v0}, LR/u;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, LQ/z;->h(Z)V

    const/4 v0, 0x3

    if-ne v3, v0, :cond_3

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v3}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v2, "BicyclingLayer"

    invoke-virtual {v0, v2, v1}, LR/u;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, LQ/z;->i(Z)V

    :goto_2
    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    packed-switch v3, :pswitch_data_0

    const v0, 0x7f0d006b

    :goto_3
    iget-object v1, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/cS;->a(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, LQ/z;->i(Z)V

    goto :goto_2

    :pswitch_0
    const v0, 0x7f0d006c

    goto :goto_3

    :pswitch_1
    const v0, 0x7f0d006d

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/z;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LQ/z;->a:LQ/p;

    sget-object v1, LQ/x;->e:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_1
    return-void
.end method

.method public d()V
    .locals 1

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d()V

    :cond_0
    return-void
.end method
