.class public LQ/m;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:Z

.field private d:LQ/r;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:LO/z;

.field private j:[LO/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/s;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->c:Z

    return-void
.end method

.method static synthetic a(LQ/m;)LO/z;
    .locals 1

    iget-object v0, p0, LQ/m;->i:LO/z;

    return-object v0
.end method

.method private a(LO/z;LO/z;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, LO/z;->v()[LO/W;

    move-result-object v2

    invoke-virtual {p2}, LO/z;->v()[LO/W;

    move-result-object v3

    array-length v0, v2

    array-length v4, v3

    if-eq v0, v4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/W;->c()Lo/u;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v5}, LO/W;->c()Lo/u;

    move-result-object v5

    invoke-virtual {v4, v5}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static synthetic a(LQ/m;Z)Z
    .locals 0

    iput-boolean p1, p0, LQ/m;->g:Z

    return p1
.end method

.method private ap()V
    .locals 3

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/m;->j:[LO/z;

    :goto_0
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->i:LO/z;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [LO/z;

    const/4 v1, 0x0

    iget-object v2, p0, LQ/m;->i:LO/z;

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method private aq()V
    .locals 3

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, LO/c;->a([LO/b;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00eb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dz;->b(Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    goto :goto_0
.end method

.method private ar()V
    .locals 3

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_0

    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v0

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-virtual {v2}, LO/z;->D()[LO/b;

    move-result-object v2

    invoke-virtual {v0, v2}, LO/c;->b([LO/b;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->e(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LO/z;[LO/z;)LO/z;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    aget-object v1, p2, v0

    invoke-direct {p0, p1, v1}, LQ/m;->a(LO/z;LO/z;)Z

    move-result v1

    if-eqz v1, :cond_0

    aget-object v0, p2, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public C()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LQ/m;->f:Z

    const v0, 0x7f0d0071

    invoke-virtual {p0, v0}, LQ/m;->c(I)V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LO/t;->a(LaH/h;LO/z;)V

    :goto_0
    invoke-virtual {p0}, LQ/m;->m()V

    return-void

    :cond_0
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v2}, LO/t;->b(Z)V

    iget-boolean v0, p0, LQ/m;->c:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LQ/m;->c:Z

    iput-boolean v1, p0, LQ/m;->h:Z

    invoke-virtual {p0}, LQ/m;->u()V

    invoke-virtual {p0, v2}, LQ/m;->a(Z)V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->i:LO/z;

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->j:[LO/z;

    invoke-direct {p0}, LQ/m;->ap()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public D()V
    .locals 3

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    new-instance v1, LQ/n;

    invoke-direct {v1, p0, v0}, LQ/n;-><init>(LQ/m;[LO/b;)V

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    return-void
.end method

.method public E()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LQ/m;->d:LQ/r;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LQ/m;->ak()V

    const/4 v1, 0x0

    iput-object v1, p0, LQ/m;->d:LQ/r;

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, LQ/m;->f:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, LQ/m;->ak()V

    iput-boolean v1, p0, LQ/m;->c:Z

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    iput-boolean v1, p0, LQ/m;->f:Z

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, LQ/m;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->f()LO/t;

    move-result-object v2

    invoke-virtual {v2}, LO/t;->b()V

    invoke-virtual {p0}, LQ/m;->ak()V

    iput-boolean v1, p0, LQ/m;->g:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->c:Z

    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/m;->b()V

    return-void
.end method

.method protected a(LO/g;)V
    .locals 0

    invoke-super {p0, p1}, LQ/s;->a(LO/g;)V

    invoke-virtual {p0}, LQ/m;->ak()V

    return-void
.end method

.method public a(LO/z;)V
    .locals 0

    iput-object p1, p0, LQ/m;->i:LO/z;

    invoke-direct {p0}, LQ/m;->ap()V

    invoke-direct {p0}, LQ/m;->aq()V

    invoke-virtual {p0}, LQ/m;->u()V

    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, LQ/m;->ak()V

    iget-boolean v0, p0, LQ/m;->f:Z

    if-eqz v0, :cond_4

    iput-boolean v3, p0, LQ/m;->f:Z

    array-length v0, p2

    if-le v0, v2, :cond_0

    iput-boolean v2, p0, LQ/m;->c:Z

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v3}, LO/t;->b(Z)V

    :cond_0
    :goto_0
    iput-boolean v2, p0, LQ/m;->h:Z

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_5

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-direct {p0, v0, p2}, LQ/m;->c(LO/z;[LO/z;)LO/z;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LQ/m;->i:LO/z;

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-nez v0, :cond_1

    iput-object p1, p0, LQ/m;->i:LO/z;

    :cond_1
    iput-object p2, p0, LQ/m;->j:[LO/z;

    invoke-direct {p0}, LQ/m;->ap()V

    invoke-direct {p0}, LQ/m;->aq()V

    invoke-virtual {p0}, LQ/m;->m()V

    invoke-direct {p0}, LQ/m;->ar()V

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    :cond_2
    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->y()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LQ/m;->d:LQ/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, LQ/m;->a:LQ/p;

    iget-object v2, p0, LQ/m;->d:LQ/r;

    invoke-virtual {v0, v2}, LQ/p;->a(LQ/r;)V

    iput-object v1, p0, LQ/m;->d:LQ/r;

    :cond_3
    return-void

    :cond_4
    iget-boolean v0, p0, LQ/m;->g:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, LQ/m;->g:Z

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_0

    array-length v0, p2

    if-ne v0, v2, :cond_0

    iput-boolean v3, p0, LQ/m;->c:Z

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v2}, LO/t;->b(Z)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .locals 1

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .locals 1

    invoke-super {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a2

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 0

    return-void
.end method

.method protected a(Lo/T;)V
    .locals 16

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/high16 v1, 0x3fc4000000000000L

    const-wide/high16 v3, 0x4000000000000000L

    const/high16 v5, 0x41f00000

    move-object/from16 v0, p0

    iget-object v6, v0, LQ/m;->a:LQ/p;

    invoke-virtual {v6}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v6

    invoke-virtual {v6}, LC/b;->a()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    mul-double v9, v1, v3

    const-wide v4, 0x7fefffffffffffffL

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v1, v0, LQ/m;->c:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    move v6, v1

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->j:[LO/z;

    array-length v1, v1

    if-ge v6, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->j:[LO/z;

    aget-object v3, v1, v6

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v9, v10}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v1

    check-cast v1, LO/D;

    if-nez v1, :cond_2

    move-object v1, v2

    move-wide v2, v4

    :goto_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-wide v4, v2

    move-object v2, v1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, LO/D;->d()D

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v11, v0, LQ/m;->i:LO/z;

    if-ne v3, v11, :cond_3

    const-wide v11, 0x3fe999999999999aL

    mul-double/2addr v7, v11

    :cond_3
    cmpg-double v7, v7, v4

    if-gez v7, :cond_5

    invoke-virtual {v1}, LO/D;->d()D

    move-result-wide v1

    move-object v13, v3

    move-wide v14, v1

    move-wide v2, v14

    move-object v1, v13

    goto :goto_2

    :cond_4
    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->i:LO/z;

    if-eq v2, v1, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LQ/m;->a(LO/z;)V

    goto :goto_0

    :cond_5
    move-object v1, v2

    move-wide v2, v4

    goto :goto_2
.end method

.method protected a(Z)V
    .locals 2

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LQ/m;->h:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->h:Z

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->c(Lcom/google/android/maps/driveabout/app/aQ;)V

    goto :goto_0
.end method

.method public a(LQ/r;)Z
    .locals 1

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0070

    invoke-virtual {p0, v0}, LQ/m;->c(I)V

    iput-object p1, p0, LQ/m;->d:LQ/r;

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/m;->h:Z

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->E()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->j:[LO/z;

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {p0}, LQ/m;->m()V

    invoke-direct {p0}, LQ/m;->ar()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LQ/m;->ap()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LQ/m;->i:LO/z;

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->f()LO/t;

    move-result-object v1

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-virtual {v1, v2}, LO/t;->a(LO/z;)V

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->f()LO/t;

    move-result-object v1

    invoke-virtual {v1, v0}, LO/t;->a(Z)V

    iget-object v1, p0, LQ/m;->i:LO/z;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "|r="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LQ/m;->i:LO/z;

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    const-string v3, "|d"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/z;->p()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "|t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/z;->o()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    aget-object v3, v2, v0

    iget-object v4, p0, LQ/m;->i:LO/z;

    if-ne v3, v4, :cond_0

    const-string v3, "|s="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "L"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LO/t;->b(Z)V

    invoke-super {p0}, LQ/s;->d()V

    iput-object v5, p0, LQ/m;->i:LO/z;

    iput-object v5, p0, LQ/m;->j:[LO/z;

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->J()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->s()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    return-void
.end method

.method protected m()V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-boolean v2, p0, LQ/m;->f:Z

    if-nez v2, :cond_1

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->o()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LQ/m;->j:[LO/z;

    if-eqz v2, :cond_1

    iget-object v2, p0, LQ/m;->j:[LO/z;

    array-length v2, v2

    if-le v2, v0, :cond_1

    :cond_0
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-boolean v1, p0, LQ/m;->c:Z

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->d(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()V
    .locals 1

    iget-boolean v0, p0, LQ/m;->c:Z

    iput-boolean v0, p0, LQ/m;->e:Z

    return-void
.end method

.method public q()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/m;->h:Z

    iget-boolean v0, p0, LQ/m;->e:Z

    iput-boolean v0, p0, LQ/m;->c:Z

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LQ/m;->m()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/t;->b(Z)V

    :cond_0
    invoke-direct {p0}, LQ/m;->ap()V

    invoke-direct {p0}, LQ/m;->ar()V

    invoke-virtual {p0}, LQ/m;->u()V

    return-void
.end method

.method protected r()V
    .locals 0

    return-void
.end method

.method protected u()V
    .locals 3

    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->i:LO/z;

    iget-object v2, p0, LQ/m;->j:[LO/z;

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/z;[LO/z;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    goto :goto_0
.end method

.method protected v()V
    .locals 2

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    return-void
.end method

.method protected x()V
    .locals 2

    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    return-void
.end method
