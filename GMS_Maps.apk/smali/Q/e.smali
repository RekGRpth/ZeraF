.class public LQ/e;
.super LQ/f;
.source "SourceFile"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/f;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/e;->c:Z

    return-void
.end method

.method private c(LO/N;Z)V
    .locals 2

    invoke-virtual {p0, p1}, LQ/e;->a(LO/N;)V

    invoke-virtual {p0}, LQ/e;->ah()V

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->z()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    goto :goto_0
.end method


# virtual methods
.method protected a(LO/N;LO/N;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2, v1}, LQ/e;->c(LO/N;Z)V

    :cond_0
    iput-boolean v1, p0, LQ/e;->c:Z

    return-void
.end method

.method protected a(LO/N;Z)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, LQ/e;->c(LO/N;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .locals 1

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    return-void
.end method

.method protected a(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, LQ/e;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    iput-boolean v3, p0, LQ/e;->c:Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    invoke-virtual {p0}, LQ/e;->ah()V

    iput-boolean v2, p0, LQ/e;->c:Z

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    new-array v2, v2, [LO/z;

    const/4 v3, 0x0

    iget-object v4, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    :cond_0
    return-void
.end method

.method protected g()V
    .locals 1

    invoke-super {p0}, LQ/f;->g()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/e;->c:Z

    return-void
.end method

.method protected n()V
    .locals 3

    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/e;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LQ/e;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_0
.end method
