.class public LQ/C;
.super LQ/E;
.source "SourceFile"


# instance fields
.field public c:Z

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/E;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/C;->c:Z

    new-instance v0, LQ/D;

    invoke-direct {v0, p0}, LQ/D;-><init>(LQ/C;)V

    iput-object v0, p0, LQ/C;->d:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(Lcom/google/android/maps/driveabout/app/aQ;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->d()LO/V;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "maps.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "myl"

    const-string v3, "saddr"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "dirflg"

    const-string v3, "d"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    :goto_0
    const-string v2, "daddr"

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v3

    invoke-virtual {v3}, LO/U;->d()LO/V;

    move-result-object v3

    invoke-virtual {v3}, LO/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const-string v2, "dirflg"

    const-string v3, "w"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const-string v2, "dirflg"

    const-string v3, "b"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_3
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method static synthetic a(LQ/C;)Z
    .locals 1

    invoke-direct {p0}, LQ/C;->as()Z

    move-result v0

    return v0
.end method

.method private ap()V
    .locals 5

    invoke-direct {p0}, LQ/C;->ar()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(I)V

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, LQ/C;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->c()V

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const v1, 0x7f0d005c

    const v2, 0x7f0d00c8

    const v3, 0x7f0d00c9

    iget-object v4, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-static {v4}, LQ/C;->a(Lcom/google/android/maps/driveabout/app/aQ;)Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(IIILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private aq()I
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x3

    :cond_0
    return v0
.end method

.method private ar()Z
    .locals 2

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->h()Lcom/google/googlenav/j;

    move-result-object v0

    invoke-direct {p0}, LQ/C;->aq()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/j;->a(I)Z

    move-result v0

    return v0
.end method

.method private as()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/ci;->h()Lcom/google/googlenav/j;

    move-result-object v1

    iget-object v2, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-static {v2}, LR/e;->a(Landroid/location/Location;)LaN/B;

    move-result-object v2

    invoke-direct {p0}, LQ/C;->aq()I

    move-result v3

    invoke-virtual {v1, v3, v2, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(LQ/C;)V
    .locals 0

    invoke-direct {p0}, LQ/C;->ap()V

    return-void
.end method


# virtual methods
.method protected ai()V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-static {}, LO/T;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->c()V

    iget-object v0, p0, LQ/C;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const v1, 0x7f0d005c

    const v2, 0x7f0d00c8

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(II)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, LQ/C;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/C;->c:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LQ/C;->ap()V

    goto :goto_0
.end method

.method public c()V
    .locals 3

    iget-boolean v0, p0, LQ/C;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/C;->a:LQ/p;

    sget-object v1, LQ/x;->d:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_0
    return-void
.end method
