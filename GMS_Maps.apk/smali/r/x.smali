.class Lr/x;
.super Lr/b;
.source "SourceFile"


# instance fields
.field final synthetic d:Lr/w;


# direct methods
.method constructor <init>(Lr/w;)V
    .locals 0

    iput-object p1, p0, Lr/x;->d:Lr/w;

    invoke-direct {p0, p1}, Lr/b;-><init>(Lr/a;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lr/x;->b:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lr/x;->b:[[B

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lr/x;->b:[[B

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lp/d;->a([BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected b(I)Lo/ap;
    .locals 7

    iget-object v0, p0, Lr/x;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lr/x;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    iget-object v1, p0, Lr/x;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    const-wide/32 v5, 0x48190800

    add-long/2addr v3, v5

    invoke-static {v0, v1, v2, v3, v4}, Lp/d;->a(Lo/aq;[BIJ)Lp/d;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(I)[B
    .locals 1

    iget-object v0, p0, Lr/x;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
