.class public Lr/B;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lt/M;

.field b:Lt/f;

.field volatile c:Z

.field private d:I

.field private final e:Z

.field private f:Ljava/util/Locale;

.field private final g:Ljava/lang/String;

.field private h:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lt/M;Lt/f;ZLjava/util/Locale;Ljava/io/File;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lr/B;->g:Ljava/lang/String;

    iput-object p2, p0, Lr/B;->a:Lt/M;

    iput-object p3, p0, Lr/B;->b:Lt/f;

    const/4 v0, -0x1

    iput v0, p0, Lr/B;->d:I

    iput-boolean p4, p0, Lr/B;->e:Z

    iput-object p5, p0, Lr/B;->f:Ljava/util/Locale;

    iput-object p6, p0, Lr/B;->h:Ljava/io/File;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/B;->b:Lt/f;

    iget-object v1, p0, Lr/B;->h:Ljava/io/File;

    invoke-interface {v0, v1}, Lt/f;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    :cond_0
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lr/B;->f:Ljava/util/Locale;

    iget-object v1, p0, Lr/B;->b:Lt/f;

    invoke-interface {v1}, Lt/f;->f()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lr/B;->b:Lt/f;

    iget-object v1, p0, Lr/B;->f:Ljava/util/Locale;

    invoke-interface {v0, v1}, Lt/f;->a(Ljava/util/Locale;)Z

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/B;->c:Z

    :cond_2
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Ljava/util/Locale;)V
    .locals 1

    iget-object v0, p0, Lr/B;->f:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lt/f;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    :cond_1
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    :cond_2
    iput-object p1, p0, Lr/B;->f:Ljava/util/Locale;

    goto :goto_0
.end method

.method a(I)Z
    .locals 2

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lt/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput p1, p0, Lr/B;->d:I

    iget-boolean v1, p0, Lr/B;->e:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lt/f;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_1
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lo/aq;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v1

    invoke-interface {v1, p1}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lo/ap;->e()I

    move-result v1

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v2

    invoke-interface {v2}, Lt/f;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()Lt/f;
    .locals 1

    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lr/B;->c:Z

    if-nez v0, :cond_1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lr/B;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    monitor-exit p0

    :goto_1
    return-object v0

    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-object v0, p0, Lr/B;->b:Lt/f;

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method protected c()V
    .locals 2

    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    :cond_0
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lt/f;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lt/f;->g()V

    iget-object v0, p0, Lr/B;->g:Ljava/lang/String;

    const-string v1, "Unable to clear disk cache"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    :cond_1
    return-void
.end method

.method d()I
    .locals 1

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lt/f;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lr/B;->d:I

    goto :goto_0
.end method

.method e()J
    .locals 2

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-interface {v0}, Lt/f;->e()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method f()J
    .locals 2

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-interface {v0}, Lt/f;->d()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method g()Z
    .locals 1

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    :cond_0
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lt/f;->g()V

    :cond_1
    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    :cond_0
    return-void
.end method
