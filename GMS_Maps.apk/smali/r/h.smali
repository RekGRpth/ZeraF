.class public abstract Lr/h;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:[Lr/k;

.field private b:I

.field private c:Lr/d;

.field private d:Ljava/util/Map;


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-array v0, p1, [Lr/k;

    iput-object v0, p0, Lr/h;->a:[Lr/k;

    const/4 v0, 0x0

    iput v0, p0, Lr/h;->b:I

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lr/h;->d:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lr/h;)Lr/d;
    .locals 1

    iget-object v0, p0, Lr/h;->c:Lr/d;

    return-object v0
.end method

.method static synthetic a(Lr/h;Lr/d;)Lr/d;
    .locals 0

    iput-object p1, p0, Lr/h;->c:Lr/d;

    return-object p1
.end method


# virtual methods
.method public A_()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lr/h;->a:[Lr/k;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lr/k;->i()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected a()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public a(Landroid/util/Pair;)Ljava/lang/Integer;
    .locals 3

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Lr/h;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final a(I)Lr/k;
    .locals 1

    iget-object v0, p0, Lr/h;->a:[Lr/k;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected final a(Landroid/util/Pair;Lr/k;)V
    .locals 3

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Lr/h;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate tile key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", already exists in batch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lr/h;->d:Ljava/util/Map;

    iget v1, p0, Lr/h;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lr/h;->a:[Lr/k;

    iget v1, p0, Lr/h;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lr/h;->b:I

    aput-object p2, v0, v1

    return-void
.end method

.method protected a(Lr/k;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract b(I)Lo/ap;
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lr/h;->b:I

    return v0
.end method

.method protected c(I)[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final d()Z
    .locals 2

    iget v0, p0, Lr/h;->b:I

    iget-object v1, p0, Lr/h;->a:[Lr/k;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
