.class public Lr/D;
.super Lr/I;
.source "SourceFile"


# static fields
.field public static final i:Lr/H;


# instance fields
.field private k:Z

.field private volatile l:Lr/H;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lr/E;

    invoke-direct {v0}, Lr/E;-><init>()V

    sput-object v0, Lr/D;->i:Lr/H;

    return-void
.end method

.method public constructor <init>(Law/p;LA/c;IFLjava/util/Locale;ZLjava/io/File;Lr/H;Lt/g;)V
    .locals 10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lr/I;-><init>(Law/p;LA/c;IFLjava/util/Locale;ZLjava/io/File;Lt/g;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lr/D;->k:Z

    move-object/from16 v0, p8

    iput-object v0, p0, Lr/D;->l:Lr/H;

    return-void
.end method

.method public static a(IZ)Lr/H;
    .locals 1

    new-instance v0, Lr/F;

    invoke-direct {v0, p0, p1}, Lr/F;-><init>(IZ)V

    return-object v0
.end method

.method private b(Lo/aq;Lo/aL;)Z
    .locals 1

    iget-object v0, p0, Lr/D;->l:Lr/H;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lr/D;->l:Lr/H;

    invoke-interface {v0, p1, p2}, Lr/H;->a(Lo/aq;Lo/aL;)Z

    move-result v0

    goto :goto_0
.end method

.method public static n()Lr/H;
    .locals 1

    new-instance v0, Lr/G;

    invoke-direct {v0}, Lr/G;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Lo/aq;Lo/aL;Ls/e;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lr/D;->b(Lo/aq;Lo/aL;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p3}, Lr/I;->a(Lo/aq;Ls/e;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p3, p1, v0, v1}, Ls/e;->a(Lo/aq;ILo/ap;)V

    goto :goto_0
.end method

.method public a(Lo/aq;Ls/e;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lr/D;->a(Lo/aq;Lo/aL;Ls/e;)V

    return-void
.end method

.method public a(Lr/H;)V
    .locals 0

    iput-object p1, p0, Lr/D;->l:Lr/H;

    return-void
.end method

.method public a(Lo/aq;Lo/aL;)Z
    .locals 1

    iget-boolean v0, p0, Lr/D;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lr/D;->b(Lo/aq;Lo/aL;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lr/D;->k:Z

    return-void
.end method
