.class Lq/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lo/af;

.field public b:Z


# direct methods
.method public constructor <init>(Lo/af;F)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lq/f;->a:Lo/af;

    iget-object v1, p0, Lq/f;->a:Lo/af;

    invoke-virtual {v1}, Lo/af;->b()Lo/X;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo/X;->d(I)F

    move-result v1

    invoke-static {v1, p2}, Lo/V;->a(FF)F

    move-result v1

    const/high16 v2, 0x42b40000

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lq/f;->b:Z

    return-void
.end method


# virtual methods
.method public a()Lo/T;
    .locals 2

    iget-object v0, p0, Lq/f;->a:Lo/af;

    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v0

    iget-boolean v1, p0, Lq/f;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lo/X;->c()Lo/T;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lo/af;)Z
    .locals 5

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lq/f;->a:Lo/af;

    invoke-virtual {v2}, Lo/af;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    move v2, v1

    :goto_1
    invoke-virtual {p1}, Lo/af;->c()I

    move-result v3

    if-ge v2, v3, :cond_2

    iget-object v3, p0, Lq/f;->a:Lo/af;

    invoke-virtual {v3, v0}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b()F
    .locals 2

    iget-object v0, p0, Lq/f;->a:Lo/af;

    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v0

    iget-boolean v1, p0, Lq/f;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Lo/X;->d(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/X;->d(I)F

    move-result v0

    invoke-static {v0}, Lq/e;->a(F)F

    move-result v0

    goto :goto_0
.end method
