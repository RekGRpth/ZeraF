.class public LaX/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:I

.field private final c:LaX/b;


# direct methods
.method public constructor <init>(Ljava/util/List;LaX/b;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Law/a;-><init>()V

    iput v3, p0, LaX/a;->b:I

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eO;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, LaX/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v2, p0, LaX/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    goto :goto_0

    :cond_0
    iput-object p2, p0, LaX/a;->c:LaX/b;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, LaX/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eO;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LaX/a;->b:I

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x86

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 2

    iget-object v0, p0, LaX/a;->c:LaX/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaX/a;->c:LaX/b;

    iget v1, p0, LaX/a;->b:I

    invoke-interface {v0, v1}, LaX/b;->a(I)V

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 2

    iget-object v0, p0, LaX/a;->c:LaX/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaX/a;->c:LaX/b;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LaX/b;->a(I)V

    :cond_0
    return-void
.end method
