.class public LX/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static volatile a:Z

.field private static volatile b:LX/q;

.field private static volatile c:LX/q;

.field private static final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-boolean v1, LX/j;->a:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, LX/j;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {}, LX/j;->c()V

    return-void
.end method

.method public static a()LX/q;
    .locals 1

    sget-object v0, LX/j;->b:LX/q;

    return-object v0
.end method

.method static synthetic a(LX/q;)LX/q;
    .locals 0

    sput-object p0, LX/j;->c:LX/q;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Law/h;LX/e;)V
    .locals 1

    const-string v0, "TransitNavigationParameters"

    invoke-static {p0, p1, p2, v0}, LX/j;->a(Landroid/content/Context;Law/h;LX/e;Ljava/lang/String;)V

    return-void
.end method

.method static a(Landroid/content/Context;Law/h;LX/e;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/16 v2, 0x9

    const/4 v3, 0x2

    sget-boolean v0, LX/j;->a:Z

    if-eqz v0, :cond_1

    invoke-static {}, LX/j;->b()V

    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/e;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p0, p3, v0}, LX/r;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_1
    new-instance v2, LX/q;

    invoke-direct {v2, v0}, LX/q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v2, LX/j;->b:LX/q;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :goto_2
    if-eqz p1, :cond_0

    new-instance v0, LX/f;

    const-class v1, LX/l;

    invoke-direct {v0, p1, p2, v1}, LX/f;-><init>(Law/h;LX/e;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Law/h;->a(Law/q;)V

    new-instance v0, LX/l;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/l;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;LX/e;LX/k;)V

    invoke-virtual {p1, v0}, Law/h;->c(Law/g;)V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->s:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    goto :goto_1

    :cond_3
    move-object v3, v5

    goto :goto_2
.end method

.method static b()V
    .locals 3

    sget-object v0, LX/j;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/j;->c:LX/q;

    sput-object v0, LX/j;->b:LX/q;

    :cond_0
    return-void
.end method

.method public static c()V
    .locals 3

    new-instance v0, LX/q;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->s:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-direct {v0, v1}, LX/q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, LX/j;->b:LX/q;

    return-void
.end method

.method static synthetic d()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    sget-object v0, LX/j;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method
