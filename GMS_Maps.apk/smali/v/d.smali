.class public Lv/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# static fields
.field private static a:Lv/d;


# instance fields
.field private final b:Law/h;

.field private final c:Lcom/google/googlenav/common/a;

.field private final d:LR/h;

.field private final e:LR/h;

.field private volatile f:Lt/e;

.field private final g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lv/d;->b:Law/h;

    iput-object v0, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    iput-object v0, p0, Lv/d;->d:LR/h;

    iput-object v0, p0, Lv/d;->e:LR/h;

    iput-object v0, p0, Lv/d;->f:Lt/e;

    iput-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private constructor <init>(Law/h;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv/d;->b:Law/h;

    iget-object v0, p0, Lv/d;->b:Law/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lv/d;->b:Law/h;

    invoke-virtual {v0, p0}, Law/h;->a(Law/q;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    new-instance v0, LR/h;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lv/d;->d:LR/h;

    new-instance v0, LR/h;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lv/d;->e:LR/h;

    const/4 v0, 0x0

    iput-object v0, p0, Lv/d;->f:Lt/e;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method static synthetic a(Lv/d;)Lt/e;
    .locals 1

    iget-object v0, p0, Lv/d;->f:Lt/e;

    return-object v0
.end method

.method public static a(Law/h;Ljava/io/File;)Lv/d;
    .locals 2

    sget-object v0, Lv/d;->a:Lv/d;

    if-nez v0, :cond_0

    new-instance v0, Lv/d;

    invoke-direct {v0, p0}, Lv/d;-><init>(Law/h;)V

    sput-object v0, Lv/d;->a:Lv/d;

    :cond_0
    new-instance v0, Lv/e;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lv/e;-><init>(Las/c;Ljava/io/File;)V

    invoke-virtual {v0}, Lv/e;->g()V

    sget-object v0, Lv/d;->a:Lv/d;

    return-object v0
.end method

.method private a(Ljava/io/File;)V
    .locals 1

    invoke-static {p1}, Lt/e;->a(Ljava/io/File;)Lt/e;

    move-result-object v0

    iput-object v0, p0, Lv/d;->f:Lt/e;

    invoke-direct {p0}, Lv/d;->e()V

    return-void
.end method

.method private a(Ljava/lang/String;Lv/a;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fj;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p2}, Lv/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p2}, Lv/a;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    new-instance v1, Lv/f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, p2, v2}, Lv/f;-><init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;Lv/e;)V

    new-instance v0, Ll/g;

    const-string v2, "addRequest"

    invoke-direct {v0, v2, v1}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget-object v0, p0, Lv/d;->b:Law/h;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method static synthetic a(Lv/d;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lv/d;->a(Ljava/io/File;)V

    return-void
.end method

.method public static c()Lv/d;
    .locals 1

    sget-object v0, Lv/d;->a:Lv/d;

    return-object v0
.end method

.method public static d()V
    .locals 1

    sget-object v0, Lv/d;->a:Lv/d;

    if-eqz v0, :cond_0

    sget-object v0, Lv/d;->a:Lv/d;

    invoke-virtual {v0}, Lv/d;->a()V

    const/4 v0, 0x0

    sput-object v0, Lv/d;->a:Lv/d;

    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method private f()V
    .locals 1

    :goto_0
    iget-object v0, p0, Lv/d;->f:Lt/e;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lv/c;)Lv/a;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lv/d;->a(Ljava/lang/String;Lv/c;Z)Lv/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lv/c;Z)Lv/a;
    .locals 7

    const/4 v1, 0x0

    if-eqz p3, :cond_3

    iget-object v2, p0, Lv/d;->e:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lv/d;->e:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv/a;

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lv/a;

    invoke-direct {v0}, Lv/a;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lv/a;->a(Z)V

    iget-object v1, p0, Lv/d;->e:LR/h;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v3}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-enter v0

    :try_start_1
    iget-object v1, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0}, Lv/a;->h()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    add-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-gez v3, :cond_1

    invoke-direct {p0, p1, v0}, Lv/d;->a(Ljava/lang/String;Lv/a;)V

    invoke-virtual {v0, v1, v2}, Lv/a;->a(J)V

    :cond_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lv/a;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p2}, Lv/a;->a(Lv/c;)V

    :cond_2
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    iget-object v1, p0, Lv/d;->d:LR/h;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lv/d;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv/a;

    if-nez v0, :cond_4

    iget-object v2, p0, Lv/d;->f:Lt/e;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0, p1}, Lt/e;->a(Ljava/lang/String;)Lv/a;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Lv/a;

    invoke-direct {v0}, Lv/a;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lv/a;->a(Z)V

    :cond_5
    iget-object v2, p0, Lv/d;->d:LR/h;

    invoke-virtual {v2, p1, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method a()V
    .locals 1

    invoke-direct {p0}, Lv/d;->f()V

    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->a()V

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Law/g;)V
    .locals 1

    instance-of v0, p1, Lv/f;

    if-eqz v0, :cond_0

    check-cast p1, Lv/f;

    invoke-virtual {p1}, Lv/f;->a()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v1, p0, Lv/d;->e:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lv/d;->e:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lv/d;->d:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lv/d;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lv/d;->f()V

    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->c()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lv/d;->f:Lt/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public b(Law/g;)V
    .locals 0

    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method
