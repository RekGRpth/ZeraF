.class public final Lcom/android/providers/settings/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/settings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final assisted_gps_enabled:I = 0x7f04000b

.field public static final data_usage_on_lockscreen:I = 0x7f040027

.field public static final def_accelerometer_rotation:I = 0x7f040004

.field public static final def_accessibility_display_magnification_auto_update:I = 0x7f04001e

.field public static final def_accessibility_display_magnification_enabled:I = 0x7f04001d

.field public static final def_accessibility_script_injection:I = 0x7f04001a

.field public static final def_accessibility_speak_password:I = 0x7f04001b

.field public static final def_airplane_mode_on:I = 0x7f040001

.field public static final def_auto_time:I = 0x7f040002

.field public static final def_auto_time_gps:I = 0x7f040023

.field public static final def_auto_time_zone:I = 0x7f040003

.field public static final def_backup_enabled:I = 0x7f040010

.field public static final def_battery_percentage:I = 0x7f040026

.field public static final def_bluetooth_on:I = 0x7f040007

.field public static final def_device_provisioned:I = 0x7f040017

.field public static final def_dim_screen:I = 0x7f040000

.field public static final def_dtmf_tones_enabled:I = 0x7f04001f

.field public static final def_haptic_feedback:I = 0x7f040006

.field public static final def_install_non_market_apps:I = 0x7f040009

.field public static final def_ipo_setting:I = 0x7f040025

.field public static final def_lockscreen_disabled:I = 0x7f040016

.field public static final def_mount_play_notification_snd:I = 0x7f040012

.field public static final def_mount_ums_autostart:I = 0x7f040013

.field public static final def_mount_ums_notify_enabled:I = 0x7f040015

.field public static final def_mount_ums_prompt:I = 0x7f040014

.field public static final def_netstats_enabled:I = 0x7f04000c

.field public static final def_networks_available_notification_on:I = 0x7f04000f

.field public static final def_notification_pulse:I = 0x7f040011

.field public static final def_notifications_use_ring_volume:I = 0x7f040018

.field public static final def_package_verifier_enable:I = 0x7f04000a

.field public static final def_roaming_indicate_needed:I = 0x7f040024

.field public static final def_screen_brightness_automatic_mode:I = 0x7f040005

.field public static final def_sound_effects_enabled:I = 0x7f040020

.field public static final def_stay_on_while_plugged_in:I = 0x7f040021

.field public static final def_touch_exploration_enabled:I = 0x7f04001c

.field public static final def_usb_mass_storage_enabled:I = 0x7f04000d

.field public static final def_user_setup_complete:I = 0x7f040022

.field public static final def_vibrate_in_silent:I = 0x7f040019

.field public static final def_wfd_auto_connect_on:I = 0x7f040028

.field public static final def_wifi_display_on:I = 0x7f040008

.field public static final def_wifi_on:I = 0x7f04000e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
