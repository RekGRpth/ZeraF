.class public Lcom/android/dreams/phototable/BummerView;
.super Landroid/widget/TextView;
.source "BummerView.java"


# static fields
.field public static final MOVE:I = 0x2

.field public static final START:I = 0x1


# instance fields
.field private mAnimTime:I

.field private mAnimate:Z

.field private mDelay:I

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/dreams/phototable/BummerView;->mDelay:I

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimTime:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimate:Z

    new-instance v0, Lcom/android/dreams/phototable/BummerView$1;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/BummerView$1;-><init>(Lcom/android/dreams/phototable/BummerView;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/BummerView;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/dreams/phototable/BummerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/dreams/phototable/BummerView;->mDelay:I

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimTime:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimate:Z

    new-instance v0, Lcom/android/dreams/phototable/BummerView$1;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/BummerView$1;-><init>(Lcom/android/dreams/phototable/BummerView;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/BummerView;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/dreams/phototable/BummerView;)Z
    .locals 1
    .param p0    # Lcom/android/dreams/phototable/BummerView;

    iget-boolean v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimate:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/dreams/phototable/BummerView;)I
    .locals 1
    .param p0    # Lcom/android/dreams/phototable/BummerView;

    iget v0, p0, Lcom/android/dreams/phototable/BummerView;->mAnimTime:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/dreams/phototable/BummerView;)I
    .locals 1
    .param p0    # Lcom/android/dreams/phototable/BummerView;

    iget v0, p0, Lcom/android/dreams/phototable/BummerView;->mDelay:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/dreams/phototable/BummerView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/dreams/phototable/BummerView;

    iget-object v0, p0, Lcom/android/dreams/phototable/BummerView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/android/dreams/phototable/BummerView$2;

    invoke-direct {v1, p0, v0}, Lcom/android/dreams/phototable/BummerView$2;-><init>(Lcom/android/dreams/phototable/BummerView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v1, p0, Lcom/android/dreams/phototable/BummerView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public setAnimationParams(ZII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    iput-boolean p1, p0, Lcom/android/dreams/phototable/BummerView;->mAnimate:Z

    iput p2, p0, Lcom/android/dreams/phototable/BummerView;->mDelay:I

    iput p3, p0, Lcom/android/dreams/phototable/BummerView;->mAnimTime:I

    return-void
.end method
