.class public Lcom/android/apps/tag/record/ImageRecord;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "ImageRecord.java"


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/apps/tag/record/ImageRecord;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public static isImage(Landroid/nfc/NdefRecord;)Z
    .locals 2
    .param p0    # Landroid/nfc/NdefRecord;

    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/record/ImageRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/ImageRecord;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/ImageRecord;
    .locals 5
    .param p0    # Landroid/nfc/NdefRecord;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toMimeType()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "not a valid image file"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "not a valid image file"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    new-instance v3, Lcom/android/apps/tag/record/ImageRecord;

    invoke-direct {v3, v0}, Lcom/android/apps/tag/record/ImageRecord;-><init>(Landroid/graphics/Bitmap;)V

    return-object v3
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    const v1, 0x7f030001

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/apps/tag/record/ImageRecord;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-object v0
.end method
