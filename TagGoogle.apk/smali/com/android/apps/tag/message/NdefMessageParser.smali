.class public Lcom/android/apps/tag/message/NdefMessageParser;
.super Ljava/lang/Object;
.source "NdefMessageParser.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRecords(Landroid/nfc/NdefMessage;)Ljava/util/List;
    .locals 1
    .param p0    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/nfc/NdefMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v0

    invoke-static {v0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;
    .locals 6
    .param p0    # [Landroid/nfc/NdefRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/nfc/NdefRecord;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_6

    aget-object v4, v0, v2

    invoke-static {v4}, Lcom/android/apps/tag/record/SmartPoster;->isPoster(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/android/apps/tag/record/SmartPoster;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v4}, Lcom/android/apps/tag/record/UriRecord;->isUri(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v4}, Lcom/android/apps/tag/record/UriRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/UriRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-static {v4}, Lcom/android/apps/tag/record/TextRecord;->isText(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v4}, Lcom/android/apps/tag/record/TextRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/TextRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-static {v4}, Lcom/android/apps/tag/record/ImageRecord;->isImage(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v4}, Lcom/android/apps/tag/record/ImageRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/ImageRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lcom/android/apps/tag/record/VCardRecord;->isVCard(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v4}, Lcom/android/apps/tag/record/VCardRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/VCardRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-static {v4}, Lcom/android/apps/tag/record/MimeRecord;->isMime(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {v4}, Lcom/android/apps/tag/record/MimeRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/MimeRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    new-instance v5, Lcom/android/apps/tag/record/UnknownRecord;

    invoke-direct {v5}, Lcom/android/apps/tag/record/UnknownRecord;-><init>()V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    return-object v1
.end method

.method public static parse(Landroid/nfc/NdefMessage;)Lcom/android/apps/tag/message/ParsedNdefMessage;
    .locals 2
    .param p0    # Landroid/nfc/NdefMessage;

    new-instance v0, Lcom/android/apps/tag/message/ParsedNdefMessage;

    invoke-static {p0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords(Landroid/nfc/NdefMessage;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/apps/tag/message/ParsedNdefMessage;-><init>(Ljava/util/List;)V

    return-object v0
.end method
