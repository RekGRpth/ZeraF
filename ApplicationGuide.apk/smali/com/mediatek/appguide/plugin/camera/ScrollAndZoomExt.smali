.class public Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;
.super Ljava/lang/Object;
.source "ScrollAndZoomExt.java"

# interfaces
.implements Lcom/mediatek/camera/ext/IAppGuideExt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;
    }
.end annotation


# static fields
.field private static final KEY_CAMERA_GUIDE:Ljava/lang/String; = "camera_guide"

.field private static final SHARED_PREFERENCE_NAME:Ljava/lang/String; = "application_guide"

.field private static final TAG:Ljava/lang/String; = "ScrollAndZoomExt"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mContext:Landroid/content/Context;

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "ScrollAndZoomExt"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public showCameraGuide(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    const/16 v4, 0x400

    const-string v1, "ScrollAndZoomExt"

    const-string v2, "showCameraGuide()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "application_guide"

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mSharedPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "camera_guide"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ScrollAndZoomExt"

    const-string v2, "already show camera guide, return"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;-><init>(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;Landroid/app/Activity;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method
