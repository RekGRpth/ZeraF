.class Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitoringActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/NetworkMonitoringActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "SetupWizard"

    const-string v2, "ACTION_NETWORK_SET_TIME"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    # invokes: Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->access$000(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_3

    const-string v1, "SetupWizard"

    const-string v2, "ACTION_NETWORK_SET_TIMEZONE"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    # invokes: Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeZoneChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->access$100(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    invoke-virtual {v1, p2}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onSimStateChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_6

    const-string v1, "SetupWizard"

    const-string v2, "ACTION_TIME_CHANGED"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    # invokes: Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->access$000(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V

    goto :goto_0

    :cond_7
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_8

    const-string v1, "SetupWizard"

    const-string v2, "ACTION_TIMEZONE_CHANGED"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;->this$0:Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    # invokes: Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeZoneChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->access$100(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V

    goto :goto_0
.end method
