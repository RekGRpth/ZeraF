.class Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;
.super Landroid/os/AsyncTask;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WalledGardenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GservicesWaiterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/WalledGardenActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p2    # Lcom/google/android/setupwizard/WalledGardenActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-static {v1}, Lcom/google/android/setupwizard/GservicesChangedReceiver;->hasGservicesChanged(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v1}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$000(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    const-wide/32 v2, 0xea60

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SetupWizard"

    const-string v2, "Timed out waiting for check in"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SetupWizard"

    const-string v2, "Interrupted while waiting for check in"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1    # Ljava/lang/Void;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/WalledGardenActivity;->finish()V

    return-void
.end method
