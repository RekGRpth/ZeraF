.class public Lcom/google/android/setupwizard/LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;,
        Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;
    }
.end annotation


# static fields
.field private static final ANDROID_POLICIES:[Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;


# instance fields
.field private mParent:Lcom/google/android/setupwizard/BaseActivity;

.field private mPolicy:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->GOOGLE_PLUS_PRIVACY_POLICY:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->ANDROID_PRIVACY_POLICY:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->GOOGLE_PLAY_TOS_POLICY:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->CHROME_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->CHROME_PRIVACY_POLICY:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/setupwizard/LinkSpan;->ANDROID_POLICIES:[Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/setupwizard/BaseActivity;Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/BaseActivity;
    .param p2    # Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lcom/google/android/setupwizard/LinkSpan;->mParent:Lcom/google/android/setupwizard/BaseActivity;

    iput-object p2, p0, Lcom/google/android/setupwizard/LinkSpan;->mPolicy:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    return-void
.end method

.method static linkify(Lcom/google/android/setupwizard/BaseActivity;I)Ljava/lang/CharSequence;
    .locals 13

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v1, Landroid/text/Annotation;

    invoke-virtual {v4, v2, v0, v1}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    array-length v6, v0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v0, v3

    const-string v1, "id"

    invoke-virtual {v7}, Landroid/text/Annotation;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v7}, Landroid/text/Annotation;->getValue()Ljava/lang/String;

    move-result-object v8

    move v1, v2

    :goto_1
    :try_start_0
    sget-object v9, Lcom/google/android/setupwizard/LinkSpan;->ANDROID_POLICIES:[Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    array-length v9, v9

    if-ge v1, v9, :cond_1

    sget-object v9, Lcom/google/android/setupwizard/LinkSpan;->ANDROID_POLICIES:[Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aget-object v9, v9, v1

    invoke-virtual {v9}, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->getTag()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v4, v7}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    invoke-virtual {v4, v7}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    new-instance v11, Lcom/google/android/setupwizard/LinkSpan;

    sget-object v12, Lcom/google/android/setupwizard/LinkSpan;->ANDROID_POLICIES:[Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    aget-object v12, v12, v1

    invoke-direct {v11, p0, v12}, Lcom/google/android/setupwizard/LinkSpan;-><init>(Lcom/google/android/setupwizard/BaseActivity;Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V

    invoke-virtual {v4, v11}, Landroid/text/SpannableString;->getSpanFlags(Ljava/lang/Object;)I

    move-result v12

    invoke-virtual {v5, v11, v9, v10, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const-string v1, "SetupWizard"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No such policy while creating link, id=\'"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "\'"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "SetupWizard"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to convert value \'"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' to a number"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    return-object v5
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/LinkSpan;->mParent:Lcom/google/android/setupwizard/BaseActivity;

    iget-object v1, p0, Lcom/google/android/setupwizard/LinkSpan;->mPolicy:Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->showAgreement(Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V

    return-void
.end method
