.class Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "NoAccountTosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/NoAccountTosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LinkSpan"
.end annotation


# instance fields
.field private mParent:Lcom/google/android/setupwizard/NoAccountTosActivity;

.field private mPolicy:Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/NoAccountTosActivity;Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/NoAccountTosActivity;
    .param p2    # Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;->mParent:Lcom/google/android/setupwizard/NoAccountTosActivity;

    iput-object p2, p0, Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;->mPolicy:Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/NoAccountTosActivity;Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;Lcom/google/android/setupwizard/NoAccountTosActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/NoAccountTosActivity;
    .param p2    # Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;
    .param p3    # Lcom/google/android/setupwizard/NoAccountTosActivity$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;-><init>(Lcom/google/android/setupwizard/NoAccountTosActivity;Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v0, Lcom/google/android/setupwizard/NoAccountTosActivity$WebViewDialog;

    iget-object v1, p0, Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;->mParent:Lcom/google/android/setupwizard/NoAccountTosActivity;

    iget-object v2, p0, Lcom/google/android/setupwizard/NoAccountTosActivity$LinkSpan;->mPolicy:Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;

    invoke-direct {v0, v1, v2}, Lcom/google/android/setupwizard/NoAccountTosActivity$WebViewDialog;-><init>(Lcom/google/android/setupwizard/NoAccountTosActivity;Lcom/google/android/setupwizard/NoAccountTosActivity$AndroidPolicy;)V

    invoke-virtual {v0}, Lcom/google/android/setupwizard/NoAccountTosActivity$WebViewDialog;->show()V

    return-void
.end method
