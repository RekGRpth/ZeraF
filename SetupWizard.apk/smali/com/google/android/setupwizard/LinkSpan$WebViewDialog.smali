.class Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;
.super Landroid/app/AlertDialog;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/LinkSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WebViewDialog"
.end annotation


# instance fields
.field private mHelper:Lcom/google/android/common/GoogleWebContentHelper;


# direct methods
.method protected constructor <init>(Lcom/google/android/setupwizard/BaseActivity;Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V
    .locals 4
    .param p1    # Lcom/google/android/setupwizard/BaseActivity;
    .param p2    # Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/google/android/common/GoogleWebContentHelper;

    invoke-direct {v1, p1}, Lcom/google/android/common/GoogleWebContentHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    iget-object v1, p0, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {p2, v0, p1}, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->getSecureUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, p1}, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->getPrettyUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/common/GoogleWebContentHelper;->setUrls(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v2

    invoke-static {}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->isWifiOnlyBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f07005d

    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/android/setupwizard/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/common/GoogleWebContentHelper;->setUnsuccessfulMessage(Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/common/GoogleWebContentHelper;->loadUrl()Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {p2}, Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;->getTitleResId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->setTitle(I)V

    const/4 v1, -0x1

    const v2, 0x7f070012

    invoke-virtual {p1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog$1;

    invoke-direct {v3, p0, p1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog$1;-><init>(Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;Lcom/google/android/setupwizard/BaseActivity;)V

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v1, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog$2;-><init>(Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;Lcom/google/android/setupwizard/BaseActivity;)V

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v1}, Lcom/google/android/common/GoogleWebContentHelper;->getLayout()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->setView(Landroid/view/View;)V

    return-void

    :cond_0
    const v1, 0x7f070014

    goto :goto_0
.end method

.method private static isWifiOnlyBuild()Z
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "get"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const-string v2, "wifi-only"

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "ro.carrier"

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/common/GoogleWebContentHelper;->handleKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
