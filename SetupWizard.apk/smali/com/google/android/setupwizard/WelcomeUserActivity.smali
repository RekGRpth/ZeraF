.class public Lcom/google/android/setupwizard/WelcomeUserActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WelcomeUserActivity.java"


# instance fields
.field private mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "SetupWizard"

    const-string v1, "WelcomeUserActivity.onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeUserActivity;->setContentView(I)V

    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/WelcomeUserActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v0, 0x7f0d0010

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SetupWizard"

    const-string v1, "WelcomeUserActivity.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->onDestroy()V

    return-void
.end method

.method protected start()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeUserActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeUserActivity;->finish()V

    return-void
.end method
