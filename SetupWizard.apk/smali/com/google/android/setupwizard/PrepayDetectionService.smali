.class public Lcom/google/android/setupwizard/PrepayDetectionService;
.super Landroid/app/Service;
.source "PrepayDetectionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;
    }
.end annotation


# instance fields
.field private mDetectionThread:Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "SetupWizard"

    const-string v1, "PrepayDetectionService.onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;

    invoke-direct {v0, p0, p0}, Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;-><init>(Lcom/google/android/setupwizard/PrepayDetectionService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/PrepayDetectionService;->mDetectionThread:Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;

    iget-object v0, p0, Lcom/google/android/setupwizard/PrepayDetectionService;->mDetectionThread:Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/PrepayDetectionService$PrepayDetectionThread;->start()V

    const/4 v0, 0x2

    return v0
.end method
