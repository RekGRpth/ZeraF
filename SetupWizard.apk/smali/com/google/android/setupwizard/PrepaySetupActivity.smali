.class public Lcom/google/android/setupwizard/PrepaySetupActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "PrepaySetupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAgreeButton:Landroid/view/View;

.field private mDisagreeButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 1

    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->setContentView(I)V

    const v0, 0x7f0d0022

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mAgreeButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mAgreeButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0d0023

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mDisagreeButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mDisagreeButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mAgreeButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/PrepaySetupActivity;->mDisagreeButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->finish()V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/PrepaySetupActivity;->initView()V

    return-void
.end method
