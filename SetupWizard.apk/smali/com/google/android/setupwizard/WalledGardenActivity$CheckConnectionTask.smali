.class Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;
.super Landroid/os/AsyncTask;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WalledGardenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckConnectionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/WalledGardenActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p2    # Lcom/google/android/setupwizard/WalledGardenActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V

    return-void
.end method

.method private declared-synchronized isWalledGardenConnection(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    # setter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v4, v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$702(Lcom/google/android/setupwizard/WalledGardenActivity;Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    const/16 v4, 0xcc

    if-eq v3, v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_3
    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :catchall_1
    move-exception v3

    :try_start_4
    iget-object v4, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v4}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;
    invoke-static {v4}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->isWalledGardenConnection(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    # invokes: Lcom/google/android/setupwizard/WalledGardenActivity;->onWalledGarden(Z)V
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$800(Lcom/google/android/setupwizard/WalledGardenActivity;Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
