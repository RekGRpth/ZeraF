.class public Lcom/google/android/setupwizard/NetworkMonitoringActivity;
.super Landroid/app/Activity;
.source "NetworkMonitoringActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/NetworkMonitor$Callback;


# static fields
.field protected static final LOCAL_LOGV:Z


# instance fields
.field private mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mSimState:Lcom/android/internal/telephony/IccCardConstants$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "SetupWizard"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    iput-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    new-instance v0, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity$1;-><init>(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/NetworkMonitoringActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/NetworkMonitoringActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->timeZoneChanged()V

    return-void
.end method

.method private timeChanged()V
    .locals 3

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "setupwizard.time_is_set"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private timeZoneChanged()V
    .locals 3

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "setupwizard.timezone_is_set"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public isTimeSet()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "setupwizard.time_is_set"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_2

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isTimeSet() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v0
.end method

.method public isTimezoneSet()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "setupwizard.timezone_is_set"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_1

    const-string v1, "SetupWizard"

    const-string v2, "isTimezoneSet() detects non-zero offset"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-boolean v1, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_2

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isTimezoneSet() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    sget-boolean v3, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "SetupWizard"

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v3, Lcom/google/android/setupwizard/NetworkMonitor;

    invoke-direct {v3, p0, p0}, Lcom/google/android/setupwizard/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/setupwizard/NetworkMonitor$Callback;)V

    iput-object v3, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.NETWORK_SET_TIME"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->isTimezoneSet()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "google_setup:ip_timezone"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v3, :cond_1

    const-string v3, "SetupWizard"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "timeZoneId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v2, :cond_2

    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v3, "SetupWizard"

    const-string v4, "failed to get alarm manager to set the system timezone"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/NetworkMonitor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mNetworkMonitor:Lcom/google/android/setupwizard/NetworkMonitor;

    :cond_1
    return-void
.end method

.method public onGlsConnected(Lcom/google/android/gsf/IGoogleLoginService;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/IGoogleLoginService;

    return-void
.end method

.method public onNetworkConnected()V
    .locals 0

    return-void
.end method

.method public onNetworkDisconnected()V
    .locals 0

    return-void
.end method

.method protected onSimStateChanged(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v1, "ss"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ABSENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    iput-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    iput-object v1, p0, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    goto :goto_0
.end method
