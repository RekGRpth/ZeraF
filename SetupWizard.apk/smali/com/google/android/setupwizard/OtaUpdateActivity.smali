.class public Lcom/google/android/setupwizard/OtaUpdateActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "OtaUpdateActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-string v3, "SetupWizard"

    const-string v4, "OtaUpdateActivity.onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_url"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_required_setup"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "SetupWizard"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "    isSecondaryUser="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->isSecondaryUser()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " updateUrl="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " updateRequiredSetup="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->isSecondaryUser()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v3, "SetupWizard"

    const-string v4, "    No OTA update required"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x7

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const v3, 0x7f030006

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->setContentView(I)V

    const v3, 0x7f0d000d

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/google/android/setupwizard/OtaUpdateActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v3, 0x7f0d0010

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->setBackButton(Landroid/view/View;)V

    const v3, 0x7f0d0020

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/OtaUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f070053

    invoke-static {p0, v3}, Lcom/google/android/setupwizard/LinkSpan;->linkify(Lcom/google/android/setupwizard/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method protected start()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/OtaUpdateActivity;->finish()V

    return-void
.end method
