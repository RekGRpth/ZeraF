.class public Lcom/google/android/setupwizard/BottomScrollView;
.super Landroid/widget/ScrollView;
.source "BottomScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;
    }
.end annotation


# instance fields
.field private mActive:Z

.field private mFirst:Z

.field private mLastHeight:I

.field private mListener:Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

.field private mScrollThreshold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mFirst:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mFirst:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mFirst:Z

    return-void
.end method

.method private checkScroll()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mListener:Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BottomScrollView;->getScrollY()I

    move-result v0

    iget v1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mScrollThreshold:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mListener:Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

    invoke-interface {v0}, Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;->onScrolledToBottom()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mFirst:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mFirst:Z

    iget-object v0, p0, Lcom/google/android/setupwizard/BottomScrollView;->mListener:Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

    invoke-interface {v0}, Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;->onRequiresScroll()V

    goto :goto_0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    sub-int v1, p5, p3

    iget v2, p0, Lcom/google/android/setupwizard/BottomScrollView;->mLastHeight:I

    if-ge v1, v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mActive:Z

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v1, p5

    add-int/2addr v1, p3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BottomScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mScrollThreshold:I

    :cond_1
    sub-int v1, p5, p3

    if-lez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BottomScrollView;->checkScroll()V

    :cond_2
    sub-int v1, p5, p3

    iput v1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mLastHeight:I

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    if-eq p4, p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BottomScrollView;->checkScroll()V

    :cond_0
    return-void
.end method

.method public setBottomScrollListener(Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

    iput-object p1, p0, Lcom/google/android/setupwizard/BottomScrollView;->mListener:Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;

    return-void
.end method
