.class Lcom/google/android/setupwizard/WelcomeActivity$4;
.super Ljava/lang/Object;
.source "WelcomeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/WelcomeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/WelcomeActivity$4;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity$4;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/setupwizard/WelcomeActivity;->access$700(Lcom/google/android/setupwizard/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity$4;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity$4;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;
    invoke-static {v1}, Lcom/google/android/setupwizard/WelcomeActivity;->access$700(Lcom/google/android/setupwizard/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v1

    # setter for: Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/WelcomeActivity;->access$802(Lcom/google/android/setupwizard/WelcomeActivity;Ljava/util/Locale;)Ljava/util/Locale;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity$4;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/setupwizard/WelcomeActivity;->access$700(Lcom/google/android/setupwizard/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    :cond_0
    return-void
.end method
