.class public Lcom/mediatek/vcalendar/component/VAlarm;
.super Lcom/mediatek/vcalendar/component/Component;
.source "VAlarm.java"


# static fields
.field public static final REMINDER:Ljava/lang/String; = "Reminder"

.field private static final TAG:Ljava/lang/String; = "VAlarm"


# direct methods
.method public constructor <init>(Lcom/mediatek/vcalendar/component/Component;)V
    .locals 2
    .param p1    # Lcom/mediatek/vcalendar/component/Component;

    const-string v0, "VALARM"

    invoke-direct {p0, v0, p1}, Lcom/mediatek/vcalendar/component/Component;-><init>(Ljava/lang/String;Lcom/mediatek/vcalendar/component/Component;)V

    const-string v0, "VAlarm"

    const-string v1, "Constructor: VALARM Component created."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public parseCursorInfo(Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->parseCursorInfo(Landroid/database/Cursor;)V

    const-string v2, "method"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/vcalendar/property/Action;->getActionString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/mediatek/vcalendar/property/Action;

    invoke-direct {v2, v1}, Lcom/mediatek/vcalendar/property/Action;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/Component;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_0
    const-string v2, "minutes"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    mul-int/lit8 v0, v2, -0x1

    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/valuetype/DDuration;->getDurationString(J)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/mediatek/vcalendar/property/Trigger;

    invoke-direct {v2, v1}, Lcom/mediatek/vcalendar/property/Trigger;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/Component;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    :cond_1
    new-instance v2, Lcom/mediatek/vcalendar/property/Description;

    const-string v3, "Reminder"

    invoke-direct {v2, v3}, Lcom/mediatek/vcalendar/property/Description;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/Component;->addProperty(Lcom/mediatek/vcalendar/property/Property;)V

    return-void
.end method

.method public toAlarmsContentValue(Ljava/util/LinkedList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v4, "VAlarm"

    const-string v5, "toAlarmsContentValue: started."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/vcalendar/component/Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/mediatek/vcalendar/component/Component;->getFirstProperty(Ljava/lang/String;)Lcom/mediatek/vcalendar/property/Property;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mediatek/vcalendar/property/Property;->toAlarmsContentValue(Landroid/content/ContentValues;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public toEventsContentValue(Landroid/content/ContentValues;)V
    .locals 2
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v0, "VAlarm"

    const-string v1, "toEventsContentValue: started."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/vcalendar/component/Component;->toEventsContentValue(Landroid/content/ContentValues;)V

    const-string v0, "hasAlarm"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hasAlarm"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method
