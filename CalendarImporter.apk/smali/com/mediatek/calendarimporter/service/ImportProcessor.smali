.class public Lcom/mediatek/calendarimporter/service/ImportProcessor;
.super Lcom/mediatek/calendarimporter/service/BaseProcessor;
.source "ImportProcessor.java"

# interfaces
.implements Lcom/mediatek/vcalendar/VCalStatusChangeOperator;


# static fields
.field static final TAG:Ljava/lang/String; = "ImportProcessor"


# instance fields
.field private mIsBytesMode:Z

.field private mParser:Lcom/mediatek/vcalendar/VCalParser;

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/service/BaseProcessor;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mIsBytesMode:Z

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    const-string v0, "ImportProcessor"

    const-string v1, "Constructor: the given vcsContent is empty."

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    :cond_0
    iput-object p3, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mUiHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mIsBytesMode:Z

    new-instance v0, Lcom/mediatek/vcalendar/VCalParser;

    invoke-direct {v0, p2, p1, p0}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Handler;
    .param p4    # Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mediatek/calendarimporter/service/BaseProcessor;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mIsBytesMode:Z

    iput-object p3, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mUiHandler:Landroid/os/Handler;

    invoke-static {p2}, Lcom/mediatek/calendarimporter/utils/StringUtils;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/vcalendar/VCalParser;

    invoke-direct {v0, p4, p1, p0}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    :goto_0
    return-void

    :cond_0
    const-string v0, "ImportProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The dst accountName :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/vcalendar/VCalParser;

    invoke-direct {v0, p4, p2, p1, p0}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    iput-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    goto :goto_0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalParser;->cancelCurrentParse()V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalParser;->close()V

    :cond_0
    invoke-super {p0, p1}, Lcom/mediatek/calendarimporter/service/BaseProcessor;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/calendarimporter/service/BaseProcessor;->run()V

    iget-boolean v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mIsBytesMode:Z

    if-eqz v0, :cond_0

    const-string v0, "ImportProcessor"

    const-string v1, "run: startParseVcsContent()"

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalParser;->startParseVcsContent()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ImportProcessor"

    const-string v1, "run: mParser.startParse()"

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalParser;->startParse()V

    goto :goto_0
.end method

.method public vCalOperationCanceled(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "ImportProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vCalOperationCanceled,finishedCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",totalCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalParser;->close()V

    return-void
.end method

.method public vCalOperationExceptionOccured(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v1, "ImportProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "vCalOperationExceptionOccured,finishedCnt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",totalCnt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/calendarimporter/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/VCalParser;->close()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public vCalOperationFinished(IILjava/lang/Object;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    const-string v1, "ImportProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "vCalOperationFinished: successCnt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",totalCnt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/calendarimporter/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mParser:Lcom/mediatek/vcalendar/VCalParser;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/VCalParser;->close()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p2, v0, Landroid/os/Message;->arg2:I

    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public vCalOperationStarted(I)V
    .locals 3
    .param p1    # I

    const-string v0, "ImportProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vCalProcessStatusUpdate: totalCnt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendarimporter/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public vCalProcessStatusUpdate(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p2, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/mediatek/calendarimporter/service/ImportProcessor;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
