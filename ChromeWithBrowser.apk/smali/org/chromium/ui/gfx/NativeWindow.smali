.class public abstract Lorg/chromium/ui/gfx/NativeWindow;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "ui"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNativeWindowAndroid:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    iput v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    iput-object p1, p0, Lorg/chromium/ui/gfx/NativeWindow;->mContext:Landroid/content/Context;

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeInit()I
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    invoke-direct {p0, v0}, Lorg/chromium/ui/gfx/NativeWindow;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getNativePointer()I
    .locals 1

    iget v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/ui/gfx/NativeWindow;->nativeInit()I

    move-result v0

    iput v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    :cond_0
    iget v0, p0, Lorg/chromium/ui/gfx/NativeWindow;->mNativeWindowAndroid:I

    return v0
.end method

.method public abstract sendBroadcast(Landroid/content/Intent;)V
.end method

.method public abstract showError(Ljava/lang/String;)V
.end method

.method public abstract showIntent(Landroid/content/Intent;Lorg/chromium/ui/gfx/NativeWindow$IntentCallback;Ljava/lang/String;)Z
.end method
