.class Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;
.super Landroid/view/View;


# instance fields
.field private mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

.field private mCurrentOrientation:I


# direct methods
.method constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/autofill/AutofillPopup;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mCurrentOrientation:I

    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView$1;-><init>(Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;)V

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;)Lorg/chromium/chrome/browser/autofill/AutofillPopup;
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    return-object v0
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mCurrentOrientation:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->dismiss()V

    :cond_0
    return-void
.end method

.method public setSize(Landroid/graphics/Rect;I)V
    .locals 5

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v0, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget v3, v2, Landroid/graphics/Point;->x:I

    if-le v0, v3, :cond_1

    iget v0, v2, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    :goto_0
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->mAutofillPopup:Lorg/chromium/chrome/browser/autofill/AutofillPopup;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/autofill/AutofillPopup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :cond_0
    iget v2, v3, Landroid/graphics/Rect;->left:I

    iget v4, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v2}, Lorg/chromium/chrome/browser/autofill/AutofillPopup$AnchorView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
