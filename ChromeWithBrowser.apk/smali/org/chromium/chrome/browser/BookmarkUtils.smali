.class public Lorg/chromium/chrome/browser/BookmarkUtils;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEFAULT_RGB_VALUE:I = 0x91

.field private static final INSTALL_SHORTCUT:Ljava/lang/String; = "com.android.launcher.action.INSTALL_SHORTCUT"

.field public static final REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB:Ljava/lang/String; = "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

.field private static final SDK_VERSION_FOR_ACCESS_TO_METHODS:I = 0xf

.field private static final TAG:Ljava/lang/String; = "BookmarkUtils"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/chrome/browser/BookmarkUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/BookmarkUtils;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAddToHomeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;III)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/BookmarkUtils;->createShortcutIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-static {p0, p3, p4, p5, p6}, Lorg/chromium/chrome/browser/BookmarkUtils;->createIcon(Landroid/content/Context;Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private static createIcon(Landroid/content/Context;Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 11

    const/4 v7, 0x0

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v1

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    move-result v0

    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :try_start_1
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v3, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v3, v1, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    if-nez p1, :cond_1

    sget v1, Lorg/chromium/chrome/R$drawable;->globe_favicon:I

    invoke-static {p0, v1, v0}, Lorg/chromium/chrome/browser/BookmarkUtils;->getBitmapFromResourceId(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v1

    const/16 v6, 0x91

    move v5, v6

    move v4, v6

    :goto_0
    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/BookmarkUtils;->getIconBackground(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v8, 0x0

    new-instance v9, Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v2, v0, v8, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lorg/chromium/chrome/browser/BookmarkUtils;->drawFaviconToCanvas(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Canvas;Landroid/graphics/Rect;III)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v7

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v7

    :goto_2
    const-string v1, "BookmarkUtils"

    const-string v2, "OutOfMemoryError while trying to draw bitmap on canvas."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v0, v7

    goto :goto_2

    :cond_1
    move v6, p4

    move v5, p3

    move v4, p2

    move-object v1, p1

    goto :goto_0
.end method

.method private static createShortcutIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private static drawFaviconToCanvas(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Canvas;Landroid/graphics/Rect;III)V
    .locals 20

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lorg/chromium/chrome/R$dimen;->favicon_colorstrip_width:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lorg/chromium/chrome/R$dimen;->favicon_colorstrip_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lorg/chromium/chrome/R$dimen;->favicon_colorstrip_padding:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lorg/chromium/chrome/R$dimen;->favicon_colorstrip_corner_radii:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lorg/chromium/chrome/R$dimen;->favicon_fold_size:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lorg/chromium/chrome/R$dimen;->favicon_fold_corner_radii:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lorg/chromium/chrome/R$dimen;->favicon_fold_border:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lorg/chromium/chrome/R$dimen;->favicon_fold_shadow:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lorg/chromium/chrome/R$dimen;->favicon_size:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v7

    int-to-float v9, v3

    const/high16 v14, 0x40000000

    div-float/2addr v9, v14

    sub-float/2addr v7, v9

    int-to-float v3, v3

    add-float v14, v7, v3

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v3, v5

    int-to-float v3, v3

    int-to-float v4, v4

    sub-float v4, v3, v4

    const/16 v9, 0xff

    move/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v9, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    new-instance v15, Landroid/graphics/Paint;

    const/16 v16, 0x1

    invoke-direct/range {v15 .. v16}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v15, v9}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v16, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v16, Landroid/graphics/Path;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v4}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v4}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v0, v6

    move/from16 v17, v0

    sub-float v17, v3, v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v17, Landroid/graphics/RectF;

    int-to-float v0, v6

    move/from16 v18, v0

    sub-float v18, v14, v18

    int-to-float v0, v6

    move/from16 v19, v0

    sub-float v19, v3, v19

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v14, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v18, 0x0

    const/high16 v19, 0x42b40000

    invoke-virtual/range {v16 .. v19}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    int-to-float v0, v6

    move/from16 v17, v0

    add-float v17, v17, v7

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v17, Landroid/graphics/RectF;

    int-to-float v0, v6

    move/from16 v18, v0

    sub-float v18, v3, v18

    int-to-float v6, v6

    add-float/2addr v6, v7

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v7, v1, v6, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v3, 0x42b40000

    const/high16 v6, 0x42b40000

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v4}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v15}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    int-to-float v3, v10

    sub-float v6, v14, v3

    int-to-float v15, v5

    add-int v3, v5, v10

    int-to-float v7, v3

    const/16 v3, 0x19

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v3, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    new-instance v16, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    const/4 v4, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    int-to-float v8, v8

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5, v8, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    const/16 v3, 0x99

    move/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    new-instance v17, Landroid/graphics/Paint;

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    new-instance v3, Landroid/graphics/LinearGradient;

    int-to-float v4, v10

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float v4, v14, v4

    int-to-float v5, v10

    const/high16 v10, 0x40000000

    div-float/2addr v5, v10

    add-float/2addr v5, v15

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v3 .. v10}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v3, v6, v15}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v14, v7}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v4, v11

    add-float/2addr v4, v6

    invoke-virtual {v3, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v11

    sub-float v5, v7, v5

    int-to-float v8, v11

    add-float/2addr v8, v6

    invoke-direct {v4, v6, v5, v8, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v5, 0x42b40000

    const/high16 v7, 0x42b40000

    invoke-virtual {v3, v4, v5, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    invoke-virtual {v3, v6, v15}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    const/16 v4, 0xcc

    move/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v4, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v4, v12

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    const/4 v3, 0x1

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v13, v13, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v3

    const-string v3, "BookmarkUtils"

    const-string v4, "OutOfMemoryError while trying to draw bitmap on canvas."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getBitmapFromResourceId(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarkUtils;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The drawable was not a bitmap drawable as expected"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static getIconBackground(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 1

    sget v0, Lorg/chromium/chrome/R$mipmap;->homescreen_bg:I

    invoke-static {p0, v0, p1}, Lorg/chromium/chrome/browser/BookmarkUtils;->getBitmapFromResourceId(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
