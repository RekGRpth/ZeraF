.class Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;
.super Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/TabBase;


# direct methods
.method private constructor <init>(Lorg/chromium/chrome/browser/TabBase;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/chrome/browser/TabBase;Lorg/chromium/chrome/browser/TabBase$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;-><init>(Lorg/chromium/chrome/browser/TabBase;)V

    return-void
.end method


# virtual methods
.method public onLoadProgressChanged(I)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    # getter for: Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;
    invoke-static {v0}, Lorg/chromium/chrome/browser/TabBase;->access$300(Lorg/chromium/chrome/browser/TabBase;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    # getter for: Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;
    invoke-static {v0}, Lorg/chromium/chrome/browser/TabBase;->access$300(Lorg/chromium/chrome/browser/TabBase;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onLoadProgressChanged(Lorg/chromium/chrome/browser/TabBase;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onLoadStarted()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    const/4 v1, 0x1

    # setter for: Lorg/chromium/chrome/browser/TabBase;->mIsLoading:Z
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/TabBase;->access$402(Lorg/chromium/chrome/browser/TabBase;Z)Z

    return-void
.end method

.method public onLoadStopped()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/TabBase;->mIsLoading:Z
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/TabBase;->access$402(Lorg/chromium/chrome/browser/TabBase;Z)Z

    return-void
.end method

.method public onUpdateUrl(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    # getter for: Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;
    invoke-static {v0}, Lorg/chromium/chrome/browser/TabBase;->access$300(Lorg/chromium/chrome/browser/TabBase;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    # getter for: Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;
    invoke-static {v0}, Lorg/chromium/chrome/browser/TabBase;->access$300(Lorg/chromium/chrome/browser/TabBase;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/TabBase;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onUpdateUrl(Lorg/chromium/chrome/browser/TabBase;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method
