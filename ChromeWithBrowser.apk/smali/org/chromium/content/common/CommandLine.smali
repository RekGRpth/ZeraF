.class public abstract Lorg/chromium/content/common/CommandLine;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ACCESSIBILITY_DEBUG_BRAILLE_SERVICE:Ljava/lang/String; = "debug-braille-service"

.field public static final ACCESSIBILITY_JAVASCRIPT_URL:Ljava/lang/String; = "accessibility-js-url"

.field public static final ADD_OFFICIAL_COMMAND_LINE:Ljava/lang/String; = "add-official-command-line"

.field public static final ENABLE_TEST_INTENTS:Ljava/lang/String; = "enable-test-intents"

.field public static final ENABLE_TOP_CONTROLS_POSITION_CALCULATION:Ljava/lang/String; = "enable-top-controls-position-calculation"

.field public static final LOG_FPS:Ljava/lang/String; = "log-fps"

.field public static final NETWORK_COUNTRY_ISO:Ljava/lang/String; = "network-country-iso"

.field private static final SWITCH_PREFIX:Ljava/lang/String; = "--"

.field private static final SWITCH_TERMINATOR:Ljava/lang/String; = "--"

.field private static final SWITCH_VALUE_SEPARATOR:Ljava/lang/String; = "="

.field public static final TABLET_UI:Ljava/lang/String; = "tablet-ui"

.field private static final TAG:Ljava/lang/String; = "CommandLine"

.field public static final TOP_CONTROLS_HEIGHT:Ljava/lang/String; = "top-controls-height"

.field public static final USE_MOBILE_UA:Ljava/lang/String; = "use-mobile-user-agent"

.field public static final WAIT_FOR_JAVA_DEBUGGER:Ljava/lang/String; = "wait-for-java-debugger"

.field private static final sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/common/CommandLine;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/common/CommandLine;->$assertionsDisabled:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/content/common/CommandLine$1;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/common/CommandLine;-><init>()V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lorg/chromium/content/common/CommandLine;->nativeHasSwitch(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lorg/chromium/content/common/CommandLine;->nativeGetSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lorg/chromium/content/common/CommandLine;->nativeAppendSwitch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lorg/chromium/content/common/CommandLine;->nativeAppendSwitchWithValue(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700([Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lorg/chromium/content/common/CommandLine;->nativeAppendSwitchesAndArguments([Ljava/lang/String;)V

    return-void
.end method

.method public static enableNativeProxy()V
    .locals 3

    sget-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lorg/chromium/content/common/CommandLine$NativeCommandLine;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/chromium/content/common/CommandLine$NativeCommandLine;-><init>(Lorg/chromium/content/common/CommandLine$1;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lorg/chromium/content/common/CommandLine;
    .locals 2

    sget-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/common/CommandLine;

    sget-boolean v1, Lorg/chromium/content/common/CommandLine;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public static getJavaSwitchesOrNull()[Ljava/lang/String;
    .locals 2

    sget-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/common/CommandLine;

    if-eqz v0, :cond_1

    sget-boolean v1, Lorg/chromium/content/common/CommandLine;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/common/CommandLine;->isNativeImplementation()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lorg/chromium/content/common/CommandLine$JavaCommandLine;

    # invokes: Lorg/chromium/content/common/CommandLine$JavaCommandLine;->getCommandLineArguments()[Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/common/CommandLine$JavaCommandLine;->access$100(Lorg/chromium/content/common/CommandLine$JavaCommandLine;)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static init([Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lorg/chromium/content/common/CommandLine$JavaCommandLine;

    invoke-direct {v0, p0}, Lorg/chromium/content/common/CommandLine$JavaCommandLine;-><init>([Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->setInstance(Lorg/chromium/content/common/CommandLine;)V

    return-void
.end method

.method public static initFromFile(Ljava/lang/String;)V
    .locals 5

    const/4 v0, 0x0

    new-array v0, v0, [C

    const/16 v1, 0x2000

    :try_start_0
    invoke-static {p0, v1}, Lorg/chromium/content/common/CommandLine;->readUtf8FileFully(Ljava/lang/String;I)[C
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->tokenizeQuotedAruments([C)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->init([Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v1

    const-string v2, "CommandLine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error reading command line file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 1

    sget-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeAppendSwitch(Ljava/lang/String;)V
.end method

.method private static native nativeAppendSwitchWithValue(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native nativeAppendSwitchesAndArguments([Ljava/lang/String;)V
.end method

.method private static native nativeGetSwitchValue(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeHasSwitch(Ljava/lang/String;)Z
.end method

.method private static native nativeReset()V
.end method

.method private static readUtf8FileFully(Ljava/lang/String;I)[C
    .locals 8

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    int-to-long v6, p1

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "File "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " length "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " exceeds limit "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    :cond_0
    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [C

    new-instance v2, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v3, "UTF-8"

    invoke-direct {v2, v4, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I

    move-result v1

    sget-boolean v3, Lorg/chromium/content/common/CommandLine;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/io/Reader;->ready()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0

    :cond_2
    array-length v3, v0

    if-ge v1, v3, :cond_3

    const/4 v3, 0x0

    invoke-static {v0, v3, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    :cond_3
    invoke-virtual {v2}, Ljava/io/Reader;->close()V

    return-object v0
.end method

.method public static reset()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->setInstance(Lorg/chromium/content/common/CommandLine;)V

    return-void
.end method

.method private static setInstance(Lorg/chromium/content/common/CommandLine;)V
    .locals 1

    sget-object v0, Lorg/chromium/content/common/CommandLine;->sCommandLine:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/common/CommandLine;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/common/CommandLine;->isNativeImplementation()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->nativeReset()V

    :cond_0
    return-void
.end method

.method public static tokenizeQuotedAruments([C)[Ljava/lang/String;
    .locals 11

    const/4 v2, 0x0

    const/4 v4, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    array-length v7, p0

    move v5, v4

    move v1, v4

    move-object v0, v2

    :goto_0
    if-ge v5, v7, :cond_8

    aget-char v3, p0, v5

    if-nez v1, :cond_0

    const/16 v8, 0x27

    if-eq v3, v8, :cond_1

    const/16 v8, 0x22

    if-eq v3, v8, :cond_1

    :cond_0
    if-ne v3, v1, :cond_4

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    const/16 v9, 0x5c

    if-ne v8, v9, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v0, v8, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    move v10, v1

    move-object v1, v0

    move v0, v10

    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v10, v0

    move-object v0, v1

    move v1, v10

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    move v1, v3

    :goto_2
    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_5
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_6
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_1

    :cond_8
    if-eqz v0, :cond_a

    if-eqz v1, :cond_9

    const-string v1, "CommandLine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unterminated quoted string: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public abstract appendSwitch(Ljava/lang/String;)V
.end method

.method public abstract appendSwitchWithValue(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract appendSwitchesAndArguments([Ljava/lang/String;)V
.end method

.method public abstract getSwitchValue(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public getSwitchValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0, p1}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public abstract hasSwitch(Ljava/lang/String;)Z
.end method

.method public isNativeImplementation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
