.class final Lorg/chromium/content/common/CleanupReference$2;
.super Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/chromium/content/common/CleanupReference;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    # getter for: Lorg/chromium/content/common/CleanupReference;->sQueue:Ljava/lang/ref/ReferenceQueue;
    invoke-static {}, Lorg/chromium/content/common/CleanupReference;->access$400()Ljava/lang/ref/ReferenceQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/common/CleanupReference;

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void

    :pswitch_0
    # getter for: Lorg/chromium/content/common/CleanupReference;->sRefs:Ljava/util/Set;
    invoke-static {}, Lorg/chromium/content/common/CleanupReference;->access$200()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    :pswitch_1
    # invokes: Lorg/chromium/content/common/CleanupReference;->runCleanupTaskInternal()V
    invoke-static {v0}, Lorg/chromium/content/common/CleanupReference;->access$300(Lorg/chromium/content/common/CleanupReference;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
