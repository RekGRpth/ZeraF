.class Lorg/chromium/content/browser/ZoomManager;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String; = "ContentViewZoom"


# instance fields
.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mMultiTouchDetector:Landroid/view/ScaleGestureDetector;

.field private mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

.field private mZoomButtonsController:Landroid/widget/ZoomButtonsController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ZoomManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    new-instance v0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;-><init>(Lorg/chromium/content/browser/ZoomManager;Lorg/chromium/content/browser/ZoomManager$1;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchDetector:Landroid/view/ScaleGestureDetector;

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ZoomManager;)Landroid/widget/ZoomButtonsController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    return-object v0
.end method

.method private getZoomControls()Landroid/widget/ZoomButtonsController;
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentSettings()Lorg/chromium/content/browser/ContentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->shouldDisplayZoomControls()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/ZoomButtonsController;

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ZoomButtonsController;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    new-instance v1, Lorg/chromium/content/browser/ZoomManager$ZoomListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/chromium/content/browser/ZoomManager$ZoomListener;-><init>(Lorg/chromium/content/browser/ZoomManager;Lorg/chromium/content/browser/ZoomManager$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    return-object v0
.end method


# virtual methods
.method dismissZoomPicker()V
    .locals 2

    invoke-direct {p0}, Lorg/chromium/content/browser/ZoomManager;->getZoomControls()Landroid/widget/ZoomButtonsController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method getZoomControlsViewForTest()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method invokeZoomPicker()V
    .locals 2

    invoke-direct {p0}, Lorg/chromium/content/browser/ZoomManager;->getZoomControls()Landroid/widget/ZoomButtonsController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method isMultiTouchZoomSupported()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->getPermanentlyIgnoreDetectorEvents()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isScaleGestureDetectionInProgress()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ZoomManager;->isMultiTouchZoomSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method passTouchEventThrough(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->setTemporarilyIgnoreDetectorEvents(Z)V

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ContentViewZoom"

    const-string v2, "ScaleGestureDetector got into a bad state!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-boolean v0, Lorg/chromium/content/browser/ZoomManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method processTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->setTemporarilyIgnoreDetectorEvents(Z)V

    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/ZoomManager;->isScaleGestureDetectionInProgress()Z

    move-result v2

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ContentViewZoom"

    const-string v3, "ScaleGestureDetector got into a bad state!"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-boolean v1, Lorg/chromium/content/browser/ZoomManager;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method updateMultiTouchSupport()V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mMultiTouchListener:Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentSettings()Lorg/chromium/content/browser/ContentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentSettings;->supportsMultiTouchZoom()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->setPermanentlyIgnoreDetectorEvents(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateZoomControls()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->canZoomIn()Z

    move-result v0

    iget-object v1, p0, Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->canZoomOut()Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v2, v0}, Landroid/widget/ZoomButtonsController;->setZoomInEnabled(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    goto :goto_0
.end method
