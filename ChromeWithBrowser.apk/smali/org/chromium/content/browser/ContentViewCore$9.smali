.class Lorg/chromium/content/browser/ContentViewCore$9;
.super Lorg/chromium/content/browser/SelectionHandleController;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$9;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/SelectionHandleController;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public selectBetweenCoordinates(IIII)V
    .locals 6

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$9;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1000(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v0

    if-eqz v0, :cond_1

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$9;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$9;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeContentViewCore:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$1000(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->nativeSelectBetweenCoordinates(IIIII)V
    invoke-static/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewCore;->access$1500(Lorg/chromium/content/browser/ContentViewCore;IIIII)V

    :cond_1
    return-void
.end method

.method public showHandles(II)V
    .locals 1

    invoke-super {p0, p1, p2}, Lorg/chromium/content/browser/SelectionHandleController;->showHandles(II)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$9;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->showSelectActionBar()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1600(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method
