.class Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;
.super Ljava/lang/Object;


# instance fields
.field final mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

.field final mCommandLine:[Ljava/lang/String;

.field final mFilesToBeMapped:[Lorg/chromium/content/browser/FileDescriptorInfo;

.field final mOnConnectionCallback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>([Ljava/lang/String;[Lorg/chromium/content/browser/FileDescriptorInfo;Lorg/chromium/content/common/ISandboxedProcessCallback;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mCommandLine:[Ljava/lang/String;

    iput-object p2, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mFilesToBeMapped:[Lorg/chromium/content/browser/FileDescriptorInfo;

    iput-object p3, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

    iput-object p4, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$ConnectionParams;->mOnConnectionCallback:Ljava/lang/Runnable;

    return-void
.end method
