.class public final enum Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

.field public static final enum NARROW_COLUMNS:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

.field public static final enum NORMAL:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

.field public static final enum SINGLE_COLUMN:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

.field public static final enum TEXT_AUTOSIZING:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NORMAL:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    new-instance v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const-string v1, "SINGLE_COLUMN"

    invoke-direct {v0, v1, v3}, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    new-instance v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const-string v1, "NARROW_COLUMNS"

    invoke-direct {v0, v1, v4}, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    new-instance v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const-string v1, "TEXT_AUTOSIZING"

    invoke-direct {v0, v1, v5}, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const/4 v0, 0x4

    new-array v0, v0, [Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    sget-object v1, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NORMAL:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    aput-object v1, v0, v2

    sget-object v1, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->SINGLE_COLUMN:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    aput-object v1, v0, v3

    sget-object v1, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    aput-object v1, v0, v4

    sget-object v1, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    aput-object v1, v0, v5

    sput-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->$VALUES:[Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    return-object v0
.end method

.method public static values()[Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;
    .locals 1

    sget-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->$VALUES:[Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    invoke-virtual {v0}, [Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    return-object v0
.end method
