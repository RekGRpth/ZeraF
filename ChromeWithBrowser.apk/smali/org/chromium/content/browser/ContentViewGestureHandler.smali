.class Lorg/chromium/content/browser/ContentViewGestureHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DELTA:Ljava/lang/String; = "Delta"

.field static final DISTANCE_X:Ljava/lang/String; = "Distance X"

.field static final DISTANCE_Y:Ljava/lang/String; = "Distance Y"

.field private static final DOUBLE_TAP_TIMEOUT:I

.field static final GESTURE_DOUBLE_TAP:I = 0x1

.field static final GESTURE_FLING_CANCEL:I = 0x9

.field static final GESTURE_FLING_START:I = 0x8

.field static final GESTURE_LONG_PRESS:I = 0x4

.field static final GESTURE_LONG_TAP:I = 0xe

.field static final GESTURE_PINCH_BEGIN:I = 0xa

.field static final GESTURE_PINCH_BY:I = 0xb

.field static final GESTURE_PINCH_END:I = 0xc

.field static final GESTURE_SCROLL_BY:I = 0x6

.field static final GESTURE_SCROLL_END:I = 0x7

.field static final GESTURE_SCROLL_START:I = 0x5

.field static final GESTURE_SHOW_PRESSED_STATE:I = 0x0

.field static final GESTURE_SHOW_PRESS_CANCEL:I = 0xd

.field static final GESTURE_SINGLE_TAP_CONFIRMED:I = 0x3

.field static final GESTURE_SINGLE_TAP_UP:I = 0x2

.field static final INPUT_EVENT_ACK_STATE_CONSUMED:I = 0x1

.field static final INPUT_EVENT_ACK_STATE_NOT_CONSUMED:I = 0x2

.field static final INPUT_EVENT_ACK_STATE_NO_CONSUMER_EXISTS:I = 0x3

.field static final INPUT_EVENT_ACK_STATE_UNKNOWN:I = 0x0

.field static final SHOW_PRESS:Ljava/lang/String; = "ShowPress"

.field private static final TAG:Ljava/lang/String;

.field static final VELOCITY_X:Ljava/lang/String; = "Velocity X"

.field static final VELOCITY_Y:Ljava/lang/String; = "Velocity Y"


# instance fields
.field private mAccumulatedScrollErrorX:F

.field private mAccumulatedScrollErrorY:F

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private final mExtraParamBundle:Landroid/os/Bundle;

.field private mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

.field private mHasTouchHandlers:Z

.field private mIgnoreSingleTap:Z

.field private mJavaScriptIsConsumingGesture:Z

.field private mLastRawX:F

.field private mLastRawY:F

.field private mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

.field private mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

.field private final mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

.field private mNativeScrolling:Z

.field private mNoTouchHandlerForGesture:Z

.field private final mPendingMotionEvents:Ljava/util/Deque;

.field private mPinchInProgress:Z

.field private mScaledTouchSlopSquare:I

.field private mSeenFirstScrollEvent:Z

.field private mShowPressIsCalled:Z

.field private mSingleTapX:I

.field private mSingleTapY:I

.field private mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;

.field private mTouchCancelEventSent:Z

.field private final mZoomManager:Lorg/chromium/content/browser/ZoomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ContentViewGestureHandler;->$assertionsDisabled:Z

    const-class v0, Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/browser/ContentViewGestureHandler;->TAG:Ljava/lang/String;

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/ContentViewGestureHandler;->DOUBLE_TAP_TIMEOUT:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;Lorg/chromium/content/browser/ZoomManager;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mHasTouchHandlers:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNoTouchHandlerForGesture:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mJavaScriptIsConsumingGesture:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPinchInProgress:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mTouchCancelEventSent:Z

    iput v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F

    iput v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F

    iput v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F

    iput v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    new-instance v0, Lorg/chromium/content/browser/LongPressDetector;

    invoke-direct {v0, p1, p0}, Lorg/chromium/content/browser/LongPressDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/LongPressDetector$LongPressDelegate;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    iput-object p2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    iput-object p3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    new-instance v0, Lorg/chromium/content/browser/SnapScrollController;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-direct {v0, v1}, Lorg/chromium/content/browser/SnapScrollController;-><init>(Lorg/chromium/content/browser/ZoomManager;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->initGestureDetectors(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z

    return v0
.end method

.method static synthetic access$002(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z

    return p1
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z

    return v0
.end method

.method static synthetic access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$102(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z

    return p1
.end method

.method static synthetic access$1100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/LongPressDetector;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    return-object v0
.end method

.method static synthetic access$1200()I
    .locals 1

    sget v0, Lorg/chromium/content/browser/ContentViewGestureHandler;->DOUBLE_TAP_TIMEOUT:I

    return v0
.end method

.method static synthetic access$1300(Lorg/chromium/content/browser/ContentViewGestureHandler;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->setClickXAndY(II)V

    return-void
.end method

.method static synthetic access$1400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ZoomManager;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    return-object v0
.end method

.method static synthetic access$1500(Lorg/chromium/content/browser/ContentViewGestureHandler;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mScaledTouchSlopSquare:I

    return v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    return v0
.end method

.method static synthetic access$202(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    return p1
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSeenFirstScrollEvent:Z

    return v0
.end method

.method static synthetic access$302(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSeenFirstScrollEvent:Z

    return p1
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;

    return-object v0
.end method

.method static synthetic access$500(Lorg/chromium/content/browser/ContentViewGestureHandler;)F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F

    return v0
.end method

.method static synthetic access$502(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F
    .locals 0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F

    return p1
.end method

.method static synthetic access$600(Lorg/chromium/content/browser/ContentViewGestureHandler;)F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F

    return v0
.end method

.method static synthetic access$602(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F
    .locals 0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F

    return p1
.end method

.method static synthetic access$700(Lorg/chromium/content/browser/ContentViewGestureHandler;)F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F

    return v0
.end method

.method static synthetic access$702(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F
    .locals 0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F

    return p1
.end method

.method static synthetic access$800(Lorg/chromium/content/browser/ContentViewGestureHandler;)F
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F

    return v0
.end method

.method static synthetic access$802(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F
    .locals 0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F

    return p1
.end method

.method static synthetic access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    return-object v0
.end method

.method private calculateDragAngle(FF)F
    .locals 5

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    float-to-double v3, v0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private drainAllPendingEventsUntilNextDown()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->processTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNoTouchHandlerForGesture:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->trySendNextEventToNative(Landroid/view/MotionEvent;)V

    goto :goto_1
.end method

.method private initGestureDetectors(Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/2addr v0, v0

    iput v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mScaledTouchSlopSquare:I

    :try_start_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    new-instance v0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewGestureHandler$1;-><init>(Lorg/chromium/content/browser/ContentViewGestureHandler;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    new-instance v1, Lorg/chromium/content/browser/third_party/GestureDetector;

    invoke-direct {v1, p1, v0}, Lorg/chromium/content/browser/third_party/GestureDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/third_party/GestureDetector;->setIsLongpressEnabled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    throw v0
.end method

.method private obtainActionCancelMotionEvent()Landroid/view/MotionEvent;
    .locals 8

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v7, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method private offerTouchEventToJavaScript(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/LongPressDetector;->onOfferTouchEventToJavaScript(Landroid/view/MotionEvent;)V

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mHasTouchHandlers:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNoTouchHandlerForGesture:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/LongPressDetector;->confirmOfferMoveEventToJavaScript(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-ne v3, v4, :cond_4

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    new-array v3, v3, [Landroid/view/MotionEvent$PointerCoords;

    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_3

    new-instance v4, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v4}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v4, v3, v1

    aget-object v4, v3, v1

    invoke-virtual {p1, v1, v4}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v1

    invoke-virtual {v0, v4, v5, v3, v1}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendTouchEventToNative(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_0
.end method

.method private processTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-boolean v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    if-eqz v2, :cond_2

    :goto_0
    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v2, p1}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPressIfNeeded(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v2, p1}, Lorg/chromium/content/browser/LongPressDetector;->startLongPressTimerIfNeeded(Landroid/view/MotionEvent;)V

    invoke-virtual {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->canHandle(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

    invoke-virtual {v1, p1}, Lorg/chromium/content/browser/third_party/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    iput-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mCurrentDownEvent:Landroid/view/MotionEvent;

    :cond_0
    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v2, p1}, Lorg/chromium/content/browser/ZoomManager;->processTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v1, v2

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->tellNativeScrollingHasEnded(J)V

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private sendTouchEventToNative(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    new-array v0, v0, [Lorg/chromium/content/browser/TouchPoint;

    invoke-static {p1, v0}, Lorg/chromium/content/browser/TouchPoint;->createTouchPoints(Landroid/view/MotionEvent;[Lorg/chromium/content/browser/TouchPoint;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget-boolean v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPinchInProgress:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5, v2, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendTouchEvent(JI[Lorg/chromium/content/browser/TouchPoint;)Z

    move-result v0

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mTouchCancelEventSent:Z

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mTouchCancelEventSent:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sget v5, Lorg/chromium/content/browser/TouchPoint;->TOUCH_EVENT_TYPE_CANCEL:I

    invoke-interface {v2, v3, v4, v5, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendTouchEvent(JI[Lorg/chromium/content/browser/TouchPoint;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mTouchCancelEventSent:Z

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private setClickXAndY(II)V
    .locals 0

    iput p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSingleTapX:I

    iput p2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSingleTapY:I

    return-void
.end method

.method private tellNativeScrollingHasEnded(J)V
    .locals 7

    const/4 v4, 0x0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/4 v1, 0x7

    const/4 v6, 0x0

    move-wide v2, p1

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    :cond_0
    return-void
.end method

.method private trySendNextEventToNative(Landroid/view/MotionEvent;)V
    .locals 1

    :goto_0
    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendTouchEventToNative(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mJavaScriptIsConsumingGesture:Z

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->processTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    move-object p1, v0

    goto :goto_0
.end method


# virtual methods
.method canHandle(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method cancelLongPress()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPress()V

    return-void
.end method

.method confirmTouchEvent(I)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/ContentViewGestureHandler;->TAG:Ljava/lang/String;

    const-string v1, "confirmTouchEvent with Empty pending list!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MotionEvent;

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPressIfNeeded(Ljava/util/Iterator;)V

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    goto :goto_0

    :pswitch_0
    sget-boolean v1, Lorg/chromium/content/browser/ContentViewGestureHandler;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_1
    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mJavaScriptIsConsumingGesture:Z

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/ZoomManager;->passTouchEventThrough(Landroid/view/MotionEvent;)V

    invoke-direct {p0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->trySendNextEventToNative(Landroid/view/MotionEvent;)V

    goto :goto_1

    :pswitch_2
    iget-boolean v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mJavaScriptIsConsumingGesture:Z

    if-nez v2, :cond_2

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->processTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    invoke-direct {p0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->trySendNextEventToNative(Landroid/view/MotionEvent;)V

    goto :goto_1

    :pswitch_3
    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNoTouchHandlerForGesture:Z

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->processTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->drainAllPendingEventsUntilNextDown()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method endFling(J)V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0x9

    const/4 v6, 0x0

    move-wide v2, p1

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->tellNativeScrollingHasEnded(J)V

    return-void
.end method

.method fling(JIIII)V
    .locals 7

    invoke-virtual {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->endFling(J)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    const-string v1, "Velocity X"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    const-string v1, "Velocity Y"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0x8

    iget-object v6, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    return-void
.end method

.method getLongPressDetector()Lorg/chromium/content/browser/LongPressDetector;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    return-object v0
.end method

.method getNumberOfPendingMotionEvents()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    return v0
.end method

.method public getSingleTapX()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSingleTapX:I

    return v0
.end method

.method public getSingleTapY()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSingleTapY:I

    return v0
.end method

.method hasTouchEventHandlers(Z)V
    .locals 1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mHasTouchHandlers:Z

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mHasTouchHandlers:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    :cond_0
    return-void
.end method

.method isNativePinching()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPinchInProgress:Z

    return v0
.end method

.method isNativeScrolling()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    :try_start_0
    const-string v0, "onTouchEvent"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPressIfNeeded(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/SnapScrollController;->setSnapScrollingMode(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mNoTouchHandlerForGesture:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mJavaScriptIsConsumingGesture:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->endFling(J)V

    :cond_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->offerTouchEventToJavaScript(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "onTouchEvent"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->processTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const-string v1, "onTouchEvent"

    invoke-static {v1}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "onTouchEvent"

    invoke-static {v1}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    throw v0
.end method

.method peekFirstInPendingMotionEvents()Landroid/view/MotionEvent;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPendingMotionEvents:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    return-object v0
.end method

.method pinchBegin(JII)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0xa

    const/4 v6, 0x0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    return-void
.end method

.method pinchBy(JIIF)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    const-string v1, "Delta"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0xb

    iget-object v6, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPinchInProgress:Z

    return-void
.end method

.method pinchEnd(J)V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0xc

    const/4 v6, 0x0

    move-wide v2, p1

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    iput-boolean v4, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mPinchInProgress:Z

    return-void
.end method

.method resetGestureHandlers()V
    .locals 2

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->obtainActionCancelMotionEvent()Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/third_party/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->obtainActionCancelMotionEvent()Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ZoomManager;->processTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/LongPressDetector;->cancelLongPress()V

    return-void
.end method

.method sendShowPressCancelIfNecessary(Landroid/view/MotionEvent;)V
    .locals 7

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0xd

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z

    goto :goto_0
.end method

.method sendShowPressedStateGestureForTest()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method setIgnoreSingleTap(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z

    return-void
.end method

.method setTestDependencies(Lorg/chromium/content/browser/LongPressDetector;Lorg/chromium/content/browser/third_party/GestureDetector;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    iput-object p2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mGestureDetector:Lorg/chromium/content/browser/third_party/GestureDetector;

    iput-object p3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    return-void
.end method

.method triggerLongTapIfNeeded(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;

    invoke-virtual {v0}, Lorg/chromium/content/browser/LongPressDetector;->isInLongPress()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v7, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->isScaleGestureDetectionInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendShowPressCancelIfNecessary(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    const/16 v1, 0xe

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    move v0, v7

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
