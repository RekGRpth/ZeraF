.class Lorg/chromium/content/browser/MonthPicker$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/MonthPicker;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/MonthPicker;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .locals 6

    const/16 v5, 0xb

    const/4 v4, 0x1

    const/4 v3, 0x2

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mCurrentDate:Ljava/util/Calendar;
    invoke-static {v1}, Lorg/chromium/content/browser/MonthPicker;->access$000(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mMonthSpinner:Landroid/widget/NumberPicker;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$200(Lorg/chromium/content/browser/MonthPicker;)Landroid/widget/NumberPicker;

    move-result-object v0

    if-ne p1, v0, :cond_2

    if-ne p2, v5, :cond_0

    if-nez p3, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    iget-object v1, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v1}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v2}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    # invokes: Lorg/chromium/content/browser/MonthPicker;->setDate(II)V
    invoke-static {v0, v1, v2}, Lorg/chromium/content/browser/MonthPicker;->access$400(Lorg/chromium/content/browser/MonthPicker;II)V

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # invokes: Lorg/chromium/content/browser/MonthPicker;->updateSpinners()V
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$500(Lorg/chromium/content/browser/MonthPicker;)V

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # invokes: Lorg/chromium/content/browser/MonthPicker;->notifyDateChanged()V
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$600(Lorg/chromium/content/browser/MonthPicker;)V

    return-void

    :cond_0
    if-nez p2, :cond_1

    if-ne p3, v5, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v0

    sub-int v1, p3, p2

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mYearSpinner:Landroid/widget/NumberPicker;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$300(Lorg/chromium/content/browser/MonthPicker;)Landroid/widget/NumberPicker;

    move-result-object v0

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lorg/chromium/content/browser/MonthPicker$1;->this$0:Lorg/chromium/content/browser/MonthPicker;

    # getter for: Lorg/chromium/content/browser/MonthPicker;->mTempDate:Ljava/util/Calendar;
    invoke-static {v0}, Lorg/chromium/content/browser/MonthPicker;->access$100(Lorg/chromium/content/browser/MonthPicker;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v4, p3}, Ljava/util/Calendar;->set(II)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
