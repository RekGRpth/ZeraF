.class Lorg/chromium/content/browser/ContentViewCore$11;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->copy()Z

    move-result v0

    return v0
.end method

.method public cut()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->cut()Z

    move-result v0

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSelectionEditable()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mSelectionEditable:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2000(Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    return v0
.end method

.method public onDestroyActionMode()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/content/browser/ContentViewCore;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->access$2102(Lorg/chromium/content/browser/ContentViewCore;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mUnselectAllOnActionModeDismiss:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2200(Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->unselect()Z

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewClient;->onContextualActionBarHidden()V

    return-void
.end method

.method public paste()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->paste()Z

    move-result v0

    return v0
.end method

.method public selectAll()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$11;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mImeAdapter:Lorg/chromium/content/browser/ImeAdapter;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$1800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ImeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ImeAdapter;->selectAll()Z

    move-result v0

    return v0
.end method
