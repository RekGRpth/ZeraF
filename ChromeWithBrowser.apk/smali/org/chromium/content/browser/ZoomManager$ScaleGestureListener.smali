.class Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field private mPermanentlyIgnoreDetectorEvents:Z

.field private mPinchEventSent:Z

.field private mTemporarilyIgnoreDetectorEvents:Z

.field final synthetic this$0:Lorg/chromium/content/browser/ZoomManager;


# direct methods
.method private constructor <init>(Lorg/chromium/content/browser/ZoomManager;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPermanentlyIgnoreDetectorEvents:Z

    iput-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mTemporarilyIgnoreDetectorEvents:Z

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/content/browser/ZoomManager;Lorg/chromium/content/browser/ZoomManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;-><init>(Lorg/chromium/content/browser/ZoomManager;)V

    return-void
.end method

.method private ignoreDetectorEvents()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPermanentlyIgnoreDetectorEvents:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mTemporarilyIgnoreDetectorEvents:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method getPermanentlyIgnoreDetectorEvents()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPermanentlyIgnoreDetectorEvents:Z

    return v0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7

    const/4 v6, 0x1

    invoke-direct {p0}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->ignoreDetectorEvents()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPinchEventSent:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchBegin(JII)V

    iput-boolean v6, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPinchEventSent:Z

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchBy(JIIF)V

    move v0, v6

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->ignoreDetectorEvents()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPinchEventSent:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->setIgnoreSingleTap(Z)V

    move v0, v1

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3

    iget-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPinchEventSent:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->this$0:Lorg/chromium/content/browser/ZoomManager;

    # getter for: Lorg/chromium/content/browser/ZoomManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lorg/chromium/content/browser/ZoomManager;->access$000(Lorg/chromium/content/browser/ZoomManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewGestureHandler()Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->pinchEnd(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPinchEventSent:Z

    goto :goto_0
.end method

.method setPermanentlyIgnoreDetectorEvents(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mPermanentlyIgnoreDetectorEvents:Z

    return-void
.end method

.method setTemporarilyIgnoreDetectorEvents(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ZoomManager$ScaleGestureListener;->mTemporarilyIgnoreDetectorEvents:Z

    return-void
.end method
