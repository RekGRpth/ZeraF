.class Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;
.super Ljava/lang/Object;


# instance fields
.field final document:Landroid/graphics/PointF;

.field final screen:Landroid/graphics/PointF;

.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewCore;

.field final window:Landroid/graphics/PointF;


# direct methods
.method private constructor <init>(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->document:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentViewCore$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method


# virtual methods
.method setDocument(FF)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->document:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateWindowFromDocument()V

    return-void
.end method

.method setWindow(FF)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateDocumentFromWindow()V

    return-void
.end method

.method updateDocumentFromWindow()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$000(Lorg/chromium/content/browser/ContentViewCore;)F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$100(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewCore;->access$000(Lorg/chromium/content/browser/ContentViewCore;)F

    move-result v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewCore;->access$200(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->document:Landroid/graphics/PointF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateScreenFromWindow()V

    return-void
.end method

.method updateScreenFromWindow()V
    .locals 4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->screen:Landroid/graphics/PointF;

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v0

    iget-object v3, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/graphics/PointF;->set(FF)V

    return-void
.end method

.method updateWindowFromDocument()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->document:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollX:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$100(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$000(Lorg/chromium/content/browser/ContentViewCore;)F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->document:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativeScrollY:I
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewCore;->access$200(Lorg/chromium/content/browser/ContentViewCore;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mNativePageScaleFactor:F
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewCore;->access$000(Lorg/chromium/content/browser/ContentViewCore;)F

    move-result v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->window:Landroid/graphics/PointF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore$NormalizedPoint;->updateScreenFromWindow()V

    return-void
.end method
