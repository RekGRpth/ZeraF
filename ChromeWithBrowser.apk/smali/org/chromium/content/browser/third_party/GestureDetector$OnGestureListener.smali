.class public interface abstract Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onDown(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onLongPress(Landroid/view/MotionEvent;)V
.end method

.method public abstract onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onShowPress(Landroid/view/MotionEvent;)V
.end method

.method public abstract onSingleTapUp(Landroid/view/MotionEvent;)Z
.end method
