.class Lorg/chromium/content/browser/ContentViewCore$2$1;
.super Landroid/os/ResultReceiver;


# instance fields
.field final synthetic this$1:Lorg/chromium/content/browser/ContentViewCore$2;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore$2;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    invoke-direct {p0, p2}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onReceiveResult(ILandroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentViewCore$2;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v2

    if-eq p1, v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/ContentViewClient;->onImeStateChangeRequested(Z)V

    if-ne p1, v3, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentViewCore$2;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    iget-object v1, v1, Lorg/chromium/content/browser/ContentViewCore$2;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mFocusPreOSKViewportRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewCore;->access$700(Lorg/chromium/content/browser/ContentViewCore;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-nez p1, :cond_3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentViewCore$2;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->scrollFocusedEditableNodeIntoView()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$600(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$2$1;->this$1:Lorg/chromium/content/browser/ContentViewCore$2;

    iget-object v0, v0, Lorg/chromium/content/browser/ContentViewCore$2;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->undoScrollFocusedEditableNodeIntoViewIfNeeded(Z)V
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->access$500(Lorg/chromium/content/browser/ContentViewCore;Z)V

    goto :goto_1
.end method
