.class Lorg/chromium/content/browser/SelectPopupDialog;
.super Ljava/lang/Object;


# static fields
.field private static sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;


# instance fields
.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mListBoxPopup:Landroid/app/AlertDialog;


# direct methods
.method private constructor <init>(Lorg/chromium/content/browser/ContentViewCore;[Ljava/lang/String;[IZ[I)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    iput-object p1, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    new-instance v1, Landroid/widget/ListView;

    iget-object v2, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    if-eqz p4, :cond_0

    const v3, 0x104000a

    new-instance v4, Lorg/chromium/content/browser/SelectPopupDialog$1;

    invoke-direct {v4, p0, v1}, Lorg/chromium/content/browser/SelectPopupDialog$1;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/widget/ListView;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v3, 0x1040000

    new-instance v4, Lorg/chromium/content/browser/SelectPopupDialog$2;

    invoke-direct {v4, p0}, Lorg/chromium/content/browser/SelectPopupDialog$2;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    new-instance v2, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;

    invoke-direct {v2, p0, p2, p3, p4}, Lorg/chromium/content/browser/SelectPopupDialog$SelectPopupArrayAdapter;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;[Ljava/lang/String;[IZ)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    if-eqz p4, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    :goto_0
    array-length v2, p5

    if-ge v0, v2, :cond_2

    aget v2, p5, v0

    invoke-virtual {v1, v2, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    new-instance v2, Lorg/chromium/content/browser/SelectPopupDialog$3;

    invoke-direct {v2, p0, v1}, Lorg/chromium/content/browser/SelectPopupDialog$3;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/widget/ListView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    array-length v2, p5

    if-lez v2, :cond_2

    aget v2, p5, v0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    aget v0, p5, v0

    invoke-virtual {v1, v0, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    new-instance v1, Lorg/chromium/content/browser/SelectPopupDialog$4;

    invoke-direct {v1, p0}, Lorg/chromium/content/browser/SelectPopupDialog$4;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    new-instance v1, Lorg/chromium/content/browser/SelectPopupDialog$5;

    invoke-direct {v1, p0}, Lorg/chromium/content/browser/SelectPopupDialog$5;-><init>(Lorg/chromium/content/browser/SelectPopupDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/SelectPopupDialog;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/widget/ListView;)[I
    .locals 1

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/SelectPopupDialog;->getSelectedIndices(Landroid/widget/ListView;)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/SelectPopupDialog;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$202(Lorg/chromium/content/browser/SelectPopupDialog;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$302(Lorg/chromium/content/browser/SelectPopupDialog;)Lorg/chromium/content/browser/SelectPopupDialog;
    .locals 0

    sput-object p0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    return-object p0
.end method

.method static getCurrent()Lorg/chromium/content/browser/SelectPopupDialog;
    .locals 1

    sget-object v0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    return-object v0
.end method

.method private getSelectedIndices(Landroid/widget/ListView;)[I
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-array v4, v2, [I

    move v0, v1

    :goto_1
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v5

    aput v5, v4, v1

    move v1, v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-object v4
.end method

.method public static hide(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    sget-object v0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    if-eqz v0, :cond_2

    if-eqz p0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    iget-object v0, v0, Lorg/chromium/content/browser/SelectPopupDialog;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-ne v0, p0, :cond_2

    :cond_0
    if-eqz p0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewCore;->selectPopupMenuItems([I)V

    :cond_1
    sget-object v0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    iget-object v0, v0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_2
    return-void
.end method

.method public static show(Lorg/chromium/content/browser/ContentViewCore;[Ljava/lang/String;[IZ[I)V
    .locals 6

    const/4 v0, 0x0

    invoke-static {v0}, Lorg/chromium/content/browser/SelectPopupDialog;->hide(Lorg/chromium/content/browser/ContentViewCore;)V

    new-instance v0, Lorg/chromium/content/browser/SelectPopupDialog;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/content/browser/SelectPopupDialog;-><init>(Lorg/chromium/content/browser/ContentViewCore;[Ljava/lang/String;[IZ[I)V

    sput-object v0, Lorg/chromium/content/browser/SelectPopupDialog;->sShownDialog:Lorg/chromium/content/browser/SelectPopupDialog;

    iget-object v0, v0, Lorg/chromium/content/browser/SelectPopupDialog;->mListBoxPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
