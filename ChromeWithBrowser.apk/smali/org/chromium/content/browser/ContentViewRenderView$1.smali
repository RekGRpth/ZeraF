.class Lorg/chromium/content/browser/ContentViewRenderView$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewRenderView;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewRenderView;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    # getter for: Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewRenderView;->access$000(Lorg/chromium/content/browser/ContentViewRenderView;)I

    move-result v1

    # invokes: Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceSetSize(III)V
    invoke-static {v0, v1, p3, p4}, Lorg/chromium/content/browser/ContentViewRenderView;->access$100(Lorg/chromium/content/browser/ContentViewRenderView;III)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    # getter for: Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewRenderView;->access$000(Lorg/chromium/content/browser/ContentViewRenderView;)I

    move-result v1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    # invokes: Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceCreated(ILandroid/view/Surface;)V
    invoke-static {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewRenderView;->access$200(Lorg/chromium/content/browser/ContentViewRenderView;ILandroid/view/Surface;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewRenderView;->onReadyToRender()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewRenderView$1;->this$0:Lorg/chromium/content/browser/ContentViewRenderView;

    # getter for: Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:I
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewRenderView;->access$000(Lorg/chromium/content/browser/ContentViewRenderView;)I

    move-result v1

    # invokes: Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceDestroyed(I)V
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->access$300(Lorg/chromium/content/browser/ContentViewRenderView;I)V

    return-void
.end method
