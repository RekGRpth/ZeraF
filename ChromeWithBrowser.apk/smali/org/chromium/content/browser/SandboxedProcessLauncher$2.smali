.class final Lorg/chromium/content/browser/SandboxedProcessLauncher$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$clientContext:I

.field final synthetic val$connection:Lorg/chromium/content/browser/SandboxedProcessConnection;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/SandboxedProcessConnection;I)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$connection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    iput p2, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$clientContext:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$connection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->getPid()I

    move-result v0

    # getter for: Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "on connect callback, pid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " context="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$clientContext:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    # getter for: Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;
    invoke-static {}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->access$100()Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$connection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget v1, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$clientContext:I

    # invokes: Lorg/chromium/content/browser/SandboxedProcessLauncher;->nativeOnSandboxedProcessStarted(II)V
    invoke-static {v1, v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->access$300(II)V

    return-void

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;->val$connection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    # invokes: Lorg/chromium/content/browser/SandboxedProcessLauncher;->freeConnection(Lorg/chromium/content/browser/SandboxedProcessConnection;)V
    invoke-static {v1}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->access$200(Lorg/chromium/content/browser/SandboxedProcessConnection;)V

    goto :goto_0
.end method
