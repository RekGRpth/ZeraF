.class Lorg/chromium/content/browser/ContentViewCore$12;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2300(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->isNativeScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mContentViewGestureHandler:Lorg/chromium/content/browser/ContentViewGestureHandler;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2300(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewGestureHandler;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->isNativePinching()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->scheduleTextHandleFadeIn()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2400(Lorg/chromium/content/browser/ContentViewCore;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->isSelectionHandleShowing()Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2500(Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mSelectionHandleController:Lorg/chromium/content/browser/SelectionHandleController;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2600(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/SelectionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SelectionHandleController;->beginHandleFadeIn()V

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # invokes: Lorg/chromium/content/browser/ContentViewCore;->isInsertionHandleShowing()Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2700(Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewCore$12;->this$0:Lorg/chromium/content/browser/ContentViewCore;

    # getter for: Lorg/chromium/content/browser/ContentViewCore;->mInsertionHandleController:Lorg/chromium/content/browser/InsertionHandleController;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewCore;->access$2800(Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/InsertionHandleController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->beginHandleFadeIn()V

    goto :goto_0
.end method
