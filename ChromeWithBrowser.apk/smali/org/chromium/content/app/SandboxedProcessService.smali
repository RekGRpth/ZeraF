.class public Lorg/chromium/content/app/SandboxedProcessService;
.super Landroid/app/Service;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field private static final MAIN_THREAD_NAME:Ljava/lang/String; = "SandboxedProcessMain"

.field private static final TAG:Ljava/lang/String; = "SandboxedProcessService"

.field private static sContext:Landroid/content/Context;


# instance fields
.field private final mBinder:Lorg/chromium/content/common/ISandboxedProcessService$Stub;

.field private mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

.field private mCommandLineParams:[Ljava/lang/String;

.field private mCpuCount:I

.field private mCpuFeatures:J

.field private mFileFds:Ljava/util/ArrayList;

.field private mFileIds:Ljava/util/ArrayList;

.field private mLibraryInitialized:Z

.field private mNativeLibraryName:Ljava/lang/String;

.field private mSandboxMainThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mLibraryInitialized:Z

    new-instance v0, Lorg/chromium/content/app/SandboxedProcessService$1;

    invoke-direct {v0, p0}, Lorg/chromium/content/app/SandboxedProcessService$1;-><init>(Lorg/chromium/content/app/SandboxedProcessService;)V

    iput-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mBinder:Lorg/chromium/content/common/ISandboxedProcessService$Stub;

    return-void
.end method

.method static synthetic access$002(Lorg/chromium/content/app/SandboxedProcessService;Lorg/chromium/content/common/ISandboxedProcessCallback;)Lorg/chromium/content/common/ISandboxedProcessCallback;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

    return-object p1
.end method

.method static synthetic access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/content/Context;Lorg/chromium/content/app/SandboxedProcessService;[I[IIJ)V
    .locals 0

    invoke-static/range {p0 .. p6}, Lorg/chromium/content/app/SandboxedProcessService;->nativeInitSandboxedProcess(Landroid/content/Context;Lorg/chromium/content/app/SandboxedProcessService;[I[IIJ)V

    return-void
.end method

.method static synthetic access$1100()V
    .locals 0

    invoke-static {}, Lorg/chromium/content/app/SandboxedProcessService;->nativeExitSandboxedProcess()V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/content/app/SandboxedProcessService;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lorg/chromium/content/app/SandboxedProcessService;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lorg/chromium/content/app/SandboxedProcessService;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCpuCount:I

    return v0
.end method

.method static synthetic access$302(Lorg/chromium/content/app/SandboxedProcessService;I)I
    .locals 0

    iput p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCpuCount:I

    return p1
.end method

.method static synthetic access$400(Lorg/chromium/content/app/SandboxedProcessService;)J
    .locals 2

    iget-wide v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCpuFeatures:J

    return-wide v0
.end method

.method static synthetic access$402(Lorg/chromium/content/app/SandboxedProcessService;J)J
    .locals 0

    iput-wide p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCpuFeatures:J

    return-wide p1
.end method

.method static synthetic access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lorg/chromium/content/app/SandboxedProcessService;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$600(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$602(Lorg/chromium/content/app/SandboxedProcessService;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$700(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mNativeLibraryName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lorg/chromium/content/app/SandboxedProcessService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mLibraryInitialized:Z

    return p1
.end method

.method static synthetic access$900()Landroid/content/Context;
    .locals 1

    sget-object v0, Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;

    return-object v0
.end method

.method private establishSurfaceTexturePeer(IILjava/lang/Object;II)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

    if-nez v0, :cond_1

    const-string v0, "SandboxedProcessService"

    const-string v1, "No callback interface has been provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    instance-of v1, p3, Landroid/view/Surface;

    if-eqz v1, :cond_2

    check-cast p3, Landroid/view/Surface;

    move v6, v0

    move-object v3, p3

    :goto_1
    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;

    move v1, p1

    move v2, p2

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/chromium/content/common/ISandboxedProcessCallback;->establishSurfacePeer(IILandroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Landroid/view/Surface;->release()V

    goto :goto_0

    :cond_2
    instance-of v0, p3, Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_3

    new-instance v3, Landroid/view/Surface;

    check-cast p3, Landroid/graphics/SurfaceTexture;

    invoke-direct {v3, p3}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    const/4 v0, 0x1

    move v6, v0

    goto :goto_1

    :cond_3
    const-string v0, "SandboxedProcessService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a valid surfaceObject: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "SandboxedProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unable to call establishSurfaceTexturePeer: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Landroid/view/Surface;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Landroid/view/Surface;->release()V

    :cond_4
    throw v0
.end method

.method static getContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;

    return-object v0
.end method

.method private static native nativeExitSandboxedProcess()V
.end method

.method private static native nativeInitSandboxedProcess(Landroid/content/Context;Lorg/chromium/content/app/SandboxedProcessService;[I[IIJ)V
.end method

.method private native nativeShutdownSandboxMainThread()V
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/content/app/SandboxedProcessService;->stopSelf()V

    iget-object v1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    monitor-enter v1

    :try_start_0
    const-string v0, "com.google.android.apps.chrome.extra.sandbox_native_library_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mNativeLibraryName:Ljava/lang/String;

    const-string v0, "com.google.android.apps.chrome.extra.sandbox_command_line"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mBinder:Lorg/chromium/content/common/ISandboxedProcessService$Stub;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCreate()V
    .locals 3

    const-string v0, "SandboxedProcessService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating new SandboxedProcessService pid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    const-string v0, "SandboxedProcessService"

    const-string v1, "SanboxedProcessService created again in process!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sput-object p0, Lorg/chromium/content/app/SandboxedProcessService;->sContext:Landroid/content/Context;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lorg/chromium/content/app/SandboxedProcessService$2;

    invoke-direct {v1, p0}, Lorg/chromium/content/app/SandboxedProcessService$2;-><init>(Lorg/chromium/content/app/SandboxedProcessService;)V

    const-string v2, "SandboxedProcessMain"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const-string v0, "SandboxedProcessService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Destroying SandboxedProcessService pid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    monitor-enter v1

    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mLibraryInitialized:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lorg/chromium/content/app/SandboxedProcessService;->nativeShutdownSandboxMainThread()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
