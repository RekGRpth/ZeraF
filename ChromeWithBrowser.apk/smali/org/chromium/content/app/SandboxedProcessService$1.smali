.class Lorg/chromium/content/app/SandboxedProcessService$1;
.super Lorg/chromium/content/common/ISandboxedProcessService$Stub;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/chromium/content/app/SandboxedProcessService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/app/SandboxedProcessService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/app/SandboxedProcessService$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/chromium/content/app/SandboxedProcessService;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    invoke-direct {p0}, Lorg/chromium/content/common/ISandboxedProcessService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public setSurface(ILandroid/view/Surface;II)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Lorg/chromium/content/common/SurfaceCallback;->setSurface(ILandroid/view/Surface;II)V

    return-void
.end method

.method public setupConnection(Landroid/os/Bundle;Lorg/chromium/content/common/ISandboxedProcessCallback;)I
    .locals 5

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mCallback:Lorg/chromium/content/common/ISandboxedProcessCallback;
    invoke-static {v0, p2}, Lorg/chromium/content/app/SandboxedProcessService;->access$002(Lorg/chromium/content/app/SandboxedProcessService;Lorg/chromium/content/common/ISandboxedProcessCallback;)Lorg/chromium/content/common/ISandboxedProcessCallback;

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$200(Lorg/chromium/content/app/SandboxedProcessService;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    const-string v1, "com.google.android.apps.chrome.extra.sandbox_command_line"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v0, v1}, Lorg/chromium/content/app/SandboxedProcessService;->access$202(Lorg/chromium/content/app/SandboxedProcessService;[Ljava/lang/String;)[Ljava/lang/String;

    :cond_0
    sget-boolean v0, Lorg/chromium/content/app/SandboxedProcessService$1;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCommandLineParams:[Ljava/lang/String;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$200(Lorg/chromium/content/app/SandboxedProcessService;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    const-string v1, "com.google.android.apps.chrome.extra.cpu_count"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mCpuCount:I
    invoke-static {v0, v1}, Lorg/chromium/content/app/SandboxedProcessService;->access$302(Lorg/chromium/content/app/SandboxedProcessService;I)I

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    const-string v1, "com.google.android.apps.chrome.extra.cpu_features"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mCpuFeatures:J
    invoke-static {v0, v3, v4}, Lorg/chromium/content/app/SandboxedProcessService;->access$402(Lorg/chromium/content/app/SandboxedProcessService;J)J

    sget-boolean v0, Lorg/chromium/content/app/SandboxedProcessService$1;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mCpuCount:I
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$300(Lorg/chromium/content/app/SandboxedProcessService;)I

    move-result v0

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lorg/chromium/content/app/SandboxedProcessService;->access$502(Lorg/chromium/content/app/SandboxedProcessService;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lorg/chromium/content/app/SandboxedProcessService;->access$602(Lorg/chromium/content/app/SandboxedProcessService;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "com.google.android.apps.chrome.extra.sandbox_extraFile_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_fd"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileFds:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/chromium/content/app/SandboxedProcessService;->access$600(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "com.google.android.apps.chrome.extra.sandbox_extraFile_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_id"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mFileIds:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/chromium/content/app/SandboxedProcessService;->access$500(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/app/SandboxedProcessService$1;->this$0:Lorg/chromium/content/app/SandboxedProcessService;

    # getter for: Lorg/chromium/content/app/SandboxedProcessService;->mSandboxMainThread:Ljava/lang/Thread;
    invoke-static {v0}, Lorg/chromium/content/app/SandboxedProcessService;->access$100(Lorg/chromium/content/app/SandboxedProcessService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    return v0
.end method
