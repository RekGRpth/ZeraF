.class public Lorg/chromium/media/VideoCapture;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "media"
.end annotation


# static fields
.field private static final GL_TEXTURE_EXTERNAL_OES:I = 0x8d65

.field private static final NUM_CAPTURE_BUFFERS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoCapture"


# instance fields
.field private mCamera:Landroid/hardware/Camera;

.field private mCameraFacing:I

.field private mCameraOrientation:I

.field private mContext:Landroid/content/Context;

.field mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

.field private mDeviceOrientation:I

.field private mExpectedFrameSize:I

.field private mGlTextures:[I

.field private mId:I

.field private mIsRunning:Z

.field private mNativeVideoCaptureDeviceAndroid:I

.field private mPixelFormat:I

.field public mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    const v0, 0x32315659

    iput v0, p0, Lorg/chromium/media/VideoCapture;->mPixelFormat:I

    iput-object v2, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    iput-boolean v1, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mExpectedFrameSize:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mId:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mNativeVideoCaptureDeviceAndroid:I

    iput-object v2, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    iput-object v2, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    iput-object v2, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iput-object p1, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    iput p2, p0, Lorg/chromium/media/VideoCapture;->mId:I

    iput p3, p0, Lorg/chromium/media/VideoCapture;->mNativeVideoCaptureDeviceAndroid:I

    return-void
.end method

.method public static createVideoCapture(Landroid/content/Context;II)Lorg/chromium/media/VideoCapture;
    .locals 1

    new-instance v0, Lorg/chromium/media/VideoCapture;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/media/VideoCapture;-><init>(Landroid/content/Context;II)V

    return-object v0
.end method

.method private getDeviceOrientation()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x5a

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xb4

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x10e

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private native nativeOnFrameAvailable(I[BIIZZ)V
.end method


# virtual methods
.method public allocate(III)Z
    .locals 12

    const-string v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "allocate: requested width="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", frameRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget v0, p0, Lorg/chromium/media/VideoCapture;->mId:I

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iget v1, p0, Lorg/chromium/media/VideoCapture;->mId:I

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    iput v0, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    invoke-direct {p0}, Lorg/chromium/media/VideoCapture;->getDeviceOrientation()I

    move-result v0

    iput v0, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    const-string v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "allocate: device orientation="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", camera orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", facing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v0

    mul-int/lit16 v4, p3, 0x3e8

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v6, 0x0

    aget v6, v0, v6

    if-gt v6, v4, :cond_0

    const/4 v6, 0x1

    aget v6, v0, v6

    if-gt v4, v6, :cond_0

    const/4 v2, 0x1

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v3, 0x1

    aget v0, v0, v3

    move v5, v0

    move v6, v1

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "allocate: fps "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_1
    new-instance v0, Lorg/chromium/media/VideoCapture$CaptureCapability;

    invoke-direct {v0}, Lorg/chromium/media/VideoCapture$CaptureCapability;-><init>()V

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iput p3, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mDesiredFps:I

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    const v4, 0x7fffffff

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, p2

    move v2, p1

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    sub-int/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v9, v0, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v9, p2

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    add-int/2addr v3, v9

    const-string v9, "VideoCapture"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "allocate: support resolution ("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "), diff="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ge v3, v4, :cond_5

    iget v9, v0, Landroid/hardware/Camera$Size;->width:I

    rem-int/lit8 v9, v9, 0x20

    if-nez v9, :cond_5

    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    move v2, v3

    :goto_3
    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_2

    :cond_2
    const v0, 0x7fffffff

    if-ne v4, v0, :cond_3

    const-string v0, "VideoCapture"

    const-string v1, "allocate: can not find a resolution whose width is multiple of 32"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iput v2, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mWidth:I

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iput v1, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mHeight:I

    const-string v0, "VideoCapture"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "allocate: matched width="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7, v2, v1}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget v0, p0, Lorg/chromium/media/VideoCapture;->mPixelFormat:I

    invoke-virtual {v7, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    invoke-virtual {v7, v6, v5}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v0, 0x1

    iget-object v3, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const v0, 0x8d65

    iget-object v3, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const v0, 0x8d65

    const/16 v3, 0x2801

    const v4, 0x46180400

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const v0, 0x8d65

    const/16 v3, 0x2800

    const v4, 0x46180400

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    const v0, 0x8d65

    const/16 v3, 0x2802

    const v4, 0x812f

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const v0, 0x8d65

    const/16 v3, 0x2803

    const v4, 0x812f

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v3, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-direct {v0, v3}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    iget-object v3, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    mul-int v0, v2, v1

    iget v1, p0, Lorg/chromium/media/VideoCapture;->mPixelFormat:I

    invoke-static {v1}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x8

    const/4 v0, 0x0

    :goto_4
    const/4 v2, 0x3

    if-ge v0, v2, :cond_4

    new-array v2, v1, [B

    iget-object v3, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iput v1, p0, Lorg/chromium/media/VideoCapture;->mExpectedFrameSize:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "allocate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    move v0, v1

    move v1, v2

    move v2, v4

    goto/16 :goto_3

    :cond_6
    move v5, v1

    move v6, v2

    move v0, v3

    goto/16 :goto_0
.end method

.method public deallocate()V
    .locals 4

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/media/VideoCapture;->stopCapture()I

    :try_start_0
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deallocate: failed to deallocate camera, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-boolean v2, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    array-length v2, p1

    iget v3, p0, Lorg/chromium/media/VideoCapture;->mExpectedFrameSize:I

    if-ne v2, v3, :cond_4

    invoke-direct {p0}, Lorg/chromium/media/VideoCapture;->getDeviceOrientation()I

    move-result v2

    iget v3, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    if-eq v2, v3, :cond_2

    iput v2, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    const-string v3, "VideoCapture"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onPreviewFrame: device orientation="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", camera orientation="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget v3, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    if-ne v3, v0, :cond_7

    iget v3, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    add-int/2addr v2, v3

    rem-int/lit16 v2, v2, 0x168

    rsub-int v2, v2, 0x168

    rem-int/lit16 v4, v2, 0x168

    const/16 v2, 0xb4

    if-eq v4, v2, :cond_3

    if-nez v4, :cond_5

    :cond_3
    move v6, v0

    :goto_1
    if-nez v6, :cond_6

    :goto_2
    move v5, v0

    :goto_3
    iget v1, p0, Lorg/chromium/media/VideoCapture;->mNativeVideoCaptureDeviceAndroid:I

    iget v3, p0, Lorg/chromium/media/VideoCapture;->mExpectedFrameSize:I

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lorg/chromium/media/VideoCapture;->nativeOnFrameAvailable(I[BIIZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    goto :goto_0

    :cond_5
    move v6, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    :try_start_2
    iget v0, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    sub-int/2addr v0, v2

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v4, v0, 0x168
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v6, v1

    move v5, v1

    goto :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    if-eqz p2, :cond_8

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    :cond_8
    throw v0
.end method

.method public queryFrameRate()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mDesiredFps:I

    return v0
.end method

.method public queryHeight()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mHeight:I

    return v0
.end method

.method public queryWidth()I
    .locals 1

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCurrentCapability:Lorg/chromium/media/VideoCapture$CaptureCapability;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureCapability;->mWidth:I

    return v0
.end method

.method public startCapture()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v1, :cond_0

    const-string v0, "VideoCapture"

    const-string v1, "startCapture: camera is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-boolean v1, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, p0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public stopCapture()I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string v0, "VideoCapture"

    const-string v1, "stopCapture: camera is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
