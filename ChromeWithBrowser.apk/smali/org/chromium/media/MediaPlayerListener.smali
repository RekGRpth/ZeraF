.class Lorg/chromium/media/MediaPlayerListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "media"
.end annotation


# static fields
.field private static final MEDIA_ERROR_DECODE:I = 0x1

.field private static final MEDIA_ERROR_FORMAT:I = 0x0

.field private static final MEDIA_ERROR_INVALID_CODE:I = 0x3

.field public static final MEDIA_ERROR_MALFORMED:I = -0x3ef

.field private static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0x2

.field public static final MEDIA_ERROR_TIMED_OUT:I = -0x6e


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mNativeMediaPlayerListener:I


# direct methods
.method private constructor <init>(ILandroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    iput p1, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    iput-object p2, p0, Lorg/chromium/media/MediaPlayerListener;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static create(ILandroid/content/Context;Landroid/media/MediaPlayer;)Lorg/chromium/media/MediaPlayerListener;
    .locals 4

    new-instance v1, Lorg/chromium/media/MediaPlayerListener;

    invoke-direct {v1, p0, p1}, Lorg/chromium/media/MediaPlayerListener;-><init>(ILandroid/content/Context;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    invoke-virtual {p2, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    const-string v0, "android.permission.WAKE_LOCK"

    invoke-virtual {p1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1a

    invoke-virtual {p2, p1, v0}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    :cond_0
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    return-object v1
.end method

.method private native nativeOnBufferingUpdate(II)V
.end method

.method private native nativeOnMediaError(II)V
.end method

.method private native nativeOnMediaInterrupted(I)V
.end method

.method private native nativeOnMediaPrepared(I)V
.end method

.method private native nativeOnPlaybackComplete(I)V
.end method

.method private native nativeOnSeekComplete(I)V
.end method

.method private native nativeOnVideoSizeChanged(III)V
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    :cond_0
    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0}, Lorg/chromium/media/MediaPlayerListener;->nativeOnMediaInterrupted(I)V

    :cond_1
    return-void
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 1

    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0, p2}, Lorg/chromium/media/MediaPlayerListener;->nativeOnBufferingUpdate(II)V

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0}, Lorg/chromium/media/MediaPlayerListener;->nativeOnPlaybackComplete(I)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    const/4 v0, 0x3

    const/4 v1, 0x1

    sparse-switch p2, :sswitch_data_0

    :goto_0
    :sswitch_0
    iget v2, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v2, v0}, Lorg/chromium/media/MediaPlayerListener;->nativeOnMediaError(II)V

    return v1

    :sswitch_1
    sparse-switch p3, :sswitch_data_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_2
    move v0, v1

    goto :goto_0

    :sswitch_3
    move v0, v1

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x64 -> :sswitch_3
        0xc8 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x3ef -> :sswitch_2
        -0x6e -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0}, Lorg/chromium/media/MediaPlayerListener;->nativeOnMediaPrepared(I)V

    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1

    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0}, Lorg/chromium/media/MediaPlayerListener;->nativeOnSeekComplete(I)V

    return-void
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 1

    iget v0, p0, Lorg/chromium/media/MediaPlayerListener;->mNativeMediaPlayerListener:I

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/media/MediaPlayerListener;->nativeOnVideoSizeChanged(III)V

    return-void
.end method

.method public releaseResources()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/media/MediaPlayerListener;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/media/MediaPlayerListener;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    return-void
.end method
