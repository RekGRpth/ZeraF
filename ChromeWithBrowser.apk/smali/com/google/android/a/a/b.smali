.class public abstract Lcom/google/android/a/a/b;
.super Ljava/lang/Object;


# static fields
.field private static a:[B

.field private static final b:[B

.field private static c:[B

.field private static d:[B

.field private static e:[B

.field private static f:[B

.field private static g:[B

.field private static h:[B

.field private static i:[B


# instance fields
.field private j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "----------------314159265358979323846"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->a:[B

    sput-object v0, Lcom/google/android/a/a/b;->b:[B

    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->c:[B

    const-string v0, "\""

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->d:[B

    const-string v0, "--"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->e:[B

    const-string v0, "Content-Disposition: form-data; name="

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->f:[B

    const-string v0, "Content-Type: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->g:[B

    const-string v0, "; charset="

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->h:[B

    const-string v0, "Content-Transfer-Encoding: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/a/a/b;->i:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([Lcom/google/android/a/a/b;[B)J
    .locals 12

    const-wide/16 v5, -0x1

    const-wide/16 v3, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parts may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    move-wide v1, v3

    :goto_0
    array-length v7, p0

    if-ge v0, v7, :cond_3

    aget-object v7, p0, v0

    iput-object p1, v7, Lcom/google/android/a/a/b;->j:[B

    aget-object v7, p0, v0

    invoke-virtual {v7}, Lcom/google/android/a/a/b;->f()J

    move-result-wide v8

    cmp-long v8, v8, v3

    if-gez v8, :cond_1

    move-wide v7, v5

    :goto_1
    cmp-long v9, v7, v3

    if-gez v9, :cond_2

    move-wide v0, v5

    :goto_2
    return-wide v0

    :cond_1
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {v7, v8}, Lcom/google/android/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-direct {v7, v8}, Lcom/google/android/a/a/b;->c(Ljava/io/OutputStream;)V

    invoke-direct {v7, v8}, Lcom/google/android/a/a/b;->d(Ljava/io/OutputStream;)V

    invoke-direct {v7, v8}, Lcom/google/android/a/a/b;->e(Ljava/io/OutputStream;)V

    invoke-static {v8}, Lcom/google/android/a/a/b;->f(Ljava/io/OutputStream;)V

    sget-object v9, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {v8, v9}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7}, Lcom/google/android/a/a/b;->f()J

    move-result-wide v10

    add-long v7, v8, v10

    goto :goto_1

    :cond_2
    add-long/2addr v1, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/a/a/b;->e:[B

    array-length v0, v0

    int-to-long v3, v0

    add-long v0, v1, v3

    array-length v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    sget-object v2, Lcom/google/android/a/a/b;->e:[B

    array-length v2, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    sget-object v2, Lcom/google/android/a/a/b;->c:[B

    array-length v2, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_2
.end method

.method public static a(Ljava/io/OutputStream;[Lcom/google/android/a/a/b;[B)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parts may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    array-length v0, p2

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "partBoundary may not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    iput-object p2, v1, Lcom/google/android/a/a/b;->j:[B

    aget-object v1, p1, v0

    invoke-direct {v1, p0}, Lcom/google/android/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lcom/google/android/a/a/b;->c(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lcom/google/android/a/a/b;->d(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lcom/google/android/a/a/b;->e(Ljava/io/OutputStream;)V

    invoke-static {p0}, Lcom/google/android/a/a/b;->f(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lcom/google/android/a/a/b;->a(Ljava/io/OutputStream;)V

    sget-object v1, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/a/a/b;->e:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0, p2}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->e:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private b(Ljava/io/OutputStream;)V
    .locals 1

    sget-object v0, Lcom/google/android/a/a/b;->e:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lcom/google/android/a/a/b;->j:[B

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/a/a/b;->b:[B

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/a/a/b;->j:[B

    goto :goto_0
.end method

.method private c(Ljava/io/OutputStream;)V
    .locals 1

    sget-object v0, Lcom/google/android/a/a/b;->f:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private d(Ljava/io/OutputStream;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/a/a/b;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    sget-object v1, Lcom/google/android/a/a/b;->g:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lcom/google/android/a/a/b;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/a/a/b;->h:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    return-void
.end method

.method private e(Ljava/io/OutputStream;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    sget-object v1, Lcom/google/android/a/a/b;->i:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    return-void
.end method

.method public static e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private static f(Ljava/io/OutputStream;)V
    .locals 1

    sget-object v0, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/a/a/b;->c:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Ljava/io/OutputStream;)V
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method protected abstract f()J
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
