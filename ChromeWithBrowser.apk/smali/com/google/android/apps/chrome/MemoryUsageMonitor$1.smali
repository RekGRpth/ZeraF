.class Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "willBeSelected"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$000(Lcom/google/android/apps/chrome/MemoryUsageMonitor;ILcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)V

    :cond_0
    :goto_0
    return-void

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabSelected(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$100(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->onTabClosing(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$200(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$1;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    const v1, 0x7fffffff

    # invokes: Lcom/google/android/apps/chrome/MemoryUsageMonitor;->freeMemory(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->access$300(Lcom/google/android/apps/chrome/MemoryUsageMonitor;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x24 -> :sswitch_2
        0x30 -> :sswitch_0
        0x31 -> :sswitch_3
    .end sparse-switch
.end method
