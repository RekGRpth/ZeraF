.class public Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;
.super Ljava/lang/Object;


# instance fields
.field private mMatchedQuery:Ljava/lang/String;

.field private mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    iput-object p2, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    iget-object v2, p1, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getMatchedQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestion()Lcom/google/android/apps/chrome/OmniboxSuggestion;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x35

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public setMatchedQuery(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxPopupItem;->mMatchedQuery:Ljava/lang/String;

    return-void
.end method
