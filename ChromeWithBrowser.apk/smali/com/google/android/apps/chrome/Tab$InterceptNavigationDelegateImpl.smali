.class Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/components/navigation_interception/InterceptNavigationDelegate;


# instance fields
.field final mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/Tab;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/UrlHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/UrlHandler;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/Tab;)V

    return-void
.end method

.method private getReferrerUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getNavigationHistory()Lorg/chromium/content/browser/NavigationHistory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationHistory;->getCurrentEntryIndex()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content/browser/NavigationEntry;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationEntry;->getOriginalUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public shouldIgnoreNavigation(Lorg/chromium/components/navigation_interception/NavigationParams;)Z
    .locals 8

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v1, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->url:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->handleSnapshotOrPrintJobUrl(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$4600(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->getReferrerUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mIncognito:Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/Tab;->access$4700(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v3

    iget v4, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->pageTransitionType:I

    iget-boolean v5, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->isRedirect:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideUrlLoading(Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mContentView:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$400(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lorg/chromium/content/browser/ContentView;->canGoToOffset(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mChromeWebContentsDelegateAndroid:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$4800(Lcom/google/android/apps/chrome/Tab;)Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;->closeContents()V

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method public shouldIgnoreNewTab(Ljava/lang/String;Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->handleSnapshotOrPrintJobUrl(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Tab;->access$4600(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideNewTab(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method
