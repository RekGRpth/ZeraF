.class public Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/ContentViewHolder;
.implements Lcom/google/android/apps/chrome/compositor/Invalidator$Host;
.implements Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;
.implements Lcom/google/android/apps/chrome/tabs/TabsView;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
.implements Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final INPUT_EVENT_LAG_FROM_VSYNC_MICROSECONDS:J = 0xc80L

.field private static final MAX_SWAP_BUFFERS_COUNT:I = 0x2

.field private static final NOTIFICATIONS:[I

.field private static final TAG:Ljava/lang/String;

.field private static final THUMBNAIL_MAX_DELAY_MS:I = 0xfa0

.field private static final THUMBNAIL_NORMAL_CAPTURE_DELAY_MS:I = 0x0

.field private static final THUMBNAIL_NO_TAB_DELAY_MS:I = 0x190

.field private static final THUMBNAIL_NTP_CAPTURE_DELAY_MS:I = 0x14


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

.field private mContentOverlayVisiblity:Z

.field private mDebugOverlay:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

.field private mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mEventOffsetX:F

.field private mEventOffsetY:F

.field private mFindToolbarShowing:Z

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private final mGrabSnapshotRunnable:Ljava/lang/Runnable;

.field private mIsKeyboardShowing:Z

.field private mLastContentOffset:F

.field private mLastVisibleContentOffset:F

.field private mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

.field private mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

.field private final mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

.field private mNeedToRender:Z

.field private mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mPendingInvalidations:Ljava/util/ArrayList;

.field private mPendingRenders:I

.field private mPendingSwapBuffersCount:I

.field private mPostHideKeyboardTask:Ljava/lang/Runnable;

.field private mSkipInvalidation:Z

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private final mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

.field private mViewVisible:Lorg/chromium/content/browser/ContentView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->TAG:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x2d
        0x3b
        0x20
        0x16
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    iput v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mGrabSnapshotRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lorg/chromium/content/browser/VSyncMonitor;

    new-instance v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-direct {v0, p1, v1}, Lorg/chromium/content/browser/VSyncMonitor;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/VSyncMonitor$Listener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerTablet;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addView(Landroid/view/View;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setBackgroundColor(I)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerPhone;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/VSyncMonitor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    return v0
.end method

.method static synthetic access$408(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->render()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    return p1
.end method

.method private flushInvalidation()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "CompositorViewHolder.flushInvalidation"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private getVisibleContentView()Lorg/chromium/content/browser/ContentView;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/ContentView;

    sget-boolean v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private modelChanged()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/Tab;->restoreState(Landroid/app/Activity;)V

    :cond_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-ne v2, v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mGrabSnapshotRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPendingTextureReadbackId()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-eqz v1, :cond_4

    const/16 v0, 0xfa0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mGrabSnapshotRunnable:Ljava/lang/Runnable;

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_4
    const/16 v0, 0x190

    goto :goto_2
.end method

.method private offsetMotionEvent(Landroid/view/MotionEvent;Z)V
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getViewport()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetX:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getViewport()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetY:F

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetX:F

    neg-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetY:F

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :cond_3
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetX:F

    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventOffsetY:F

    goto :goto_0
.end method

.method private postRender()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$5;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPendingTextureReadbackId()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isLoadingAndRenderingDone()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v2, "chrome://newtab/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x14

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mGrabSnapshotRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->removeCallbacks(Ljava/lang/Runnable;)Z

    new-instance v2, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    int-to-long v0, v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private propagateViewportToLayouts(II)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    float-to-int v2, v2

    invoke-direct {v1, v4, v2, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    float-to-int v3, v3

    invoke-direct {v2, v4, v3, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onViewportSizeChanged(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    return-void
.end method

.method private render()V
    .locals 3

    const-string v0, "CompositorViewHolder:render"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onUpdate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->layoutToLayers(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;)V

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->postRender()V

    :cond_1
    const-string v0, "CompositorViewHolder:render"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    return-void
.end method

.method private updateContentOverlayVisibility(Z)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isAlive()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateContentViewLayoutParams(Landroid/view/ViewGroup$MarginLayoutParams;)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v3}, Lorg/chromium/content/browser/ContentView;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->requestFocus()Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->setVisibility(I)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public clearChildFocus(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public deferInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    if-gtz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public enableTabSwiping(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->enableTabSwiping(Z)V

    return-void
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method public getGLResourceProvider()Lcom/google/android/apps/chrome/gl/GLResourceProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    return-object v0
.end method

.method public getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    return-object v0
.end method

.method public getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
    .locals 0

    return-object p0
.end method

.method public getLayoutTabsDrawnCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getLastLayerCount()I

    move-result v0

    return v0
.end method

.method public getTitleCache()Lcom/google/android/apps/chrome/tabs/TitleCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public getViewport()Landroid/graphics/Rect;
    .locals 5

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public getVisibleViewport()Landroid/graphics/Rect;
    .locals 5

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method protected handleFindInPage(Z)V
    .locals 0

    return-void
.end method

.method public hide()V
    .locals 0

    return-void
.end method

.method public hideKeyboard(Ljava/lang/Runnable;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public isTabInteractive()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isTabInteractive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadPersitentTextureDataIfNeeded()V
    .locals 0

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-static {p0}, Lcom/google/android/apps/chrome/compositor/Invalidator;->set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    return-void
.end method

.method public onContentOffsetChanged(F)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->unregisterNotifications()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/Invalidator;->set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onEndGesture()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onEndGesture()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->offsetMotionEvent(Landroid/view/MotionEvent;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->onMotionEvent(Landroid/view/MotionEvent;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z

    move-result v0

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    if-eqz p1, :cond_0

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/ViewUtilities;->isKeyboardShowing(Landroid/app/Activity;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    return-void
.end method

.method public onNativeLibraryReady()V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Should be called once"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->initNativeCompositor()V

    new-instance v0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    invoke-virtual {v0}, Lorg/chromium/content/browser/VSyncMonitor;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->removeListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/content/browser/ContentViewCore;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    :cond_0
    return-void
.end method

.method public onStartGesture()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onStartGesture()V

    return-void
.end method

.method public onSurfaceCreated()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    return-void
.end method

.method public onSwapBuffersCompleted()V
    .locals 2

    const-string v0, "onSwapBuffersCompleted"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    if-nez v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->offsetMotionEvent(Landroid/view/MotionEvent;Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->onMotionEvent(Landroid/view/MotionEvent;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewportSizeChanged(II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    return-void
.end method

.method public onVisibleContentOffsetChanged(F)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onHostFocusChanged(Z)V

    return-void
.end method

.method public postLoadBitmapFromResource(Lcom/google/android/apps/chrome/tabs/BitmapRequester;I)V
    .locals 0

    return-void
.end method

.method public propagateEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getVisibleContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public pushDebugRect(Landroid/graphics/Rect;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mDebugOverlay:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mDebugOverlay:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mDebugOverlay:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mDebugOverlay:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->pushRect(Landroid/graphics/Rect;I)V

    return-void
.end method

.method public pushDebugValues(FFF)V
    .locals 0

    return-void
.end method

.method public requestRender()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->consumePendingRendererFrame()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    iget v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    add-int/2addr v0, v2

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    const-string v0, "requestRender:now"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    if-gtz v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    const-string v0, "requestRender:later"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    invoke-virtual {v0}, Lorg/chromium/content/browser/VSyncMonitor;->requestUpdate()V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public setContentOverlayVisibility(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    :cond_0
    return-void
.end method

.method public setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    return-void
.end method

.method public setFullscreenHandler(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getVisibleContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    return-void
.end method

.method public setLayoutManager(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    move-result-object v0

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/TabModelSelector;->registerChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V

    return-void
.end method

.method public show()V
    .locals 0

    return-void
.end method

.method public shutDown()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    invoke-virtual {v0}, Lorg/chromium/content/browser/VSyncMonitor;->unregisterListener()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->shutDown()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->shutDown()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerDecorationCache:Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->shutDown()V

    :cond_1
    return-void
.end method
