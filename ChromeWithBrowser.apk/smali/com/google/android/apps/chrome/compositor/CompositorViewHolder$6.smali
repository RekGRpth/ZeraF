.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

.field final synthetic val$cache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->val$cache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->val$cache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPendingTextureReadbackId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->val$cache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->commitTextureReadback()Z

    :cond_0
    return-void
.end method
