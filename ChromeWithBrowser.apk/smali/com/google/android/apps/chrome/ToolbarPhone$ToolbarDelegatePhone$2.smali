.class Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;
.super Lcom/google/android/apps/chrome/ChromeAnimation;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$600(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone$2;->this$1:Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    return-void
.end method
