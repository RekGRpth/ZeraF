.class public interface abstract Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onRefineSuggestion(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
.end method

.method public abstract onSelection(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
.end method

.method public abstract onSetUrlToSuggestion(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V
.end method
