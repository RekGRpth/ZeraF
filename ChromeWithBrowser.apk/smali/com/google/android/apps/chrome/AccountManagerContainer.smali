.class public Lcom/google/android/apps/chrome/AccountManagerContainer;
.super Ljava/lang/Object;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mActivity:Landroid/app/Activity;

.field private mAuthToken:Ljava/lang/String;

.field private mExactMatch:Z

.field private mParsedAccountName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mActivity:Landroid/app/Activity;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-static {p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weblogin:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAuthToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mParsedAccountName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public exactMatch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mExactMatch:Z

    return v0
.end method

.method public getAccount(Landroid/accounts/AccountManagerCallback;)Landroid/accounts/Account;
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mParsedAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "exact-match-auto-login"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mExactMatch:Z

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAuthToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mActivity:Landroid/app/Activity;

    move-object v5, p1

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthToken(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;)V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mAuthToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/AccountManagerContainer;->mActivity:Landroid/app/Activity;

    move-object v1, p1

    move-object v5, p2

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method
