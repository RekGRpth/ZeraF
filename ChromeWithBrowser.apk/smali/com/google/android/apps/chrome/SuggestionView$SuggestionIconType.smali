.class final enum Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field public static final enum BOOKMARK:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field public static final enum GLOBE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field public static final enum HISTORY:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field public static final enum MAGNIFIER:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

.field public static final enum VOICE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const-string v1, "BOOKMARK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->BOOKMARK:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const-string v1, "HISTORY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->HISTORY:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const-string v1, "GLOBE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->GLOBE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const-string v1, "MAGNIFIER"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->MAGNIFIER:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    new-instance v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->VOICE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->BOOKMARK:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->HISTORY:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->GLOBE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->MAGNIFIER:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->VOICE:Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->$VALUES:[Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->$VALUES:[Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/SuggestionView$SuggestionIconType;

    return-object v0
.end method
