.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field final synthetic val$show:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->val$show:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method

.method private resetBrowserOffsets()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    const/high16 v1, 0x7fc00000

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;F)F

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$402(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->resetBrowserOffsets()V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->val$show:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentShowTime:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$600(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    const/4 v3, 0x0

    const-wide/16 v4, 0xbb8

    sub-long v0, v4, v0

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    # invokes: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V
    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$700(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;ZJ)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->resetBrowserOffsets()V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->val$show:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentShowTime:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->access$602(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;J)J

    :cond_0
    return-void
.end method
