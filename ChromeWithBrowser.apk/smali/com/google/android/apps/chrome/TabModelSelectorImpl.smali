.class public Lcom/google/android/apps/chrome/TabModelSelectorImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/TabModelSelector;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final INCOGNITO_TAB_MODEL_INDEX:I = 0x1

.field private static final NORMAL_TAB_MODEL_INDEX:I = 0x0

.field private static final NOTIFICATIONS:[I

.field public static final NTP_CACHED_TAG:Ljava/lang/String; = "#cached_ntp"

.field private static final NTP_CACHE_CREATE_DELAY_MS:J = 0x12cL

.field private static final NTP_CACHE_CREATE_DELAY_URL_LAUNCH_MS:J = 0xea60L

.field private static final NTP_PRIME_BITMAP_CAPTURE_DELAY_MS:J = 0x3e8L

.field private static final TAG:Ljava/lang/String;

.field private static final UNKNOWN_APP_ID:Ljava/lang/String; = "com.google.android.apps.chrome.unknown_app"

.field public static sAllowCreateCachedNtp:Z


# instance fields
.field private mActiveModelIndex:I

.field private mActiveState:Z

.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mChangeListeners:Ljava/util/ArrayList;

.field private mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

.field private mInOverviewMode:Z

.field private mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

.field private final mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mNtpCacheCreateDelayMs:J

.field private final mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;

.field private mPreloadWebViewContainer:Landroid/view/ViewGroup;

.field private final mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

.field private mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

.field private mVisibleTab:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const-class v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->TAG:Ljava/lang/String;

    sput-boolean v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->sAllowCreateCachedNtp:Z

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x1
        0x10
        0x1b
        0x20
        0x2
        0x19
        0x39
        0x3a
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;ZZLorg/chromium/ui/gfx/NativeWindow;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveState:Z

    new-array v2, v1, [Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    const-wide/16 v2, 0x12c

    iput-wide v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz p2, :cond_1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    new-instance v0, Lcom/google/android/apps/chrome/TabPersistentStore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    new-instance v0, Lcom/google/android/apps/chrome/TabModelOrderController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/TabModelOrderController;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;

    iput-object p4, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    if-eqz p3, :cond_0

    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;)Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelOrderController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lorg/chromium/ui/gfx/NativeWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)I
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getTransitionType(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreated(ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/chrome/TabModelSelectorImpl;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J

    return-wide p1
.end method

.method static synthetic access$1900()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IIIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabMoved(IIIZ)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleOnOverviewModeHidden()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleOnPageLoadStopped(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleNewTabPageReady(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleAutoLoginResult(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->handleAutoLoginDisabled()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->blockingNotifyTabClosing(IZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabPersistentStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IZIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabClosed(IZIZ)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabSelected(IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;

    return-object p1
.end method

.method private blockingNotifyModelSelected()V
    .locals 3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->isIncognitoSelected()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    const-string v2, "tabId"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v0, 0xc

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private blockingNotifyTabClosing(IZ)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "animate"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0x24

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private getModel(I)Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/EmptyTabModel;->getInstance()Lcom/google/android/apps/chrome/EmptyTabModel;

    move-result-object v0

    goto :goto_0
.end method

.method private static getTransitionType(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)I
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$3;->$SwitchMap$com$google$android$apps$chrome$TabModel$TabLaunchType:[I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    const/high16 v0, 0x8000000

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleAutoLoginDisabled()V
    .locals 8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    move v0, v1

    :goto_1
    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_1

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/Tab;->getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/Tab;->getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->dismissAutoLogins()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private handleAutoLoginResult(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    move v0, v1

    :goto_1
    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_1

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/Tab;->getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/Tab;->getInfoBarContainer()Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    move-result-object v6

    invoke-virtual {v6, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/infobar/InfoBarContainer;->dismissAutoLogins(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private handleNewTabPageReady(I)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabOrCachedNtpById(I)Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$3000(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_1

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->onNtpResourcesLoaded(Lcom/google/android/apps/chrome/Tab;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$3100(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;Lcom/google/android/apps/chrome/Tab;)V

    :cond_1
    return-void
.end method

.method private handleOnOverviewModeHidden()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private handleOnOverviewModeShown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z

    return-void
.end method

.method private handleOnPageLoadStopped(I)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    sget-boolean v2, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-eqz v0, :cond_1

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createDelayedCacheNtp(I)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$3200(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v1, v2, v0

    invoke-interface {v1, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->addTabToSaveQueue(Lcom/google/android/apps/chrome/Tab;)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private notifyTabClosed(IZIZ)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "incognito"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "nextId"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "nextIncognito"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v1, 0x5

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyTabCreated(ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V
    .locals 3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "tabId"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :cond_0
    const-string v2, "sourceTabId"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "type"

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "x"

    invoke-virtual {v1, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "y"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "incognito"

    invoke-virtual {v1, v0, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "willBeSelected"

    invoke-virtual {v1, v0, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v0, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    .locals 3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "tabId"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :cond_0
    const-string v2, "sourceTabId"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "type"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "incognito"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "willBeSelected"

    invoke-virtual {v1, v0, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v0, 0x30

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyTabMoved(IIIZ)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "fromPosition"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "toPosition"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "incognito"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v1, 0x4

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private notifyTabSelected(IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "lastId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "type"

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "incognito"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method clearCachedNtpAndThumbnails()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->clearCachedNtpAndThumbnails()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->access$3400(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V

    return-void
.end method

.method public clearEncryptedState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->clearEncryptedState()V

    return-void
.end method

.method public clearState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->clearState()V

    return-void
.end method

.method public closeAllTabs()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->closeAllTabs()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z

    move-result v0

    return v0
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-interface {v4, p1, p2}, Lcom/google/android/apps/chrome/TabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public closeTabById(I)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTabById(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 5

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveState:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->destroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->destroy()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-array v0, v1, [Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    return-void
.end method

.method public getAllModels()[Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    return-object v0
.end method

.method public getCurrentModel()Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(I)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentModelIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    return v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    return-object v0
.end method

.method public getModel(Z)Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(I)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrivateDirtyMemoryOfRenderersKBytes()I
    .locals 11

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v6, v5

    move v3, v1

    move v0, v1

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    move v2, v0

    move v0, v1

    :goto_1
    invoke-interface {v7}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v8

    if-ge v0, v8, :cond_1

    invoke-interface {v7, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPrivateSizeKBytes()I

    move-result v8

    add-int/2addr v2, v8

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_0

    :cond_2
    return v0
.end method

.method public getRestoredTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->getRestoredTabCount()I

    move-result v0

    return v0
.end method

.method public getTabById(I)Lcom/google/android/apps/chrome/Tab;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object v0
.end method

.method public getTotalTabCount()I
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public isIncognitoSelected()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->loadState()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->incrementIdCounterTo(I)V

    :cond_0
    return-void
.end method

.method public notifyChanged()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;->onChange()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onActivityDestroyed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->destroy()V

    return-void
.end method

.method onNativeLibraryReady(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveState:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "onNativeLibraryReady called twice!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->setStackedReads(Z)V

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    new-instance v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Z)V

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mModels:[Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    new-instance v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Z)V

    aput-object v1, v0, v4

    new-instance v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sget-object v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveState:Z

    return-void
.end method

.method public openNewTab(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Lcom/google/android/apps/chrome/Tab;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0, p4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-nez p3, :cond_0

    invoke-interface {v0, p1, v2, p2}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v5

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v1

    if-ne p4, v1, :cond_1

    invoke-interface {v0, v5}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v4

    :goto_1
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v6

    move-object v1, p1

    move-object v3, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;

    goto :goto_0

    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public registerChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public saveState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->saveState()V

    return-void
.end method

.method public selectModel(Z)V
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActiveModelIndex:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(I)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->blockingNotifyModelSelected()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$2;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setClearBrowsingDataActivityStarter(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

    return-void
.end method

.method public setPreloadWebViewContainer(Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method public unregisterChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
