.class public Lcom/google/android/apps/chrome/EmptyTabModel;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/TabModel;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/EmptyTabModel;
    .locals 1

    # getter for: Lcom/google/android/apps/chrome/EmptyTabModel$LazyHolder;->INSTANCE:Lcom/google/android/apps/chrome/EmptyTabModel;
    invoke-static {}, Lcom/google/android/apps/chrome/EmptyTabModel$LazyHolder;->access$000()Lcom/google/android/apps/chrome/EmptyTabModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bringToFrontOrLaunchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V
    .locals 0

    return-void
.end method

.method public closeAllTabs()V
    .locals 0

    return-void
.end method

.method public closeCurrentTab()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public closeTabById(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public closeTabByIndex(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public createTabWithNativeContents(IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public destroy()V
    .locals 0

    return-void
.end method

.method public getCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNextTabIfClosed(I)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTab(I)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabById(I)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabIndexById(I)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getTabIndexByUrl(Ljava/lang/String;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getView(I)Lorg/chromium/content/browser/ContentView;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public index()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public indexOf(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSessionRestoreInProgress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public launchNTP()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public moveTab(II)V
    .locals 0

    return-void
.end method

.method public setIndex(I)V
    .locals 0

    return-void
.end method
