.class public Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;
.super Ljava/lang/Object;


# static fields
.field public static final CACHE_ACTIVE_SIZE:I = 0x1

.field public static final CACHE_INACTIVE_SIZE:I

.field private static final mByteBufferCache:Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

.field private static final mHeaderBuffer:Ljava/nio/ByteBuffer;

.field private static final mIoBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$1;)V

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mByteBufferCache:Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    const/16 v0, 0x10

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized createTexture(Ljava/io/InputStream;)Landroid/opengl/ETC1Util$ETC1Texture;
    .locals 9

    const/16 v5, 0x10

    const/4 v0, 0x0

    const-class v1, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-virtual {p0, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-eq v2, v5, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Unable to read PKM file header."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    sget-object v3, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-virtual {v2, v3, v4, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    invoke-static {v2}, Landroid/opengl/ETC1;->isValid(Ljava/nio/Buffer;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Not a PKM file."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    invoke-static {v2}, Landroid/opengl/ETC1;->getWidth(Ljava/nio/Buffer;)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mHeaderBuffer:Ljava/nio/ByteBuffer;

    invoke-static {v3}, Landroid/opengl/ETC1;->getHeight(Ljava/nio/Buffer;)I

    move-result v3

    invoke-static {v2, v3}, Landroid/opengl/ETC1;->getEncodedDataSize(II)I

    move-result v4

    sget-object v5, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mByteBufferCache:Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->getBuffer(I)Ljava/nio/ByteBuffer;
    invoke-static {v5, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->access$200(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    :goto_0
    if-ge v0, v4, :cond_3

    sget-object v6, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    array-length v6, v6

    sub-int v7, v4, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    sget-object v7, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    if-eq v7, v6, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Unable to read PKM file data."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v7, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mIoBuffer:[B

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    add-int/2addr v0, v6

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    new-instance v0, Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-direct {v0, v2, v3, v5}, Landroid/opengl/ETC1Util$ETC1Texture;-><init>(IILjava/nio/ByteBuffer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized recycleTexture(Landroid/opengl/ETC1Util$ETC1Texture;)V
    .locals 3

    const-class v1, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;

    monitor-enter v1

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mByteBufferCache:Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

    invoke-virtual {p0}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->returnBuffer(Ljava/nio/ByteBuffer;)V
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->access$100(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setByteBufferCacheSize(I)V
    .locals 2

    const-class v1, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->mByteBufferCache:Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->setSize(I)V
    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->access$300(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
