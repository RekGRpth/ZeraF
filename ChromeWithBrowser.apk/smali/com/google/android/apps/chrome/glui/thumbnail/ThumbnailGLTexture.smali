.class public Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;
.super Ljava/lang/Object;


# instance fields
.field private mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

.field private mScale:F

.field private mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

.field protected mThumbnailProvider:Lcom/google/android/apps/chrome/TabThumbnailProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/TabThumbnailProvider;Lcom/google/android/apps/chrome/gl/GLResourceProvider;)V
    .locals 4

    const/high16 v3, 0x3f800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mThumbnailProvider:Lcom/google/android/apps/chrome/TabThumbnailProvider;

    iput-object p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    div-float v2, v3, v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->getContentTexture(FLorg/chromium/content/browser/ContentViewCore;)Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    :cond_1
    return-void
.end method

.method private getBitmapForTexture()Landroid/graphics/Bitmap;
    .locals 6

    const/4 v0, 0x0

    const/high16 v5, 0x3f800000

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->isTextureValid()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "getBitmapForTexture"

    invoke-static {v1}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->copyTextureToBitmap(Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mThumbnailProvider:Lcom/google/android/apps/chrome/TabThumbnailProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_2

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    div-float v3, v5, v3

    iget v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    div-float v4, v5, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    const-string v0, "getBitmapForTexture"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    iget v0, v0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    iget v1, v1, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->deleteTexture(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    :cond_0
    return-void
.end method

.method public generateThumbnailBitmap()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getBitmapForTexture()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mScale:F

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;-><init>(Landroid/graphics/Bitmap;F)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mThumbnailProvider:Lcom/google/android/apps/chrome/TabThumbnailProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getId()I

    move-result v0

    return v0
.end method

.method public isTextureValid()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    iget v0, v0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mTextureId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->mTextureData:Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;

    iget-object v0, v0, Lcom/google/android/apps/chrome/gl/GLResourceProvider$ContentTextureData;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
