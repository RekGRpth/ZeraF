.class public Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CAPTURE_MIN_REQUEST_TIME_MS:J = 0x3e8L

.field private static final CLEAN_UP_MEMORY_DELAY:I = 0x1f4

.field private static final MAX_META_DATA_SIZE:I = 0xa

.field private static final NOTIFICATIONS:[I

.field private static final TAG:Ljava/lang/String;

.field private static final TEXTURE_DIRECTORY:Ljava/lang/String; = "textures"

.field private static final USE_ONLY_SELECTED_LIVE_LAYER:Z = true

.field private static dropCachedNtpOnLowMemory:Z


# instance fields
.field protected final mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

.field private final mApproximationCacheSize:I

.field protected final mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

.field private final mCleanupCPUMemoryRunnable:Ljava/lang/Runnable;

.field private final mCleanupReference:Lorg/chromium/content/common/CleanupReference;

.field private final mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field protected mCompressionQueueMaxSize:I

.field private mCompressionTaskRunning:Z

.field private final mContext:Landroid/content/Context;

.field private final mDefaultCacheSize:I

.field protected mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

.field private final mHandler:Landroid/os/Handler;

.field private final mLastVisibleListIds:Ljava/util/ArrayList;

.field private final mListeners:Ljava/util/ArrayList;

.field private final mLiveLayers:Ljava/util/ArrayList;

.field private mNativeThumbnailCache:I

.field private final mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field protected mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

.field private final mPinnedTextures:Ljava/util/HashMap;

.field private final mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private mReadTaskRunning:Z

.field private mStackedReads:Z

.field private final mTextureFiles:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

.field private final mTexturesDir:Ljava/io/File;

.field private final mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

.field protected mUseApproximationTextures:Z

.field private final mVisibleIdsList:Ljava/util/ArrayDeque;

.field private final mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field protected mWriteQueueMaxSize:I

.field private mWriteTaskRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    sput-boolean v1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->dropCachedNtpOnLowMemory:Z

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0xc
        0x3
        0x27
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/gl/GLResourceProvider;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/gl/GLResourceProvider;Z)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/gl/GLResourceProvider;Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mUseApproximationTextures:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLiveLayers:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    const/16 v3, 0xa

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;

    invoke-direct {v0, p0, v5}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCleanupCPUMemoryRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mStackedReads:Z

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    const/16 v3, 0x64

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTextureFiles:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$5;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    const-string v3, "textures"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTexturesDir:Ljava/io/File;

    new-instance v0, Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    const/high16 v3, 0x7f0d0000

    const-string v4, "thumbnails"

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/utilities/ResourceUtilities;->getIntegerResourceWithOverride(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mDefaultCacheSize:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d0003

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueueMaxSize:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d0002

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueueMaxSize:I

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mDefaultCacheSize:I

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0001

    const-string v4, "approximation-thumbnails"

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/utilities/ResourceUtilities;->getIntegerResourceWithOverride(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCacheSize:I

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCacheSize:I

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mUseApproximationTextures:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;->detachFromThread()V

    if-eqz p3, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeInit(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeGetAllAttachedLiveLayers(I)[I

    move-result-object v0

    :goto_1
    array-length v1, v0

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLiveLayers:Ljava/util/ArrayList;

    aget v3, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/chromium/content/common/CleanupReference;

    new-instance v1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    invoke-direct {v1, v2, v5}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$DestroyRunnable;-><init>(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/common/CleanupReference;-><init>(Ljava/lang/Object;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCleanupReference:Lorg/chromium/content/common/CleanupReference;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->registerNotifications()V

    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->makeSpaceForNewItemIfNecessary(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->compressNextTexture()V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Ljava/lang/Integer;)Ljava/io/File;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getFile(Ljava/lang/Integer;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(I)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeDestroy(I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unregisterNotifications()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeSetSelectedTab(I)V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeNextTexture()V

    return-void
.end method

.method private addIdToVisibleIds(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->maximumCacheSize()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private checkAndUpdateThumbnailMetaData(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->isSavedAndViewDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getProgress()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;

    if-eqz v0, :cond_2

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->access$200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mCaptureTime:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->access$300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;-><init>(JLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->shouldCleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupTexture()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto :goto_0
.end method

.method private compressNextTexture()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;->calledOnValidThread()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    const-string v1, "compressNextTexture not called on valid thread"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$3;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$3;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method private compressTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->compressionRequired()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeDuplicateIdsFromQueueHelper(Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueueMaxSize:I

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Compression queue is full, dropping thumbnail from queue"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->compressNextTexture()V

    move v0, v1

    goto :goto_0
.end method

.method private deleteFileAsync(I)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$4;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$4;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getFile(Ljava/lang/Integer;)Ljava/io/File;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTextureFiles:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTexturesDir:Ljava/io/File;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTextureFiles:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private getMemoryDebugString()Ljava/lang/String;
    .locals 36

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v12}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move-wide/from16 v27, v3

    move v3, v2

    move-wide/from16 v29, v5

    move-wide/from16 v4, v27

    move-wide/from16 v31, v7

    move-wide/from16 v6, v29

    move/from16 v33, v9

    move-wide/from16 v8, v31

    move-wide/from16 v34, v10

    move-wide/from16 v11, v34

    move/from16 v10, v33

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v14

    if-eqz v14, :cond_0

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v11, v14

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getCompressedData()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v14

    if-eqz v14, :cond_1

    const-wide/16 v14, 0x1

    add-long/2addr v6, v14

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getCompressedData()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v14

    invoke-virtual {v14}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v8, v14

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureSizeGuess()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_7

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureSizeGuess()J

    move-result-wide v14

    add-long/2addr v4, v14

    move v2, v3

    move-wide/from16 v27, v4

    move-wide/from16 v3, v27

    :goto_1
    move-wide/from16 v27, v3

    move-wide/from16 v4, v27

    move v3, v2

    goto :goto_0

    :cond_2
    const-wide/16 v19, 0x0

    const/16 v18, 0x0

    const-wide/16 v16, 0x0

    const/4 v15, 0x0

    const-wide/16 v13, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->values()Ljava/util/Collection;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v22

    move-wide/from16 v27, v13

    move v13, v2

    move/from16 v29, v15

    move-wide/from16 v14, v27

    move-wide/from16 v30, v16

    move/from16 v16, v29

    move/from16 v32, v18

    move-wide/from16 v17, v30

    move-wide/from16 v33, v19

    move-wide/from16 v20, v33

    move/from16 v19, v32

    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v23

    if-eqz v23, :cond_3

    add-int/lit8 v19, v19, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    add-long v20, v20, v23

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getCompressedData()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v23

    if-eqz v23, :cond_4

    add-int/lit8 v16, v16, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getCompressedData()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    add-long v17, v17, v23

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureSizeGuess()J

    move-result-wide v23

    const-wide/16 v25, 0x0

    cmp-long v23, v23, v25

    if-lez v23, :cond_6

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureSizeGuess()J

    move-result-wide v23

    add-long v14, v14, v23

    move v2, v13

    move-wide/from16 v27, v14

    move-wide/from16 v13, v27

    :goto_3
    move-wide/from16 v27, v13

    move-wide/from16 v14, v27

    move v13, v2

    goto :goto_2

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v22, " Cached [Raw("

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, "): "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, " bytes, Comp("

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "): "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " bytes, Tex("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes], "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Approx Cached [Raw("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes, Comp("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v17

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes, Tex("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes], Comp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_6
    move v2, v13

    move-wide/from16 v27, v14

    move-wide/from16 v13, v27

    goto/16 :goto_3

    :cond_7
    move v2, v3

    move-wide/from16 v27, v4

    move-wide/from16 v3, v27

    goto/16 :goto_1
.end method

.method private getPinnedTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    return-object v0
.end method

.method private handleLowMemoryOnQueue(Ljava/util/concurrent/ConcurrentLinkedQueue;Z)I
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v3

    const/4 v4, -0x2

    if-eq v3, v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    if-nez p2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-ne v0, v3, :cond_0

    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized isTextureInCache(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-ne p1, v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private layerRequested(I)V
    .locals 8

    const/4 v1, -0x2

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->rebuildTexture()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawTextureId()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getScale()F

    move-result v2

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getOriginalWidth()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getOriginalHeight()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float v7, v0, v2

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeUpdateLayer(IIIIIFF)V

    goto :goto_0

    :cond_2
    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    goto :goto_0
.end method

.method private liveLayerListUpdated(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLiveLayers:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLiveLayers:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private makeSpaceForNewItemIfNecessary(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->maximumCacheSize()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeTextureFromCache()V

    goto :goto_0
.end method

.method private static native nativeDestroy(I)V
.end method

.method private native nativeGetAllAttachedLiveLayers(I)[I
.end method

.method private native nativeInit(Z)I
.end method

.method private native nativeSetSelectedTab(I)V
.end method

.method private native nativeUpdateLayer(IIIIIFF)V
.end method

.method private putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 3

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawData()Landroid/graphics/Bitmap;

    move-result-object v1

    const/high16 v2, 0x3f800000

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;-><init>(Landroid/graphics/Bitmap;F)V

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_0
.end method

.method private putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, -0x2

    if-ne p1, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeFromQueues(I)V

    :cond_2
    :goto_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->compressTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mUseApproximationTextures:Z

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->createApproximation()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_6

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getScale()F

    move-result v1

    invoke-direct {v0, p1, v2, v3, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V

    move-object v1, v0

    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->addIdToVisibleIds(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeFromQueues(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->makeSpaceForNewItemIfNecessary(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_2

    :cond_6
    move-object v1, v0

    goto :goto_3
.end method

.method private putTextureFromBitmap(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;)Z
    .locals 5

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getScale()F

    move-result v3

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->enabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v2, "Tab.thumbnailBitmapData"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "bitmap: (w="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",h="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getHeight()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") scale="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getScale()F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " depth="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v0, v4, :cond_3

    const-string v0, "32"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const-string v0, "16"

    goto :goto_1
.end method

.method private readNextTexture()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$2;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private readNextTexture(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mStackedReads:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture()V

    return-void
.end method

.method private rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V
    .locals 0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->rebuildTexture()V

    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto :goto_0
.end method

.method private registerNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method private removeDuplicateIdsFromQueueHelper(Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    if-ne v0, p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_0

    :cond_2
    return v1
.end method

.method private removeFromQueues(I)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne v0, v2, :cond_1

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v2

    if-ne v2, p1, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne v0, v2, :cond_4

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->rebuildGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method private removeTextureFromCache()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v3, v0}, Ljava/util/ArrayDeque;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v0

    :cond_1
    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v1, v0

    :cond_3
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    :cond_4
    return-void
.end method

.method private shouldCleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$GLOptimizedApproximationTexture;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private unregisterNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method private writeNextTexture()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;->calledOnValidThread()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    const-string v1, "writeNextTexture not called on valid thread"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method private writeTextureIfNecessary(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->dataWriteRequired()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeDuplicateIdsFromQueueHelper(Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueueMaxSize:I

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Write queue is full, dropping thumbnail from queue"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeNextTexture()V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public addTextureChangeListener(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected aggressivelyRebuildTextures()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public cacheTabThumbnail(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getId()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getId()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->clearPendingTextureRead()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->invalidateIfChanged(Lcom/google/android/apps/chrome/TabThumbnailProvider;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->checkAndUpdateThumbnailMetaData(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->createThumbnailGLTexture(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->isTextureValid()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->clearPendingTextureRead()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cleanupPersistentData(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTexturesDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->deleteFileAsync(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public cleanupPersistentDataAtAndAboveId(I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mTexturesDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x2

    if-eq v3, v4, :cond_2

    if-lt v3, p1, :cond_2

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->deleteFileAsync(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public clearPendingTextureRead()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    goto :goto_0
.end method

.method public commitTextureReadback()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->generateThumbnailBitmap()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getId()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected createThumbnailGLTexture(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;-><init>(Lcom/google/android/apps/chrome/TabThumbnailProvider;Lcom/google/android/apps/chrome/gl/GLResourceProvider;)V

    return-object v0
.end method

.method protected deleteCorruptTextureSource(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->deleteFileAsync(I)V

    return-void
.end method

.method public deleteTexturesAndScheduleReload()V
    .locals 4

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupTexture()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->attemptToScheduleRebuildFromData()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupTexture()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->attemptToScheduleRebuildFromData()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPinnedTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->attemptToScheduleRebuildFromData()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture()V

    return-void
.end method

.method public destroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->clearPendingTextureRead()V

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCleanupReference:Lorg/chromium/content/common/CleanupReference;

    invoke-virtual {v0}, Lorg/chromium/content/common/CleanupReference;->cleanupNow()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    :cond_0
    return-void
.end method

.method public getCurrentCacheSize()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v0

    return v0
.end method

.method public getMaximumCacheSize()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->maximumCacheSize()I

    move-result v0

    return v0
.end method

.method public getPendingTextureReadbackId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected getTabBitmap(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
    .locals 1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->isClosing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getBitmap()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    return-object v0
.end method

.method public getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPinnedTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_3

    if-eqz p2, :cond_2

    const/4 v0, -0x2

    if-eq p1, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->addIdToVisibleIds(I)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    :cond_3
    return-object v0
.end method

.method protected getTextureInputStream(I)Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/io/FileInputStream;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getFile(Ljava/lang/Integer;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method protected getTextureOutputStream(I)Ljava/io/OutputStream;
    .locals 2

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getFile(Ljava/lang/Integer;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public handleLowMemory(Z)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->handleLowMemoryOnQueue(Ljava/util/concurrent/ConcurrentLinkedQueue;Z)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->handleLowMemoryOnQueue(Ljava/util/concurrent/ConcurrentLinkedQueue;Z)I

    move-result v4

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, -0x2

    if-ne v6, v7, :cond_0

    sget-boolean v6, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->dropCachedNtpOnLowMemory:Z

    if-eqz v6, :cond_4

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const/4 v5, 0x1

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_2
    if-ge v2, v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeTextureFromCache()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v2, v0

    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Low Memory: Freed "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " textures from compression queue, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from write queue, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " from the pinned list, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from the cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public hasTexturesToCompress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionTaskRunning:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCompressionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTexturesToRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadTaskRunning:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTexturesToWrite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateIfChanged(Lcom/google/android/apps/chrome/TabThumbnailProvider;)V
    .locals 5

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getId()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;

    const-wide/16 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;-><init>(JLjava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->access$200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabThumbnailProvider;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->remove(I)V

    goto :goto_0
.end method

.method public isTextureCached(IZZ)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    :cond_1
    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->textureInputFileExists(I)Z

    move-result v0

    :cond_2
    return v0
.end method

.method protected notifyListenersOfTextureChange(IZ)V
    .locals 8

    const/high16 v6, 0x3f800000

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getRawTextureId()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getScale()F

    move-result v2

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getOriginalWidth()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getOriginalHeight()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTextureHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float v7, v0, v2

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeUpdateLayer(IIIIIFF)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNonThreadSafe:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;->calledOnValidThread()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;

    const-string v1, "notifyListenersOfTextureChange not called on valid thread"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mNativeThumbnailCache:I

    move-object v0, p0

    move v2, p1

    move v4, v3

    move v5, v3

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->nativeUpdateLayer(IIIIIFF)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;->onTextureChange(IZ)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public pinTexture(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->commitTextureReadback()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_1
    return-void
.end method

.method public putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;)V
    .locals 4

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getScale()F

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_0
.end method

.method public putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;Z)V
    .locals 6

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->bitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->getScale()F

    move-result v5

    move v1, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;ZF)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->putTexture(ILcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    goto :goto_0
.end method

.method public remove(I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPendingTextureReadback:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailGLTexture;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->clearPendingTextureRead()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPinnedTexture(I)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    if-eq v1, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mThumbnailMetaData:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->deleteFileAsync(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->removeFromQueues(I)V

    if-nez v2, :cond_3

    if-nez v0, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    :cond_4
    return-void
.end method

.method public removeTextureChangeListener(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$TextureChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setMaximumCacheSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->setMaximumCacheSize(I)V

    return-void
.end method

.method public setStackedReads(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mStackedReads:Z

    return-void
.end method

.method protected textureInputFileExists(I)Z
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getFile(Ljava/lang/Integer;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public unpinAllExcept(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_1

    aget-object v0, v2, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v3, -0x2

    if-eq v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public unpinTexture(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mPinnedTextures:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->notifyListenersOfTextureChange(IZ)V

    :cond_0
    return-void
.end method

.method public updateVisibleIds(Ljava/util/List;)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->maximumCacheSize()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_8

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v2, v4, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_5

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mLastVisibleListIds:Ljava/util/ArrayList;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mVisibleIdsList:Ljava/util/ArrayDeque;

    invoke-virtual {v4, v0}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->maximumCacheSize()I

    move-result v0

    if-ge v1, v0, :cond_7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mReadQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->readNextTexture()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCleanupCPUMemoryRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCleanupCPUMemoryRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_2
.end method
