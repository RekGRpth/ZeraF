.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 6

    const/4 v0, 0x0

    aget-object v1, p1, v0

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTextureOutputStream(I)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    :try_start_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->writeDataToStream(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-object v1

    :catch_0
    move-exception v2

    :try_start_2
    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not open file to write texture for tab "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/StreamUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->doInBackground([Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mWriteTaskRunning:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$802(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->writeNextTexture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$900(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$WriteTextureTask;->onPostExecute(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method
