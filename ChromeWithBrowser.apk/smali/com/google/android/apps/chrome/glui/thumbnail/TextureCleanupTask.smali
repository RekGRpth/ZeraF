.class Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

.field private final mTextureId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/gl/GLResourceProvider;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iput p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->mTextureId:I

    return-void
.end method


# virtual methods
.method public cleanup(Ljava/lang/Integer;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "UI thread expected"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->mTextureId:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->deleteTexture(I)V

    return-void
.end method

.method public bridge synthetic cleanup(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;->cleanup(Ljava/lang/Integer;)V

    return-void
.end method
