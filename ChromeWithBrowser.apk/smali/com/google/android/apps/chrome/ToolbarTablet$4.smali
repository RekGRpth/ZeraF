.class Lcom/google/android/apps/chrome/ToolbarTablet$4;
.super Lcom/google/android/apps/chrome/KeyboardNavigationListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    const v1, 0x7f0f006e

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    return-object v0
.end method
