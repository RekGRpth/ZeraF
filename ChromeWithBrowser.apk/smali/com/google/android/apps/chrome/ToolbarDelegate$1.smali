.class Lcom/google/android/apps/chrome/ToolbarDelegate$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarDelegate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setOverviewMode(Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setOverviewMode(Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabCount()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$100(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabCount()V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabCrash(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$200(Lcom/google/android/apps/chrome/ToolbarDelegate;I)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$300(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabLoadingState(IZZ)V
    invoke-static {v0, v1, v3, v3}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$400(Lcom/google/android/apps/chrome/ToolbarDelegate;IZZ)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->onPageLoadFinished(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$500(Lcom/google/android/apps/chrome/ToolbarDelegate;I)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "progress"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$600(Lcom/google/android/apps/chrome/ToolbarDelegate;II)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabTitle(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$700(Lcom/google/android/apps/chrome/ToolbarDelegate;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabModelSelected(Z)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$800(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$100(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # invokes: Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$100(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onDefaultSearchEngineChanged()V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/ToolbarDelegate;->handleFindToolbarStateChange(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1000(Lcom/google/android/apps/chrome/ToolbarDelegate;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->handleFindToolbarStateChange(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1000(Lcom/google/android/apps/chrome/ToolbarDelegate;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1100(Lcom/google/android/apps/chrome/ToolbarDelegate;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1102(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # getter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1100(Lcom/google/android/apps/chrome/ToolbarDelegate;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    # setter for: Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->access$1102(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;->this$0:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onAccessibilityStatusChanged(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_a
        0xb -> :sswitch_9
        0xc -> :sswitch_b
        0xf -> :sswitch_1
        0x12 -> :sswitch_e
        0x15 -> :sswitch_d
        0x19 -> :sswitch_c
        0x2a -> :sswitch_f
        0x2d -> :sswitch_11
        0x36 -> :sswitch_12
        0x37 -> :sswitch_13
        0x3b -> :sswitch_10
        0x3e -> :sswitch_14
    .end sparse-switch
.end method
