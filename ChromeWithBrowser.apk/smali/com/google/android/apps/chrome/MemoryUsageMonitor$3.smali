.class Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

.field final synthetic val$foregroundPid:I

.field final synthetic val$memoryUsageByRenderer:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/MemoryUsageMonitor;ILjava/util/HashMap;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->this$0:Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    iput p2, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$foregroundPid:I

    iput-object p3, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$memoryUsageByRenderer:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab;)I
    .locals 7

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v3

    if-eq v0, v3, :cond_3

    iget v4, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$foregroundPid:I

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v4, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$foregroundPid:I

    if-ne v3, v4, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$memoryUsageByRenderer:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->val$memoryUsageByRenderer:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v4, v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    if-ge v4, v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getLastShownTimestamp()J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->getLastShownTimestamp()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-gez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    cmp-long v0, v3, v5

    if-lez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/Tab;

    check-cast p2, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/MemoryUsageMonitor$3;->compare(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    return v0
.end method
