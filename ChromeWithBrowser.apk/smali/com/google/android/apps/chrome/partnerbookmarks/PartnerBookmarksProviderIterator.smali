.class public Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;


# static fields
.field private static final BOOKMARKS_COLUMN_FAVICON:Ljava/lang/String; = "favicon"

.field private static final BOOKMARKS_COLUMN_ID:Ljava/lang/String; = "_id"

.field private static final BOOKMARKS_COLUMN_PARENT:Ljava/lang/String; = "parent"

.field private static final BOOKMARKS_COLUMN_TITLE:Ljava/lang/String; = "title"

.field private static final BOOKMARKS_COLUMN_TOUCHICON:Ljava/lang/String; = "touchicon"

.field private static final BOOKMARKS_COLUMN_TYPE:Ljava/lang/String; = "type"

.field private static final BOOKMARKS_COLUMN_URL:Ljava/lang/String; = "url"

.field private static final BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

.field private static final BOOKMARKS_PATH:Ljava/lang/String; = "bookmarks"

.field private static final BOOKMARKS_PROJECTION:[Ljava/lang/String;

.field private static final BOOKMARKS_SORT_ORDER:Ljava/lang/String; = "type DESC, _id ASC"

.field private static final BOOKMARK_CONTAINER_FOLDER_ID:J = 0x0L

.field private static final BOOKMARK_TYPE_FOLDER:I = 0x2

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final PROVIDER_AUTHORITY:Ljava/lang/String; = "com.android.partnerbookmarks"

.field private static final TAG:Ljava/lang/String; = "PartnerBookmarksProviderIterator"


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.partnerbookmarks"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "bookmarks"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "parent"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "favicon"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "touchicon"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->BOOKMARKS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/ContentResolver;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    return-void
.end method

.method public static createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;
    .locals 6

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->BOOKMARKS_PROJECTION:[Ljava/lang/String;

    const-string v5, "type DESC, _id ASC"

    move-object v0, p0

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;-><init>(Landroid/content/ContentResolver;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public static isProviderAvailable(Landroid/content/ContentResolver;)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    iget-wide v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->id:J

    cmp-long v2, v2, v5

    if-nez v2, :cond_2

    const-string v1, "PartnerBookmarksProviderIterator"

    const-string v2, "Dropping the bookmark: reserved _id was used"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "parent"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    iget-wide v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    cmp-long v2, v2, v5

    if-nez v2, :cond_3

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->parentId:J

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "type"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "url"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "title"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "favicon"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->favicon:[B

    iget-object v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string v4, "touchicon"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->touchicon:[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->isFolder:Z

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->url:Ljava/lang/String;

    if-eqz v2, :cond_5

    :cond_4
    iget-object v2, v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->title:Ljava/lang/String;

    if-nez v2, :cond_7

    :cond_5
    const-string v1, "PartnerBookmarksProviderIterator"

    const-string v2, "Dropping the bookmark: no title, or no url on a non-foler"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "PartnerBookmarksProviderIterator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dropping the bookmark: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->next()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
