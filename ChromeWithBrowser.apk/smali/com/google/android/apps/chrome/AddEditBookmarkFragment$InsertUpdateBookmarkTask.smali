.class Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;


# instance fields
.field private mBookmarkId:Ljava/lang/Long;

.field private final mBookmarkValues:Landroid/content/ContentValues;

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Landroid/content/ContentValues;Ljava/lang/Long;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    iput-object p3, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->onSuccess(J)V

    return-void
.end method

.method protected runBackgroundTask()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    :goto_1
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$900(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->setBackEnabled(Z)V

    return-void
.end method
