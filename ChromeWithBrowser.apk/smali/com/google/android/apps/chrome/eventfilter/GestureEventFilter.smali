.class public Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDetector:Landroid/view/GestureDetector;

.field private final mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

.field private mSingleInput:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;)V
    .locals 2

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    iput-object p3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;-><init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    return v0
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v6, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    const/4 v5, 0x5

    if-ne v8, v5, :cond_2

    move v5, v6

    :goto_0
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onPinch(FFFFZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v7}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    if-eq v8, v6, :cond_0

    const/4 v0, 0x3

    if-ne v8, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onUpOrCancel()V

    :cond_1
    return v6

    :cond_2
    move v5, v7

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v6}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    iput-boolean v6, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    goto :goto_1
.end method
