.class public interface abstract Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getView()Landroid/view/View;
.end method

.method public abstract onEndGesture()V
.end method

.method public abstract onStartGesture()V
.end method

.method public abstract propagateEvent(Landroid/view/MotionEvent;)Z
.end method
