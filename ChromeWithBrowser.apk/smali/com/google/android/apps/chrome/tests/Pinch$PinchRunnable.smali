.class Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final MAX_DISTANCE:I = 0x190

.field private static final MIN_DISTANCE:I = 0x28

.field private static final ZOOM_DELAY:I = 0xf

.field private static final ZOOM_IN_STATE:I = 0x1

.field private static final ZOOM_OUT_STATE:I = 0x2


# instance fields
.field private mDistance:I

.field private mPinchAnchorX:I

.field private mPinchAnchorY:I

.field private mState:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/tests/Pinch;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tests/Pinch;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mState:I

    const/16 v0, 0x28

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorX:I

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorY:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const-wide/16 v9, 0xf

    const/16 v8, 0x28

    const/16 v7, 0x190

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    if-ne v0, v8, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, v3, v3}, Lorg/chromium/content/browser/ContentView;->pinchBegin(JII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->startFpsProfiling()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorX:I

    iget v4, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorY:I

    iget v5, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget-object v6, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    # getter for: Lcom/google/android/apps/chrome/tests/Pinch;->mDistanceDelta:I
    invoke-static {v6}, Lcom/google/android/apps/chrome/tests/Pinch;->access$000(Lcom/google/android/apps/chrome/tests/Pinch;)I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/content/browser/ContentView;->pinchBy(JIIF)V

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    # getter for: Lcom/google/android/apps/chrome/tests/Pinch;->mDistanceDelta:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tests/Pinch;->access$000(Lcom/google/android/apps/chrome/tests/Pinch;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    if-ge v0, v7, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0, v9, v10}, Lorg/chromium/content/browser/ContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentView;->pinchEnd(J)V

    const-string v0, "ChromeTest"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ZoomIn fps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->stopFpsProfiling()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mState:I

    iput v7, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, p0, v1, v2}, Lorg/chromium/content/browser/ContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    if-ne v0, v7, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, v3, v3}, Lorg/chromium/content/browser/ContentView;->pinchBegin(JII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->startFpsProfiling()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorX:I

    iget v4, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mPinchAnchorY:I

    iget v5, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget-object v6, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    # getter for: Lcom/google/android/apps/chrome/tests/Pinch;->mDistanceDelta:I
    invoke-static {v6}, Lcom/google/android/apps/chrome/tests/Pinch;->access$000(Lcom/google/android/apps/chrome/tests/Pinch;)I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/content/browser/ContentView;->pinchBy(JIIF)V

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    # getter for: Lcom/google/android/apps/chrome/tests/Pinch;->mDistanceDelta:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tests/Pinch;->access$000(Lcom/google/android/apps/chrome/tests/Pinch;)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    iget v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->mDistance:I

    if-le v0, v8, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, p0, v9, v10}, Lorg/chromium/content/browser/ContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentView;->pinchEnd(J)V

    const-string v0, "ChromeTest"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ZoomOut fps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tests/Pinch$PinchRunnable;->this$0:Lcom/google/android/apps/chrome/tests/Pinch;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tests/Pinch;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->stopFpsProfiling()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
