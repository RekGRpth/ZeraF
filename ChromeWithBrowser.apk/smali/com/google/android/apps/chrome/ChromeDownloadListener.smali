.class Lcom/google/android/apps/chrome/ChromeDownloadListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;
.implements Lorg/chromium/content/browser/ContentViewDownloadDelegate;


# static fields
.field private static final CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern;

.field private static final LOGTAG:Ljava/lang/String; = "ChromeDownloadListener"

.field private static final UNKNOWN_MIME_TYPE:Ljava/lang/String; = "application/unknown"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

.field private final mTab:Lcom/google/android/apps/chrome/Tab;

.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/TabModelSelector;Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/Tab;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    return-void
.end method

.method private alertDownloadFailure(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private checkExternalStorageAndNotify(Ljava/lang/String;)Z
    .locals 4

    const v0, 0x7f0700aa

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v2, "null"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->alertDownloadFailure(I)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "shared"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v0, 0x7f0700ac

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->alertDownloadFailure(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private completePostDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V
    .locals 9

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iget-object v1, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilename:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mDescription:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mMimeType:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilepath:Ljava/lang/String;

    iget-wide v6, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mContentLength:J

    move v8, v3

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "url"

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "path"

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilepath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "success"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0xe

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void
.end method

.method private confirmDangerousDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->getDownloadWarningText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/Tab;

    new-instance v0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/Tab;->addInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    goto :goto_0
.end method

.method private static discardFile(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeDownloadListener$3;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeDownloadListener$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private downloadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private encodePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/16 v7, 0x5d

    const/16 v6, 0x5b

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-char v4, v2, v1

    if-eq v4, v6, :cond_0

    if-ne v4, v7, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    :goto_2
    return-object p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v2

    :goto_3
    if-ge v0, v3, :cond_5

    aget-char v4, v2, v0

    if-eq v4, v6, :cond_3

    if-ne v4, v7, :cond_4

    :cond_3
    const/16 v5, 0x25

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method private enqueueDownloadManagerRequest(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->onNewDownloadStart()V

    iget-object v0, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilename:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    invoke-virtual {v1}, Landroid/app/DownloadManager$Request;->allowScanningByMediaScanner()V

    iget-object v0, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    const-string v0, "Cookie"

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mCookie:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    const-string v0, "Referer"

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mReferer:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    const-string v0, "User-Agent"

    iget-object v2, p1, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string v2, "download"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    new-instance v2, Lcom/google/android/apps/chrome/ChromeDownloadListener$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeDownloadListener$2;-><init>(Lcom/google/android/apps/chrome/ChromeDownloadListener;Landroid/app/DownloadManager;Landroid/app/DownloadManager$Request;)V

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0700ae

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v3, 0x2e

    invoke-static {p0, p2, p1}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static isDangerousExtension(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "apk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->isDangerousExtension(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->isDangerousExtension(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onNewDownloadStart()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isInitialNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0700af

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private static parseContentDisposition(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "text/plain"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/octet-stream"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->parseContentDisposition(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    move-object p1, v0

    :cond_2
    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move-object p0, v0

    :cond_3
    return-object p0
.end method

.method private saveDataUri(Ljava/lang/String;)V
    .locals 6

    :try_start_0
    new-instance v3, Lcom/google/android/apps/chrome/third_party/DataUri;

    invoke-direct {v3, p1}, Lcom/google/android/apps/chrome/third_party/DataUri;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd-HH-mm-ss-"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/DataUri;->getMimeType()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->downloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->checkExternalStorageAndNotify(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/DownloadManager;

    new-instance v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromeDownloadListener$1;-><init>(Lcom/google/android/apps/chrome/ChromeDownloadListener;Ljava/lang/String;Lcom/google/android/apps/chrome/third_party/DataUri;Landroid/app/DownloadManager;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeDownloadListener$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ChromeDownloadListener"

    const-string v2, "Invalid data url "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public onConfirmInfoBarButtonClicked(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;Z)V
    .locals 1

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mIsGETRequest:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->enqueueDownloadManagerRequest(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->dismiss()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->completePostDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mIsGETRequest:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilepath:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->discardFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method onDownloadStartNoStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-static {p1, v5, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->downloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->checkExternalStorageAndNotify(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/DataUri;->isDataUri(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->saveDataUri(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v4, Lcom/google/android/apps/chrome/third_party/WebAddress;

    invoke-direct {v4, p1}, Lcom/google/android/apps/chrome/third_party/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/WebAddress;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->encodePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/third_party/WebAddress;->setPath(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/WebAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const-wide/16 v11, 0x0

    const/4 v13, 0x1

    move-object/from16 v4, p2

    move-object/from16 v6, p5

    move-object/from16 v10, p6

    invoke-direct/range {v2 .. v13}, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    const-string v3, "application/x-shockwave-flash"

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v7, v5}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->confirmDangerousDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v2, "ChromeDownloadListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception trying to parse url: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->enqueueDownloadManagerRequest(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    goto :goto_0
.end method

.method public onHttpPostDownloadCompleted(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 12

    if-eqz p6, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "ChromeDownloadListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Download failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v3, "application/unknown"

    :goto_1
    new-instance v6, Lcom/google/android/apps/chrome/third_party/WebAddress;

    invoke-direct {v6, p1}, Lcom/google/android/apps/chrome/third_party/WebAddress;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/third_party/WebAddress;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/third_party/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v11, 0x0

    move-object v7, p3

    move-wide/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    invoke-static {v5, v3}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->confirmDangerousDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->completePostDownload(Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;)V

    goto :goto_0

    :cond_3
    move-object v3, p2

    goto :goto_1
.end method

.method public onHttpPostDownloadStarted()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->onNewDownloadStart()V

    return-void
.end method

.method public onInfoBarDismissed(Lcom/google/android/apps/chrome/infobar/InfoBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mIsGETRequest:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilepath:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->discardFile(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mPendingRequest:Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;

    return-void
.end method

.method public requestHttpGetDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    const-string v3, "attachment"

    const/16 v5, 0xa

    move-object v0, p3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ChromeDownloadListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activity not found for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " over "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/apps/chrome/ChromeDownloadListener;->onDownloadStartNoStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
