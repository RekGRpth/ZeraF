.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v2, "Chrome_SnapshotID"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v2, "Chrome_SnapshotURI"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v4, "Chrome_SnapshotState"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v4

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v2, v0

    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v5

    if-eqz v5, :cond_0

    move v0, v1

    :goto_1
    invoke-interface {v5}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_0

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v6

    invoke-virtual {v6, v3, v2, v4}, Lcom/google/android/apps/chrome/Tab;->snapshotStateQueryResult(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v1, 0x1

    :cond_0
    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v1, "Chrome_ErrorMessage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v2, "Chrome_DocumentID"

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string v3, "Chrome_ErrorMessage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
