.class Lcom/google/android/apps/chrome/SuggestionView$PerformSelectSuggestion;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/SuggestionView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/SuggestionView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/SuggestionView;Lcom/google/android/apps/chrome/SuggestionView$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/SuggestionView$PerformSelectSuggestion;-><init>(Lcom/google/android/apps/chrome/SuggestionView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSelectionHandler:Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SuggestionView;->access$600(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/SuggestionView;->mSuggestion:Lcom/google/android/apps/chrome/OmniboxSuggestion;
    invoke-static {v1}, Lcom/google/android/apps/chrome/SuggestionView;->access$300(Lcom/google/android/apps/chrome/SuggestionView;)Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OmniboxPopupAdapter$OmniboxSuggestionSelectionHandler;->onSelection(Lcom/google/android/apps/chrome/OmniboxSuggestion;)V

    return-void
.end method
