.class public Lcom/google/android/apps/chrome/SendGoogleFeedback;
.super Landroid/app/Fragment;


# static fields
.field protected static final LOG_TAG:Ljava/lang/String; = "SendGoogleFeedback"

.field private static sCurrentScreenshot:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/SendGoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method private static LogFeedbackExtraInfo()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isSpdyProxyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SendGoogleFeedback"

    const-string v1, "SPDY proxy ON before sending feedback"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "SendGoogleFeedback"

    const-string v1, "SPDY proxy OFF before sending feedback"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCurrentScreenshot()Landroid/graphics/Bitmap;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/SendGoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static launchGoogleFeedback(Landroid/content/Context;)V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->LogFeedbackExtraInfo()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/chrome/SendGoogleFeedback$1;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/SendGoogleFeedback$1;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public static setCurrentScreenshot(Landroid/graphics/Bitmap;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/SendGoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->launchGoogleFeedback(Landroid/content/Context;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceActivity;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SendGoogleFeedback;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
