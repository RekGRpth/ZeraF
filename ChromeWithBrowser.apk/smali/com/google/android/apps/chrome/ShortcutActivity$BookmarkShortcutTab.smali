.class Lcom/google/android/apps/chrome/ShortcutActivity$BookmarkShortcutTab;
.super Lcom/google/android/apps/chrome/Tab;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ShortcutActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ShortcutActivity;Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/apps/chrome/ShortcutActivity$BookmarkShortcutTab;->this$0:Lcom/google/android/apps/chrome/ShortcutActivity;

    const/4 v2, 0x0

    # getter for: Lcom/google/android/apps/chrome/ShortcutActivity;->mActivityNativeWindow:Lorg/chromium/ui/gfx/ActivityNativeWindow;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ShortcutActivity;->access$000(Lcom/google/android/apps/chrome/ShortcutActivity;)Lorg/chromium/ui/gfx/ActivityNativeWindow;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Tab;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V

    return-void
.end method


# virtual methods
.method protected installBookmarkShortcut(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity$BookmarkShortcutTab;->this$0:Lcom/google/android/apps/chrome/ShortcutActivity;

    # invokes: Lcom/google/android/apps/chrome/ShortcutActivity;->onShortcutAdded(Landroid/content/Intent;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ShortcutActivity;->access$100(Lcom/google/android/apps/chrome/ShortcutActivity;Landroid/content/Intent;)V

    return-void
.end method
