.class public Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static final TOKEN_AUTH_URL:Landroid/net/Uri;

.field private static sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActivity:Landroid/app/Activity;

.field private mLsid:Ljava/lang/String;

.field private mRetried:Z

.field private mSid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    const-string v0, "https://www.google.com/accounts/IssueAuthToken?service=gaia&Session=false"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

    const-string v0, "https://www.google.com/accounts/TokenAuth"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TOKEN_AUTH_URL:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mSid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->getAutoLoginLSID()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mLsid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->doLogin()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->nativeInit()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->nativeLogIn(ILjava/lang/String;)V

    return-void
.end method

.method private static createHttpClientFactory(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    :cond_0
    return-void
.end method

.method private doLogin()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mSid:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mLsid:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "auto-login"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private getAutoLoginLSID()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mAccount:Landroid/accounts/Account;

    const-string v3, "LSID"

    new-instance v4, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$2;-><init>(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    return-void
.end method

.method private getAutoLoginSID()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mAccount:Landroid/accounts/Account;

    const-string v3, "SID"

    new-instance v4, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$1;-><init>(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    return-void
.end method

.method private native nativeInit()I
.end method

.method private native nativeLogIn(ILjava/lang/String;)V
.end method

.method private retry()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mSid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "com.google"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mSid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mLsid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "com.google"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mLsid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mRetried:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mRetried:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->start()V

    :cond_2
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    sget-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "SID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mSid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "LSID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mLsid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->createHttpClientFactory(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v1

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "LOGIN_FAIL: Bad status from auth url "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x193

    if-ne v3, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    const-string v3, "LOGIN_FAIL: Invalidating tokens..."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->retry()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    const-string v3, "LOGIN_FAIL: Null entity in response"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v3, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "LOGIN_FAIL: Exception acquiring uber token "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->abort()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    throw v0

    :cond_2
    :try_start_3
    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TAG:Ljava/lang/String;

    const-string v4, "LOGIN_SUCCESS"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    sget-object v1, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->TOKEN_AUTH_URL:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "ChromiumBrowser"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "auth"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "continue"

    const-string v2, "http://www.google.com"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->mActivity:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$3;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin$3;-><init>(Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public start()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->getAutoLoginSID()V

    return-void
.end method
