.class public Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;
.super Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String; = "ChromeSyncInvalidationListener"


# instance fields
.field mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

.field protected mHandler:Landroid/os/Handler;

.field private final mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mModelTypesToRegister:Ljava/util/Set;

.field protected mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stop()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->start(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;

    return-object p1
.end method

.method static getClientKey(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "chromesync#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;-><init>(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$1;)V

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$102(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mBundle:Landroid/os/Bundle;
    invoke-static {v0, p4}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$202(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Landroid/os/Bundle;)Landroid/os/Bundle;

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;
    invoke-static {v0, p5}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$302(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lorg/chromium/sync/internal_api/pub/base/ModelType;)Lorg/chromium/sync/internal_api/pub/base/ModelType;

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$402(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mRegistrationState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;
    invoke-static {v0, p6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$502(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->access$602(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;)Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not posting message of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to dead handler"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private postShutdown()V
    .locals 7

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_SHUTDOWN:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method private postStop()V
    .locals 7

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_STOP:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method private static prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;
    .locals 2

    instance-of v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ClientKey = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->getClientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-nez p0, :cond_1

    const-string v0, "null"

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private start(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-nez p1, :cond_3

    move-object v0, v1

    :goto_0
    check-cast v0, Landroid/accounts/Account;

    if-nez p1, :cond_4

    :goto_1
    new-instance v2, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v2, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_1

    if-nez v1, :cond_8

    :cond_1
    invoke-virtual {v2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedTypes()Ljava/util/Set;

    move-result-object v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_6

    :cond_2
    const-string v2, "ChromeSyncInvalidationListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No account {"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_5

    const-string v0, "null"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "} or no saved sync types {"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return-void

    :cond_3
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v1, "registered_types"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v0, "exists"

    goto :goto_2

    :cond_6
    const-string v2, "ChromeSyncInvalidationListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loaded: enabled types = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", account client key = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getClientKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->isChromeInForeground()Z

    move-result v3

    if-eqz v2, :cond_7

    if-nez v3, :cond_9

    :cond_7
    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Not starting invalidation. syncEnabled = "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inForeground = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stopSelfInternal()V

    goto :goto_3

    :cond_8
    invoke-virtual {v2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setAccount(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Landroid/accounts/Account;)V

    invoke-virtual {v2, v3, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setSyncTypes(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    const-string v2, "ChromeSyncInvalidationListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Saved: enabled types = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", account client key = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getClientKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->getClientKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getClientKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stop()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->createClient(Landroid/accounts/Account;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->start()V

    :cond_b
    invoke-static {v1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    if-nez v1, :cond_c

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;

    goto/16 :goto_3

    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->setRegisteredTypes(Ljava/util/Set;)V

    goto/16 :goto_3
.end method

.method private stop()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->stop()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    if-eqz v0, :cond_2

    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stopping cache invalidation client: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->stop()V

    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Releasing cache invalidation client: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->release()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    :cond_2
    return-void
.end method


# virtual methods
.method protected createClient(Landroid/accounts/Account;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;
    .locals 6

    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getClientKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    move-result v2

    const-string v4, "chromiumsync"

    const-class v5, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    move-object v0, p0

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->create(Landroid/content/Context;Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-result-object v0

    return-object v0
.end method

.method protected doRequestSync(Landroid/os/Bundle;)V
    .locals 2

    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 2

    const-string v0, "ChromeSyncInvalidationListener"

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p2}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->fromObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t interpret model type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_FAILURE:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto :goto_0
.end method

.method public informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p2}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->fromObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t interpret model type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REGISTRATION_STATUS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto :goto_0
.end method

.method public invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 7

    const/4 v5, 0x0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    if-eqz p2, :cond_1

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, "objectId"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getVersion()J

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "payload"

    new-instance v1, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void

    :cond_0
    const-string v0, "payload"

    const-string v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "ChromeSyncInvalidationListener"

    const-string v1, "Received invalidate without invalidation information. Treating invalidation as invalidateAll"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 7

    const/4 v5, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method public invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 7

    const/4 v5, 0x0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, "ChromeSyncInvalidationListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received invalidateUnknownVersion for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in ChromeSyncInvalidationListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "objectId"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "version"

    const-wide/16 v1, 0x0

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "payload"

    const-string v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REQUEST_SYNC:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method isChromeInForeground()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->isChromeInForeground()Z

    move-result v0

    return v0
.end method

.method isSyncEnabled(Landroid/accounts/Account;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->onCreate()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "chrome-sync-invalidation-looper"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->onDestroy()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postShutdown()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "stop"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postStop()V

    :goto_1
    const/4 v0, 0x2

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postStart(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method postStart(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_START:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v3, v2

    move-object v4, p1

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method public ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V
    .locals 7

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_READY:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method protected receivedWrongClient()V
    .locals 0

    return-void
.end method

.method public reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V
    .locals 7

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_REISSUE_REGISTRATIONS:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->postInvalidationRunnable(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;Landroid/os/Bundle;Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    return-void
.end method

.method protected stopSelfInternal()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stopSelf()V

    return-void
.end method
