.class Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;

.field final synthetic val$passphrase:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;->val$passphrase:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;->val$passphrase:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->access$000(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->access$100(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
