.class Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

.field private mBundle:Landroid/os/Bundle;

.field private mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field private mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

.field private mRegistrationState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

.field private mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;-><init>(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/types/AckHandle;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mBundle:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lorg/chromium/sync/internal_api/pub/base/ModelType;)Lorg/chromium/sync/internal_api/pub/base/ModelType;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mRegistrationState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;)Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    return-object p1
.end method

.method private isProvidedClientSameAsCurrent()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    instance-of v0, v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->getClientKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->getClientKey()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->getClientKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    if-ne v0, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    sget-object v1, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->MSG_START:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->isProvidedClientSameAsCurrent()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ChromeSyncInvalidationListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provided with wrong client when trying to handle: type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", provided client = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    # invokes: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$700(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", stored client = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v2, v2, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    # invokes: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->prettyPrint(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$700(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->receivedWrongClient()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$1;->$SwitchMap$com$google$android$apps$chrome$sync$ChromeSyncInvalidationListener$MessageType:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mType:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mProvidedClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mAckHandle:Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mBundle:Landroid/os/Bundle;

    # invokes: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->start(Landroid/os/Bundle;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$800(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    new-instance v1, Lcom/google/android/apps/chrome/sync/RegistrationManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v2, v2, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mClient:Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v3, v3, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/sync/RegistrationManager;-><init>(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Landroid/os/Handler;)V

    iput-object v1, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    # getter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$900(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    # getter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$900(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->setRegisteredTypes(Ljava/util/Set;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mModelTypesToRegister:Ljava/util/Set;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$902(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;Ljava/util/Set;)Ljava/util/Set;

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->reissueRegistrations()V

    goto :goto_1

    :cond_3
    const-string v0, "ChromeSyncInvalidationListener"

    const-string v1, "Ignoring reissueRegistrations, haven\'t received ready"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mRegistrationState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->informRegistrationStatus(Lorg/chromium/sync/internal_api/pub/base/ModelType;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mRegistrationManager:Lcom/google/android/apps/chrome/sync/RegistrationManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/RegistrationManager;->informRegistrationFailure(Lorg/chromium/sync/internal_api/pub/base/ModelType;)V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->doRequestSync(Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    # invokes: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stop()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$1000(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stopSelfInternal()V

    goto :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    # invokes: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->stop()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$1000(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    # getter for: Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandlerThreadIsAlive:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->access$1100(Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener$InvalidationRunnable;->this$0:Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
