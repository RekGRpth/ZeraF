.class Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

.field final synthetic val$application:Lcom/google/android/apps/chrome/ChromeMobileApplication;

.field final synthetic val$objectId:Ljava/lang/String;

.field final synthetic val$payload:Ljava/lang/String;

.field final synthetic val$version:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;Lcom/google/android/apps/chrome/ChromeMobileApplication;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->this$0:Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$application:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$objectId:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$version:J

    iput-object p6, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$payload:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$application:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$application:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$objectId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$version:J

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;->val$payload:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->requestSyncFromNativeChrome(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ChromeBrowserSyncAdapter"

    const-string v2, "Failed to start browser processs."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
