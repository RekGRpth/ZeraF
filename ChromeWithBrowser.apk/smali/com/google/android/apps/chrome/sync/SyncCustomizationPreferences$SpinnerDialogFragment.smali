.class public Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closePage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1600(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-object v0
.end method
