.class public Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
.super Ljava/lang/Object;


# instance fields
.field private mTempAutoLogin:Ljava/lang/Boolean;

.field private mTempMasterSyncState:Ljava/lang/Boolean;

.field private mTempSendToDevice:Ljava/lang/Boolean;

.field private mTempSync:Ljava/lang/Boolean;

.field private mTempWantedSyncState:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public autoLogin(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempAutoLogin:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/google/android/apps/chrome/sync/SyncStates;
    .locals 7

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncStates;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempSync:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempWantedSyncState:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempMasterSyncState:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempSendToDevice:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempAutoLogin:Ljava/lang/Boolean;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/SyncStates;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/chrome/sync/SyncStates$1;)V

    return-object v0
.end method

.method public masterSyncState(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempMasterSyncState:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sendToDevice(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempSendToDevice:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempSync:Ljava/lang/Boolean;

    return-object p0
.end method

.method public wantedSyncState(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->mTempWantedSyncState:Ljava/lang/Boolean;

    return-object p0
.end method
