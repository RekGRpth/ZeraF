.class public Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mConfirmPassphrase:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V

    return-void
.end method

.method private validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 4

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const v0, 0x7f07009b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const v0, 0x7f07009c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;->onPassphraseEntered(Ljava/lang/String;ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->dismiss()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040032

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0f00b1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f0f00b2

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$4;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment$4;-><init>(Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070079

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070073

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070072

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
