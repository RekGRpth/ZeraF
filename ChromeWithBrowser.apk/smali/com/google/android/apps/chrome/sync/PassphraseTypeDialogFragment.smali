.class public Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final ARG_ALLOWED_TYPES:Ljava/lang/String; = "arg_allowed_types"

.field public static final ARG_CURRENT_TYPE:Ljava/lang/String; = "arg_current_type"

.field private static final ARG_MIGRATED:Ljava/lang/String; = "arg_migrated"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getAllowedTypesFromArguments()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static create(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/util/ArrayList;Z)Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "arg_current_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "arg_allowed_types"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "arg_migrated"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private createAdapter(Z)Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V

    new-instance v1, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    sget-object v2, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const v3, 0x7f070079

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V

    if-eqz p1, :cond_0

    new-instance v2, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    sget-object v3, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->KEYSTORE_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const v4, 0x7f070078

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->add(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->add(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V

    :goto_0
    new-instance v1, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getDisplayNames()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2, v5}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V

    return-object v1

    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    sget-object v3, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    const v4, 0x7f070076

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$1;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->add(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->add(Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V

    goto :goto_0
.end method

.method private getAllowedTypesFromArguments()Ljava/util/ArrayList;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "arg_allowed_types"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "Unable to find argument with allowed types. Returning empty list."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method private getCurrentTypeFromArguments()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "arg_current_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "Unable to find argument with current type. Setting to INVALID."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->INVALID:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040033

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f00b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "arg_migrated"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->createAdapter(Z)Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Adapter;->getPositionForType(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070074

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f070075

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getAllowedTypesFromArguments()Ljava/util/ArrayList;

    move-result-object v1

    long-to-int v2, p4

    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->fromInternalValue(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p4, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Listener;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Listener;->onPassphraseTypeSelected(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->dismiss()V

    :cond_1
    return-void
.end method
