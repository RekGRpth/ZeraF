.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

.field final synthetic val$passphrase:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;->val$passphrase:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v2, "booleanResult"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    const-string v1, "SyncCustomizationPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GAIA password valid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;->val$passphrase:Ljava/lang/String;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureEncryption(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1800(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;Z)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v2, "SyncCustomizationPreferences"

    const-string v3, "unable to verify password"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->notifyInvalidPassphrase()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1900(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    goto :goto_1
.end method
