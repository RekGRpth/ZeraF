.class Lcom/google/android/apps/chrome/sync/RegistrationStatus;
.super Ljava/lang/Object;


# static fields
.field static final INITIAL_REGISTRATION_DELAY_SECONDS:I = 0x5

.field static final MAX_REGISTRATION_DELAY_SECONDS:I = 0xe10

.field private static final TAG:Ljava/lang/String; = "RegistrationStatus"


# instance fields
.field private mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

.field private mDelay:I

.field private mDelayGenerator:Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;

.field private mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

.field private mHandler:Landroid/os/Handler;

.field private mInvalidationClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

.field private mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field private mNextDelay:I

.field mRandom:Ljava/util/Random;

.field private mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Landroid/os/Handler;Lorg/chromium/sync/internal_api/pub/base/ModelType;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRandom:Ljava/util/Random;

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mInvalidationClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/RegistrationStatus;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->doRegisterOrUnregister(Z)V

    return-void
.end method

.method private doRegisterOrUnregister(Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    iput v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mNextDelay:I

    new-instance v0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRandom:Ljava/util/Random;

    const/16 v2, 0xe10

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;-><init>(Ljava/util/Random;II)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelayGenerator:Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    sget-object v1, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mInvalidationClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->toObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mInvalidationClient:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->toObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    goto :goto_0
.end method


# virtual methods
.method firePendingRegistrationForTest()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;->run()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    :cond_0
    return-void
.end method

.method getDelay()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    return v0
.end method

.method getModelType()Lorg/chromium/sync/internal_api/pub/base/ModelType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    return-object v0
.end method

.method informRegistrationFailure()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->stop()V

    iget v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mNextDelay:I

    iput v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelayGenerator:Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->getNextDelayMs()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mNextDelay:I

    const-string v0, "RegistrationStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registering "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mModelType:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;-><init>(Lcom/google/android/apps/chrome/sync/RegistrationStatus;Lcom/google/android/apps/chrome/sync/RegistrationStatus$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDelay:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->doRegisterOrUnregister(Z)V

    :cond_0
    return-void
.end method

.method isPending(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isRegistered(Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method registerType()V
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->doRegisterOrUnregister(Z)V

    return-void
.end method

.method reissueRegistration()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mCurrentState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->informRegistrationFailure()V

    return-void
.end method

.method stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mRegistrationCallback:Lcom/google/android/apps/chrome/sync/RegistrationStatus$RegistrationCallback;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method unRegisterType()V
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->UNREGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->mDesiredState:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/RegistrationStatus;->doRegisterOrUnregister(Z)V

    return-void
.end method
