.class Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/PassphraseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public syncStateChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    # invokes: Lcom/google/android/apps/chrome/sync/PassphraseActivity;->removeSyncStateChangedListener()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->access$000(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;->this$0:Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    # invokes: Lcom/google/android/apps/chrome/sync/PassphraseActivity;->displayPasswordDialog()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->access$100(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V

    :cond_0
    return-void
.end method
