.class Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/GpuProfiler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/GpuProfiler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/GpuProfiler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GPU_PROFILER_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "file"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/GpuProfiler;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/GpuProfiler;->startProfiling(Ljava/lang/String;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/GpuProfiler;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/GpuProfiler;->startProfiling(Z)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GPU_PROFILER_STOP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/GpuProfiler$ProfilerBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/GpuProfiler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/GpuProfiler;->stopProfiling()V

    goto :goto_0

    :cond_2
    const-string v0, "GpuProfiler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
