.class Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;
.implements Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;


# instance fields
.field private final mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

.field private mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mUpdateListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

.field private final mWidgetId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput p2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iget v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    # invokes: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService;->getWidgetState(Landroid/content/Context;I)Landroid/content/SharedPreferences;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService;->access$000(Landroid/content/Context;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;)Lcom/google/android/apps/chrome/ChromeMobileApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    return-object v0
.end method

.method private getBookmarkForPosition(I)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    goto :goto_0
.end method

.method private static getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private loadBookmarkFolder(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 7

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BookmarkThumbnailWidgetService"

    const-string v1, "Trying to load bookmark folder from the UI thread."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->bookmarkNodeExists(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J

    move-result-wide v0

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v5, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->bookmarkNodeExists(Landroid/content/Context;J)Z

    move-result v5

    if-nez v5, :cond_1

    move-wide v0, v2

    :cond_1
    :goto_2
    cmp-long v5, v0, v2

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v5}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v5

    invoke-virtual {v5}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v5, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->isBookmarkInMobileBookmarksBranch(Landroid/content/Context;J)Z

    move-result v5

    if-nez v5, :cond_2

    move-wide v0, v2

    :cond_2
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-ltz v5, :cond_3

    cmp-long v5, v0, v2

    if-nez v5, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getMobileBookmarksFolderId(Landroid/content/Context;)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    move-object v0, v4

    goto :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/16 v3, 0xf

    invoke-static {v2, v0, v1, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getBookmarkNode(Landroid/content/Context;JI)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-wide v0, p1

    goto :goto_2
.end method

.method private syncState()V
    .locals 5

    const-wide/16 v3, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "current_folder"

    invoke-interface {v0, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->loadBookmarkFolder(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "current_folder"

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-static {v2}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getCount()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getBookmarkForPosition(I)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040008

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 11

    const/4 v10, 0x0

    const/4 v2, 0x0

    const v9, 0x7f0f002f

    const v8, 0x7f0f002e

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez v0, :cond_0

    const-string v0, "BookmarkThumbnailWidgetService"

    const-string v1, "No current folder data available."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getBookmarkForPosition(I)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v0, "BookmarkThumbnailWidgetService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t get bookmark for position "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-ne v5, v0, :cond_2

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "BookmarkThumbnailWidgetService"

    const-string v1, "Invalid bookmark data: loop detected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-ne v5, v0, :cond_4

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_1
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v6, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f040009

    invoke-direct {v2, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    :goto_2
    const v6, 0x7f0f0030

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v3, v4

    :cond_3
    invoke-virtual {v2, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-ne v5, v3, :cond_6

    const v3, 0x7f0200b9

    :goto_3
    invoke-virtual {v2, v8, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v3, 0x7f020077

    invoke-virtual {v2, v9, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_4
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v3

    if-nez v3, :cond_a

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v4}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService;->getChangeFolderAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    :goto_5
    const v1, 0x7f0f002d

    invoke-virtual {v2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    goto :goto_1

    :cond_5
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v6, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f040008

    invoke-direct {v2, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_2

    :cond_6
    const v3, 0x7f0200ba

    goto :goto_3

    :cond_7
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v6, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->favicon()[B

    move-result-object v6

    if-eqz v6, :cond_8

    array-length v7, v6

    if-lez v7, :cond_8

    array-length v7, v6

    invoke-static {v6, v10, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v9, v6}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :goto_6
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->thumbnail()[B

    move-result-object v6

    if-eqz v6, :cond_9

    array-length v7, v6

    if-lez v7, :cond_9

    array-length v7, v6

    invoke-static {v6, v10, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_4

    :cond_8
    const v6, 0x7f020075

    invoke-virtual {v2, v9, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_6

    :cond_9
    const v3, 0x7f020020

    invoke-virtual {v2, v8, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_4

    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5

    :cond_b
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_5
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onBookmarkModelUpdated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->refreshWidget()V

    return-void
.end method

.method public onCreate()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mUpdateListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BookmarkThumbnailWidgetService"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onDataSetChanged()V
    .locals 2

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->syncState()V

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mUpdateListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mUpdateListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->destroy()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iget v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService;->deleteWidgetState(Landroid/content/Context;I)V

    return-void
.end method

.method public onSyncEnabledStatusUpdated(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-static {v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->getFolderId(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)J

    move-result-wide v1

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;-><init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;ZJ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onThumbnailUpdated(Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mCurrentFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->refreshWidget()V

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method refreshWidget()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v2}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetProvider;->getBookmarkAppWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const-class v5, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetProvider;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method requestFolderChange(J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v2}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService;->getChangeFolderAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const-class v3, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetProxy;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
