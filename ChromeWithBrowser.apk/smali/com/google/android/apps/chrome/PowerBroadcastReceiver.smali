.class public Lcom/google/android/apps/chrome/PowerBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mIsRegistered:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mNeedToRunActions:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mPowerManagerHelper:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

.field private mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mNeedToRunActions:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mIsRegistered:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    new-instance v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mPowerManagerHelper:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

    return-void
.end method


# virtual methods
.method public isRegistered()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mIsRegistered:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->isChromeInForeground(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->runActions(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method registerReceiver(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mIsRegistered:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mNeedToRunActions:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_1
    return-void
.end method

.method runActions(Landroid/content/Context;Z)V
    .locals 4

    sget-boolean v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mPowerManagerHelper:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mPowerManagerHelper:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mNeedToRunActions:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->unregisterReceiver(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    iget-object v2, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;->delayToRun()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    return-void
.end method

.method setPowerManagerHelperForTests(Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mPowerManagerHelper:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$PowerManagerHelper;

    return-void
.end method

.method setServiceRunnableForTests(Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    return-void
.end method

.method unregisterReceiver(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mServiceRunnable:Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mIsRegistered:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->mNeedToRunActions:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_0
    return-void
.end method
