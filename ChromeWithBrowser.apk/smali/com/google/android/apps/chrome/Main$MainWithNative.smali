.class Lcom/google/android/apps/chrome/Main$MainWithNative;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;Lcom/google/android/apps/chrome/Main$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/Main$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;-><init>(Lcom/google/android/apps/chrome/Main;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/Main$MainWithNative;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->onPause()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onDestroy()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$3600(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/Main$MainWithNative;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/Main$MainWithNative;IILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/Main$MainWithNative;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->onResume()V

    return-void
.end method

.method static synthetic access$5300(Lcom/google/android/apps/chrome/Main$MainWithNative;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->updateOrientation()V

    return-void
.end method

.method private clearOrLoadPreviousState(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/Tab;->restoreEncryptionKey(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->clearEncryptedState()V

    :cond_1
    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "no-restore-state"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->clearState()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3000(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->loadState()V

    goto :goto_0
.end method

.method private createNewTab(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$6102(Lcom/google/android/apps/chrome/Main;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$2800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/IntentHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/IntentHandler;->shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$2800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/IntentHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/IntentHandler;->onNewIntent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # setter for: Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/Main;->access$6102(Lcom/google/android/apps/chrome/Main;Z)Z

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "homepage"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "chrome://newtab/"

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/TabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method

.method private createSearchEngineListener()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->setDefaultCountryCodeForSearchEngineLocalization()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6000(Lcom/google/android/apps/chrome/Main;)V

    new-instance v0, Lcom/google/android/apps/chrome/Main$UpdateSearchEngineFromListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/Main$UpdateSearchEngineFromListener;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->registerTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V

    return-void
.end method

.method private handleDebugIntent(Landroid/content/Intent;Lcom/google/android/apps/chrome/Tab;)V
    .locals 6

    const-string v0, "com.google.android.apps.chrome.ACTION_CLOSE_TABS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getAllModels()[Lcom/google/android/apps/chrome/TabModel;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    invoke-interface {v4}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    invoke-interface {v4, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v5

    if-eq v5, p2, :cond_0

    invoke-interface {v4, v0}, Lcom/google/android/apps/chrome/TabModel;->closeTabByIndex(I)Z

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.apps.chrome.ACTION_LOW_MEMORY"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->onLowMemory()V

    :cond_3
    :goto_2
    return-void

    :cond_4
    const-string v0, "com.google.android.apps.chrome.ACTION_TRIM_MEMORY"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Main;->onTrimMemory(I)V

    goto :goto_2
.end method

.method private initializeUI()V
    .locals 10

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;-><init>()V

    # setter for: Lcom/google/android/apps/chrome/Main;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$4402(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;)Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v4

    const-string v0, "preload-webview-container"

    invoke-virtual {v4, v0}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIsTablet:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4500(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v2, 0x7f0f0077

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->setPreloadWebViewContainer(Landroid/view/ViewGroup;)V

    :cond_1
    new-instance v0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {v0, v1}, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;-><init>(Landroid/app/Activity;)V

    invoke-static {v0}, Lorg/chromium/content/browser/ContentVideoView;->registerContentVideoViewContextDelegate(Lorg/chromium/content/browser/ContentVideoViewContextDelegate;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onNativeLibraryReady()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getRestoredTabCount()I

    move-result v0

    if-gt v0, v6, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onStateRestored()V

    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$2;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$3;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$4;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/LocationBar;->setIgnoreURLBarModification(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Main$MainWithNative$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$5;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->registerChangeListener(Lcom/google/android/apps/chrome/TabModelSelector$ChangeListener;)V

    iget-object v9, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/google/android/apps/chrome/Main;->access$4900(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;

    move-result-object v2

    const-string v3, "enable-fullscreen"

    invoke-virtual {v4, v3}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v3

    const-string v5, "enable-persistent-fullscreen"

    invoke-virtual {v4, v5}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v5}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;-><init>(Landroid/view/Window;Landroid/view/ViewGroup;ZZLcom/google/android/apps/chrome/TabModelSelector;)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v9, v0}, Lcom/google/android/apps/chrome/Main;->access$4302(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/ContentViewHolder;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->setFullscreenHandler(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/TabsView;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->setLayoutManager(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getRestoredTabCount()I

    move-result v0

    if-gt v0, v6, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/apps/chrome/ContentViewHolder;->enableTabSwiping(Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v2, 0x7f0f0084

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/Main;->mMenuAnchor:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$5102(Lcom/google/android/apps/chrome/Main;Landroid/view/View;)Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->updateOrientation()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/Main$MainWithNative$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$6;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ContentViewHolder;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/PageBackgroundColorDrawable;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v8, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;
    invoke-static {v2}, Lcom/google/android/apps/chrome/Main;->access$4800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$CustomSelectionActionModeCallback;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v4, 0x7f0f0081

    const v5, 0x7f0f0082

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/view/ActionMode$Callback;Landroid/app/Activity;II)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
    invoke-static {v8, v0}, Lcom/google/android/apps/chrome/Main;->access$5702(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->initialize()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/TabsView;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIsTablet:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4500(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v1, 0x7f0f007c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v2, 0x7f0f007d

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    # setter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/Main;->access$5802(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/OverviewBehavior;)Lcom/google/android/apps/chrome/OverviewBehavior;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const v3, 0x7f0f0083

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/app/Activity;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->initialize()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mControlContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4900(Lcom/google/android/apps/chrome/Main;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enable_tilt_scroll"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$5800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->setTiltControlsEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mContentViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$5000(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ContentViewHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "text_scale"

    const/high16 v2, 0x3f800000

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->fontScale:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getFontScaleMultiplier(Landroid/content/Context;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setFontScaleFactor(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefUserSetForceEnableZoom()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getForceEnableZoomFontScaleThreshold(Landroid/content/Context;)F

    move-result v2

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_7

    move v0, v6

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setForceEnableZoom(Z)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # setter for: Lcom/google/android/apps/chrome/Main;->mUIInitialized:Z
    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/Main;->access$5902(Lcom/google/android/apps/chrome/Main;Z)Z

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/TabsView;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getOverviewBehavior()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$5802(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/OverviewBehavior;)Lcom/google/android/apps/chrome/OverviewBehavior;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$5800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto :goto_1
.end method

.method private markSessionEnd()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    move-result-object v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/Main$MainWithNative;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->logAndEndSession()V

    :cond_1
    return-void
.end method

.method private markSessionResume()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$6202(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/uma/UmaSessionStats;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->updateMetricsServiceState()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->startNewSession(Lcom/google/android/apps/chrome/TabModelSelector;)V

    return-void
.end method

.method private onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v1, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mChromeWindow:Lcom/google/android/apps/chrome/ChromeWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ChromeWindow;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeWindow;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x65

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # setter for: Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/Main;->access$3002(Lcom/google/android/apps/chrome/Main;Z)Z

    if-ne p2, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowComplete(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCreatedTabOnStartup:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6100(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "Signed in to account"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->showSyncSignInNotification(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    const-string v0, "Close App"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->closeAllTabs()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->launchFirstRunExperience()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3100(Lcom/google/android/apps/chrome/Main;)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x66

    if-ne p1, v0, :cond_5

    const/16 v0, 0x32

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    goto :goto_0

    :cond_5
    const/16 v0, 0x67

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onBookmarkUiVisibilityChange(Z)V

    goto :goto_0

    :cond_6
    const/16 v0, 0x68

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method private onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lorg/chromium/net/NetworkChangeNotifier;->init(Landroid/content/Context;)Lorg/chromium/net/NetworkChangeNotifier;

    const/4 v0, 0x1

    invoke-static {v0}, Lorg/chromium/net/NetworkChangeNotifier;->setAutoDetectConnectivityState(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v1, Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v2}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->getDefaultMaxActiveTabs(Landroid/content/Context;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v3}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;->getDefaultDelegate(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/MemoryUsageMonitor;-><init>(ILcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mMemoryUsageMonitor:Lcom/google/android/apps/chrome/MemoryUsageMonitor;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$2602(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/MemoryUsageMonitor;)Lcom/google/android/apps/chrome/MemoryUsageMonitor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcesses(Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v1, Lcom/google/android/apps/chrome/IntentHandler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Main;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v4

    const-string v5, "enable-test-intents"

    invoke-virtual {v4, v5}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/chrome/IntentHandler;-><init>(Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;Ljava/lang/String;Z)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$2802(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/IntentHandler;)Lcom/google/android/apps/chrome/IntentHandler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->onNativeLibraryReady()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    invoke-static {v3}, Lcom/google/android/apps/chrome/Main;->access$1700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getGLResourceProvider()Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/gl/GLResourceProvider;)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$2902(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$2900(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->onNativeLibraryReady(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->removeSessionCookies()V

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "First run is running"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const-string v1, "First run is running"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$3002(Lcom/google/android/apps/chrome/Main;Z)Z

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->clearOrLoadPreviousState(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->createNewTab(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->launchFirstRunExperience()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3100(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "Pages towards showing uma info bar"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mUmaStatsOptInListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$3200(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    new-instance v2, Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/chrome/UmaStatsOptInListener;-><init>(Lcom/google/android/apps/chrome/Main;I)V

    # setter for: Lcom/google/android/apps/chrome/Main;->mUmaStatsOptInListener:Lcom/google/android/apps/chrome/UmaStatsOptInListener;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/Main;->access$3202(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/UmaStatsOptInListener;)Lcom/google/android/apps/chrome/UmaStatsOptInListener;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIsOnFirstRun:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3000(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getRestoredTabCount()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->launchWelcomePage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3300(Lcom/google/android/apps/chrome/Main;)V

    :cond_3
    # getter for: Lcom/google/android/apps/chrome/Main;->NOTIFICATIONS:[I
    invoke-static {}, Lcom/google/android/apps/chrome/Main;->access$3400()[I

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Main;->access$3500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "notification-center-logging"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enableLogging()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->createSearchEngineListener()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->initializeUI()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    const/16 v0, 0x25

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void
.end method

.method private onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onDestroy()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$3600(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V

    return-void
.end method

.method private onNewIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/IntentHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/IntentHandler;->shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2800(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/IntentHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->onNewIntent(Landroid/content/Intent;)Z

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "enable-test-intents"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->handleDebugIntent(Landroid/content/Intent;Lcom/google/android/apps/chrome/Tab;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    throw v0
.end method

.method private onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/TabModelSelectorImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$2700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->saveState()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->onActivityPause()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onPause()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$3700(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->flushPersistentData()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getGpuProfiler()Lcom/google/android/apps/chrome/GpuProfiler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/GpuProfiler;->unregisterReceiver(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getPowerBroadcastReceiver()Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->unregisterReceiver(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->markSessionEnd()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->hideMenus()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3800(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->hideSuggestions()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$3900(Lcom/google/android/apps/chrome/Main;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private onResume()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->mBackgroundProcessing:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->onResume()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$4000(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->onActivityResume()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->onActivityResume()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getGpuProfiler()Lcom/google/android/apps/chrome/GpuProfiler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/GpuProfiler;->registerReceiver(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getPowerBroadcastReceiver()Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->registerReceiver(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main;->mToolbar:Lcom/google/android/apps/chrome/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->updateMicButtonState()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->markSessionResume()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->onMainActivityResume()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getPowerBroadcastReceiver()Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->runActions(Landroid/content/Context;Z)V

    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$MainWithNative$1;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/Main$MainWithNative$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsTransient()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4300(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4100(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/OverviewBehavior;->showOverview(Z)V

    goto :goto_0
.end method

.method private updateOrientation()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/Main;->mCurrentOrientation:I
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/Main;->access$5202(Lcom/google/android/apps/chrome/Main;I)I

    :cond_0
    return-void
.end method
