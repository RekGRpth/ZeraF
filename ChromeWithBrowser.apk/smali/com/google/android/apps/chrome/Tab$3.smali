.class Lcom/google/android/apps/chrome/Tab$3;
.super Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;-><init>()V

    return-void
.end method


# virtual methods
.method public addNewContents(IIILandroid/graphics/Rect;Z)Z
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mNativeTabAndroidImpl:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$1600(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    # invokes: Lcom/google/android/apps/chrome/Tab;->nativeAddNewContents(IIIILandroid/graphics/Rect;Z)Z
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/Tab;->access$1700(Lcom/google/android/apps/chrome/Tab;IIIILandroid/graphics/Rect;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v7

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mId:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$1800(Lcom/google/android/apps/chrome/Tab;)I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/apps/chrome/TabModel;->createTabWithNativeContents(IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method

.method public closeContents()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2000(Lcom/google/android/apps/chrome/Tab;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mCloseContentsRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$1900(Lcom/google/android/apps/chrome/Tab;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2000(Lcom/google/android/apps/chrome/Tab;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mCloseContentsRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$1900(Lcom/google/android/apps/chrome/Tab;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public handleKeyboardEvent(Landroid/view/KeyEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/chrome/Main;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/Main;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    :cond_0
    return-void
.end method

.method public isFullscreenForTabOrPending()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v0

    goto :goto_0
.end method

.method public onFindMatchRectsAvailable(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1500(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1500(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;->onFindMatchRects(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V

    :cond_0
    return-void
.end method

.method public onFindResultAvailable(Lorg/chromium/chrome/browser/FindNotificationDetails;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFindResultListener:Lcom/google/android/apps/chrome/Tab$FindResultListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindResultListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFindResultListener:Lcom/google/android/apps/chrome/Tab$FindResultListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$FindResultListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/Tab$FindResultListener;->onFindResult(Lorg/chromium/chrome/browser/FindNotificationDetails;)V

    :cond_0
    return-void
.end method

.method public onLoadProgressChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mIsLoading:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1200(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyLoadProgress()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1300(Lcom/google/android/apps/chrome/Tab;)V

    goto :goto_0
.end method

.method public onLoadStarted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/16 v1, 0x1a

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$900(Lcom/google/android/apps/chrome/Tab;I)V

    return-void
.end method

.method public onLoadStopped()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/16 v1, 0x1b

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageLoad(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$900(Lcom/google/android/apps/chrome/Tab;I)V

    return-void
.end method

.method public onTitleUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->updateTitle()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1000(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageTitleChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$1100(Lcom/google/android/apps/chrome/Tab;)V

    :cond_0
    return-void
.end method

.method public onUpdateUrl(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/Tab;->mIsTabStateDirty:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Tab;->access$2102(Lcom/google/android/apps/chrome/Tab;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->updateNTPToolbarUrl(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Tab;->access$2200(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageUrlChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2300(Lcom/google/android/apps/chrome/Tab;)V

    return-void
.end method

.method public openNewTab(Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$800(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/Tab$InterceptNavigationDelegateImpl;->shouldIgnoreNewTab(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-interface {v0, p1, v1, v2, p2}, Lcom/google/android/apps/chrome/TabModelSelector;->openNewTab(Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Lcom/google/android/apps/chrome/Tab;Z)V

    :cond_0
    return-void
.end method

.method public showRepostFormWarningDialog(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->continuePendingReload()V

    :cond_0
    return-void
.end method

.method public takeFocus(Z)Z
    .locals 3

    const v2, 0x7f0f006f

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    const v1, 0x7f0f0074

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/Tab;->access$700(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->getLocationBar(Landroid/app/Activity;)Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    const v1, 0x7f0f006e

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0
.end method

.method public toggleFullscreenModeForTab(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$3;->this$0:Lcom/google/android/apps/chrome/Tab;

    # getter for: Lcom/google/android/apps/chrome/Tab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Tab;->access$2400(Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    goto :goto_0
.end method
