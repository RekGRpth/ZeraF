.class Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFolderId:J

.field private mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field final synthetic this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;J)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mContext:Landroid/content/Context;

    iput-wide p2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mFolderId:J

    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mFolderId:J

    iget-object v4, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v4

    # invokes: Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->handleLoadAllFolders(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->access$100(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V

    return-void
.end method

.method protected runBackgroundTask()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getBookmarkFolderHierarchy(Landroid/content/Context;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-void
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    # getter for: Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->access$200(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    # getter for: Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->access$300(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
