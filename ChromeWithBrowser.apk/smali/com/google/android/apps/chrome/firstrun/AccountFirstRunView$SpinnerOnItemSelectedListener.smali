.class Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$002(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$100(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;->onNewAccount()V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$002(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method
