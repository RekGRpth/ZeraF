.class final Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$callback:Ljava/lang/Runnable;

.field final synthetic val$syncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$syncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$appContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final tokenAvailable(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$syncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$syncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$account:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$appContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$account:Landroid/accounts/Account;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lorg/chromium/sync/notifier/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$callback:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method
