.class Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;
.super Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getPercentageTranslationX()F
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->getTranslationX()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public setPercentageTranslationX(F)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$Container;->setTranslationX(F)V

    return-void
.end method
