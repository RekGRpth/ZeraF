.class public Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;


# static fields
.field private static final PROGRESS_DELAY_MS:I = 0xc8


# instance fields
.field private mAcceptButton:Landroid/widget/Button;

.field private mHandler:Landroid/os/Handler;

.field private mProgressRunnable:Ljava/lang/Runnable;

.field private mSendReportCheckBox:Landroid/widget/CheckBox;

.field private mTerms:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040018

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mAcceptButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0f0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mTerms:Landroid/widget/TextView;

    new-instance v0, Ljava/util/Scanner;

    invoke-static {}, Lcom/google/android/apps/chrome/BrowserVersion;->getTermsOfServiceHtml()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    const-string v2, "\\A"

    invoke-virtual {v0, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mTerms:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    const-string v3, "</head>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f0f0059

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mAcceptButton:Landroid/widget/Button;

    const v0, 0x7f0f0052

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mAcceptButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;-><init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
