.class public Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mProgressRunnable:Ljava/lang/Runnable;

.field private mSignedText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public bridge synthetic onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040014

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0f0055

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mSignedText:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/SignedFirstRunFragment;->mSignedText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
