.class final Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/MemoryUsageMonitor$Delegate;


# instance fields
.field final synthetic val$model:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;->val$model:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final clearCachedNtpAndThumbnails()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;->val$model:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->clearCachedNtpAndThumbnails()V

    return-void
.end method

.method public final getTabModel(Z)Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;->val$model:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    return-object v0
.end method

.method public final registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public final saveStateAndDestroyTab(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/MemoryUsageMonitor$2;->val$model:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->saveStateAndDestroy()V

    return-void
.end method

.method public final unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method
