.class Lcom/google/android/apps/chrome/LocationBar$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/LocationBar$4;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->updateDeleteButtonVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2000(Lcom/google/android/apps/chrome/LocationBar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->updateNavigationButton()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2100(Lcom/google/android/apps/chrome/LocationBar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mIgnoreURLBarModification:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$300(Lcom/google/android/apps/chrome/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->clearListSelection()V

    :cond_2
    if-nez p1, :cond_4

    const-string v0, ""

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz p1, :cond_3

    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    if-eq v2, v3, :cond_3

    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_3

    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    :cond_3
    if-ltz v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v1, v2, :cond_5

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/LocationBar;->notifyTextChanged(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mAutocompleteIndex:I
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->access$202(Lcom/google/android/apps/chrome/LocationBar;I)I

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v2, v2, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->stop(Z)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2300(Lcom/google/android/apps/chrome/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getSelectionStart()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setSelection(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_2

    :cond_6
    sget-boolean v2, Lcom/google/android/apps/chrome/LocationBar$4;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/LocationBar;->access$2200(Lcom/google/android/apps/chrome/LocationBar;)Ljava/lang/Runnable;

    move-result-object v2

    if-eqz v2, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Multiple omnibox requests in flight."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    new-instance v3, Lcom/google/android/apps/chrome/LocationBar$4$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/google/android/apps/chrome/LocationBar$4$1;-><init>(Lcom/google/android/apps/chrome/LocationBar$4;Landroid/text/Editable;Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->access$2202(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2200(Lcom/google/android/apps/chrome/LocationBar;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/LocationBar;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2200(Lcom/google/android/apps/chrome/LocationBar;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2200(Lcom/google/android/apps/chrome/LocationBar;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2202(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2302(Lcom/google/android/apps/chrome/LocationBar;Z)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
