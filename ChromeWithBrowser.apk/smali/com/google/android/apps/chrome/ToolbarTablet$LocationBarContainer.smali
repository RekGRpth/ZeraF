.class Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;
.super Landroid/widget/RelativeLayout;


# instance fields
.field private mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 5

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f0f006e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    const v1, 0x7f0f0072

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;-><init>(Landroid/view/ViewGroup;[Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
