.class Lcom/google/android/apps/chrome/Main$10;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;

.field final synthetic val$type:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$10;->this$0:Lcom/google/android/apps/chrome/Main;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Main$10;->val$type:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$10;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    const-string v1, "chrome://newtab/"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/Main$10;->val$type:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/TabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    return-void
.end method
