.class public Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mFaviconDimension:I

.field private final mMaxWidth:I

.field private final mNullFaviconResourceId:I

.field private final mTextHeight:F

.field private final mTextLeftPadding:I

.field private final mTextPaint:Landroid/text/TextPaint;

.field private final mTextYOffset:F

.field private final mViewHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 8

    const/4 v7, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    const v0, 0x7f020076

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mNullFaviconResourceId:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_1

    const v0, 0x7f0a0019

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    if-eqz p2, :cond_2

    const v0, 0x7f0a001b

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    if-eqz p2, :cond_3

    const v0, 0x7f080034

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    if-eqz p2, :cond_4

    const v0, 0x7f080035

    :goto_4
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const v5, 0x7f080036

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->fontScale:F

    mul-float/2addr v5, v6

    new-instance v6, Landroid/text/TextPaint;

    invoke-direct {v6}, Landroid/text/TextPaint;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    const v6, 0x3a83126f

    int-to-float v4, v4

    int-to-float v0, v0

    invoke-virtual {v2, v6, v4, v0, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextHeight:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextYOffset:F

    const v0, 0x7f080037

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextLeftPadding:I

    const v0, 0x7f080038

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mFaviconDimension:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mFaviconDimension:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextHeight:F

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mViewHeight:I

    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getMaxTitleWidth(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mMaxWidth:I

    return-void

    :cond_0
    const v0, 0x7f020075

    goto/16 :goto_0

    :cond_1
    const v0, 0x7f0a0018

    goto/16 :goto_1

    :cond_2
    const v0, 0x7f0a001a

    goto/16 :goto_2

    :cond_3
    const v0, 0x7f080032

    goto/16 :goto_3

    :cond_4
    const v0, 0x7f080033

    goto/16 :goto_4
.end method


# virtual methods
.method public create(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10

    const/4 v8, 0x0

    const/16 v9, 0x31

    const/4 v0, 0x0

    const/high16 v6, 0x40000000

    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    invoke-static {p2, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mMaxWidth:I

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mFaviconDimension:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextLeftPadding:I

    add-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mViewHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    if-nez p3, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mNullFaviconResourceId:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v3, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p3

    :cond_1
    if-eqz p3, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mFaviconDimension:I

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    div-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mViewHeight:I

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v0, p3, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    if-eqz v2, :cond_3

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mFaviconDimension:I

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextLeftPadding:I

    add-int/2addr v1, v4

    int-to-float v4, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mViewHeight:I

    int-to-float v1, v1

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextHeight:F

    sub-float/2addr v1, v5

    div-float/2addr v1, v6

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextYOffset:F

    add-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v5, v1

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->mTextPaint:Landroid/text/TextPaint;

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    move-object v0, v7

    :goto_1
    return-object v0

    :cond_4
    move v2, v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->TAG:Ljava/lang/String;

    const-string v1, "OutOfMemoryError while building title texture."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v9}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    :goto_2
    move-object v0, v8

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->TAG:Ljava/lang/String;

    const-string v1, "InflateException while building title texture."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v9}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto :goto_2
.end method
