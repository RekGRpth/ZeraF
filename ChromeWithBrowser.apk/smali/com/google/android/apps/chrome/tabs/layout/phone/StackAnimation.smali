.class public abstract Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;
.super Ljava/lang/Object;


# static fields
.field protected static final DISCARD_ANIMATION_DURATION:I = 0x190

.field protected static final ENTER_STACK_ANIMATION_DURATION:I = 0x12c

.field protected static final ENTER_STACK_BORDER_ALPHA_DELAY:I = 0x4b

.field protected static final ENTER_STACK_BORDER_ALPHA_DURATION:I = 0xe1

.field protected static final FULL_ROLL_ANIMATION_DURATION:I = 0x3e8

.field protected static final INITIAL_ALPHA_AMOUNT:F = 0.1f

.field protected static final INITIAL_SCALE_AMOUNT:F = 0.75f

.field protected static final REACH_TOP_ANIMATION_DURATION:I = 0x190

.field public static final SCALE_AMOUNT:F = 0.9f

.field protected static final START_PINCH_ANIMATION_DURATION:I = 0x4b

.field protected static final TAB_FOCUSED_ANIMATION_DURATION:I = 0x190

.field protected static final TAB_FOCUSED_BORDER_ALPHA_DELAY:I = 0x0

.field protected static final TAB_FOCUSED_BORDER_ALPHA_DURATION:I = 0x12c

.field protected static final TAB_FOCUSED_MAX_DELAY:I = 0x64

.field protected static final TAB_OPENED_ANIMATION_DURATION:I = 0x12c

.field protected static final TAB_OPENED_BORDER_ALPHA_DELAY:I = 0x64

.field protected static final TAB_OPENED_BORDER_ALPHA_DURATION:I = 0x64

.field protected static final TAB_REORDER_DURATION:I = 0x1f4

.field protected static final TAB_REORDER_START_SPAN:I = 0x190

.field protected static final UNDISCARD_ANIMATION_DURATION:I = 0x96

.field protected static final VIEW_MORE_ANIMATION_DURATION:I = 0x190

.field protected static final VIEW_MORE_MIN_SIZE:I = 0xc8

.field protected static final VIEW_MORE_SIZE_RATIO:F = 0.75f


# instance fields
.field protected mHeight:I

.field protected mWidth:I


# direct methods
.method protected constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->mWidth:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->mHeight:I

    return-void
.end method

.method public static createAnimationFactory(III)Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;
    .locals 1

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;-><init>(II)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;-><init>(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected abstract addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V
.end method

.method public createAnimatorSetForType(Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIIFFF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackAnimation$OverviewAnimationType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0, p2, p3, p5, p7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createEnterStackAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p2, p3, p5, p7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createTabFocusedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createViewMoreAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2, p7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createReachTopAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2, p5, p7, p8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createUpdateDiscardAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IFF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p2, p3, p8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createStartPinchAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createFullRollAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected abstract createEnterStackAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
.end method

.method protected createFullRollAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 8

    const/4 v5, 0x0

    const/high16 v7, 0x40000000

    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    move v6, v5

    :goto_0
    array-length v0, p1

    if-ge v6, v0, :cond_0

    aget-object v0, p1, v6

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v3

    div-float/2addr v3, v7

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    div-float/2addr v3, v7

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    const/high16 v3, -0x3c4c0000

    const/16 v4, 0x3e8

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method protected createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    const/4 v9, 0x0

    const/4 v4, 0x0

    if-ltz p2, :cond_0

    array-length v0, p1

    if-lt p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    aget-object v1, p1, p2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setVisible(Z)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXInStackInfluence(F)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYInStackInfluence(F)V

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    neg-float v3, p3

    const-wide/16 v5, 0x12c

    const-wide/16 v7, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    goto :goto_0
.end method

.method protected abstract createReachTopAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)Lcom/google/android/apps/chrome/ChromeAnimation;
.end method

.method protected createStartPinchAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 7

    const/4 v5, 0x0

    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    move v6, v5

    :goto_0
    array-length v0, p1

    if-ge v6, v0, :cond_0

    aget-object v0, p1, v6

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x4b

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method protected abstract createTabFocusedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
.end method

.method protected createUpdateDiscardAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IFF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 15

    new-instance v2, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    move v11, v1

    move v13, v3

    move v12, v4

    :goto_0
    move-object/from16 v0, p1

    array-length v1, v0

    if-ge v11, v1, :cond_2

    aget-object v14, p1, v11

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x96

    const/4 v6, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v12, v12, 0x1

    const/4 v1, 0x1

    if-ne v12, v1, :cond_1

    invoke-virtual {p0, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F

    move-result v13

    move v3, v13

    move v4, v12

    :goto_1
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    move v13, v3

    move v12, v4

    goto :goto_0

    :cond_0
    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v5

    const/4 v6, 0x0

    const-wide/16 v7, 0x96

    const-wide/16 v9, 0x0

    move-object v3, v14

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :cond_1
    move v3, v13

    move v4, v12

    goto :goto_1

    :cond_2
    if-lez v12, :cond_7

    const/4 v1, 0x1

    if-ne v12, v1, :cond_3

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v12

    :goto_2
    const/4 v1, 0x0

    const/4 v3, 0x0

    move v14, v3

    :goto_3
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v14, v3, :cond_7

    aget-object v3, p1, v14

    const/high16 v4, 0x43c80000

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->getScreenSizeInScrollDirection()F

    move-result v5

    div-float/2addr v4, v5

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F

    move-result v5

    sub-float/2addr v5, v13

    mul-float/2addr v4, v5

    float-to-long v9, v4

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v5

    const/4 v4, 0x0

    cmpl-float v4, v5, v4

    if-ltz v4, :cond_4

    const/high16 v4, 0x3f800000

    move v6, v4

    :goto_4
    const/high16 v4, 0x43c80000

    const/high16 v7, 0x3f800000

    div-float v8, v5, p4

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v4, v7

    float-to-long v7, v4

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    mul-float v6, v6, p4

    const/4 v11, 0x0

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    :goto_5
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    goto :goto_3

    :cond_3
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;

    move-result-object v12

    goto :goto_2

    :cond_4
    const/high16 v4, -0x40800000

    move v6, v4

    goto :goto_4

    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    mul-int v4, p2, v1

    int-to-float v4, v4

    move/from16 v0, p3

    invoke-static {v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v6

    cmpl-float v4, v5, v6

    if-eqz v4, :cond_6

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const-wide/16 v7, 0x1f4

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    return-object v2
.end method

.method protected abstract createViewMoreAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/ChromeAnimation;
.end method

.method protected abstract getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
.end method

.method protected abstract getScreenPositionInSideDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
.end method

.method protected abstract getScreenSizeInScrollDirection()F
.end method
