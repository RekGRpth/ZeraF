.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FLING_MIN_DURATION:I = 0x64

.field private static final FLING_SPEED_DP:F = 1.5f

.field private static final INNER_MARGIN_PERCENT_PERCENT:F = 0.17f

.field private static final LAYOUTTAB_ASYNCHRONOUS_INITIALIZATION_BATCH_SIZE:I = 0x4

.field private static final MIN_INNER_MARGIN_PERCENT_DP:I = 0x37

.field private static final NOTIFICATIONS:[I

.field private static final SWITCH_STACK_FLING_DT:F = 0.033333335f

.field private static final TAG:Ljava/lang/String;

.field private static final THRESHOLD_TIME_TO_SWITCH_STACK_INPUT_MODE:F = 200.0f

.field private static final THRESHOLD_TO_SWITCH_STACK:F = 0.4f


# instance fields
.field private mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

.field private mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

.field private mClicked:Z

.field private mDelayedLayoutTabInitRequired:Z

.field private mFlingFromModelChange:Z

.field private mFlingSpeed:F

.field private mInnerMarginPercent:F

.field private mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

.field private mLastOnDownTimeStamp:J

.field private mLastOnDownX:F

.field private mLastOnDownY:F

.field private final mMinDirectionThreshold:F

.field private final mMinMaxInnerMargin:I

.field private final mMinShortPressThresholdSqr:F

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

.field private mRenderedScrollOffset:F

.field private mScrollIndexOffset:F

.field private mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mSortingComparator:Ljava/util/Comparator;

.field private mStackAnimationCount:I

.field private final mStackRects:[Landroid/graphics/RectF;

.field private final mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

.field private final mVisibilityArray:Ljava/util/ArrayList;

.field private final mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->TAG:Ljava/lang/String;

    new-array v0, v1, [I

    const/16 v1, 0xd

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinShortPressThresholdSqr:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x425c0000

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I

    const/high16 v1, 0x3fc00000

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    new-array v0, v7, [Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {v1, p1, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {v1, p1, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    aput-object v1, v0, v6

    new-array v0, v7, [Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    aput-object v1, v0, v6

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    return v0
.end method

.method private static addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, p2, 0x1

    aget-object v3, v2, v0

    aput-object v3, p1, p2

    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_0

    :cond_0
    return p2
.end method

.method private appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    move v1, p5

    move v2, v0

    :goto_0
    array-length v0, v3

    if-ge v2, v0, :cond_1

    aget-object v0, v3, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    aput-object v4, p4, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, p5

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private canScrollLinearly(I)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    neg-float v1, v1

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    xor-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    goto :goto_0
.end method

.method private computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;
    .locals 8

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStackIndexAt(FF)I

    move-result v0

    if-eq v5, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownX:F

    add-float v1, p3, p5

    sub-float v1, v0, v1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownY:F

    add-float v2, p4, p6

    sub-float v2, v0, v2

    mul-float v0, p5, p5

    mul-float v6, p6, p6

    add-float/2addr v6, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v7

    if-ne v7, v3, :cond_3

    :goto_2
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    mul-float/2addr v1, v7

    cmpl-float v1, v6, v1

    if-lez v1, :cond_4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    if-nez v5, :cond_5

    move v2, v3

    :goto_3
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_6

    move v1, v3

    :goto_4
    xor-int/2addr v1, v2

    if-eqz v1, :cond_7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_4

    :cond_7
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownTimeStamp:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43480000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_8

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_8
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinShortPressThresholdSqr:F

    cmpl-float v0, v6, v0

    if-lez v0, :cond_9

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto/16 :goto_0
.end method

.method private finishScrollStacks()V
    .locals 9

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    neg-int v0, v0

    int-to-float v4, v0

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    const-wide/16 v2, 0x64

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-long v0, v0

    add-long v5, v2, v0

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    const-wide/16 v7, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->propagateTabModel()V

    goto :goto_0
.end method

.method private flingStacks(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->canScrollLinearly(I)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setActiveStackState(Z)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    goto :goto_1
.end method

.method private getFullScrollDistance()F
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I

    int-to-float v0, v0

    :goto_0
    const/high16 v1, 0x40000000

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    goto :goto_0
.end method

.method private requestStackUpdate()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    return-void
.end method

.method private resetScrollData()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    return-void
.end method

.method private scrollStacks(F)V
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    div-float v0, p1, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->canScrollLinearly(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    return-void

    :cond_0
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, -0x40800000

    :goto_1
    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private startMarginAnimation(Z)V
    .locals 9

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v4, 0x3f800000

    :goto_0
    cmpl-float v0, v3, v4

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->INNER_MARGIN_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const-wide/16 v5, 0xc8

    const-wide/16 v7, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private updateDelayedLayoutTabInit([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    array-length v3, p1

    move v1, v2

    move v0, v2

    :goto_1
    if-ge v1, v3, :cond_3

    const/4 v4, 0x4

    if-ge v0, v4, :cond_0

    aget-object v4, p1, v1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    invoke-super {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    goto :goto_0
.end method

.method private updateSortedPriorityArray(Ljava/util/Comparator;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getCount()I

    move-result v3

    add-int/2addr v2, v3

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-eq v3, v2, :cond_2

    :cond_1
    new-array v2, v2, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I

    move-result v0

    sget-boolean v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-eq v0, v2, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v0, p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    move v0, v1

    goto :goto_0
.end method

.method private updateTabPriority()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateSortedPriorityArray(Ljava/util/Comparator;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateThumbnailVisibility([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateDelayedLayoutTabInit([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V

    goto :goto_0
.end method

.method private updateThumbnailVisibility([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public cleanupInstanceData(J)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    return-void
.end method

.method public click(JFF)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStackIndexAt(FF)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->click(JFF)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->contextChanged(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->resetDimensionConstants(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    return-void
.end method

.method public doneHiding()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->doneHiding()V

    return-void
.end method

.method public drag(JFFFF)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->drag(JFFFF)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :goto_1
    invoke-direct {p0, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->scrollStacks(F)V

    goto :goto_0

    :cond_3
    move p5, p6

    goto :goto_1
.end method

.method public fling(JFFFF)V
    .locals 9

    const/4 v8, 0x1

    const v7, 0x3d088889

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_0

    mul-float v5, p5, v7

    mul-float v6, p6, v7

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->fling(JFFFF)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v8, :cond_3

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v8, :cond_4

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v8, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I

    int-to-float v0, v0

    :goto_3
    mul-float v1, p5, v7

    add-float/2addr v1, p3

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    sub-float/2addr v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->scrollStacks(F)V

    goto :goto_0

    :cond_3
    move p5, p6

    goto :goto_1

    :cond_4
    move p3, p4

    goto :goto_2

    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I

    int-to-float v0, v0

    goto :goto_3
.end method

.method protected getTabStack(I)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTabStack(Z)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    aget-object v0, v1, v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getTabStackIndex(I)I
    .locals 3

    const/4 v0, 0x1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlesCloseAll()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isInitFromHostNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isListeningTiltScroll()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->animationIsRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isListeningTiltScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifySizeChanged(III)V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->notifySizeChanged(III)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->notifySizeChanged(III)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    return-void
.end method

.method protected onAnimationFinished()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->propagateTabModel()V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    :cond_0
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    :cond_0
    return-void
.end method

.method public onDown(JFF)V
    .locals 7

    const/4 v5, 0x0

    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownX:F

    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownY:F

    iput-wide p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownTimeStamp:J

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onDown(J)V

    return-void
.end method

.method public onLongPress(JFF)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onLongPress(JFF)V

    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onPinch(JFFFFZ)V

    return-void
.end method

.method public onStackAnimationFinished()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    :cond_0
    return-void
.end method

.method public onStackAnimationStarted()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    return-void
.end method

.method public onTextureChange(IZ)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTextureChange(IZ)V

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestUpdate()V

    :cond_0
    return-void
.end method

.method public onTiltScroll(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onTiltScroll(F)V

    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v2

    rsub-int/lit8 v3, v2, 0x1

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    if-nez v4, :cond_0

    int-to-float v2, v2

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x3ecccccd

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v2

    if-eqz v2, :cond_0

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setActiveStackState(Z)Z

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpOrCancel(J)V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateAnimation(JZ)Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v4, v4, v0

    invoke-virtual {v4, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateAnimation(JZ)Z

    move-result v4

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    move v0, v1

    goto :goto_0
.end method

.method public pushDebugRect(Landroid/graphics/Rect;I)V
    .locals 2

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, p1, Landroid/graphics/Rect;->right:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-le v0, v1, :cond_1

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->pushDebugRect(Landroid/graphics/Rect;I)V

    return-void
.end method

.method public setActiveStackState(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$2;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackLayout$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    goto :goto_0

    :pswitch_1
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setDeferredTabModel(Lcom/google/android/apps/chrome/tabs/DeferredTabModel;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setDeferredTabModel(Lcom/google/android/apps/chrome/tabs/DeferredTabModel;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    return-void
.end method

.method public show(JZ)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->reset()V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->show()V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    if-eq v3, v0, :cond_3

    move v0, v1

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v4, v4, v3

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v4, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stackEntered(JZ)V

    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v2, v1

    :cond_6
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    if-nez p3, :cond_7

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onUpdateAnimation(JZ)Z

    :cond_7
    return-void
.end method

.method public startHiding(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->startHiding(I)V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public tabClosing(JI)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(I)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosing(JI)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabClosingEffect(JI)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabModelSwitched(Z)V

    :cond_0
    return-void
.end method

.method public tabCreated(JIIIZZ)V
    .locals 2

    invoke-super/range {p0 .. p7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreated(JIIIZZ)V

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startHiding(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p6}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->selectModel(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabCreated(JI)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    invoke-virtual {p0, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabModelSwitched(Z)V

    return-void
.end method

.method public tabModelSwitched(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    return-void
.end method

.method public tabSelecting(JI)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelecting(JI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabSelectingEffect(J)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    return-void
.end method

.method public tabsAllClosing(JZ)V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabsAllClosing(JZ)V

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(Z)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabsAllClosingEffect(J)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabModelSwitched(Z)V

    :cond_0
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 11

    const/4 v1, -0x1

    const/4 v7, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v4

    iput v4, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v3

    iget v4, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v5

    add-float/2addr v4, v5

    iput v4, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Top()F

    move-result v4

    iput v4, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v3

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getHeight()F

    move-result v5

    add-float/2addr v4, v5

    iput v4, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v7

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v3

    iget v4, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationX()F

    move-result v5

    add-float/2addr v4, v5

    iput v4, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v7

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v7

    iget v4, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v5

    add-float/2addr v4, v5

    iput v4, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v7

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v3

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationY()F

    move-result v5

    add-float/2addr v4, v5

    iput v4, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v2, v2, v7

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v4, v4, v7

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getHeight()F

    move-result v0

    add-float/2addr v0, v4

    iput v0, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v0, v3

    const/high16 v0, 0x3f800000

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v4, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v0

    :goto_0
    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setStackFocusInfo(FI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v7

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    neg-float v2, v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    if-ne v4, v5, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getDeferredTabModel(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v1

    :cond_0
    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setStackFocusInfo(FI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v1, v1, v3

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabPosition(JLandroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    aget-object v1, v1, v7

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabPosition(JLandroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getVisibleCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getVisibleCount()I

    move-result v1

    add-int v10, v0, v1

    if-nez v10, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentModelIndex()I

    move-result v0

    if-ne v0, v7, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v0, p0

    move-wide v1, p1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v9

    iget-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v4, p0

    move-wide v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v0

    :goto_2
    sget-boolean v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    if-nez v1, :cond_6

    if-eq v0, v10, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "index should be incremented up to tabVisibleCount"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-eq v0, v10, :cond_1

    :cond_4
    new-array v0, v10, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_1

    :cond_5
    iget-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v4, p0

    move-wide v5, p1

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v5

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v0

    goto :goto_2

    :cond_6
    move v0, v3

    :goto_3
    if-ge v3, v10, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v1, v1, v3

    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v1

    if-eqz v1, :cond_7

    move v0, v7

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestUpdate()V

    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateTabPriority()V

    return-void
.end method
