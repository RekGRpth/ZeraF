.class public Lcom/google/android/apps/chrome/tabs/DeferredTabModel;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mCurrentTabId:I

.field private mCurrentTabIndex:I

.field private mIsIncognito:Z

.field private mRemovedIds:Ljava/util/ArrayList;

.field private mTabIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static removeAt([II)[I
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p0

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_2

    aget v2, p0, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    array-length v0, v1

    if-ge p1, v0, :cond_3

    add-int/lit8 v0, p1, 0x1

    aget v0, p0, v0

    aput v0, v1, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method private removeTabByIndex(II)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v3, -0x1

    if-eq p1, v3, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v1, v1

    if-lt p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aget v2, v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->removeAt([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v1

    if-eq v1, v3, :cond_4

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    if-ne v1, p1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v1, v1

    if-lez v1, :cond_5

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aget v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    goto :goto_1

    :cond_5
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    if-le v0, p1, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    aget v1, v1, v2

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method public addTab(IIZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aput p1, v1, v0

    :cond_0
    if-eqz p3, :cond_1

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->moveTab(II)V

    return-void
.end method

.method public contains(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public flushRemovedIds()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v0, v0

    return v0
.end method

.method public getCurrentTabId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    return v0
.end method

.method public getCurrentTabIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    return v0
.end method

.method public getTabId(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getTabIndex(I)I
    .locals 3

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aget v2, v2, v0

    if-ne p1, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public init(Lcom/google/android/apps/chrome/TabModel;)V
    .locals 5

    const/4 v1, -0x1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModel;->isIncognito()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mIsIncognito:Z

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v2

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    aput v3, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    if-eqz v2, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v1

    :cond_1
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public isIncognito()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mIsIncognito:Z

    return v0
.end method

.method public moveTab(II)V
    .locals 4

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v0, v0

    if-lt p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-eq v0, p2, :cond_0

    if-ge v0, p2, :cond_2

    :goto_1
    if-ge v0, p2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    if-le v0, p2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    aput v2, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aput p1, v0, p2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    goto :goto_0
.end method

.method public removeAllTabs()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mRemovedIds:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    iput v4, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mTabIds:[I

    return-void
.end method

.method public removeTabById(II)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->removeTabByIndex(II)Z

    move-result v0

    return v0
.end method

.method public setCurrentTabId(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentTabIndex(I)Z
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabIndex:I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->mCurrentTabId:I

    const/4 v0, 0x1

    goto :goto_0
.end method
