.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getLayoutTabsDrawnCount()I
.end method

.method public abstract getViewport()Landroid/graphics/Rect;
.end method

.method public abstract getVisibleViewport()Landroid/graphics/Rect;
.end method

.method public abstract loadPersitentTextureDataIfNeeded()V
.end method

.method public abstract onSurfaceCreated()V
.end method

.method public abstract onSwapBuffersCompleted()V
.end method

.method public abstract postLoadBitmapFromResource(Lcom/google/android/apps/chrome/tabs/BitmapRequester;I)V
.end method

.method public abstract pushDebugRect(Landroid/graphics/Rect;I)V
.end method

.method public abstract pushDebugValues(FFF)V
.end method

.method public abstract requestRender()V
.end method
