.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->handleNotificationMessage(Landroid/os/Message;)V

    return-void

    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "lastId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabSelected(I)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_INSTANT:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v3, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq v3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "sourceTabId"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "incognito"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "willBeSelected"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabCreated(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "nextId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabClosed(II)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabModelSwitched(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->access$000(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;Z)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "toPosition"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "incognito"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabMoved(IIZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;IIZ)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "color"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getFallbackTextureId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->initFromHost(II)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_4
        0x5 -> :sswitch_2
        0xc -> :sswitch_3
        0x3f -> :sswitch_5
    .end sparse-switch
.end method
