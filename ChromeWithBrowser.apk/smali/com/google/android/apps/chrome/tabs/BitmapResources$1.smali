.class Lcom/google/android/apps/chrome/tabs/BitmapResources$1;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/BitmapResources;

.field final synthetic val$id:I

.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/BitmapResources;Landroid/content/res/Resources;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->this$0:Lcom/google/android/apps/chrome/tabs/BitmapResources;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->val$res:Landroid/content/res/Resources;

    iput p3, p0, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->val$id:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->val$res:Landroid/content/res/Resources;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->val$id:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/BitmapResources$1;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
