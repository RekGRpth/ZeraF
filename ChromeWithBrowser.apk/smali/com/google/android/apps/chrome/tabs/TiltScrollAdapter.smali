.class public Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final ACTIVATION_DELAY_MS:J = 0x1f4L

.field private static final ANGLE_DURATION:F = 8.0f

.field private static final ANGLE_MAX:F = 1.5707964f

.field private static final AVERAGE_1:F = 0.5f

.field private static final AVERAGE_2:F = 0.25f

.field private static final AVERAGE_BIAS:F = 2.5E-4f

.field private static final AVERAGE_CUBE:F = 0.0025f

.field private static final DIRECTION_CLAMP:F = 0.5f

.field private static final DIRECTION_THRESHOLD:F = 0.1f

.field private static final INTENT_DURATION:F = 1.0f

.field private static final INTENT_FADE:F = 0.01f

.field private static final INTENT_MULTIPLIER:F = 0.2f

.field private static final LANDSCAPE_TILT_MULTIPLIER:F = 1.5f

.field private static final NOISE_FADE:F = 0.05f

.field private static final NOISE_MULTIPLIER:F = 2.0f

.field private static final SCROLL_EXPONENTIAL_FACTOR:F = 5.0f

.field private static final SCROLL_LINEAR_FACTOR:F = 34.0f

.field private static final SKIP_DELTA_TIME_THRESHOLD:F = 0.05f

.field private static final TAG:Ljava/lang/String;

.field private static final TOLERANCE_X:F = 1.0f

.field private static final TOLERANCE_Y:F = 1.0f

.field private static final TOLERANCE_Z:F = 1.0f


# instance fields
.field private mActivateRunnable:Ljava/lang/Runnable;

.field private mActive:Z

.field private mAngle:F

.field private mEventMultiplier:F

.field private final mFailedPassiveRequirements:Ljava/util/Set;

.field private final mFailedRequirements:Ljava/util/Set;

.field private final mHandler:Landroid/os/Handler;

.field private mIntentAmount:F

.field private final mListener:Lcom/google/android/apps/chrome/tabs/TiltScrollListener;

.field private mNoise:F

.field private final mScreenDensity:F

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTimestamp:J

.field private mVelocityAverageX1:F

.field private mVelocityAverageX2:F

.field private mVelocityAverageY1:F

.field private mVelocityAverageY2:F

.field private mVelocityAverageZ1:F

.field private mVelocityAverageZ2:F

.field private mVelocityBias:F

.field private mVelocityCubeAverage:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TiltScrollListener;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_VISIBLE:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;->LAYOUT_ENABLED:Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    const-class v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX2:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY1:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ1:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActive:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mListener:Lcom/google/android/apps/chrome/tabs/TiltScrollListener;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mScreenDensity:F

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;)Landroid/hardware/SensorManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method private activate()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActive:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$1;-><init>(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActive:Z

    goto :goto_0
.end method

.method private computeScrollBy(FF)F
    .locals 5

    const/high16 v0, 0x40a00000

    mul-float/2addr v0, p1

    float-to-double v1, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    neg-float v0, v0

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->exp(D)D

    move-result-wide v3

    sub-double v0, v1, v3

    const-wide/high16 v2, 0x4041000000000000L

    mul-double/2addr v0, v2

    float-to-double v2, p2

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mScreenDensity:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3c23d70a

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private deactivate()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActive:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActivateRunnable:Ljava/lang/Runnable;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method private getSensorManager()Landroid/hardware/SensorManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "sensor"

    invoke-static {v0}, Lorg/chromium/base/WeakContext;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorManager:Landroid/hardware/SensorManager;

    goto :goto_0
.end method

.method private isPassivelyIgnoringSensorData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static timeBasedFade(FFFF)F
    .locals 12

    const-wide v10, 0x400921fb54442d18L

    const-wide/high16 v8, 0x4000000000000000L

    const-wide/high16 v6, 0x3ff0000000000000L

    invoke-static {p0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    const/high16 v1, 0x3f800000

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3ee4f8b588e368f1L

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    float-to-double v1, v1

    mul-double/2addr v1, v8

    sub-double/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->acos(D)D

    move-result-wide v1

    div-double/2addr v1, v10

    div-float v3, p1, p2

    float-to-double v3, v3

    add-double/2addr v1, v3

    float-to-double v3, v0

    mul-double v0, v1, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    add-double/2addr v0, v6

    mul-double/2addr v0, v3

    div-double/2addr v0, v8

    float-to-double v2, p3

    mul-double/2addr v0, v2

    double-to-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method public isActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mActive:Z

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onRotationChange(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    :goto_0
    return-void

    :pswitch_1
    const/high16 v0, 0x3fc00000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    goto :goto_0

    :pswitch_2
    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    goto :goto_0

    :pswitch_3
    const/high16 v0, -0x40400000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10

    const/high16 v9, 0x3f400000

    const/high16 v5, 0x3e800000

    const/high16 v8, 0x3f000000

    const/high16 v7, 0x3f800000

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->isPassivelyIgnoringSensorData()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mListener:Lcom/google/android/apps/chrome/tabs/TiltScrollListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/TiltScrollListener;->isListening()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX2:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY1:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ1:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    :cond_2
    :goto_1
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    goto :goto_0

    :cond_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    mul-float/2addr v0, v1

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    mul-float/2addr v1, v2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mEventMultiplier:F

    mul-float/2addr v2, v3

    mul-float v3, v0, v8

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    mul-float v3, v1, v8

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY1:F

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY1:F

    mul-float v3, v2, v8

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ1:F

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ1:F

    mul-float v3, v0, v5

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX2:F

    mul-float/2addr v4, v9

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX2:F

    mul-float v3, v1, v5

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    mul-float/2addr v4, v9

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    mul-float v3, v2, v5

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    mul-float/2addr v4, v9

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    const v3, 0x3983126f

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    const v5, 0x3f7fef9e

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    mul-float v3, v0, v0

    mul-float/2addr v0, v3

    const v3, 0x3b23d70a

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    const v4, 0x3f7f5c29

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX2:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v7

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY1:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float/2addr v3, v7

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ1:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v4, v7

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageY2:F

    sub-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v1, v3

    sub-float/2addr v1, v0

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageZ2:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v2, v4

    sub-float v0, v2, v0

    invoke-static {v1, v6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    const v3, 0x3f733333

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    const/high16 v3, 0x40000000

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    invoke-static {v0, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityAverageX1:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40a00000

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    sub-float v3, v7, v3

    const v4, 0x3e4ccccd

    mul-float/2addr v3, v4

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mNoise:F

    sub-float v2, v7, v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    const v2, 0x3f7d70a4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    iget-wide v1, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    sub-long/2addr v1, v3

    long-to-double v1, v1

    const-wide v3, 0x41cdcd6500000000L

    div-double/2addr v1, v3

    double-to-float v1, v1

    cmpg-float v2, v1, v6

    if-lez v2, :cond_2

    const v2, 0x3d4ccccd

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    sub-float/2addr v2, v3

    const/high16 v3, -0x41000000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityBias:F

    add-float/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mVelocityCubeAverage:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v3

    mul-float/2addr v2, v3

    const v3, 0x3dcccccd

    div-float/2addr v2, v3

    add-float/2addr v2, v8

    const/high16 v3, -0x40800000

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v2

    add-float/2addr v2, v7

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    mul-float/2addr v0, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mListener:Lcom/google/android/apps/chrome/tabs/TiltScrollListener;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->computeScrollBy(FF)F

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/tabs/TiltScrollListener;->onTiltScroll(F)V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    const/high16 v2, 0x41000000

    const v3, 0x3fc90fdb

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->timeBasedFade(FFFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mAngle:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    invoke-static {v0, v1, v7, v7}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->timeBasedFade(FFFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mIntentAmount:F

    goto/16 :goto_1
.end method

.method public passivePause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public passiveResume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedPassiveRequirements:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mSensorTimestamp:J

    :cond_0
    return-void
.end method

.method public pause(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->deactivate()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public resume(Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter$Requirement;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->mFailedRequirements:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/TiltScrollAdapter;->activate()V

    :cond_0
    return-void
.end method
