.class public final enum Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum ROTATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum TILTX:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum TILTY:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

.field public static final enum Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "BORDER_ALPHA"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "BORDER_SCALE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "ALPHA"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "ROTATION"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ROTATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "SCALE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "TILTX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTX:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "TILTY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTY:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "X"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-string v1, "Y"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ROTATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTX:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTY:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    return-object v0
.end method
