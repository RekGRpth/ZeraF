.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract cleanupLayout()V
.end method

.method public abstract getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;
.end method

.method public abstract getTabBorder()Lcom/google/android/apps/chrome/tabs/TabBorder;
.end method

.method public abstract getTabShadow()Lcom/google/android/apps/chrome/tabs/TabShadow;
.end method

.method public abstract getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
.end method

.method public abstract getViewport()Landroid/graphics/Rect;
.end method
