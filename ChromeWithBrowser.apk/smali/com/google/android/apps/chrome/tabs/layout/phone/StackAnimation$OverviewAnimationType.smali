.class public final enum Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum NEW_TAB_OPENED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field public static final enum VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "ENTER_STACK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "NEW_TAB_OPENED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NEW_TAB_OPENED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "TAB_FOCUSED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "VIEW_MORE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "REACH_TOP"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "DISCARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "UNDISCARD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "START_PINCH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "FULL_ROLL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const-string v1, "NONE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NEW_TAB_OPENED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    return-object v0
.end method
