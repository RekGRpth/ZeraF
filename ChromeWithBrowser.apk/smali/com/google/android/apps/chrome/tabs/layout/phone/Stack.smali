.class public Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BACKWARDS_TILT_SCALE:F = 0.5f

.field private static final DISCARD_COMMIT_THRESHOLD:F = 0.6f

.field private static final DISCARD_END_SCALE:F = 0.85f

.field private static final DISCARD_FLING_DT:F = 0.022222223f

.field private static final DISCARD_FLING_MAX_CONTRIBUTION:F = 0.4f

.field private static final DISCARD_MAX_ANGLE:F = 30.0f

.field public static final DISCARD_RANGE_SCREEN:F = 0.7f

.field private static final DISCARD_SAFE_SELECTION_PCTG:F = 0.1f

.field private static final DRAG_ANGLE_THRESHOLD:F

.field private static final DRAG_MOTION_THRESHOLD_DP:F = 1.25f

.field private static final DRAG_TIME_THRESHOLD:J = 0x190L

.field public static final MAX_NUMBER_OF_STACKED_TABS_BOTTOM:I = 0x3

.field public static final MAX_NUMBER_OF_STACKED_TABS_TOP:I = 0x3

.field private static final MAX_OVER_FLING_SCALE:F = 0.5f

.field private static final MAX_UNDER_SCROLL_SCALE:F = 2.0f

.field private static final OVERSCROLL_FULL_ROLL_TRIGGER:I = 0x5

.field private static final OVERSCROLL_TOP_SLIDE_PCTG:F = 0.25f

.field private static final SCROLL_WARP_PCTG:F = 0.4f

.field public static final SPACING_SCREEN:F = 0.26f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final SKIP_TILT_SCROLL_THRESHOLD:F

.field private final TILT_SCROLL_REMAINING_FADE:F

.field private mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

.field private mCurrentMode:I

.field private mCurrentScrollDirection:F

.field private mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

.field private mDiscardingTabIndex:I

.field private mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

.field private mEvenOutProgress:F

.field private mEvenOutRate:F

.field private mHeight:I

.field private mIsDying:Z

.field private mLastConsumeTiltScroll:J

.field private mLastPinch0Offset:F

.field private mLastPinch1Offset:F

.field private mLastScrollUpdate:J

.field private final mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

.field private mLongPressSelected:I

.field private mMaxOverScroll:I

.field private mMaxOverScrollAngle:F

.field private mMaxOverScrollSlide:I

.field private mMaxTiltScrollSpeed:F

.field private mMaxUnderScroll:I

.field private mMinScrollMotion:F

.field private mMinSpacing:F

.field private final mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mOverScrollCounter:I

.field private mOverScrollDerivative:I

.field private mOverScrollOffset:F

.field private final mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

.field private mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field private mPinch0TabIndex:I

.field private mPinch1TabIndex:I

.field private mRecomputePosition:Z

.field private mReferenceOrderIndex:I

.field private mRotationDirection:F

.field private mScrollOffset:F

.field private mScrollOffsetForDyingTabs:F

.field private mScrollTarget:F

.field private mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

.field private mScrollingTabIndex:I

.field private mSpacing:I

.field private mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mTiltScrollRemaining:F

.field private final mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mWarpSize:F

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->TAG:Ljava/lang/String;

    const-wide/high16 v0, 0x403e000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->DRAG_ANGLE_THRESHOLD:F

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000

    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollDerivative:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:I

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollSlide:I

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    const v0, 0x3d4ccccd

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->SKIP_TILT_SCROLL_THRESHOLD:F

    const v0, 0x3ca3d70a

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->TILT_SCROLL_REMAINING_FADE:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastConsumeTiltScroll:J

    const/high16 v0, 0x447a0000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxTiltScrollSpeed:F

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinSpacing:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    return-void
.end method

.method private allowOverscroll()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    add-float/2addr v0, p2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->scrollToScreen(FF)F

    move-result v0

    return v0
.end method

.method private commitDiscard(JZ)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v2

    div-float/2addr v1, v2

    const v2, 0x3f19999a

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v0

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabClosing(JI)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->stackViewSwipeCloseTab()V

    :goto_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    goto :goto_1
.end method

.method public static computeDiscardAlpha(FF)F
    .locals 3

    const/high16 v0, 0x3f800000

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    div-float v1, p0, p1

    const/high16 v2, -0x40800000

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->smoothstep(F)F

    move-result v1

    mul-float/2addr v1, v1

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public static computeDiscardAngle(FFF)F
    .locals 2

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f800000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    div-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x41f00000

    mul-float/2addr v0, v1

    mul-float/2addr v0, p2

    goto :goto_0
.end method

.method public static computeDiscardScale(FF)F
    .locals 3

    const/high16 v0, 0x3f800000

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    div-float v1, p0, p1

    const v2, 0x3f59999a

    mul-float/2addr v1, v1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v0

    goto :goto_0
.end method

.method public static computeDiscardSideOffset(FFF)F
    .locals 7

    const/4 v0, 0x0

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x3f800000

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAngle(FFF)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    const-wide v5, 0x3f50624de0000000L

    cmpg-double v3, v3, v5

    if-ltz v3, :cond_0

    float-to-double v3, p0

    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    move-result-wide v5

    div-double/2addr v3, v5

    const-wide/high16 v5, 0x3ff0000000000000L

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    sub-double v0, v5, v0

    mul-double/2addr v0, v3

    double-to-float v0, v0

    goto :goto_0
.end method

.method private computeDragLock(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;
    .locals 9

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sget v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->DRAG_ANGLE_THRESHOLD:F

    mul-float/2addr v0, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x190

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v5, v6, :cond_1

    sub-float v5, v1, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_3

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v5, v6, :cond_2

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v2, v5, :cond_4

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    :cond_3
    iput-wide v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v1, v2, :cond_4

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v1, v2, :cond_6

    :goto_1
    return-object v0

    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    goto :goto_1
.end method

.method private computeOverscrollPercent()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private computeSpacing(I)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-le p1, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()I

    move-result v2

    int-to-float v1, v2

    const v3, 0x3e851eb8

    mul-float/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinSpacing:F

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v3, :cond_2

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v1, v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, -0x14

    int-to-float v0, v0

    int-to-float v2, p1

    const v3, 0x3f4ccccd

    mul-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_3
    return v0
.end method

.method private computeTabClippingVisibilityHelper()V
    .locals 14

    const/4 v10, 0x0

    const/4 v9, 0x1

    const v1, 0x7f7fffff

    const/high16 v6, 0x3f800000

    const/4 v13, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v9, :cond_1

    move v8, v9

    :goto_0
    if-eqz v8, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    :goto_1
    int-to-float v0, v0

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_2
    if-ltz v7, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setVisible(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v0

    cmpl-float v0, v0, v13

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackOffset()F

    move-result v0

    cmpl-float v0, v0, v13

    if-nez v0, :cond_0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRotation()F

    move-result v0

    cmpl-float v0, v0, v13

    if-nez v0, :cond_0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v0

    cmpg-float v0, v0, v6

    if-gez v0, :cond_3

    :cond_0
    invoke-virtual {v11, v1, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipSize(FF)V

    move v0, v3

    :goto_3
    add-int/lit8 v2, v7, -0x1

    move v7, v2

    move v3, v0

    goto :goto_2

    :cond_1
    move v8, v10

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    goto :goto_1

    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v2

    :goto_4
    if-eqz v8, :cond_5

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v0

    :goto_5
    sub-float v4, v3, v2

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v12

    cmpg-float v0, v12, v13

    if-gtz v0, :cond_6

    invoke-virtual {v11, v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setVisible(Z)V

    :goto_6
    if-lez v7, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    add-int/lit8 v3, v7, -0x1

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v4

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_a

    invoke-virtual {v3, v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setUseBorderClipMargin(Z)V

    move v0, v2

    goto :goto_3

    :cond_4
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    goto :goto_4

    :cond_5
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v0

    goto :goto_5

    :cond_6
    sget v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    div-float v0, v12, v0

    invoke-static {v0, v13, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {v11, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v0

    cmpl-float v0, v0, v13

    if-gtz v0, :cond_7

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v0

    cmpl-float v0, v0, v13

    if-lez v0, :cond_d

    :cond_7
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    div-float/2addr v0, v4

    const v4, 0x3f19999a

    mul-float/2addr v0, v4

    add-float/2addr v0, v6

    move v5, v0

    :goto_7
    if-eqz v8, :cond_8

    move v4, v1

    :goto_8
    if-eqz v8, :cond_9

    mul-float v0, v12, v5

    :goto_9
    invoke-virtual {v11, v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipSize(FF)V

    goto :goto_6

    :cond_8
    mul-float v0, v12, v5

    move v4, v0

    goto :goto_8

    :cond_9
    move v0, v1

    goto :goto_9

    :cond_a
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v0

    mul-float/2addr v0, v12

    add-float/2addr v0, v2

    invoke-virtual {v3, v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setUseBorderClipMargin(Z)V

    goto/16 :goto_3

    :cond_b
    return-void

    :cond_c
    move v0, v3

    goto/16 :goto_3

    :cond_d
    move v5, v6

    goto :goto_7
.end method

.method private computeTabOffsetHelper(Landroid/graphics/RectF;)V
    .locals 14

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v9

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeOverscrollPercent()F

    move-result v10

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v5

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    if-ge v1, v4, :cond_7

    sget-boolean v4, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v1

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v4, v1

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v11

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    :goto_2
    invoke-direct {p0, v6, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const/4 v7, 0x3

    if-ge v3, v7, :cond_2

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v7

    float-to-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    double-to-float v7, v12

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    double-to-float v12, v12

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    invoke-static {v7, v12}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v12

    mul-float/2addr v7, v12

    sget v12, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    mul-float/2addr v7, v12

    add-float/2addr v2, v7

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x0

    :goto_3
    add-int/2addr v3, v6

    const/4 v6, 0x0

    cmpg-float v6, v10, v6

    if-gez v6, :cond_11

    const/high16 v6, 0x3e800000

    div-float v6, v10, v6

    mul-float/2addr v6, v4

    add-float/2addr v4, v6

    const/4 v6, 0x0

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    move v7, v4

    :goto_4
    if-eqz v0, :cond_5

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v4

    sub-float v4, v8, v4

    const/high16 v6, 0x40000000

    div-float v6, v4, v6

    :goto_5
    const/high16 v4, 0x40000000

    div-float v4, v6, v4

    if-eqz v0, :cond_6

    add-float/2addr v4, v7

    :goto_6
    invoke-virtual {v11, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    invoke-virtual {v11, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_3
    move v4, v5

    goto :goto_2

    :cond_4
    const/4 v6, 0x1

    goto :goto_3

    :cond_5
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v4

    sub-float v4, v9, v4

    const/high16 v6, 0x40000000

    div-float v6, v4, v6

    goto :goto_5

    :cond_6
    add-float/2addr v6, v7

    goto :goto_6

    :cond_7
    const/4 v3, 0x0

    if-eqz v0, :cond_8

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    int-to-float v1, v1

    :goto_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move v2, v3

    :goto_8
    if-ltz v4, :cond_c

    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v4

    if-nez v3, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    int-to-float v1, v1

    goto :goto_7

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_a

    if-eqz v0, :cond_b

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    :goto_9
    cmpl-float v3, v3, v1

    if-ltz v3, :cond_a

    const/4 v3, 0x3

    if-ge v2, v3, :cond_a

    sget v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    sub-float/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    :cond_a
    add-int/lit8 v3, v4, -0x1

    move v4, v3

    goto :goto_8

    :cond_b
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_9

    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v4

    const/4 v1, 0x0

    :goto_a
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    sget-boolean v2, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v1

    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v3

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackOffset()F

    move-result v7

    add-float/2addr v3, v7

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXOutOfStack()F

    move-result v7

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYOutOfStack()F

    move-result v8

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackInfluence()F

    move-result v9

    invoke-static {v7, v2, v9}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v2

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackInfluence()F

    move-result v7

    invoke-static {v8, v3, v7}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v3

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_e

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v5

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    invoke-static {v5, v4, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardSideOffset(FFF)F

    move-result v7

    if-eqz v0, :cond_f

    add-float/2addr v2, v5

    add-float/2addr v3, v7

    :cond_e
    :goto_b
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v5

    invoke-virtual {v6, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    iget v2, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    invoke-virtual {v6, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_f
    sub-float/2addr v2, v7

    add-float/2addr v3, v5

    goto :goto_b

    :cond_10
    return-void

    :cond_11
    move v7, v4

    goto/16 :goto_4
.end method

.method private computeTabScaleAlphaDepthHelper(Landroid/graphics/RectF;)V
    .locals 8

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v2

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v5

    invoke-static {v5, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardScale(FF)F

    move-result v6

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v7

    mul-float/2addr v6, v7

    mul-float/2addr v6, v0

    invoke-virtual {v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    invoke-static {v5, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAlpha(FF)F

    move-result v6

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getAlpha()F

    move-result v7

    mul-float/2addr v6, v7

    invoke-virtual {v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    invoke-static {v5, v2, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAngle(FFF)F

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getRotation()F

    move-result v3

    add-float/2addr v3, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setRotation(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private computeTabScrollOffsetHelper()V
    .locals 5

    const v1, 0x7f7fffff

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    neg-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v3

    add-float/2addr v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v1

    add-float/2addr v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private computeTabTiltHelper(JLandroid/graphics/RectF;)V
    .locals 10

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeOverscrollPercent()F

    move-result v4

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v4, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v1, v5, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    cmpg-float v1, v4, v1

    if-gez v1, :cond_8

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x0

    const/high16 v2, -0x41800000

    cmpg-float v2, v4, v2

    if-gez v2, :cond_5

    const/high16 v1, 0x3e800000

    add-float/2addr v1, v4

    const/high16 v2, 0x3f400000

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    neg-float v1, v1

    invoke-interface {v2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    neg-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000

    mul-float/2addr v1, v2

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v3

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v2

    add-float/2addr v2, v3

    :goto_2
    const/4 v3, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    if-ge v3, v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    if-eqz v0, :cond_7

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v5

    sub-float v5, v2, v5

    invoke-virtual {v4, v1, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    add-float/2addr v2, v3

    goto :goto_2

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v5

    sub-float v5, v2, v5

    invoke-virtual {v4, v1, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    goto :goto_4

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    mul-float/2addr v5, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollSlide:I

    int-to-float v4, v4

    mul-float/2addr v4, v1

    const/4 v1, 0x0

    :goto_5
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    if-eqz v0, :cond_9

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v7

    div-float/2addr v7, v3

    const/high16 v8, 0x3f000000

    add-float/2addr v7, v8

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v7

    mul-float/2addr v7, v5

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v8

    const/high16 v9, 0x40400000

    div-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v7

    add-float/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    div-float/2addr v7, v2

    const/high16 v8, 0x3f000000

    add-float/2addr v7, v8

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v7

    mul-float/2addr v7, v5

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v8

    const/high16 v9, 0x40400000

    div-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    add-float/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_6
.end method

.method private computeTabVisibilitySortingHelper(Landroid/graphics/RectF;)V
    .locals 9

    const/4 v1, 0x0

    const/high16 v3, 0x40000000

    const/4 v8, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    cmpl-float v2, v2, v8

    if-lez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    cmpg-float v2, v2, v8

    if-gez v2, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(III)I

    move-result v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/RectF;->left:F

    invoke-static {v4, v8, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v4

    iget v5, p1, Landroid/graphics/RectF;->right:F

    invoke-static {v5, v8, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v5

    iget v6, p1, Landroid/graphics/RectF;->top:F

    invoke-static {v6, v8, v3}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v6

    iget v7, p1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v7, v8, v3}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v7

    sub-float v4, v5, v4

    sub-float v5, v7, v6

    mul-float/2addr v4, v5

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float v2, v4, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->updateStackVisiblityValue(F)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->updateVisiblityValue(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private consumeTiltScroll(J)V
    .locals 7

    const/high16 v4, 0x40000000

    const/4 v6, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastConsumeTiltScroll:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000

    div-float/2addr v0, v1

    const v1, 0x3d4ccccd

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxTiltScrollSpeed:F

    mul-float/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float v3, v1, v0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    int-to-float v0, v0

    div-float v1, v0, v4

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    int-to-float v0, v0

    div-float v2, v0, v4

    const/4 v5, 0x1

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scroll(FFFFZ)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    const v1, 0x3f7ae148

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_1

    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->springBack(J)V

    :cond_2
    iput-wide p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastConsumeTiltScroll:J

    return-void
.end method

.method private createStackTabs()V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    new-array v0, v3, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->isHiding()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getTabId(I)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v7, v6, v5, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v8

    invoke-direct {p0, v4, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->findTabById([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v8

    aput-object v8, v7, v2

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v2

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    new-instance v8, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {v8, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    aput-object v8, v7, v2

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v2

    invoke-virtual {v6, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v2

    invoke-virtual {v7, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    goto :goto_1
.end method

.method private createTabHelper(I)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->contains(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v3

    if-eq v3, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createStackTabs()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private discard(FFFF)V
    .locals 6

    const/4 v5, 0x1

    const/high16 v4, 0x3f800000

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-gez v0, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClickTargetBounds()Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v1, v5, :cond_3

    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, p1

    iget v2, v0, Landroid/graphics/RectF;->right:F

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    :goto_1
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3dcccccd

    mul-float/2addr v0, v2

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v5, :cond_4

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->addToDiscardAmount(F)V

    goto :goto_0

    :cond_3
    const/high16 v1, 0x40000000

    const/high16 v2, 0x40800000

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    int-to-float v3, v3

    div-float v3, p1, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    const/high16 v2, -0x40800000

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, p2

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v2, p2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    goto :goto_1

    :cond_4
    move p3, p4

    goto :goto_2
.end method

.method private evenOutTabs(F)Z
    .locals 13

    const/4 v3, 0x1

    const/4 v12, 0x0

    const/high16 v5, 0x3f800000

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    cmpl-float v1, v1, v5

    if-gez v1, :cond_0

    cmpl-float v1, p1, v12

    if-nez v1, :cond_2

    :cond_0
    move v2, v0

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    sub-float v2, v5, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    sub-float v1, v5, v1

    div-float v5, v4, v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()I

    move-result v1

    int-to-float v6, v1

    move v1, v0

    move v2, v0

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v7, v7

    if-ge v0, v7, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v0

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v7

    iget v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v8, v0

    int-to-float v8, v8

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v8

    iget v9, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v9, v7

    invoke-direct {p0, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v9

    iget v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v10, v8

    invoke-direct {p0, v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->min(FF)F

    move-result v10

    cmpl-float v11, v9, v10

    if-nez v11, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v0

    invoke-virtual {v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    sub-float/2addr v8, v7

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v8, v7

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->min(FF)F

    move-result v8

    cmpl-float v8, v9, v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v8, v8, v0

    invoke-virtual {v8, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    goto :goto_2

    :cond_4
    sub-float v8, v10, v9

    mul-float/2addr v8, p1

    cmpl-float v8, v8, v12

    if-lez v8, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    move v2, v3

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    if-nez v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    add-float/2addr v0, v4

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    goto/16 :goto_0
.end method

.method private findTabById([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v2, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v3

    if-ne v3, p2, :cond_2

    aget-object v0, p1, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private finishAnimation(J)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onStackAnimationFinished()V

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackAnimation$OverviewAnimationType:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-void

    :pswitch_1
    const/16 v0, 0xd

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->springBack(J)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->propagateTabClosing()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_4

    move v0, v1

    move v2, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->releaseTabLayout(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    :cond_4
    :pswitch_4
    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move v0, v1

    :goto_3
    array-length v4, v3

    if-ge v1, v4, :cond_7

    aget-object v4, v3, v1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v3, v1

    aput-object v5, v4, v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    add-int/lit8 v0, v0, 0x1

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    sget-boolean v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    if-eq v0, v2, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private forceScrollStop()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateOverscrollOffset()V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    return-void
.end method

.method private getDiscardRange()F
    .locals 3

    const v1, 0x3f333333

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    :goto_0
    int-to-float v0, v0

    mul-float/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    goto :goto_0
.end method

.method private getMaxScroll(Z)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getMinScroll(Z)F
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v2, :cond_1

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v0

    :cond_2
    if-eqz p1, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:I

    neg-int v1, v0

    :cond_3
    int-to-float v0, v1

    sub-float/2addr v0, v2

    return v0
.end method

.method private getScrollDimensionSize()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    goto :goto_0
.end method

.method private getTabAtPositon(FF)I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FFF)I

    move-result v0

    return v0
.end method

.method private getTabAtPositon(FFF)I
    .locals 6

    const/4 v4, -0x1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    add-int/lit8 v2, v1, -0x1

    move v1, v0

    move v3, v4

    :goto_0
    if-ltz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->computeDistanceTo(FF)F

    move-result v0

    cmpg-float v5, v0, v1

    if-gez v5, :cond_2

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_1
    add-int/lit8 v2, v2, -0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v0

    move v0, v2

    :goto_2
    cmpg-float v1, v1, p3

    if-gtz v1, :cond_1

    :goto_3
    return v0

    :cond_1
    move v0, v4

    goto :goto_3

    :cond_2
    move v0, v1

    move v1, v3

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v1, v0

    move v0, v4

    goto :goto_2
.end method

.method private resetAllScrollOffset()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x40000000

    div-float v2, v0, v2

    const/high16 v3, 0x3f000000

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v4

    int-to-float v5, v4

    cmpg-float v5, v5, v2

    if-ltz v5, :cond_1

    int-to-float v5, v3

    cmpg-float v5, v5, v0

    if-gtz v5, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v3, v0

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    sub-int v5, v3, v4

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    cmpg-float v5, v5, v2

    if-gez v5, :cond_3

    int-to-float v2, v3

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1

    :cond_3
    int-to-float v0, v4

    sub-float v0, v2, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0
.end method

.method private resetInputActionIndices()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    return-void
.end method

.method private screenToScroll(F)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v0

    return v0
.end method

.method private scroll(FFFFZ)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p5, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    if-ltz v1, :cond_3

    if-eqz p5, :cond_4

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    :cond_4
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    if-nez v1, :cond_7

    move v1, p4

    move p4, v0

    :goto_2
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->evenOutTabs(F)Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    if-lez v2, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v2

    sub-float v1, v0, v1

    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0

    :cond_6
    move p4, p3

    goto :goto_1

    :cond_7
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    if-lez v1, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v0

    add-float/2addr v0, p4

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v0

    sub-float v1, v0, v1

    invoke-static {p4}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f000000

    mul-float/2addr v3, v4

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40000000

    mul-float/2addr v4, v5

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    mul-float/2addr v1, v2

    goto :goto_2

    :cond_8
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    if-gez v1, :cond_9

    if-nez p5, :cond_9

    move p4, v0

    move v1, v0

    goto :goto_2

    :cond_9
    move v1, p4

    goto :goto_2
.end method

.method private scrollToScreen(F)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->scrollToScreen(FF)F

    move-result v0

    return v0
.end method

.method private setScrollTarget(FZ)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->allowOverscroll()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v0

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    return-void
.end method

.method private smoothInput(FF)F
    .locals 2

    const/high16 v1, 0x41a00000

    sub-float v0, p2, v1

    add-float/2addr v1, p2

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    const v1, 0x3f666666

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->interpolate(FFF)F

    move-result v0

    return v0
.end method

.method private springBack(J)V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v0

    float-to-int v5, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v0

    float-to-int v6, v0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v2, v5

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v2, v6

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    float-to-int v2, v2

    move v3, v1

    move v4, v1

    move-wide v7, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->springBack(IIIIIIJ)Z

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v2, v5

    int-to-float v3, v6

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    :cond_1
    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V
    .locals 6

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V
    .locals 9

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stopScrollingMovement(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    if-eqz v0, :cond_2

    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCurrentTabIndex()I

    move-result v3

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v8

    move-object v1, p3

    move v4, p4

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createAnimatorSetForType(Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIIFFF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onStackAnimationStarted()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_1

    if-eqz p5, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;Z)V
    .locals 6

    const/4 v4, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    return-void
.end method

.method private stopScrollingMovement(J)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->computeScrollOffset(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0
.end method

.method private updateCurrentMode(I)V
    .locals 3

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3ecccccd

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createAnimationFactory(III)Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    return-void
.end method

.method private updateOverscrollOffset()V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->allowOverscroll()Z

    move-result v1

    if-nez v1, :cond_0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    sub-float v0, v1, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollDerivative:I

    if-eq v1, v2, :cond_2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    cmpg-float v2, v0, v3

    if-gez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    :cond_1
    :goto_0
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollDerivative:I

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    return-void

    :cond_2
    cmpl-float v2, v0, v3

    if-gtz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_3
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    goto :goto_0
.end method

.method private updateScrollOffset(J)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->consumeTiltScroll(J)V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->computeScrollOffset(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateOverscrollOffset()V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->smoothInput(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    goto :goto_1
.end method


# virtual methods
.method public cleanupTabs()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetInputActionIndices()V

    return-void
.end method

.method public click(JFF)V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTouchSlop()F

    move-result v1

    invoke-direct {p0, p3, p4, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FFF)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v3, v0, :cond_2

    :goto_1
    invoke-virtual {v2, p3, p4, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->checkCloseHitTest(FFZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v2, v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabClosing(JI)V

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->stackViewCloseTab()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v2, v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->tabSelecting(JI)V

    goto :goto_0
.end method

.method public computeTabPosition(JLandroid/graphics/RectF;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabScaleAlphaDepthHelper(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabScrollOffsetHelper()V

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabOffsetHelper(Landroid/graphics/RectF;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabTiltHelper(JLandroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabClippingVisibilityHelper()V

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabVisibilitySortingHelper(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3fa00000

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    const v1, 0x7f080013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:I

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    const v1, 0x7f08001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollSlide:I

    const/high16 v1, 0x3f800000

    const v2, 0x7f080014

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxTiltScrollSpeed:F

    const v1, 0x7f080012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinSpacing:F

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    return-void
.end method

.method public drag(JFFFF)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, p5

    move v1, p6

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDragLock(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->discard(FFFF)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    return-void

    :cond_0
    move v0, p6

    move v1, p5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-ltz v0, :cond_2

    invoke-direct {p0, p1, p2, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    :cond_2
    move-object v0, p0

    move v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scroll(FFFFZ)V

    goto :goto_1
.end method

.method public fling(JFFFF)V
    .locals 15

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v2

    const v3, 0x3ecccccd

    mul-float/2addr v2, v3

    const v3, 0x3cb60b61

    mul-float v3, v3, p5

    neg-float v4, v2

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTabIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->addToDiscardAmount(F)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move/from16 p5, p6

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    float-to-int v4, v4

    const/4 v5, 0x0

    move/from16 v0, p6

    float-to-int v6, v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v9

    float-to-int v9, v9

    const/4 v10, 0x0

    invoke-direct {p0, v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v10

    float-to-int v10, v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    cmpl-float v12, p6, v12

    if-lez v12, :cond_4

    iget v12, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:I

    :goto_3
    int-to-float v12, v12

    const/high16 v13, 0x3f000000

    mul-float/2addr v12, v13

    float-to-int v12, v12

    move-wide/from16 v13, p1

    invoke-virtual/range {v2 .. v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->fling(IIIIIIIIIIJ)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getFinalY()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_1

    :cond_3
    move/from16 p6, p5

    goto :goto_2

    :cond_4
    iget v12, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:I

    goto :goto_3
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    return-object v0
.end method

.method public getVisibleCount()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    :cond_2
    return v1
.end method

.method public isDisplayable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isListeningTiltScroll()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifySizeChanged(III)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWidth:I

    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mHeight:I

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateCurrentMode(I)V

    return-void
.end method

.method public onDown(J)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stopScrollingMovement(J)V

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    return-void
.end method

.method public onLongPress(JFF)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    if-ltz v0, :cond_0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    :cond_0
    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v3, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v3, v4, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v3, :cond_3

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    cmpl-float v3, p4, p6

    if-lez v3, :cond_7

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_a

    move/from16 v5, p5

    :goto_2
    if-eqz v3, :cond_b

    move/from16 v4, p6

    :goto_3
    if-eqz v3, :cond_c

    move/from16 v7, p3

    :goto_4
    if-eqz v3, :cond_d

    :goto_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_e

    move v3, v4

    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v8, 0x1

    if-ne v6, v8, :cond_f

    move/from16 v11, p4

    :goto_7
    if-eqz p7, :cond_4

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTabIndex:I

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    :cond_4
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v9, :cond_15

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v8

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)I

    move-result v4

    if-ltz v8, :cond_5

    if-gez v4, :cond_14

    :cond_5
    const/4 v8, -0x1

    const/4 v4, -0x1

    move v10, v4

    :goto_8
    if-ltz v8, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-ne v4, v8, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    if-ne v4, v10, :cond_6

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    invoke-static {v6, v4, v5}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v12

    if-lt v8, v10, :cond_11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    sub-float v4, v3, v4

    if-nez v8, :cond_10

    add-float/2addr v4, v12

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    :cond_6
    :goto_9
    move-object/from16 v0, p0

    iput v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch1Offset:F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    goto/16 :goto_0

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_8
    cmpl-float v3, p3, p5

    if-lez v3, :cond_9

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_a
    move/from16 v5, p3

    goto/16 :goto_2

    :cond_b
    move/from16 v4, p4

    goto/16 :goto_3

    :cond_c
    move/from16 v7, p5

    goto/16 :goto_4

    :cond_d
    move/from16 p4, p6

    goto/16 :goto_5

    :cond_e
    move v3, v5

    goto/16 :goto_6

    :cond_f
    move v11, v7

    goto/16 :goto_7

    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    add-float/2addr v5, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v5

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_9

    :cond_11
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    sub-float v4, v3, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v13

    add-float v9, v13, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch1Offset:F

    sub-float v4, v11, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v5

    add-float v14, v5, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v4

    add-float v6, v9, v13

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v6

    add-float/2addr v6, v12

    sub-float v4, v6, v4

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    move v4, v8

    move v6, v9

    move v7, v9

    :goto_a
    if-gt v4, v10, :cond_12

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v15, v15, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v15

    sub-float/2addr v15, v13

    sub-float v16, v5, v13

    div-float v15, v15, v16

    const/high16 v16, 0x3f800000

    sub-float v16, v16, v15

    mul-float v16, v16, v9

    mul-float/2addr v15, v14

    add-float v15, v15, v16

    invoke-static {v7, v15}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v15

    sget v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float v7, v15, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    add-float/2addr v6, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-object/from16 v16, v0

    aget-object v16, v16, v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_12
    sub-float v5, v14, v5

    add-int/lit8 v4, v10, 0x1

    :goto_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v14, v14

    if-ge v4, v14, :cond_13

    const/high16 v14, 0x40000000

    div-float/2addr v5, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v14, v14, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v14

    add-float/2addr v14, v5

    invoke-static {v7, v14}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v14

    sget v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float v7, v14, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v6, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    add-float/2addr v6, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v15, v15, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    invoke-virtual {v15, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :cond_13
    sub-float v5, v9, v13

    add-int/lit8 v4, v8, -0x1

    :goto_c
    if-lez v4, :cond_6

    const/high16 v6, 0x40000000

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    sub-float v6, v9, v6

    sget v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    sub-float v7, v9, v7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v13, v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v13

    add-float/2addr v13, v5

    invoke-static {v6, v13}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v6

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    sub-float/2addr v6, v13

    invoke-virtual {v7, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    add-int/lit8 v4, v4, -0x1

    goto :goto_c

    :cond_14
    move v10, v4

    goto/16 :goto_8

    :cond_15
    move v10, v6

    goto/16 :goto_8
.end method

.method public onTiltScroll(F)V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTiltScrollRemaining:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v0, p1

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    :cond_0
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-ltz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetInputActionIndices()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->springBack(J)V

    return-void
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 2

    if-nez p3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateScrollOffset(J)V

    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_2

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    :goto_0
    if-nez p3, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    :cond_2
    if-eqz p3, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->forceScrollStop()V

    :cond_3
    return v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_0
.end method

.method public requestRender()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestRender()V

    return-void
.end method

.method public requestUpdate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestUpdate()V

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    return-void
.end method

.method public setDeferredTabModel(Lcom/google/android/apps/chrome/tabs/DeferredTabModel;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDeferredTabModel:Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    return-void
.end method

.method public setStackFocusInfo(FI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderCloseButtonAlpha(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public show()V
    .locals 1

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRotationDirection:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createStackTabs()V

    return-void
.end method

.method public stackEntered(JZ)V
    .locals 3

    const/4 v1, 0x0

    if-nez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetAllScrollOffset()V

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public tabClosingEffect(JI)V
    .locals 7

    const/4 v5, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move v0, v1

    move v2, v1

    move v3, v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    if-ge v0, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v4

    if-ne v4, p3, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_2
    or-int/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDying(Z)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v4, v1

    goto :goto_2

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v4, v0

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v6, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    move v3, v4

    goto :goto_3

    :cond_4
    if-eqz v2, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    :cond_5
    if-nez v3, :cond_0

    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    goto :goto_0
.end method

.method public tabCreated(JI)V
    .locals 1

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createTabHelper(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NEW_TAB_OPENED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    goto :goto_0
.end method

.method public tabSelectingEffect(J)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    return-void
.end method

.method public tabsAllClosingEffect(J)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_1

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    :goto_1
    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDying(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v1

    goto :goto_1

    :cond_1
    move v2, v4

    :cond_2
    if-eqz v2, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    return-void
.end method

.method public updateSnap(J)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v1, :cond_1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method
