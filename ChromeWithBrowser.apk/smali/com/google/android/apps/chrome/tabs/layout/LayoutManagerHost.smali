.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;
.super Ljava/lang/Object;


# static fields
.field public static final LOG_CHROME_VIEW_SHOW_TIME:Z


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
.end method

.method public abstract getTitleCache()Lcom/google/android/apps/chrome/tabs/TitleCache;
.end method

.method public abstract getWidth()I
.end method

.method public abstract hide()V
.end method

.method public abstract requestRender()V
.end method

.method public abstract setContentOverlayVisibility(Z)V
.end method

.method public abstract setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
.end method

.method public abstract show()V
.end method
