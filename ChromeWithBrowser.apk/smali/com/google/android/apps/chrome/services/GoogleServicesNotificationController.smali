.class public Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;
.implements Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SIGNED_IN_TO_CHROME_ID:I = 0x3

.field private static final SNAPSHOT_NOTIFICATION_ID:I = 0x2

.field private static final SYNC_NOTIFICATION_ID:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GoogleServicesNotificationController"

.field private static final lock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

.field private final mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->$assertionsDisabled:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->lock:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    new-instance v1, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;-><init>(Landroid/app/NotificationManager;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getSnapshotMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;)Landroid/content/Intent;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createSettingsIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;ILjava/lang/String;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method private cancelNotification(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;->cancel(I)V

    return-void
.end method

.method public static createNewInstance(Landroid/content/Context;Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
    .locals 4

    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    sput-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    :goto_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    monitor-exit v1

    return-object v0

    :cond_0
    const-string v0, "GoogleServicesNotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GoogleServicesNotificationController already created. Currently on thread: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private createPasswordIntent()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method private createSettingsIntent()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method private formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
    .locals 2

    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSnapshotMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->getSummaryResourceId(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f070083

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private shouldSyncAuthErrorBeShown()Z
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$2;->$SwitchMap$com$google$android$apps$chrome$sync$GoogleServiceAuthError$State:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v1, "GoogleServicesNotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not showing unknown Auth Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const v1, 0x7f070251

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v2, p3, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x7f02007f

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    new-instance v1, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v1, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v1, p2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    invoke-interface {v1, p4, v0}, Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;->notify(ILandroid/app/Notification;)V

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    goto :goto_0
.end method

.method private updateNotification()V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSyncStatusNotification()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSnapshotStateNotification()V

    return-void
.end method

.method private updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V
    .locals 0

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2, p2, p3, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private updateSnapshotStateNotification()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$1;-><init>(Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateSyncStatusNotification()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->shouldSyncAuthErrorBeShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->getMessage()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createSettingsIntent()Landroid/content/Intent;

    move-result-object v0

    :goto_1
    const v2, 0x7f07007f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$2;->$SwitchMap$com$google$android$apps$chrome$sync$SyncDecryptionPassphraseType:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncDecryptionPassphraseTypeIfRequired()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    goto :goto_0

    :pswitch_0
    const v1, 0x7f070071

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createPasswordIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    const v1, 0x7f070070

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createPasswordIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public displayAndroidMasterSyncDisabledNotification()V
    .locals 3

    const v0, 0x7f07007f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f07006f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-direct {p0, v0, v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    return-void
.end method

.method public onClearSignedInUser()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public overrideNotificationManagerForTests(Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    return-void
.end method

.method public showOneOffNotification(ILjava/lang/String;)V
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, p1, p2, p2, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public showOneOffNotification(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_0
    const-string v0, "GoogleServicesNotificationController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unique ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " might override current notifications."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, p2, p3, p4, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    return-void
.end method

.method public snapshotStateChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateNotification()V

    return-void
.end method

.method public syncStateChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateNotification()V

    return-void
.end method
