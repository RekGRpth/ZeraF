.class Lcom/google/android/apps/chrome/NewTabPageToolbar$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/NewTabPageToolbar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelected(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/chrome/NewTabPageToolbar$3$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/chrome/NewTabPageToolbar$3$1;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar$3;Landroid/app/Activity;Landroid/accounts/Account;)V

    new-instance v3, Lcom/google/android/apps/chrome/NewTabPageToolbar$3$2;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/android/apps/chrome/NewTabPageToolbar$3$2;-><init>(Lcom/google/android/apps/chrome/NewTabPageToolbar$3;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onAccountSelectionCanceled()V
    .locals 0

    return-void
.end method

.method public onNewAccount()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/NewTabPageToolbar$3;->this$0:Lcom/google/android/apps/chrome/NewTabPageToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->startAccountRequest()V

    return-void
.end method
