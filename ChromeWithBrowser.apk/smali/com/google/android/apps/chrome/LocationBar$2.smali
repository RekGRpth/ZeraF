.class Lcom/google/android/apps/chrome/LocationBar$2;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1100(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1200(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1100(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$1200(Lcom/google/android/apps/chrome/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$2;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
