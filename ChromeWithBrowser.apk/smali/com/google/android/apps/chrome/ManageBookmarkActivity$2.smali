.class Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

.field final synthetic val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private finishAddEdit()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method


# virtual methods
.method public onCancel()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->finishAddEdit()V

    return-void
.end method

.method public onRemove()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->finishAddEdit()V

    return-void
.end method

.method public onSuccess(J)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->finishAddEdit()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->selectFolder(J)V

    :cond_0
    return-void
.end method

.method public setBackEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    # setter for: Lcom/google/android/apps/chrome/ManageBookmarkActivity;->mIsBackEnabled:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->access$102(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Z)Z

    return-void
.end method

.method public triggerFolderSelection()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getParentFolderId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isFolder()Z

    move-result v5

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->newInstance(ZJZ)Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v3, v4, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/ManageBookmarkActivity;

    # invokes: Lcom/google/android/apps/chrome/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/ManageBookmarkActivity;->access$000(Lcom/google/android/apps/chrome/ManageBookmarkActivity;Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)V

    if-eqz v0, :cond_1

    const-string v0, "SelectFolder"

    :goto_1
    const v1, 0x7f0f0085

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "AddFolderSelectFolder"

    goto :goto_1
.end method
