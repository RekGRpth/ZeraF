.class public Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;
.super Ljava/lang/Object;


# static fields
.field static final APPLICATION_VERSION_KEY:Ljava/lang/String; = "chrome_to_mobile_application_version"

.field static final AUTH_TOKEN_KEY:Ljava/lang/String; = "chrome_to_mobile_authToken"

.field public static final CURRENT_VERSION:J = 0x3L

.field static final ENABLED_KEY:Ljava/lang/String; = "chrome_to_mobile_enabled"

.field static final LAST_UPDATED_TIMESTAMP_KEY:Ljava/lang/String; = "chrome_to_mobile_last_updated_timestamp"

.field static final NEEDS_UPDATING_KEY:Ljava/lang/String; = "chrome_to_mobile_needs_updating"

.field static final PRINTER_ID_KEY:Ljava/lang/String; = "chrome_to_mobile_printerId"

.field private static final TAG:Ljava/lang/String;

.field static final VERSION_KEY:Ljava/lang/String; = "chrome_to_mobile_version"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearPrinterId(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_printerId"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getApplicationVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_application_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAuthToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_authToken"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastUpdatedTimestamp(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_last_updated_timestamp"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getPrinterId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_printerId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVersion(Landroid/content/Context;)J
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_version"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hasPrinterId(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnabled(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static needsUpdating(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_needs_updating"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setApplicationVersion(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_application_version"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static setAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_authToken"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static setEnabled(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_enabled"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-void
.end method

.method public static setLastUpdatedTimestamp(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_last_updated_timestamp"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static setNeedsUpdating(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_needs_updating"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-void
.end method

.method public static setPrinterId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_printerId"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static setVersion(Landroid/content/Context;J)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "chrome_to_mobile_version"

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;J)V
    .locals 1

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 1

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
