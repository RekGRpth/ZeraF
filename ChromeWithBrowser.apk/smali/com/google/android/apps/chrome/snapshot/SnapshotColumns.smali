.class public final Lcom/google/android/apps/chrome/snapshot/SnapshotColumns;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "createTime"

.field public static final DOWNLOAD_ID:Ljava/lang/String; = "downloadId"

.field public static final DOWNLOAD_URI:Ljava/lang/String; = "downloadUri"

.field public static final ID_PATH_POSITION:I = 0x1

.field public static final JOB_ID:Ljava/lang/String; = "jobId"

.field public static final LOCAL_URI:Ljava/lang/String; = "localUri"

.field public static final MIME_TYPE:Ljava/lang/String; = "mimeType"

.field public static final PAGE_URL_DOWNLOAD_ID:Ljava/lang/String; = "pageUrlDownloadId"

.field public static final PAGE_URL_DOWNLOAD_URI:Ljava/lang/String; = "pageUrlDownloadUri"

.field public static final PAGE_URL_JOB_ID:Ljava/lang/String; = "pageUrlJobId"

.field public static final PAGE_URL_MIME_TYPE:Ljava/lang/String; = "pageUrlMimeType"

.field public static final PAGE_URL_STATE:Ljava/lang/String; = "pageUrlState"

.field public static final PRINTER_ID:Ljava/lang/String; = "printerId"

.field public static final SNAPSHOT_ID:Ljava/lang/String; = "snapshotId"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final URI:Ljava/lang/String; = "uri"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
