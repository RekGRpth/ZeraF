.class public Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;
.super Ljava/lang/Object;


# instance fields
.field private mAllowScanningByMediaScanner:Z

.field private mDescription:Ljava/lang/String;

.field private mDownloadsDirectory:Ljava/lang/String;

.field private mFilename:Ljava/lang/String;

.field private mHeaderFields:Ljava/util/List;

.field private mTitle:Ljava/lang/String;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public allowScanningByMediaScanner()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mAllowScanningByMediaScanner:Z

    return-void
.end method

.method public asServiceRequest()Landroid/app/DownloadManager$Request;
    .locals 4

    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->getHeaderFields()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/HeaderField;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mDownloadsDirectory:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mFilename:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mAllowScanningByMediaScanner:Z

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/app/DownloadManager$Request;->allowScanningByMediaScanner()V

    :cond_1
    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to create folder for offline copies: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f070154

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderFields()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mHeaderFields:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mDescription:Ljava/lang/String;

    return-void
.end method

.method public setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mDownloadsDirectory:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mFilename:Ljava/lang/String;

    return-void
.end method

.method public setHeaders(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mHeaderFields:Ljava/util/List;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->mTitle:Ljava/lang/String;

    return-void
.end method
