.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addC2dmInformation(Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 3

    new-instance v0, Lcom/google/android/a/a/d;

    const-string v1, "remove_tag"

    const-string v2, "__c2dm__reg_id=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasC2dmRegistrationIdTag()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "__c2dm__reg_id"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getC2dmRegistrationIdTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/a/a/d;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private static addVersionInformation(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 3

    new-instance v0, Lcom/google/android/a/a/d;

    const-string v1, "remove_tag"

    const-string v2, "__version__application=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "__version__application"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getApplicationVersionTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/a/a/d;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/a/a/d;

    const-string v1, "remove_tag"

    const-string v2, "__version__protocol=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "__version__protocol"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProtocolVersionTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/a/a/d;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static getDescriptionOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "description"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static getIdOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static getNameOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static getProxyOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "proxy"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static getStatusOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "status"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static getStringParts(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Ljava/util/List;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "printerid"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "proxy"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProxy()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "printer"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "description"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "status"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "capabilities"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilities()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "default"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getDefaultValues()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "capsHash"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilitiesHash()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/a/a/d;

    const-string v2, "type"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0, v0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->addVersionInformation(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->addC2dmInformation(Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    return-object v0
.end method

.method private static getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/a/a/d;
    .locals 4

    new-instance v0, Lcom/google/android/a/a/d;

    const-string v1, "tag"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getTypeOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "type"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public static parse(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->createDefaultNonChangingPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parseTags(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getIdOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getProxyOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProxy(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTypeOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getNameOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withName(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getDescriptionOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withDescription(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getStatusOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method private static parseSplitTag(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;[Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "__version__application"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withApplicationVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "__version__protocol"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProtocolVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-string v0, "__c2dm__reg_id"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    goto :goto_0
.end method

.method private static parseTags(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5

    const-string v0, "tags"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->TAG:Ljava/lang/String;

    const-string v1, "Could not find any tags for printer"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object p0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parseSplitTag(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;[Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static toMultipartEntity(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/a/a/a;
    .locals 3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilitiesHash()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getStringParts(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/a/a/a;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/a/a/b;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/a/a/b;

    invoke-direct {v1, v0}, Lcom/google/android/a/a/a;-><init>([Lcom/google/android/a/a/b;)V

    move-object v0, v1

    goto :goto_0
.end method
