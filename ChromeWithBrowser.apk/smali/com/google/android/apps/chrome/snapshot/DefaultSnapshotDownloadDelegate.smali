.class public Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDownloadService:Landroid/app/DownloadManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    return-void
.end method

.method private static getDownloadError(Landroid/database/Cursor;)I
    .locals 1

    const-string v0, "reason"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method private getDownloadInfoCursor(J)Landroid/database/Cursor;
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/app/DownloadManager$Query;

    invoke-direct {v0}, Landroid/app/DownloadManager$Query;-><init>()V

    new-array v1, v3, [J

    const/4 v2, 0x0

    aput-wide p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->TAG:Ljava/lang/String;

    const-string v1, "Failed to find download in DownloadManager."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getDownloadMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    const-string v0, "media_type"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDownloadStatus(Landroid/database/Cursor;)I
    .locals 1

    const-string v0, "status"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public enqueue(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;)J
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->asServiceRequest()Landroid/app/DownloadManager$Request;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getUriForDownloadedFile(J)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/DownloadManager;->getUriForDownloadedFile(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public queryByDownloadId(J)Lcom/google/android/apps/chrome/snapshot/DownloadInfo;
    .locals 6

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadInfoCursor(J)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadStatus(Landroid/database/Cursor;)I

    move-result v3

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadError(Landroid/database/Cursor;)I

    move-result v4

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;

    move-wide v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;-><init>(JIILjava/lang/String;)V

    goto :goto_0
.end method
