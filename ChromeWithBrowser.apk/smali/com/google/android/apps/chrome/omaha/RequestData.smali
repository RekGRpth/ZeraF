.class public Lcom/google/android/apps/chrome/omaha/RequestData;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCreationTimestamp:J

.field private mRequestID:Ljava/lang/String;

.field private mSendInstallEvent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omaha/RequestData;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZJLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lcom/google/android/apps/chrome/omaha/RequestData;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mSendInstallEvent:Z

    iput-wide p2, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mCreationTimestamp:J

    iput-object p4, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mRequestID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAgeInMilliseconds(J)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mCreationTimestamp:J

    sub-long v0, p1, v0

    return-wide v0
.end method

.method public getAgeInSeconds(J)J
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/omaha/RequestData;->getAgeInMilliseconds(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mCreationTimestamp:J

    return-wide v0
.end method

.method public getRequestID()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mRequestID:Ljava/lang/String;

    return-object v0
.end method

.method public isSendInstallEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/RequestData;->mSendInstallEvent:Z

    return v0
.end method
