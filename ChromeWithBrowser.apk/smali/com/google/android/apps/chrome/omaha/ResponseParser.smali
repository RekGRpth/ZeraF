.class public Lcom/google/android/apps/chrome/omaha/ResponseParser;
.super Lorg/xml/sax/helpers/DefaultHandler;


# static fields
.field private static final EXTRA_TAGS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ResponseParser"


# instance fields
.field private mAppID:Ljava/lang/String;

.field private mDaystartSeconds:I

.field private mMarketVersion:Ljava/lang/String;

.field private mParsedApp:Z

.field private mParsedDaystart:Z

.field private mParsedInstallEvent:Z

.field private mParsedManifest:Z

.field private mParsedPing:Z

.field private mParsedResponse:Z

.field private mParsedURL:Z

.field private mParsedUpdateCheck:Z

.field private mURL:Ljava/lang/String;

.field private mUpdateStatus:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "action"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actions"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "package"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "packages"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "urls"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->EXTRA_TAGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mAppID:Ljava/lang/String;

    if-eqz p3, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedUpdateCheck:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedPing:Z

    :goto_0
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1, p0}, Ljavax/xml/parsers/SAXParser;->parse(Lorg/xml/sax/InputSource;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedPing:Z

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v1, "Failed to find <ping> tag"

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedInstallEvent:Z

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Hit IOException"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Hit ParserConfigurationException"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Hit SAXParseException"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Hit SAXException"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    return-void
.end method


# virtual methods
.method public getDaystartSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mDaystartSeconds:I

    return v0
.end method

.method public getMarketVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mMarketVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    return-object v0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->EXTRA_TAGS:[Ljava/lang/String;

    invoke-static {v0, p3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedResponse:Z

    if-nez v0, :cond_2

    const-string v0, "response"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "3.0"

    const-string v1, "protocol"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "prod"

    const-string v1, "server"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedResponse:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <response>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedDaystart:Z

    if-nez v0, :cond_4

    const-string v0, "daystart"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedDaystart:Z

    const-string v0, "elapsed_seconds"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mDaystartSeconds:I

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <daystart>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedApp:Z

    if-nez v0, :cond_6

    const-string v0, "app"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "ok"

    const-string v1, "status"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mAppID:Ljava/lang/String;

    const-string v1, "appid"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedApp:Z

    goto :goto_0

    :cond_5
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <app>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedInstallEvent:Z

    if-nez v0, :cond_8

    const-string v0, "event"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "ok"

    const-string v1, "status"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedInstallEvent:Z

    goto :goto_0

    :cond_7
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <event> for install"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedUpdateCheck:Z

    if-nez v0, :cond_d

    const-string v0, "updatecheck"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "status"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    const-string v0, "noupdate"

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    const-string v1, "error-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    :cond_9
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedURL:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedManifest:Z

    :cond_a
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedUpdateCheck:Z

    goto/16 :goto_0

    :cond_b
    const-string v0, "ok"

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Lorg/xml/sax/SAXException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<updatecheck> status was unrecognized:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mUpdateStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <updatecheck>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedURL:Z

    if-nez v0, :cond_f

    const-string v0, "url"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedURL:Z

    const-string v0, "codebase"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mURL:Ljava/lang/String;

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <url>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedManifest:Z

    if-nez v0, :cond_11

    const-string v0, "manifest"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedManifest:Z

    const-string v0, "version"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mMarketVersion:Ljava/lang/String;

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <manifest>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedPing:Z

    if-nez v0, :cond_13

    const-string v0, "ping"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "ok"

    const-string v1, "status"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/ResponseParser;->mParsedPing:Z

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lorg/xml/sax/SAXException;

    const-string v1, "Couldn\'t parse <ping>"

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    new-instance v0, Lorg/xml/sax/SAXException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found unexpected tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
