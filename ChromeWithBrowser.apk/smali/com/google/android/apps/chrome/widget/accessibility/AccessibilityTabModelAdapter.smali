.class public Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;
.super Landroid/widget/BaseAdapter;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mInternalListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

.field private mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTabModel:Lcom/google/android/apps/chrome/TabModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x5
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$1;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mInternalListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    new-instance v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter$2;-><init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;)Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    if-nez v0, :cond_0

    move-object p2, v1

    :goto_0
    return-object p2

    :cond_0
    if-eqz p2, :cond_1

    instance-of v2, p2, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;

    if-eqz v2, :cond_1

    check-cast p2, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;

    :goto_1
    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setTabData(Lcom/google/android/apps/chrome/Tab;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mInternalListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;->setListner(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040001

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem;

    move-object p2, v1

    goto :goto_1
.end method

.method public registerForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public setListener(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mListener:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    return-void
.end method

.method public setTabModel(Lcom/google/android/apps/chrome/TabModel;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public unregisterForNotifications()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityTabModelAdapter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method
