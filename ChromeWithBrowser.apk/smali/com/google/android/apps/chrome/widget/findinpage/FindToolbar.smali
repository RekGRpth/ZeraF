.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;
.implements Lcom/google/android/apps/chrome/Tab$FindResultListener;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mActive:Z

.field protected mCloseFindButton:Landroid/widget/ImageButton;

.field private mCurrentTab:Lcom/google/android/apps/chrome/Tab;

.field protected mFindNextButton:Landroid/widget/ImageButton;

.field protected mFindPrevButton:Landroid/widget/ImageButton;

.field private mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

.field private mFindStatus:Landroid/widget/TextView;

.field private mLastUserSearch:Ljava/lang/String;

.field private final mNoResultsBackgroundColor:I

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

.field private mSearchKeyShouldTriggerSearch:Z

.field private mSettingFindTextProgrammatically:Z

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private showKeyboardOnceWindowIsFocused:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0xc
        0x8
        0x24
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboardOnceWindowIsFocused:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNoResultsBackgroundColor:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 1

    const/16 v0, 0x2d

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->hideKeyboardAndStartFinding(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboard()V

    return-void
.end method

.method private clearResults()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, ""

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setStatus(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->clearMatchRects()V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    return-void
.end method

.method private hideKeyboardAndStartFinding(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v1}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/apps/chrome/Tab;->startFinding(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method private populateFindText(Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getPreviousFindText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method private sendHideFindToolbarNotification()V
    .locals 1

    const/16 v0, 0x2d

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    return-void
.end method

.method private setResultsBarVisibility(Z)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    goto :goto_0
.end method

.method private setStatus(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    new-instance v0, Landroid/graphics/drawable/PaintDrawable;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNoResultsBackgroundColor:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    const/high16 v1, 0x40800000

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadius(F)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showKeyboard()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->hasWindowFocus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboardOnceWindowIsFocused:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->showKeyboard(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->isViewAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/Tab;->setFindResultListener(Lcom/google/android/apps/chrome/Tab$FindResultListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/Tab;->setFindMatchRectsListener(Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->populateFindText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->requestFocus()Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboard()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->updateVisualsForTabModel(Z)V

    const/16 v0, 0x3b

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto :goto_0
.end method

.method public deactivate()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->setFindResultListener(Lcom/google/android/apps/chrome/Tab$FindResultListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->setFindMatchRectsListener(Lcom/google/android/apps/chrome/Tab$FindMatchRectsListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->stopFinding(I)V

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    goto :goto_0
.end method

.method protected findResultSelected(Landroid/graphics/Rect;)V
    .locals 0

    return-void
.end method

.method public getFindResultBar()Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    return-object v0
.end method

.method protected isViewAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFindMatchRects(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iget v1, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->version:I

    iget-object v2, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->rects:[Landroid/graphics/RectF;

    iget-object v3, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->activeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->clearMatchRects()V

    goto :goto_0
.end method

.method public onFindResult(Lorg/chromium/chrome/browser/FindNotificationDetails;)V
    .locals 7

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iput-boolean v2, v3, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    :cond_0
    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->activeMatchOrdinal:I

    if-eq v3, v0, :cond_1

    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-ne v3, v1, :cond_3

    :cond_1
    iget-boolean v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-nez v3, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-le v3, v1, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    :cond_4
    iget-boolean v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-eqz v3, :cond_6

    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-lez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v4, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    :cond_5
    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/Tab;->requestFindMatchRects(I)V

    :goto_1
    iget-object v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->rendererSelectionRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findResultSelected(Landroid/graphics/Rect;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f070252

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->activeMatchOrdinal:I

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setStatus(Ljava/lang/String;Z)V

    iget v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-nez v0, :cond_2

    iget-boolean v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getPreviousFindText()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "haptic_feedback_enabled"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_3
    if-eqz v1, :cond_2

    const-string v0, "vibrator"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3
.end method

.method public onFinishInflate()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setOrientation(I)V

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setGravity(I)V

    const v0, 0x7f0f003d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setFindToolbar(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setInputType(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setSelectAllOnFocus(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f0f003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    const v0, 0x7f0f003f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$4;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0040

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$5;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    const v0, 0x7f0f003c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCloseFindButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCloseFindButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$6;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onHideAnimationStart()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboardOnceWindowIsFocused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboardOnceWindowIsFocused:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$8;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public requestQueryFocus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->requestFocus()Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboard()V

    return-void
.end method

.method public setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method

.method public setFindText(Ljava/lang/String;)V
    .locals 0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->populateFindText(Ljava/lang/String;)V

    return-void
.end method

.method protected setPrevNextEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->updateVisualsForTabModel(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateVisualsForTabModel(Z)V
    .locals 0

    return-void
.end method
