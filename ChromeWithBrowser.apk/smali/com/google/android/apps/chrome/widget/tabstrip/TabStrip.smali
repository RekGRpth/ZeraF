.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;
.super Landroid/widget/FrameLayout;


# static fields
.field private static final COMPRESSED_CLICK_VISIBILITY_LIMIT_PERCENTAGE:F = 0.3f

.field private static final DOWN_DRAG_START_THRESHOLD_DP:F = 50.0f

.field private static final DRAG_END_DP:F = 18.4f

.field private static final DRAG_SCROLL_LEFT:I = 0x1

.field private static final DRAG_SCROLL_NONE:I = 0x0

.field private static final DRAG_SCROLL_RIGHT:I = 0x2

.field private static final DRAG_SPEED_DP:F = 1000.0f

.field private static final DRAG_START_DP:F = 87.4f

.field private static final EPSILON:F = 0.001f

.field public static final FADE_DURATION_MS:I = 0x4b

.field private static final MAX_TABS_TO_VISIBLY_STACK:I = 0x4

.field private static final MAX_TAB_WIDTH_DP:F = 270.0f

.field private static final MIN_TAB_WIDTH_DP:F = 200.0f

.field private static final NEW_TAB_BUTTON_OVERLAP_DP:F = 28.0f

.field private static final NEW_TAB_BUTTON_SPACE_DP:F = 47.0f

.field private static final NOTIFICATIONS:[I

.field private static final RESIZE_MESSAGE:I = 0x1

.field private static final RESIZE_TIMEOUT:I = 0x5dc

.field private static final TAB_DRAG_OVERLAP_PERCENTAGE_THRESHOLD:F = 0.53f

.field private static final TAB_END_STACK_WIDTH_DP:F = 4.0f

.field private static final TAB_OVERLAP_DP:F = 34.0f

.field private static final TAN_OF_TILT_FOR_DOWN_DRAG:F

.field private static final mMaxScrollOffset:F


# instance fields
.field private mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

.field private mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

.field private mCurrentTabWidth:F

.field private mCurrentlyFaded:Z

.field private mCurrentlyScrollingForFastExpand:Z

.field private mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mDraggedXOffset:F

.field private mFadeView:Landroid/view/View;

.field private final mGesture:Landroid/view/GestureDetector;

.field private final mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final mInternalHandler:Landroid/os/Handler;

.field private mLastDragScrollTimeMs:J

.field private mLastXDragPosition:F

.field private mMinScrollOffset:F

.field private mNewTabButton:Landroid/widget/ImageButton;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mRefreshTabVisualsDueToResize:Z

.field private mScrollFlags:I

.field private mScrollOffset:F

.field private final mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

.field private mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mStrokeView:Landroid/view/View;

.field private mTabModel:Lcom/google/android/apps/chrome/TabModel;

.field private final mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

.field private final mTabViews:Ljava/util/List;

.field private mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

.field private final mfDownDragStartThresholdDip:F

.field private final mfDragEndDip:F

.field private final mfDragSpeed:F

.field private final mfDragStartDip:F

.field private final mfMaxTabWidth:F

.field private final mfMinTabWidth:F

.field private final mfNewTabButtonOverlap:F

.field private final mfNewTabButtonSpace:F

.field private final mfTabEndStackWidth:F

.field private final mfTabHeight:F

.field private final mfTabOverlap:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x3fe921fb54442d18L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->TAN_OF_TILT_FOR_DOWN_DRAG:F

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    return-void

    :array_0
    .array-data 4
        0x2
        0x3
        0x5
        0x6
        0x1a
        0x1b
        0xa
        0x11
        0x12
        0x14
        0x15
        0x8
        0x9
        0x1c
        0x7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    iput v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40800000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabEndStackWidth:F

    const/high16 v1, 0x42080000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    const/high16 v1, 0x423c0000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    const/high16 v1, 0x43870000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMaxTabWidth:F

    const/high16 v1, 0x43480000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMinTabWidth:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabHeight:F

    const v1, 0x42aecccd

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragStartDip:F

    const v1, 0x41933333

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragEndDip:F

    const/high16 v1, 0x447a0000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragSpeed:F

    const/high16 v1, 0x41e00000

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonOverlap:F

    const/high16 v1, 0x42480000

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDownDragStartThresholdDip:F

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMaxTabWidth:F

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGesture:Landroid/view/GestureDetector;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/TabModel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabClosed(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCrashed(I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadStarted(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadFinished(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabTitleChanged(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IIIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabFaviconChanged(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/ContentViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadStarted(I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadFinished(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;FZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDownDragStartThresholdDip:F

    return v0
.end method

.method static synthetic access$3600()F
    .locals 1

    sget v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->TAN_OF_TILT_FOR_DOWN_DRAG:F

    return v0
.end method

.method static synthetic access$3700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startDrag(F)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    return v0
.end method

.method private calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    .locals 9

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_1

    :cond_0
    move v0, v3

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v0

    :goto_1
    neg-int v1, v4

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v2, v5

    mul-float/2addr v2, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    sub-float/2addr v1, v5

    add-int/lit8 v5, v4, 0x1

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v7, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v1, v5

    if-ge v4, v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v0, v4

    sub-float v0, v1, v0

    move v1, v2

    :goto_2
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_4

    if-eqz p3, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float v0, v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    :cond_3
    if-le v4, v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v0, v4

    add-float/2addr v0, v2

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_2

    :cond_4
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_5

    if-eqz p4, :cond_5

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float/2addr v0, v1

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_0

    :cond_6
    move v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private checkDragPosition()Z
    .locals 5

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const v2, 0x3f07ae14

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    cmpl-float v3, v3, v2

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    add-int/lit8 v3, v1, 0x2

    invoke-direct {p0, v2, v1, v3, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    add-int/lit8 v1, v1, 0x2

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/chrome/TabModel;->moveTab(II)V

    :goto_0
    return v0

    :cond_0
    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    neg-float v2, v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_1

    if-lez v1, :cond_1

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    add-int/lit8 v3, v1, -0x1

    invoke-direct {p0, v2, v1, v3, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/chrome/TabModel;->moveTab(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createTabViewForTab(Lcom/google/android/apps/chrome/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabHeight:F

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->initializeControl(Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;Lcom/google/android/apps/chrome/Tab;FZ)V

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTabWidth(FZ)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v2

    float-to-int v2, v2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleAnimationUpdates()V
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/OverScroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getCurrX()I

    move-result v0

    int-to-float v0, v0

    move v3, v0

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    if-eqz v4, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragStartDip:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragStartDip:F

    sub-float/2addr v7, v8

    cmpg-float v8, v5, v6

    if-gez v8, :cond_4

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragEndDip:F

    invoke-static {v5, v0, v6}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v5

    sub-float v5, v6, v5

    sub-float v0, v6, v0

    div-float v0, v5, v0

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragSpeed:F

    mul-float/2addr v0, v5

    mul-float/2addr v0, v4

    add-float/2addr v3, v0

    move v0, v1

    :cond_1
    :goto_2
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    :cond_2
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->update()Z

    move-result v7

    or-int/2addr v4, v7

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v7, v8, :cond_2

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    sub-long v7, v5, v7

    long-to-float v4, v7

    const/high16 v7, 0x447a0000

    div-float/2addr v4, v7

    iput-wide v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    goto :goto_1

    :cond_4
    cmpl-float v6, v5, v7

    if-lez v6, :cond_5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragStartDip:F

    add-float/2addr v0, v7

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragEndDip:F

    sub-float/2addr v0, v6

    invoke-static {v5, v7, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v5

    sub-float/2addr v5, v7

    sub-float/2addr v0, v7

    div-float v0, v5, v0

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfDragSpeed:F

    mul-float/2addr v0, v5

    mul-float/2addr v0, v4

    sub-float/2addr v3, v0

    move v0, v1

    goto :goto_2

    :cond_5
    iput-wide v9, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    goto :goto_2

    :cond_6
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->die()V

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/chrome/TabModel;->closeTabById(I)Z

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v0, v6, :cond_7

    move v2, v1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_9

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_9

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    :cond_9
    if-nez v4, :cond_a

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_b

    :cond_a
    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(FZZ)V

    :cond_b
    return-void

    :cond_c
    move v3, v0

    move v0, v2

    goto/16 :goto_0
.end method

.method private handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v7

    :pswitch_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->stopDrag()V

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    float-to-int v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    float-to-int v3, v3

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/third_party/OverScroller;->springBack(IIIIII)Z

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-lez v1, :cond_0

    if-lez v0, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->checkDragPosition()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    goto :goto_1

    :pswitch_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private refreshTabStrip()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeAllViews()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v3, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->createTabViewForTab(Lcom/google/android/apps/chrome/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    if-ne v0, v2, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-gt v0, v2, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v3, v5, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v3, v2, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v5, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    invoke-direct {p0, v6, v6}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    return-void
.end method

.method private resetResizeTimeout(Z)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    return-void
.end method

.method private setBackgroundTabFading(Z)V
    .locals 7

    const-wide/16 v5, 0x4b

    const/16 v3, 0x4b

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    const-string v1, "alpha"

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    const-string v1, "alpha"

    new-array v2, v2, [F

    const/high16 v3, 0x3f800000

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    goto :goto_1
.end method

.method private setScrollOffsetPosition(FZZ)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float/2addr v2, v0

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->checkDragPosition()Z

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    :cond_2
    return-void
.end method

.method private startDrag(F)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    iput v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0, v0, v2, v2, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/TabModel;->setIndex(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x2e

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startFastExpand(I)V
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2, p1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    goto :goto_0
.end method

.method private stopDrag()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    const/16 v0, 0x2f

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    :cond_0
    return-void
.end method

.method private tabClosed(I)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v3, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeView(Landroid/view/View;)V

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    :goto_1
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    goto :goto_1
.end method

.method private tabCrashed(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->crashed()V

    :cond_0
    return-void
.end method

.method private tabCreated(IZ)V
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    if-le v0, v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-le v0, v1, :cond_6

    move v0, v1

    :goto_1
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->createTabViewForTab(Lcom/google/android/apps/chrome/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v5, v4}, Lcom/google/android/apps/chrome/TabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v5, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v6

    float-to-int v6, v6

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz p2, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->unselected()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    :cond_3
    if-eqz v0, :cond_4

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    if-eqz v4, :cond_5

    invoke-direct {p0, v4, v2, v2, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_5

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move-object v0, v3

    goto :goto_2
.end method

.method private tabFaviconChanged(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method private tabLoadFinished(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->loadingFinished()V

    :cond_0
    return-void
.end method

.method private tabLoadStarted(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->loadingStarted()V

    :cond_0
    return-void
.end method

.method private tabMoved(IIIZ)V
    .locals 5

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, p3, :cond_0

    if-eq v1, p2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eq v0, v3, :cond_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    if-ge v1, p3, :cond_3

    add-int/lit8 p3, p3, -0x1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-eqz p4, :cond_7

    if-ge p3, p2, :cond_4

    add-int/lit8 v0, p3, 0x1

    move v4, v0

    :goto_1
    if-ge p3, p2, :cond_5

    const/4 v0, -0x1

    move v1, v0

    :goto_2
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->finishAnimation()V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    add-int/lit8 v0, p3, -0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eq v0, v3, :cond_7

    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v3, v4

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateSlideFrom(F)V

    :cond_7
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    goto :goto_0
.end method

.method private tabPageLoadFinished(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->pageLoadingFinished()V

    :cond_0
    return-void
.end method

.method private tabPageLoadStarted(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->pageLoadingStarted()V

    :cond_0
    return-void
.end method

.method private tabSelected(IZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v0

    if-ne v0, p1, :cond_1

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    if-eq v1, p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->stopDrag()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->unselected()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    :cond_4
    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    :cond_5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    goto :goto_0
.end method

.method private tabTitleChanged(ILjava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method private updateCurrentTabWidth(IZ)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    add-int/lit8 v3, v1, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    int-to-float v2, v1

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v2

    if-nez v2, :cond_0

    if-ne v1, v4, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMaxTabWidth:F

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMaxTabWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfMinTabWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTabWidth(FZ)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateScrollOffsetLimits(FZZ)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v0

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabHeight:F

    div-float/2addr v0, v4

    sub-float v0, v3, v0

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v2, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const v1, -0x457ced91

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const v1, 0x3a83126f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iput v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    return-void
.end method

.method private updateScrollOffsetLimits(ZZ)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(FZZ)V

    return-void
.end method

.method private updateTabStripPositions(Z)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    move v3, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabEndStackWidth:F

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float/2addr v1, v2

    move v4, v1

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabEndStackWidth:F

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonSpace:F

    sub-float v6, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v8

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object v9, v5

    move v10, v7

    move v11, v8

    move v8, v2

    move v7, v1

    :goto_2
    if-ge v7, v13, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v15

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v2, :cond_0

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    float-to-int v5, v15

    if-eq v2, v5, :cond_1

    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v5, v15

    const/4 v12, -0x1

    invoke-direct {v2, v5, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    if-gt v7, v3, :cond_d

    const/4 v2, 0x4

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v5, v2

    :goto_3
    if-lt v7, v3, :cond_e

    add-int/lit8 v2, v13, -0x1

    sub-int/2addr v2, v7

    const/4 v12, 0x4

    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_4
    int-to-float v5, v5

    mul-float v12, v14, v5

    int-to-float v2, v2

    mul-float/2addr v2, v14

    sub-float v2, v6, v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v1, v5, :cond_f

    add-int/lit8 v5, v13, -0x1

    if-lt v7, v5, :cond_2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const/16 v16, 0x0

    cmpg-float v5, v5, v16

    if-gez v5, :cond_f

    :cond_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    add-float/2addr v5, v11

    :goto_5
    sub-float v16, v2, v15

    move/from16 v0, v16

    invoke-static {v5, v12, v0}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v16

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-result-object v5

    sget-object v17, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-object/from16 v0, v17

    if-eq v5, v0, :cond_3

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTranslationX(F)V

    :cond_3
    if-eqz v9, :cond_11

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    float-to-int v5, v5

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v5, v0, :cond_11

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v5, v0, :cond_11

    if-gt v7, v3, :cond_10

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    const/16 v17, 0x4

    move/from16 v0, v17

    if-eq v5, v0, :cond_10

    const/4 v5, 0x4

    invoke-virtual {v9, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    :cond_4
    :goto_6
    if-ne v7, v3, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_9

    const/high16 v5, 0x3f800000

    if-eq v7, v3, :cond_7

    if-le v7, v3, :cond_13

    add-float v5, v12, v4

    :goto_7
    if-ge v7, v3, :cond_6

    sub-float/2addr v2, v4

    :cond_6
    add-float v9, v11, v15

    invoke-static {v9, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v11, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    sub-float/2addr v2, v5

    div-float/2addr v2, v15

    const/4 v5, 0x0

    const/high16 v9, 0x3f800000

    invoke-static {v2, v5, v9}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v2

    move v5, v2

    :cond_7
    if-le v7, v3, :cond_8

    add-int/lit8 v2, v3, 0x1

    if-ne v7, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v2, :cond_14

    :cond_8
    const/4 v2, 0x0

    :goto_8
    move/from16 v0, p1

    invoke-virtual {v1, v2, v5, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->handleLayout(FFZ)V

    :cond_9
    const/high16 v2, 0x3f800000

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabHeight:F

    div-float/2addr v5, v9

    sub-float/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    sub-float v5, v15, v5

    mul-float/2addr v2, v5

    const/4 v5, 0x0

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    add-float v5, v16, v2

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v8

    add-float v9, v11, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v1, v5, :cond_a

    add-int/lit8 v5, v13, -0x1

    if-ne v7, v5, :cond_a

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const/4 v10, 0x0

    cmpg-float v5, v5, v10

    if-gez v5, :cond_a

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    sub-float/2addr v2, v5

    :cond_a
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v10, v2

    move v11, v9

    move-object v9, v1

    goto/16 :goto_2

    :cond_b
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_1

    :cond_d
    const/4 v2, 0x4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v5, 0x4

    sub-int v12, v7, v3

    invoke-static {v5, v12}, Ljava/lang/Math;->min(II)I

    move-result v5

    add-int/2addr v2, v5

    move v5, v2

    goto/16 :goto_3

    :cond_e
    add-int/lit8 v2, v13, -0x1

    sub-int/2addr v2, v3

    const/4 v12, 0x4

    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int v12, v3, v7

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    add-int/2addr v2, v12

    goto/16 :goto_4

    :cond_f
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getOffsetX()F

    move-result v5

    add-float/2addr v5, v11

    goto/16 :goto_5

    :cond_10
    if-le v7, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    const/4 v9, 0x4

    if-eq v5, v9, :cond_4

    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_11
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    float-to-int v5, v5

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v5, v0, :cond_4

    if-gt v7, v3, :cond_12

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_12

    const/4 v5, 0x0

    invoke-virtual {v9, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_12
    if-le v7, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_13
    move v5, v12

    goto/16 :goto_7

    :cond_14
    sub-float v2, v10, v16

    const/4 v9, 0x0

    invoke-static {v2, v9}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float/2addr v2, v15

    goto/16 :goto_8

    :cond_15
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabOverlap:F

    add-float/2addr v1, v8

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_16

    move v1, v6

    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfNewTabButtonOverlap:F

    sub-float/2addr v1, v3

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setTranslationX(F)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getHeight()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mfTabHeight:F

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTranslationY(F)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    return-void
.end method


# virtual methods
.method public getTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const-string v0, "TabStrip:onDrawFPS"

    invoke-static {v0}, Lorg/chromium/content/common/PerfTraceEvent;->instant(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleAnimationUpdates()V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->requestLayout()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f00bd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f00be

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    const v0, 0x7f0f00bf

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeAllViewsInLayout()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    move v1, v0

    :goto_0
    move v3, v2

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-gt v3, v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v0, v6, v4, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v4, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v6, v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    invoke-direct {p0, v3, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public refreshSelection()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    :cond_0
    return-void
.end method

.method public setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mChromeViewHolder:Lcom/google/android/apps/chrome/ContentViewHolder;

    return-void
.end method

.method public setTabModel(Lcom/google/android/apps/chrome/TabModel;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lcom/google/android/apps/chrome/TabModel;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    return-void
.end method

.method public testFling(F)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, p1, v1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    return-void
.end method

.method public testSetScrollOffset(F)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    return-void
.end method
