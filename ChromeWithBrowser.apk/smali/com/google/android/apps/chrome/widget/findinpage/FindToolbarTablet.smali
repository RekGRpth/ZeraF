.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;
.super Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# static fields
.field private static final HORIZONTAL_ANIMATION_DURATION_MS:I = 0xc8

.field private static final VERTICAL_ANIMATION_DURATION_MS:I = 0xfa


# instance fields
.field private mActivationText:Ljava/lang/String;

.field private mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mBufferSpace:I

.field private mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;Lcom/google/android/apps/chrome/ChromeAnimation;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    return-void
.end method

.method private setShowState(Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->invalidate()V

    :cond_1
    return-void

    :cond_2
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->onHideAnimationStart()V

    goto :goto_0
.end method

.method private superActivate(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->isViewAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setShowState(Z)V

    goto :goto_0
.end method

.method public deactivate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setShowState(Z)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getTranslationY()F

    move-result v1

    neg-float v1, v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$4;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public findResultSelected(Landroid/graphics/Rect;)V
    .locals 9

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findResultSelected(Landroid/graphics/Rect;)V

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mBufferSpace:I

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getLeft()I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getHeight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v1, v2, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    if-eqz p1, :cond_0

    invoke-static {v1, p1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, v0

    if-lez v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mBufferSpace:I

    sub-int/2addr v0, v1

    int-to-float v3, v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getTranslationX()F

    move-result v0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$3;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v8, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getTranslationX()F

    move-result v2

    const-wide/16 v4, 0xc8

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 10

    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mBufferSpace:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$1;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v9, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;

    neg-int v1, v8

    int-to-float v2, v1

    const/4 v3, 0x0

    const-wide/16 v4, 0xfa

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$2;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v9, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;

    const/4 v2, 0x0

    neg-int v1, v8

    int-to-float v3, v1

    const-wide/16 v4, 0xfa

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setVisibility(I)V

    return-void
.end method
