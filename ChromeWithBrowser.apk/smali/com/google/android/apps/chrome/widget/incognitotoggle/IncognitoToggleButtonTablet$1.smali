.class Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;->this$0:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;->this$0:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    # getter for: Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->access$000(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;->this$0:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    # getter for: Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->access$000(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;->this$0:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    # getter for: Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->access$000(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModelSelector;->selectModel(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
