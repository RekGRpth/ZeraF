.class public Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lcom/google/android/apps/chrome/widget/lazyloading/LazilyLoadable;


# static fields
.field private static final ANIMATE_DURATION_FADE_MS:I = 0x1e

.field private static final ANIMATE_DURATION_MS:I = 0xc8


# instance fields
.field private mAnimateInAnimation:Landroid/animation/AnimatorSet;

.field private mAnimateOutAnimation:Landroid/animation/AnimatorSet;

.field private mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private mWrapper:Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method private buildAnimatorSets()V
    .locals 13

    const v0, 0x7f0f003a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0038

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0039

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020093

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    const-string v6, "alpha"

    const/4 v7, 0x2

    new-array v7, v7, [F

    fill-array-data v7, :array_0

    invoke-static {p0, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v7, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "translationY"

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    neg-float v10, v3

    aput v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-static {v0, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-string v8, "translationY"

    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    neg-float v11, v4

    aput v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v9, v10

    invoke-static {v1, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const-wide/16 v9, 0xc8

    invoke-virtual {v8, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const-string v9, "translationX"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v5, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x0

    aput v12, v10, v11

    invoke-static {v2, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    const-wide/16 v10, 0xc8

    invoke-virtual {v9, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v9

    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Animator;

    const/4 v12, 0x0

    aput-object v7, v11, v12

    const/4 v7, 0x1

    aput-object v8, v11, v7

    const/4 v7, 0x2

    aput-object v9, v11, v7

    invoke-virtual {v10, v11}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateInAnimation:Landroid/animation/AnimatorSet;

    iget-object v7, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateInAnimation:Landroid/animation/AnimatorSet;

    const/4 v8, 0x2

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v6, 0x1

    aput-object v10, v8, v6

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateInAnimation:Landroid/animation/AnimatorSet;

    new-instance v7, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$3;

    invoke-direct {v7, p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$3;-><init>(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-string v6, "alpha"

    const/4 v7, 0x2

    new-array v7, v7, [F

    fill-array-data v7, :array_1

    invoke-static {p0, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v7, 0x1e

    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v7, "translationY"

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x1

    neg-float v3, v3

    aput v3, v8, v9

    invoke-static {v0, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v7, 0xc8

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string v3, "translationY"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    neg-float v4, v4

    aput v4, v7, v8

    invoke-static {v1, v3, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-string v3, "translationX"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v4, v7

    const/4 v7, 0x1

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v3, 0xc8

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateOutAnimation:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateOutAnimation:Landroid/animation/AnimatorSet;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v6, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateOutAnimation:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$4;-><init>(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mWrapper:Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;->uninitialize()V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f0038

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$1;-><init>(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f003a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet$2;-><init>(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->buildAnimatorSets()V

    return-void
.end method

.method public setEmptyContainerState(Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateInAnimation:Landroid/animation/AnimatorSet;

    if-eq v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateInAnimation:Landroid/animation/AnimatorSet;

    invoke-static {p0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_2
    return-void

    :cond_3
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mCurrentTransitionAnimation:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateOutAnimation:Landroid/animation/AnimatorSet;

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mAnimateOutAnimation:Landroid/animation/AnimatorSet;

    goto :goto_0
.end method

.method public setLazyViewLoader(Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mWrapper:Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    const v0, 0x7f0f0039

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    return-void
.end method
