.class Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStateChangeListenerImpl;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout$AccessibilityStateChangeListenerImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessibilityStateChanged(Z)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "enabled"

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->shouldUseAccessibilityMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0x3e

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method
