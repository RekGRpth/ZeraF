.class public Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CONFIDENCE_THRESHOLD_HIDE_ALTS:F = 0.8f

.field private static final CONFIDENCE_THRESHOLD_SHOW:F = 0.3f


# instance fields
.field private final mConfidenceThresholdHideAlts:F

.field private final mConfidenceThresholdShow:F

.field private mResults:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    const v0, 0x3e99999a

    iput v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    const v0, 0x3f4ccccd

    iput v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    return-void
.end method

.method protected constructor <init>(FF)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    iput p1, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    iput p2, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    return-void
.end method

.method private addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;F)V
    .locals 8

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->doesVoiceResultHaveMatch(Ljava/util/List;Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    cmpg-float v0, v0, p3

    if-gez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion;

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->nativeType()I

    move-result v1

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/chrome/LocationBar;->getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/OmniboxSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private doesVoiceResultHaveMatch(Ljava/util/List;Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;)Z
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addVoiceSuggestions([Lcom/google/android/apps/chrome/OmniboxSuggestion;I)[Lcom/google/android/apps/chrome/OmniboxSuggestion;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, p1

    :goto_0
    return-object v0

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    array-length v0, p1

    if-lez v0, :cond_2

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;

    const/4 v2, 0x0

    invoke-direct {p0, v3, v0, v2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;F)V

    if-eqz p1, :cond_3

    array-length v1, p1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    const/4 v0, 0x1

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int v4, v1, p2

    if-ge v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;

    iget v4, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;F)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/chrome/OmniboxSuggestion;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/OmniboxSuggestion;

    goto :goto_0
.end method

.method public clearVoiceSearchResults()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public getResults()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setVoiceResultsFromIntentBundle(Landroid/os/Bundle;)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->clearVoiceSearchResults()V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const-string v0, "android.speech.extra.CONFIDENCE_SCORES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    array-length v1, v3

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    array-length v1, v3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, " "

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;

    if-nez v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_3
    aget v4, v3, v1

    invoke-direct {v6, v0, v4}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;-><init>(Ljava/lang/String;F)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
