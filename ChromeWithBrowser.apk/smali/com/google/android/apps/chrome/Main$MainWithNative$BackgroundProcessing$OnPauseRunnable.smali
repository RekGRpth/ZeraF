.class Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->this$2:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Lcom/google/android/apps/chrome/Main$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;-><init>(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->this$2:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mOnPauseRunnable:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$2202(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;)Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;

    sget-boolean v0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->this$2:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # getter for: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$2300(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing$OnPauseRunnable;->this$2:Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;

    # setter for: Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->mWebKitTimersAreSuspended:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;->access$2302(Lcom/google/android/apps/chrome/Main$MainWithNative$BackgroundProcessing;Z)Z

    invoke-static {v2}, Lorg/chromium/chrome/browser/ProcessUtils;->toggleWebKitSharedTimers(Z)V

    return-void
.end method
