.class Lcom/google/android/apps/chrome/ToolbarPhone$1;
.super Landroid/util/Property;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarPhone$1;->this$0:Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/lang/Integer;
    .locals 1

    # getter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$000(Lcom/google/android/apps/chrome/ToolbarPhone;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone$1;->get(Lcom/google/android/apps/chrome/ToolbarPhone;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/chrome/ToolbarPhone;Ljava/lang/Integer;)V
    .locals 1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/ToolbarPhone;->mLocationBarBgRightMargin:I
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/ToolbarPhone;->access$002(Lcom/google/android/apps/chrome/ToolbarPhone;I)I

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/ToolbarPhone;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/ToolbarPhone$1;->set(Lcom/google/android/apps/chrome/ToolbarPhone;Ljava/lang/Integer;)V

    return-void
.end method
