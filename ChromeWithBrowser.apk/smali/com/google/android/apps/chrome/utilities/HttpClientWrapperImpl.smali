.class public Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;


# instance fields
.field private final httpClient:Landroid/net/http/AndroidHttpClient;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->httpClient:Landroid/net/http/AndroidHttpClient;

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->httpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    return-void
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->httpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0, p1}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method
