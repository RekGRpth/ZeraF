.class public Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;
.super Ljava/lang/Object;


# static fields
.field private static final MAX_EXPONENT:I = 0xa

.field private static final PREFERENCE_DELAY:Ljava/lang/String; = "delay"

.field private static final PREFERENCE_FAILED_ATTEMPTS:Ljava/lang/String; = "backoffFailedAttempts"

.field private static TAG:Ljava/lang/String;

.field private static random:Ljava/util/Random;


# instance fields
.field private mBaseMilliseconds:J

.field private mContext:Landroid/content/Context;

.field private mMaxMilliseconds:J

.field private mPreferencePackage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ExponentialBackoffScheduler"

    sput-object v0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->random:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mPreferencePackage:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    iput-wide p3, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mBaseMilliseconds:J

    iput-wide p5, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mMaxMilliseconds:J

    return-void
.end method

.method private computeConstrainedBackoffCoefficient(I)I
    .locals 2

    const/16 v0, 0xa

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x1

    shl-int v0, v1, v0

    sget-object v1, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->random:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private generateRandomDelay()J
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getNumFailedAttempts()I

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mBaseMilliseconds:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mMaxMilliseconds:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "delay"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-wide v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->computeConstrainedBackoffCoefficient(I)I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mBaseMilliseconds:J

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mMaxMilliseconds:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mPreferencePackage:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancelAlarm(Landroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    const/high16 v2, 0x20000000

    invoke-static {v1, v0, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public createAlarm(Landroid/content/Intent;)J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->generateRandomDelay()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public createAlarm(Landroid/content/Intent;J)J
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, p1, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {p0, v0, p2, p3, v1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->setAlarm(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;)V

    return-wide p2
.end method

.method public getCurrentTime()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGeneratedDelay()J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "delay"

    iget-wide v2, p0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->mBaseMilliseconds:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNumFailedAttempts()I
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "backoffFailedAttempts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public increaseFailedAttempts()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getNumFailedAttempts()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "backoffFailedAttempts"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public resetFailedAttempts()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "backoffFailedAttempts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected setAlarm(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;)V
    .locals 5

    sget-object v0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "now("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") refiringAt("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method
