.class Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;
.super Ljava/lang/Object;


# instance fields
.field private final mAllocationStack:Ljava/lang/Throwable;

.field private final mClazz:Ljava/lang/Class;

.field private final mCleanupTask:Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;

.field private final mDescription:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/apps/chrome/utilities/LeakDetector;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/utilities/LeakDetector;Ljava/lang/Class;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->this$0:Lcom/google/android/apps/chrome/utilities/LeakDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mClazz:Ljava/lang/Class;

    iput-object p3, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mDescription:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mAllocationStack:Ljava/lang/Throwable;

    iput-object p4, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mCleanupTask:Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/utilities/LeakDetector;Ljava/lang/Class;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;Lcom/google/android/apps/chrome/utilities/LeakDetector$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;-><init>(Lcom/google/android/apps/chrome/utilities/LeakDetector;Ljava/lang/Class;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mAllocationStack:Ljava/lang/Throwable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mCleanupTask:Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mDescription:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mClazz:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/LeakDetector$LeakInfo;->mDescription:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
