.class public final enum Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum HISTORY_BODY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum HISTORY_KEYWORD:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum HISTORY_TITLE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum HISTORY_URL:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum NAVSUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum OPEN_HISTORY_PAGE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum SEARCH_HISTORY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum SEARCH_OTHER_ENGINE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum SEARCH_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum URL_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field public static final enum VOICE_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;


# instance fields
.field private final mNativeType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "VOICE_SUGGEST"

    const/16 v2, -0x64

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "URL_WHAT_YOU_TYPED"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "HISTORY_URL"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_URL:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "HISTORY_TITLE"

    invoke-direct {v0, v1, v7, v6}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_TITLE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "HISTORY_BODY"

    invoke-direct {v0, v1, v8, v7}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_BODY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "HISTORY_KEYWORD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_KEYWORD:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "NAVSUGGEST"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->NAVSUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "SEARCH_WHAT_YOU_TYPED"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "SEARCH_HISTORY"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "SEARCH_SUGGEST"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "SEARCH_OTHER_ENGINE"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_OTHER_ENGINE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    new-instance v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const-string v1, "OPEN_HISTORY_PAGE"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->OPEN_HISTORY_PAGE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_URL:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_TITLE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_BODY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_KEYWORD:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->NAVSUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_SUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->SEARCH_OTHER_ENGINE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->OPEN_HISTORY_PAGE:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->$VALUES:[Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->mNativeType:I

    return-void
.end method

.method static GetTypeFromNativeType(I)Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->values()[Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget v4, v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->mNativeType:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->mNativeType:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->$VALUES:[Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    return-object v0
.end method


# virtual methods
.method public final isUrl()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->HISTORY_URL:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->NAVSUGGEST:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nativeType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->mNativeType:I

    return v0
.end method
