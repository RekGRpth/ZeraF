.class public Lcom/google/android/apps/chrome/bridge/AutocompleteController;
.super Ljava/lang/Object;


# static fields
.field private static final MAX_DEFAULT_SUGGESTION_COUNT:I = 0x5

.field private static final MAX_VOICE_SUGGESTION_COUNT:I = 0x3


# instance fields
.field private mListener:Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;

.field private mNativeAutocompleteBridge:I

.field private mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeInit(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mListener:Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;-><init>(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;)V

    return-void
.end method

.method public static native nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeStart(ILjava/lang/String;Ljava/lang/String;ZZZZ)V
.end method

.method private native nativeStop(IZ)V
.end method

.method private notifyNativeDestroyed()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    return-void
.end method


# virtual methods
.method protected native nativeInit(Lcom/google/android/apps/chrome/Tab;)I
.end method

.method public onSuggestionsReceived([Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x5

    array-length v0, p1

    if-le v0, v1, :cond_0

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/OmniboxSuggestion;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->addVoiceSuggestions([Lcom/google/android/apps/chrome/OmniboxSuggestion;I)[Lcom/google/android/apps/chrome/OmniboxSuggestion;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mListener:Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;

    invoke-interface {v1, v0, p2}, Lcom/google/android/apps/chrome/bridge/AutocompleteController$OnSuggestionsReceivedListener;->onSuggestionsReceived([Lcom/google/android/apps/chrome/OmniboxSuggestion;Ljava/lang/String;)V

    const/16 v0, 0x1f

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public onVoiceResults(Landroid/os/Bundle;)Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->setVoiceResultsFromIntentBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->getResults()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProfile(Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->stop(Z)V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeInit(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    goto :goto_0
.end method

.method public start(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;Z)V
    .locals 8

    const/4 v5, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeInit(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeStart(ILjava/lang/String;Ljava/lang/String;ZZZZ)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider;->clearVoiceSearchResults()V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->mNativeAutocompleteBridge:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->nativeStop(IZ)V

    :cond_1
    return-void
.end method
