.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

.field final synthetic val$this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iput-object p2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;->val$this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startActivity()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v1, v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x30020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ShowClearBrowsingData"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, ":android:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;->this$1:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    iget-object v1, v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
