.class Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;
.super Ljava/lang/Object;


# instance fields
.field final mContentLength:J

.field final mCookie:Ljava/lang/String;

.field final mDescription:Ljava/lang/String;

.field final mFilename:Ljava/lang/String;

.field final mFilepath:Ljava/lang/String;

.field final mIsGETRequest:Z

.field final mMimeType:Ljava/lang/String;

.field final mReferer:Ljava/lang/String;

.field final mUrl:Ljava/lang/String;

.field final mUserAgent:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mUserAgent:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mMimeType:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mCookie:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mReferer:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilename:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mDescription:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mFilepath:Ljava/lang/String;

    iput-wide p9, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mContentLength:J

    iput-boolean p11, p0, Lcom/google/android/apps/chrome/ChromeDownloadListener$DownloadInfo;->mIsGETRequest:Z

    return-void
.end method
