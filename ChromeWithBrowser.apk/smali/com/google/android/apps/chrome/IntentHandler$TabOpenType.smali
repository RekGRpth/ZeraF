.class public final enum Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

.field public static final enum CLOBBER_CURRENT_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

.field public static final enum OPEN_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

.field public static final enum REUSE_APP_ID_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

.field public static final enum REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    const-string v1, "OPEN_NEW_TAB"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->OPEN_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    const-string v1, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    const-string v1, "REUSE_APP_ID_MATCHING_TAB_ELSE_NEW_TAB"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_APP_ID_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    const-string v1, "CLOBBER_CURRENT_TAB"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->CLOBBER_CURRENT_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->OPEN_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_APP_ID_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->CLOBBER_CURRENT_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->$VALUES:[Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->$VALUES:[Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    return-object v0
.end method
