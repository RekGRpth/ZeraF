.class Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;
.super Lcom/google/android/apps/chrome/LocationBar$UrlBar;


# instance fields
.field private mFocused:Z

.field private mForceNotEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;->mForceNotEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;->mFocused:Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;->mFocused:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;->mForceNotEnabled:Z

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone$UrlBarPhone;->mForceNotEnabled:Z

    return v0
.end method
