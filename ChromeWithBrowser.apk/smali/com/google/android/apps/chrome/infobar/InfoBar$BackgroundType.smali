.class final enum Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

.field public static final enum CUSTOM:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

.field public static final enum INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

.field public static final enum WARNING:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    new-instance v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->WARNING:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    new-instance v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->CUSTOM:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    sget-object v1, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->WARNING:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->CUSTOM:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->$VALUES:[Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->$VALUES:[Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method
