.class public Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "OpenURLWithIntentInfoBar"


# instance fields
.field private mActivityContext:Landroid/content/Context;

.field private mButtonMessage:Ljava/lang/String;

.field private mMessage:Ljava/lang/String;

.field private mNavigateButton:Landroid/widget/Button;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mActivityContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mButtonMessage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0f005f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mNavigateButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mNavigateButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mButtonMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mNavigateButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method protected getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    const v0, 0x7f02008d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mNavigateButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->dismiss()V

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mActivityContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "OpenURLWithIntentInfoBar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to launch Activity for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
