.class public Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;


# instance fields
.field private mBackground:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

.field private mCenterIcon:Z

.field private mIconResourceId:Ljava/lang/Integer;

.field private mLinkable:Z

.field private mTextView:Landroid/widget/TextView;

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/Integer;Ljava/lang/CharSequence;Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mLinkable:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mCenterIcon:Z

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mIconResourceId:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTitle:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mBackground:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-void
.end method

.method public static createInfoBar(ILjava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
    .locals 4

    new-instance v0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/Integer;Ljava/lang/CharSequence;Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;)V

    return-object v0
.end method

.method public static createInfoBar(Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    sget-object v1, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-direct {v0, v2, v2, p0, v1}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/Integer;Ljava/lang/CharSequence;Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;)V

    return-object v0
.end method

.method public static createWarningInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    const v1, 0x7f0200c1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->WARNING:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/Integer;Ljava/lang/CharSequence;Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;)V

    return-object v0
.end method

.method public static createWarningInfoBar(Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->createWarningInfoBar(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/CharSequence;)Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mLinkable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/LinkMovementMethod;

    invoke-direct {v2}, Landroid/text/method/LinkMovementMethod;-><init>()V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    return-object v1
.end method

.method protected getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mBackground:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    return-object v0
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mIconResourceId:Ljava/lang/Integer;

    return-object v0
.end method

.method protected setCenterIcon(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mCenterIcon:Z

    return-void
.end method

.method public setLinkable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mLinkable:Z

    return-void
.end method

.method protected setSpan(Ljava/lang/Object;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method protected setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTitle:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    :cond_0
    return-void
.end method

.method protected shouldCenterIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->mCenterIcon:Z

    return v0
.end method
