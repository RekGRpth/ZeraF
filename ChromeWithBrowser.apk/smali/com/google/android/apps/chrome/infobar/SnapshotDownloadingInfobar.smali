.class public Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;
.super Lcom/google/android/apps/chrome/infobar/MessageInfoBar;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final DOT_ANIMATION_DELAY:I = 0x3e8


# instance fields
.field private mDotCount:I

.field private mMessage:Ljava/lang/String;

.field private mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V
    .locals 3

    const v0, 0x7f02004f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->INFO:Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;Ljava/lang/Integer;Ljava/lang/CharSequence;Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setExpireOnNavigation(Z)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mUIHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/infobar/MessageInfoBar;->createContent(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07013c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->run()V

    return-object v0
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    iget v2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setSpan(Ljava/lang/Object;III)V

    iget v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mUIHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
