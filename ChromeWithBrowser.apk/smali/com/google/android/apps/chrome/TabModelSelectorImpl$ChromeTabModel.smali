.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/TabModel;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

.field private mIndex:I

.field private final mIsIncognito:Z

.field private mNativeChromeTabModel:I

.field private final mTabs:Ljava/util/List;

.field private final mWindowId:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->nativeInit(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->nativeGetWindowId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mWindowId:I

    new-instance v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$1;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    # setter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$002(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;)Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createCachedNtp()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->clearCachedNtp()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->nativeBroadcastSessionRestoreComplete(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabOrCachedNtpById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;Lcom/google/android/apps/chrome/Tab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->onNtpResourcesLoaded(Lcom/google/android/apps/chrome/Tab;)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createDelayedCacheNtp(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->clearCachedNtpAndThumbnails()V

    return-void
.end method

.method private addTab(ILcom/google/android/apps/chrome/Tab;)I
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-gez p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isCurrentModel()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mWindowId:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/Tab;->setWindowId(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    return v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-gt p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    goto :goto_0
.end method

.method private cacheTabBitmap(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab;)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getPendingTextureReadbackId()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->clearPendingTextureRead()V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cacheTabThumbnail(Lcom/google/android/apps/chrome/TabThumbnailProvider;)Z

    :cond_1
    return-void
.end method

.method private clearCachedNtp()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method

.method private clearCachedNtpAndThumbnails()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->clearCachedNtp()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->handleLowMemory(Z)V

    return-void
.end method

.method private createCachedNtp()V
    .locals 8

    const/4 v7, 0x0

    const/4 v5, -0x1

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->sAllowCreateCachedNtp:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mPreloadWebViewContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    const/4 v1, -0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v7, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->isTextureCached(IZZ)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v6, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$2;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V

    new-instance v0, Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lorg/chromium/ui/gfx/NativeWindow;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Tab;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/Tab;->initialize(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    const-string v1, "chrome://newtab/#cached_ntp"

    const/4 v2, 0x0

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0, v5}, Lorg/chromium/content/browser/ContentView;->setBackgroundColor(I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v0, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private createDelayedCacheNtp(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x2

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->sAllowCreateCachedNtp:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->isTextureCached(IZZ)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chrome://newtab/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$3;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    const-wide/16 v1, 0x12c

    # setter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNtpCacheCreateDelayMs:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1802(Lcom/google/android/apps/chrome/TabModelSelectorImpl;J)J

    goto :goto_0
.end method

.method private getTabOrCachedNtpById(I)Lcom/google/android/apps/chrome/Tab;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mCachedNtpTab:Lcom/google/android/apps/chrome/Tab;

    :cond_0
    return-object v0
.end method

.method private isCurrentModel()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeBroadcastSessionRestoreComplete(I)V
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeGetWindowId(I)I
.end method

.method private native nativeInit(Z)I
.end method

.method private onNtpResourcesLoaded(Lcom/google/android/apps/chrome/Tab;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel$4;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;I)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private reloadPreviousTabIfNeeded(ZI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->needsReload()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->reload()V

    goto :goto_0
.end method

.method private setIndex(ILcom/google/android/apps/chrome/TabModel$TabSelectionType;)V
    .locals 5

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    if-eq v3, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :cond_0
    :goto_1
    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_CLOSE:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    if-ne p2, v3, :cond_1

    move v0, v1

    :cond_1
    iget v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-ne v3, p1, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    if-ne v3, p0, :cond_6

    if-eqz v2, :cond_2

    invoke-direct {p0, v2, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->showTab(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabSelected(IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V
    invoke-static {v1, v2, v0, p2, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isCurrentModel()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->selectModel(Z)V

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_8

    iput v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-nez v1, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    :goto_4
    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(III)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->getTexture(IZ)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->showTab(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_USER:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    if-ne p2, v1, :cond_a

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->tabSwitched()V

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabSelected(IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V
    invoke-static {v1, v2, v0, p2, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$800(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IILcom/google/android/apps/chrome/TabModel$TabSelectionType;Z)V

    goto :goto_4
.end method

.method private showTab(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/TabModel$TabSelectionType;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isHidden()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eq v0, p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isClosing()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getLaunchType()Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_NEW:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    if-eq p2, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->cacheTabBitmap(Lcom/google/android/apps/chrome/Tab;Lcom/google/android/apps/chrome/Tab;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->hide()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabPersistentStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$900(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->addTabToSaveQueue(Lcom/google/android/apps/chrome/Tab;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/Tab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/Tab;->show(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # setter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$902(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/Tab;)Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method


# virtual methods
.method public bringToFrontOrLaunchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)V
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->setIndex(I)V

    const/4 v1, 0x1

    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public closeAllTabs()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabPersistentStore;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isIncognito()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->cancelLoadingTabs(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$300(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0x2b

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(ILandroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTabByIndex(I)Z

    goto :goto_0
.end method

.method public closeCurrentTab()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTabByIndex(I)Z

    move-result v0

    return v0
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;)Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z

    move-result v0

    return v0
.end method

.method public closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z
    .locals 11

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/high16 v2, -0x80000000

    if-nez p1, :cond_1

    sget-boolean v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Tab is null!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    move v0, v3

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->setClosing()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v7

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v8

    invoke-virtual {p0, v7}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getNextTabIfClosed(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->blockingNotifyTabClosing(IZ)V
    invoke-static {v0, v7, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/TabPersistentStore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabPersistentStore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore;->removeTabFromQueues(Lcom/google/android/apps/chrome/Tab;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->remove(I)V

    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    :cond_2
    if-nez v9, :cond_9

    move v5, v3

    :goto_2
    if-nez v9, :cond_a

    move v3, v1

    :goto_3
    if-nez v9, :cond_b

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabClosed(IZIZ)V
    invoke-static {v0, v7, v10, v3, v5}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$700(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IZIZ)V

    if-eq v9, v6, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    if-eq v5, v0, :cond_3

    invoke-virtual {p0, v8}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    :cond_3
    invoke-direct {p0, v5, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->reloadPreviousTabIfNeeded(ZI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_CLOSE:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->setIndex(ILcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    :goto_5
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->createHistoricalTab()V

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v0

    move v1, v0

    :goto_6
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/Tab;->destroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/Tab;->deleteState(Landroid/app/Activity;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->destroyIncognitoProfile()V

    :cond_5
    if-eq v1, v2, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isSavedAndViewDestroyed()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getRenderProcessPid()I

    move-result v3

    if-ne v3, v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->purgeRenderProcessNativeMemory()V

    :cond_7
    move v0, v4

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v9}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v3

    move v5, v3

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v9}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    move v3, v0

    goto/16 :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v0

    move v1, v0

    goto/16 :goto_4

    :cond_c
    iput v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    goto :goto_5

    :cond_d
    move v1, v2

    goto :goto_6
.end method

.method public closeTabById(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    goto :goto_0
.end method

.method public closeTabByIndex(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;)Z

    move-result v0

    goto :goto_0
.end method

.method public createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)Lcom/google/android/apps/chrome/Tab;
    .locals 7

    const/4 v5, -0x1

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-eq p3, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne p3, v0, :cond_2

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabIndexById(I)I

    move-result v1

    if-eq v1, v5, :cond_1

    add-int/lit8 p4, v1, 0x1

    move v5, v0

    move v4, p4

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0

    :cond_1
    move v5, v0

    move v4, p4

    goto :goto_0

    :cond_2
    move v4, p4

    goto :goto_0
.end method

.method public createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;
    .locals 10

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelOrderController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-virtual {v0, p3, v1}, Lcom/google/android/apps/chrome/TabModelOrderController;->willOpenInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z

    move-result v8

    new-instance v0, Lcom/google/android/apps/chrome/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lorg/chromium/ui/gfx/NativeWindow;

    move-result-object v3

    move-object v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/Tab;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    invoke-static {v1, v2, p3, v3, v8}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->initialize(I)V

    if-eqz p6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->setParentIsIncognito()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelOrderController;

    move-result-object v1

    invoke-virtual {v1, p3, p4, v0}, Lcom/google/android/apps/chrome/TabModelOrderController;->determineInsertionIndex(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/Tab;)I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->addTab(ILcom/google/android/apps/chrome/Tab;)I

    move-result v9

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/NewTabPageUtil;->appendNtpSectionIfNeeded(Landroid/content/Context;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getTransitionType(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)I
    invoke-static {p3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1300(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)I

    move-result v1

    invoke-virtual {v0, v3, p2, v1}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    move-object v4, p3

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreated(ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V

    if-eqz v8, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->selectModel(Z)V

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_NEW:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    invoke-direct {p0, v9, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->setIndex(ILcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    :cond_1
    return-object v0
.end method

.method public createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
    .locals 14

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-object/from16 v0, p3

    if-eq v0, v2, :cond_2

    const/4 v10, 0x1

    :goto_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getSingleTapX()I

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getSingleTapY()I

    move-result v2

    move v11, v2

    move v12, v3

    :goto_1
    new-instance v2, Lcom/google/android/apps/chrome/Tab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mNativeWindow:Lorg/chromium/ui/gfx/NativeWindow;
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lorg/chromium/ui/gfx/NativeWindow;

    move-result-object v6

    move v3, p1

    move-object/from16 v7, p3

    move/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/chrome/Tab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/gfx/NativeWindow;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    move-object/from16 v0, p3

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreating(ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    invoke-static {v3, v4, v0, v5, v10}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/Tab;->initialize(I)V

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/Tab;->associateWithApp(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mOrderController:Lcom/google/android/apps/chrome/TabModelOrderController;
    invoke-static {v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelOrderController;

    move-result-object v3

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/chrome/TabModelOrderController;->determineInsertionIndex(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/Tab;)I

    move-result v3

    invoke-direct {p0, v3, v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->addTab(ILcom/google/android/apps/chrome/Tab;)I

    move-result v13

    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v5

    invoke-virtual {v5}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v5

    iget-boolean v9, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    move-object/from16 v6, p3

    move v7, v12

    move v8, v11

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabCreated(ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V
    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$1400(Lcom/google/android/apps/chrome/TabModelSelectorImpl;ILjava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZZ)V

    if-eqz v10, :cond_1

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_NEW:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    invoke-direct {p0, v13, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->setIndex(ILcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    :cond_1
    return-object v2

    :cond_2
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_3
    move v11, v2

    move v12, v3

    goto :goto_1
.end method

.method public createTabWithNativeContents(IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;
    .locals 7

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabIndexById(I)I

    move-result v4

    if-ltz v4, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    const/4 v1, -0x1

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    move-object v0, p0

    move v2, p1

    move v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createTabWithNativeContents(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public createTabWithNativeContents(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/chrome/TabModel;->createTabWithNativeContents(IILjava/lang/String;)Lcom/google/android/apps/chrome/Tab;

    return-void
.end method

.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->destroy()V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mNativeChromeTabModel:I

    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->index()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->index()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNextTabIfClosed(I)Lcom/google/android/apps/chrome/Tab;
    .locals 5

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-nez v4, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->indexOf(Lcom/google/android/apps/chrome/Tab;)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->parentIsIncognito()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getParentId()I

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    const/4 v3, 0x0

    if-eq v4, v1, :cond_3

    if-eqz v1, :cond_3

    move-object v0, v1

    :cond_1
    :goto_2
    move-object v1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mInOverviewMode:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$300(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_4
    if-eqz v2, :cond_5

    move-object v0, v2

    goto :goto_2

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    goto :goto_2

    :cond_6
    move-object v0, v3

    goto :goto_2
.end method

.method public getTab(I)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    goto :goto_0
.end method

.method public getTabById(I)Lcom/google/android/apps/chrome/Tab;
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public getTabFromView(Lorg/chromium/content/browser/ContentView;)Lcom/google/android/apps/chrome/Tab;
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public getTabIndexById(I)I
    .locals 3

    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    if-eq v0, p1, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public getTabIndexByUrl(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getView(I)Lorg/chromium/content/browser/ContentView;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    goto :goto_0
.end method

.method public index()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    return v0
.end method

.method public indexOf(Lcom/google/android/apps/chrome/Tab;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    return v0
.end method

.method public isSessionRestoreInProgress()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$2100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public launchNTP()Lcom/google/android/apps/chrome/Tab;
    .locals 3

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    const-string v0, "chrome://newtab/"

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    return-object v0
.end method

.method public launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    return-object v0
.end method

.method public launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/chrome/Tab;
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz p4, :cond_0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p3, "com.google.android.apps.chrome.unknown_app"

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    move v4, v6

    :goto_1
    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-ge v4, v1, :cond_3

    invoke-interface {v0, v4}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/Tab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->createNewTab(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;IIZ)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/Tab;->associateWithApp(Ljava/lang/String;)V

    invoke-virtual {p0, v7, v6}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->closeTab(Lcom/google/android/apps/chrome/Tab;Z)Z

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->launchUrl(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/Tab;->associateWithApp(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public moveTab(II)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/chrome/utilities/MathUtils;->clamp(III)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->getTabIndexById(I)I

    move-result v2

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    if-eq v2, v1, :cond_0

    add-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Tab;

    if-ge v2, v1, :cond_2

    add-int/lit8 v1, v1, -0x1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mTabs:Ljava/util/List;

    invoke-interface {v3, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-ne v2, v0, :cond_4

    iput v1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyChanged()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIsIncognito:Z

    # invokes: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->notifyTabMoved(IIIZ)V
    invoke-static {v0, p1, v2, v1, v3}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$200(Lcom/google/android/apps/chrome/TabModelSelectorImpl;IIIZ)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-ge v2, v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-lt v1, v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-le v2, v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    if-gt v1, v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->mIndex:I

    goto :goto_1
.end method

.method public openClearBrowsingData()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/TabModelSelectorImpl;->mClearBrowsingDataActivityStarter:Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl;->access$000(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ClearBrowsingDataActivityStarter;->startActivity()V

    return-void
.end method

.method public setIndex(I)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabSelectionType;->FROM_USER:Lcom/google/android/apps/chrome/TabModel$TabSelectionType;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$ChromeTabModel;->setIndex(ILcom/google/android/apps/chrome/TabModel$TabSelectionType;)V

    return-void
.end method
