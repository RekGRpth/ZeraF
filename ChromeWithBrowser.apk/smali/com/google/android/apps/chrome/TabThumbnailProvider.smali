.class public interface abstract Lcom/google/android/apps/chrome/TabThumbnailProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getBitmap()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getId()I
.end method

.method public abstract getProgress()I
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public abstract getView()Lorg/chromium/content/browser/ContentView;
.end method

.method public abstract getWidth()I
.end method

.method public abstract isClosing()Z
.end method

.method public abstract isReady()Z
.end method

.method public abstract isSavedAndViewDestroyed()Z
.end method

.method public abstract isTextureViewAvailable()Z
.end method

.method public abstract useTextureView()Z
.end method
