.class public Lcom/google/android/apps/chrome/LocationBarPhone;
.super Lcom/google/android/apps/chrome/LocationBar;


# static fields
.field private static final KEYBOARD_HIDE_DELAY_MS:I = 0x96

.field private static final KEYBOARD_MODE_CHANGE_DELAY_MS:I = 0x12c

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mKeyboardResizeModeTask:Ljava/lang/Runnable;

.field private mUrlFocusChangeInProgress:Z

.field private mVoiceSearchEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/LocationBarPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/LocationBarPhone;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/LocationBarPhone;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    return-object p1
.end method


# virtual methods
.method protected finishUrlFocusChange(Z)V
    .locals 5

    const/16 v2, 0x20

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    new-instance v1, Lcom/google/android/apps/chrome/LocationBarPhone$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBarPhone$2;-><init>(Lcom/google/android/apps/chrome/LocationBarPhone;)V

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/chrome/LocationBarPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v2, 0x10

    if-eq v1, v2, :cond_0

    new-instance v1, Lcom/google/android/apps/chrome/LocationBarPhone$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/LocationBarPhone$3;-><init>(Lcom/google/android/apps/chrome/LocationBarPhone;Landroid/view/Window;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBarPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    :cond_2
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    if-eq v1, v2, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->openKeyboard()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarPhone;->getSuggestionListPopup()Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarPhone;->getSuggestionListPopup()Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarPhone;->getSuggestionListPopup()Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->invalidateSuggestionViews()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar;->onFinishInflate()V

    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    new-instance v1, Lcom/google/android/apps/chrome/LocationBarPhone$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/LocationBarPhone$1;-><init>(Lcom/google/android/apps/chrome/LocationBarPhone;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    const v1, 0x7f0f0070

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/LocationBarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/LocationBarPhone;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/LocationBarPhone;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method protected onUrlFocusChange(Z)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBarPhone;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBarPhone;->setFocusableInTouchMode(Z)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->onUrlFocusChange(Z)V

    return-void
.end method

.method protected setLoadProgress(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar;->setLoadProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mToolbar:Lcom/google/android/apps/chrome/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getToolbarView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ToolbarPhone;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ToolbarPhone;->setLoadProgress(I)V

    return-void
.end method

.method protected shouldAnimateIconChanges()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/LocationBar;->shouldAnimateIconChanges()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateDeleteButton(Z)V
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mMicButton:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mVoiceSearchEnabled:Z

    if-eqz v3, :cond_1

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected updateMicButtonState()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarPhone;->isVoiceSearchEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mVoiceSearchEnabled:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mMicButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mVoiceSearchEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
