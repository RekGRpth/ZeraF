.class public Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const v2, 0x7f090004

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v0, v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->values()[Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v1

    array-length v1, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->setEntries(I)V

    const v0, 0x7f090003

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->setEntryValues(I)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-static {p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefBandwidthPreference()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->GetBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->getDisplayTitle()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->checkAllowPrerender()Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefBandwidthPreference()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->GetBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/BandwidthType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/BandwidthType;->getDisplayTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrefetchBandwidthPreference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    return v0
.end method
