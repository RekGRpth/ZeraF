.class public Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;
.implements Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;


# static fields
.field private static final CLEAR_CACHE:I = 0x1

.field private static final CLEAR_COOKIES_AND_SITE_DATA:I = 0x2

.field private static final CLEAR_FORM_DATA:I = 0x4

.field private static final CLEAR_HISTORY:I = 0x0

.field private static final CLEAR_PASSWORDS:I = 0x3


# instance fields
.field private mItems:[Ljava/lang/String;

.field private mItemsChecked:[Z

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method private clearBrowsingData()V
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0700fc

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0700fd

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v5, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    aget-boolean v2, v1, v4

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    aget-boolean v3, v1, v5

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v4, 0x2

    aget-boolean v4, v1, v4

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v5, 0x3

    aget-boolean v5, v1, v5

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v6, 0x4

    aget-boolean v6, v1, v6

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->clearBrowsingData(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;ZZZZZ)V

    return-void
.end method


# virtual methods
.method public onBrowsingDataCleared()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->clearBrowsingData()V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    aput-boolean p3, v0, p2

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    const v3, 0x7f0700f8

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    const v3, 0x7f0700f7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700f9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    const/4 v3, 0x3

    const v5, 0x7f0700fa

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    const/4 v3, 0x4

    const v5, 0x7f0700fb

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->setItemsToBeChecked(ZZZZZ)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0700f5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0701c9

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0701c8

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItems:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method setItemsToBeChecked(ZZZZZ)V
    .locals 2

    const/4 v0, 0x5

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v1, 0x1

    aput-boolean p2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v1, 0x2

    aput-boolean p3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v1, 0x3

    aput-boolean p4, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mItemsChecked:[Z

    const/4 v1, 0x4

    aput-boolean p5, v0, v1

    return-void
.end method
