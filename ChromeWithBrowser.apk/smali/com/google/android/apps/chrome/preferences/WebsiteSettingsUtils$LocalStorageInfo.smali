.class public Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mOrigin:Ljava/lang/String;

.field private mSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->mOrigin:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeClearLocalStorageData(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->access$200(Ljava/lang/String;)V

    return-void
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->mSize:J

    return-wide v0
.end method

.method public setInfo(Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->mOrigin:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$LocalStorageInfo;->mSize:J

    return-void
.end method
