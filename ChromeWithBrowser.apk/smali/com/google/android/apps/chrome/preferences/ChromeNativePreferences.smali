.class public final Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AUTOFILL_ADDR1:Ljava/lang/String; = "addr1"

.field public static final AUTOFILL_ADDR2:Ljava/lang/String; = "addr2"

.field public static final AUTOFILL_CITY:Ljava/lang/String; = "city"

.field public static final AUTOFILL_COMPANY:Ljava/lang/String; = "company"

.field public static final AUTOFILL_COUNTRY:Ljava/lang/String; = "country"

.field public static final AUTOFILL_EMAIL:Ljava/lang/String; = "email"

.field public static final AUTOFILL_GUID:Ljava/lang/String; = "guid"

.field public static final AUTOFILL_MONTH:Ljava/lang/String; = "month"

.field public static final AUTOFILL_NAME:Ljava/lang/String; = "name"

.field public static final AUTOFILL_NUMBER:Ljava/lang/String; = "number"

.field public static final AUTOFILL_OBFUSCATED:Ljava/lang/String; = "obfuscated"

.field public static final AUTOFILL_PHONE:Ljava/lang/String; = "phone"

.field public static final AUTOFILL_PREVIEW:Ljava/lang/String; = "preview"

.field public static final AUTOFILL_STATE:Ljava/lang/String; = "state"

.field public static final AUTOFILL_YEAR:Ljava/lang/String; = "year"

.field public static final AUTOFILL_ZIP:Ljava/lang/String; = "zip"

.field public static final EXCEPTION_DISPLAY_PATTERN:Ljava/lang/String; = "displayPattern"

.field public static final EXCEPTION_SETTING:Ljava/lang/String; = "setting"

.field public static final EXCEPTION_SETTING_ALLOW:Ljava/lang/String; = "allow"

.field public static final EXCEPTION_SETTING_BLOCK:Ljava/lang/String; = "block"

.field public static final EXCEPTION_SETTING_DEFAULT:Ljava/lang/String; = "default"

.field private static final LOG_TAG:Ljava/lang/String; = "ChromeNativePreferences"

.field public static final PASSWORD_LIST_ID:Ljava/lang/String; = "id"

.field public static final PASSWORD_LIST_NAME:Ljava/lang/String; = "name"

.field public static final PASSWORD_LIST_PASSWORD:Ljava/lang/String; = "password"

.field public static final PASSWORD_LIST_URL:Ljava/lang/String; = "url"

.field public static final SEARCH_ENGINE_ID:Ljava/lang/String; = "searchEngineId"

.field public static final SEARCH_ENGINE_KEYWORD:Ljava/lang/String; = "searchEngineKeyword"

.field public static final SEARCH_ENGINE_SHORT_NAME:Ljava/lang/String; = "searchEngineShortName"

.field public static final SEARCH_ENGINE_URL:Ljava/lang/String; = "searchEngineUrl"

.field private static sDefaultCountryCodeAtInstall:Ljava/lang/String;

.field private static sExecutablePathValue:Ljava/lang/String;

.field private static sPrefs:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

.field private static sProfilePathValue:Ljava/lang/String;


# instance fields
.field private mAcceptCookiesEnabled:Z

.field private mAllowLocationEnabled:Z

.field private mAllowPopupsEnabled:Z

.field private mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

.field private mAutofillEnabled:Z

.field private mAutologinEnabled:Z

.field private mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

.field private mDoNotTrackEnabled:Z

.field private mFontScaleFactor:F

.field private mForceEnableZoom:Z

.field private mJavaScriptEnabled:Z

.field private mMinimumFontSize:I

.field private mNativeAutofillPersonalDataManagerObserver:I

.field private mNativeDevToolsServer:I

.field private mNativeTemplateURLServiceLoadedObserver:I

.field private mNetworkPredictionEnabled:Z

.field private mRememberPasswordsEnabled:Z

.field private mResolveNavigationErrorEnabled:Z

.field private mSearchEngine:I

.field private mSearchSuggestEnabled:Z

.field private mSpdyProxyEnabled:Z

.field private mSpdyProxyOrigin:Ljava/lang/String;

.field private mSpdyProxyProperty:Ljava/lang/String;

.field private mSpdyProxyValue:Ljava/lang/String;

.field private mSyncAccountName:Ljava/lang/String;

.field private mSyncAutofill:Z

.field private mSyncBookmarks:Z

.field private mSyncEverything:Z

.field private mSyncPasswords:Z

.field private mSyncSessions:Z

.field private mSyncSetupCompleted:Z

.field private mSyncSuppressStart:Z

.field private mSyncTypedUrls:Z

.field private mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->$assertionsDisabled:Z

    const-string v0, ""

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sDefaultCountryCodeAtInstall:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeAutofillPersonalDataManagerObserver:I

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeDevToolsServer:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeInitSpdyProxySettings()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeInitRemoteDebugging()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeDevToolsServer:I

    return-void
.end method

.method private autofillDataUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;->onAutoFillDataUpdated()V

    :cond_0
    return-void
.end method

.method private browsingDataCleared()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;->onBrowsingDataCleared()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    :cond_0
    return-void
.end method

.method public static getDefaultCountryCodeAtInstall()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sDefaultCountryCodeAtInstall:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;
    .locals 2

    const-class v1, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getSavedNamePassword(I)Ljava/util/HashMap;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetSavedNamePassword(I)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public static getSavedPasswordException(I)Ljava/util/HashMap;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetSavedPasswordException(I)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method private native nativeClearBrowsingData(ZZZZZ)V
.end method

.method private native nativeDeleteAutofillCreditCard(Ljava/lang/String;)V
.end method

.method private native nativeDeleteAutofillProfile(Ljava/lang/String;)V
.end method

.method private native nativeDestroyPersonalDataManagerObserver(I)V
.end method

.method private native nativeDestroyRemoteDebugging(I)V
.end method

.method private native nativeDestroyTemplateURLServiceLoadedObserver(I)V
.end method

.method private native nativeGet()V
.end method

.method private native nativeGetAboutVersionStrings()Ljava/util/HashMap;
.end method

.method private native nativeGetAutofillCreditCard(Ljava/lang/String;)Ljava/util/HashMap;
.end method

.method private native nativeGetAutofillCreditCardGUIDs()[Ljava/lang/String;
.end method

.method private native nativeGetAutofillProfile(Ljava/lang/String;)Ljava/util/HashMap;
.end method

.method private native nativeGetAutofillProfileGUIDs()[Ljava/lang/String;
.end method

.method private native nativeGetDoNotTrackLearnMoreURL()Ljava/lang/String;
.end method

.method private native nativeGetLocalizedSearchEngines()[Ljava/util/HashMap;
.end method

.method private native nativeGetPopupExceptions()[Ljava/util/HashMap;
.end method

.method private static native nativeGetSavedNamePassword(I)Ljava/util/HashMap;
.end method

.method private static native nativeGetSavedPasswordException(I)Ljava/util/HashMap;
.end method

.method private native nativeInitAutofillPersonalDataManagerObserver()I
.end method

.method private native nativeInitRemoteDebugging()I
.end method

.method private native nativeInitSpdyProxySettings()V
.end method

.method private native nativeInitTemplateURLServiceLoadedObserver()I
.end method

.method private native nativeIsRemoteDebuggingEnabled(I)Z
.end method

.method private native nativeIsTemplateURLServiceLoaded()Z
.end method

.method private native nativeRemovePopupException(Ljava/lang/String;)V
.end method

.method private static native nativeRemoveSavedNamePassword(I)V
.end method

.method private static native nativeRemoveSavedPasswordException(I)V
.end method

.method private native nativeSetAllowCookiesEnabled(Z)V
.end method

.method private native nativeSetAllowLocationEnabled(Z)V
.end method

.method private native nativeSetAllowPopupsEnabled(Z)V
.end method

.method private native nativeSetAutoFillEnabled(Z)V
.end method

.method private native nativeSetAutofillCreditCard(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
.end method

.method private native nativeSetAutofillProfile(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
.end method

.method private native nativeSetAutologinEnabled(Z)V
.end method

.method private native nativeSetDoNotTrackEnabled(Z)V
.end method

.method private native nativeSetFontScaleFactor(F)V
.end method

.method private native nativeSetForceEnableZoom(Z)V
.end method

.method private native nativeSetJavaScriptEnabled(Z)V
.end method

.method private native nativeSetMinimumFontSize(I)V
.end method

.method private native nativeSetNetworkPredictionEnabled(Z)V
.end method

.method private native nativeSetPathValuesForAboutChrome()V
.end method

.method private native nativeSetPopupException(Ljava/lang/String;Z)V
.end method

.method private native nativeSetRememberPasswordsEnabled(Z)V
.end method

.method private native nativeSetRemoteDebuggingEnabled(IZ)V
.end method

.method private native nativeSetResolveNavigationErrorEnabled(Z)V
.end method

.method private native nativeSetSearchEngine(I)V
.end method

.method private native nativeSetSearchSuggestEnabled(Z)V
.end method

.method private native nativeSetSpdyProxyEnabled(Z)V
.end method

.method private native nativeSetSyncSuppressStart(Z)V
.end method

.method private static native nativeStartPasswordListRequest(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PasswordListObserver;)V
.end method

.method private static native nativeStopPasswordListRequest()V
.end method

.method private native nativeUpdateSearchEngineInJava()V
.end method

.method public static removeSavedNamePassword(I)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeRemoveSavedNamePassword(I)V

    return-void
.end method

.method public static removeSavedPasswordException(I)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeRemoveSavedPasswordException(I)V

    return-void
.end method

.method public static setDefaultCountryCodeAtInstall(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v0

    const-string v1, "default-country-code"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "default-country-code"

    invoke-virtual {v0, v1}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sDefaultCountryCodeAtInstall:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sDefaultCountryCodeAtInstall:Ljava/lang/String;

    goto :goto_0
.end method

.method public static setExecutablePathValue(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sExecutablePathValue:Ljava/lang/String;

    return-void
.end method

.method public static setProfilePathValue(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    return-void
.end method

.method public static startPasswordListRequest(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PasswordListObserver;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeStartPasswordListRequest(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PasswordListObserver;)V

    return-void
.end method

.method public static stopPasswordListRequest()V
    .locals 0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeStopPasswordListRequest()V

    return-void
.end method

.method private templateURLServiceLoaded()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;->onTemplateURLServiceLoaded()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final clearBrowsingData(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;ZZZZZ)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeClearBrowsingData(ZZZZZ)V

    return-void
.end method

.method public final deleteAutofillCreditCard(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeDeleteAutofillCreditCard(Ljava/lang/String;)V

    return-void
.end method

.method public final deleteAutofillProfile(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeDeleteAutofillProfile(Ljava/lang/String;)V

    return-void
.end method

.method public final getAboutVersionStrings()Ljava/util/HashMap;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAboutVersionStrings()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getAutofillCreditCard(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAutofillCreditCard(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getAutofillCreditCardGUIDs()[Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAutofillCreditCardGUIDs()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getAutofillProfile(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAutofillProfile(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getAutofillProfileGUIDs()[Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAutofillProfileGUIDs()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDoNotTrackLearnMoreURL()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetDoNotTrackLearnMoreURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getExecutablePathValue()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sExecutablePathValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getFontScaleFactor()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mFontScaleFactor:F

    return v0
.end method

.method public final getForceEnableZoom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mForceEnableZoom:Z

    return v0
.end method

.method public final getLocalizedSearchEngines()[Ljava/util/HashMap;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetLocalizedSearchEngines()[Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getMinimumFontSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mMinimumFontSize:I

    return v0
.end method

.method public final getPopupExceptionSettingFromPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v1, "default"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getPopupExceptions()[Ljava/util/HashMap;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    const-string v0, "displayPattern"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "setting"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final getPopupExceptions()[Ljava/util/HashMap;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetPopupExceptions()[Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final getProfilePathValue()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getSearchEngine()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchEngine:I

    return v0
.end method

.method public final getSpdyProxyOrigin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSpdyProxyOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public final getSpdyProxyProperty()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSpdyProxyProperty:Ljava/lang/String;

    return-object v0
.end method

.method public final getSpdyProxyValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSpdyProxyValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getSyncAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public final isAcceptCookiesEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAcceptCookiesEnabled:Z

    return v0
.end method

.method public final isAllowLocationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    return v0
.end method

.method public final isAutofillEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutofillEnabled:Z

    return v0
.end method

.method public final isAutologinEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutologinEnabled:Z

    return v0
.end method

.method public final isDoNotTrackEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mDoNotTrackEnabled:Z

    return v0
.end method

.method public final isNetworkPredictionEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionEnabled:Z

    return v0
.end method

.method public final isRememberPasswordsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mRememberPasswordsEnabled:Z

    return v0
.end method

.method public final isRemoteDebuggingEnabled()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeDevToolsServer:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeIsRemoteDebuggingEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final isResolveNavigationErrorEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mResolveNavigationErrorEnabled:Z

    return v0
.end method

.method public final isSearchSuggestEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchSuggestEnabled:Z

    return v0
.end method

.method public final isSpdyProxyEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSpdyProxyEnabled:Z

    return v0
.end method

.method public final isSyncAutofillEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncAutofill:Z

    return v0
.end method

.method public final isSyncBookmarksEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncBookmarks:Z

    return v0
.end method

.method public final isSyncEverythingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncEverything:Z

    return v0
.end method

.method public final isSyncPasswordsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncPasswords:Z

    return v0
.end method

.method public final isSyncSessionEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncSessions:Z

    return v0
.end method

.method public final isSyncTypedUrlsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncTypedUrls:Z

    return v0
.end method

.method public final isTemplateURLServiceLoaded()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeIsTemplateURLServiceLoaded()Z

    move-result v0

    return v0
.end method

.method public final javaScriptEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    return v0
.end method

.method public final popupsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowPopupsEnabled:Z

    return v0
.end method

.method public final registerAutoFillDataUpdatedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

    if-eqz v0, :cond_0

    const-string v0, "ChromeNativePreferences"

    const-string v1, "mAutoFillDataUpdatedListener set again before getting destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->unregisterAutoFillDataUpdatedListener()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeInitAutofillPersonalDataManagerObserver()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeAutofillPersonalDataManagerObserver:I

    return-void
.end method

.method public final registerTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ChromeNativePreferences"

    const-string v2, "mTemplateURLServiceLoadedListener set again before getting destroyed"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeInitTemplateURLServiceLoadedObserver()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final removePopupException(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeRemovePopupException(Ljava/lang/String;)V

    return-void
.end method

.method public final setAllowCookiesEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAcceptCookiesEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowCookiesEnabled(Z)V

    return-void
.end method

.method public final setAllowLocationEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowLocationEnabled(Z)V

    goto :goto_0
.end method

.method public final setAllowPopupsEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowPopupsEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowPopupsEnabled(Z)V

    return-void
.end method

.method public final setAutoFillEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutofillEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAutoFillEnabled(Z)V

    return-void
.end method

.method public final setAutofillCreditCard(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAutofillCreditCard(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setAutofillProfile(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAutofillProfile(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setAutologinEnabled(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAutologinEnabled(Z)V

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutologinEnabled:Z

    return-void
.end method

.method public final setDoNotTrackEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mDoNotTrackEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetDoNotTrackEnabled(Z)V

    return-void
.end method

.method public final setFontScaleFactor(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mFontScaleFactor:F

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetFontScaleFactor(F)V

    return-void
.end method

.method public final setForceEnableZoom(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mForceEnableZoom:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetForceEnableZoom(Z)V

    return-void
.end method

.method public final setJavaScriptEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetJavaScriptEnabled(Z)V

    return-void
.end method

.method public final setMinimumFontSize(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mMinimumFontSize:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetMinimumFontSize(I)V

    return-void
.end method

.method public final setNetworkPredictionEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetNetworkPredictionEnabled(Z)V

    return-void
.end method

.method public final setPathValuesForAboutChrome()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sExecutablePathValue:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetPathValuesForAboutChrome()V

    :cond_0
    return-void
.end method

.method public final setPopupException(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetPopupException(Ljava/lang/String;Z)V

    return-void
.end method

.method public final setRememberPasswordsEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mRememberPasswordsEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetRememberPasswordsEnabled(Z)V

    return-void
.end method

.method public final setRemoteDebuggingEnabled(Z)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeDevToolsServer:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetRemoteDebuggingEnabled(IZ)V

    return-void
.end method

.method public final setResolveNavigationErrorEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mResolveNavigationErrorEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetResolveNavigationErrorEnabled(Z)V

    return-void
.end method

.method public final setSearchEngine(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchEngine:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetSearchEngine(I)V

    return-void
.end method

.method public final setSearchSuggestEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchSuggestEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetSearchSuggestEnabled(Z)V

    return-void
.end method

.method public final setSpdyProxyEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSpdyProxyEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetSpdyProxyEnabled(Z)V

    return-void
.end method

.method public final setSyncSuppressStart(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncSuppressStart:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncSuppressStart:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetSyncSuppressStart(Z)V

    return-void
.end method

.method public final syncSetupCompleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncSetupCompleted:Z

    return v0
.end method

.method public final syncSuppressStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSyncSuppressStart:Z

    return v0
.end method

.method public final unregisterAutoFillDataUpdatedListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutoFillDataUpdatedListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$onAutoFillDataUpdatedListener;

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeAutofillPersonalDataManagerObserver:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeAutofillPersonalDataManagerObserver:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeDestroyPersonalDataManagerObserver(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeAutofillPersonalDataManagerObserver:I

    :cond_0
    return-void
.end method

.method public final unregisterTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTemplateURLServiceLoadedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeDestroyTemplateURLServiceLoadedObserver(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNativeTemplateURLServiceLoadedObserver:I

    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final update()V
    .locals 0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGet()V

    return-void
.end method

.method public final updateSearchEngineFromNative()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeUpdateSearchEngineInJava()V

    return-void
.end method
