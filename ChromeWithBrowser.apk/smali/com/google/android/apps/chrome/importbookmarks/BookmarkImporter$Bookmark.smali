.class Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;
.super Ljava/lang/Object;


# instance fields
.field public created:Ljava/lang/Long;

.field public entries:Ljava/util/ArrayList;

.field public favicon:[B

.field public id:J

.field public isFolder:Z

.field public lastVisit:Ljava/lang/Long;

.field public nativeId:J

.field public parent:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;

.field public parentId:J

.field public processed:Z

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public visits:Ljava/lang/Long;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$Bookmark;->entries:Ljava/util/ArrayList;

    return-void
.end method
