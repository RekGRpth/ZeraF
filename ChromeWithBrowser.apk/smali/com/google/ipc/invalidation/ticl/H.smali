.class public final Lcom/google/ipc/invalidation/ticl/H;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/protobuf/AbstractMessageLite;

.field private final b:[B

.field private final c:I


# direct methods
.method private constructor <init>(Lcom/google/protobuf/AbstractMessageLite;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/AbstractMessageLite;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    invoke-virtual {p1}, Lcom/google/protobuf/AbstractMessageLite;->b()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/H;->b:[B

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/H;->b:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/H;->c:I

    return-void
.end method

.method public static a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/H;-><init>(Lcom/google/protobuf/AbstractMessageLite;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/protobuf/AbstractMessageLite;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    instance-of v1, p1, Lcom/google/ipc/invalidation/ticl/H;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/ticl/H;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p1, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/H;->c:I

    iget v2, p1, Lcom/google/ipc/invalidation/ticl/H;->c:I

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/H;->b:[B

    iget-object v1, p1, Lcom/google/ipc/invalidation/ticl/H;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/H;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PW-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/H;->a:Lcom/google/protobuf/AbstractMessageLite;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
