.class final Lcom/google/ipc/invalidation/ticl/K;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/ticl/ac;

.field private final b:Lcom/google/ipc/invalidation/external/client/SystemResources;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Set;

.field private final e:Ljava/util/Set;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/K;->b:Lcom/google/ipc/invalidation/external/client/SystemResources;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/google/ipc/invalidation/ticl/K;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;)V

    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    sget-object v3, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    sget-object v3, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    :cond_4
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    :cond_5
    return-void
.end method


# virtual methods
.method final a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v5

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->b:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-virtual {v5, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    iput-object v4, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->b:Lcom/google/ipc/invalidation/external/client/SystemResources;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Cannot send message since no token and no initialize msg: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v5, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->k:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    move-object v0, v4

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->c:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    :cond_4
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    move-result-object v6

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v1

    check-cast v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v8, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    if-ne v0, v8, :cond_6

    move v0, v2

    :goto_5
    invoke-static {v1, v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {v6}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->d:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    :cond_8
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->e:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    :cond_a
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->a:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ah;->a:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-virtual {v5, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    iput-object v4, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    :cond_b
    move-object v0, v5

    goto/16 :goto_0
.end method

.method public final a()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;
    .locals 6

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    sget-object v4, Lcom/google/ipc/invalidation/ticl/J;->a:[I

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/H;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/H;->a()Lcom/google/protobuf/AbstractMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    :cond_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    :cond_4
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/K;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/K;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->d:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->c:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/K;->e:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/H;->a(Lcom/google/protobuf/AbstractMessageLite;)Lcom/google/ipc/invalidation/ticl/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
