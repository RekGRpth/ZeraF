.class public Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;
.super Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "AndroidMessageReceiverService"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;-><init>(Ljava/lang/String;)V

    const-string v0, "MsgRcvrSvc"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method


# virtual methods
.method protected onDeletedMessages(I)V
    .locals 0

    return-void
.end method

.method protected onMessage(Landroid/content/Intent;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "content"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "content"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v1, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/O;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->a([B)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/protobuf/ByteString;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "echo-token"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/channel/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Failed parsing inbound message: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "GCM Intent has no message content: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onRegistered(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.ipc.invalidation.channel.sender.gcm_regid_change"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class v1, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageSenderService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->c()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v1, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/O;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/channel/AndroidMessageReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method protected onUnregistered(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
