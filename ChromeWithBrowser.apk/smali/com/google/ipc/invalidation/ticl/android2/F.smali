.class public final Lcom/google/ipc/invalidation/ticl/android2/F;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/a/M;


# static fields
.field public static final a:Lcom/google/ipc/invalidation/ticl/android2/G;

.field public static final b:Lcom/google/ipc/invalidation/ticl/android2/L;

.field public static final c:Lcom/google/ipc/invalidation/ticl/android2/J;

.field public static final d:Lcom/google/ipc/invalidation/ticl/android2/K;

.field public static final e:Lcom/google/ipc/invalidation/ticl/android2/H;

.field public static final f:Lcom/google/ipc/invalidation/a/N;

.field public static final g:Lcom/google/ipc/invalidation/a/N;

.field public static final h:Lcom/google/ipc/invalidation/a/N;

.field public static final i:Lcom/google/ipc/invalidation/a/N;

.field public static final j:Lcom/google/ipc/invalidation/a/N;

.field public static final k:Lcom/google/ipc/invalidation/a/N;

.field public static final l:Lcom/google/ipc/invalidation/a/N;

.field public static final m:Lcom/google/ipc/invalidation/a/N;

.field private static final n:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/G;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/G;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->a:Lcom/google/ipc/invalidation/ticl/android2/G;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/L;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/L;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->b:Lcom/google/ipc/invalidation/ticl/android2/L;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/J;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/J;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->c:Lcom/google/ipc/invalidation/ticl/android2/J;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/K;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/K;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->d:Lcom/google/ipc/invalidation/ticl/android2/K;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/H;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/H;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->e:Lcom/google/ipc/invalidation/ticl/android2/H;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/I;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/I;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "serial"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "version"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ready"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "invalidate"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "registration_status"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "registration_failure"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "reissue_registrations"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "error"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->n:Ljava/util/Set;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "serial"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->f:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "version"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->g:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "ready"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->h:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "invalidate"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->i:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "registration_status"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->j:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "registration_failure"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->k:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "reissue_registrations"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->l:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "error"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->m:Lcom/google/ipc/invalidation/a/N;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->n:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Z
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g()Z

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i()Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->i:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k()Z

    move-result v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->j:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->m()Z

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->k:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->o()Z

    move-result v0

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->l:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->q()Z

    move-result v0

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->m:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->s()Z

    move-result v0

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->i:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->j:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->k:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->l:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->r()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/ipc/invalidation/ticl/android2/F;->m:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->t()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
