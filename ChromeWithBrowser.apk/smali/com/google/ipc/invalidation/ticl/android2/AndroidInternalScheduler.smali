.class public final Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;


# static fields
.field private static a:Z


# instance fields
.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/ipc/invalidation/ticl/android2/a;

.field private e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/a;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "No task registered for %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iget-wide v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->j()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Ignoring event with wrong ticl id (not %s): %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cannot overwrite task registered on %s, %s; tasks = %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p0, v4, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final getCurrentTimeMs()J
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isRunningOnThread()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final schedule(ILjava/lang/Runnable;)V
    .locals 6

    instance-of v0, p2, Lcom/google/ipc/invalidation/b/g;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported: can only schedule named runnables, not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/ipc/invalidation/b/g;

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/b/g;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->f:J

    invoke-static {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/P;->a(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-class v2, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler$AlarmReceiver;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-wide v2, 0x41dfffffffc00000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    const/high16 v3, 0x40000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->c:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->d:Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v2

    int-to-long v4, p1

    add-long/2addr v2, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 1

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {v0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->e:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method
