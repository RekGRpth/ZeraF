.class public final Lcom/google/ipc/invalidation/ticl/android2/P;
.super Ljava/lang/Object;


# static fields
.field static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(II)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-void
.end method

.method public static a(Ljava/lang/String;J)Landroid/content/Intent;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;->a(J)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->b()[B

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-scheduler"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Landroid/content/Intent;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;->b()[B

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-outbound-message"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
