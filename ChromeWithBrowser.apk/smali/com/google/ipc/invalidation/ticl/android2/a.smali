.class public Lcom/google/ipc/invalidation/ticl/android2/a;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Landroid/content/Intent;
    .locals 4

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-internal-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorReason()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->isTransient()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/protobuf/ByteString;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ipcinv-internal-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;
    .locals 4

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Z)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Landroid/content/Intent;
    .locals 4

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a(Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v1
.end method

.method public static a([BI)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v1

    invoke-static {p0}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static a(Lcom/google/ipc/invalidation/b/i;Landroid/content/Intent;)Lcom/google/ipc/invalidation/b/i;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string v0, "intent("

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    :try_start_0
    const-string v0, "ipcinv-scheduler"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;

    move-result-object v0

    if-nez v0, :cond_2

    :goto_1
    move v0, v1

    :goto_2
    if-nez v0, :cond_1

    const-string v0, "UNKNOWN@AndroidStrings"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v1, ", extras = "

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v1, ", "

    invoke-static {v1}, Lcom/google/a/a/a;->a(Ljava/lang/String;)Lcom/google/a/a/a;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/a/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_3
    const-string v0, ")"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object p0

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v2, "ipcinv-scheduler"

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, "(eventName = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", ticlId = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->j()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/ipc/invalidation/b/i;->a(J)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "ERROR@AndroidStrings"

    invoke-virtual {p0, v1}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_3

    :cond_3
    :try_start_2
    const-string v0, "ipcinv-outbound-message"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;

    move-result-object v0

    if-nez v0, :cond_4

    :goto_4
    move v0, v1

    goto :goto_2

    :cond_4
    const-string v2, "ipcinv-outbound-message"

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidNetworkSendRequest;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_4

    :cond_5
    const-string v0, "ipcinv-upcall"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "ipcinv-upcall"

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, "::"

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, ".ready()"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    :cond_6
    :goto_5
    move v0, v1

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "invalidate(ackHandle = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    const-string v2, ", "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->g()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/ipc/invalidation/b/i;

    :cond_8
    :goto_6
    const-string v0, ")"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_5

    :cond_9
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->l()Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_6

    :cond_a
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->i()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "UNKNOWN: "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_6

    :cond_b
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->m()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "registrationStatus(objectId = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    const-string v2, ", isRegistered = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->h()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Z)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_5

    :cond_c
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->o()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "registrationFailure(objectId = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    const-string v2, ", isTransient = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->h()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Z)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto/16 :goto_5

    :cond_d
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->q()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->r()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "reissueRegistrations(prefix = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", length = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->h()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(I)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto/16 :goto_5

    :cond_e
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->s()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->t()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "error(code = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(I)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", message = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", isTransient = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->j()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Z)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto/16 :goto_5

    :cond_f
    const-string v0, "UNKNOWN@AndroidStrings"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto/16 :goto_5

    :cond_10
    const-string v0, "ipcinv-internal-downcall"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    if-eqz v0, :cond_11

    const-string v2, "ipcinv-internal-downcall"

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, "::"

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v0

    if-nez v0, :cond_12

    :cond_11
    :goto_7
    move v0, v1

    goto/16 :goto_2

    :cond_12
    const-string v2, "serverMessage("

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_7

    :cond_13
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v0

    if-eqz v0, :cond_11

    const-string v2, "networkStatus(isOnline = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->f()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Z)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_7

    :cond_14
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->k()Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v0, "newtworkAddrChange()"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_7

    :cond_15
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->m()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    if-eqz v0, :cond_11

    const-string v2, "createClient(type = "

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(I)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", name = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    const-string v3, ", skipStartForTest = "

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->l()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/b/i;->a(Z)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_7

    :cond_16
    const-string v0, "UNKNOWN@AndroidStrings"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto/16 :goto_7

    :cond_17
    const-string v0, "ipcinv-downcall"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_20

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    if-eqz v0, :cond_18

    const-string v3, "ipcinv-downcall"

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v3

    const-string v4, "::"

    invoke-virtual {v3, v4}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i()Z

    move-result v3

    if-eqz v3, :cond_19

    const-string v0, "start()"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    :cond_18
    :goto_8
    move v0, v1

    goto/16 :goto_2

    :cond_19
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->k()Z

    move-result v3

    if-eqz v3, :cond_1a

    const-string v0, "stop()"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_8

    :cond_1a
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->m()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v0

    if-eqz v0, :cond_18

    const-string v2, "ack("

    invoke-virtual {p0, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    const-string v0, ")"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_8

    :cond_1b
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->o()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    if-eqz v0, :cond_18

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->f()I

    move-result v3

    if-lez v3, :cond_1d

    const-string v3, "register("

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->e()Ljava/util/List;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_1e

    const-string v3, "ObjectIds: "

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    if-nez v3, :cond_1c

    const-string v3, ", "

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    :cond_1c
    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/b/i;

    move v3, v2

    goto :goto_a

    :cond_1d
    const-string v3, "unregister("

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_9

    :cond_1e
    const-string v0, ")"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;

    goto :goto_8

    :cond_1f
    const-string v0, "UNKNOWN@AndroidStrings"

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;
    :try_end_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_8

    :cond_20
    move v0, v2

    goto/16 :goto_2
.end method

.method private static a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/Client$AckHandleP;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "AckHandle: "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/google/ipc/invalidation/b/i;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/ipc/invalidation/b/i;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/ipc/invalidation/b/i;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/b/i;->a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;

    move-result-object p0

    goto :goto_0
.end method

.method public static b()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ipcinv-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static b(Lcom/google/protos/ipc/invalidation/Client$AckHandleP;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$AckHandleP;->a()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v1

    const-string v2, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)Landroid/content/Intent;
    .locals 4

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->b(Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ipcinv-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v1
.end method

.method public static c()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ipcinv-internal-downcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method public static d()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ipcinv-upcall"

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/android2/a;->g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method

.method private static e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static g()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
