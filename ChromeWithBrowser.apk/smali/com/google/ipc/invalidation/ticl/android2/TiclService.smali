.class public Lcom/google/ipc/invalidation/ticl/android2/TiclService;
.super Landroid/app/IntentService;


# instance fields
.field private a:Lcom/google/ipc/invalidation/ticl/android2/Q;

.field private b:Lcom/google/ipc/invalidation/ticl/android2/b;

.field private final c:Lcom/google/a/b/B;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "TiclService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/a/b/B;

    invoke-direct {v0}, Lcom/google/a/b/B;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->c:Lcom/google/a/b/B;

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/ipc/invalidation/ticl/android2/Q;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)Lcom/google/a/b/B;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->c:Lcom/google/a/b/B;

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-direct {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;-><init>()V

    const-string v3, "TiclService"

    new-instance v4, Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-direct {v4, p0, v2, v3, v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Ljava/lang/String;B)V

    iput-object v4, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/Q;->start()V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "onHandleIntent(%s)"

    new-array v4, v0, [Ljava/lang/Object;

    new-instance v5, Lcom/google/ipc/invalidation/ticl/android2/N;

    invoke-direct {v5, p1}, Lcom/google/ipc/invalidation/ticl/android2/N;-><init>(Landroid/content/Intent;)V

    aput-object v5, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/b;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Ignoring null intent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->stop()V

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    :goto_0
    return-void

    :cond_0
    :try_start_1
    const-string v2, "ipcinv-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v0, "ipcinv-downcall"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    :try_end_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Ignoring invalid downcall message: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->stop()V

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Failed parsing ClientDowncall from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->stop()V

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    iput-object v7, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    throw v0

    :cond_2
    :try_start_5
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Handle client downcall: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-static {p0, v1}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v2, "Client does not exist on downcall"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->newInstance(IZLjava/lang/String;Lcom/google/ipc/invalidation/external/client/types/ErrorContext;)Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/ipc/invalidation/ticl/android2/n;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_3
    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Dropping client downcall since no Ticl: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->m()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/m;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    :cond_5
    :goto_2
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->k()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/m;->start()V

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->k()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/m;->stop()V

    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->o()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->f()I

    move-result v3

    if-lez v3, :cond_9

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->e()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/b/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/ipc/invalidation/ticl/android2/m;->register(Ljava/util/Collection;)V

    :cond_9
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->h()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->g()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/b/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/m;->unregister(Ljava/util/Collection;)V

    goto :goto_2

    :cond_a
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid downcall passed validation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto/16 :goto_1

    :cond_c
    const-string v2, "ipcinv-internal-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "ipcinv-internal-downcall"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    :try_start_6
    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    :try_end_6
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v2

    :try_start_7
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-virtual {v3, v2}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;)Z

    move-result v3

    if-nez v3, :cond_d

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Ignoring invalid internal downcall message: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-interface {v0, v1, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v3, "Failed parsing InternalDowncall from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/google/protobuf/InvalidProtocolBufferException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-interface {v1, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_d
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    const-string v4, "Handle internal downcall: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g()Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-static {p0, v3}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;

    move-result-object v3

    if-eqz v3, :cond_e

    :goto_3
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->f()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v1

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onMessageReceived([B)V

    :goto_4
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto/16 :goto_1

    :cond_e
    move v0, v1

    goto :goto_3

    :cond_f
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Message for unstarted Ticl; rewrite state"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getStorage()Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;

    move-result-object v0

    const-string v1, "ClientToken"

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/S;

    invoke-direct {v2, p0}, Lcom/google/ipc/invalidation/ticl/android2/S;-><init>(Lcom/google/ipc/invalidation/ticl/android2/TiclService;)V

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;->readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    goto :goto_4

    :cond_10
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->f()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onOnlineStatusChange(Z)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto/16 :goto_1

    :cond_11
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->a()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;->onAddressChange()V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto/16 :goto_1

    :cond_12
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->m()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    invoke-static {p0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    const-string v2, "Create client: creating"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->l()Z

    move-result v5

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)V

    goto/16 :goto_1

    :cond_13
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid internal downcall passed validation: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    const-string v0, "ipcinv-scheduler"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "ipcinv-scheduler"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    :try_start_8
    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;
    :try_end_8
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v1

    :try_start_9
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->b:Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v2, "Ignoring invalid scheduler event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Failed parsing SchedulerEvent from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_15
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v2, "Handle scheduler event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;

    move-result-object v2

    if-nez v2, :cond_16

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v2, "Dropping event %s; Ticl state does not exist"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;->h()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_16
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidSchedulerEvent;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto/16 :goto_1

    :cond_17
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/TiclService;->a:Lcom/google/ipc/invalidation/ticl/android2/Q;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/Q;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Received Intent without any recognized extras: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_1
.end method
