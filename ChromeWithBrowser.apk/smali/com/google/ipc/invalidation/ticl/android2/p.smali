.class public final Lcom/google/ipc/invalidation/ticl/android2/p;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

.field private final b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

.field private final c:Lcom/google/ipc/invalidation/ticl/android2/b;

.field private final d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/InvalidationListener;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/b;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->c:Lcom/google/ipc/invalidation/ticl/android2/b;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/o;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    invoke-direct {v0, p2, v1}, Lcom/google/ipc/invalidation/ticl/android2/o;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    return-void
.end method

.method private b(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "ipcinv-upcall"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->c:Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-virtual {v3, v1}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    const-string v4, "Ignoring invalid listener upcall: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    const-string v3, "Could not parse listener upcall from %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {v1, v3, v4}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/ticl/android2/p;->b(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->g()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    move-result-object v0

    invoke-interface {v1, v3, v0, v2}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->k()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v1, v0, v2}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->i()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    invoke-interface {v1, v3, v0, v2}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid invalidate upcall: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->m()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    :goto_1
    invoke-interface {v1, v2, v3, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->UNREGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    goto :goto_1

    :cond_7
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->o()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->h()Z

    move-result v4

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->q()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->r()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->f()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->h()I

    move-result v0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->s()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->t()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->j()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->newInstance(IZLjava/lang/String;Lcom/google/ipc/invalidation/external/client/types/ErrorContext;)Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->d:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    invoke-interface {v1, v2, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationListener;->informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/android2/p;->b:Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    const-string v2, "Dropping listener Intent with unknown call: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
