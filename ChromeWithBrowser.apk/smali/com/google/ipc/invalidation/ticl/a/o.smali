.class public Lcom/google/ipc/invalidation/ticl/a/o;
.super Lcom/google/ipc/invalidation/ticl/a/a;


# static fields
.field private static b:Ljava/util/concurrent/atomic/AtomicReference;

.field private static c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static f:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static g:Lcom/google/ipc/invalidation/ticl/a/k;

.field private static final h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private static i:Ljava/lang/String;


# instance fields
.field private final j:Lcom/google/a/b/B;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    const-string v0, "InvService"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v0, "https://clients4.google.com/"

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/a;-><init>()V

    new-instance v0, Lcom/google/a/b/B;

    invoke-direct {v0}, Lcom/google/a/b/B;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/o;->j:Lcom/google/a/b/B;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method static a()Lcom/google/ipc/invalidation/ticl/a/k;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/ipc/invalidation/ticl/a/r;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
    .locals 7

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/a/r;->d()Ljava/util/Map;

    move-result-object v3

    const-string v0, "ClientToken"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "No client state found in storage for %s: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-interface {v0, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v2

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/o;->j:Lcom/google/a/b/B;

    invoke-static {v3, v0, v4}, Lcom/google/android/b/c;->a(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;[BLcom/google/a/b/B;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Invalid client state found in storage for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-interface {v0, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/k;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Stopping AndroidInvalidationService since no clients remain: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/a/o;->stopSelf()V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Not stopping service since %s clients remain (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/ticl/a/k;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    invoke-virtual {p2, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientType()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getAuthType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getIntent()Landroid/content/Intent;

    move-result-object v5

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/ipc/invalidation/ticl/a/l;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->i:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setAuthType(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    :cond_0
    return-void
.end method

.method protected final c(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->start()V

    :cond_0
    return-void
.end method

.method protected final d(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->stop()V

    :cond_0
    return-void
.end method

.method protected final e(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    :cond_0
    return-void
.end method

.method protected final f(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    :cond_0
    return-void
.end method

.method protected final g(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getAckHandle()Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v1

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    :cond_0
    return-void
.end method

.method protected final h(Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;Lcom/google/ipc/invalidation/external/client/android/service/Request;Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->destroy()V

    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/a;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-super {p0}, Lcom/google/ipc/invalidation/ticl/a/a;->onCreate()V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/a/k;-><init>(Lcom/google/ipc/invalidation/ticl/a/o;)V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/k;->c()V

    :cond_0
    invoke-super {p0}, Lcom/google/ipc/invalidation/ticl/a/a;->onDestroy()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Received action = %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "message"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    const-string v0, "clientKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a(Ljava/lang/String;)Lcom/google/ipc/invalidation/ticl/a/l;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->f()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Dropping GCM message for unknown or unstarted client: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->getClientKey()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Received message for unloaded client; rewriting state file: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->e()Lcom/google/ipc/invalidation/ticl/a/r;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Ljava/lang/String;Lcom/google/ipc/invalidation/ticl/a/r;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->c()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/a/o;->j:Lcom/google/a/b/B;

    invoke-static {v0, v3}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;Lcom/google/a/b/B;)[B

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/r;->d()Ljava/util/Map;

    move-result-object v3

    const-string v4, "ClientToken"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/r;->e()V

    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/ticl/a/a;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Request;->SERVICE_INTENT:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Ljava/lang/String;)V

    :cond_2
    monitor-exit v1

    return v0

    :cond_3
    const-string v3, "echo-token"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Update %s with new echo token: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->d()Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/ipc/invalidation/ticl/a/d;->b(Ljava/lang/String;)V

    const-string v3, "data"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_4

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Deliver to %s message %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    aput-object v3, v6, v0

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/ticl/a/l;->d()Lcom/google/ipc/invalidation/ticl/a/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/ipc/invalidation/ticl/a/d;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_4
    :try_start_1
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Got mailbox intent: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    const-string v0, "register"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/k;->b()V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto/16 :goto_0

    :cond_6
    const-string v0, "error"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Unable to perform GCM registration: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/o;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "onUnbind"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0, p1}, Lcom/google/ipc/invalidation/ticl/a/a;->onUnbind(Landroid/content/Intent;)Z

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->g:Lcom/google/ipc/invalidation/ticl/a/k;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/a/k;->a()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/o;->h:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, " clients still active in onUnbind"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const-string v0, "onUnbind"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
