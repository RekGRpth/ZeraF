.class final Lcom/google/ipc/invalidation/ticl/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/external/client/android/service/Event;

.field private synthetic b:Lcom/google/ipc/invalidation/ticl/a/m;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/m;Lcom/google/ipc/invalidation/external/client/android/service/Event;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/n;->b:Lcom/google/ipc/invalidation/ticl/a/m;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/n;->a:Lcom/google/ipc/invalidation/external/client/android/service/Event;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic run(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/l;->g()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Sending %s event to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/n;->a:Lcom/google/ipc/invalidation/external/client/android/service/Event;

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAction()Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/n;->b:Lcom/google/ipc/invalidation/ticl/a/m;

    iget-object v4, v4, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/a/l;->c(Lcom/google/ipc/invalidation/ticl/a/l;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/n;->b:Lcom/google/ipc/invalidation/ticl/a/m;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/a/m;->a:Lcom/google/ipc/invalidation/ticl/a/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/l;->a(Lcom/google/ipc/invalidation/ticl/a/l;)Lcom/google/ipc/invalidation/ticl/a/o;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/n;->a:Lcom/google/ipc/invalidation/external/client/android/service/Event;

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/a/o;->a(Lcom/google/ipc/invalidation/external/client/android/service/ListenerService;Lcom/google/ipc/invalidation/external/client/android/service/Event;)V

    return-void
.end method
