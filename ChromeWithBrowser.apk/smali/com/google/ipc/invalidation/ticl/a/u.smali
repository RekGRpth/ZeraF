.class final Lcom/google/ipc/invalidation/ticl/a/u;
.super Lcom/google/ipc/invalidation/b/g;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:Lcom/google/ipc/invalidation/external/client/types/Callback;

.field private synthetic c:Lcom/google/ipc/invalidation/ticl/a/r;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/r;Ljava/lang/String;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/u;->c:Lcom/google/ipc/invalidation/ticl/a/r;

    iput-object p3, p0, Lcom/google/ipc/invalidation/ticl/a/u;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/ipc/invalidation/ticl/a/u;->b:Lcom/google/ipc/invalidation/external/client/types/Callback;

    invoke-direct {p0, p2}, Lcom/google/ipc/invalidation/b/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/u;->c:Lcom/google/ipc/invalidation/ticl/a/r;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/r;->a(Lcom/google/ipc/invalidation/ticl/a/r;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/u;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/u;->b:Lcom/google/ipc/invalidation/external/client/types/Callback;

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/r;->f()Lcom/google/ipc/invalidation/external/client/types/Status;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/ipc/invalidation/external/client/types/Callback;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/external/client/types/Status$Code;->PERMANENT_FAILURE:Lcom/google/ipc/invalidation/external/client/types/Status$Code;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No value in map for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/u;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/Status;->newInstance(Lcom/google/ipc/invalidation/external/client/types/Status$Code;Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/types/Status;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/u;->b:Lcom/google/ipc/invalidation/external/client/types/Callback;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/ipc/invalidation/external/client/types/Callback;->accept(Ljava/lang/Object;)V

    goto :goto_0
.end method
