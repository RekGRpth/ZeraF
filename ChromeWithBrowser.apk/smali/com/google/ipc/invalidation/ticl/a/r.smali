.class public final Lcom/google/ipc/invalidation/ticl/a/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$Storage;


# static fields
.field private static final a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private static final b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private static final c:Lcom/google/ipc/invalidation/external/client/types/Status;


# instance fields
.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

.field private final g:Ljava/util/Map;

.field private h:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "InvStorage"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->b(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    sget-object v0, Lcom/google/ipc/invalidation/external/client/types/Status$Code;->SUCCESS:Lcom/google/ipc/invalidation/external/client/types/Status$Code;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/Status;->newInstance(Lcom/google/ipc/invalidation/external/client/types/Status$Code;Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/types/Status;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->c:Lcom/google/ipc/invalidation/external/client/types/Status;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->g:Ljava/util/Map;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->h:Ljava/util/concurrent/ScheduledExecutorService;

    const-string v0, "context"

    invoke-static {p1, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "key"

    invoke-static {p2, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/r;->d:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/a/r;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->g:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/a/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->h()V

    return-void
.end method

.method static synthetic f()Lcom/google/ipc/invalidation/external/client/types/Status;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->c:Lcom/google/ipc/invalidation/external/client/types/Status;

    return-object v0
.end method

.method private g()Ljava/io/File;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->d:Landroid/content/Context;

    const-string v1, "InvalidationClient"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private h()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->g()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Ljava/io/OutputStream;)V

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "State written for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Unable to close state file"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_3
    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Unable to open state file"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Unable to close state file"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_5
    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Error writing state"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Unable to close state file"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_2

    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_2
    :goto_5
    throw v0

    :catch_5
    move-exception v1

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Unable to close state file"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_3

    :catch_7
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method final a()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    return-object v0
.end method

.method final a(ILandroid/accounts/Account;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    const-string v1, "No component found in event intent"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/ipc/invalidation/ticl/a/r;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->e(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->f(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->h()V

    return-void
.end method

.method final b()Z
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->g()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v4

    iput-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unexpected client key mismatch: %s, %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v7}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v0, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unable to close state file"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-interface {v3, v4, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Loaded metadata: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/ipc/invalidation/ticl/a/r;->f:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    aput-object v8, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;

    sget-object v5, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v6, "Loaded property: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-interface {v5, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/r;->g:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v0

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v3

    :goto_3
    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_4
    move v0, v1

    goto :goto_1

    :cond_3
    :try_start_5
    sget-object v0, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Loaded state for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-interface {v0, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v3, :cond_4

    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_4
    :goto_5
    move v0, v2

    goto :goto_1

    :catch_2
    move-exception v0

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unable to close state file"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-interface {v3, v4, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :catch_3
    move-exception v0

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unable to close state file"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-interface {v3, v4, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :catch_4
    move-exception v3

    move-object v9, v3

    move-object v3, v0

    move-object v0, v9

    :goto_6
    :try_start_7
    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Error reading client state"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v3, :cond_2

    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_4

    :catch_5
    move-exception v0

    sget-object v3, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unable to close state file"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    invoke-interface {v3, v4, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :catchall_0
    move-exception v3

    move-object v9, v3

    move-object v3, v0

    move-object v0, v9

    :goto_7
    if-eqz v3, :cond_5

    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :cond_5
    :goto_8
    throw v0

    :catch_6
    move-exception v3

    sget-object v4, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Unable to close state file"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    invoke-interface {v4, v5, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_8

    :catchall_1
    move-exception v0

    goto :goto_7

    :catch_7
    move-exception v0

    goto :goto_6

    :catch_8
    move-exception v3

    goto :goto_3
.end method

.method final c()V
    .locals 6

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->g()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    sget-object v1, Lcom/google/ipc/invalidation/ticl/a/r;->a:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Deleted state for %s from %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/a/r;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final d()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->g:Ljava/util/Map;

    return-object v0
.end method

.method public final deleteKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/s;

    const-string v2, "AndroidStorage.deleteKey"

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/s;-><init>(Lcom/google/ipc/invalidation/ticl/a/r;Ljava/lang/String;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/a/r;->h()V

    return-void
.end method

.method public final readAllKeys(Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/t;

    const-string v2, "AndroidStorage.readAllKeys"

    invoke-direct {v1, p0, v2, p1}, Lcom/google/ipc/invalidation/ticl/a/t;-><init>(Lcom/google/ipc/invalidation/ticl/a/r;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final readKey(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/r;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/ipc/invalidation/ticl/a/u;

    const-string v2, "AndroidStorage.readKey"

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/google/ipc/invalidation/ticl/a/u;-><init>(Lcom/google/ipc/invalidation/ticl/a/r;Ljava/lang/String;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final setSystemResources(Lcom/google/ipc/invalidation/external/client/SystemResources;)V
    .locals 0

    return-void
.end method

.method public final writeKey(Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V
    .locals 7

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/a/r;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/a/v;

    const-string v2, "AndroidStorage.writeKey"

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/a/v;-><init>(Lcom/google/ipc/invalidation/ticl/a/r;Ljava/lang/String;Ljava/lang/String;[BLcom/google/ipc/invalidation/external/client/types/Callback;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
