.class final Lcom/google/ipc/invalidation/ticl/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel$NetworkListener;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/ticl/l;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/m;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAddressChange()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/m;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/m;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0, v2, v2}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;ZZ)V

    return-void
.end method

.method public final onMessageReceived([B)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/m;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/l;->a([B)V

    return-void
.end method

.method public final onOnlineStatusChange(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/m;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/l;->a(Z)V

    return-void
.end method
