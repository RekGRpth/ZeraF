.class public final Lcom/google/ipc/invalidation/a/X;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/ipc/invalidation/a/Q;

.field final b:Lcom/google/ipc/invalidation/a/Q;

.field final c:Lcom/google/ipc/invalidation/a/Q;

.field final d:Lcom/google/ipc/invalidation/a/Q;

.field final e:Lcom/google/ipc/invalidation/a/Q;

.field final f:Lcom/google/ipc/invalidation/a/Q;

.field final g:Lcom/google/ipc/invalidation/a/Q;

.field final synthetic h:Lcom/google/ipc/invalidation/a/R;

.field private i:Lcom/google/ipc/invalidation/a/Q;

.field private j:Lcom/google/ipc/invalidation/a/Q;

.field private k:Lcom/google/ipc/invalidation/a/Q;


# direct methods
.method private constructor <init>(Lcom/google/ipc/invalidation/a/R;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/X;->h:Lcom/google/ipc/invalidation/a/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/a/Y;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->C:Lcom/google/ipc/invalidation/a/E;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/E;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/E;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/Y;-><init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->a:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->o:Lcom/google/ipc/invalidation/a/q;

    new-array v2, v7, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/q;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->b:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Z;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->m:Lcom/google/ipc/invalidation/a/n;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/n;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/n;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/Z;-><init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->i:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/aa;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->l:Lcom/google/ipc/invalidation/a/m;

    const/4 v2, 0x6

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/m;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->i:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/m;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/m;->c:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/m;->e:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/ipc/invalidation/a/m;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/google/ipc/invalidation/a/m;->f:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/aa;-><init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->c:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->r:Lcom/google/ipc/invalidation/a/t;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/t;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->i:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/t;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->e:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/ab;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->v:Lcom/google/ipc/invalidation/a/x;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/x;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/x;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/ab;-><init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->f:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/ac;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->p:Lcom/google/ipc/invalidation/a/r;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/r;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/r;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, p0, v1, v2}, Lcom/google/ipc/invalidation/a/ac;-><init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->j:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->n:Lcom/google/ipc/invalidation/a/p;

    new-array v2, v5, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/p;->a:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/p;->b:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->j:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->k:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->b:Lcom/google/ipc/invalidation/a/c;

    const/16 v2, 0xd

    new-array v2, v2, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/c;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->a:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/google/ipc/invalidation/a/c;->b:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/google/ipc/invalidation/a/c;->c:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/ipc/invalidation/a/c;->d:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/ipc/invalidation/a/c;->e:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v3}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->f:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->g:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->h:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->i:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->k:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->j:Lcom/google/ipc/invalidation/a/N;

    iget-object v5, p0, Lcom/google/ipc/invalidation/a/X;->k:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v4, v5}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->l:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/google/ipc/invalidation/a/c;->m:Lcom/google/ipc/invalidation/a/N;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/O;->b(Lcom/google/ipc/invalidation/a/N;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->g:Lcom/google/ipc/invalidation/a/Q;

    new-instance v0, Lcom/google/ipc/invalidation/a/Q;

    sget-object v1, Lcom/google/ipc/invalidation/a/a;->k:Lcom/google/ipc/invalidation/a/l;

    new-array v2, v7, [Lcom/google/ipc/invalidation/a/O;

    sget-object v3, Lcom/google/ipc/invalidation/a/l;->a:Lcom/google/ipc/invalidation/a/N;

    iget-object v4, p0, Lcom/google/ipc/invalidation/a/X;->c:Lcom/google/ipc/invalidation/a/Q;

    invoke-static {v3, v4}, Lcom/google/ipc/invalidation/a/O;->a(Lcom/google/ipc/invalidation/a/N;Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/O;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/a/X;->d:Lcom/google/ipc/invalidation/a/Q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ipc/invalidation/a/R;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/a/X;-><init>(Lcom/google/ipc/invalidation/a/R;)V

    return-void
.end method
