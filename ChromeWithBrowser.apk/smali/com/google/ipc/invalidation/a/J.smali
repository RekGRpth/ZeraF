.class public abstract Lcom/google/ipc/invalidation/a/J;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lcom/google/ipc/invalidation/b/a;


# direct methods
.method protected constructor <init>(Lcom/google/ipc/invalidation/b/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/J;->a:Lcom/google/ipc/invalidation/b/a;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/a/Q;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/a/O;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/O;->a()Lcom/google/ipc/invalidation/a/N;

    move-result-object v1

    invoke-static {p2}, Lcom/google/ipc/invalidation/a/Q;->a(Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/M;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/google/ipc/invalidation/a/M;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/O;->b()Lcom/google/ipc/invalidation/a/P;

    move-result-object v6

    sget-object v7, Lcom/google/ipc/invalidation/a/P;->a:Lcom/google/ipc/invalidation/a/P;

    if-ne v6, v7, :cond_1

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/google/ipc/invalidation/a/J;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v2, "Required field not set: %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/O;->a()Lcom/google/ipc/invalidation/a/N;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/N;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-interface {v1, v2, v4}, Lcom/google/ipc/invalidation/b/a;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    :goto_0
    return v0

    :cond_1
    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/O;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p2}, Lcom/google/ipc/invalidation/a/Q;->a(Lcom/google/ipc/invalidation/a/Q;)Lcom/google/ipc/invalidation/a/M;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/google/ipc/invalidation/a/M;->b(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/util/List;

    if-eqz v2, :cond_3

    check-cast v1, Ljava/util/List;

    :goto_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/a/O;->c()Lcom/google/ipc/invalidation/a/Q;

    move-result-object v6

    invoke-virtual {p0, v1, v6}, Lcom/google/ipc/invalidation/a/J;->a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/Q;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/google/ipc/invalidation/a/K;

    invoke-direct {v2, v1}, Lcom/google/ipc/invalidation/a/K;-><init>(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p2, p1}, Lcom/google/ipc/invalidation/a/Q;->a(Lcom/google/protobuf/MessageLite;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/J;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v1, "Failed post-validation of message (%s): %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    aput-object p1, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    goto :goto_0

    :cond_5
    move v0, v4

    goto :goto_0
.end method
