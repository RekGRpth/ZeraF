.class public Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;
.super Ljava/lang/Object;


# static fields
.field private static final clientMap:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;
    .locals 7

    const-string v0, "context"

    invoke-static {p0, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p3, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "authType must be a non-empty string value"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    const-string v0, "listenerClass"

    invoke-static {p5, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->resume(Landroid/content/Context;Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->initialize()V

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method static release(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static resetForTest()V
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public static resume(Landroid/content/Context;Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->resume(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-result-object v0

    return-object v0
.end method

.method public static resume(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->clientMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->addReference()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;

    invoke-direct {v0, p0, p1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->initResumed(Z)V

    goto :goto_0
.end method
