.class public Lcom/google/ipc/invalidation/external/client/android/service/Event$Parameter;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message$Parameter;


# static fields
.field public static final ERROR_INFO:Ljava/lang/String; = "errorInfo"

.field public static final INVALIDATION:Ljava/lang/String; = "invalidation"

.field public static final IS_TRANSIENT:Ljava/lang/String; = "isTransient"

.field public static final PREFIX:Ljava/lang/String; = "prefix"

.field public static final PREFIX_LENGTH:Ljava/lang/String; = "prefixLength"

.field public static final REGISTRATION_STATE:Ljava/lang/String; = "registrationState"


# instance fields
.field final synthetic this$0:Lcom/google/ipc/invalidation/external/client/android/service/Event;


# direct methods
.method public constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/Event;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Parameter;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/Event;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Message$Parameter;-><init>()V

    return-void
.end method
