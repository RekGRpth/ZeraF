.class public final Lcom/google/ipc/invalidation/external/client/android2/AndroidClientFactory;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createClient(Landroid/content/Context;Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;[B)V
    .locals 3

    invoke-static {}, Lcom/google/ipc/invalidation/ticl/l;->a()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, p2, v0, v2}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v1, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/android2/O;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
