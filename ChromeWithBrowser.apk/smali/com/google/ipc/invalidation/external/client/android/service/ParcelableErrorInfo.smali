.class Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo$1;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo$1;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v1

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->newInstance(IZLjava/lang/String;Lcom/google/ipc/invalidation/external/client/types/ErrorContext;)Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;

    iget-object v1, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorReason()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->isTransient()Z

    move-result v2

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
