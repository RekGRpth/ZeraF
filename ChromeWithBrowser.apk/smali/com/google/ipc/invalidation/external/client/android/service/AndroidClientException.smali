.class public Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;
.super Ljava/lang/RuntimeException;


# instance fields
.field final message:Ljava/lang/String;

.field final status:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;->status:I

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/android/service/AndroidClientException;->message:Ljava/lang/String;

    return-void
.end method
