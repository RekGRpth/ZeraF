.class Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "onServiceConnected: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$100(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    invoke-virtual {v2, p2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->asInterface(Landroid/os/IBinder;)Ljava/lang/Object;

    move-result-object v2

    # setter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$202(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # invokes: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->handleQueue()V
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$300(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 5

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "onServiceDisconnected: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;
    invoke-static {v4}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$400(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$100(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    const/4 v2, 0x0

    # setter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$202(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
