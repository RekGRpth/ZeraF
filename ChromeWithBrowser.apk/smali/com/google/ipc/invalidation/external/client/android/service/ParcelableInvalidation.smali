.class Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final includePayload:Z

.field final invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation$1;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation$1;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v1

    aget-boolean v5, v1, v2

    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v6

    const/4 v1, 0x0

    aget-boolean v6, v6, v2

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    :cond_0
    iget-object v0, v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-static {v0, v3, v4, v1, v5}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->newInstance(Lcom/google/ipc/invalidation/external/client/types/ObjectId;J[BZ)Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->includePayload:Z

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/external/client/types/Invalidation;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    iput-boolean p2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->includePayload:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;

    iget-object v1, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;-><init>(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getVersion()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    new-array v0, v3, [Z

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getIsTrickleRestartForInternalUse()Z

    move-result v1

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->includePayload:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    new-array v1, v3, [Z

    aput-boolean v3, v1, v2

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    :goto_0
    return-void

    :cond_0
    new-array v0, v3, [Z

    aput-boolean v2, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    goto :goto_0
.end method
