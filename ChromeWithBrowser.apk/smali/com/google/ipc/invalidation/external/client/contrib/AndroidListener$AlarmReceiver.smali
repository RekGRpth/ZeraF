.class public final Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$AlarmReceiver;
.super Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->issueAndroidListenerIntent(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
