.class public abstract Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;
.super Landroid/app/Service;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/InvalidationListener;


# static fields
.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# instance fields
.field private isCreated:Z

.field private final listenerBinder:Lcom/google/ipc/invalidation/external/client/android/service/ListenerService$Stub;

.field private final lock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvListener"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener$1;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener$1;-><init>(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->listenerBinder:Lcom/google/ipc/invalidation/external/client/android/service/ListenerService$Stub;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->lock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->isCreated:Z

    return-void
.end method


# virtual methods
.method protected handleEvent(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 10

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->lock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->isCreated:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Dropping bundle since not created: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v0, v1, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Event;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getActionOrdinal()I

    move-result v1

    invoke-static {v1, p2}, Lcom/google/ipc/invalidation/external/client/android/service/Response;->newBuilder(ILandroid/os/Bundle;)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAction()Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getClientKey()Ljava/lang/String;

    move-result-object v5

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v6, "Received %s event for %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    invoke-interface {v1, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v5, :cond_2

    :try_start_1
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Missing client id:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v4, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Failure in handleEvent"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    :try_start_3
    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->release()V

    :cond_1
    :goto_1
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    const/4 v6, 0x0

    :try_start_4
    invoke-static {p0, v5, v6}, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->resume(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;

    move-result-object v1

    sget-object v6, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v7, "%s event for %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v9, 0x1

    aput-object v5, v8, v9

    invoke-interface {v6, v7, v8}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v5, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener$2;->$SwitchMap$com$google$ipc$invalidation$external$client$android$service$Event$Action:[I

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    sget-object v4, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v5, "Urecognized event: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;->setStatus(I)Lcom/google/ipc/invalidation/external/client/android/service/Response$Builder;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_1

    :try_start_5
    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->release()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :pswitch_0
    :try_start_6
    invoke-virtual {p0, v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->ready(Lcom/google/ipc/invalidation/external/client/InvalidationClient;)V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3

    :try_start_7
    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;->release()V

    :cond_3
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :pswitch_1
    :try_start_8
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getInvalidation()Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAckHandle()Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v0

    invoke-virtual {p0, v1, v4, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->invalidate(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/Invalidation;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_2

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAckHandle()Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v0

    invoke-virtual {p0, v1, v4, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_2

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getAckHandle()Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->invalidateAll(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    goto :goto_2

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getRegistrationState()Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    move-result-object v0

    invoke-virtual {p0, v1, v4, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->informRegistrationStatus(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V

    goto :goto_2

    :pswitch_5
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getError()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getIsTransient()Z

    move-result v0

    invoke-virtual {p0, v1, v4, v0, v5}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V

    goto :goto_2

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getPrefix()[B

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getPrefixLength()I

    move-result v0

    invoke-virtual {p0, v1, v4, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->reissueRegistrations(Lcom/google/ipc/invalidation/external/client/InvalidationClient;[BI)V

    goto :goto_2

    :pswitch_7
    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getErrorInfo()Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->informError(Lcom/google/ipc/invalidation/external/client/InvalidationClient;Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 5

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Binding: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->listenerBinder:Lcom/google/ipc/invalidation/external/client/android/service/ListenerService$Stub;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCreate()V
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "onCreate: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->isCreated:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDestroy()V
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "onDestroy: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationListener;->isCreated:Z

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
