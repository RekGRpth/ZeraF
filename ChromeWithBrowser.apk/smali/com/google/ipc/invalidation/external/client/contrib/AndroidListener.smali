.class public abstract Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;
.super Landroid/app/IntentService;


# static fields
.field static initialMaxDelayMs:I

.field static lastClientIdForTest:[B

.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field static maxDelayFactor:I


# instance fields
.field private final clock:Lcom/google/ipc/invalidation/ticl/android2/a;

.field private intentMapper:Lcom/google/ipc/invalidation/ticl/android2/p;

.field private final invalidationListener:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

.field private state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, ""

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->initialMaxDelayMs:I

    const/16 v0, 0x168

    sput v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->maxDelayFactor:I

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$1;-><init>(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->invalidationListener:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/a;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/ticl/android2/a;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->clock:Lcom/google/ipc/invalidation/ticl/android2/a;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->setIntentRedelivery(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;)Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    return-object v0
.end method

.method public static createAcknowledgeIntent(Landroid/content/Context;[B)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->createAckIntent(Landroid/content/Context;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createRegisterIntent(Landroid/content/Context;[BLjava/lang/Iterable;)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->createRegistrationIntent(Landroid/content/Context;[BLjava/lang/Iterable;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createStartIntent(Landroid/content/Context;I[B)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->createStartIntent(Landroid/content/Context;I[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createStopIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->createStopIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createUnregisterIntent(Landroid/content/Context;[BLjava/lang/Iterable;)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->createRegistrationIntent(Landroid/content/Context;[BLjava/lang/Iterable;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getClient()Lcom/google/ipc/invalidation/external/client/InvalidationClient;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->intentMapper:Lcom/google/ipc/invalidation/ticl/android2/p;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/android2/p;->a:Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    return-object v0
.end method

.method private getPersistentState()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->readState()[B

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->a([B)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->isValidAndroidListenerState(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Invalid listener state."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Failed to parse listener state: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private initializeState()V
    .locals 4

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getPersistentState()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    sget v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->initialMaxDelayMs:I

    sget v3, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->maxDelayFactor:I

    invoke-direct {v1, v2, v3, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;-><init>(IILcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)V

    iput-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    sget v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->initialMaxDelayMs:I

    sget v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->maxDelayFactor:I

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;-><init>(II)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    goto :goto_0
.end method

.method private issueRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)V
    .locals 1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->addDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getClient()Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->removeDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getClient()Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    goto :goto_0
.end method

.method public static setAuthToken(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1, p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->issueAuthTokenResponse(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private tryHandleAckIntent(Landroid/content/Intent;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->findAckHandle(Landroid/content/Intent;)[B

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getClient()Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    move-result-object v1

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private tryHandleAuthTokenRequestIntent(Landroid/content/Intent;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->isAuthTokenRequest(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    const-string v0, "com.google.ipc.invalidaton.AUTH_TOKEN_INVALIDATE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "com.google.ipc.invalidation.AUTH_TOKEN_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Authorization request without pending intent extra."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->requestAuthToken(Landroid/app/PendingIntent;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private tryHandleRegistrationIntent(Landroid/content/Intent;)Z
    .locals 11

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->findRegistrationCommand(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {v9}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->isValidRegistrationCommand(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v8

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v9}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Ignoring registration request for old client. Old ID = {0}, New ID = {1}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->i()Lcom/google/protobuf/ByteString;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/protobuf/ByteString;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v9}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f()Z

    move-result v4

    invoke-virtual {v9}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->k()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0, v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getNextDelay(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)I

    move-result v5

    :goto_2
    if-nez v5, :cond_3

    invoke-direct {p0, v3, v4}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->issueRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->clock:Lcom/google/ipc/invalidation/ticl/android2/a;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getClientId()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iget-object v6, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v6}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getNextRequestCode()I

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->issueDelayedRegistrationIntent(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZII)V

    goto :goto_1

    :cond_4
    move v0, v7

    goto :goto_0

    :cond_5
    move v5, v8

    goto :goto_2
.end method

.method private tryHandleStartIntent(Landroid/content/Intent;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->findStartCommand(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->isValidStartCommand(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    sget v3, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->initialMaxDelayMs:I

    sget v4, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->maxDelayFactor:I

    invoke-direct {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;-><init>(II)V

    iput-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->f()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->h()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v3

    invoke-static {v2, v1, v3, v0}, Lcom/google/ipc/invalidation/ticl/android2/a;->a(I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->issueTiclIntent(Landroid/content/Context;Landroid/content/Intent;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private tryHandleStopIntent(Landroid/content/Intent;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->isStopIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getClient()Lcom/google/ipc/invalidation/external/client/InvalidationClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/InvalidationClient;->stop()V

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public acknowledge([B)V
    .locals 2

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createAcknowledgeIntent(Landroid/content/Context;[B)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method getStateForTest()Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    return-object v0
.end method

.method public abstract informError(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
.end method

.method public abstract informRegistrationFailure([BLcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
.end method

.method public abstract informRegistrationStatus([BLcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
.end method

.method public abstract invalidate(Lcom/google/ipc/invalidation/external/client/types/Invalidation;[B)V
.end method

.method public abstract invalidateAll([B)V
.end method

.method public abstract invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/types/ObjectId;[B)V
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/p;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->invalidationListener:Lcom/google/ipc/invalidation/external/client/InvalidationListener;

    invoke-direct {v0, v1, p0}, Lcom/google/ipc/invalidation/ticl/android2/p;-><init>(Lcom/google/ipc/invalidation/external/client/InvalidationListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->intentMapper:Lcom/google/ipc/invalidation/ticl/android2/p;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->initializeState()V

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->tryHandleAuthTokenRequestIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->tryHandleRegistrationIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->tryHandleStartIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->tryHandleStopIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->tryHandleAckIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->intentMapper:Lcom/google/ipc/invalidation/ticl/android2/p;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/android2/p;->a(Landroid/content/Intent;)V

    :cond_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->getIsDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->marshal()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->b()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->writeState([B)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->state:Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->resetIsDirty()V

    goto :goto_0
.end method

.method public abstract readState()[B
.end method

.method public abstract ready([B)V
.end method

.method public register([BLjava/lang/Iterable;)V
    .locals 2

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createRegisterIntent(Landroid/content/Context;[BLjava/lang/Iterable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public abstract reissueRegistrations([B)V
.end method

.method public abstract requestAuthToken(Landroid/app/PendingIntent;Ljava/lang/String;)V
.end method

.method public unregister([BLjava/lang/Iterable;)V
    .locals 2

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createUnregisterIntent(Landroid/content/Context;[BLjava/lang/Iterable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public abstract writeState([B)V
.end method
