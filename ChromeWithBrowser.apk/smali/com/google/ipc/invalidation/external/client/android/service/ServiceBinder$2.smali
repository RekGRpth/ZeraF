.class Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;


# instance fields
.field final synthetic this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Ljava/lang/Object;)V
    .locals 7

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$100(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v2, "Unbinding %s from %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;
    invoke-static {v5}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$400(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;
    invoke-static {v5}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$200(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$600(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceConnection:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$500(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    const/4 v2, 0x0

    # setter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$202(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    const/4 v2, 0x0

    # setter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z
    invoke-static {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$702(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Z)Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;
    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$800(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v2, "Still have %s work items in release of %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;
    invoke-static {v5}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$800(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/util/Queue;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;
    invoke-static {v5}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$400(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Exception unbinding from %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;->this$0:Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;

    # getter for: Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;
    invoke-static {v6}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->access$400(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
