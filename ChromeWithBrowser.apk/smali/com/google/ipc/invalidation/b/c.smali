.class public final Lcom/google/ipc/invalidation/b/c;
.super Lcom/google/ipc/invalidation/b/e;

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final a:Lcom/google/ipc/invalidation/b/c;

.field private static final b:[C

.field private static final c:[C

.field private static final d:[C


# instance fields
.field private final e:[B

.field private volatile f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/16 v3, 0x100

    const/4 v1, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/b/c;

    new-array v2, v1, [B

    invoke-direct {v0, v2}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    sput-object v0, Lcom/google/ipc/invalidation/b/c;->a:Lcom/google/ipc/invalidation/b/c;

    new-array v0, v3, [C

    sput-object v0, Lcom/google/ipc/invalidation/b/c;->b:[C

    new-array v0, v3, [C

    sput-object v0, Lcom/google/ipc/invalidation/b/c;->c:[C

    new-array v0, v3, [C

    sput-object v0, Lcom/google/ipc/invalidation/b/c;->d:[C

    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/ipc/invalidation/b/c;->b:[C

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const-string v2, "\\%03o"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->b:[C

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v3, v0

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->c:[C

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v3, v0

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->d:[C

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    aput-char v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ipc/invalidation/b/c;->f:I

    iput-object p1, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    return-void
.end method

.method public static a(Lcom/google/ipc/invalidation/b/i;[B)Lcom/google/ipc/invalidation/b/i;
    .locals 5

    const/16 v4, 0x5c

    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-byte v0, p1, v1

    sparse-switch v0, :sswitch_data_0

    const/16 v3, 0x20

    if-lt v0, v3, :cond_0

    const/16 v3, 0x7f

    if-ge v0, v3, :cond_0

    const/16 v3, 0x27

    if-eq v0, v3, :cond_0

    int-to-char v0, v0

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    const/16 v0, 0x6e

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    const/16 v0, 0x72

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    const/16 v0, 0x74

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :cond_0
    if-gez v0, :cond_1

    add-int/lit16 v0, v0, 0x100

    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->b:[C

    aget-char v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->c:[C

    aget-char v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    sget-object v3, Lcom/google/ipc/invalidation/b/c;->d:[C

    aget-char v0, v3, v0

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/b/i;->a(C)Lcom/google/ipc/invalidation/b/i;

    goto :goto_1

    :cond_2
    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_0
        0xd -> :sswitch_1
        0x22 -> :sswitch_3
        0x5c -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(Lcom/google/protobuf/ByteString;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/b/c;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/b/i;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/b/i;-><init>()V

    invoke-static {v0, p0}, Lcom/google/ipc/invalidation/b/c;->a(Lcom/google/ipc/invalidation/b/i;[B)Lcom/google/ipc/invalidation/b/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/i;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/ipc/invalidation/b/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/b/c;->a(Lcom/google/ipc/invalidation/b/i;[B)Lcom/google/ipc/invalidation/b/i;

    return-void
.end method

.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    return-object v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 6

    const/4 v0, 0x0

    check-cast p1, Lcom/google/ipc/invalidation/b/c;

    iget-object v1, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    iget-object v2, p1, Lcom/google/ipc/invalidation/b/c;->e:[B

    if-nez v1, :cond_1

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    array-length v3, v1

    array-length v4, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_4

    aget-byte v4, v1, v0

    aget-byte v5, v2, v0

    if-eq v4, v5, :cond_3

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    sub-int v0, v1, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    array-length v0, v1

    array-length v1, v2

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/ipc/invalidation/b/c;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/ipc/invalidation/b/c;

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    iget-object v1, p1, Lcom/google/ipc/invalidation/b/c;->e:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget v0, p0, Lcom/google/ipc/invalidation/b/c;->f:I

    if-nez v0, :cond_2

    iget-object v4, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/c;->e:[B

    array-length v2, v0

    const/4 v0, 0x0

    move v1, v0

    move v0, v2

    :goto_0
    if-ge v1, v2, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    aget-byte v3, v4, v1

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput v0, p0, Lcom/google/ipc/invalidation/b/c;->f:I

    :cond_2
    return v0
.end method
