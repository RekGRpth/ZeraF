.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcallOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->j()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->b(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;I)I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
