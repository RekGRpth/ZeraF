.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcallOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private c:Z

.field private d:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 3

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c:Z

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d:Ljava/lang/Object;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    goto :goto_0

    :cond_4
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    goto :goto_1
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d:Ljava/lang/Object;

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->c:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;Z)Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;I)I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
