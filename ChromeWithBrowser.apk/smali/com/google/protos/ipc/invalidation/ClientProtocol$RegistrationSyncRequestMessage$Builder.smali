.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessageOrBuilder;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;
    .locals 1

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :pswitch_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :cond_0
    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;B)V

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage$Builder;

    move-result-object v0

    return-object v0
.end method
