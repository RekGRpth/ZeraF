.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessageOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/protobuf/ByteString;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 3

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    goto :goto_1
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
