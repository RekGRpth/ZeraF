.class public final enum Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

.field private static enum b:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    const-string v1, "BATCH"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->b:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    sget-object v1, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->b:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    aput-object v1, v0, v3

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->a:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->b:Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->c:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$MajorVersion;->c:I

    return v0
.end method
