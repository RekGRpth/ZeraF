.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigPOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->b:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->b:I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    :cond_2
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;I)I

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;Ljava/util/List;)Ljava/util/List;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
