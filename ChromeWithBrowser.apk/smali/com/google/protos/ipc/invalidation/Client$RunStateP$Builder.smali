.class public final Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Client$RunStatePOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;)Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;->a(I)Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;-><init>(Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/Client$RunStateP$Builder;->b:Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;)Lcom/google/protos/ipc/invalidation/Client$RunStateP$State;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->a(Lcom/google/protos/ipc/invalidation/Client$RunStateP;I)I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
