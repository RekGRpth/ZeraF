.class public final Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidState$StoredStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

.field private d:Ljava/util/List;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a:Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;-><init>(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a:Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f:I

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    iput v2, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->c:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d:Ljava/util/List;

    return-object v0
.end method

.method public final h()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->e:B

    goto :goto_0
.end method
