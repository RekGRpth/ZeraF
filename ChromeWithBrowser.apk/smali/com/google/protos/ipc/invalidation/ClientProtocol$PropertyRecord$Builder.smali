.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecordOrBuilder;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->b:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->b:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v2

    if-eq v1, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->c:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->b:Ljava/lang/Object;

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord$Builder;->c:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;I)I

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
