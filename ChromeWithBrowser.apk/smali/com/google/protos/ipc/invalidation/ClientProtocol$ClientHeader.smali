.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeaderOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

.field private d:Lcom/google/protobuf/ByteString;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

.field private f:J

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->f:J

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g:J

    const-string v1, ""

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->i:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->j:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->k:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->j:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->k:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->i:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->f:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g:J

    return-wide p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v0

    return-object v0
.end method

.method private t()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->f:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->t()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_6
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->f:J

    invoke-static {v4, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->t()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->k:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->f:J

    return-wide v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->g:J

    return-wide v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->a(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->i:I

    return v0
.end method

.method public final s()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->j:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->j:B

    goto :goto_0
.end method
