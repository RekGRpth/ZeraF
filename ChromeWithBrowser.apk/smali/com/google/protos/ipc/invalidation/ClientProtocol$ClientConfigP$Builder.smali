.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigPOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:I

.field private k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

.field private l:Z

.field private m:I

.field private n:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    const v1, 0xea60

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d:I

    const v0, 0x124f80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->e:I

    const v0, 0x1499700

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f:I

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->g:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->h:I

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->j:I

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->m:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->n:Z

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->E()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c:I

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->e:I

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f:I

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->g:I

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->h:I

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->i:Z

    goto/16 :goto_0

    :sswitch_9
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->j:I

    goto/16 :goto_0

    :sswitch_a
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    goto/16 :goto_0

    :sswitch_b
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->l:Z

    goto/16 :goto_0

    :sswitch_c
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->m:I

    goto/16 :goto_0

    :sswitch_d
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->n:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n()I

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f:I

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p()I

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->g:I

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->r()I

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->h:I

    :cond_8
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->s()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->t()Z

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->i:Z

    :cond_9
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->u()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->j:I

    :cond_a
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->w()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->x()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_f

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v2

    if-eq v1, v2, :cond_f

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    :goto_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    :cond_b
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->y()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->A()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->B()I

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->m:I

    :cond_d
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->D()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    goto/16 :goto_0

    :cond_e
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto/16 :goto_1

    :cond_f
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    goto :goto_2
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->l:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d:I

    return-object p0
.end method

.method public final b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->n:Z

    return-object p0
.end method

.method public final c(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->e:I

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->E()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->c:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->e:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->f:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->g:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->h:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->i:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->j:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->k:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->l:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->m:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->n:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->c(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;I)I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method
