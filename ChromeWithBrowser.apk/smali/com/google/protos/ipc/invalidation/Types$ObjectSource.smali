.class public final Lcom/google/protos/ipc/invalidation/Types$ObjectSource;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Types$ObjectSourceOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->d:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->e:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->d:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;-><init>(Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Types$ObjectSource;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Types$ObjectSource;Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;)Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/Types$ObjectSource;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->a:Lcom/google/protos/ipc/invalidation/Types$ObjectSource;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;->c()Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/Types$ObjectSource;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->newBuilder()Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;->a(Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Builder;)Lcom/google/protos/ipc/invalidation/Types$ObjectSource;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;->a()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iput v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->e:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->c:Lcom/google/protos/ipc/invalidation/Types$ObjectSource$Type;

    return-object v0
.end method

.method public final g()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->d:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Types$ObjectSource;->d:B

    goto :goto_0
.end method
