.class public final Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncallOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

.field private f:Z

.field private g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f:Z

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f:Z

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->i:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->d:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$ServerMessage;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->e:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->g:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    return-object v0
.end method

.method public final o()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall;->h:B

    goto :goto_0
.end method
