.class public final Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessageOrBuilder;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/protobuf/ByteString;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->b:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->b:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;
    .locals 4

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->d()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    move-result-object v2

    if-eq v1, v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->b:Ljava/lang/Object;

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->h()Lcom/google/protobuf/ByteString;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    :cond_3
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;-><init>(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;->a(Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidChannel$AddressedAndroidMessage$Builder;

    move-result-object v0

    return-object v0
.end method
