.class public final Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidState$StoredStateOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

.field private c:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->h()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 3

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x4a -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->d()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v2

    if-eq v1, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->f()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    :cond_0
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    iget v1, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->g()V

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;-><init>(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;Ljava/util/List;)Ljava/util/List;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->a(Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;)Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    :goto_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->a:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->b:Lcom/google/protos/ipc/invalidation/AndroidState$ClientMetadata;

    goto :goto_0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidState$ClientProperty;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState;->h()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidState$StoredState$Builder;

    move-result-object v0

    return-object v0
.end method
