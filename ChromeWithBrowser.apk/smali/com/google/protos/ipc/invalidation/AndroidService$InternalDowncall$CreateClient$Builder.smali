.class public final Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClientOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/protobuf/ByteString;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 3

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->e:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    goto :goto_1
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->e:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
