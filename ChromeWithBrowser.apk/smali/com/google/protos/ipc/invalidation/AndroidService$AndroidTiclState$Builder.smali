.class public final Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

.field private d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    :cond_5
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->h()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    :goto_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    :goto_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    goto/16 :goto_0

    :cond_4
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto :goto_1

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    goto :goto_3
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->k()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;I)I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
