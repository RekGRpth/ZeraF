.class public final enum Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

.field private static enum c:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    const-string v1, "TEST"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->a:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    const-string v1, "ANDROID"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v3, v2}, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->b:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    new-instance v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    const-string v1, "LCS"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v5, v2}, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->c:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->a:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->b:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->c:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    aput-object v1, v0, v5

    new-instance v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->d:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->a:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->b:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->c:Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x71 -> :sswitch_1
        0x72 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Channel$NetworkEndpointId$NetworkAddress;->d:I

    return v0
.end method
