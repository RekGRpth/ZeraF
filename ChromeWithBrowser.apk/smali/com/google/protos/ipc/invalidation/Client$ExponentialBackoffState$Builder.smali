.class public final Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffStateOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    goto :goto_0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->i()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->f()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;-><init>(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState$Builder;->c:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->b(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
