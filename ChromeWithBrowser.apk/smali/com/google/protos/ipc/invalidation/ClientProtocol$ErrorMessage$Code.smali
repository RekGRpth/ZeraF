.class public final enum Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

.field private static final synthetic d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    const-string v1, "AUTH_FAILURE"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    const-string v1, "UNKNOWN_FAILURE"

    const/16 v2, 0x2710

    invoke-direct {v0, v1, v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->c:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2710 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a()[Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    invoke-virtual {v0}, [Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage$Code;->c:I

    return v0
.end method
