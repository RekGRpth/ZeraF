.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessageOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;


# instance fields
.field private b:I

.field private c:J

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c:J

    return-wide p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c:J

    invoke-static {v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->c:J

    return-wide v0
.end method

.method public final g()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->d:B

    goto :goto_0
.end method
