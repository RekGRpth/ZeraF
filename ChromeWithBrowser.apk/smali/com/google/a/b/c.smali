.class final enum Lcom/google/a/b/c;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/a/b/c;

.field public static final enum b:Lcom/google/a/b/c;

.field public static final enum c:Lcom/google/a/b/c;

.field public static final enum d:Lcom/google/a/b/c;

.field private static final synthetic e:[Lcom/google/a/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/a/b/c;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lcom/google/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/a/b/c;->a:Lcom/google/a/b/c;

    new-instance v0, Lcom/google/a/b/c;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lcom/google/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/a/b/c;->b:Lcom/google/a/b/c;

    new-instance v0, Lcom/google/a/b/c;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/google/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/a/b/c;->c:Lcom/google/a/b/c;

    new-instance v0, Lcom/google/a/b/c;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/google/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/a/b/c;->d:Lcom/google/a/b/c;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/a/b/c;

    sget-object v1, Lcom/google/a/b/c;->a:Lcom/google/a/b/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/a/b/c;->b:Lcom/google/a/b/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/a/b/c;->c:Lcom/google/a/b/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/a/b/c;->d:Lcom/google/a/b/c;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/a/b/c;->e:[Lcom/google/a/b/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()[Lcom/google/a/b/c;
    .locals 1

    sget-object v0, Lcom/google/a/b/c;->e:[Lcom/google/a/b/c;

    invoke-virtual {v0}, [Lcom/google/a/b/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/b/c;

    return-object v0
.end method
