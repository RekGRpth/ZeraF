.class public Lcom/mediatek/exchange/smartpush/SmartPushService;
.super Landroid/app/Service;
.source "SmartPushService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
    }
.end annotation


# static fields
.field private static final DAY:I = 0x5265c00

.field private static final HOUR:I = 0x36ee80

.field protected static INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService; = null

.field private static final MINUTE:I = 0xea60

.field private static final SECOND:I = 0x3e8

.field public static final SYNC_FREQUENCY_HIGH:I = 0x2

.field public static final SYNC_FREQUENCY_LOW:I = 0x0

.field public static final SYNC_FREQUENCY_MEDIUM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SmartPushService"

.field private static final WEEK:I = 0x240c8400

.field private static final WHERE_PROTOCOL_EAS:Ljava/lang/String; = "protocol=\"eas\""

.field private static sServiceThread:Ljava/lang/Thread;

.field private static volatile sStartingUp:Z

.field private static volatile sStop:Z

.field private static final sSyncLock:Ljava/lang/Object;

.field private static sTodayStartTime:J


# instance fields
.field private mAccountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mKicked:Z

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    sput-boolean v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    sput-boolean v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sSyncLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mPendingIntent:Landroid/app/PendingIntent;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    return-void
.end method

.method static synthetic access$1100()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    return v0
.end method

.method static synthetic access$1200()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sSyncLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1300()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    return v0
.end method

.method static synthetic access$1302(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    return p0
.end method

.method static synthetic access$1400()Ljava/lang/Thread;
    .locals 1

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$300()J
    .locals 2

    sget-wide v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    return-wide v0
.end method

.method private acquireWakeLock()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "SMARTPUSH_SERVICE"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v1, "SmartPushService"

    const-string v2, "+SMARTPUSH_SERVICE WAKE LOCK ACQUIRED"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static alarmSmartPushService(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    if-eqz v0, :cond_0

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    const-string v1, "SmartPushService"

    const-string v2, "Alarm received: Kick"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v0

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    const-string v1, "SmartPushService"

    const-string v2, "Alarm received: start smartpush service"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->startSmartPushService(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private calculate()V
    .locals 7

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "SmartPushService"

    const-string v4, "startCalculate..."

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->access$000()Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    invoke-static {v3, p0, v4}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->access$100(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;Landroid/content/Context;Ljava/util/HashMap;)V

    const-string v3, "SmartPushService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Calculate end!!! cost: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->getPreferences(Landroid/content/Context;)Lcom/mediatek/exchange/smartpush/SmartPushPreferences;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->settLastCalculateTime(J)V

    return-void
.end method

.method private changeSyncFrequency(IJ)V
    .locals 11
    .param p1    # I
    .param p2    # J

    const/4 v6, -0x2

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "syncInterval"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0, p2, p3}, Lcom/android/emailcommon/provider/Account;->getInboxId(Landroid/content/Context;J)J

    move-result-wide v3

    invoke-static {p0, v3, v4}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v2

    sget-object v7, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v7, 0x42

    invoke-static {p0, p2, p3, v7}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v3

    sget-object v7, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v7, 0x41

    invoke-static {p0, p2, p3, v7}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v3

    sget-object v7, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "com.android.email.provider"

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    const-string v7, "SmartPushService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "changeSyncFrequency from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v2, Lcom/android/emailcommon/provider/Mailbox;->mSyncInterval:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Lcom/android/emailcommon/provider/Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, v2, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") of account "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, v2, Lcom/android/emailcommon/provider/Mailbox;->mAccountKey:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const/4 v6, -0x2

    goto/16 :goto_0

    :pswitch_1
    const/16 v6, 0x3c

    goto/16 :goto_0

    :pswitch_2
    const/4 v6, -0x1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v7, "SmartPushService"

    const-string v8, "RemoteException when updating mailboxes sync interval"

    invoke-static {v7, v8}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v7, "SmartPushService"

    const-string v8, "OperationApplicationException when updating mailboxes sync interval"

    invoke-static {v7, v8}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private checkNextCalculateWait()J
    .locals 10

    const-wide/32 v8, 0x5265c00

    invoke-static {p0}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->getPreferences(Landroid/content/Context;)Lcom/mediatek/exchange/smartpush/SmartPushPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->getLastCalculateTime()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v3, v5, v0

    const-string v5, "SmartPushService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "since the last calculate time = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v5, v3, v8

    if-ltz v5, :cond_0

    const-wide/16 v5, 0x0

    :goto_0
    return-wide v5

    :cond_0
    sub-long v5, v8, v3

    goto :goto_0
.end method

.method private clearAlarm()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const-string v1, "SmartPushService"

    const-string v2, "-Alarm cleared"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private deleteStaleData()V
    .locals 9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x48190801

    sub-long v1, v3, v5

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/provider/SmartPush;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "timestamp < ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const-string v3, "SmartPushService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows stale habit data were deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static kick(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    if-eqz v0, :cond_1

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    const-string v1, "SmartPushService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Kick: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v0

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v1, :cond_0

    const-string v1, "SmartPushService"

    const-string v2, "Start smartpushservice when kick"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-static {v1}, Lcom/mediatek/exchange/smartpush/SmartPushService;->startSmartPushService(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private makeAdjustments()J
    .locals 23

    sget-boolean v17, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    if-eqz v17, :cond_0

    const-wide/16 v8, 0x0

    :goto_0
    return-wide v8

    :cond_0
    const-string v17, "SmartPushService"

    const-string v18, "makeAdjustments..."

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    sget-wide v19, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    sub-long v17, v17, v19

    const-wide/32 v19, 0x6ddd00

    div-long v17, v17, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->intValue()I

    move-result v16

    const-string v17, "SmartPushService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "current time scale: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/32 v17, 0x6ddd00

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    sget-wide v21, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    sub-long v19, v19, v21

    const-wide/32 v21, 0x6ddd00

    rem-long v19, v19, v21

    sub-long v12, v17, v19

    const-string v17, "SmartPushService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "The time remaining to the next time scale: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-wide v8, 0x7fffffffffffffffL

    sget-object v17, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v17, :cond_1

    const-wide/32 v8, 0xea60

    goto :goto_0

    :cond_1
    sget-object v17, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    move-object/from16 v19, v0

    monitor-enter v19

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    move-wide v10, v12

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/SmartPush;->isSmartPushAccount(Landroid/content/Context;J)Z

    move-result v17

    if-nez v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v17

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v17

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->access$000()Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->getResult(J)[I

    move-result-object v15

    if-nez v15, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->getPreferences(Landroid/content/Context;)Lcom/mediatek/exchange/smartpush/SmartPushPreferences;

    move-result-object v14

    invoke-virtual {v14}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->removeLastCalculateTime()V

    const-wide/16 v17, 0x0

    monitor-exit v19

    move-wide/from16 v8, v17

    goto/16 :goto_0

    :cond_3
    aget v17, v15, v16

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/exchange/smartpush/SmartPushService;->changeSyncFrequency(IJ)V

    move/from16 v6, v16

    :goto_2
    const/16 v17, 0xb

    move/from16 v0, v17

    if-ge v6, v0, :cond_4

    aget v17, v15, v6

    add-int/lit8 v6, v6, 0x1

    aget v18, v15, v6

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    const-wide/32 v17, 0x6ddd00

    add-long v10, v10, v17

    goto :goto_2

    :cond_4
    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    goto :goto_1

    :cond_5
    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v17, "SmartPushService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "The time remaining to the next adjustments: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private releaseWakeLock()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-string v0, "SmartPushService"

    const-string v1, "-SMARTPUSH_SERVICE WAKE LOCK RELEASED"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static runAsleep(J)V
    .locals 1
    .param p0    # J

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    if-eqz v0, :cond_0

    invoke-direct {v0, p0, p1}, Lcom/mediatek/exchange/smartpush/SmartPushService;->setAlarm(J)V

    invoke-direct {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->releaseWakeLock()V

    :cond_0
    return-void
.end method

.method public static runAwake()V
    .locals 1

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    if-eqz v0, :cond_0

    invoke-direct {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->acquireWakeLock()V

    invoke-direct {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->clearAlarm()V

    :cond_0
    return-void
.end method

.method private setAlarm(J)V
    .locals 6
    .param p1    # J

    const/4 v5, 0x0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/exchange/smartpush/SmartPushAlarmReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mPendingIntent:Landroid/app/PendingIntent;

    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    iget-object v4, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v5, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const-string v2, "SmartPushService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+Alarm set for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private shouldRunSmartPushService()J
    .locals 29

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-wide/32 v4, 0x5265c00

    div-long v18, v14, v4

    const-wide/32 v4, 0x5265c00

    mul-long v4, v4, v18

    sput-wide v4, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    const-string v4, "SmartPushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Today start time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-wide v6, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    const-wide/32 v20, 0x5265c00

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const-string v7, "syncInterval =?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, -0x5

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-eqz v13, :cond_4

    :goto_0
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    sget-object v5, Lcom/android/emailcommon/provider/SmartPush;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "timestamp"

    aput-object v7, v6, v4

    const-string v7, "accountKey=? AND eventType !=?"

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x1

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v10}, Lcom/android/emailcommon/utility/Utility;->getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v22

    if-eqz v22, :cond_2

    sget-wide v4, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v23, v4, v6

    const-string v4, "SmartPushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v23

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms habit data"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/32 v4, 0xa4cb800

    cmp-long v4, v23, v4

    if-ltz v4, :cond_0

    const-wide/32 v4, 0x5265c00

    div-long v16, v23, v4

    const-string v4, "SmartPushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " days habit data"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mAccountMap:Ljava/util/HashMap;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_0
    sget-wide v6, Lcom/mediatek/exchange/smartpush/SmartPushService;->sTodayStartTime:J

    const-wide/32 v8, 0x5265c00

    const-wide/16 v27, 0x2

    const-wide/16 v4, 0x0

    cmp-long v4, v23, v4

    if-gez v4, :cond_1

    const-wide/16 v4, -0x1

    :goto_1
    sub-long v4, v27, v4

    mul-long/2addr v4, v8

    add-long/2addr v4, v6

    sub-long v25, v4, v14

    move-wide/from16 v0, v20

    move-wide/from16 v2, v25

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v20

    goto/16 :goto_0

    :cond_1
    const-wide/32 v4, 0x5265c00

    div-long v4, v23, v4

    goto :goto_1

    :cond_2
    const-string v4, "SmartPushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No habit data record for account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_4
    const-string v4, "SmartPushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The habit data will be enough after "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-wide v20
.end method

.method private shutdown()V
    .locals 3

    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    const-string v0, "SmartPushService"

    const-string v2, "Goodbye"

    invoke-static {v0, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static startSmartPushService(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/exchange/smartpush/SmartPushService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    new-instance v0, Lcom/mediatek/exchange/smartpush/SmartPushService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$1;-><init>(Lcom/mediatek/exchange/smartpush/SmartPushService;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SmartPushService"

    const-string v1, "SmartPushService onDestroy"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/exchange/smartpush/SmartPushService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$2;-><init>(Lcom/mediatek/exchange/smartpush/SmartPushService;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    if-nez v1, :cond_3

    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    if-nez v1, :cond_3

    sput-boolean v6, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    :try_start_0
    sget-object v2, Lcom/mediatek/exchange/smartpush/SmartPushService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget-object v1, Lcom/android/emailcommon/provider/HostAuth;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "protocol=\"eas\""

    const/4 v4, 0x0

    invoke-static {p0, v1, v3, v4}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Ljava/lang/Thread;

    const-string v3, "SmartPushService"

    invoke-direct {v1, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    sput-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    sput-object p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->INSTANCE:Lcom/mediatek/exchange/smartpush/SmartPushService;

    invoke-static {p0}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->getPreferences(Landroid/content/Context;)Lcom/mediatek/exchange/smartpush/SmartPushPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/exchange/smartpush/SmartPushPreferences;->removeLastCalculateTime()V

    const-string v1, "SmartPushService"

    const-string v3, "SmartPushService thread start to run"

    invoke-static {v1, v3}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_1
    sget-object v1, Lcom/mediatek/exchange/smartpush/SmartPushService;->sServiceThread:Ljava/lang/Thread;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sput-boolean v5, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    :cond_3
    return v6

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    sput-boolean v5, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStartingUp:Z

    throw v1
.end method

.method public run()V
    .locals 15

    const-wide/16 v13, 0x2710

    const-wide/16 v11, 0x0

    const/4 v9, 0x0

    sput-boolean v9, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    :goto_0
    :try_start_0
    sget-boolean v9, Lcom/mediatek/exchange/smartpush/SmartPushService;->sStop:Z

    if-nez v9, :cond_6

    const-string v9, "SmartPushService"

    const-string v10, "SmartPushService loop one time"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService;->runAwake()V

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->deleteStaleData()V

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->shouldRunSmartPushService()J

    move-result-wide v5

    cmp-long v9, v5, v11

    if-lez v9, :cond_0

    const-string v9, "SmartPushService"

    const-string v10, "No eligible smart push account found"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    add-long v9, v5, v13

    invoke-static {v9, v10}, Lcom/mediatek/exchange/smartpush/SmartPushService;->runAsleep(J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-wide/16 v9, 0x1388

    add-long/2addr v9, v5

    :try_start_2
    invoke-virtual {p0, v9, v10}, Ljava/lang/Object;->wait(J)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v9
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    :try_start_4
    const-string v9, "SmartPushService"

    const-string v10, "SmartPushService interrupted"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    const-string v9, "SmartPushService"

    const-string v10, "RuntimeException in SmartPushService"

    invoke-static {v9, v10, v0}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v9

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->shutdown()V

    throw v9

    :cond_0
    :try_start_6
    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->checkNextCalculateWait()J

    move-result-wide v3

    const-wide/32 v9, 0x927c0

    cmp-long v9, v3, v9

    if-gez v9, :cond_1

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->calculate()V

    const-wide/32 v3, 0x5265c00

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->makeAdjustments()J
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-wide v1

    cmp-long v9, v1, v3

    if-gez v9, :cond_7

    move-wide v7, v1

    :goto_1
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    :try_start_8
    iget-boolean v9, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    if-nez v9, :cond_4

    cmp-long v9, v7, v11

    if-gez v9, :cond_2

    const-wide/16 v7, 0x3e8

    :cond_2
    cmp-long v9, v7, v13

    if-lez v9, :cond_3

    const-wide/16 v9, 0xbb8

    add-long/2addr v9, v7

    invoke-static {v9, v10}, Lcom/mediatek/exchange/smartpush/SmartPushService;->runAsleep(J)V

    :cond_3
    invoke-virtual {p0, v7, v8}, Ljava/lang/Object;->wait(J)V

    :cond_4
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    monitor-enter p0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    iget-boolean v9, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    if-eqz v9, :cond_5

    const-string v9, "SmartPushService"

    const-string v10, "Wait deferred due to kick"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    :cond_5
    monitor-exit p0

    goto/16 :goto_0

    :catchall_2
    move-exception v9

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v9
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :catch_2
    move-exception v0

    :try_start_c
    const-string v9, "SmartPushService"

    const-string v10, "SmartPushService Exception occured"

    invoke-static {v9, v10, v0}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/mediatek/exchange/smartpush/SmartPushService;

    invoke-direct {v9, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v9}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_6
    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService;->shutdown()V

    return-void

    :cond_7
    move-wide v7, v3

    goto :goto_1

    :catchall_3
    move-exception v9

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v9
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    :catch_3
    move-exception v0

    :try_start_f
    const-string v9, "SmartPushService"

    const-string v10, "SmartPushService interrupted"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    :try_start_10
    monitor-enter p0
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :try_start_11
    iget-boolean v9, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    if-eqz v9, :cond_8

    const-string v9, "SmartPushService"

    const-string v10, "Wait deferred due to kick"

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    :cond_8
    monitor-exit p0

    goto/16 :goto_0

    :catchall_4
    move-exception v9

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    :try_start_12
    throw v9

    :catchall_5
    move-exception v9

    monitor-enter p0
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :try_start_13
    iget-boolean v10, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    if-eqz v10, :cond_9

    const-string v10, "SmartPushService"

    const-string v11, "Wait deferred due to kick"

    invoke-static {v10, v11}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/mediatek/exchange/smartpush/SmartPushService;->mKicked:Z

    :cond_9
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    :try_start_14
    throw v9
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :catchall_6
    move-exception v9

    :try_start_15
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    :try_start_16
    throw v9
    :try_end_16
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_1
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_2
    .catchall {:try_start_16 .. :try_end_16} :catchall_1
.end method
