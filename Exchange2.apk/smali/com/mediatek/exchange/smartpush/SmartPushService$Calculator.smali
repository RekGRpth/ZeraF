.class Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
.super Ljava/lang/Object;
.source "SmartPushService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/exchange/smartpush/SmartPushService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Calculator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;,
        Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;
    }
.end annotation


# static fields
.field private static sCalculator:Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;


# instance fields
.field habitDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    invoke-direct {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;-><init>()V

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->sCalculator:Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->habitDataList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000()Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
    .locals 1

    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->getCalculator()Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 0
    .param p0    # Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->startCalculate(Landroid/content/Context;Ljava/util/HashMap;)V

    return-void
.end method

.method private static getCalculator()Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
    .locals 1

    sget-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->sCalculator:Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;

    return-object v0
.end method

.method private startCalculate(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->habitDataList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v7, "SmartPushService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "accountId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,dayCount:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;

    invoke-direct {v5, p1, v0, v1, v2}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;-><init>(Landroid/content/Context;JI)V

    iget-object v7, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->habitDataList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->access$200(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;)V

    goto :goto_0
.end method


# virtual methods
.method public getResult(J)[I
    .locals 4
    .param p1    # J

    iget-object v2, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;->habitDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;

    iget-wide v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mAccountId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mResults:[I

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
