.class Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;
.super Ljava/lang/Object;
.source "SmartPushService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HabitData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;
    }
.end annotation


# static fields
.field private static final EVENTTYPE_COLUMN:I = 0x0

.field private static final HABIT_PROJECTION:[Ljava/lang/String;

.field private static final HABIT_SELECTION:Ljava/lang/String; = "accountKey =?"

.field private static final TIMESTAMP_COLUMN:I = 0x1

.field private static final VALUE_COLUMN:I = 0x2


# instance fields
.field mAccountId:J

.field mContext:Landroid/content/Context;

.field mResults:[I

.field mTableData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;",
            ">;>;"
        }
    .end annotation
.end field

.field mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "eventType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->HABIT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;JI)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTableData:Ljava/util/ArrayList;

    const/16 v1, 0xc

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mResults:[I

    iput-object p1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mContext:Landroid/content/Context;

    iput-wide p2, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mAccountId:J

    new-array v1, p4, [Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    iput-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p4, :cond_0

    iget-object v1, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTableData:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;)V
    .locals 0
    .param p0    # Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->startCalculate()V

    return-void
.end method

.method private startCalculate()V
    .locals 32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/SmartPush;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->HABIT_PROJECTION:[Ljava/lang/String;

    const-string v5, "accountKey =?"

    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mAccountId:J

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_3

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService;->access$300()J

    move-result-wide v2

    sub-long v24, v2, v28

    const-wide/16 v2, 0x0

    cmp-long v2, v24, v2

    if-gtz v2, :cond_1

    const/4 v12, -0x1

    :goto_1
    if-ltz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    array-length v2, v2

    if-ge v12, v2, :cond_0

    invoke-static {}, Lcom/mediatek/exchange/smartpush/SmartPushService;->access$300()J

    move-result-wide v2

    add-int/lit8 v4, v12, 0x1

    const v7, 0x5265c00

    mul-int/2addr v4, v7

    int-to-long v7, v4

    sub-long v26, v2, v7

    sub-long v5, v28, v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTableData:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/ArrayList;

    move-object/from16 v30, v0

    new-instance v2, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;

    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v3, 0x2

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;-><init>(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;IJJ)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_1
    const-wide/32 v2, 0x5265c00

    :try_start_1
    div-long v2, v24, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v12

    goto :goto_1

    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    const/16 v2, 0xc

    new-array v14, v2, [F

    const/4 v15, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    array-length v2, v2

    if-ge v15, v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    new-instance v3, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;-><init>(Lcom/mediatek/exchange/smartpush/SmartPushService$1;)V

    aput-object v3, v2, v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    aget-object v3, v2, v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTableData:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-static {v3, v2}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->access$500(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;Ljava/util/ArrayList;)V

    const-string v2, "SmartPushService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Table["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] startCalculate..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    aget-object v2, v2, v15

    invoke-static {v2}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->access$600(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;)V

    const/16 v17, 0x0

    :goto_3
    const/16 v2, 0xc

    move/from16 v0, v17

    if-ge v0, v2, :cond_4

    aget v2, v14, v17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mTables:[Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    aget-object v3, v3, v15

    invoke-static {v3}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->access$700(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;)[F

    move-result-object v3

    aget v3, v3, v17

    add-float/2addr v2, v3

    aput v2, v14, v17

    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    :cond_5
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "["

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v9, v14

    array-length v0, v9

    move/from16 v19, v0

    const/16 v16, 0x0

    :goto_4
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_6

    aget v13, v9, v16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    :cond_6
    const-string v2, "]"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "SmartPushService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chances: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, [F

    invoke-static/range {v23 .. v23}, Ljava/util/Arrays;->sort([F)V

    const/16 v21, 0x0

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    const/16 v20, 0x0

    :goto_5
    const/4 v2, 0x3

    move/from16 v0, v20

    if-ge v0, v2, :cond_a

    :goto_6
    add-int/lit8 v2, v20, 0x1

    mul-int/lit8 v2, v2, 0x4

    move/from16 v0, v21

    if-ge v0, v2, :cond_9

    const/16 v18, 0x0

    :goto_7
    const/16 v2, 0xc

    move/from16 v0, v18

    if-ge v0, v2, :cond_7

    aget v2, v14, v18

    aget v3, v23, v21

    cmpl-float v2, v2, v3

    if-nez v2, :cond_8

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData;->mResults:[I

    aput v20, v2, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v2, "SmartPushService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "final mResults["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    add-int/lit8 v21, v21, 0x1

    goto :goto_6

    :cond_8
    add-int/lit8 v18, v18, 0x1

    goto :goto_7

    :cond_9
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    :cond_a
    return-void
.end method
