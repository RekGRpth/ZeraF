.class public Lcom/android/exchange/CommandStatusException;
.super Lcom/android/exchange/EasException;
.source "CommandStatusException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/CommandStatusException$CommandStatus;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final mItemId:Ljava/lang/String;

.field public final mStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/android/exchange/EasException;-><init>()V

    iput p1, p0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/CommandStatusException;->mItemId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/exchange/EasException;-><init>()V

    iput p1, p0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    iput-object p2, p0, Lcom/android/exchange/CommandStatusException;->mItemId:Ljava/lang/String;

    return-void
.end method
