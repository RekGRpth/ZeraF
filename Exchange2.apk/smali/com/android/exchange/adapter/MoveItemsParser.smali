.class public Lcom/android/exchange/adapter/MoveItemsParser;
.super Lcom/android/exchange/adapter/Parser;
.source "MoveItemsParser.java"


# static fields
.field private static final STATUS_ALREADY_EXISTS:I = 0x6

.field public static final STATUS_CODE_RETRY:I = 0x3

.field public static final STATUS_CODE_REVERT:I = 0x2

.field public static final STATUS_CODE_SUCCESS:I = 0x1

.field private static final STATUS_INTERNAL_ERROR:I = 0x5

.field private static final STATUS_LOCKED:I = 0x7

.field private static final STATUS_NO_DESTINATION_FOLDER:I = 0x2

.field private static final STATUS_NO_SOURCE_FOLDER:I = 0x1

.field private static final STATUS_SOURCE_DESTINATION_SAME:I = 0x4

.field private static final STATUS_SUCCESS:I = 0x3


# instance fields
.field private mNewServerId:Ljava/lang/String;

.field private final mService:Lcom/android/exchange/EasSyncService;

.field private mStatusCode:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    iput-object p2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    return-void
.end method


# virtual methods
.method public getNewServerId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    return v0
.end method

.method public parse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v1

    const/16 v2, 0x145

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v2, 0x14a

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->parseResponse()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->skipTag()V

    goto :goto_0

    :cond_2
    return v0
.end method

.method public parseResponse()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    :cond_0
    :goto_0
    const/16 v1, 0x14a

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/Parser;->nextTag(I)I

    move-result v1

    if-eq v1, v6, :cond_3

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v2, 0x14b

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->getValueInt()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    :goto_1
    if-eq v0, v6, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v2, v5, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in MoveItems: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iput v5, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    goto :goto_1

    :pswitch_2
    iput v6, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    const/16 v2, 0x14c

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v2, v5, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Moved message id is now: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->skipTag()V

    goto :goto_0

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
