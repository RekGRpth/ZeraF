.class public final Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils;
.super Ljava/lang/Object;
.source "BinaryDictIOUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;
    }
.end annotation


# static fields
.field private static final DBG:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteWord(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;Ljava/lang/String;)V
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p0, v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readHeader(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;)Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils;->getTerminalPosition(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;Ljava/lang/String;)I

    move-result v3

    const/16 v4, -0x63

    if-ne v3, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0, v3}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->readUnsignedByte()I

    move-result v0

    xor-int/lit8 v2, v0, 0x10

    invoke-interface {p0, v3}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    int-to-byte v4, v2

    invoke-interface {p0, v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->put(B)V

    goto :goto_0
.end method

.method public static getTerminalPosition(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;Ljava/lang/String;)I
    .locals 19
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;
        }
    .end annotation

    if-nez p1, :cond_1

    const/16 v4, -0x63

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-interface/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v17

    if-eqz v17, :cond_2

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readHeader(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;)Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;

    move-result-object v10

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->codePointCount(II)I

    move-result v15

    const/4 v6, 0x0

    :goto_1
    const/16 v17, 0x30

    move/from16 v0, v17

    if-ge v6, v0, :cond_f

    move/from16 v0, v16

    if-lt v0, v15, :cond_4

    const/16 v4, -0x63

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    :cond_4
    invoke-interface/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v17

    iget v0, v10, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mHeaderSize:I

    move/from16 v18, v0

    sub-int v9, v17, v18

    invoke-static/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readCharGroupCount(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;)I

    move-result v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(I)I

    move-result v17

    add-int v9, v9, v17

    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v3, :cond_b

    invoke-interface/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v4

    invoke-interface/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v17

    iget-object v0, v10, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mFormatOptions:Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readCharGroup(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;ILcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)Lcom/android/inputmethod/latin/makedict/CharGroupInfo;

    move-result-object v5

    iget v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFlags:I

    move/from16 v17, v0

    iget-object v0, v10, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mFormatOptions:Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->isMovedGroup(ILcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)Z

    move-result v17

    if-eqz v17, :cond_5

    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_5
    const/4 v14, 0x1

    const/4 v13, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v12

    :goto_4
    iget-object v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v13, v0, :cond_7

    add-int v17, v16, v13

    move/from16 v0, v17

    if-ge v0, v15, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->codePointAt(I)I

    move-result v17

    iget-object v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    move-object/from16 v18, v0

    aget v18, v18, v13

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_8

    :cond_6
    const/4 v14, 0x0

    :cond_7
    if-eqz v14, :cond_c

    iget-object v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    add-int v17, v17, v16

    move/from16 v0, v17

    if-ne v0, v15, :cond_9

    iget v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFrequency:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    const/16 v4, -0x63

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v13, v13, 0x1

    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v12, v1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v12

    goto :goto_4

    :cond_9
    iget-object v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    add-int v16, v16, v17

    iget v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mChildrenAddress:I

    move/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    const/16 v4, -0x63

    goto/16 :goto_0

    :cond_a
    const/4 v8, 0x1

    iget v0, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mChildrenAddress:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    :cond_b
    if-eqz v8, :cond_d

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    :cond_c
    iget v9, v5, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mEndAddress:I

    goto/16 :goto_3

    :cond_d
    iget-object v0, v10, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mFormatOptions:Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;->mSupportsDynamicUpdate:Z

    move/from16 v17, v0

    if-nez v17, :cond_e

    const/16 v4, -0x63

    goto/16 :goto_0

    :cond_e
    invoke-interface/range {p0 .. p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->readUnsignedInt24()I

    move-result v7

    if-nez v7, :cond_3

    const/16 v4, -0x63

    goto/16 :goto_0

    :cond_f
    const/16 v4, -0x63

    goto/16 :goto_0
.end method

.method private static putSInt24(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;I)V
    .locals 3
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gez p1, :cond_0

    const/16 v1, 0x80

    :goto_0
    shr-int/lit8 v2, v0, 0x10

    or-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-interface {p0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->put(B)V

    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-interface {p0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->put(B)V

    and-int/lit16 v1, v0, 0xff

    int-to-byte v1, v1

    invoke-interface {p0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->put(B)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static readUnigramsAndBigramsBinary(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 7
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/PendingAttribute;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/inputmethod/latin/makedict/UnsupportedFormatException;
        }
    .end annotation

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readHeader(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;)Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;

    move-result-object v6

    iget v1, v6, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mHeaderSize:I

    iget-object v5, v6, Lcom/android/inputmethod/latin/makedict/FormatSpec$FileHeader;->mFormatOptions:Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils;->readUnigramsAndBigramsBinaryInner(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;ILjava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)V

    return-void
.end method

.method private static readUnigramsAndBigramsBinaryInner(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;ILjava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)V
    .locals 15
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .param p1    # I
    .param p5    # Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/PendingAttribute;",
            ">;>;",
            "Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;",
            ")V"
        }
    .end annotation

    const/16 v12, 0x31

    new-array v10, v12, [I

    new-instance v11, Ljava/util/Stack;

    invoke-direct {v11}, Ljava/util/Stack;-><init>()V

    const/4 v4, 0x0

    new-instance v7, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;

    const/4 v12, 0x0

    move/from16 v0, p1

    invoke-direct {v7, v0, v12}, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;-><init>(II)V

    invoke-virtual {v11, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    invoke-virtual {v11}, Ljava/util/Stack;->empty()Z

    move-result v12

    if-nez v12, :cond_a

    invoke-virtual {v11}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;

    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v12

    iget v13, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    if-eq v12, v13, :cond_1

    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    invoke-interface {p0, v12}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    :cond_1
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mLength:I

    if-eq v4, v12, :cond_2

    iget v4, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mLength:I

    :cond_2
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_3

    invoke-static {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readCharGroupCount(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;)I

    move-result v12

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    iget v13, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    invoke-static {v13}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->getGroupCountSize(I)I

    move-result v13

    add-int/2addr v12, v13

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    const/4 v12, 0x0

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mPosition:I

    :cond_3
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    if-nez v12, :cond_4

    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    sub-int v12, v12, p1

    move-object/from16 v0, p5

    invoke-static {p0, v12, v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->readCharGroup(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;ILcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)Lcom/android/inputmethod/latin/makedict/CharGroupInfo;

    move-result-object v6

    const/4 v3, 0x0

    :goto_1
    iget-object v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    array-length v12, v12

    if-ge v3, v12, :cond_5

    add-int/lit8 v5, v4, 0x1

    iget-object v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    aget v12, v12, v3

    aput v12, v10, v4

    add-int/lit8 v3, v3, 0x1

    move v4, v5

    goto :goto_1

    :cond_5
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mPosition:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mPosition:I

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFlags:I

    move-object/from16 v0, p5

    invoke-static {v12, v0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->isMovedGroup(ILcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)Z

    move-result v8

    if-nez v8, :cond_6

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFrequency:I

    const/4 v13, -0x1

    if-eq v12, v13, :cond_6

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mOriginalAddress:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct {v13, v10, v14, v4}, Ljava/lang/String;-><init>([III)V

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mOriginalAddress:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iget v13, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFrequency:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-interface {v0, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mBigrams:Ljava/util/ArrayList;

    if-eqz v12, :cond_6

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mOriginalAddress:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iget-object v13, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mBigrams:Ljava/util/ArrayList;

    move-object/from16 v0, p4

    invoke-interface {v0, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mPosition:I

    iget v13, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    if-ne v12, v13, :cond_9

    move-object/from16 v0, p5

    iget-boolean v12, v0, Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;->mSupportsDynamicUpdate:Z

    if-eqz v12, :cond_8

    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->readUnsignedInt24()I

    move-result v2

    if-eqz v2, :cond_7

    const/4 v12, -0x1

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    iput v2, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    :goto_2
    if-nez v8, :cond_0

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mChildrenAddress:I

    invoke-static {v12}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput;->hasChildrenAddress(I)Z

    move-result v12

    if-eqz v12, :cond_0

    new-instance v1, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;

    iget v12, v6, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mChildrenAddress:I

    add-int v12, v12, p1

    invoke-direct {v1, v12, v4}, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;-><init>(II)V

    invoke-virtual {v11, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_2

    :cond_8
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_2

    :cond_9
    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v12

    iput v12, v9, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    goto :goto_2

    :cond_a
    return-void
.end method

.method public static updateParentAddress(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;IILcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;)V
    .locals 5
    .param p0    # Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;

    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position()I

    move-result v1

    invoke-interface {p0, p1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    iget-boolean v3, p3, Lcom/android/inputmethod/latin/makedict/FormatSpec$FormatOptions;->mSupportsDynamicUpdate:Z

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "this file format does not support parent addresses"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-interface {p0}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->readUnsignedByte()I

    move-result v0

    sub-int v2, p2, p1

    invoke-static {p0, v2}, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils;->putSInt24(Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;I)V

    invoke-interface {p0, v1}, Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;->position(I)V

    return-void
.end method
