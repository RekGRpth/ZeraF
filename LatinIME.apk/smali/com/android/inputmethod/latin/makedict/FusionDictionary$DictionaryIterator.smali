.class public final Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;
.super Ljava/lang/Object;
.source "FusionDictionary.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/makedict/FusionDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DictionaryIterator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/android/inputmethod/latin/makedict/Word;",
        ">;"
    }
.end annotation


# instance fields
.field final mCurrentString:Ljava/lang/StringBuilder;

.field final mPositions:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    new-instance v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    iget-object v2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    iget-object v2, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->pos:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public next()Lcom/android/inputmethod/latin/makedict/Word;
    .locals 13

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->length:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_0
    :goto_0
    iget-object v0, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->pos:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->pos:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;

    iget-object v0, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v0, v0

    iput v0, v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->length:I

    iget-object v7, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v12, v7

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v12, :cond_1

    aget v10, v7, v11

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    if-eqz v0, :cond_2

    new-instance v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    iget-object v0, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iget-object v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;->mData:Ljava/util/ArrayList;

    invoke-direct {v9, v0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;-><init>(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v0, v9}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    :cond_2
    iget v0, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    if-ltz v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/latin/makedict/Word;

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    iget-object v3, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    iget-object v4, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    iget-boolean v5, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsNotAWord:Z

    iget-boolean v6, v8, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsBlacklistEntry:Z

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/makedict/Word;-><init>(Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;ZZ)V

    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mCurrentString:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    iget-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->mPositions:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;

    iget v0, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator$Position;->length:I

    sub-int v0, v2, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$DictionaryIterator;->next()Lcom/android/inputmethod/latin/makedict/Word;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
