.class final Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;
.super Ljava/lang/Object;
.source "BinaryDictIOUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Position"
.end annotation


# static fields
.field public static final NOT_READ_GROUPCOUNT:I = -0x1


# instance fields
.field public mAddress:I

.field public mLength:I

.field public mNumOfCharGroup:I

.field public mPosition:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mAddress:I

    iput p2, p0, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mLength:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/inputmethod/latin/makedict/BinaryDictIOUtils$Position;->mNumOfCharGroup:I

    return-void
.end method
