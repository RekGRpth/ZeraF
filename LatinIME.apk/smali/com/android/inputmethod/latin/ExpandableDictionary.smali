.class public Lcom/android/inputmethod/latin/ExpandableDictionary;
.super Lcom/android/inputmethod/latin/Dictionary;
.source "ExpandableDictionary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/ExpandableDictionary$LoadDictionaryTask;,
        Lcom/android/inputmethod/latin/ExpandableDictionary$NextHistoryWord;,
        Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;,
        Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;,
        Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;,
        Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    }
.end annotation


# static fields
.field private static final BASE_CHARS:[C

.field protected static final BIGRAM_MAX_FREQUENCY:I = 0xff


# instance fields
.field private mCodes:[[I

.field private mContext:Landroid/content/Context;

.field private mInputLength:I

.field private final mLookedUpString:[C

.field private mMaxDepth:I

.field private mRequiresReload:Z

.field private mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

.field private mUpdatingDictionary:Z

.field private mUpdatingLock:Ljava/lang/Object;

.field private mWordBuilder:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x500

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->BASE_CHARS:[C

    return-void

    :array_0
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x3s
        0x4s
        0x5s
        0x6s
        0x7s
        0x8s
        0x9s
        0xas
        0xbs
        0xcs
        0xds
        0xes
        0xfs
        0x10s
        0x11s
        0x12s
        0x13s
        0x14s
        0x15s
        0x16s
        0x17s
        0x18s
        0x19s
        0x1as
        0x1bs
        0x1cs
        0x1ds
        0x1es
        0x1fs
        0x20s
        0x21s
        0x22s
        0x23s
        0x24s
        0x25s
        0x26s
        0x27s
        0x28s
        0x29s
        0x2as
        0x2bs
        0x2cs
        0x2ds
        0x2es
        0x2fs
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x3as
        0x3bs
        0x3cs
        0x3ds
        0x3es
        0x3fs
        0x40s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
        0x5bs
        0x5cs
        0x5ds
        0x5es
        0x5fs
        0x60s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
        0x67s
        0x68s
        0x69s
        0x6as
        0x6bs
        0x6cs
        0x6ds
        0x6es
        0x6fs
        0x70s
        0x71s
        0x72s
        0x73s
        0x74s
        0x75s
        0x76s
        0x77s
        0x78s
        0x79s
        0x7as
        0x7bs
        0x7cs
        0x7ds
        0x7es
        0x7fs
        0x80s
        0x81s
        0x82s
        0x83s
        0x84s
        0x85s
        0x86s
        0x87s
        0x88s
        0x89s
        0x8as
        0x8bs
        0x8cs
        0x8ds
        0x8es
        0x8fs
        0x90s
        0x91s
        0x92s
        0x93s
        0x94s
        0x95s
        0x96s
        0x97s
        0x98s
        0x99s
        0x9as
        0x9bs
        0x9cs
        0x9ds
        0x9es
        0x9fs
        0x20s
        0xa1s
        0xa2s
        0xa3s
        0xa4s
        0xa5s
        0xa6s
        0xa7s
        0x20s
        0xa9s
        0x61s
        0xabs
        0xacs
        0xads
        0xaes
        0x20s
        0xb0s
        0xb1s
        0x32s
        0x33s
        0x20s
        0x3bcs
        0xb6s
        0xb7s
        0x20s
        0x31s
        0x6fs
        0xbbs
        0x31s
        0x31s
        0x33s
        0xbfs
        0x41s
        0x41s
        0x41s
        0x41s
        0x41s
        0x41s
        0xc6s
        0x43s
        0x45s
        0x45s
        0x45s
        0x45s
        0x49s
        0x49s
        0x49s
        0x49s
        0xd0s
        0x4es
        0x4fs
        0x4fs
        0x4fs
        0x4fs
        0x4fs
        0xd7s
        0x4fs
        0x55s
        0x55s
        0x55s
        0x55s
        0x59s
        0xdes
        0x73s
        0x61s
        0x61s
        0x61s
        0x61s
        0x61s
        0x61s
        0xe6s
        0x63s
        0x65s
        0x65s
        0x65s
        0x65s
        0x69s
        0x69s
        0x69s
        0x69s
        0xf0s
        0x6es
        0x6fs
        0x6fs
        0x6fs
        0x6fs
        0x6fs
        0xf7s
        0x6fs
        0x75s
        0x75s
        0x75s
        0x75s
        0x79s
        0xfes
        0x79s
        0x41s
        0x61s
        0x41s
        0x61s
        0x41s
        0x61s
        0x43s
        0x63s
        0x43s
        0x63s
        0x43s
        0x63s
        0x43s
        0x63s
        0x44s
        0x64s
        0x110s
        0x111s
        0x45s
        0x65s
        0x45s
        0x65s
        0x45s
        0x65s
        0x45s
        0x65s
        0x45s
        0x65s
        0x47s
        0x67s
        0x47s
        0x67s
        0x47s
        0x67s
        0x47s
        0x67s
        0x48s
        0x68s
        0x126s
        0x127s
        0x49s
        0x69s
        0x49s
        0x69s
        0x49s
        0x69s
        0x49s
        0x69s
        0x49s
        0x131s
        0x49s
        0x69s
        0x4as
        0x6as
        0x4bs
        0x6bs
        0x138s
        0x4cs
        0x6cs
        0x4cs
        0x6cs
        0x4cs
        0x6cs
        0x4cs
        0x6cs
        0x141s
        0x142s
        0x4es
        0x6es
        0x4es
        0x6es
        0x4es
        0x6es
        0x2bcs
        0x14as
        0x14bs
        0x4fs
        0x6fs
        0x4fs
        0x6fs
        0x4fs
        0x6fs
        0x152s
        0x153s
        0x52s
        0x72s
        0x52s
        0x72s
        0x52s
        0x72s
        0x53s
        0x73s
        0x53s
        0x73s
        0x53s
        0x73s
        0x53s
        0x73s
        0x54s
        0x74s
        0x54s
        0x74s
        0x166s
        0x167s
        0x55s
        0x75s
        0x55s
        0x75s
        0x55s
        0x75s
        0x55s
        0x75s
        0x55s
        0x75s
        0x55s
        0x75s
        0x57s
        0x77s
        0x59s
        0x79s
        0x59s
        0x5as
        0x7as
        0x5as
        0x7as
        0x5as
        0x7as
        0x73s
        0x180s
        0x181s
        0x182s
        0x183s
        0x184s
        0x185s
        0x186s
        0x187s
        0x188s
        0x189s
        0x18as
        0x18bs
        0x18cs
        0x18ds
        0x18es
        0x18fs
        0x190s
        0x191s
        0x192s
        0x193s
        0x194s
        0x195s
        0x196s
        0x197s
        0x198s
        0x199s
        0x19as
        0x19bs
        0x19cs
        0x19ds
        0x19es
        0x19fs
        0x4fs
        0x6fs
        0x1a2s
        0x1a3s
        0x1a4s
        0x1a5s
        0x1a6s
        0x1a7s
        0x1a8s
        0x1a9s
        0x1aas
        0x1abs
        0x1acs
        0x1ads
        0x1aes
        0x55s
        0x75s
        0x1b1s
        0x1b2s
        0x1b3s
        0x1b4s
        0x1b5s
        0x1b6s
        0x1b7s
        0x1b8s
        0x1b9s
        0x1bas
        0x1bbs
        0x1bcs
        0x1bds
        0x1bes
        0x1bfs
        0x1c0s
        0x1c1s
        0x1c2s
        0x1c3s
        0x44s
        0x44s
        0x64s
        0x4cs
        0x4cs
        0x6cs
        0x4es
        0x4es
        0x6es
        0x41s
        0x61s
        0x49s
        0x69s
        0x4fs
        0x6fs
        0x55s
        0x75s
        0xdcs
        0xfcs
        0xdcs
        0xfcs
        0xdcs
        0xfcs
        0xdcs
        0xfcs
        0x1dds
        0xc4s
        0xe4s
        0x226s
        0x227s
        0xc6s
        0xe6s
        0x1e4s
        0x1e5s
        0x47s
        0x67s
        0x4bs
        0x6bs
        0x4fs
        0x6fs
        0x1eas
        0x1ebs
        0x1b7s
        0x292s
        0x6as
        0x44s
        0x44s
        0x64s
        0x47s
        0x67s
        0x1f6s
        0x1f7s
        0x4es
        0x6es
        0xc5s
        0xe5s
        0xc6s
        0xe6s
        0xd8s
        0xf8s
        0x41s
        0x61s
        0x41s
        0x61s
        0x45s
        0x65s
        0x45s
        0x65s
        0x49s
        0x69s
        0x49s
        0x69s
        0x4fs
        0x6fs
        0x4fs
        0x6fs
        0x52s
        0x72s
        0x52s
        0x72s
        0x55s
        0x75s
        0x55s
        0x75s
        0x53s
        0x73s
        0x54s
        0x74s
        0x21cs
        0x21ds
        0x48s
        0x68s
        0x220s
        0x221s
        0x222s
        0x223s
        0x224s
        0x225s
        0x41s
        0x61s
        0x45s
        0x65s
        0xd6s
        0xf6s
        0xd5s
        0xf5s
        0x4fs
        0x6fs
        0x22es
        0x22fs
        0x59s
        0x79s
        0x234s
        0x235s
        0x236s
        0x237s
        0x238s
        0x239s
        0x23as
        0x23bs
        0x23cs
        0x23ds
        0x23es
        0x23fs
        0x240s
        0x241s
        0x242s
        0x243s
        0x244s
        0x245s
        0x246s
        0x247s
        0x248s
        0x249s
        0x24as
        0x24bs
        0x24cs
        0x24ds
        0x24es
        0x24fs
        0x250s
        0x251s
        0x252s
        0x253s
        0x254s
        0x255s
        0x256s
        0x257s
        0x258s
        0x259s
        0x25as
        0x25bs
        0x25cs
        0x25ds
        0x25es
        0x25fs
        0x260s
        0x261s
        0x262s
        0x263s
        0x264s
        0x265s
        0x266s
        0x267s
        0x268s
        0x269s
        0x26as
        0x26bs
        0x26cs
        0x26ds
        0x26es
        0x26fs
        0x270s
        0x271s
        0x272s
        0x273s
        0x274s
        0x275s
        0x276s
        0x277s
        0x278s
        0x279s
        0x27as
        0x27bs
        0x27cs
        0x27ds
        0x27es
        0x27fs
        0x280s
        0x281s
        0x282s
        0x283s
        0x284s
        0x285s
        0x286s
        0x287s
        0x288s
        0x289s
        0x28as
        0x28bs
        0x28cs
        0x28ds
        0x28es
        0x28fs
        0x290s
        0x291s
        0x292s
        0x293s
        0x294s
        0x295s
        0x296s
        0x297s
        0x298s
        0x299s
        0x29as
        0x29bs
        0x29cs
        0x29ds
        0x29es
        0x29fs
        0x2a0s
        0x2a1s
        0x2a2s
        0x2a3s
        0x2a4s
        0x2a5s
        0x2a6s
        0x2a7s
        0x2a8s
        0x2a9s
        0x2aas
        0x2abs
        0x2acs
        0x2ads
        0x2aes
        0x2afs
        0x68s
        0x266s
        0x6as
        0x72s
        0x279s
        0x27bs
        0x281s
        0x77s
        0x79s
        0x2b9s
        0x2bas
        0x2bbs
        0x2bcs
        0x2bds
        0x2bes
        0x2bfs
        0x2c0s
        0x2c1s
        0x2c2s
        0x2c3s
        0x2c4s
        0x2c5s
        0x2c6s
        0x2c7s
        0x2c8s
        0x2c9s
        0x2cas
        0x2cbs
        0x2ccs
        0x2cds
        0x2ces
        0x2cfs
        0x2d0s
        0x2d1s
        0x2d2s
        0x2d3s
        0x2d4s
        0x2d5s
        0x2d6s
        0x2d7s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x2des
        0x2dfs
        0x263s
        0x6cs
        0x73s
        0x78s
        0x295s
        0x2e5s
        0x2e6s
        0x2e7s
        0x2e8s
        0x2e9s
        0x2eas
        0x2ebs
        0x2ecs
        0x2eds
        0x2ees
        0x2efs
        0x2f0s
        0x2f1s
        0x2f2s
        0x2f3s
        0x2f4s
        0x2f5s
        0x2f6s
        0x2f7s
        0x2f8s
        0x2f9s
        0x2fas
        0x2fbs
        0x2fcs
        0x2fds
        0x2fes
        0x2ffs
        0x300s
        0x301s
        0x302s
        0x303s
        0x304s
        0x305s
        0x306s
        0x307s
        0x308s
        0x309s
        0x30as
        0x30bs
        0x30cs
        0x30ds
        0x30es
        0x30fs
        0x310s
        0x311s
        0x312s
        0x313s
        0x314s
        0x315s
        0x316s
        0x317s
        0x318s
        0x319s
        0x31as
        0x31bs
        0x31cs
        0x31ds
        0x31es
        0x31fs
        0x320s
        0x321s
        0x322s
        0x323s
        0x324s
        0x325s
        0x326s
        0x327s
        0x328s
        0x329s
        0x32as
        0x32bs
        0x32cs
        0x32ds
        0x32es
        0x32fs
        0x330s
        0x331s
        0x332s
        0x333s
        0x334s
        0x335s
        0x336s
        0x337s
        0x338s
        0x339s
        0x33as
        0x33bs
        0x33cs
        0x33ds
        0x33es
        0x33fs
        0x300s
        0x301s
        0x342s
        0x313s
        0x308s
        0x345s
        0x346s
        0x347s
        0x348s
        0x349s
        0x34as
        0x34bs
        0x34cs
        0x34ds
        0x34es
        0x34fs
        0x350s
        0x351s
        0x352s
        0x353s
        0x354s
        0x355s
        0x356s
        0x357s
        0x358s
        0x359s
        0x35as
        0x35bs
        0x35cs
        0x35ds
        0x35es
        0x35fs
        0x360s
        0x361s
        0x362s
        0x363s
        0x364s
        0x365s
        0x366s
        0x367s
        0x368s
        0x369s
        0x36as
        0x36bs
        0x36cs
        0x36ds
        0x36es
        0x36fs
        0x370s
        0x371s
        0x372s
        0x373s
        0x2b9s
        0x375s
        0x376s
        0x377s
        0x378s
        0x379s
        0x20s
        0x37bs
        0x37cs
        0x37ds
        0x3bs
        0x37fs
        0x380s
        0x381s
        0x382s
        0x383s
        0x20s
        0xa8s
        0x391s
        0xb7s
        0x395s
        0x397s
        0x399s
        0x38bs
        0x39fs
        0x38ds
        0x3a5s
        0x3a9s
        0x3cas
        0x391s
        0x392s
        0x393s
        0x394s
        0x395s
        0x396s
        0x397s
        0x398s
        0x399s
        0x39as
        0x39bs
        0x39cs
        0x39ds
        0x39es
        0x39fs
        0x3a0s
        0x3a1s
        0x3a2s
        0x3a3s
        0x3a4s
        0x3a5s
        0x3a6s
        0x3a7s
        0x3a8s
        0x3a9s
        0x399s
        0x3a5s
        0x3b1s
        0x3b5s
        0x3b7s
        0x3b9s
        0x3cbs
        0x3b1s
        0x3b2s
        0x3b3s
        0x3b4s
        0x3b5s
        0x3b6s
        0x3b7s
        0x3b8s
        0x3b9s
        0x3bas
        0x3bbs
        0x3bcs
        0x3bds
        0x3bes
        0x3bfs
        0x3c0s
        0x3c1s
        0x3c2s
        0x3c3s
        0x3c4s
        0x3c5s
        0x3c6s
        0x3c7s
        0x3c8s
        0x3c9s
        0x3b9s
        0x3c5s
        0x3bfs
        0x3c5s
        0x3c9s
        0x3cfs
        0x3b2s
        0x3b8s
        0x3a5s
        0x3d2s
        0x3d2s
        0x3c6s
        0x3c0s
        0x3d7s
        0x3d8s
        0x3d9s
        0x3das
        0x3dbs
        0x3dcs
        0x3dds
        0x3des
        0x3dfs
        0x3e0s
        0x3e1s
        0x3e2s
        0x3e3s
        0x3e4s
        0x3e5s
        0x3e6s
        0x3e7s
        0x3e8s
        0x3e9s
        0x3eas
        0x3ebs
        0x3ecs
        0x3eds
        0x3ees
        0x3efs
        0x3bas
        0x3c1s
        0x3c2s
        0x3f3s
        0x398s
        0x3b5s
        0x3f6s
        0x3f7s
        0x3f8s
        0x3a3s
        0x3fas
        0x3fbs
        0x3fcs
        0x3fds
        0x3fes
        0x3ffs
        0x415s
        0x415s
        0x402s
        0x413s
        0x404s
        0x405s
        0x406s
        0x406s
        0x408s
        0x409s
        0x40as
        0x40bs
        0x41as
        0x418s
        0x423s
        0x40fs
        0x410s
        0x411s
        0x412s
        0x413s
        0x414s
        0x415s
        0x416s
        0x417s
        0x418s
        0x418s
        0x41as
        0x41bs
        0x41cs
        0x41ds
        0x41es
        0x41fs
        0x420s
        0x421s
        0x422s
        0x423s
        0x424s
        0x425s
        0x426s
        0x427s
        0x428s
        0x429s
        0x42as
        0x42bs
        0x42cs
        0x42ds
        0x42es
        0x42fs
        0x430s
        0x431s
        0x432s
        0x433s
        0x434s
        0x435s
        0x436s
        0x437s
        0x438s
        0x438s
        0x43as
        0x43bs
        0x43cs
        0x43ds
        0x43es
        0x43fs
        0x440s
        0x441s
        0x442s
        0x443s
        0x444s
        0x445s
        0x446s
        0x447s
        0x448s
        0x449s
        0x44as
        0x44bs
        0x44cs
        0x44ds
        0x44es
        0x44fs
        0x435s
        0x435s
        0x452s
        0x433s
        0x454s
        0x455s
        0x456s
        0x456s
        0x458s
        0x459s
        0x45as
        0x45bs
        0x43as
        0x438s
        0x443s
        0x45fs
        0x460s
        0x461s
        0x462s
        0x463s
        0x464s
        0x465s
        0x466s
        0x467s
        0x468s
        0x469s
        0x46as
        0x46bs
        0x46cs
        0x46ds
        0x46es
        0x46fs
        0x470s
        0x471s
        0x472s
        0x473s
        0x474s
        0x475s
        0x474s
        0x475s
        0x478s
        0x479s
        0x47as
        0x47bs
        0x47cs
        0x47ds
        0x47es
        0x47fs
        0x480s
        0x481s
        0x482s
        0x483s
        0x484s
        0x485s
        0x486s
        0x487s
        0x488s
        0x489s
        0x48as
        0x48bs
        0x48cs
        0x48ds
        0x48es
        0x48fs
        0x490s
        0x491s
        0x492s
        0x493s
        0x494s
        0x495s
        0x496s
        0x497s
        0x498s
        0x499s
        0x49as
        0x49bs
        0x49cs
        0x49ds
        0x49es
        0x49fs
        0x4a0s
        0x4a1s
        0x4a2s
        0x4a3s
        0x4a4s
        0x4a5s
        0x4a6s
        0x4a7s
        0x4a8s
        0x4a9s
        0x4aas
        0x4abs
        0x4acs
        0x4ads
        0x4aes
        0x4afs
        0x4b0s
        0x4b1s
        0x4b2s
        0x4b3s
        0x4b4s
        0x4b5s
        0x4b6s
        0x4b7s
        0x4b8s
        0x4b9s
        0x4bas
        0x4bbs
        0x4bcs
        0x4bds
        0x4bes
        0x4bfs
        0x4c0s
        0x416s
        0x436s
        0x4c3s
        0x4c4s
        0x4c5s
        0x4c6s
        0x4c7s
        0x4c8s
        0x4c9s
        0x4cas
        0x4cbs
        0x4ccs
        0x4cds
        0x4ces
        0x4cfs
        0x410s
        0x430s
        0x410s
        0x430s
        0x4d4s
        0x4d5s
        0x415s
        0x435s
        0x4d8s
        0x4d9s
        0x4d8s
        0x4d9s
        0x416s
        0x436s
        0x417s
        0x437s
        0x4e0s
        0x4e1s
        0x418s
        0x438s
        0x418s
        0x438s
        0x41es
        0x43es
        0x4e8s
        0x4e9s
        0x4e8s
        0x4e9s
        0x42ds
        0x44ds
        0x423s
        0x443s
        0x423s
        0x443s
        0x423s
        0x443s
        0x427s
        0x447s
        0x4f6s
        0x4f7s
        0x42bs
        0x44bs
        0x4fas
        0x4fbs
        0x4fcs
        0x4fds
        0x4fes
        0x4ffs
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/16 v1, 0x30

    invoke-direct {p0, p2}, Lcom/android/inputmethod/latin/Dictionary;-><init>(Ljava/lang/String;)V

    new-array v0, v1, [C

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mWordBuilder:[C

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    new-array v0, v1, [C

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mLookedUpString:[C

    iput-object p1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->clearDictionary()V

    new-array v0, v1, [[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/ExpandableDictionary;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/ExpandableDictionary;

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/inputmethod/latin/ExpandableDictionary;Z)Z
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/ExpandableDictionary;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    return p1
.end method

.method private addWordAndShortcutsFromNode(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;[CIILjava/util/ArrayList;)Z
    .locals 10
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    .param p2    # [C
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/ExpandableDictionary$Node;",
            "[CII",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)Z"
        }
    .end annotation

    const/16 v9, 0x12

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-lez p4, :cond_1

    iget-boolean v5, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutOnly:Z

    if-nez v5, :cond_1

    new-instance v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    new-instance v6, Ljava/lang/String;

    add-int/lit8 v7, p3, 0x1

    invoke-direct {v6, p2, v3, v7}, Ljava/lang/String;-><init>([CII)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/Dictionary;->mDictType:Ljava/lang/String;

    invoke-direct {v5, v6, p4, v4, v7}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;IILjava/lang/String;)V

    invoke-virtual {p5, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v5, v9, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    iget-object v5, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    iget-object v5, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    new-instance v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    new-instance v6, Ljava/lang/String;

    array-length v7, v1

    invoke-direct {v6, v1, v3, v7}, Ljava/lang/String;-><init>([CII)V

    const/4 v7, 0x7

    iget-object v8, p0, Lcom/android/inputmethod/latin/Dictionary;->mDictType:Ljava/lang/String;

    invoke-direct {v5, v6, p4, v7, v8}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;IILjava/lang/String;)V

    invoke-virtual {p5, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v5, v9, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method private addWordRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILjava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)V
    .locals 14
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    move/from16 v0, p3

    if-gt v13, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p2 .. p3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    iget v9, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mLength:I

    const/4 v7, 0x0

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v9, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mData:[Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    aget-object v12, v1, v10

    iget-char v1, v12, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    if-ne v1, v8, :cond_5

    move-object v7, v12

    :cond_2
    if-eqz p4, :cond_6

    const/4 v11, 0x1

    :goto_2
    if-nez v7, :cond_3

    new-instance v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    invoke-direct {v7}, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;-><init>()V

    iput-char v8, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    move-object/from16 v0, p6

    iput-object v0, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mParent:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    iput-boolean v11, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutOnly:Z

    invoke-virtual {p1, v7}, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->add(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;)V

    :cond_3
    add-int/lit8 v1, p3, 0x1

    if-ne v13, v1, :cond_8

    if-eqz p4, :cond_8

    const/4 v1, 0x1

    iput-boolean v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mTerminal:Z

    if-eqz v11, :cond_7

    iget-object v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    :cond_4
    iget-object v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    iget v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    move/from16 v0, p5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    iget v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    const/16 v2, 0xff

    if-le v1, v2, :cond_0

    const/16 v1, 0xff

    iput v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    goto :goto_0

    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_6
    const/4 v11, 0x0

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutOnly:Z

    goto :goto_3

    :cond_8
    iget-object v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    if-nez v1, :cond_9

    new-instance v1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {v1}, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;-><init>()V

    iput-object v1, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    :cond_9
    iget-object v2, v7, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    add-int/lit8 v4, p3, 0x1

    move-object v1, p0

    move-object/from16 v3, p2

    move-object/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWordRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILjava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)V

    goto :goto_0
.end method

.method private static computeSkippedWordFinalFreq(III)I
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x3

    if-lt p2, v0, :cond_0

    mul-int v0, p0, p1

    add-int/lit8 v1, p2, -0x2

    mul-int/2addr v0, v1

    add-int/lit8 v1, p2, -0x1

    div-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reverseLookUp(Ljava/util/LinkedList;Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;

    invoke-interface {v3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getWordNode()Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v4

    invoke-interface {v3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getFrequency()I

    move-result v0

    const/16 v2, 0x30

    :cond_1
    add-int/lit8 v2, v2, -0x1

    iget-object v5, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mLookedUpString:[C

    iget-char v6, v4, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    aput-char v6, v5, v2

    iget-object v4, v4, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mParent:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    if-eqz v4, :cond_2

    if-gtz v2, :cond_1

    :cond_2
    if-ltz v0, :cond_0

    if-nez v4, :cond_0

    new-instance v5, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;

    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mLookedUpString:[C

    rsub-int/lit8 v8, v2, 0x30

    invoke-direct {v6, v7, v2, v8}, Ljava/lang/String;-><init>([CII)V

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/inputmethod/latin/Dictionary;->mDictType:Ljava/lang/String;

    invoke-direct {v5, v6, v0, v7, v8}, Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;-><init>(Ljava/lang/CharSequence;IILjava/lang/String;)V

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method private runBigramReverseLookUp(Ljava/lang/CharSequence;Ljava/util/ArrayList;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchNode(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/CharSequence;II)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    invoke-direct {p0, v1, p2}, Lcom/android/inputmethod/latin/ExpandableDictionary;->reverseLookUp(Ljava/util/LinkedList;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method private searchNode(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/CharSequence;II)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    .locals 7
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I

    iget v0, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mLength:I

    invoke-interface {p2, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v5, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mData:[Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    aget-object v3, v5, v2

    iget-char v5, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    if-ne v5, v1, :cond_1

    add-int/lit8 v5, p4, -0x1

    if-ne p3, v5, :cond_0

    iget-boolean v5, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mTerminal:Z

    if-eqz v5, :cond_1

    :goto_1
    return-object v3

    :cond_0
    iget-object v5, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    if-eqz v5, :cond_1

    iget-object v5, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    add-int/lit8 v6, p3, 0x1

    invoke-direct {p0, v5, p2, v6, p4}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchNode(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/CharSequence;II)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v3, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;
    .locals 8
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p2, p3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget v2, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mLength:I

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    iget-object v6, p1, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mData:[Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    aget-object v4, v6, v3

    iget-char v6, v4, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    if-ne v6, v0, :cond_2

    move-object v1, v4

    :cond_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    invoke-direct {v1}, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;-><init>()V

    iput-char v0, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    iput-object p4, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mParent:Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->add(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;)V

    :cond_1
    add-int/lit8 v6, p3, 0x1

    if-ne v5, v6, :cond_3

    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mTerminal:Z

    :goto_1
    return-object v1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v6, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    if-nez v6, :cond_4

    new-instance v6, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {v6}, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;-><init>()V

    iput-object v6, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    :cond_4
    iget-object v6, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    add-int/lit8 v7, p3, 0x1

    invoke-direct {p0, v6, p2, v7, v1}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v1

    goto :goto_1
.end method

.method private setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6, v7, v8}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v1

    iget-object v5, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {p0, v5, p2, v7, v8}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v4

    iget-object v0, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v5

    iput-object v5, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    iget-object v0, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    :cond_1
    if-eqz p4, :cond_4

    iget-object v5, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    new-instance v6, Lcom/android/inputmethod/latin/ExpandableDictionary$NextHistoryWord;

    invoke-direct {v6, v4, p4}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextHistoryWord;-><init>(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_0
    return p3

    :cond_2
    invoke-virtual {v0}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;

    invoke-interface {v3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getWordNode()Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v5

    if-ne v5, v4, :cond_3

    invoke-interface {v3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->notifyTypedAgainAndGetFrequency()I

    move-result p3

    goto :goto_0

    :cond_4
    iget-object v5, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    new-instance v6, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;

    invoke-direct {v6, v4, p3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextStaticWord;-><init>(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;I)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static toLowerCase(C)C
    .locals 2
    .param p0    # C

    move v0, p0

    sget-object v1, Lcom/android/inputmethod/latin/ExpandableDictionary;->BASE_CHARS:[C

    array-length v1, v1

    if-ge p0, v1, :cond_0

    sget-object v1, Lcom/android/inputmethod/latin/ExpandableDictionary;->BASE_CHARS:[C

    aget-char v0, v1, p0

    :cond_0
    const/16 v1, 0x41

    if-lt v0, v1, :cond_2

    const/16 v1, 0x5a

    if-gt v0, v1, :cond_2

    or-int/lit8 v1, v0, 0x20

    int-to-char v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/16 v1, 0x7f

    if-le v0, v1, :cond_1

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public addWord(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWordRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILjava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)V

    goto :goto_0
.end method

.method protected final blockingReloadDictionaryIfRequired()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->reloadDictionaryIfRequired()Z

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->waitForDictionaryLoading()V

    return-void
.end method

.method protected clearDictionary()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    return-void
.end method

.method protected getBigramWord(Ljava/lang/String;Ljava/lang/String;)Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v8, v5}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v1

    iget-object v6, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {p0, v6, p2, v8, v5}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v4

    iget-object v0, v1, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    move-object v3, v5

    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {v0}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;

    invoke-interface {v3}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getWordNode()Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v6

    if-ne v6, v4, :cond_2

    goto :goto_0

    :cond_3
    move-object v3, v5

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getMaxWordLength()I
    .locals 1

    const/16 v0, 0x30

    return v0
.end method

.method public getRequiresReload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRequiresReload:Z

    return v0
.end method

.method public getSuggestions(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/keyboard/ProximityInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/WordComposer;",
            "Ljava/lang/CharSequence;",
            "Lcom/android/inputmethod/keyboard/ProximityInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->reloadDictionaryIfRequired()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    const/16 v2, 0x30

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->runBigramReverseLookUp(Ljava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method protected getWordFrequency(Ljava/lang/CharSequence;)I
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchNode(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/CharSequence;II)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    goto :goto_0
.end method

.method protected getWordsInner(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;)Ljava/util/ArrayList;
    .locals 16
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Lcom/android/inputmethod/keyboard/ProximityInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/WordComposer;",
            "Ljava/lang/CharSequence;",
            "Lcom/android/inputmethod/keyboard/ProximityInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    array-length v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    if-ge v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    new-array v1, v1, [[I

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/latin/WordComposer;->getInputPointers()Lcom/android/inputmethod/latin/InputPointers;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/inputmethod/latin/InputPointers;->getXCoordinates()[I

    move-result-object v13

    invoke-virtual {v11}, Lcom/android/inputmethod/latin/InputPointers;->getYCoordinates()[I

    move-result-object v15

    const/4 v9, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    if-ge v9, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    aget-object v1, v1, v9

    array-length v1, v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    const/16 v2, 0x10

    new-array v2, v2, [I

    aput-object v2, v1, v9

    :cond_2
    if-eqz v13, :cond_3

    array-length v1, v13

    if-ge v9, v1, :cond_3

    aget v12, v13, v9

    :goto_1
    if-eqz v13, :cond_4

    array-length v1, v15

    if-ge v9, v1, :cond_4

    aget v14, v15, v9

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/android/inputmethod/latin/WordComposer;->getCodeAt(I)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    aget-object v2, v2, v9

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v14, v1, v2}, Lcom/android/inputmethod/keyboard/ProximityInfo;->fillArrayWithNearestKeyCodes(III[I)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_3
    const/4 v12, -0x1

    goto :goto_1

    :cond_4
    const/4 v14, -0x1

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    mul-int/lit8 v1, v1, 0x3

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mMaxDepth:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mWordBuilder:[C

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, -0x1

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    invoke-virtual/range {v1 .. v10}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    const/4 v9, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    if-ge v9, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mWordBuilder:[C

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    invoke-virtual/range {v1 .. v10}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_6
    return-object v10
.end method

.method protected getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V
    .locals 29
    .param p1    # Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;
    .param p2    # Lcom/android/inputmethod/latin/WordComposer;
    .param p3    # [C
    .param p4    # I
    .param p5    # Z
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;",
            "Lcom/android/inputmethod/latin/WordComposer;",
            "[CIZIII",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/SuggestedWords$SuggestedWordInfo;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mLength:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mMaxDepth:I

    move/from16 v0, p4

    if-le v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    move/from16 v0, v20

    move/from16 v1, p7

    if-gt v0, v1, :cond_5

    const/16 v23, 0x0

    :goto_0
    const/16 v25, 0x0

    :goto_1
    move/from16 v0, v25

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;->mData:[Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    aget-object v3, v2, v25

    iget-char v0, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mCode:C

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/inputmethod/latin/ExpandableDictionary;->toLowerCase(C)C

    move-result v27

    iget-boolean v0, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mTerminal:Z

    move/from16 v28, v0

    iget-object v8, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mChildren:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    iget v0, v3, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mFrequency:I

    move/from16 v24, v0

    if-nez p5, :cond_2

    if-nez v23, :cond_7

    :cond_2
    aput-char v19, p3, p4

    if-eqz v28, :cond_3

    if-gez p8, :cond_6

    mul-int v6, v24, p6

    :goto_2
    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v7, p9

    invoke-direct/range {v2 .. v7}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWordAndShortcutsFromNode(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;[CIILjava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    if-eqz v8, :cond_4

    add-int/lit8 v11, p4, 0x1

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v13, p6

    move/from16 v14, p7

    move/from16 v15, p8

    move-object/from16 v16, p9

    invoke-virtual/range {v7 .. v16}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    :cond_4
    :goto_3
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mCodes:[[I

    aget-object v23, v2, p7

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    move/from16 v0, v24

    move/from16 v1, p6

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/ExpandableDictionary;->computeSkippedWordFinalFreq(III)I

    move-result v6

    goto :goto_2

    :cond_7
    const/16 v2, 0x27

    move/from16 v0, v19

    if-ne v0, v2, :cond_8

    const/4 v2, 0x0

    aget v2, v23, v2

    const/16 v4, 0x27

    if-ne v2, v4, :cond_9

    :cond_8
    move/from16 v0, p4

    move/from16 v1, p8

    if-ne v0, v1, :cond_a

    :cond_9
    aput-char v19, p3, p4

    if-eqz v8, :cond_4

    add-int/lit8 v11, p4, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    move/from16 v15, p8

    move-object/from16 v16, p9

    invoke-virtual/range {v7 .. v16}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    goto :goto_3

    :cond_a
    if-ltz p8, :cond_e

    const/16 v18, 0x1

    :goto_4
    const/16 v26, 0x0

    :goto_5
    move/from16 v0, v26

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    if-lez v26, :cond_f

    const/16 v17, 0x1

    :goto_6
    aget v22, v23, v26

    const/4 v2, -0x1

    move/from16 v0, v22

    if-eq v0, v2, :cond_4

    move/from16 v0, v22

    move/from16 v1, v27

    if-eq v0, v1, :cond_b

    move/from16 v0, v22

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    :cond_b
    aput-char v19, p3, p4

    add-int/lit8 v2, p7, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_11

    if-eqz v28, :cond_c

    if-gez p8, :cond_10

    mul-int v2, v24, p6

    mul-int v2, v2, v17

    mul-int/lit8 v6, v2, 0x2

    :goto_7
    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v7, p9

    invoke-direct/range {v2 .. v7}, Lcom/android/inputmethod/latin/ExpandableDictionary;->addWordAndShortcutsFromNode(Lcom/android/inputmethod/latin/ExpandableDictionary$Node;[CIILjava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_c
    if-eqz v8, :cond_d

    add-int/lit8 v11, p4, 0x1

    const/4 v12, 0x1

    mul-int v13, p6, v17

    add-int/lit8 v14, p7, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v15, p8

    move-object/from16 v16, p9

    invoke-virtual/range {v7 .. v16}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    :cond_d
    :goto_8
    add-int/lit8 v26, v26, 0x1

    goto :goto_5

    :cond_e
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v18, v0

    goto :goto_4

    :cond_f
    const/16 v17, 0x2

    goto :goto_6

    :cond_10
    mul-int v2, p6, v17

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mInputLength:I

    move/from16 v0, v24

    invoke-static {v0, v2, v4}, Lcom/android/inputmethod/latin/ExpandableDictionary;->computeSkippedWordFinalFreq(III)I

    move-result v6

    goto :goto_7

    :cond_11
    if-eqz v8, :cond_d

    add-int/lit8 v11, p4, 0x1

    const/4 v12, 0x0

    mul-int v13, p6, v17

    add-int/lit8 v14, p7, 0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v15, p8

    move-object/from16 v16, p9

    invoke-virtual/range {v7 .. v16}, Lcom/android/inputmethod/latin/ExpandableDictionary;->getWordsRec(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Lcom/android/inputmethod/latin/WordComposer;[CIZIIILjava/util/ArrayList;)V

    goto :goto_8
.end method

.method public declared-synchronized isValidWord(Ljava/lang/CharSequence;)Z
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v3, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRequiresReload:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->startDictionaryLoadingTaskLocked()V

    :cond_0
    iget-boolean v3, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    if-eqz v3, :cond_2

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    :cond_2
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v2, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-direct {p0, v2, p1, v3, v4}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchNode(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/CharSequence;II)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mShortcutOnly:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public loadDictionary()V
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->startDictionaryLoadingTaskLocked()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public loadDictionaryAsync()V
    .locals 0

    return-void
.end method

.method reloadDictionaryIfRequired()Z
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRequiresReload:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->startDictionaryLoadingTaskLocked()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected removeBigram(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8, v6, v9}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v2

    iget-object v7, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRoots:Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;

    invoke-direct {p0, v7, p2, v6, v9}, Lcom/android/inputmethod/latin/ExpandableDictionary;->searchWord(Lcom/android/inputmethod/latin/ExpandableDictionary$NodeArray;Ljava/lang/String;ILcom/android/inputmethod/latin/ExpandableDictionary$Node;)Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v5

    iget-object v1, v2, Lcom/android/inputmethod/latin/ExpandableDictionary$Node;->mNGrams:Ljava/util/LinkedList;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {v1}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;

    invoke-interface {v4}, Lcom/android/inputmethod/latin/ExpandableDictionary$NextWord;->getWordNode()Lcom/android/inputmethod/latin/ExpandableDictionary$Node;

    move-result-object v7

    if-ne v7, v5, :cond_2

    move-object v0, v4

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_0
.end method

.method public setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/inputmethod/latin/ExpandableDictionary;->setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I

    move-result v0

    return v0
.end method

.method public setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/android/inputmethod/latin/ExpandableDictionary;->setBigramAndGetFrequency(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/UserHistoryForgettingCurveUtils$ForgettingCurveParams;)I

    move-result v0

    return v0
.end method

.method public setRequiresReload(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-boolean p1, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRequiresReload:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startDictionaryLoadingTaskLocked()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mRequiresReload:Z

    new-instance v0, Lcom/android/inputmethod/latin/ExpandableDictionary$LoadDictionaryTask;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/ExpandableDictionary$LoadDictionaryTask;-><init>(Lcom/android/inputmethod/latin/ExpandableDictionary;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method waitForDictionaryLoading()V
    .locals 2

    :goto_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/ExpandableDictionary;->mUpdatingDictionary:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method
