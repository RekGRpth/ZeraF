.class public final Lcom/android/inputmethod/latin/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final Keyboard:I = 0x7f0f0003

.field public static final KeyboardIcons:I = 0x7f0f0002

.field public static final KeyboardIcons_Black:I = 0x7f0f0000

.field public static final KeyboardIcons_IceCreamSandwich:I = 0x7f0f0001

.field public static final KeyboardTheme:I = 0x7f0f002b

.field public static final KeyboardTheme_Gingerbread:I = 0x7f0f002c

.field public static final KeyboardTheme_HighContrast:I = 0x7f0f002a

.field public static final KeyboardTheme_IceCreamSandwich:I = 0x7f0f002d

.field public static final KeyboardTheme_Stone:I = 0x7f0f002f

.field public static final KeyboardTheme_Stone_Bold:I = 0x7f0f002e

.field public static final KeyboardView:I = 0x7f0f0004

.field public static final KeyboardView_Gingerbread:I = 0x7f0f001a

.field public static final KeyboardView_HighContrast:I = 0x7f0f000f

.field public static final KeyboardView_IceCreamSandwich:I = 0x7f0f001f

.field public static final KeyboardView_Stone:I = 0x7f0f0012

.field public static final KeyboardView_Stone_Bold:I = 0x7f0f0017

.field public static final Keyboard_Gingerbread:I = 0x7f0f0019

.field public static final Keyboard_HighContrast:I = 0x7f0f000e

.field public static final Keyboard_IceCreamSandwich:I = 0x7f0f001e

.field public static final Keyboard_Stone:I = 0x7f0f0011

.field public static final Keyboard_Stone_Bold:I = 0x7f0f0016

.field public static final MainKeyboardView:I = 0x7f0f0005

.field public static final MainKeyboardView_Gingerbread:I = 0x7f0f001b

.field public static final MainKeyboardView_HighContrast:I = 0x7f0f0010

.field public static final MainKeyboardView_IceCreamSandwich:I = 0x7f0f0020

.field public static final MainKeyboardView_Stone:I = 0x7f0f0013

.field public static final MainKeyboardView_Stone_Bold:I = 0x7f0f0018

.field public static final MoreKeysKeyboard:I = 0x7f0f0006

.field public static final MoreKeysKeyboardAnimation:I = 0x7f0f0029

.field public static final MoreKeysKeyboardPanelStyle:I = 0x7f0f0008

.field public static final MoreKeysKeyboardPanelStyle_IceCreamSandwich:I = 0x7f0f0023

.field public static final MoreKeysKeyboardView:I = 0x7f0f0007

.field public static final MoreKeysKeyboardView_Gingerbread:I = 0x7f0f001d

.field public static final MoreKeysKeyboardView_IceCreamSandwich:I = 0x7f0f0022

.field public static final MoreKeysKeyboardView_Stone:I = 0x7f0f0015

.field public static final MoreKeysKeyboard_Gingerbread:I = 0x7f0f001c

.field public static final MoreKeysKeyboard_IceCreamSandwich:I = 0x7f0f0021

.field public static final MoreKeysKeyboard_Stone:I = 0x7f0f0014

.field public static final MoreSuggestionsViewStyle:I = 0x7f0f000b

.field public static final MoreSuggestionsViewStyle_IceCreamSandwich:I = 0x7f0f0026

.field public static final SuggestionBackgroundStyle:I = 0x7f0f000c

.field public static final SuggestionBackgroundStyle_IceCreamSandwich:I = 0x7f0f0027

.field public static final SuggestionPreviewBackgroundStyle:I = 0x7f0f000d

.field public static final SuggestionPreviewBackgroundStyle_IceCreamSandwich:I = 0x7f0f0028

.field public static final SuggestionStripViewStyle:I = 0x7f0f000a

.field public static final SuggestionStripViewStyle_IceCreamSandwich:I = 0x7f0f0025

.field public static final SuggestionsStripBackgroundStyle:I = 0x7f0f0009

.field public static final SuggestionsStripBackgroundStyle_IceCreamSandwich:I = 0x7f0f0024


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
