.class public final Lcom/android/inputmethod/latin/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add:I = 0x7f0b00b0

.field public static final add_style:I = 0x7f0b00af

.field public static final added_word:I = 0x7f0b005c

.field public static final advanced_settings:I = 0x7f0b003b

.field public static final advanced_settings_summary:I = 0x7f0b003c

.field public static final android_spell_checker_settings:I = 0x7f0b002d

.field public static final aosp_android_keyboard_ime_name:I = 0x7f0b002e

.field public static final aosp_spell_checker_service_name:I = 0x7f0b0031

.field public static final auto_cap:I = 0x7f0b0046

.field public static final auto_cap_summary:I = 0x7f0b0047

.field public static final auto_correction:I = 0x7f0b004f

.field public static final auto_correction_summary:I = 0x7f0b0050

.field public static final auto_correction_threshold_mode_aggeressive:I = 0x7f0b0053

.field public static final auto_correction_threshold_mode_index_aggeressive:I = 0x7f0b000f

.field public static final auto_correction_threshold_mode_index_modest:I = 0x7f0b000e

.field public static final auto_correction_threshold_mode_index_off:I = 0x7f0b000d

.field public static final auto_correction_threshold_mode_index_very_aggeressive:I = 0x7f0b0010

.field public static final auto_correction_threshold_mode_modest:I = 0x7f0b0052

.field public static final auto_correction_threshold_mode_off:I = 0x7f0b0051

.field public static final auto_correction_threshold_mode_very_aggeressive:I = 0x7f0b0054

.field public static final bigram_prediction:I = 0x7f0b0055

.field public static final bigram_prediction_summary:I = 0x7f0b0056

.field public static final bigram_suggestion:I = 0x7f0b00bf

.field public static final bigram_suggestion_summary:I = 0x7f0b00c0

.field public static final config_default_keyboard_theme_index:I = 0x7f0b0000

.field public static final configure_dictionaries_title:I = 0x7f0b0048

.field public static final configure_input_method:I = 0x7f0b008b

.field public static final correction_category:I = 0x7f0b0038

.field public static final custom_input_style_already_exists:I = 0x7f0b00b8

.field public static final custom_input_style_note_message:I = 0x7f0b00b5

.field public static final custom_input_styles_title:I = 0x7f0b00ae

.field public static final dictionary_pack_package_name:I = 0x7f0b0025

.field public static final dictionary_pack_settings_activity:I = 0x7f0b0026

.field public static final enable:I = 0x7f0b00b6

.field public static final enable_span_insert:I = 0x7f0b00bd

.field public static final enable_span_insert_summary:I = 0x7f0b00be

.field public static final english_ime_debug_settings:I = 0x7f0b0014

.field public static final english_ime_input_options:I = 0x7f0b002f

.field public static final english_ime_name:I = 0x7f0b002a

.field public static final english_ime_research_log:I = 0x7f0b0030

.field public static final english_ime_settings:I = 0x7f0b002c

.field public static final general_category:I = 0x7f0b0037

.field public static final gesture_floating_preview_text:I = 0x7f0b005a

.field public static final gesture_floating_preview_text_summary:I = 0x7f0b005b

.field public static final gesture_input:I = 0x7f0b0057

.field public static final gesture_input_summary:I = 0x7f0b0058

.field public static final gesture_preview_trail:I = 0x7f0b0059

.field public static final gesture_typing_category:I = 0x7f0b0039

.field public static final has_dictionary:I = 0x7f0b009f

.field public static final hint_add_to_dictionary:I = 0x7f0b009e

.field public static final include_other_imes_in_language_switch_list:I = 0x7f0b003d

.field public static final include_other_imes_in_language_switch_list_summary:I = 0x7f0b003e

.field public static final key_preview_popup_dismiss_default_delay:I = 0x7f0b0043

.field public static final key_preview_popup_dismiss_delay:I = 0x7f0b0041

.field public static final key_preview_popup_dismiss_no_delay:I = 0x7f0b0042

.field public static final keyboard_layout:I = 0x7f0b00a2

.field public static final keyboard_layout_set:I = 0x7f0b00b4

.field public static final label_done_key:I = 0x7f0b0060

.field public static final label_go_key:I = 0x7f0b005d

.field public static final label_next_key:I = 0x7f0b005e

.field public static final label_pause_key:I = 0x7f0b0065

.field public static final label_previous_key:I = 0x7f0b005f

.field public static final label_send_key:I = 0x7f0b0061

.field public static final label_to_alpha_key:I = 0x7f0b0062

.field public static final label_to_symbol_key:I = 0x7f0b0063

.field public static final label_to_symbol_with_microphone_key:I = 0x7f0b0064

.field public static final label_wait_key:I = 0x7f0b0066

.field public static final language_selection_title:I = 0x7f0b008c

.field public static final layout_basic:I = 0x7f0b0017

.field public static final layout_gingerbread:I = 0x7f0b001b

.field public static final layout_high_contrast:I = 0x7f0b0018

.field public static final layout_ics:I = 0x7f0b001c

.field public static final layout_stone_bold:I = 0x7f0b0019

.field public static final layout_stone_normal:I = 0x7f0b001a

.field public static final layout_switch_back_symbols:I = 0x7f0b0008

.field public static final main_dictionary:I = 0x7f0b0049

.field public static final misc_category:I = 0x7f0b003a

.field public static final not_now:I = 0x7f0b00b7

.field public static final phantom_space_promoting_symbols:I = 0x7f0b0006

.field public static final popup_on_keypress:I = 0x7f0b0036

.field public static final prefs_debug_mode:I = 0x7f0b0015

.field public static final prefs_description_log:I = 0x7f0b00a1

.field public static final prefs_enable_log:I = 0x7f0b00a0

.field public static final prefs_force_non_distinct_multitouch:I = 0x7f0b0016

.field public static final prefs_keypress_sound_volume_settings:I = 0x7f0b00bb

.field public static final prefs_keypress_vibration_duration_settings:I = 0x7f0b00ba

.field public static final prefs_show_suggestions:I = 0x7f0b004a

.field public static final prefs_show_suggestions_summary:I = 0x7f0b004b

.field public static final prefs_suggestion_visibility_default_value:I = 0x7f0b000c

.field public static final prefs_suggestion_visibility_hide_name:I = 0x7f0b004e

.field public static final prefs_suggestion_visibility_hide_value:I = 0x7f0b000b

.field public static final prefs_suggestion_visibility_show_name:I = 0x7f0b004c

.field public static final prefs_suggestion_visibility_show_only_portrait_name:I = 0x7f0b004d

.field public static final prefs_suggestion_visibility_show_only_portrait_value:I = 0x7f0b000a

.field public static final prefs_suggestion_visibility_show_value:I = 0x7f0b0009

.field public static final prefs_usability_study_mode:I = 0x7f0b00b9

.field public static final remove:I = 0x7f0b00b1

.field public static final research_do_not_log_this_session:I = 0x7f0b008d

.field public static final research_enable_session_logging:I = 0x7f0b008e

.field public static final research_feedback_cancel:I = 0x7f0b0098

.field public static final research_feedback_dialog_title:I = 0x7f0b0094

.field public static final research_feedback_hint:I = 0x7f0b0096

.field public static final research_feedback_include_history_label:I = 0x7f0b0095

.field public static final research_feedback_menu_option:I = 0x7f0b0093

.field public static final research_feedback_send:I = 0x7f0b0097

.field public static final research_log_uploader_name:I = 0x7f0b009c

.field public static final research_logger_upload_url:I = 0x7f0b00c1

.field public static final research_logging_disabled:I = 0x7f0b009b

.field public static final research_notify_logging_suspended:I = 0x7f0b0090

.field public static final research_notify_session_log_deleting:I = 0x7f0b008f

.field public static final research_notify_session_log_not_deleted:I = 0x7f0b0091

.field public static final research_notify_session_logging_enabled:I = 0x7f0b0092

.field public static final research_please_exit_feedback_form:I = 0x7f0b0099

.field public static final research_splash_content:I = 0x7f0b0029

.field public static final research_splash_title:I = 0x7f0b009a

.field public static final save:I = 0x7f0b00b2

.field public static final select_language:I = 0x7f0b009d

.field public static final settings_ms:I = 0x7f0b0027

.field public static final settings_warning_researcher_mode:I = 0x7f0b0028

.field public static final show_language_switch_key:I = 0x7f0b003f

.field public static final show_language_switch_key_summary:I = 0x7f0b0040

.field public static final sound_on_keypress:I = 0x7f0b0035

.field public static final spell_checker_service_name:I = 0x7f0b002b

.field public static final spellchecker_recommended_threshold_value:I = 0x7f0b0001

.field public static final spellchecker_suggestion_threshold_value:I = 0x7f0b0002

.field public static final spoken_current_text_is:I = 0x7f0b0068

.field public static final spoken_description_action_next:I = 0x7f0b007b

.field public static final spoken_description_action_previous:I = 0x7f0b007c

.field public static final spoken_description_caps_lock:I = 0x7f0b006d

.field public static final spoken_description_delete:I = 0x7f0b006e

.field public static final spoken_description_dot:I = 0x7f0b0079

.field public static final spoken_description_language_switch:I = 0x7f0b007a

.field public static final spoken_description_mic:I = 0x7f0b0075

.field public static final spoken_description_mode_alpha:I = 0x7f0b0081

.field public static final spoken_description_mode_phone:I = 0x7f0b0082

.field public static final spoken_description_mode_phone_shift:I = 0x7f0b0083

.field public static final spoken_description_mode_symbol:I = 0x7f0b0080

.field public static final spoken_description_return:I = 0x7f0b0077

.field public static final spoken_description_search:I = 0x7f0b0078

.field public static final spoken_description_settings:I = 0x7f0b0072

.field public static final spoken_description_shift:I = 0x7f0b006b

.field public static final spoken_description_shift_shifted:I = 0x7f0b006c

.field public static final spoken_description_shiftmode_locked:I = 0x7f0b007e

.field public static final spoken_description_shiftmode_off:I = 0x7f0b007f

.field public static final spoken_description_shiftmode_on:I = 0x7f0b007d

.field public static final spoken_description_smiley:I = 0x7f0b0076

.field public static final spoken_description_space:I = 0x7f0b0074

.field public static final spoken_description_tab:I = 0x7f0b0073

.field public static final spoken_description_to_alpha:I = 0x7f0b0070

.field public static final spoken_description_to_numeric:I = 0x7f0b0071

.field public static final spoken_description_to_symbol:I = 0x7f0b006f

.field public static final spoken_description_unknown:I = 0x7f0b006a

.field public static final spoken_no_text_entered:I = 0x7f0b0069

.field public static final spoken_use_headphones:I = 0x7f0b0067

.field public static final subtype_bulgarian_bds:I = 0x7f0b0024

.field public static final subtype_en_GB:I = 0x7f0b00a3

.field public static final subtype_en_US:I = 0x7f0b00a4

.field public static final subtype_generic:I = 0x7f0b001d

.field public static final subtype_generic_azerty:I = 0x7f0b0020

.field public static final subtype_generic_colemak:I = 0x7f0b0022

.field public static final subtype_generic_dvorak:I = 0x7f0b0021

.field public static final subtype_generic_pcqwerty:I = 0x7f0b0023

.field public static final subtype_generic_qwerty:I = 0x7f0b001e

.field public static final subtype_generic_qwertz:I = 0x7f0b001f

.field public static final subtype_locale:I = 0x7f0b00b3

.field public static final subtype_no_language:I = 0x7f0b00a7

.field public static final subtype_no_language_azerty:I = 0x7f0b00aa

.field public static final subtype_no_language_colemak:I = 0x7f0b00ac

.field public static final subtype_no_language_dvorak:I = 0x7f0b00ab

.field public static final subtype_no_language_pcqwerty:I = 0x7f0b00ad

.field public static final subtype_no_language_qwerty:I = 0x7f0b00a8

.field public static final subtype_no_language_qwertz:I = 0x7f0b00a9

.field public static final subtype_with_layout_en_GB:I = 0x7f0b00a5

.field public static final subtype_with_layout_en_US:I = 0x7f0b00a6

.field public static final suggested_punctuations:I = 0x7f0b0003

.field public static final suppress_language_switch_key:I = 0x7f0b00bc

.field public static final symbols_excluded_from_word_separators:I = 0x7f0b0007

.field public static final use_contacts_dict:I = 0x7f0b0044

.field public static final use_contacts_dict_summary:I = 0x7f0b0045

.field public static final use_contacts_for_spellchecking_option_summary:I = 0x7f0b0033

.field public static final use_contacts_for_spellchecking_option_title:I = 0x7f0b0032

.field public static final vibrate_on_keypress:I = 0x7f0b0034

.field public static final voice_input:I = 0x7f0b0084

.field public static final voice_input_modes_main_keyboard:I = 0x7f0b0085

.field public static final voice_input_modes_off:I = 0x7f0b0087

.field public static final voice_input_modes_summary_main_keyboard:I = 0x7f0b0088

.field public static final voice_input_modes_summary_off:I = 0x7f0b008a

.field public static final voice_input_modes_summary_symbols_keyboard:I = 0x7f0b0089

.field public static final voice_input_modes_symbols_keyboard:I = 0x7f0b0086

.field public static final voice_mode_main:I = 0x7f0b0011

.field public static final voice_mode_off:I = 0x7f0b0013

.field public static final voice_mode_symbols:I = 0x7f0b0012

.field public static final weak_space_stripping_symbols:I = 0x7f0b0005

.field public static final weak_space_swapping_symbols:I = 0x7f0b0004


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
