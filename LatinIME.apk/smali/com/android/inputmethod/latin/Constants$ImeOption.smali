.class public final Lcom/android/inputmethod/latin/Constants$ImeOption;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImeOption"
.end annotation


# static fields
.field public static final FORCE_ASCII:Ljava/lang/String; = "forceAscii"

.field public static final NO_MICROPHONE:Ljava/lang/String; = "noMicrophoneKey"

.field public static final NO_MICROPHONE_COMPAT:Ljava/lang/String; = "nm"

.field public static final NO_SETTINGS_KEY:Ljava/lang/String; = "noSettingsKey"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
