.class public final Lcom/android/inputmethod/latin/Constants$Subtype$ExtraValue;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/Constants$Subtype;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExtraValue"
.end annotation


# static fields
.field public static final ASCII_CAPABLE:Ljava/lang/String; = "AsciiCapable"

.field public static final IS_ADDITIONAL_SUBTYPE:Ljava/lang/String; = "isAdditionalSubtype"

.field public static final KEYBOARD_LAYOUT_SET:Ljava/lang/String; = "KeyboardLayoutSet"

.field public static final REQ_NETWORK_CONNECTIVITY:Ljava/lang/String; = "requireNetworkConnectivity"

.field public static final UNTRANSLATABLE_STRING_IN_SUBTYPE_NAME:Ljava/lang/String; = "UntranslatableReplacementStringInSubtypeName"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
