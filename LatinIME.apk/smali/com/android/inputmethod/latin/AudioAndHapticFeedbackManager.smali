.class public final Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;
.super Ljava/lang/Object;
.source "AudioAndHapticFeedbackManager.java"


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

.field private mSoundOn:Z

.field private final mVibratorUtils:Lcom/android/inputmethod/latin/VibratorUtils;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/LatinIME;Lcom/android/inputmethod/latin/SettingsValues;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;
    .param p2    # Lcom/android/inputmethod/latin/SettingsValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-static {p1}, Lcom/android/inputmethod/latin/VibratorUtils;->getInstance(Landroid/content/Context;)Lcom/android/inputmethod/latin/VibratorUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mVibratorUtils:Lcom/android/inputmethod/latin/VibratorUtils;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->reevaluateIfSoundIsOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSoundOn:Z

    return-void
.end method

.method private playKeyClick(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSoundOn:Z

    if-eqz v1, :cond_0

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x5

    :goto_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mFxVolume:F

    invoke-virtual {v1, v0, v2}, Landroid/media/AudioManager;->playSoundEffect(IF)V

    goto :goto_0

    :sswitch_0
    const/4 v0, 0x7

    goto :goto_1

    :sswitch_1
    const/16 v0, 0x8

    goto :goto_1

    :sswitch_2
    const/4 v0, 0x6

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method private reevaluateIfSoundIsOn()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mSoundOn:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public hapticAndAudioFeedback(ILandroid/view/View;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/view/View;

    invoke-virtual {p0, p2}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->vibrate(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->playKeyClick(I)V

    return-void
.end method

.method public onRingerModeChanged()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->reevaluateIfSoundIsOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSoundOn:Z

    return-void
.end method

.method public vibrate(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mVibrateOn:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mKeypressVibrationDuration:I

    if-gez v0, :cond_2

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->performHapticFeedback(II)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mVibratorUtils:Lcom/android/inputmethod/latin/VibratorUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mVibratorUtils:Lcom/android/inputmethod/latin/VibratorUtils;

    iget-object v1, p0, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->mSettingsValues:Lcom/android/inputmethod/latin/SettingsValues;

    iget v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mKeypressVibrationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/latin/VibratorUtils;->vibrate(J)V

    goto :goto_0
.end method
