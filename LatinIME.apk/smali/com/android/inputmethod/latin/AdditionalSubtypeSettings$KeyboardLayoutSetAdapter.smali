.class final Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$KeyboardLayoutSetAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AdditionalSubtypeSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/AdditionalSubtypeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "KeyboardLayoutSetAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$KeyboardLayoutSetItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const v5, 0x1090008

    invoke-direct {p0, p1, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v5, 0x1090009

    invoke-virtual {p0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeLocale;->getPredefinedKeyboardLayoutSet()[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    const-string v5, "zz"

    const/4 v6, 0x0

    invoke-static {v5, v2, v6}, Lcom/android/inputmethod/latin/AdditionalSubtype;->createAdditionalSubtype(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v4

    new-instance v5, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$KeyboardLayoutSetItem;

    invoke-direct {v5, v4}, Lcom/android/inputmethod/latin/AdditionalSubtypeSettings$KeyboardLayoutSetItem;-><init>(Landroid/view/inputmethod/InputMethodSubtype;)V

    invoke-virtual {p0, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
