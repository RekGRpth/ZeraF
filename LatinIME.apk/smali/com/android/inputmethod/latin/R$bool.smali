.class public final Lcom/android/inputmethod/latin/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final config_auto_correction_spacebar_led_enabled:I = 0x7f09000c

.field public static final config_default_next_word_prediction:I = 0x7f090006

.field public static final config_default_popup_preview:I = 0x7f090005

.field public static final config_default_sound_enabled:I = 0x7f090007

.field public static final config_default_vibration_enabled:I = 0x7f090008

.field public static final config_enable_bigram_suggestions_option:I = 0x7f09000b

.field public static final config_enable_show_popup_on_keypress_option:I = 0x7f090003

.field public static final config_enable_show_voice_key_option:I = 0x7f090002

.field public static final config_enable_usability_study_mode_option:I = 0x7f090004

.field public static final config_gesture_input_enabled_by_build_config:I = 0x7f09000d

.field public static final config_show_more_keys_keyboard_at_touched_point:I = 0x7f09000a

.field public static final config_sliding_key_input_enabled:I = 0x7f090009

.field public static final config_use_fullscreen_mode:I = 0x7f090001

.field public static final im_is_default:I = 0x7f090000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
