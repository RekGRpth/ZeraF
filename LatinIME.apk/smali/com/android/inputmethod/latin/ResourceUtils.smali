.class public final Lcom/android/inputmethod/latin/ResourceUtils;
.super Ljava/lang/Object;
.source "ResourceUtils.java"


# static fields
.field private static final HARDWARE_PREFIX:Ljava/lang/String;

.field public static final UNDEFINED_DIMENSION:I = -0x1

.field public static final UNDEFINED_RATIO:F = -1.0f

.field private static final sDeviceOverrideValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/ResourceUtils;->HARDWARE_PREFIX:Ljava/lang/String;

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/ResourceUtils;->sDeviceOverrideValueMap:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v5, v7, Landroid/content/res/Configuration;->orientation:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/android/inputmethod/latin/ResourceUtils;->sDeviceOverrideValueMap:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    move-object v6, p2

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v0, v2

    sget-object v7, Lcom/android/inputmethod/latin/ResourceUtils;->HARDWARE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/android/inputmethod/latin/ResourceUtils;->HARDWARE_PREFIX:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    :cond_0
    sget-object v7, Lcom/android/inputmethod/latin/ResourceUtils;->sDeviceOverrideValueMap:Ljava/util/HashMap;

    invoke-virtual {v7, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget-object v7, Lcom/android/inputmethod/latin/ResourceUtils;->sDeviceOverrideValueMap:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    return-object v7

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # I
    .param p3    # F

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return p3

    :cond_1
    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isFractionValue(Landroid/util/TypedValue;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, p2, p2, p3}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result p3

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isDimensionValue(Landroid/util/TypedValue;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    goto :goto_0
.end method

.method public static getDimensionPixelSize(Landroid/content/res/TypedArray;I)I
    .locals 3
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I

    const/4 v1, -0x1

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isDimensionValue(Landroid/util/TypedValue;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, p1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    goto :goto_0
.end method

.method public static getEnumValue(Landroid/content/res/TypedArray;II)I
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isIntegerValue(Landroid/util/TypedValue;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    goto :goto_0
.end method

.method public static getFraction(Landroid/content/res/TypedArray;I)F
    .locals 1
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I

    const/high16 v0, -0x40800000

    invoke-static {p0, p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    return v0
.end method

.method public static getFraction(Landroid/content/res/TypedArray;IF)F
    .locals 3
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # F

    const/4 v2, 0x1

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isFractionValue(Landroid/util/TypedValue;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-virtual {p0, p1, v2, v2, p2}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result p2

    goto :goto_0
.end method

.method public static isDimensionValue(Landroid/util/TypedValue;)Z
    .locals 2
    .param p0    # Landroid/util/TypedValue;

    iget v0, p0, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFractionValue(Landroid/util/TypedValue;)Z
    .locals 2
    .param p0    # Landroid/util/TypedValue;

    iget v0, p0, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIntegerValue(Landroid/util/TypedValue;)Z
    .locals 2
    .param p0    # Landroid/util/TypedValue;

    iget v0, p0, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget v0, p0, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isStringValue(Landroid/util/TypedValue;)Z
    .locals 2
    .param p0    # Landroid/util/TypedValue;

    iget v0, p0, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidDimensionPixelOffset(I)Z
    .locals 1
    .param p0    # I

    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidDimensionPixelSize(I)Z
    .locals 1
    .param p0    # I

    if-lez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidFraction(F)Z
    .locals 1
    .param p0    # F

    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
