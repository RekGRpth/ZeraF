.class public final Lcom/android/inputmethod/latin/WordComposer;
.super Ljava/lang/Object;
.source "WordComposer.java"


# static fields
.field public static final CAPS_MODE_AUTO_SHIFTED:I = 0x5

.field public static final CAPS_MODE_AUTO_SHIFT_LOCKED:I = 0x7

.field public static final CAPS_MODE_MANUAL_SHIFTED:I = 0x1

.field public static final CAPS_MODE_MANUAL_SHIFT_LOCKED:I = 0x3

.field public static final CAPS_MODE_OFF:I = 0x0

.field private static final N:I = 0x30


# instance fields
.field private mAutoCorrection:Ljava/lang/CharSequence;

.field private mCapitalizedMode:I

.field private mCapsCount:I

.field private mCodePointSize:I

.field private mDigitsCount:I

.field private final mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

.field private mIsBatchMode:Z

.field private mIsFirstCharCapitalized:Z

.field private mIsResumed:Z

.field private mPrimaryKeyCodes:[I

.field private mTrailingSingleQuotesCount:I

.field private final mTypedWord:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x30

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/InputPointers;

    invoke-direct {v0, v2}, Lcom/android/inputmethod/latin/InputPointers;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/latin/WordComposer;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/InputPointers;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/InputPointers;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    iget-object v1, p1, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/InputPointers;->copy(Lcom/android/inputmethod/latin/InputPointers;)V

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    iget v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    iget-boolean v0, p1, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method private addKeyInfo(ILcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v3, v0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v4, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    iget v3, v0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v4, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    div-int/lit8 v4, v4, 0x2

    add-int v2, v3, v4

    :goto_0
    invoke-virtual {p0, p1, v1, v2}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    return-void

    :cond_0
    const/4 v1, -0x1

    const/4 v2, -0x1

    goto :goto_0
.end method

.method private static isFirstCharCapitalized(IIZ)Z
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # Z

    if-nez p0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final refreshSize()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->codePointCount(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCodePointSize:I

    return-void
.end method


# virtual methods
.method public add(III)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/16 v0, 0x30

    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    const/16 v0, 0x20

    if-lt p1, v0, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v0

    :goto_0
    aput v0, v2, v1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/InputPointers;->addPointer(IIIII)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    invoke-static {v1, p1, v0}, Lcom/android/inputmethod/latin/WordComposer;->isFirstCharCapitalized(IIZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->isDigit(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    :cond_2
    const/16 v0, 0x27

    if-ne v0, p1, :cond_4

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void

    :cond_3
    move v0, p1

    goto :goto_0

    :cond_4
    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    goto :goto_1
.end method

.method public commitWord(ILjava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/inputmethod/latin/LastComposedWord;
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/CharSequence;

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    const/16 v2, 0x30

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    new-instance v0, Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    iget-object v3, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/LastComposedWord;-><init>([ILcom/android/inputmethod/latin/InputPointers;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/InputPointers;->reset()V

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    :cond_0
    iput v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iput v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    iput v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-object v0
.end method

.method public deleteLast()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v3, v2, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "In WordComposer: mCodes and mTypedWords have non-matching lengths"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->codePointBefore(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSupplementaryCodePoint(I)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x2

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :goto_0
    invoke-static {v1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    :cond_1
    invoke-static {v1}, Ljava/lang/Character;->isDigit(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    :cond_3
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    :cond_4
    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    if-lez v4, :cond_7

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    :cond_5
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void

    :cond_6
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_7
    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    :goto_1
    if-lez v0, :cond_5

    iget-object v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v5, -0x1

    invoke-virtual {v4, v0, v5}, Ljava/lang/StringBuilder;->offsetByCodePoints(II)I

    move-result v0

    const/16 v4, 0x27

    iget-object v5, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->codePointAt(I)I

    move-result v5

    if-ne v4, v5, :cond_5

    iget v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    goto :goto_1
.end method

.method public getAutoCorrectionOrNull()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCodeAt(I)I
    .locals 1
    .param p1    # I

    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getInputPointers()Lcom/android/inputmethod/latin/InputPointers;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    return-object v0
.end method

.method public getTypedWord()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasDigits()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllUpperCase()Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v2

    if-gt v2, v1, :cond_2

    iget v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v2, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public isBatchMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    return v0
.end method

.method public final isComposingWord()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirstCharCapitalized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    return v0
.end method

.method public isMostlyCaps()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isResumed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return v0
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapsCount:I

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mDigitsCount:I

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsFirstCharCapitalized:Z

    iput v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    return-void
.end method

.method public resumeSuggestionOnLastComposedWord(Lcom/android/inputmethod/latin/LastComposedWord;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v0, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mPrimaryKeyCodes:[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mPrimaryKeyCodes:[I

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    iget-object v1, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/InputPointers;->set(Lcom/android/inputmethod/latin/InputPointers;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTypedWord:Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/WordComposer;->refreshSize()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-void
.end method

.method public setAutoCorrection(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/android/inputmethod/latin/WordComposer;->mAutoCorrection:Ljava/lang/CharSequence;

    return-void
.end method

.method public setBatchInputPointers(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mInputPointers:Lcom/android/inputmethod/latin/InputPointers;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/InputPointers;->set(Lcom/android/inputmethod/latin/InputPointers;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    return-void
.end method

.method public setBatchInputWord(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x1

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsBatchMode:Z

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-virtual {p0, v0, v3, v3}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    invoke-static {p1, v1, v4}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setCapitalizedModeAtStartComposingTime(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    return-void
.end method

.method public setComposingWord(Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/android/inputmethod/latin/WordComposer;->addKeyInfo(ILcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-static {p1, v1, v3}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v1

    goto :goto_0

    :cond_0
    iput-boolean v3, p0, Lcom/android/inputmethod/latin/WordComposer;->mIsResumed:Z

    return-void
.end method

.method public final size()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCodePointSize:I

    return v0
.end method

.method public trailingSingleQuotesCount()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mTrailingSingleQuotesCount:I

    return v0
.end method

.method public wasAutoCapitalized()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public wasShiftedNoLock()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/inputmethod/latin/WordComposer;->mCapitalizedMode:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
