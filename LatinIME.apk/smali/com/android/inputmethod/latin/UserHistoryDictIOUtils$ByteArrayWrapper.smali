.class public final Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;
.super Ljava/lang/Object;
.source "UserHistoryDictIOUtils.java"

# interfaces
.implements Lcom/android/inputmethod/latin/makedict/BinaryDictInputOutput$FusionDictionaryBufferInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/UserHistoryDictIOUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteArrayWrapper"
.end annotation


# instance fields
.field private mBuffer:[B

.field private mPosition:I


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mBuffer:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    return-void
.end method


# virtual methods
.method public capacity()I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mBuffer:[B

    array-length v0, v0

    return v0
.end method

.method public limit()I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mBuffer:[B

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public position()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    return v0
.end method

.method public position(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    return-void
.end method

.method public put(B)V
    .locals 3
    .param p1    # B

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mBuffer:[B

    iget v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    aput-byte p1, v0, v1

    return-void
.end method

.method public readInt()I
    .locals 3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedShort()I

    move-result v0

    shl-int/lit8 v1, v0, 0x10

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedShort()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method public readUnsignedByte()I
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mBuffer:[B

    iget v1, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->mPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public readUnsignedInt24()I
    .locals 3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedShort()I

    move-result v0

    shl-int/lit8 v1, v0, 0x8

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedByte()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method public readUnsignedShort()I
    .locals 3

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedByte()I

    move-result v0

    shl-int/lit8 v1, v0, 0x8

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/UserHistoryDictIOUtils$ByteArrayWrapper;->readUnsignedByte()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method
