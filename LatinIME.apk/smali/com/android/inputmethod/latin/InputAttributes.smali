.class public final Lcom/android/inputmethod/latin/InputAttributes;
.super Ljava/lang/Object;
.source "InputAttributes.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field public final mApplicationSpecifiedCompletionOn:Z

.field private final mInputType:I

.field public final mInputTypeNoAutoCorrect:Z

.field public final mIsSettingsSuggestionStripOn:Z

.field public final mShouldInsertSpacesAutomatically:Z


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 13
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v9, Lcom/android/inputmethod/latin/InputAttributes;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    if-eqz p1, :cond_1

    iget v5, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    :goto_0
    and-int/lit8 v4, v5, 0xf

    iput v5, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputType:I

    if-eq v4, v8, :cond_4

    if-nez p1, :cond_2

    iget-object v8, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v9, "No editor info for this field. Bug?"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mShouldInsertSpacesAutomatically:Z

    :goto_2
    return-void

    :cond_1
    move v5, v7

    goto :goto_0

    :cond_2
    if-nez v5, :cond_3

    iget-object v8, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v9, "InputType.TYPE_NULL is specified"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    if-nez v4, :cond_0

    iget-object v9, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v10, "Unexpected input class: inputType=0x%08x imeOptions=0x%08x"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v7

    iget v12, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    and-int/lit16 v6, v5, 0xff0

    const/high16 v9, 0x80000

    and-int/2addr v9, v5

    if-eqz v9, :cond_8

    move v3, v8

    :goto_3
    const/high16 v9, 0x20000

    and-int/2addr v9, v5

    if-eqz v9, :cond_9

    move v2, v8

    :goto_4
    const v9, 0x8000

    and-int/2addr v9, v5

    if-eqz v9, :cond_a

    move v1, v8

    :goto_5
    const/high16 v9, 0x10000

    and-int/2addr v9, v5

    if-eqz v9, :cond_b

    move v0, v8

    :goto_6
    invoke-static {v5}, Lcom/android/inputmethod/latin/InputTypeUtils;->isPasswordInputType(I)Z

    move-result v9

    if-nez v9, :cond_5

    invoke-static {v5}, Lcom/android/inputmethod/latin/InputTypeUtils;->isVisiblePasswordInputType(I)Z

    move-result v9

    if-nez v9, :cond_5

    invoke-static {v6}, Lcom/android/inputmethod/latin/InputTypeUtils;->isEmailVariation(I)Z

    move-result v9

    if-nez v9, :cond_5

    const/16 v9, 0x10

    if-eq v9, v6, :cond_5

    const/16 v9, 0xb0

    if-eq v9, v6, :cond_5

    if-nez v3, :cond_5

    if-eqz v0, :cond_c

    :cond_5
    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    :goto_7
    invoke-static {v5}, Lcom/android/inputmethod/latin/InputTypeUtils;->isAutoSpaceFriendlyType(I)Z

    move-result v9

    iput-boolean v9, p0, Lcom/android/inputmethod/latin/InputAttributes;->mShouldInsertSpacesAutomatically:Z

    const/16 v9, 0xa0

    if-ne v6, v9, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    if-nez v3, :cond_7

    if-nez v1, :cond_d

    if-nez v2, :cond_d

    :cond_7
    iput-boolean v8, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    :goto_8
    if-eqz v0, :cond_e

    if-eqz p2, :cond_e

    :goto_9
    iput-boolean v8, p0, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    goto/16 :goto_2

    :cond_8
    move v3, v7

    goto :goto_3

    :cond_9
    move v2, v7

    goto :goto_4

    :cond_a
    move v1, v7

    goto :goto_5

    :cond_b
    move v0, v7

    goto :goto_6

    :cond_c
    iput-boolean v8, p0, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    goto :goto_7

    :cond_d
    iput-boolean v7, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    goto :goto_8

    :cond_e
    move v8, v7

    goto :goto_9
.end method

.method private dumpFlags(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "Input class:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    and-int/lit8 v0, p1, 0xf

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_CLASS_TEXT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_CLASS_PHONE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_CLASS_NUMBER"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_CLASS_DATETIME"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "Variation:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    and-int/lit8 v1, p1, 0x20

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_EMAIL_ADDRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    and-int/lit8 v1, p1, 0x30

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_EMAIL_SUBJECT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    and-int/lit16 v1, p1, 0xb0

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_FILTER"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    and-int/lit8 v1, p1, 0x50

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_LONG_MESSAGE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    and-int/lit8 v1, p1, 0x0

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_NORMAL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    and-int/lit16 v1, p1, 0x80

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_PASSWORD"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    and-int/lit8 v1, p1, 0x60

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_PERSON_NAME"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    and-int/lit16 v1, p1, 0xc0

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_PHONETIC"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    and-int/lit8 v1, p1, 0x70

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_POSTAL_ADDRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    and-int/lit8 v1, p1, 0x40

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_SHORT_MESSAGE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_URI"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    and-int/lit16 v1, p1, 0x90

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_VISIBLE_PASSWORD"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    and-int/lit16 v1, p1, 0xa0

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_WEB_EDIT_TEXT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    and-int/lit16 v1, p1, 0xd0

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    and-int/lit16 v1, p1, 0xe0

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_VARIATION_WEB_PASSWORD"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "Flags:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v1, 0x80000

    and-int/2addr v1, p1

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_NO_SUGGESTIONS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    const/high16 v1, 0x20000

    and-int/2addr v1, p1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_MULTI_LINE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    const/high16 v1, 0x40000

    and-int/2addr v1, p1

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_IME_MULTI_LINE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    and-int/lit16 v1, p1, 0x2000

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_CAP_WORDS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    and-int/lit16 v1, p1, 0x4000

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_CAP_SENTENCES"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    and-int/lit16 v1, p1, 0x1000

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_CAP_CHARACTERS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_18
    const v1, 0x8000

    and-int/2addr v1, p1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_AUTO_CORRECT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    const/high16 v1, 0x10000

    and-int/2addr v1, p1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->TAG:Ljava/lang/String;

    const-string v2, "  TYPE_TEXT_FLAG_AUTO_COMPLETE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    return-void
.end method

.method public static inPrivateImeOptions(Ljava/lang/String;Ljava/lang/String;Landroid/view/inputmethod/EditorInfo;)Z
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/inputmethod/EditorInfo;

    if-nez p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    if-eqz p0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p2, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/StringUtils;->containsInCsv(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    :cond_1
    move-object v0, p1

    goto :goto_1
.end method


# virtual methods
.method public isSameInputType(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 2
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputType:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n mInputTypeNoAutoCorrect = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->mInputTypeNoAutoCorrect:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n mIsSettingsSuggestionStripOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->mIsSettingsSuggestionStripOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n mApplicationSpecifiedCompletionOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/InputAttributes;->mApplicationSpecifiedCompletionOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
