.class public final Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;
.super Ljava/lang/Object;
.source "BinaryDictionaryFileDumper.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final DICTIONARY_PROJECTION:[Ljava/lang/String;

.field private static final FILE_READ_BUFFER_SIZE:I = 0x2000

.field private static final MAGIC_NUMBER_VERSION_1:[B

.field private static final MAGIC_NUMBER_VERSION_2:[B

.field public static final QUERY_PARAMETER_DELETE_RESULT:Ljava/lang/String; = "result"

.field public static final QUERY_PARAMETER_FAILURE:Ljava/lang/String; = "failure"

.field public static final QUERY_PARAMETER_MAY_PROMPT_USER:Ljava/lang/String; = "mayPrompt"

.field public static final QUERY_PARAMETER_SUCCESS:Ljava/lang/String; = "success"

.field public static final QUERY_PARAMETER_TRUE:Ljava/lang/String; = "true"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x4

    const-class v0, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->MAGIC_NUMBER_VERSION_1:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->MAGIC_NUMBER_VERSION_2:[B

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->DICTIONARY_PROJECTION:[Ljava/lang/String;

    return-void

    :array_0
    .array-data 1
        0x78t
        -0x4ft
        0x0t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        -0x65t
        -0x3ft
        0x3at
        -0x2t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cacheWordList(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Landroid/content/Context;)Lcom/android/inputmethod/latin/AssetFileAddress;
    .locals 34
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v4, 0x2

    const/4 v6, 0x3

    const/4 v8, 0x4

    const/4 v11, 0x5

    const/4 v10, 0x0

    const/4 v9, 0x5

    invoke-static/range {p0 .. p0}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->getProviderUriBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/BinaryDictionaryGetter;->getCacheFileName(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/BinaryDictionaryGetter;->getTempFileName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    const/16 v20, 0x0

    :goto_0
    const/16 v30, 0x5

    move/from16 v0, v20

    move/from16 v1, v30

    if-gt v0, v1, :cond_17

    const/16 v21, 0x0

    const/16 v19, 0x0

    const/16 v27, 0x0

    const/4 v15, 0x0

    const/4 v13, 0x0

    const/16 v22, 0x0

    const/16 v24, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v29 .. v29}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v28

    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->openAssetFileDescriptor(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    if-nez v12, :cond_5

    const/16 v30, 0x0

    if-eqz v19, :cond_0

    :try_start_1
    #Replaced unresolvable odex instruction with a throw
    throw v19

    :cond_0
    if-eqz v27, :cond_1

    #Replaced unresolvable odex instruction with a throw
    throw v27

    :cond_1
    if-eqz v15, :cond_2

    #Replaced unresolvable odex instruction with a throw
    throw v15

    :cond_2
    if-eqz v13, :cond_3

    #Replaced unresolvable odex instruction with a throw
    throw v13
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    :cond_3
    :goto_1
    if-eqz v24, :cond_4

    :try_start_2
    #Replaced unresolvable odex instruction with a throw
    throw v24
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    :cond_4
    :goto_2
    return-object v30

    :cond_5
    :try_start_3
    invoke-virtual {v12}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v21

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    new-instance v25, Ljava/io/FileOutputStream;

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    packed-switch v20, :pswitch_data_0

    :goto_3
    :try_start_5
    new-instance v14, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    move-object/from16 v0, v25

    invoke-static {v14, v0}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->checkMagicAndCopyFileTo(Ljava/io/BufferedInputStream;Ljava/io/FileOutputStream;)V

    invoke-virtual/range {v25 .. v25}, Ljava/io/OutputStream;->flush()V

    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V

    new-instance v17, Ljava/io/File;

    invoke-direct/range {v17 .. v18}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v30

    if-nez v30, :cond_c

    new-instance v30, Ljava/io/IOException;

    const-string v31, "Can\'t move the file to its final name"

    invoke-direct/range {v30 .. v31}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v30
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_0
    move-exception v16

    move-object/from16 v24, v25

    move-object/from16 v22, v23

    move-object v13, v14

    :goto_4
    if-eqz v22, :cond_6

    :try_start_7
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->delete()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_6
    if-eqz v19, :cond_7

    :try_start_8
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    :cond_7
    if-eqz v27, :cond_8

    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V

    :cond_8
    if-eqz v15, :cond_9

    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    :cond_9
    if-eqz v13, :cond_a

    invoke-virtual {v13}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    :cond_a
    :goto_5
    if-eqz v24, :cond_b

    :try_start_9
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :cond_b
    :goto_6
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    :pswitch_0
    :try_start_a
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/FileTransforms;->getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/android/inputmethod/latin/FileTransforms;->getDecryptedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v15

    invoke-static {v15}, Lcom/android/inputmethod/latin/FileTransforms;->getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v19

    goto :goto_3

    :pswitch_1
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/FileTransforms;->getDecryptedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v15

    invoke-static {v15}, Lcom/android/inputmethod/latin/FileTransforms;->getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v19

    goto :goto_3

    :pswitch_2
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/FileTransforms;->getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/android/inputmethod/latin/FileTransforms;->getDecryptedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v19

    goto :goto_3

    :pswitch_3
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/FileTransforms;->getUncompressedStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v19

    goto :goto_3

    :pswitch_4
    invoke-static/range {v21 .. v21}, Lcom/android/inputmethod/latin/FileTransforms;->getDecryptedStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v19

    goto :goto_3

    :pswitch_5
    move-object/from16 v19, v21

    goto :goto_3

    :cond_c
    :try_start_b
    const-string v30, "result"

    const-string v31, "success"

    invoke-virtual/range {v29 .. v31}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual/range {v29 .. v29}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v30

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    move-object/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v30

    if-gtz v30, :cond_d

    sget-object v30, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    const-string v31, "Could not have the dictionary pack delete a word list"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/android/inputmethod/latin/BinaryDictionaryGetter;->removeFilesWithIdExcept(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    invoke-static/range {v18 .. v18}, Lcom/android/inputmethod/latin/AssetFileAddress;->makeFromFileName(Ljava/lang/String;)Lcom/android/inputmethod/latin/AssetFileAddress;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-result-object v30

    if-eqz v19, :cond_e

    :try_start_c
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    :cond_e
    if-eqz v27, :cond_f

    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V

    :cond_f
    if-eqz v15, :cond_10

    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    :cond_10
    if-eqz v14, :cond_11

    invoke-virtual {v14}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    :cond_11
    :goto_7
    if-eqz v25, :cond_4

    :try_start_d
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    :goto_8
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catchall_0
    move-exception v30

    :goto_9
    if-eqz v19, :cond_12

    :try_start_e
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    :cond_12
    if-eqz v27, :cond_13

    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V

    :cond_13
    if-eqz v15, :cond_14

    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    :cond_14
    if-eqz v13, :cond_15

    invoke-virtual {v13}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    :cond_15
    :goto_a
    if-eqz v24, :cond_16

    :try_start_f
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_2

    :cond_16
    :goto_b
    throw v30

    :cond_17
    sget-object v30, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    const-string v31, "Could not copy a word list. Will not be able to use it."

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v30, "result"

    const-string v31, "failure"

    invoke-virtual/range {v29 .. v31}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual/range {v29 .. v29}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v30

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    move-object/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v30

    if-gtz v30, :cond_18

    sget-object v30, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    const-string v31, "In addition, we were unable to delete it."

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_18
    const/16 v30, 0x0

    goto/16 :goto_2

    :catch_2
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    :catch_3
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file descriptor : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :catch_4
    move-exception v16

    sget-object v30, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Exception while closing a file : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :catch_5
    move-exception v16

    sget-object v30, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Exception while closing a file descriptor : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :catch_6
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_8

    :catch_7
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file descriptor : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_8
    move-exception v16

    sget-object v31, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception while closing a file descriptor : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :catchall_1
    move-exception v30

    move-object/from16 v22, v23

    goto/16 :goto_9

    :catchall_2
    move-exception v30

    move-object/from16 v24, v25

    move-object/from16 v22, v23

    goto/16 :goto_9

    :catchall_3
    move-exception v30

    move-object/from16 v24, v25

    move-object/from16 v22, v23

    move-object v13, v14

    goto/16 :goto_9

    :catch_9
    move-exception v16

    goto/16 :goto_4

    :catch_a
    move-exception v16

    move-object/from16 v22, v23

    goto/16 :goto_4

    :catch_b
    move-exception v16

    move-object/from16 v24, v25

    move-object/from16 v22, v23

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static cacheWordListsFromContentProvider(Ljava/util/Locale;Landroid/content/Context;Z)Ljava/util/List;
    .locals 8
    .param p0    # Ljava/util/Locale;
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/android/inputmethod/latin/AssetFileAddress;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {p0, p1, p2}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->getWordListWordListInfos(Ljava/util/Locale;Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v4

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/latin/WordListInfo;

    iget-object v6, v3, Lcom/android/inputmethod/latin/WordListInfo;->mId:Ljava/lang/String;

    iget-object v7, v3, Lcom/android/inputmethod/latin/WordListInfo;->mLocale:Ljava/lang/String;

    invoke-static {v6, v7, v5, p1}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->cacheWordList(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Landroid/content/Context;)Lcom/android/inputmethod/latin/AssetFileAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static checkMagicAndCopyFileTo(Ljava/io/BufferedInputStream;Ljava/io/FileOutputStream;)V
    .locals 7
    .param p0    # Ljava/io/BufferedInputStream;
    .param p1    # Ljava/io/FileOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    sget-object v5, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->MAGIC_NUMBER_VERSION_2:[B

    array-length v1, v5

    new-array v2, v1, [B

    invoke-virtual {p0, v2, v6, v1}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v4

    if-ge v4, v1, :cond_0

    new-instance v5, Ljava/io/IOException;

    const-string v6, "Less bytes to read than the magic number length"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    sget-object v5, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->MAGIC_NUMBER_VERSION_2:[B

    invoke-static {v5, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->MAGIC_NUMBER_VERSION_1:[B

    invoke-static {v5, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Ljava/io/IOException;

    const-string v6, "Wrong magic number for downloaded file"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    const/16 v5, 0x2000

    new-array v0, v5, [B

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    :goto_0
    if-ltz v3, :cond_2

    invoke-virtual {p1, v0, v6, v3}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/io/BufferedInputStream;->close()V

    return-void
.end method

.method private static getProviderUriBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.inputmethod.latin.dictionarypack"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static getWordListWordListInfos(Ljava/util/Locale;Landroid/content/Context;Z)Ljava/util/List;
    .locals 12
    .param p0    # Ljava/util/Locale;
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/android/inputmethod/latin/WordListInfo;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->getProviderUriBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    if-nez p2, :cond_0

    const-string v2, "mayPrompt"

    const-string v4, "true"

    invoke-virtual {v6, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->DICTIONARY_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    :goto_0
    return-object v9

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    goto :goto_0

    :cond_3
    :try_start_0
    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    :cond_4
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    sget-object v2, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception communicating with the dictionary pack : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    goto :goto_0

    :cond_5
    :try_start_1
    new-instance v2, Lcom/android/inputmethod/latin/WordListInfo;

    invoke-direct {v2, v10, v11}, Lcom/android/inputmethod/latin/WordListInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private static openAssetFileDescriptor(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    .locals 3
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    :try_start_0
    const-string v1, "r"

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/inputmethod/latin/BinaryDictionaryFileDumper;->TAG:Ljava/lang/String;

    const-string v2, "Could not find a word list from the dictionary provider."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method
