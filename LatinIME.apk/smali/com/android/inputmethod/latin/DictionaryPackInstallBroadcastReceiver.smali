.class public final Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DictionaryPackInstallBroadcastReceiver.java"


# static fields
.field static final NEW_DICTIONARY_INTENT_ACTION:Ljava/lang/String; = "com.android.inputmethod.latin.dictionarypack.newdict"


# instance fields
.field final mService:Lcom/android/inputmethod/latin/LatinIME;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;->mService:Lcom/android/inputmethod/latin/LatinIME;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v11, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v9}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {v8}, Lcom/android/inputmethod/latin/TargetApplicationGetter;->removeApplicationInfoCache(Ljava/lang/String;)V

    const/16 v11, 0x8

    :try_start_0
    invoke-virtual {v6, v8, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    iget-object v10, v7, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    if-eqz v10, :cond_0

    move-object v1, v10

    array-length v5, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v4, v1, v3

    const-string v11, "com.android.inputmethod.latin.dictionarypack"

    iget-object v12, v4, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v11}, Lcom/android/inputmethod/latin/LatinIME;->resetSuggestMainDict()V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const-string v11, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    const-string v11, "android.intent.extra.REPLACING"

    const/4 v12, 0x0

    invoke-virtual {p2, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, p0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v11}, Lcom/android/inputmethod/latin/LatinIME;->resetSuggestMainDict()V

    goto :goto_0

    :cond_4
    const-string v11, "com.android.inputmethod.latin.dictionarypack.newdict"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;->mService:Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v11}, Lcom/android/inputmethod/latin/LatinIME;->resetSuggestMainDict()V

    goto :goto_0
.end method
