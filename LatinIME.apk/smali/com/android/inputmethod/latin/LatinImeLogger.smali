.class public final Lcom/android/inputmethod/latin/LatinImeLogger;
.super Ljava/lang/Object;
.source "LatinImeLogger.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static sDBG:Z

.field public static sUsabilityStudy:Z

.field public static sVISUALDEBUG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    sput-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static commit()V
    .locals 0

    return-void
.end method

.method public static init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/content/SharedPreferences;

    return-void
.end method

.method public static logOnAutoCorrectionCancelled()V
    .locals 0

    return-void
.end method

.method public static logOnAutoCorrectionForGeometric(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/InputPointers;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/latin/InputPointers;

    return-void
.end method

.method public static logOnAutoCorrectionForTyping(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    return-void
.end method

.method public static logOnDelete(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    return-void
.end method

.method public static logOnException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Throwable;

    return-void
.end method

.method public static logOnInputChar()V
    .locals 0

    return-void
.end method

.method public static logOnInputSeparator()V
    .locals 0

    return-void
.end method

.method public static logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/android/inputmethod/latin/SuggestedWords;

    return-void
.end method

.method public static logOnWarning(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    return-void
.end method

.method public static onAddSuggestedWord(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public static onDestroy()V
    .locals 0

    return-void
.end method

.method public static onPrintAllUsabilityStudyLogs()V
    .locals 0

    return-void
.end method

.method public static onSetKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/keyboard/Keyboard;

    return-void
.end method

.method public static onStartInputView(Landroid/view/inputmethod/EditorInfo;)V
    .locals 0
    .param p0    # Landroid/view/inputmethod/EditorInfo;

    return-void
.end method

.method public static onStartSuggestion(Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    return-void
.end method
