.class public Lcom/android/inputmethod/keyboard/KeyboardView;
.super Landroid/view/View;
.source "KeyboardView.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;
    }
.end annotation


# static fields
.field private static final KEY_LABEL_REFERENCE_CHAR:[C

.field private static final KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

.field private static final KEY_PREVIEW_BACKGROUND_DEFAULT_STATE:[I

.field private static final KEY_PREVIEW_BACKGROUND_STATE_TABLE:[[[I

.field private static final LABEL_ICON_MARGIN:F = 0.05f

.field private static final MAX_LABEL_RATIO:F = 0.9f

.field private static final POPUP_HINT_CHAR:Ljava/lang/String; = "\u2026"

.field private static final PREVIEW_ALPHA:I = 0xf0

.field private static final STATE_HAS_MOREKEYS:I = 0x1

.field private static final STATE_LEFT:I = 0x1

.field private static final STATE_MIDDLE:I = 0x0

.field private static final STATE_NORMAL:I = 0x0

.field private static final STATE_RIGHT:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static final sTextHeightCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTextWidthCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBackgroundDimAlpha:I

.field private final mClipRegion:Landroid/graphics/Region;

.field private final mCoordinates:[I

.field private mDelayAfterPreview:I

.field private final mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

.field private final mFontMetrics:Landroid/graphics/Paint$FontMetrics;

.field private mInvalidateAllKeys:Z

.field private final mInvalidatedKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field protected final mKeyBackground:Landroid/graphics/drawable/Drawable;

.field protected final mKeyBackgroundPadding:Landroid/graphics/Rect;

.field protected final mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

.field private final mKeyHintLetterPadding:F

.field private final mKeyLabelHorizontalPadding:I

.field private final mKeyPopupHintLetterPadding:F

.field protected final mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;

.field private final mKeyPreviewLayoutId:I

.field private final mKeyPreviewTexts:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyShiftedLetterHintPadding:F

.field private final mKeyTextShadowRadius:F

.field protected final mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

.field private mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

.field protected final mMoreKeysLayout:I

.field private mNeedsToDimEntireKeyboard:Z

.field private mOffscreenBuffer:Landroid/graphics/Bitmap;

.field private final mOffscreenCanvas:Landroid/graphics/Canvas;

.field private final mPaint:Landroid/graphics/Paint;

.field private final mPreviewHeight:I

.field private final mPreviewLingerTimeout:I

.field private final mPreviewOffset:I

.field private final mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

.field private mShowKeyPreviewPopup:Z

.field private final mTextBounds:Landroid/graphics/Rect;

.field protected final mVerticalCorrection:F

.field private final mWorkingRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-class v0, Lcom/android/inputmethod/keyboard/KeyboardView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->TAG:Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [[[I

    new-array v1, v6, [[I

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardView;->EMPTY_STATE_SET:[I

    aput-object v2, v1, v4

    new-array v2, v5, [I

    const v3, 0x7f010014

    aput v3, v2, v4

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v5, [I

    const v3, 0x7f010012

    aput v3, v2, v4

    aput-object v2, v1, v4

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v5, [I

    const v3, 0x7f010013

    aput v3, v2, v4

    aput-object v2, v1, v4

    new-array v2, v6, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_PREVIEW_BACKGROUND_STATE_TABLE:[[[I

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_PREVIEW_BACKGROUND_STATE_TABLE:[[[I

    aget-object v0, v0, v4

    aget-object v0, v0, v4

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_PREVIEW_BACKGROUND_DEFAULT_STATE:[I

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newSparseArray()Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Landroid/util/SparseArray;

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newSparseArray()Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Landroid/util/SparseArray;

    new-array v0, v5, [C

    const/16 v1, 0x4d

    aput-char v1, v0, v4

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    new-array v0, v5, [C

    const/16 v1, 0x38

    aput-char v1, v0, v4

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

    return-void

    :array_0
    .array-data 4
        0x7f010012
        0x7f010014
    .end array-data

    :array_1
    .array-data 4
        0x7f010013
        0x7f010014
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010001

    invoke-direct {p0, p1, p2, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v8, 0x7f0f0004

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackgroundPadding:Landroid/graphics/Rect;

    new-instance v2, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-direct {v2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    new-array v2, v7, [I

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCoordinates:[I

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newSparseArray()Landroid/util/SparseArray;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    new-instance v2, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;

    invoke-direct {v2}, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;

    iput-boolean v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mWorkingRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Region;

    invoke-direct {v2}, Landroid/graphics/Region;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mClipRegion:Landroid/graphics/Region;

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenCanvas:Landroid/graphics/Canvas;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint$FontMetrics;

    invoke-direct {v2}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    new-instance v2, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    invoke-direct {v2, p0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;-><init>(Lcom/android/inputmethod/keyboard/KeyboardView;)V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    sget-object v2, Lcom/android/inputmethod/latin/R$styleable;->KeyboardView:[I

    invoke-virtual {p1, p2, v2, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackgroundPadding:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewOffset:I

    const/16 v2, 0xb

    const/16 v3, 0x50

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewHeight:I

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewLingerTimeout:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewLingerTimeout:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    invoke-virtual {v1, v6, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyLabelHorizontalPadding:I

    invoke-virtual {v1, v7, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyHintLetterPadding:F

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPopupHintLetterPadding:F

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyShiftedLetterHintPadding:F

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyTextShadowRadius:F

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    if-nez v2, :cond_0

    iput-boolean v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    :cond_0
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mVerticalCorrection:F

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mMoreKeysLayout:I

    const/16 v2, 0xf

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBackgroundDimAlpha:I

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v2, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    invoke-virtual {p1, p2, v2, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->newInstance(Landroid/content/res/TypedArray;)Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-direct {v2, p1, p2}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/keyboard/KeyboardView;)Landroid/util/SparseArray;
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/KeyboardView;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    return-object v0
.end method

.method private addKeyPreview(Landroid/widget/TextView;)V
    .locals 3
    .param p1    # Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->locatePreviewPlacerView()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-static {v1, v2, v2}, Lcom/android/inputmethod/keyboard/ViewLayoutUtils;->newLayoutParam(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private static blendAlpha(Landroid/graphics/Paint;I)V
    .locals 5
    .param p0    # Landroid/graphics/Paint;
    .param p1    # I

    invoke-virtual {p0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    mul-int/2addr v1, p1

    div-int/lit16 v1, v1, 0xff

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    return-void
.end method

.method private dismissAllKeyPreviews()V
    .locals 4

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/inputmethod/keyboard/PointerTracker;->setReleasedKeyGraphicsToAllKeys()V

    return-void
.end method

.method private static drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected static drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .locals 3
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v2, v2, p4, p5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # Landroid/graphics/Paint;

    const/4 v1, 0x0

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p6, p5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0, p1, p2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object v0, p0

    move v2, v1

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    neg-float v0, p1

    neg-float v1, p2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V
    .locals 6
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p1

    move v4, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private freeOffscreenBuffer()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method private static getCharGeometryCacheKey(CLandroid/graphics/Paint;)I
    .locals 4
    .param p0    # C
    .param p1    # Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    float-to-int v2, v3

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    shl-int/lit8 v0, p0, 0xf

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_0

    add-int v3, v0, v2

    :goto_0
    return v3

    :cond_0
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_1

    add-int v3, v0, v2

    add-int/lit16 v3, v3, 0x1000

    goto :goto_0

    :cond_1
    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    if-ne v1, v3, :cond_2

    add-int v3, v0, v2

    add-int/lit16 v3, v3, 0x2000

    goto :goto_0

    :cond_2
    add-int v3, v0, v2

    goto :goto_0
.end method

.method private getCharHeight([CLandroid/graphics/Paint;)F
    .locals 6
    .param p1    # [C
    .param p2    # Landroid/graphics/Paint;

    const/4 v5, 0x0

    aget-char v3, p1, v5

    invoke-static {v3, p2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharGeometryCacheKey(CLandroid/graphics/Paint;)I

    move-result v2

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v5, v3, v4}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v1, v3

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextHeightCache:Landroid/util/SparseArray;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private getCharWidth([CLandroid/graphics/Paint;)F
    .locals 6
    .param p1    # [C
    .param p2    # Landroid/graphics/Paint;

    const/4 v5, 0x0

    aget-char v3, p1, v5

    invoke-static {v3, p2}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharGeometryCacheKey(CLandroid/graphics/Paint;)I

    move-result v1

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v5, v3, v4}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v2, v3

    sget-object v3, Lcom/android/inputmethod/keyboard/KeyboardView;->sTextWidthCache:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private getKeyPreviewText(I)Landroid/widget/TextView;
    .locals 6
    .param p1    # I

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    move-object v2, v1

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    if-eqz v3, :cond_1

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewLayoutId:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :goto_1
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewTexts:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v2, v1

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    goto :goto_1
.end method

.method private isSpecialChar(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :cond_0
    sparse-switch v0, :sswitch_data_0

    :goto_0
    return v1

    :sswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x981 -> :sswitch_0
        0x982 -> :sswitch_0
        0x9be -> :sswitch_0
        0x9bf -> :sswitch_0
        0x9c0 -> :sswitch_0
        0x9c1 -> :sswitch_0
        0x9c2 -> :sswitch_0
        0x9c3 -> :sswitch_0
        0x9c7 -> :sswitch_0
        0x9c8 -> :sswitch_0
        0x9cb -> :sswitch_0
        0x9cc -> :sswitch_0
    .end sparse-switch
.end method

.method private locatePreviewPlacerView()V
    .locals 9

    const/4 v8, 0x1

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    const/4 v6, 0x2

    new-array v3, v6, [I

    invoke-virtual {p0, v3}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    aget v6, v3, v8

    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v7, v7, 0x4

    if-lt v6, v7, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardView;->TAG:Ljava/lang/String;

    const-string v7, "Cannot find root view"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const v6, 0x1020002

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    if-nez v5, :cond_3

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardView;->TAG:Ljava/lang/String;

    const-string v7, "Cannot find android.R.id.content view to add PreviewPlacerView"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    const/4 v7, 0x0

    aget v7, v3, v7

    aget v8, v3, v8

    invoke-virtual {v6, v7, v8, v4, v1}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->setKeyboardViewGeometry(IIII)V

    goto :goto_0
.end method

.method private maybeAllocateOffscreenBuffer()Z
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v3, v1, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v3, v0, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->freeOffscreenBuffer()V

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 7
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->getDrawX()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    add-int v1, v5, v6

    iget v5, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    add-int v2, v5, v6

    int-to-float v5, v1

    int-to-float v6, v2

    invoke-virtual {p2, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget v5, v5, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget v6, v6, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    sub-int v3, v5, v6

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/Key;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {v5, v3, v0}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mayCloneAndUpdateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    move-result-object v4

    const/16 v5, 0xff

    iput v5, v4, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyBackground(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;)V

    :cond_0
    invoke-virtual {p0, p1, p2, p3, v4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    neg-int v5, v1

    int-to-float v5, v5

    neg-int v6, v2

    int-to-float v6, v6

    invoke-virtual {p2, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private onDrawKeyboard(Landroid/graphics/Canvas;)V
    .locals 19
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_1
    const/4 v10, 0x1

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v13

    if-nez v10, :cond_2

    if-eqz v13, :cond_7

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mClipRegion:Landroid/graphics/Region;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v3, v4, v0, v11}, Landroid/graphics/Region;->set(IIII)Z

    :cond_3
    if-nez v13, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mClipRegion:Landroid/graphics/Region;

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipRegion(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    const/high16 v2, -0x1000000

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    if-eqz v9, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    if-nez v10, :cond_5

    if-eqz v13, :cond_9

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v8, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v15, v8

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v15, :cond_b

    aget-object v14, v8, v12

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v14, v1, v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_6
    const/4 v10, 0x0

    goto :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mClipRegion:Landroid/graphics/Region;

    invoke-virtual {v2}, Landroid/graphics/Region;->setEmpty()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v2, v14}, Lcom/android/inputmethod/keyboard/Keyboard;->hasKey(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget v2, v14, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int v17, v2, v3

    iget v2, v14, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int v18, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mWorkingRect:Landroid/graphics/Rect;

    iget v3, v14, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int v3, v3, v17

    iget v4, v14, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int v4, v4, v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mClipRegion:Landroid/graphics/Region;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mWorkingRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_a
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {v2, v14}, Lcom/android/inputmethod/keyboard/Keyboard;->hasKey(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v14, v1, v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKey(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eqz v2, :cond_c

    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mBackgroundDimAlpha:I

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v16

    int-to-float v5, v0

    int-to-float v6, v11

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->cancelAllMessages()V

    return-void
.end method

.method public closing()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->dismissAllKeyPreviews()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->cancelAllMessages()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public dimEntireKeyboard(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eq v1, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mNeedsToDimEntireKeyboard:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateAllKeys()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissGestureFloatingPreviewText()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->locatePreviewPlacerView()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->dismissGestureFloatingPreviewText()V

    return-void
.end method

.method public dismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->dismissKeyPreview(JLcom/android/inputmethod/keyboard/PointerTracker;)V

    return-void
.end method

.method public dismissMoreKeysPanel()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    const v8, -0x3f7f8000

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->getDrawWidth()I

    move-result v3

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget-object v5, p4, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v5, p4, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    int-to-float v5, v5

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v5, p4, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    int-to-float v5, v3

    iget v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyHintLetterPadding:F

    sub-float/2addr v5, v6

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    invoke-direct {p0, v6, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    sub-float v0, v5, v6

    int-to-float v5, v2

    iget v6, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPopupHintLetterPadding:F

    sub-float v1, v5, v6

    const-string v5, "\u2026"

    invoke-virtual {p2, v5, v0, v1, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-boolean v5, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v5, :cond_0

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    float-to-int v5, v1

    int-to-float v5, v5

    int-to-float v6, v3

    invoke-static {p2, v5, v6, v8, v4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    float-to-int v5, v0

    int-to-float v5, v5

    int-to-float v6, v2

    invoke-static {p2, v5, v6, v8, v4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    return-object v0
.end method

.method public getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Paint;

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v0, v1, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public invalidateAllKeys()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-boolean v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int v0, v2, v3

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int v1, v2, v3

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int/2addr v2, v0

    iget v3, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int/2addr v3, v1

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0
.end method

.method public isKeyPreviewPopupEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    return v0
.end method

.method public newDefaultLabelPaint()Landroid/graphics/Paint;
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget-object v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->freeOffscreenBuffer()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyboard(Landroid/graphics/Canvas;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidatedKeys:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_1
    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    :cond_2
    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->maybeAllocateOffscreenBuffer()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mInvalidateAllKeys:Z

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyboard(Landroid/graphics/Canvas;)V

    :cond_4
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mOffscreenBuffer:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onDrawKeyBackground(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;)V
    .locals 16
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackgroundPadding:Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->getDrawWidth()I

    move-result v1

    iget v2, v15, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v2, v15, Landroid/graphics/Rect;->right:I

    add-int v10, v1, v2

    move-object/from16 v0, p1

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v2, v15, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v2, v15, Landroid/graphics/Rect;->bottom:I

    add-int v9, v1, v2

    iget v1, v15, Landroid/graphics/Rect;->left:I

    neg-int v11, v1

    iget v1, v15, Landroid/graphics/Rect;->top:I

    neg-int v12, v1

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->getCurrentDrawableState()[I

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v14}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v13

    iget v1, v13, Landroid/graphics/Rect;->right:I

    if-ne v10, v1, :cond_0

    iget v1, v13, Landroid/graphics/Rect;->bottom:I

    if-eq v9, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v8, v1, v2, v10, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    int-to-float v1, v11

    int-to-float v2, v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget-boolean v1, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v10

    int-to-float v5, v9

    const/high16 v6, -0x7f400000

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v7}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_2
    neg-int v1, v11

    int-to-float v1, v1

    neg-int v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method protected onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V
    .locals 37
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->getDrawWidth()I

    move-result v31

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    move/from16 v30, v0

    move/from16 v0, v31

    int-to-float v2, v0

    const/high16 v4, 0x3f000000

    mul-float v28, v2, v4

    move/from16 v0, v30

    int-to-float v2, v0

    const/high16 v4, 0x3f000000

    mul-float v29, v2, v4

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v31

    int-to-float v5, v0

    move/from16 v0, v30

    int-to-float v6, v0

    const v7, -0x7fffff40

    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v8}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v2, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    move-object/from16 v0, p4

    iget v4, v0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lcom/android/inputmethod/keyboard/Key;->getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    move/from16 v6, v28

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectTypeface(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)Landroid/graphics/Typeface;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I

    move-result v36

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->isSpecialChar(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    move/from16 v34, v36

    :goto_0
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharHeight([CLandroid/graphics/Paint;)F

    move-result v32

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v33

    const/high16 v2, 0x40000000

    div-float v2, v32, v2

    add-float v7, v29, v2

    const/16 v35, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeft()Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyLabelHorizontalPadding:I

    int-to-float v6, v2

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->needsXScale()Z

    move-result v2

    if-eqz v2, :cond_1

    const/high16 v2, 0x3f800000

    move/from16 v0, v31

    int-to-float v4, v0

    const v5, 0x3f666666

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v5

    div-float/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextScaleX(F)V

    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectTextColor(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyTextShadowRadius:F

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    iget v8, v0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextShadowColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4, v5, v8}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    :goto_2
    move-object/from16 v0, p4

    iget v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->blendAlpha(Landroid/graphics/Paint;I)V

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v2, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4, v5, v8}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    const/high16 v2, 0x3f800000

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextScaleX(F)V

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    sub-int v2, v30, v13

    div-int/lit8 v11, v2, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconLeft()Z

    move-result v2

    if-eqz v2, :cond_e

    const/high16 v2, 0x40000000

    div-float v2, v35, v2

    sub-float v2, v28, v2

    float-to-int v10, v2

    move-object/from16 v8, p2

    invoke-static/range {v8 .. v13}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    :cond_2
    :goto_3
    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v2, :cond_3

    new-instance v26, Landroid/graphics/Paint;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v31

    int-to-float v2, v0

    const v4, -0x3fff8000

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v7, v2, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    move/from16 v0, v30

    int-to-float v2, v0

    const v4, -0x3f7fff80

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v6, v2, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectHintTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->selectHintTextColor(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p4

    iget v2, v0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->blendAlpha(Landroid/graphics/Paint;I)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasHintLabel()Z

    move-result v2

    if-eqz v2, :cond_f

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v2

    const/high16 v4, 0x40000000

    mul-float/2addr v2, v4

    add-float v18, v6, v2

    sget-object v2, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharHeight([CLandroid/graphics/Paint;)F

    move-result v2

    const/high16 v4, 0x40000000

    div-float/2addr v2, v4

    add-float v19, v29, v2

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :goto_4
    const/16 v16, 0x0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v17

    move-object/from16 v14, p2

    move-object/from16 v20, p3

    invoke-virtual/range {v14 .. v20}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v2, :cond_4

    new-instance v26, Landroid/graphics/Paint;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v19

    float-to-int v2, v0

    int-to-float v2, v2

    move/from16 v0, v31

    int-to-float v4, v0

    const v5, -0x3f7f8000

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v2, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawHorizontalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    move/from16 v0, v18

    float-to-int v2, v0

    int-to-float v2, v2

    move/from16 v0, v30

    int-to-float v4, v0

    const v5, -0x3f7f8000

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v2, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    if-nez v2, :cond_5

    if-eqz v9, :cond_5

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    move/from16 v0, v31

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    sub-int v2, v30, v13

    div-int/lit8 v11, v2, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeft()Z

    move-result v2

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyLabelHorizontalPadding:I

    move/from16 v27, v10

    :goto_5
    move-object/from16 v8, p2

    invoke-static/range {v8 .. v13}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    sget-boolean v2, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v2, :cond_5

    new-instance v26, Landroid/graphics/Paint;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Paint;-><init>()V

    move/from16 v0, v27

    int-to-float v2, v0

    move/from16 v0, v30

    int-to-float v4, v0

    const v5, -0x3f7fff80

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-static {v0, v2, v4, v5, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawVerticalLine(Landroid/graphics/Canvas;FFILandroid/graphics/Paint;)V

    int-to-float v0, v10

    move/from16 v21, v0

    int-to-float v0, v11

    move/from16 v22, v0

    int-to-float v0, v12

    move/from16 v23, v0

    int-to-float v0, v13

    move/from16 v24, v0

    const/high16 v25, -0x7f400000

    move-object/from16 v20, p2

    invoke-static/range {v20 .. v26}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawRectangle(Landroid/graphics/Canvas;FFFFILandroid/graphics/Paint;)V

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasPopupHint()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    if-eqz v2, :cond_6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    array-length v2, v2

    if-lez v2, :cond_6

    invoke-virtual/range {p0 .. p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    :cond_6
    return-void

    :cond_7
    move/from16 v0, v36

    int-to-double v4, v0

    const-wide v16, 0x3fe4cccccccccccdL

    mul-double v4, v4, v16

    double-to-int v0, v4

    move/from16 v34, v0

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignRight()Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyLabelHorizontalPadding:I

    sub-int v2, v31, v2

    int-to-float v6, v2

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignLeftOfCenter()Z

    move-result v2

    if-eqz v2, :cond_a

    const/high16 v2, 0x40e00000

    mul-float v2, v2, v33

    const/high16 v4, 0x40800000

    div-float/2addr v2, v4

    sub-float v6, v28, v2

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconLeft()Z

    move-result v2

    if-eqz v2, :cond_b

    if-eqz v9, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v2

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    const v4, 0x3d4ccccd

    move/from16 v0, v31

    int-to-float v5, v0

    mul-float/2addr v4, v5

    add-float v35, v2, v4

    const/high16 v2, 0x40000000

    div-float v2, v35, v2

    add-float v6, v28, v2

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconRight()Z

    move-result v2

    if-eqz v2, :cond_c

    if-eqz v9, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v2

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    const v4, 0x3d4ccccd

    move/from16 v0, v31

    int-to-float v5, v0

    mul-float/2addr v4, v5

    add-float v35, v2, v4

    const/high16 v2, 0x40000000

    div-float v2, v35, v2

    sub-float v6, v28, v2

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_1

    :cond_c
    move/from16 v6, v28

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_1

    :cond_d
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_2

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasLabelWithIconRight()Z

    move-result v2

    if-eqz v2, :cond_2

    const/high16 v2, 0x40000000

    div-float v2, v35, v2

    add-float v2, v2, v28

    int-to-float v4, v12

    sub-float/2addr v2, v4

    float-to-int v10, v2

    move-object/from16 v8, p2

    invoke-static/range {v8 .. v13}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    goto/16 :goto_3

    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v2

    if-eqz v2, :cond_10

    move/from16 v0, v31

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyShiftedLetterHintPadding:F

    sub-float/2addr v2, v4

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v4

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float v18, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mFontMetrics:Landroid/graphics/Paint$FontMetrics;

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v2

    move/from16 v19, v0

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_4

    :cond_10
    move/from16 v0, v31

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyHintLetterPadding:F

    sub-float/2addr v2, v4

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_NUMERIC_HINT_LABEL_REFERENCE_CHAR:[C

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getCharWidth([CLandroid/graphics/Paint;)F

    move-result v4

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float v18, v2, v4

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    neg-float v0, v2

    move/from16 v19, v0

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_4

    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/Key;->isAlignRight()Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyLabelHorizontalPadding:I

    sub-int v2, v31, v2

    sub-int v10, v2, v12

    add-int v27, v10, v12

    goto/16 :goto_5

    :cond_12
    sub-int v2, v31, v12

    div-int/lit8 v10, v2, 0x2

    div-int/lit8 v2, v12, 0x2

    add-int v27, v10, v2

    goto/16 :goto_5
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    iget v1, v1, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int v0, v1, v2

    invoke-virtual {p0, p1, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public purgeKeyboardAndClosing()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    return-void
.end method

.method public setGesturePreviewMode(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->setGesturePreviewMode(ZZ)V

    return-void
.end method

.method public setKeyPreviewPopupEnabled(ZI)V
    .locals 0
    .param p1    # Z
    .param p2    # I

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    iput p2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDelayAfterPreview:I

    return-void
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->onSetKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateAllKeys()V

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->updateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget-object v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->updateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)V

    return-void
.end method

.method public showGestureFloatingPreviewText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->locatePreviewPlacerView()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->setGestureFloatingPreviewText(Ljava/lang/String;)V

    return-void
.end method

.method public showGesturePreviewTrail(Lcom/android/inputmethod/keyboard/PointerTracker;Z)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;
    .param p2    # Z

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->locatePreviewPlacerView()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewPlacerView:Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->invalidatePointer(Lcom/android/inputmethod/keyboard/PointerTracker;Z)V

    return-void
.end method

.method public showKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V
    .locals 21
    .param p1    # Lcom/android/inputmethod/keyboard/PointerTracker;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mShowKeyPreviewPopup:Z

    move/from16 v17, v0

    if-nez v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    move/from16 v17, v0

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v11, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;->mPreviewVisibleOffset:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyPreviewText(I)Landroid/widget/TextView;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    if-nez v17, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/inputmethod/keyboard/KeyboardView;->addKeyPreview(Landroid/widget/TextView;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mDrawingHandler:Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView$DrawingHandler;->cancelDismissKeyPreview(Lcom/android/inputmethod/keyboard/PointerTracker;)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKey()Lcom/android/inputmethod/keyboard/Key;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget v0, v5, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextColor:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v12}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_3

    sget-object v17, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_PREVIEW_BACKGROUND_DEFAULT_STATE:[I

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    const/16 v17, 0xf0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_3
    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v17

    if-eqz v17, :cond_5

    iget-object v9, v7, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    :goto_1
    if-eqz v9, :cond_7

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-static {v9}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_6

    const/16 v17, 0x0

    iget v0, v5, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    sget-object v17, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :goto_2
    invoke-virtual {v12, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    const/16 v17, -0x2

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/Key;->getDrawWidth()I

    move-result v8

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewHeight:I

    invoke-virtual {v12}, Landroid/view/View;->getPaddingLeft()I

    move-result v17

    sub-int v17, v13, v17

    invoke-virtual {v12}, Landroid/view/View;->getPaddingRight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v11, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;->mPreviewVisibleWidth:I

    invoke-virtual {v12}, Landroid/view/View;->getPaddingTop()I

    move-result v17

    sub-int v17, v10, v17

    invoke-virtual {v12}, Landroid/view/View;->getPaddingBottom()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v11, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;->mPreviewVisibleHeight:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewOffset:I

    move/from16 v17, v0

    invoke-virtual {v12}, Landroid/view/View;->getPaddingBottom()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v11, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;->mPreviewVisibleOffset:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCoordinates:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/Key;->getDrawX()I

    move-result v17

    sub-int v18, v13, v8

    div-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCoordinates:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget v18, v18, v19

    add-int v14, v17, v18

    if-gez v14, :cond_8

    const/4 v14, 0x0

    const/16 v16, 0x1

    :goto_4
    iget v0, v7, Lcom/android/inputmethod/keyboard/Key;->mY:I

    move/from16 v17, v0

    sub-int v17, v17, v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mPreviewOffset:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mCoordinates:[I

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget v18, v18, v19

    add-int v15, v17, v18

    if-eqz v4, :cond_4

    iget-object v0, v7, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    const/4 v6, 0x1

    :goto_5
    sget-object v17, Lcom/android/inputmethod/keyboard/KeyboardView;->KEY_PREVIEW_BACKGROUND_STATE_TABLE:[[[I

    aget-object v17, v17, v16

    aget-object v17, v17, v6

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_4
    invoke-static {v12, v14, v15, v13, v10}, Lcom/android/inputmethod/keyboard/ViewLayoutUtils;->placeViewAt(Landroid/view/View;IIII)V

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v9, v7, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    const/16 v17, 0x0

    iget v0, v5, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v7, v5}, Lcom/android/inputmethod/keyboard/Key;->selectTypeface(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)Landroid/graphics/Typeface;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_2

    :cond_7
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyboard:Lcom/android/inputmethod/keyboard/Keyboard;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/android/inputmethod/keyboard/Key;->getPreviewIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v17

    sub-int v17, v17, v13

    move/from16 v0, v17

    if-le v14, v0, :cond_9

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v17

    sub-int v14, v17, v13

    const/16 v16, 0x2

    goto/16 :goto_4

    :cond_9
    const/16 v16, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v6, 0x0

    goto :goto_5
.end method
