.class public final Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;
.super Lcom/android/inputmethod/keyboard/Keyboard;
.source "MoreKeysKeyboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$MoreKeyDivider;,
        Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;,
        Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$MoreKeysKeyboardParams;
    }
.end annotation


# instance fields
.field private final mDefaultKeyCoordX:I


# direct methods
.method constructor <init>(Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$MoreKeysKeyboardParams;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$MoreKeysKeyboardParams;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/Keyboard;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$MoreKeysKeyboardParams;->getDefaultKeyCoordX()I

    move-result v0

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mDefaultKeyWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;->mDefaultKeyCoordX:I

    return-void
.end method


# virtual methods
.method public getDefaultCoordX()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;->mDefaultKeyCoordX:I

    return v0
.end method
