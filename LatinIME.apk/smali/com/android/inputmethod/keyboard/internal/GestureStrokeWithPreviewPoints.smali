.class public final Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;
.super Lcom/android/inputmethod/keyboard/internal/GestureStroke;
.source "GestureStrokeWithPreviewPoints.java"


# static fields
.field private static final MIN_PREVIEW_SAMPLE_LENGTH_RATIO_TO_KEY_WIDTH:F = 0.1f

.field public static final PREVIEW_CAPACITY:I = 0x100


# instance fields
.field private mLastPreviewSize:I

.field private mLastX:I

.field private mLastY:I

.field private mMinPreviewSampleLengthSquare:I

.field private final mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mPreviewXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mPreviewYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private mStrokeId:I


# direct methods
.method public constructor <init>(ILcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    const/16 v1, 0x100

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;-><init>(ILcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;)V

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    return-void
.end method

.method private needsSampling(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastX:I

    sub-int v0, p1, v2

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastY:I

    sub-int v1, p2, v2

    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mMinPreviewSampleLengthSquare:I

    if-lt v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addPoint(IIIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->addPoint(IIIZ)V

    if-nez p4, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->needsSampling(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p2}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iput p1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastX:I

    iput p2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastY:I

    :cond_1
    return-void
.end method

.method public appendPreviewStroke(Lcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p2    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p3    # Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    sub-int v0, v1, v2

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    invoke-virtual {p1, v1, v2, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    invoke-virtual {p2, v1, v2, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    invoke-virtual {p3, v1, v2, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    goto :goto_0
.end method

.method public getGestureStrokeId()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mStrokeId:I

    return v0
.end method

.method public getGestureStrokePreviewSize()I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v0

    return v0
.end method

.method protected reset()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->reset()V

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mStrokeId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mStrokeId:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mLastPreviewSize:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mPreviewYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    return-void
.end method

.method public setKeyboardGeometry(I)V
    .locals 3
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->setKeyboardGeometry(I)V

    int-to-float v1, p1

    const v2, 0x3dcccccd

    mul-float v0, v1, v2

    mul-float v1, v0, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStrokeWithPreviewPoints;->mMinPreviewSampleLengthSquare:I

    return-void
.end method
