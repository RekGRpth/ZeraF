.class public final Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;
.super Ljava/lang/Object;
.source "PointerTrackerQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final INITIAL_CAPACITY:I = 0xa

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mArraySize:I

.field private final mExpandableArrayOfActivePointers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    return-void
.end method


# virtual methods
.method public declared-synchronized add(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_0
    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getOldestElement()Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasModifierKeyOlderThan(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;)Z
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_1
    monitor-exit p0

    return v4

    :cond_1
    :try_start_1
    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;->isModifier()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized isAnyInSlidingKeyInput()Z
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    invoke-interface {v1}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;->isInSlidingKeyInput()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    monitor-exit p0

    return v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public releaseAllPointers(J)V
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->releaseAllPointersExcept(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;J)V

    return-void
.end method

.method public declared-synchronized releaseAllPointersExcept(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;J)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    if-ne v2, p1, :cond_2

    if-lez v1, :cond_0

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found duplicated element in releaseAllPointersExcept: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    if-eq v5, v4, :cond_1

    invoke-virtual {v3, v5, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v5, v5, 0x1

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v2, p2, p3}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;->onPhantomUpEvent(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_3
    :try_start_1
    iput v5, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized releaseAllPointersOlderThan(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;J)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v4, v0, :cond_0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    if-ne v2, p1, :cond_4

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v4, v0, :cond_7

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    if-ne v2, p1, :cond_2

    if-lez v1, :cond_1

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found duplicated element in releaseAllPointersOlderThan: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    add-int/lit8 v1, v1, 0x1

    :cond_2
    if-eq v5, v4, :cond_3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v2}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;->isModifier()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-interface {v2, p2, p3}, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;->onPhantomUpEvent(J)V

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    if-eq v5, v4, :cond_6

    invoke-virtual {v3, v5, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_7
    iput v5, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized remove(Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;)V
    .locals 8
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    if-ne v1, p1, :cond_1

    if-eq v4, v3, :cond_0

    sget-object v5, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found duplicated element in remove: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eq v4, v3, :cond_2

    invoke-virtual {v2, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    iput v4, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized size()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 7

    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mExpandableArrayOfActivePointers:Ljava/util/ArrayList;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue;->mArraySize:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/PointerTrackerQueue$Element;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_0

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    monitor-exit p0

    return-object v5

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method
