.class final Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;
.super Lcom/android/inputmethod/keyboard/internal/KeyStyle;
.source "KeyStylesSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DeclaredKeyStyle"
.end annotation


# instance fields
.field private final mParentStyleName:Ljava/lang/String;

.field private final mStyleAttributes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mStyles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/keyboard/internal/KeyStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;Ljava/util/HashMap;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/inputmethod/keyboard/internal/KeyStyle;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)V

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newSparseArray()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyles:Ljava/util/HashMap;

    return-void
.end method

.method private readFlag(Landroid/content/res/TypedArray;I)V
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v1, 0x0

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_0
    or-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private readInt(Landroid/content/res/TypedArray;I)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private readString(Landroid/content/res/TypedArray;I)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private readStringArray(Landroid/content/res/TypedArray;I)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getFlag(Landroid/content/res/TypedArray;I)I
    .locals 5
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {v3, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    or-int/2addr v0, v3

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyles:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/internal/KeyStyle;

    invoke-virtual {v1, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v3

    or-int/2addr v3, v0

    return v3
.end method

.method public getInt(Landroid/content/res/TypedArray;II)I
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyles:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v2

    goto :goto_0
.end method

.method public getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->parseString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyles:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyle;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->parseStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyleAttributes:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mStyles:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->mParentStyleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/KeyStyle;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public readKeyAttributes(Landroid/content/res/TypedArray;)V
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readStringArray(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readStringArray(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readFlag(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xb

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xc

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xd

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readString(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readInt(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readInt(Landroid/content/res/TypedArray;I)V

    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet$DeclaredKeyStyle;->readFlag(Landroid/content/res/TypedArray;I)V

    return-void
.end method
