.class final Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView$DrawingHandler;
.super Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;
.source "PreviewPlacerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DrawingHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper",
        "<",
        "Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final MSG_DISMISS_GESTURE_FLOATING_PREVIEW_TEXT:I = 0x0

.field private static final MSG_UPDATE_GESTURE_PREVIEW_TRAIL:I = 0x1


# instance fields
.field private final mGestureFloatingPreviewTextLingerTimeout:I

.field private final mGesturePreviewTrailParams:Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;I)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView$DrawingHandler;->mGesturePreviewTrailParams:Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;

    iput p3, p0, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView$DrawingHandler;->mGestureFloatingPreviewTextLingerTimeout:I

    return-void
.end method


# virtual methods
.method public dismissGestureFloatingPreviewText()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView$DrawingHandler;->mGestureFloatingPreviewTextLingerTimeout:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/StaticInnerHandlerWrapper;->getOuterInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView;->setGestureFloatingPreviewText(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public postUpdateGestureTrailPreview()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/PreviewPlacerView$DrawingHandler;->mGesturePreviewTrailParams:Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GesturePreviewTrail$Params;->mUpdateInterval:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
