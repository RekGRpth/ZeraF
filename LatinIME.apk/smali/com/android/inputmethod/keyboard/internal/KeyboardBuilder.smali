.class public Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;
.super Ljava/lang/Object;
.source "KeyboardBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KP:",
        "Lcom/android/inputmethod/keyboard/internal/KeyboardParams;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final BUILDER_TAG:Ljava/lang/String; = "Keyboard.Builder"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_KEYBOARD_COLUMNS:I = 0xa

.field private static final DEFAULT_KEYBOARD_ROWS:I = 0x4

.field private static final SPACES:Ljava/lang/String; = "                                             "

.field private static final TAG_CASE:Ljava/lang/String; = "case"

.field private static final TAG_DEFAULT:Ljava/lang/String; = "default"

.field private static final TAG_INCLUDE:Ljava/lang/String; = "include"

.field private static final TAG_KEY:Ljava/lang/String; = "Key"

.field private static final TAG_KEYBOARD:Ljava/lang/String; = "Keyboard"

.field public static final TAG_KEY_STYLE:Ljava/lang/String; = "key-style"

.field private static final TAG_MERGE:Ljava/lang/String; = "merge"

.field private static final TAG_ROW:Ljava/lang/String; = "Row"

.field private static final TAG_SPACER:Ljava/lang/String; = "Spacer"

.field private static final TAG_SWITCH:Ljava/lang/String; = "switch"


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mCurrentRow:Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

.field private mCurrentY:I

.field private final mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mIndent:I

.field private mLeftEdge:Z

.field protected final mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKP;"
        }
    .end annotation
.end field

.field protected final mResources:Landroid/content/res/Resources;

.field private mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

.field private mTopEdge:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TKP;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentRow:Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p2, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->GRID_WIDTH:I

    const v1, 0x7f0a0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p2, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->GRID_HEIGHT:I

    return-void
.end method

.method private addEdgeSpace(FLcom/android/inputmethod/keyboard/internal/KeyboardRow;)V
    .locals 1
    .param p1    # F
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    invoke-virtual {p2, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->advanceXPos(F)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mLeftEdge:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    return-void
.end method

.method private static booleanAttr(Landroid/content/res/TypedArray;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, " %s=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v3

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private endKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->onAddKey(Lcom/android/inputmethod/keyboard/Key;)V

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mLeftEdge:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/keyboard/Key;->markAsLeftEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mLeftEdge:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mTopEdge:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/keyboard/Key;->markAsTopEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    :cond_1
    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    return-void
.end method

.method private endKeyboard()V
    .locals 0

    return-void
.end method

.method private endRow(Lcom/android/inputmethod/keyboard/internal/KeyboardRow;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentRow:Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/InflateException;

    const-string v1, "orphan end row tag"

    invoke-direct {v0, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/Key;->markAsRightEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    int-to-float v0, v0

    invoke-direct {p0, v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->addEdgeSpace(FLcom/android/inputmethod/keyboard/internal/KeyboardRow;)V

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->mRowHeight:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    iput-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentRow:Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mTopEdge:Z

    return-void
.end method

.method private varargs endTag(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const-string v0, "Keyboard.Builder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->spaces(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static matchBoolean(Landroid/content/res/TypedArray;IZ)Z
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-ne v1, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static matchInteger(Landroid/content/res/TypedArray;II)Z
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    if-ne v1, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static matchString(Landroid/content/res/TypedArray;ILjava/lang/String;)Z
    .locals 2
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/android/inputmethod/latin/StringUtils;->containsInArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static matchTypedValue(Landroid/content/res/TypedArray;IILjava/lang/String;)Z
    .locals 4
    .param p0    # Landroid/content/res/TypedArray;
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isIntegerValue(Landroid/util/TypedValue;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    if-eq p2, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/inputmethod/latin/ResourceUtils;->isStringValue(Landroid/util/TypedValue;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\|"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/android/inputmethod/latin/StringUtils;->containsInArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private parseCase(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)Z
    .locals 2
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseCaseCondition(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez p2, :cond_1

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    :goto_1
    return v0

    :cond_0
    move p3, v1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    :goto_2
    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_1

    :cond_2
    move p3, v1

    goto :goto_2
.end method

.method private parseCaseCondition(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 23
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    if-nez v7, :cond_0

    const/16 v18, 0x1

    :goto_0
    return v18

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    invoke-static/range {p1 .. p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v21

    sget-object v22, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Case:[I

    invoke-virtual/range {v20 .. v22}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    const/16 v20, 0x0

    :try_start_0
    iget v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    move/from16 v21, v0

    iget v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/inputmethod/keyboard/KeyboardId;->elementIdToName(I)Ljava/lang/String;

    move-result-object v22

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v3, v0, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchTypedValue(Landroid/content/res/TypedArray;IILjava/lang/String;)Z

    move-result v10

    const/16 v20, 0x1

    iget v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mMode:I

    move/from16 v21, v0

    iget v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mMode:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/inputmethod/keyboard/KeyboardId;->modeName(I)Ljava/lang/String;

    move-result-object v22

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v3, v0, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchTypedValue(Landroid/content/res/TypedArray;IILjava/lang/String;)Z

    move-result v14

    const/16 v20, 0x2

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardId;->navigateNext()Z

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v15

    const/16 v20, 0x3

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardId;->navigatePrevious()Z

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v16

    const/16 v20, 0x4

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardId;->passwordInput()Z

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v17

    const/16 v20, 0x5

    iget-boolean v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mClobberSettingsKey:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v4

    const/16 v20, 0x6

    iget-boolean v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mShortcutKeyEnabled:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v19

    const/16 v20, 0x7

    iget-boolean v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mHasShortcutKey:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v6

    const/16 v20, 0x8

    iget-boolean v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mLanguageSwitchKeyEnabled:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v12

    const/16 v20, 0x9

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardId;->isMultiLine()Z

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchBoolean(Landroid/content/res/TypedArray;IZ)Z

    move-result v9

    const/16 v20, 0xa

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardId;->imeAction()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchInteger(Landroid/content/res/TypedArray;II)Z

    move-result v8

    const/16 v20, 0xb

    iget-object v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchString(Landroid/content/res/TypedArray;ILjava/lang/String;)Z

    move-result v13

    const/16 v20, 0xc

    iget-object v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchString(Landroid/content/res/TypedArray;ILjava/lang/String;)Z

    move-result v11

    const/16 v20, 0xd

    iget-object v0, v7, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->matchString(Landroid/content/res/TypedArray;ILjava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v10, :cond_1

    if-eqz v14, :cond_1

    if-eqz v15, :cond_1

    if-eqz v16, :cond_1

    if-eqz v17, :cond_1

    if-eqz v4, :cond_1

    if-eqz v19, :cond_1

    if-eqz v6, :cond_1

    if-eqz v12, :cond_1

    if-eqz v9, :cond_1

    if-eqz v8, :cond_1

    if-eqz v13, :cond_1

    if-eqz v11, :cond_1

    if-eqz v5, :cond_1

    const/16 v18, 0x1

    :goto_1
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0

    :cond_1
    const/16 v18, 0x0

    goto :goto_1

    :catchall_0
    move-exception v20

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v20
.end method

.method private parseDefault(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)Z
    .locals 1
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0
.end method

.method private parseIncludeInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 11
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p3, :cond_0

    const-string v8, "include"

    invoke-static {v8, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    sget-object v9, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Include:[I

    invoke-virtual {v8, v0, v9}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    sget-object v9, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    invoke-virtual {v8, v0, v9}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    :try_start_0
    const-string v9, "keyboardLayout"

    const-string v10, "include"

    invoke-static {v2, v8, v9, v10, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkAttributeExists(Landroid/content/res/TypedArray;ILjava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    if-eqz p2, :cond_3

    const/16 v8, 0x12

    invoke-virtual {v1, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p2, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getKeyX(Landroid/content/res/TypedArray;)F

    move-result v8

    invoke-virtual {p2, v8}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setXPos(F)V

    :cond_1
    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getDefaultKeyWidth()F

    move-result v7

    const/16 v8, 0x11

    invoke-virtual {v1, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p2, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getKeyWidth(Landroid/content/res/TypedArray;)F

    move-result v8

    invoke-virtual {p2, v8}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyWidth(F)V

    :cond_2
    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getDefaultKeyLabelFlags()I

    move-result v6

    const/16 v8, 0xa

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    or-int/2addr v8, v6

    invoke-virtual {p2, v8}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyLabelFlags(I)V

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getDefaultBackgroundType()I

    move-result v5

    const/4 v8, 0x5

    invoke-virtual {v1, v8, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    invoke-virtual {p2, v8}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultBackgroundType(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    const-string v8, "include"

    invoke-static {v8, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    :try_start_1
    invoke-direct {p0, v4, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseMerge(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p2, :cond_4

    invoke-virtual {p2, v7}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyWidth(F)V

    invoke-virtual {p2, v6}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyLabelFlags(I)V

    invoke-virtual {p2, v5}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultBackgroundType(I)V

    :cond_4
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v8

    :catchall_1
    move-exception v8

    if-eqz p2, :cond_5

    invoke-virtual {p2, v7}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyWidth(F)V

    invoke-virtual {p2, v6}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultKeyLabelFlags(I)V

    invoke-virtual {p2, v5}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setDefaultBackgroundType(I)V

    :cond_5
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    throw v8
.end method

.method private parseIncludeKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V
    .locals 1
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseIncludeInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    return-void
.end method

.method private parseIncludeRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 0
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseIncludeInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    return-void
.end method

.method private parseKey(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 3
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p3, :cond_0

    const-string v1, "Key"

    invoke-static {v1, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/inputmethod/keyboard/Key;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-direct {v0, v1, v2, p2, p1}, Lcom/android/inputmethod/keyboard/Key;-><init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Lorg/xmlpull/v1/XmlPullParser;)V

    const-string v1, "Key"

    invoke-static {v1, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->endKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method

.method private parseKeyStyle(Lorg/xmlpull/v1/XmlPullParser;Z)V
    .locals 5
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v3

    sget-object v4, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_KeyStyle:[I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v3

    sget-object v4, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;

    const-string v3, "<key-style/> needs styleName attribute"

    invoke-direct {v2, v3, p1}, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v2

    :cond_0
    if-nez p2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v2, v2, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;

    invoke-virtual {v2, v1, v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;->parseKeyStyleAttributes(Landroid/content/res/TypedArray;Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const-string v2, "key-style"

    invoke-static {v2, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    return-void
.end method

.method private parseKeyboard(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Keyboard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboardAttributes(Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->startKeyboard()V

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    :cond_1
    return-void

    :cond_2
    new-instance v2, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;

    const-string v3, "Keyboard"

    invoke-direct {v2, p1, v3}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v2
.end method

.method private parseKeyboardAttributes(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 22
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {p1 .. p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v18

    sget-object v19, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    const/high16 v20, 0x7f010000

    const v21, 0x7f0f0003

    invoke-virtual/range {v17 .. v21}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    invoke-static/range {p1 .. p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v18

    sget-object v19, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    invoke-virtual/range {v17 .. v19}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0e000f

    const/16 v19, 0x0

    invoke-static/range {v17 .. v19}, Lcom/android/inputmethod/latin/ResourceUtils;->getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v18, v0

    mul-float v9, v17, v18

    :goto_0
    const/16 v17, 0x3

    div-int/lit8 v18, v4, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v8, v0, v4, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v13

    const/16 v17, 0x4

    div-int/lit8 v18, v4, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v8, v0, v4, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v14

    const/16 v17, 0x0

    cmpg-float v17, v14, v17

    if-gez v17, :cond_0

    const/16 v17, 0x4

    div-int/lit8 v18, v5, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v8, v0, v5, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    neg-float v14, v0

    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-static {v9, v13}, Ljava/lang/Math;->min(FF)F

    move-result v17

    move/from16 v0, v17

    invoke-static {v0, v14}, Ljava/lang/Math;->max(FF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedWidth:I

    const/16 v17, 0x5

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTopPadding:I

    const/16 v17, 0x6

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBottomPadding:I

    const/16 v17, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedWidth:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedWidth:I

    move/from16 v17, v0

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    move/from16 v18, v0

    mul-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalCenterPadding:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    const/16 v17, 0x11

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    move/from16 v18, v0

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0xa

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v7, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mDefaultKeyWidth:I

    const/16 v17, 0x9

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalGap:I

    const/16 v17, 0xa

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mVerticalGap:I

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    move/from16 v17, v0

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTopPadding:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBottomPadding:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mVerticalGap:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseHeight:I

    const/16 v17, 0x8

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseHeight:I

    move/from16 v18, v0

    iget v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseHeight:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x4

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mDefaultRowHeight:I

    invoke-static {v7}, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->newInstance(Landroid/content/res/TypedArray;)Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    const/16 v17, 0xb

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v17

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMoreKeysTemplate:I

    const/16 v17, 0x4

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v17

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxMoreKeysKeyboardColumn:I

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v17

    move/from16 v0, v17

    iput v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mThemeId:I

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->loadIcons(Landroid/content/res/TypedArray;)V

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;->setLanguage(Ljava/lang/String;)V

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;->setLanguage(Ljava/lang/String;)V

    new-instance v6, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder$1;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v15}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder$1;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v17

    if-eqz v17, :cond_3

    const/4 v12, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v6, v0, v12}, Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;->runInLocale(Landroid/content/res/Resources;Ljava/util/Locale;)Ljava/lang/Object;

    const/16 v17, 0x1

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v16

    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->load([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_2
    const/16 v17, 0x2

    :try_start_1
    div-int/lit8 v18, v4, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v9

    goto/16 :goto_0

    :cond_3
    iget-object v0, v15, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v12, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v17

    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    throw v17
.end method

.method private parseKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V
    .locals 5
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_7

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Row"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseRowAttributes(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    move-result-object v1

    if-nez p2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->startRow(Lcom/android/inputmethod/keyboard/internal/KeyboardRow;)V

    :cond_1
    invoke-direct {p0, p1, v1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_2
    const-string v3, "include"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseIncludeKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    goto :goto_0

    :cond_3
    const-string v3, "switch"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseSwitchKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    goto :goto_0

    :cond_4
    const-string v3, "key-style"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyStyle(Lorg/xmlpull/v1/XmlPullParser;Z)V

    goto :goto_0

    :cond_5
    new-instance v3, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;

    const-string v4, "Row"

    invoke-direct {v3, p1, v4}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v3

    :cond_6
    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Keyboard"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->endKeyboard()V

    :cond_7
    return-void

    :cond_8
    const-string v3, "case"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "merge"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    new-instance v3, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;

    const-string v4, "Row"

    invoke-direct {v3, p1, v4}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v3
.end method

.method private parseMerge(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "merge"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p2, :cond_2

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;

    const-string v3, "Included keyboard layout must have <merge> root element"

    invoke-direct {v2, v3, p1}, Lcom/android/inputmethod/latin/XmlParseUtils$ParseException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    throw v2
.end method

.method private parseRowAttributes(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .locals 5
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    sget-object v3, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0x9

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalAttribute;

    const-string v2, "horizontalGap"

    invoke-direct {v1, p1, v2}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalAttribute;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1

    :cond_0
    const/16 v1, 0xa

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalAttribute;

    const-string v2, "verticalGap"

    invoke-direct {v1, p1, v2}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalAttribute;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v1, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;-><init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Lorg/xmlpull/v1/XmlPullParser;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method

.method private parseRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Key"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKey(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_1
    const-string v2, "Spacer"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseSpacer(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_2
    const-string v2, "include"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseIncludeRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_3
    const-string v2, "switch"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseSwitchRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    goto :goto_0

    :cond_4
    const-string v2, "key-style"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0, p1, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyStyle(Lorg/xmlpull/v1/XmlPullParser;Z)V

    goto :goto_0

    :cond_5
    new-instance v2, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;

    const-string v3, "Key"

    invoke-direct {v2, p1, v3}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v2

    :cond_6
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Row"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez p3, :cond_7

    invoke-direct {p0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->endRow(Lcom/android/inputmethod/keyboard/internal/KeyboardRow;)V

    :cond_7
    return-void

    :cond_8
    const-string v2, "case"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "default"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "merge"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;

    const-string v3, "Key"

    invoke-direct {v2, p1, v3}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v2
.end method

.method private parseSpacer(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 3
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p3, :cond_0

    const-string v1, "Spacer"

    invoke-static {v1, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/inputmethod/keyboard/Key$Spacer;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-direct {v0, v1, v2, p2, p1}, Lcom/android/inputmethod/keyboard/Key$Spacer;-><init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Lorg/xmlpull/v1/XmlPullParser;)V

    const-string v1, "Spacer"

    invoke-static {v1, p1}, Lcom/android/inputmethod/latin/XmlParseUtils;->checkEndTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->endKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method

.method private parseSwitchInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 5
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v4, :cond_6

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "case"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_1

    move v3, v4

    :goto_1
    invoke-direct {p0, p1, p2, v3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseCase(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)Z

    move-result v3

    or-int/2addr v1, v3

    goto :goto_0

    :cond_1
    move v3, p3

    goto :goto_1

    :cond_2
    const-string v3, "default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v1, :cond_3

    move v3, v4

    :goto_2
    invoke-direct {p0, p1, p2, v3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseDefault(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)Z

    move-result v3

    or-int/2addr v1, v3

    goto :goto_0

    :cond_3
    move v3, p3

    goto :goto_2

    :cond_4
    new-instance v3, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;

    const-string v4, "Key"

    invoke-direct {v3, p1, v4}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalStartTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v3

    :cond_5
    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "switch"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    return-void

    :cond_7
    new-instance v3, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;

    const-string v4, "Key"

    invoke-direct {v3, p1, v4}, Lcom/android/inputmethod/latin/XmlParseUtils$IllegalEndTag;-><init>(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v3
.end method

.method private parseSwitchKeyboardContent(Lorg/xmlpull/v1/XmlPullParser;Z)V
    .locals 1
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseSwitchInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    return-void
.end method

.method private parseSwitchRowContent(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V
    .locals 0
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseSwitchInternal(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Z)V

    return-void
.end method

.method private static spaces(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    const-string v0, "                                             "

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p0, v0, :cond_0

    const-string v0, "                                             "

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "                                             "

    goto :goto_0
.end method

.method private varargs startEndTag(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const-string v0, "Keyboard.Builder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->spaces(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    return-void
.end method

.method private startKeyboard()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTopPadding:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentY:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mTopEdge:Z

    return-void
.end method

.method private startRow(Lcom/android/inputmethod/keyboard/internal/KeyboardRow;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    int-to-float v0, v0

    invoke-direct {p0, v0, p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->addEdgeSpace(FLcom/android/inputmethod/keyboard/internal/KeyboardRow;)V

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mCurrentRow:Lcom/android/inputmethod/keyboard/internal/KeyboardRow;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mLeftEdge:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mRightEdgeKey:Lcom/android/inputmethod/keyboard/Key;

    return-void
.end method

.method private varargs startTag(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const-string v0, "Keyboard.Builder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mIndent:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->spaces(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static textAttr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    const-string v0, " %s=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 2

    new-instance v0, Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/Keyboard;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    return-object v0
.end method

.method public disableTouchPositionCorrectionDataForTest()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->setEnabled(Z)V

    return-void
.end method

.method public load(ILcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;
    .locals 5
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/inputmethod/keyboard/KeyboardId;",
            ")",
            "Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder",
            "<TKP;>;"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iput-object p2, v2, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->parseKeyboard(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    return-object p0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "Keyboard.Builder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyboard XML parse error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    throw v2

    :catch_1
    move-exception v0

    :try_start_2
    const-string v2, "Keyboard.Builder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyboard XML parse error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public setAutoGenerate(Lcom/android/inputmethod/keyboard/internal/KeysCache;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeysCache;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iput-object p1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    return-void
.end method

.method public setProximityCharsCorrectionEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->mParams:Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iput-boolean p1, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mProximityCharsCorrectionEnabled:Z

    return-void
.end method
