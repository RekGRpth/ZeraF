.class public Lcom/android/inputmethod/keyboard/internal/GestureStroke;
.super Ljava/lang/Object;
.source "GestureStroke.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEBUG_SPEED:Z = false

.field public static final DEFAULT_CAPACITY:I = 0x80

.field private static final MSEC_PER_SEC:I = 0x3e8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAfterFastTyping:Z

.field private mDetectFastMoveSpeedThreshold:I

.field private mDetectFastMoveTime:I

.field private mDetectFastMoveX:I

.field private mDetectFastMoveY:I

.field private final mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private mGestureDynamicDistanceThresholdFrom:I

.field private mGestureDynamicDistanceThresholdTo:I

.field private mGestureRecognitionSpeedThreshold:I

.field private mGestureSamplingMinimumDistance:I

.field private mIncrementalRecognitionSize:I

.field private mKeyWidth:I

.field private mLastIncrementalBatchSize:I

.field private mLastMajorEventTime:J

.field private mLastMajorEventX:I

.field private mLastMajorEventY:I

.field private final mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

.field private final mPointerId:I

.field private final mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    const/16 v1, 0x80

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iput p1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mPointerId:I

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    return-void
.end method

.method private appendBatchPoints(Lcom/android/inputmethod/latin/InputPointers;I)V
    .locals 7
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;
    .param p2    # I

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastIncrementalBatchSize:I

    sub-int v6, p2, v0

    if-gtz v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mPointerId:I

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget v5, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastIncrementalBatchSize:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Lcom/android/inputmethod/latin/InputPointers;->append(ILcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iput p2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastIncrementalBatchSize:I

    goto :goto_0
.end method

.method private appendPoint(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p2}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    return-void
.end method

.method private detectFastMove(III)I
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v8, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v2

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v8, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v3

    invoke-static {v2, v3, p1, p2}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getDistance(IIII)I

    move-result v0

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v8, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v8

    sub-int v4, p3, v8

    if-lez v4, :cond_0

    invoke-static {v2, v3, p1, p2}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getDistance(IIII)I

    move-result v5

    mul-int/lit16 v6, v5, 0x3e8

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->hasDetectedFastMove()Z

    move-result v8

    if-nez v8, :cond_0

    iget v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveSpeedThreshold:I

    mul-int/2addr v8, v4

    if-le v6, v8, :cond_0

    iput p3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveTime:I

    iput p1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveX:I

    iput p2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveY:I

    :cond_0
    return v0
.end method

.method private static getDistance(IIII)I
    .locals 4
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sub-int v0, p0, p2

    sub-int v1, p1, p3

    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v2, v2

    return v2
.end method

.method private getGestureDynamicDistanceThreshold(I)I
    .locals 3
    .param p1    # I

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mAfterFastTyping:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    if-lt p1, v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdTo:I

    :goto_0
    return v1

    :cond_1
    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdFrom:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdTo:I

    sub-int/2addr v1, v2

    mul-int/2addr v1, p1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    div-int v0, v1, v2

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdFrom:I

    sub-int/2addr v1, v0

    goto :goto_0
.end method

.method private getGestureDynamicTimeThreshold(I)I
    .locals 3
    .param p1    # I

    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mAfterFastTyping:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    if-lt p1, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdTo:I

    :goto_0
    return v1

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdFrom:I

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdTo:I

    sub-int/2addr v1, v2

    mul-int/2addr v1, p1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    div-int v0, v1, v2

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdFrom:I

    sub-int/2addr v1, v0

    goto :goto_0
.end method

.method private final hasDetectedFastMove()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveTime:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateIncrementalRecognitionSize(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-long v3, p3

    iget-wide v5, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventTime:J

    sub-long/2addr v3, v5

    long-to-int v0, v3

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventX:I

    iget v4, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventY:I

    invoke-static {v3, v4, p1, p2}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getDistance(IIII)I

    move-result v1

    mul-int/lit16 v2, v1, 0x3e8

    iget v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureRecognitionSpeedThreshold:I

    mul-int/2addr v3, v0

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v3

    iput v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mIncrementalRecognitionSize:I

    goto :goto_0
.end method

.method private updateMajorEvent(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-long v0, p3

    iput-wide v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventTime:J

    iput p1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventX:I

    iput p2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventY:I

    return-void
.end method


# virtual methods
.method public addPoint(IIIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v1

    if-gtz v1, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->appendPoint(III)V

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->updateMajorEvent(III)V

    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->updateIncrementalRecognitionSize(III)V

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->updateMajorEvent(III)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->detectFastMove(III)I

    move-result v0

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureSamplingMinimumDistance:I

    if-le v0, v2, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->appendPoint(III)V

    goto :goto_0
.end method

.method public final appendAllBatchPoints(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->appendBatchPoints(Lcom/android/inputmethod/latin/InputPointers;I)V

    return-void
.end method

.method public final appendIncrementalBatchPoints(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mIncrementalRecognitionSize:I

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->appendBatchPoints(Lcom/android/inputmethod/latin/InputPointers;I)V

    return-void
.end method

.method public final hasRecognitionTimePast(JJ)Z
    .locals 2
    .param p1    # J
    .param p3    # J

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionMinimumTime:I

    int-to-long v0, v0

    add-long/2addr v0, p3

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStartOfAGesture()Z
    .locals 11

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->hasDetectedFastMove()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v7, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v5

    if-lez v5, :cond_0

    add-int/lit8 v4, v5, -0x1

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v7, v4}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v7

    iget v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveTime:I

    sub-int v1, v7, v8

    if-ltz v1, :cond_0

    iget-object v7, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v7, v4}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v7

    iget-object v8, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v8, v4}, Lcom/android/inputmethod/latin/ResizableIntArray;->get(I)I

    move-result v8

    iget v9, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveX:I

    iget v10, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveY:I

    invoke-static {v7, v8, v9, v10}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getDistance(IIII)I

    move-result v0

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getGestureDynamicDistanceThreshold(I)I

    move-result v2

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->getGestureDynamicTimeThreshold(I)I

    move-result v6

    if-lt v1, v6, :cond_2

    if-lt v0, v2, :cond_2

    const/4 v3, 0x1

    :cond_2
    goto :goto_0
.end method

.method public onDownEvent(IIJJJ)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # J
    .param p5    # J
    .param p7    # J

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->reset()V

    sub-long v0, p3, p7

    iget-object v3, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v3, v3, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mStaticTimeThresholdAfterFastTyping:I

    int-to-long v3, v3

    cmp-long v3, v0, v3

    if-gez v3, :cond_0

    iput-boolean v5, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mAfterFastTyping:Z

    :cond_0
    sub-long v3, p3, p5

    long-to-int v2, v3

    invoke-virtual {p0, p1, p2, v2, v5}, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->addPoint(IIIZ)V

    return-void
.end method

.method protected reset()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mIncrementalRecognitionSize:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastIncrementalBatchSize:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mEventTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/latin/ResizableIntArray;->setLength(I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mLastMajorEventTime:J

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveTime:I

    iput-boolean v2, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mAfterFastTyping:Z

    return-void
.end method

.method public setKeyboardGeometry(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mKeyWidth:I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDetectFastMoveSpeedThreshold:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mDetectFastMoveSpeedThreshold:I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdFrom:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdFrom:I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdTo:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureDynamicDistanceThresholdTo:I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mSamplingMinimumDistance:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureSamplingMinimumDistance:I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mParams:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionSpeedThreshold:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke;->mGestureRecognitionSpeedThreshold:I

    return-void
.end method
