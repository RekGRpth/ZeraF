.class public final Lcom/android/inputmethod/keyboard/MoreKeysDetector;
.super Lcom/android/inputmethod/keyboard/KeyDetector;
.source "MoreKeysDetector.java"


# instance fields
.field private final mSlideAllowanceSquare:I

.field private final mSlideAllowanceSquareTop:I


# direct methods
.method public constructor <init>(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/KeyDetector;-><init>(F)V

    mul-float v0, p1, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysDetector;->mSlideAllowanceSquare:I

    iget v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysDetector;->mSlideAllowanceSquare:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysDetector;->mSlideAllowanceSquareTop:I

    return-void
.end method


# virtual methods
.method public alwaysAllowsSlidingInput()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public detectHitKey(II)Lcom/android/inputmethod/keyboard/Key;
    .locals 10
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchX(I)I

    move-result v7

    invoke-virtual {p0, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchY(I)I

    move-result v8

    const/4 v6, 0x0

    if-gez p2, :cond_1

    iget v5, p0, Lcom/android/inputmethod/keyboard/MoreKeysDetector;->mSlideAllowanceSquareTop:I

    :goto_0
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyDetector;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v9

    iget-object v0, v9, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v3, v0, v2

    invoke-virtual {v3, v7, v8}, Lcom/android/inputmethod/keyboard/Key;->squaredDistanceToEdge(II)I

    move-result v1

    if-ge v1, v5, :cond_0

    move-object v6, v3

    move v5, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget v5, p0, Lcom/android/inputmethod/keyboard/MoreKeysDetector;->mSlideAllowanceSquare:I

    goto :goto_0

    :cond_2
    return-object v6
.end method
