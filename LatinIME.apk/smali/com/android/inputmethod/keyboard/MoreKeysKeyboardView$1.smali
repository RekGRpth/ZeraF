.class Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;
.super Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;
.source "MoreKeysKeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCancelInput()V

    return-void
.end method

.method public onCodeInput(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1, v1, v1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCodeInput(III)V

    return-void
.end method

.method public onEndBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onEndBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V

    return-void
.end method

.method public onPressKey(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onPressKey(I)V

    return-void
.end method

.method public onReleaseKey(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onReleaseKey(IZ)V

    return-void
.end method

.method public onStartBatchInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onStartBatchInput()V

    return-void
.end method

.method public onTextInput(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onTextInput(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onUpdateBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView$1;->this$0:Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->access$000(Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;)Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onUpdateBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V

    return-void
.end method
