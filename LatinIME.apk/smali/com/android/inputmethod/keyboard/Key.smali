.class public Lcom/android/inputmethod/keyboard/Key;
.super Ljava/lang/Object;
.source "Key.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/Key$Spacer;,
        Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/inputmethod/keyboard/Key;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_FLAGS_ALT_CODE_WHILE_TYPING:I = 0x4

.field private static final ACTION_FLAGS_ENABLE_LONG_PRESS:I = 0x8

.field private static final ACTION_FLAGS_IS_REPEATABLE:I = 0x1

.field private static final ACTION_FLAGS_NO_KEY_PREVIEW:I = 0x2

.field public static final BACKGROUND_TYPE_ACTION:I = 0x2

.field public static final BACKGROUND_TYPE_FUNCTIONAL:I = 0x1

.field public static final BACKGROUND_TYPE_NORMAL:I = 0x0

.field public static final BACKGROUND_TYPE_STICKY_OFF:I = 0x3

.field public static final BACKGROUND_TYPE_STICKY_ON:I = 0x4

.field private static final KEY_STATE_ACTIVE_NORMAL:[I

.field private static final KEY_STATE_ACTIVE_PRESSED:[I

.field private static final KEY_STATE_FUNCTIONAL_NORMAL:[I

.field private static final KEY_STATE_FUNCTIONAL_PRESSED:[I

.field private static final KEY_STATE_NORMAL:[I

.field private static final KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

.field private static final KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

.field private static final KEY_STATE_PRESSED:[I

.field private static final KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

.field private static final KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

.field private static final LABEL_FLAGS_ALIGN_LEFT:I = 0x1

.field private static final LABEL_FLAGS_ALIGN_LEFT_OF_CENTER:I = 0x8

.field private static final LABEL_FLAGS_ALIGN_RIGHT:I = 0x2

.field private static final LABEL_FLAGS_AUTO_X_SCALE:I = 0x4000

.field private static final LABEL_FLAGS_DISABLE_ADDITIONAL_MORE_KEYS:I = -0x80000000

.field private static final LABEL_FLAGS_DISABLE_HINT_LABEL:I = 0x40000000

.field private static final LABEL_FLAGS_FOLLOW_KEY_HINT_LABEL_RATIO:I = 0x140

.field private static final LABEL_FLAGS_FOLLOW_KEY_LABEL_RATIO:I = 0xc0

.field private static final LABEL_FLAGS_FOLLOW_KEY_LARGE_LABEL_RATIO:I = 0x100

.field private static final LABEL_FLAGS_FOLLOW_KEY_LARGE_LETTER_RATIO:I = 0x40

.field private static final LABEL_FLAGS_FOLLOW_KEY_LETTER_RATIO:I = 0x80

.field private static final LABEL_FLAGS_FOLLOW_KEY_TEXT_RATIO_MASK:I = 0x1c0

.field private static final LABEL_FLAGS_FONT_MONO_SPACE:I = 0x20

.field private static final LABEL_FLAGS_FONT_NORMAL:I = 0x10

.field private static final LABEL_FLAGS_FROM_CUSTOM_ACTION_LABEL:I = 0x20000

.field private static final LABEL_FLAGS_HAS_HINT_LABEL:I = 0x800

.field private static final LABEL_FLAGS_HAS_POPUP_HINT:I = 0x200

.field private static final LABEL_FLAGS_HAS_SHIFTED_LETTER_HINT:I = 0x400

.field private static final LABEL_FLAGS_PRESERVE_CASE:I = 0x8000

.field private static final LABEL_FLAGS_SHIFTED_LETTER_ACTIVATED:I = 0x10000

.field private static final LABEL_FLAGS_WITH_ICON_LEFT:I = 0x1000

.field private static final LABEL_FLAGS_WITH_ICON_RIGHT:I = 0x2000

.field private static final MORE_KEYS_AUTO_COLUMN_ORDER:Ljava/lang/String; = "!autoColumnOrder!"

.field private static final MORE_KEYS_COLUMN_MASK:I = 0xff

.field private static final MORE_KEYS_EMBEDDED_MORE_KEY:Ljava/lang/String; = "!embeddedMoreKey!"

.field private static final MORE_KEYS_FIXED_COLUMN_ORDER:Ljava/lang/String; = "!fixedColumnOrder!"

.field private static final MORE_KEYS_FLAGS_EMBEDDED_MORE_KEY:I = 0x10000000

.field private static final MORE_KEYS_FLAGS_FIXED_COLUMN_ORDER:I = -0x80000000

.field private static final MORE_KEYS_FLAGS_HAS_LABELS:I = 0x40000000

.field private static final MORE_KEYS_FLAGS_NEEDS_DIVIDERS:I = 0x20000000

.field private static final MORE_KEYS_HAS_LABELS:Ljava/lang/String; = "!hasLabels!"

.field private static final MORE_KEYS_NEEDS_DIVIDERS:Ljava/lang/String; = "!needsDividers!"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActionFlags:I

.field public final mBackgroundType:I

.field public final mCode:I

.field private mEnabled:Z

.field private final mHashCode:I

.field public final mHeight:I

.field public final mHintLabel:Ljava/lang/String;

.field public final mHitBox:Landroid/graphics/Rect;

.field private final mIconId:I

.field public final mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

.field public final mLabel:Ljava/lang/String;

.field private final mLabelFlags:I

.field public final mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

.field private final mMoreKeysColumnAndFlags:I

.field private final mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

.field private mPressed:Z

.field public final mWidth:I

.field public final mX:I

.field public final mY:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->TAG:Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

    new-array v0, v3, [I

    const v1, 0x101009f

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL:[I

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED:[I

    new-array v0, v3, [I

    const v1, 0x10100a3

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_NORMAL:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_PRESSED:[I

    new-array v0, v3, [I

    const v1, 0x10100a2

    aput v1, v0, v2

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_NORMAL:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_PRESSED:[I

    return-void

    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data

    :array_1
    .array-data 4
        0x10100a7
        0x101009f
        0x10100a0
    .end array-data

    :array_2
    .array-data 4
        0x10100a7
        0x101009f
    .end array-data

    :array_3
    .array-data 4
        0x10100a3
        0x10100a7
    .end array-data

    :array_4
    .array-data 4
        0x10100a2
        0x10100a7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Lcom/android/inputmethod/keyboard/internal/KeyboardRow;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 31
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;
    .param p3    # Lcom/android/inputmethod/keyboard/internal/KeyboardRow;
    .param p4    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v15, 0x0

    :goto_0
    move-object/from16 v0, p3

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->mRowHeight:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mVerticalGap:I

    sub-int v5, v18, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static/range {p4 .. p4}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v5

    sget-object v28, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v17

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;->getKeyStyle(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/inputmethod/keyboard/internal/KeyStyle;

    move-result-object v26

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getKeyX(Landroid/content/res/TypedArray;)F

    move-result v20

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getKeyWidth(Landroid/content/res/TypedArray;F)F

    move-result v19

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getKeyY()I

    move-result v21

    const/high16 v5, 0x40000000

    div-float v5, v15, v5

    add-float v5, v5, v20

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    sub-float v5, v19, v15

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v28

    add-float v29, v20, v19

    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->round(F)I

    move-result v29

    add-int/lit8 v29, v29, 0x1

    add-int v30, v21, v18

    move/from16 v0, v28

    move/from16 v1, v21

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    add-float v5, v20, v19

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->setXPos(F)V

    const/4 v5, 0x5

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getDefaultBackgroundType()I

    move-result v28

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    move/from16 v2, v28

    invoke-virtual {v0, v1, v5, v2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    const/16 v5, 0xf

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    move/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-static {v0, v5, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v10

    const/16 v5, 0x10

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBaseWidth:I

    move/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-static {v0, v5, v1, v2}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionOrFraction(Landroid/content/res/TypedArray;IIF)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v11

    const/16 v5, 0xb

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    const/16 v5, 0xc

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v8

    const/16 v5, 0xd

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIconId(Ljava/lang/String;)I

    move-result v9

    const/16 v5, 0xa

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Lcom/android/inputmethod/keyboard/internal/KeyboardRow;->getDefaultKeyLabelFlags()I

    move-result v28

    or-int v5, v5, v28

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-static {v5, v0}, Lcom/android/inputmethod/keyboard/Key;->needsToUpperCase(II)Z

    move-result v25

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v0, v5, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    move-object/from16 v22, v0

    const/4 v5, 0x6

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getFlag(Landroid/content/res/TypedArray;I)I

    move-result v12

    const/4 v5, 0x2

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v23

    const/4 v5, 0x4

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxMoreKeysKeyboardColumn:I

    move/from16 v28, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    move/from16 v2, v28

    invoke-virtual {v0, v1, v5, v2}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getInt(Landroid/content/res/TypedArray;II)I

    move-result v24

    const-string v5, "!autoColumnOrder!"

    const/16 v28, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-static {v0, v5, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIntValue([Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    if-lez v27, :cond_0

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    :cond_0
    const-string v5, "!fixedColumnOrder!"

    const/16 v28, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-static {v0, v5, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getIntValue([Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    if-lez v27, :cond_1

    const/high16 v5, -0x80000000

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v28, v0

    or-int v24, v5, v28

    :cond_1
    const-string v5, "!hasLabels!"

    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/high16 v5, 0x40000000

    or-int v24, v24, v5

    :cond_2
    const-string v5, "!needsDividers!"

    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/high16 v5, 0x20000000

    or-int v24, v24, v5

    :cond_3
    const-string v5, "!embeddedMoreKey!"

    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->getBooleanValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/high16 v5, 0x10000000

    or-int v24, v24, v5

    :cond_4
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v28, -0x80000000

    and-int v5, v5, v28

    if-eqz v5, :cond_6

    const/4 v13, 0x0

    :goto_1
    move-object/from16 v0, v23

    invoke-static {v0, v13}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->insertAdditionalMoreKeys([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_7

    or-int/lit8 v12, v12, 0x8

    move-object/from16 v0, v23

    array-length v5, v0

    new-array v5, v5, [Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    const/16 v16, 0x0

    :goto_2
    move-object/from16 v0, v23

    array-length v5, v0

    move/from16 v0, v16

    if-ge v0, v5, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    new-instance v28, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    aget-object v29, v23, v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v30, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v25

    move-object/from16 v3, v22

    move-object/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;-><init>(Ljava/lang/String;ZLjava/util/Locale;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;)V

    aput-object v28, v5, v16

    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_5
    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalGap:I

    int-to-float v15, v5

    goto/16 :goto_0

    :cond_6
    const/4 v5, 0x3

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getStringArray(Landroid/content/res/TypedArray;I)[Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    :cond_7
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    :cond_8
    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v28, 0x20000

    and-int v5, v5, v28

    if-eqz v5, :cond_a

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v5, v5, Lcom/android/inputmethod/keyboard/KeyboardId;->mCustomActionLabel:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    :goto_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v28, 0x40000000

    and-int v5, v5, v28

    if-eqz v5, :cond_b

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    :goto_4
    const/4 v5, 0x7

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v28, v0

    const/16 v29, -0xc

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCode(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;I)I

    move-result v14

    const/16 v5, -0xc

    if-ne v14, v5, :cond_e

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v5

    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v5, v0, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v5

    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    :goto_5
    const/4 v5, 0x1

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    move-object/from16 v28, v0

    const/16 v29, -0xc

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->parseCode(Ljava/lang/String;Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;I)I

    move-result v5

    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfCodeForLocale(IZLjava/util/Locale;)I

    move-result v7

    if-nez v6, :cond_11

    const/16 v5, -0xc

    if-ne v7, v5, :cond_11

    if-nez v8, :cond_11

    if-nez v9, :cond_11

    if-nez v10, :cond_11

    if-nez v11, :cond_11

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    :goto_6
    invoke-static/range {v17 .. v17}, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->newInstance(Landroid/content/res/TypedArray;)Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v5

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    sget-object v5, Lcom/android/inputmethod/keyboard/Key;->TAG:Ljava/lang/String;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "hasShiftedLetterHint specified without keyHintLabel: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    return-void

    :cond_a
    const/16 v5, 0x8

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    goto/16 :goto_3

    :cond_b
    const/16 v5, 0x9

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/android/inputmethod/keyboard/internal/KeyStyle;->getString(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfStringForLocale(Ljava/lang/String;ZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    const/4 v5, -0x3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_e
    const/16 v5, -0xc

    if-ne v14, v5, :cond_10

    if-eqz v6, :cond_10

    invoke-static {v6}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v5

    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v5, v0, :cond_f

    const/4 v5, 0x0

    invoke-virtual {v6, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v5, -0x3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_10
    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v14, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeySpecParser;->toUpperCaseOfCodeForLocale(IZLjava/util/Locale;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    goto/16 :goto_5

    :cond_11
    new-instance v5, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    invoke-direct/range {v5 .. v11}, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;-><init>(Ljava/lang/String;IIIII)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    goto/16 :goto_6
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;IIIII)V
    .locals 12
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;
    .param p2    # Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iget-object v2, p2, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;->mLabel:Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p2, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;->mIconId:I

    iget v5, p2, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;->mCode:I

    iget-object v6, p2, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;->mOutputText:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/android/inputmethod/keyboard/Key;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V

    return-void
.end method

.method public constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IIIII)V
    .locals 8
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mVerticalGap:I

    sub-int v1, p10, v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalGap:I

    sub-int v1, p9, v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iput-object p3, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    move/from16 v0, p11

    iput v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    if-nez p6, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    :goto_0
    iput p5, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v1, -0xc

    if-eq p5, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    iput p4, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalGap:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p7

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    move/from16 v0, p8

    iput v0, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    add-int v2, p7, p9

    add-int/lit8 v2, v2, 0x1

    add-int v3, p8, p10

    move/from16 v0, p8

    invoke-virtual {v1, p7, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-static {p0}, Lcom/android/inputmethod/keyboard/Key;->computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    return-void

    :cond_0
    new-instance v1, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    const/16 v3, -0xc

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p6

    invoke-direct/range {v1 .. v7}, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;-><init>(Ljava/lang/String;IIIII)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static backgroundName(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "normal"

    goto :goto_0

    :pswitch_1
    const-string v0, "functional"

    goto :goto_0

    :pswitch_2
    const-string v0, "action"

    goto :goto_0

    :pswitch_3
    const-string v0, "stickyOff"

    goto :goto_0

    :pswitch_4
    const-string v0, "stickyOn"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static computeHashCode(Lcom/android/inputmethod/keyboard/Key;)I
    .locals 3
    .param p0    # Lcom/android/inputmethod/keyboard/Key;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->getOutputText()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private equalsInternal(Lcom/android/inputmethod/keyboard/Key;)Z
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v0, 0x1

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-ne v1, v2, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    if-ne v1, v2, :cond_2

    iget-object v1, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->getOutputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->getOutputText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    if-ne v1, v2, :cond_2

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static needsToUpperCase(II)Z
    .locals 2
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x0

    const v1, 0x8000

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final altCodeWhileTyping()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public compareTo(Lcom/android/inputmethod/keyboard/Key;)I
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/Key;->equalsInternal(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/Key;->compareTo(Lcom/android/inputmethod/keyboard/Key;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/inputmethod/keyboard/Key;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/inputmethod/keyboard/Key;

    invoke-direct {p0, p1}, Lcom/android/inputmethod/keyboard/Key;->equalsInternal(Lcom/android/inputmethod/keyboard/Key;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAltCode()I
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mAltCode:I

    :goto_0
    return v1

    :cond_0
    const/16 v1, -0xc

    goto :goto_0
.end method

.method public final getCurrentDrawableState()[I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    packed-switch v0, :pswitch_data_0

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED:[I

    :goto_0
    return-object v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_PRESSED:[I

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_FUNCTIONAL_NORMAL:[I

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_PRESSED:[I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_ACTIVE_NORMAL:[I

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_OFF:[I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_OFF:[I

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_PRESSED_HIGHLIGHT_ON:[I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL_HIGHLIGHT_ON:[I

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/inputmethod/keyboard/Key;->KEY_STATE_NORMAL:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getDrawWidth()I
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-nez v0, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mVisualInsetsLeft:I

    sub-int/2addr v1, v2

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mVisualInsetsRight:I

    sub-int/2addr v1, v2

    goto :goto_0
.end method

.method public final getDrawX()I
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-nez v0, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mVisualInsetsLeft:I

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;
    .param p2    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mDisabledIconId:I

    :goto_0
    iget-boolean v4, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    if-eqz v4, :cond_2

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    :goto_1
    invoke-virtual {p1, v3}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-object v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1
.end method

.method public final getMoreKeyLabelFlags()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasLabelsInMoreKeys()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public final getMoreKeysColumn()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final getOutputText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mOutputText:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPreviewIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mOptionalAttributes:Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mPreviewIconId:I

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-virtual {p1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1
.end method

.method public final hasEmbeddedMoreKey()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHintLabel()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLabelWithIconLeft()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLabelWithIconRight()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLabelsInMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x40000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPopupHint()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasShiftedLetterHint()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHashCode:I

    return v0
.end method

.method public final isAlignLeft()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAlignLeftOfCenter()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAlignRight()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    return v0
.end method

.method public final isFixedColumnOrderMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLongPressEnabled()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isModifier()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnKey(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public final isRepeatable()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isShift()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isShiftedLetterActivated()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSpacer()Z
    .locals 1

    instance-of v0, p0, Lcom/android/inputmethod/keyboard/Key$Spacer;

    return v0
.end method

.method public markAsBottomEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mBottomPadding:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method public markAsLeftEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    return-void
.end method

.method public markAsRightEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mOccupiedWidth:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalEdgesPadding:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    return-void
.end method

.method public markAsTopEdge(Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mHitBox:Landroid/graphics/Rect;

    iget v1, p1, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTopPadding:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    return-void
.end method

.method public final needsDividersInMoreKeys()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mMoreKeysColumnAndFlags:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final needsXScale()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final noKeyPreview()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mActionFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPressed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    return-void
.end method

.method public onReleased()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/keyboard/Key;->mPressed:Z

    return-void
.end method

.method public final selectHintTextColor(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasHintLabel()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintActivatedColor:I

    goto :goto_0

    :cond_1
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintInactivatedColor:I

    goto :goto_0

    :cond_2
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterColor:I

    goto :goto_0
.end method

.method public final selectHintTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasHintLabel()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasShiftedLetterHint()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintSize:I

    goto :goto_0

    :cond_1
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    goto :goto_0
.end method

.method public final selectMoreKeyTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->hasLabelsInMoreKeys()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    goto :goto_0
.end method

.method public final selectTextColor(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->isShiftedLetterActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextInactivatedColor:I

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextColor:I

    goto :goto_0
.end method

.method public final selectTextSize(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)I
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit16 v0, v0, 0x1c0

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    :goto_0
    return v0

    :sswitch_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    goto :goto_0

    :sswitch_1
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLetterSize:I

    goto :goto_0

    :sswitch_2
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    goto :goto_0

    :sswitch_3
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLabelSize:I

    goto :goto_0

    :sswitch_4
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    goto :goto_0

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_1
        0x80 -> :sswitch_0
        0xc0 -> :sswitch_2
        0x100 -> :sswitch_3
        0x140 -> :sswitch_4
    .end sparse-switch
.end method

.method public final selectTypeface(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)Landroid/graphics/Typeface;
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/keyboard/Key;->mLabelFlags:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1

    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/Key;->mEnabled:Z

    return-void
.end method

.method public squaredDistanceToEdge(II)I
    .locals 10
    .param p1    # I
    .param p2    # I

    iget v5, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v8, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    add-int v6, v5, v8

    iget v7, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget v8, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    add-int v0, v7, v8

    if-ge p1, v5, :cond_0

    move v3, v5

    :goto_0
    if-ge p2, v7, :cond_2

    move v4, v7

    :goto_1
    sub-int v1, p1, v3

    sub-int v2, p2, v4

    mul-int v8, v1, v1

    mul-int v9, v2, v2

    add-int/2addr v8, v9

    return v8

    :cond_0
    if-le p1, v6, :cond_1

    move v3, v6

    goto :goto_0

    :cond_1
    move v3, p1

    goto :goto_0

    :cond_2
    if-le p2, v0, :cond_3

    move v4, v0

    goto :goto_1

    :cond_3
    move v4, p2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/inputmethod/latin/StringUtils;->codePointCount(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    iget v2, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    if-ne v1, v2, :cond_0

    const-string v0, ""

    :goto_0
    const-string v1, "%s%s %d,%d %dx%d %s/%s/%s"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    const/4 v3, 0x2

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mX:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mY:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/Key;->mHintLabel:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mIconId:I

    invoke-static {v4}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget v4, p0, Lcom/android/inputmethod/keyboard/Key;->mBackgroundType:I

    invoke-static {v4}, Lcom/android/inputmethod/keyboard/Key;->backgroundName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
