.class public Lcom/android/inputmethod/research/ResearchLogger;
.super Ljava/lang/Object;
.source "ResearchLogger.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEBUG:Z = false

.field public static final DEFAULT_USABILITY_STUDY_MODE:Z = false

.field static final DIGIT_REPLACEMENT_CODEPOINT:I

.field private static final DURATION_BETWEEN_DIR_CLEANUP_IN_MS:J = 0x5265c00L

.field private static final EVENTKEYS_FEEDBACK:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_ONCODEINPUT:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_ONDISPLAYCOMPLETIONS:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_ONSTARTINPUTVIEWINTERNAL:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_ONUPDATESELECTION:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_ONWINDOWHIDDEN:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_PICKSUGGESTIONMANUALLY:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_PUNCTUATIONSUGGESTION:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_REVERTCOMMIT:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_SENDKEYCODEPOINT:[Ljava/lang/String;

.field private static final EVENTKEYS_LATINIME_SWAPSWAPPERANDSPACE:[Ljava/lang/String;

.field private static final EVENTKEYS_MAINKEYBOARDVIEW_ONLONGPRESS:[Ljava/lang/String;

.field private static final EVENTKEYS_MAINKEYBOARDVIEW_PROCESSMOTIONEVENT:[Ljava/lang/String;

.field private static final EVENTKEYS_MAINKEYBOARDVIEW_SETKEYBOARD:[Ljava/lang/String;

.field private static final EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

.field private static final EVENTKEYS_POINTERTRACKER_CALLLISTENERONCANCELINPUT:[Ljava/lang/String;

.field private static final EVENTKEYS_POINTERTRACKER_CALLLISTENERONCODEINPUT:[Ljava/lang/String;

.field private static final EVENTKEYS_POINTERTRACKER_CALLLISTENERONRELEASE:[Ljava/lang/String;

.field private static final EVENTKEYS_POINTERTRACKER_ONDOWNEVENT:[Ljava/lang/String;

.field private static final EVENTKEYS_POINTERTRACKER_ONMOVEEVENT:[Ljava/lang/String;

.field private static final EVENTKEYS_PREFS_CHANGED:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_COMMITCOMPLETION:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_COMMITTEXT:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_DELETESURROUNDINGTEXT:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_FINISHCOMPOSINGTEXT:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_PERFORMEDITORACTION:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_SENDKEYEVENT:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_SETCOMPOSINGTEXT:[Ljava/lang/String;

.field private static final EVENTKEYS_RICHINPUTCONNECTION_SETSELECTION:[Ljava/lang/String;

.field private static final EVENTKEYS_STATISTICS:[Ljava/lang/String;

.field private static final EVENTKEYS_SUDDENJUMPINGTOUCHEVENTHANDLER_ONTOUCHEVENT:[Ljava/lang/String;

.field private static final EVENTKEYS_SUGGESTIONSTRIPVIEW_SETSUGGESTIONS:[Ljava/lang/String;

.field private static final EVENTKEYS_USER_FEEDBACK:[Ljava/lang/String;

.field private static final EVENTKEYS_USER_TIMESTAMP:[Ljava/lang/String;

.field public static final FEEDBACK_WORD_BUFFER_SIZE:I = 0x5

.field static final FILENAME_PREFIX:Ljava/lang/String; = "researchLog"

.field private static final FILENAME_SUFFIX:Ljava/lang/String; = ".txt"

.field private static final IS_SHOWING_INDICATOR:Z = true

.field private static final IS_SHOWING_INDICATOR_CLEARLY:Z = false

.field private static final MAX_INPUTVIEW_LENGTH_TO_CAPTURE:I = 0x2000

.field private static final MAX_LOGFILE_AGE_IN_MS:J = 0x5265c00L

.field private static final OUTPUT_ENTIRE_BUFFER:Z = false

.field private static final OUTPUT_FORMAT_VERSION:I = 0x1

.field private static final PREF_LAST_CLEANUP_TIME:Ljava/lang/String; = "pref_last_cleanup_time"

.field private static final PREF_RESEARCH_HAS_SEEN_SPLASH:Ljava/lang/String; = "pref_research_has_seen_splash"

.field private static final PREF_RESEARCH_LOGGER_UUID_STRING:Ljava/lang/String; = "pref_research_logger_uuid"

.field private static final PREF_USABILITY_STUDY_MODE:Ljava/lang/String; = "usability_study_mode"

.field protected static final SUSPEND_DURATION_IN_MINUTES:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TIMESTAMP_DATEFORMAT:Ljava/text/SimpleDateFormat;

.field private static final WHITESPACE_SEPARATORS:Ljava/lang/String; = " \t\n\r"

.field static final WORD_REPLACEMENT_STRING:Ljava/lang/String; = "\ue001"

.field private static final sInstance:Lcom/android/inputmethod/research/ResearchLogger;

.field static sIsLogging:Z

.field private static sLatinIMEExpectingUpdateSelection:Z


# instance fields
.field private mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

.field private mDictionary:Lcom/android/inputmethod/latin/Dictionary;

.field mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

.field mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

.field mFilesDir:Ljava/io/File;

.field private mInFeedbackDialog:Z

.field private mInputMethodService:Landroid/inputmethodservice/InputMethodService;

.field private mIsLoggingSuspended:Z

.field private mIsPasswordView:Z

.field private mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

.field mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

.field mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mResumeTime:J

.field private mSplashDialog:Landroid/app/Dialog;

.field private final mStatistics:Lcom/android/inputmethod/research/Statistics;

.field private mSuggest:Lcom/android/inputmethod/latin/Suggest;

.field mUUIDString:Ljava/lang/String;

.field private mUploadIntent:Landroid/content/Intent;

.field private mUploadPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/android/inputmethod/research/ResearchLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->$assertionsDisabled:Z

    const-class v0, Lcom/android/inputmethod/research/ResearchLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    sput-boolean v2, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmmssS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->TIMESTAMP_DATEFORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Lcom/android/inputmethod/research/ResearchLogger;

    invoke-direct {v0}, Lcom/android/inputmethod/research/ResearchLogger;-><init>()V

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->sInstance:Lcom/android/inputmethod/research/ResearchLogger;

    const-string v0, "\ue000"

    invoke-static {v0, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    sput v0, Lcom/android/inputmethod/research/ResearchLogger;->DIGIT_REPLACEMENT_CODEPOINT:I

    sput-boolean v2, Lcom/android/inputmethod/research/ResearchLogger;->sLatinIMEExpectingUpdateSelection:Z

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "UserTimestamp"

    aput-object v3, v0, v2

    const-string v3, "contents"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_FEEDBACK:[Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/Object;

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "LatinIMEOnStartInputViewInternal"

    aput-object v3, v0, v2

    const-string v3, "uuid"

    aput-object v3, v0, v1

    const-string v3, "packageName"

    aput-object v3, v0, v5

    const-string v3, "inputType"

    aput-object v3, v0, v6

    const-string v3, "imeOptions"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "fieldId"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "display"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "model"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "prefs"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "versionCode"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string v4, "versionName"

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const-string v4, "outputFormatVersion"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONSTARTINPUTVIEWINTERNAL:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "UserFeedback"

    aput-object v3, v0, v2

    const-string v3, "FeedbackContents"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_USER_FEEDBACK:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "PrefsChanged"

    aput-object v3, v0, v2

    const-string v3, "prefs"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_PREFS_CHANGED:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "MainKeyboardViewProcessMotionEvent"

    aput-object v3, v0, v2

    const-string v3, "action"

    aput-object v3, v0, v1

    const-string v3, "eventTime"

    aput-object v3, v0, v5

    const-string v3, "id"

    aput-object v3, v0, v6

    const-string v3, "x"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "y"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "size"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "pressure"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_PROCESSMOTIONEVENT:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "LatinIMEOnCodeInput"

    aput-object v3, v0, v2

    const-string v3, "code"

    aput-object v3, v0, v1

    const-string v3, "x"

    aput-object v3, v0, v5

    const-string v3, "y"

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONCODEINPUT:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "LatinIMEOnDisplayCompletions"

    aput-object v3, v0, v2

    const-string v3, "applicationSpecifiedCompletions"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONDISPLAYCOMPLETIONS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "LatinIMEOnWindowHidden"

    aput-object v3, v0, v2

    const-string v3, "isTextTruncated"

    aput-object v3, v0, v1

    const-string v3, "text"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONWINDOWHIDDEN:[Ljava/lang/String;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "LatinIMEOnUpdateSelection"

    aput-object v3, v0, v2

    const-string v3, "lastSelectionStart"

    aput-object v3, v0, v1

    const-string v3, "lastSelectionEnd"

    aput-object v3, v0, v5

    const-string v3, "oldSelStart"

    aput-object v3, v0, v6

    const-string v3, "oldSelEnd"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "newSelStart"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "newSelEnd"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "composingSpanStart"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "composingSpanEnd"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "expectingUpdateSelection"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string v4, "expectingUpdateSelectionFromLogger"

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const-string v4, "context"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONUPDATESELECTION:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "LatinIMEPickSuggestionManually"

    aput-object v3, v0, v2

    const-string v3, "replacedWord"

    aput-object v3, v0, v1

    const-string v3, "index"

    aput-object v3, v0, v5

    const-string v3, "suggestion"

    aput-object v3, v0, v6

    const-string v3, "x"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "y"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_PICKSUGGESTIONMANUALLY:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "LatinIMEPunctuationSuggestion"

    aput-object v3, v0, v2

    const-string v3, "index"

    aput-object v3, v0, v1

    const-string v3, "suggestion"

    aput-object v3, v0, v5

    const-string v3, "x"

    aput-object v3, v0, v6

    const-string v3, "y"

    aput-object v3, v0, v7

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_PUNCTUATIONSUGGESTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "LatinIMESendKeyCodePoint"

    aput-object v3, v0, v2

    const-string v3, "code"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_SENDKEYCODEPOINT:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "LatinIMESwapSwapperAndSpace"

    aput-object v3, v0, v2

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_SWAPSWAPPERANDSPACE:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "MainKeyboardViewOnLongPress"

    aput-object v3, v0, v2

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_ONLONGPRESS:[Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "MainKeyboardViewSetKeyboard"

    aput-object v3, v0, v2

    const-string v3, "elementId"

    aput-object v3, v0, v1

    const-string v3, "locale"

    aput-object v3, v0, v5

    const-string v3, "orientation"

    aput-object v3, v0, v6

    const-string v3, "width"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "modeName"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "action"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "navigateNext"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "navigatePrevious"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "clobberSettingsKey"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string v4, "passwordInput"

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const-string v4, "shortcutKeyEnabled"

    aput-object v4, v0, v3

    const/16 v3, 0xc

    const-string v4, "hasShortcutKey"

    aput-object v4, v0, v3

    const/16 v3, 0xd

    const-string v4, "languageSwitchKeyEnabled"

    aput-object v4, v0, v3

    const/16 v3, 0xe

    const-string v4, "isMultiLine"

    aput-object v4, v0, v3

    const/16 v3, 0xf

    const-string v4, "tw"

    aput-object v4, v0, v3

    const/16 v3, 0x10

    const-string v4, "th"

    aput-object v4, v0, v3

    const/16 v3, 0x11

    const-string v4, "keys"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_SETKEYBOARD:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "LatinIMERevertCommit"

    aput-object v3, v0, v2

    const-string v3, "originallyTypedWord"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_REVERTCOMMIT:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "PointerTrackerCallListenerOnCancelInput"

    aput-object v3, v0, v2

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONCANCELINPUT:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "PointerTrackerCallListenerOnCodeInput"

    aput-object v3, v0, v2

    const-string v3, "code"

    aput-object v3, v0, v1

    const-string v3, "outputText"

    aput-object v3, v0, v5

    const-string v3, "x"

    aput-object v3, v0, v6

    const-string v3, "y"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "ignoreModifierKey"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "altersCode"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "isEnabled"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONCODEINPUT:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "PointerTrackerCallListenerOnRelease"

    aput-object v3, v0, v2

    const-string v3, "code"

    aput-object v3, v0, v1

    const-string v3, "withSliding"

    aput-object v3, v0, v5

    const-string v3, "ignoreModifierKey"

    aput-object v3, v0, v6

    const-string v3, "isEnabled"

    aput-object v3, v0, v7

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONRELEASE:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "PointerTrackerOnDownEvent"

    aput-object v3, v0, v2

    const-string v3, "deltaT"

    aput-object v3, v0, v1

    const-string v3, "distanceSquared"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_ONDOWNEVENT:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "PointerTrackerOnMoveEvent"

    aput-object v3, v0, v2

    const-string v3, "x"

    aput-object v3, v0, v1

    const-string v3, "y"

    aput-object v3, v0, v5

    const-string v3, "lastX"

    aput-object v3, v0, v6

    const-string v3, "lastY"

    aput-object v3, v0, v7

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_ONMOVEEVENT:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "RichInputConnectionCommitCompletion"

    aput-object v3, v0, v2

    const-string v3, "completionInfo"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_COMMITCOMPLETION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "RichInputConnectionCommitText"

    aput-object v3, v0, v2

    const-string v3, "typedWord"

    aput-object v3, v0, v1

    const-string v3, "newCursorPosition"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_COMMITTEXT:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "RichInputConnectionDeleteSurroundingText"

    aput-object v3, v0, v2

    const-string v3, "beforeLength"

    aput-object v3, v0, v1

    const-string v3, "afterLength"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_DELETESURROUNDINGTEXT:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "RichInputConnectionFinishComposingText"

    aput-object v3, v0, v2

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_FINISHCOMPOSINGTEXT:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "RichInputConnectionPerformEditorAction"

    aput-object v3, v0, v2

    const-string v3, "imeActionNext"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_PERFORMEDITORACTION:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "RichInputConnectionSendKeyEvent"

    aput-object v3, v0, v2

    const-string v3, "eventTime"

    aput-object v3, v0, v1

    const-string v3, "action"

    aput-object v3, v0, v5

    const-string v3, "code"

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SENDKEYEVENT:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "RichInputConnectionSetComposingText"

    aput-object v3, v0, v2

    const-string v3, "text"

    aput-object v3, v0, v1

    const-string v3, "newCursorPosition"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SETCOMPOSINGTEXT:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "RichInputConnectionSetSelection"

    aput-object v3, v0, v2

    const-string v3, "from"

    aput-object v3, v0, v1

    const-string v3, "to"

    aput-object v3, v0, v5

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SETSELECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "SuddenJumpingTouchEventHandlerOnTouchEvent"

    aput-object v3, v0, v2

    const-string v3, "motionEvent"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_SUDDENJUMPINGTOUCHEVENTHANDLER_ONTOUCHEVENT:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v3, "SuggestionStripViewSetSuggestions"

    aput-object v3, v0, v2

    const-string v3, "suggestedWords"

    aput-object v3, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_SUGGESTIONSTRIPVIEW_SETSUGGESTIONS:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "UserTimestamp"

    aput-object v3, v0, v2

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_USER_TIMESTAMP:[Ljava/lang/String;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "Statistics"

    aput-object v3, v0, v2

    const-string v2, "charCount"

    aput-object v2, v0, v1

    const-string v1, "letterCount"

    aput-object v1, v0, v5

    const-string v1, "numberCount"

    aput-object v1, v0, v6

    const-string v1, "spaceCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "deleteOpsCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "wordCount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "isEmptyUponStarting"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "isEmptinessStateKnown"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "averageTimeBetweenKeys"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "averageTimeBeforeDelete"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "averageTimeDuringRepeatedDelete"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "averageTimeAfterDelete"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_STATISTICS:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsPasswordView:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsLoggingSuspended:Z

    new-instance v0, Lcom/android/inputmethod/research/LogUnit;

    invoke-direct {v0}, Lcom/android/inputmethod/research/LogUnit;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mResumeTime:J

    iput-boolean v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInFeedbackDialog:Z

    invoke-static {}, Lcom/android/inputmethod/research/Statistics;->getInstance()Lcom/android/inputmethod/research/Statistics;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/research/ResearchLogger;)Landroid/inputmethodservice/InputMethodService;
    .locals 1
    .param p0    # Lcom/android/inputmethod/research/ResearchLogger;

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/inputmethod/research/ResearchLogger;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Lcom/android/inputmethod/research/ResearchLogger;

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method private checkForEmptyEditor()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v3}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, v5, v4}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/research/Statistics;->setIsEmptyUponStarting(Z)V

    goto :goto_0

    :cond_2
    invoke-interface {v0, v5, v4}, Landroid/view/inputmethod/InputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/research/Statistics;->setIsEmptyUponStarting(Z)V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v3, v5}, Lcom/android/inputmethod/research/Statistics;->setIsEmptyUponStarting(Z)V

    goto :goto_0
.end method

.method private cleanupLoggingDir(Ljava/io/File;J)V
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # J

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "researchLog"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v4, v4, p2

    if-gez v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private createLogFile(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .param p1    # Ljava/io/File;

    const/16 v2, 0x2d

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "researchLog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUUIDString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->TIMESTAMP_DATEFORMAT:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private declared-synchronized enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    array-length v1, p1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->isAllowedToLog()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/inputmethod/research/LogUnit;->addLogStatement([Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Boolean;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    array-length v1, p1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->isAllowedToLog()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/inputmethod/research/LogUnit;->addLogStatement([Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Boolean;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static getAndClearLatinIMEExpectingUpdateSelection()Z
    .locals 2

    sget-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->sLatinIMEExpectingUpdateSelection:Z

    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/inputmethod/research/ResearchLogger;->sLatinIMEExpectingUpdateSelection:Z

    return v0
.end method

.method public static getInstance()Lcom/android/inputmethod/research/ResearchLogger;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/research/ResearchLogger;->sInstance:Lcom/android/inputmethod/research/ResearchLogger;

    return-object v0
.end method

.method private static getUUID(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/SharedPreferences;

    const-string v3, "pref_research_logger_uuid"

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "pref_research_logger_uuid"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-object v2
.end method

.method private hasOnlyLetters(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    :cond_0
    return v3

    :cond_1
    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    goto :goto_0
.end method

.method private hasSeenSplash()Z
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "pref_research_has_seen_splash"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isAllowedToLog()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsPasswordView:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsLoggingSuspended:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInFeedbackDialog:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static latinIME_onCodeInput(III)V
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    const/4 v4, 0x3

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitFromCodePoint(I)I

    move-result v5

    invoke-static {v5}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    sget-object v4, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONCODEINPUT:[Ljava/lang/String;

    invoke-direct {v0, v4, v3}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {v0}, Lcom/android/inputmethod/research/ResearchLogger;->setCurrentLogUnitContainsDigitFlag()V

    :cond_0
    iget-object v4, v0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v4, p0, v1, v2}, Lcom/android/inputmethod/research/Statistics;->recordChar(IJ)V

    return-void
.end method

.method public static latinIME_onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .locals 3
    .param p0    # [Landroid/view/inputmethod/CompletionInfo;

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONDISPLAYCOMPLETIONS:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static latinIME_onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Landroid/content/SharedPreferences;)V
    .locals 10
    .param p0    # Landroid/view/inputmethod/EditorInfo;
    .param p1    # Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v3

    invoke-direct {v3}, Lcom/android/inputmethod/research/ResearchLogger;->start()V

    if-eqz p0, :cond_0

    iget-object v0, v3, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v7, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const/16 v7, 0xb

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/android/inputmethod/research/ResearchLogger;->mUUIDString:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x1

    iget-object v8, p0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x2

    iget v8, p0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x3

    iget v8, p0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x4

    iget v8, p0, Landroid/view/inputmethod/EditorInfo;->fieldId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x5

    sget-object v8, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x6

    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x7

    aput-object p1, v4, v7

    const/16 v7, 0x8

    aput-object v5, v4, v7

    const/16 v7, 0x9

    aput-object v6, v4, v7

    const/16 v7, 0xa

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    sget-object v7, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONSTARTINPUTVIEWINTERNAL:[Ljava/lang/String;

    invoke-direct {v3, v7, v4}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public static latinIME_onUpdateSelection(IIIIIIIIZZLcom/android/inputmethod/latin/RichInputConnection;)V
    .locals 8
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Z
    .param p9    # Z
    .param p10    # Lcom/android/inputmethod/latin/RichInputConnection;

    const-string v5, ""

    if-eqz p10, :cond_0

    const-string v6, " \t\n\r"

    const/4 v7, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v0, v6, v7}, Lcom/android/inputmethod/latin/RichInputConnection;->getWordRangeAtCursor(Ljava/lang/String;I)Lcom/android/inputmethod/latin/RichInputConnection$Range;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v5, v1, Lcom/android/inputmethod/latin/RichInputConnection$Range;->mWord:Ljava/lang/String;

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v2

    invoke-direct {v2, v5}, Lcom/android/inputmethod/research/ResearchLogger;->scrubWord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0xb

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x5

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x6

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x7

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/16 v6, 0x8

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v6

    const/16 v6, 0x9

    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v6

    const/16 v6, 0xa

    aput-object v3, v4, v6

    sget-object v6, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONUPDATESELECTION:[Ljava/lang/String;

    invoke-direct {v2, v6, v4}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static latinIME_onWindowHidden(IILandroid/view/inputmethod/InputConnection;)V
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # Landroid/view/inputmethod/InputConnection;

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    const v3, 0x102001f

    invoke-interface {p2, v3}, Landroid/view/inputmethod/InputConnection;->performContextMenuAction(I)Z

    invoke-interface {p2, v5}, Landroid/view/inputmethod/InputConnection;->getSelectedText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p2, p0, p1}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    invoke-interface {p2}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    sput-boolean v4, Lcom/android/inputmethod/research/ResearchLogger;->sLatinIMEExpectingUpdateSelection:Z

    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v4

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v3, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_ONWINDOWHIDDEN:[Ljava/lang/String;

    invoke-direct {v1, v3, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/android/inputmethod/research/ResearchLogger;->commitCurrentLogUnit()V

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/research/ResearchLogger;->stop()V

    :cond_0
    return-void
.end method

.method public static latinIME_pickSuggestionManually(Ljava/lang/String;ILjava/lang/CharSequence;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;

    const/4 v4, -0x2

    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitsFromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x2

    if-nez p2, :cond_0

    const/4 v2, 0x0

    :goto_0
    aput-object v2, v1, v3

    const/4 v2, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_PICKSUGGESTIONMANUALLY:[Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitsFromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static latinIME_punctuationSuggestion(ILjava/lang/CharSequence;)V
    .locals 4
    .param p0    # I
    .param p1    # Ljava/lang/CharSequence;

    const/4 v3, -0x2

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_PUNCTUATIONSUGGESTION:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static latinIME_revertCommit(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_REVERTCOMMIT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static latinIME_sendKeyCodePoint(I)V
    .locals 4
    .param p0    # I

    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitFromCodePoint(I)I

    move-result v3

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_SENDKEYCODEPOINT:[Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {v0}, Lcom/android/inputmethod/research/ResearchLogger;->setCurrentLogUnitContainsDigitFlag()V

    :cond_0
    return-void
.end method

.method public static latinIME_swapSwapperAndSpace()V
    .locals 3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_LATINIME_SWAPSWAPPERANDSPACE:[Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private static logStatistics()V
    .locals 5

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    iget-object v1, v0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    const/16 v3, 0xc

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mCharCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mLetterCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mNumberCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mSpaceCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mDeleteKeyCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, v1, Lcom/android/inputmethod/research/Statistics;->mWordCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-boolean v4, v1, Lcom/android/inputmethod/research/Statistics;->mIsEmptyUponStarting:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-boolean v4, v1, Lcom/android/inputmethod/research/Statistics;->mIsEmptinessStateKnown:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-object v4, v1, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v4}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->getAverageTime()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, v1, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v4}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->getAverageTime()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget-object v4, v1, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v4}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->getAverageTime()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, v1, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v4}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->getAverageTime()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    sget-object v3, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_STATISTICS:[Ljava/lang/String;

    invoke-direct {v0, v3, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static mainKeyboardView_onLongPress()V
    .locals 3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_ONLONGPRESS:[Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static mainKeyboardView_processMotionEvent(Landroid/view/MotionEvent;IJIIII)V
    .locals 6
    .param p0    # Landroid/view/MotionEvent;
    .param p1    # I
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    if-eqz p0, :cond_0

    packed-switch p1, :pswitch_data_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, p4}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v2

    invoke-virtual {p0, p4}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    const/4 v4, 0x7

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v4

    sget-object v5, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_PROCESSMOTIONEVENT:[Ljava/lang/String;

    invoke-direct {v4, v5, v3}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :pswitch_0
    const-string v0, "CANCEL"

    goto :goto_0

    :pswitch_1
    const-string v0, "UP"

    goto :goto_0

    :pswitch_2
    const-string v0, "DOWN"

    goto :goto_0

    :pswitch_3
    const-string v0, "POINTER_UP"

    goto :goto_0

    :pswitch_4
    const-string v0, "POINTER_DOWN"

    goto :goto_0

    :pswitch_5
    const-string v0, "MOVE"

    goto :goto_0

    :pswitch_6
    const-string v0, "OUTSIDE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public static mainKeyboardView_setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 7
    .param p0    # Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->passwordInput()Z

    move-result v0

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v3

    invoke-direct {v3, v0}, Lcom/android/inputmethod/research/ResearchLogger;->setIsPasswordView(Z)V

    const/16 v3, 0x11

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    invoke-static {v4}, Lcom/android/inputmethod/keyboard/KeyboardId;->elementIdToName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mLocale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    const-string v6, "KeyboardLayoutSet"

    invoke-virtual {v5, v6}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mOrientation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mMode:I

    invoke-static {v4}, Lcom/android/inputmethod/keyboard/KeyboardId;->modeName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->imeAction()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->navigateNext()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->navigatePrevious()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-boolean v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mClobberSettingsKey:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget-boolean v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mShortcutKeyEnabled:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-boolean v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mHasShortcutKey:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    iget-boolean v4, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mLanguageSwitchKeyEnabled:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->isMultiLine()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xf

    iget v4, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mOccupiedHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x10

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mKeys:[Lcom/android/inputmethod/keyboard/Key;

    aput-object v4, v2, v3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v3

    invoke-direct {v3, v0}, Lcom/android/inputmethod/research/ResearchLogger;->setIsPasswordView(Z)V

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v3

    sget-object v4, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_MAINKEYBOARDVIEW_SETKEYBOARD:[Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private maybeShowSplashScreen()V
    .locals 7

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->hasSeenSplash()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_0

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b009a

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b0029

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040013

    new-instance v6, Lcom/android/inputmethod/research/ResearchLogger$3;

    invoke-direct {v6, p0}, Lcom/android/inputmethod/research/ResearchLogger$3;-><init>(Lcom/android/inputmethod/research/ResearchLogger;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040009

    new-instance v6, Lcom/android/inputmethod/research/ResearchLogger$2;

    invoke-direct {v6, p0}, Lcom/android/inputmethod/research/ResearchLogger$2;-><init>(Lcom/android/inputmethod/research/ResearchLogger;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/android/inputmethod/research/ResearchLogger$1;

    invoke-direct {v5, p0}, Lcom/android/inputmethod/research/ResearchLogger$1;-><init>(Lcom/android/inputmethod/research/ResearchLogger;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/16 v4, 0x3eb

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/high16 v4, 0x20000

    invoke-virtual {v2, v4}, Landroid/view/Window;->addFlags(I)V

    iget-object v4, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSplashDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private onWordComplete(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWordComplete: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/inputmethod/research/ResearchLogger;->hasOnlyLetters(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/research/LogUnit;->setWord(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics;->recordWordEntered()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->commitCurrentLogUnit()V

    return-void
.end method

.method public static pointerTracker_callListenerOnCancelInput()V
    .locals 3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONCANCELINPUT:[Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static pointerTracker_callListenerOnCodeInput(Lcom/android/inputmethod/keyboard/Key;IIZZI)V
    .locals 4
    .param p0    # Lcom/android/inputmethod/keyboard/Key;
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # Z
    .param p5    # I

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->getOutputText()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x7

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p5}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitFromCodePoint(I)I

    move-result v3

    invoke-static {v3}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x1

    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    aput-object v2, v1, v3

    const/4 v2, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v2

    sget-object v3, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONCODEINPUT:[Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitsFromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static pointerTracker_callListenerOnRelease(Lcom/android/inputmethod/keyboard/Key;IZZ)V
    .locals 3
    .param p0    # Lcom/android/inputmethod/keyboard/Key;
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    if-eqz p0, :cond_0

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitFromCodePoint(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/inputmethod/keyboard/Keyboard;->printableCode(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_CALLLISTENERONRELEASE:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static pointerTracker_onDownEvent(JI)V
    .locals 3
    .param p0    # J
    .param p2    # I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_ONDOWNEVENT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static pointerTracker_onMoveEvent(IIII)V
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_POINTERTRACKER_ONMOVEEVENT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static prefsChanged(Landroid/content/SharedPreferences;)V
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_PREFS_CHANGED:[Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private restart()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->stop()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->start()V

    return-void
.end method

.method private resumeLogging()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mResumeTime:J

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->updateSuspendedState()V

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->requestIndicatorRedraw()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->isAllowedToLog()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->restart()V

    :cond_0
    return-void
.end method

.method public static richInputConnection_commitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .locals 3
    .param p0    # Landroid/view/inputmethod/CompletionInfo;

    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_COMMITCOMPLETION:[Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V
    .locals 0
    .param p0    # Landroid/view/inputmethod/CorrectionInfo;

    return-void
.end method

.method public static richInputConnection_commitText(Ljava/lang/CharSequence;I)V
    .locals 5
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/inputmethod/research/ResearchLogger;->scrubDigitsFromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v3, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_COMMITTEXT:[Ljava/lang/String;

    invoke-direct {v0, v3, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {v0, v1}, Lcom/android/inputmethod/research/ResearchLogger;->onWordComplete(Ljava/lang/String;)V

    return-void
.end method

.method public static richInputConnection_deleteSurroundingText(II)V
    .locals 3
    .param p0    # I
    .param p1    # I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_DELETESURROUNDINGTEXT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_finishComposingText()V
    .locals 3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_FINISHCOMPOSINGTEXT:[Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_performEditorAction(I)V
    .locals 3
    .param p0    # I

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_PERFORMEDITORACTION:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_sendKeyEvent(Landroid/view/KeyEvent;)V
    .locals 4
    .param p0    # Landroid/view/KeyEvent;

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SENDKEYEVENT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_setComposingText(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "setComposingText is null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SETCOMPOSINGTEXT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static richInputConnection_setSelection(II)V
    .locals 3
    .param p0    # I
    .param p1    # I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_RICHINPUTCONNECTION_SETSELECTION:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static scheduleUploadingService(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const-wide/32 v2, 0x36ee80

    const/4 v4, 0x0

    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/android/inputmethod/research/UploaderService;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v4, v7, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v1, 0x2

    move-wide v4, v2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private static scrubDigitFromCodePoint(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p0, Lcom/android/inputmethod/research/ResearchLogger;->DIGIT_REPLACEMENT_CODEPOINT:I

    :cond_0
    return p0
.end method

.method static scrubDigitsFromString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-static {p0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget v4, Lcom/android/inputmethod/research/ResearchLogger;->DIGIT_REPLACEMENT_CODEPOINT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    const/4 v4, 0x1

    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    if-nez v3, :cond_4

    :goto_2
    return-object p0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_2
.end method

.method private scrubWord(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    if-nez v0, :cond_1

    const-string p1, "\ue001"

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mDictionary:Lcom/android/inputmethod/latin/Dictionary;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/Dictionary;->isValidWord(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "\ue001"

    goto :goto_0
.end method

.method private setCurrentLogUnitContainsDigitFlag()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/LogUnit;->setContainsDigit()V

    return-void
.end method

.method private setIsPasswordView(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsPasswordView:Z

    return-void
.end method

.method private setLoggingAllowed(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "usability_study_mode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sput-boolean p1, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    goto :goto_0
.end method

.method private start()V
    .locals 2

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->maybeShowSplashScreen()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->updateSuspendedState()V

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->requestIndicatorRedraw()V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mStatistics:Lcom/android/inputmethod/research/Statistics;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics;->reset()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->checkForEmptyEditor()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->isAllowedToLog()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    sget-object v0, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    const-string v1, "IME storage directory does not exist.  Cannot start logging."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    if-nez v0, :cond_4

    new-instance v0, Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/android/inputmethod/research/ResearchLogger;->createLogFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/research/ResearchLog;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    new-instance v0, Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/research/MainLogBuffer;-><init>(Lcom/android/inputmethod/research/ResearchLog;)V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/research/MainLogBuffer;->setSuggest(Lcom/android/inputmethod/latin/Suggest;)V

    :cond_4
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/android/inputmethod/research/ResearchLogger;->createLogFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/inputmethod/research/ResearchLog;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    new-instance v0, Lcom/android/inputmethod/research/LogBuffer;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/android/inputmethod/research/LogBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    goto :goto_0
.end method

.method public static suddenJumpingTouchEventHandler_onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p0    # Landroid/view/MotionEvent;

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_SUDDENJUMPINGTOUCHEVENTHANDLER_ONTOUCHEVENT:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static suggestionStripView_setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;)V
    .locals 3
    .param p0    # Lcom/android/inputmethod/latin/SuggestedWords;

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v1

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_SUGGESTIONSTRIPVIEW_SETSUGGESTIONS:[Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/android/inputmethod/research/ResearchLogger;->enqueuePotentiallyPrivateEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private suspendLoggingUntil(J)V
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsLoggingSuspended:Z

    iput-wide p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mResumeTime:J

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->requestIndicatorRedraw()V

    return-void
.end method

.method private updateSuspendedState()V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mResumeTime:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mIsLoggingSuspended:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public abort()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    invoke-virtual {v2}, Lcom/android/inputmethod/research/LogBuffer;->clear()V

    :try_start_0
    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-virtual {v2}, Lcom/android/inputmethod/research/ResearchLog;->blockingAbort()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    iput-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    invoke-virtual {v2}, Lcom/android/inputmethod/research/LogBuffer;->clear()V

    :try_start_1
    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-virtual {v2}, Lcom/android/inputmethod/research/ResearchLog;->blockingAbort()Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    :goto_1
    iput-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :goto_2
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_1

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method commitCurrentLogUnit()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/LogUnit;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/research/MainLogBuffer;->shiftIn(Lcom/android/inputmethod/research/LogUnit;)V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/MainLogBuffer;->isSafeToLog()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->publishLogBuffer(Lcom/android/inputmethod/research/LogBuffer;Lcom/android/inputmethod/research/ResearchLog;Z)V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/MainLogBuffer;->resetWordCounter()V

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/research/LogBuffer;->shiftIn(Lcom/android/inputmethod/research/LogUnit;)V

    :cond_1
    new-instance v0, Lcom/android/inputmethod/research/LogUnit;

    invoke-direct {v0}, Lcom/android/inputmethod/research/LogUnit;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mCurrentLogUnit:Lcom/android/inputmethod/research/LogUnit;

    sget-object v0, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    const-string v1, "commitCurrentLogUnit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public init(Landroid/inputmethodservice/InputMethodService;Landroid/content/SharedPreferences;)V
    .locals 13
    .param p1    # Landroid/inputmethodservice/InputMethodService;
    .param p2    # Landroid/content/SharedPreferences;

    const-wide/32 v11, 0x5265c00

    const/4 v10, 0x0

    sget-boolean v7, Lcom/android/inputmethod/research/ResearchLogger;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    if-nez p1, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    :cond_0
    if-nez p1, :cond_4

    sget-object v7, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    const-string v8, "IMS is null; logging is off"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-eqz p2, :cond_3

    invoke-static {p2}, Lcom/android/inputmethod/research/ResearchLogger;->getUUID(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUUIDString:Ljava/lang/String;

    const-string v7, "usability_study_mode"

    invoke-interface {p2, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v7, "usability_study_mode"

    invoke-interface {v0, v7, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    const-string v7, "usability_study_mode"

    invoke-interface {p2, v7, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    sput-boolean v7, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    invoke-interface {p2, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const-string v7, "pref_last_cleanup_time"

    const-wide/16 v8, 0x0

    invoke-interface {p2, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long v7, v1, v11

    cmp-long v7, v7, v3

    if-gez v7, :cond_3

    sub-long v5, v3, v11

    iget-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-direct {p0, v7, v5, v6}, Lcom/android/inputmethod/research/ResearchLogger;->cleanupLoggingDir(Ljava/io/File;J)V

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v7, "pref_last_cleanup_time"

    invoke-interface {v0, v7, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    iput-object p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    iput-object p2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    const-class v9, Lcom/android/inputmethod/research/UploaderService;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUploadIntent:Landroid/content/Intent;

    iget-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    iget-object v8, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUploadIntent:Landroid/content/Intent;

    invoke-static {v7, v10, v8, v10}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUploadPendingIntent:Landroid/app/PendingIntent;

    return-void

    :cond_4
    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    iget-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_5
    sget-object v7, Lcom/android/inputmethod/research/ResearchLogger;->TAG:Ljava/lang/String;

    const-string v8, "IME storage directory does not exist."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initSuggest(Lcom/android/inputmethod/latin/Suggest;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/Suggest;

    iput-object p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/research/MainLogBuffer;->setSuggest(Lcom/android/inputmethod/latin/Suggest;)V

    :cond_0
    return-void
.end method

.method public latinIME_onFinishInputInternal()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->stop()V

    return-void
.end method

.method public mainKeyboardView_onAttachedToWindow(Lcom/android/inputmethod/keyboard/MainKeyboardView;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/MainKeyboardView;

    iput-object p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->maybeShowSplashScreen()V

    return-void
.end method

.method public mainKeyboardView_onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    return-void
.end method

.method public onLeavingSendFeedbackDialog()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInFeedbackDialog:Z

    return-void
.end method

.method public onResearchKeySelected(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;

    iget-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInFeedbackDialog:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0b0099

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/inputmethod/research/ResearchLogger;->presentFeedbackDialog(Lcom/android/inputmethod/latin/LatinIME;)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "usability_study_mode"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    sget-boolean v0, Lcom/android/inputmethod/research/ResearchLogger;->sIsLogging:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->abort()Z

    :cond_2
    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->requestIndicatorRedraw()V

    iput-object p1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/android/inputmethod/research/ResearchLogger;->prefsChanged(Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method public onUserLoggingConsent()V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/inputmethod/research/ResearchLogger;->setLoggingAllowed(Z)V

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_research_has_seen_splash"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->restart()V

    goto :goto_0
.end method

.method public paintIndicator(Lcom/android/inputmethod/keyboard/KeyboardView;Landroid/graphics/Paint;Landroid/graphics/Canvas;II)V
    .locals 9
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardView;
    .param p2    # Landroid/graphics/Paint;
    .param p3    # Landroid/graphics/Canvas;
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Lcom/android/inputmethod/research/ResearchLogger;->isAllowedToLog()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/android/inputmethod/keyboard/MainKeyboardView;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    const/high16 v0, -0x10000

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v8

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v7

    const/high16 v0, 0x40400000

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    int-to-float v1, p4

    int-to-float v2, p5

    int-to-float v3, p4

    int-to-float v4, p5

    move-object v0, p3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p2, v6}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method

.method public presentFeedbackDialog(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/LatinIME;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInFeedbackDialog:Z

    const-class v0, Lcom/android/inputmethod/research/FeedbackActivity;

    invoke-virtual {p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->launchKeyboardedDialogActivity(Ljava/lang/Class;)V

    return-void
.end method

.method publishLogBuffer(Lcom/android/inputmethod/research/LogBuffer;Lcom/android/inputmethod/research/ResearchLog;Z)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/research/LogBuffer;
    .param p2    # Lcom/android/inputmethod/research/ResearchLog;
    .param p3    # Z

    :goto_0
    invoke-virtual {p1}, Lcom/android/inputmethod/research/LogBuffer;->shiftOut()Lcom/android/inputmethod/research/LogUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, v0, p3}, Lcom/android/inputmethod/research/ResearchLog;->publish(Lcom/android/inputmethod/research/LogUnit;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public requestIndicatorRedraw()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainKeyboardView:Lcom/android/inputmethod/keyboard/MainKeyboardView;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateAllKeys()V

    goto :goto_0
.end method

.method public sendFeedback(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->commitCurrentLogUnit()V

    :goto_1
    new-instance v0, Lcom/android/inputmethod/research/LogUnit;

    invoke-direct {v0}, Lcom/android/inputmethod/research/LogUnit;-><init>()V

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_FEEDBACK:[Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/android/inputmethod/research/LogUnit;->addLogStatement([Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/research/LogBuffer;->shiftIn(Lcom/android/inputmethod/research/LogUnit;)V

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/inputmethod/research/ResearchLogger;->publishLogBuffer(Lcom/android/inputmethod/research/LogBuffer;Lcom/android/inputmethod/research/ResearchLog;Z)V

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    new-instance v3, Lcom/android/inputmethod/research/ResearchLogger$4;

    invoke-direct {v3, p0}, Lcom/android/inputmethod/research/ResearchLogger$4;-><init>(Lcom/android/inputmethod/research/ResearchLogger;)V

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/research/ResearchLog;->close(Ljava/lang/Runnable;)V

    new-instance v2, Lcom/android/inputmethod/research/ResearchLog;

    iget-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFilesDir:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/android/inputmethod/research/ResearchLogger;->createLogFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/inputmethod/research/ResearchLog;-><init>(Ljava/io/File;)V

    iput-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    invoke-virtual {v2}, Lcom/android/inputmethod/research/LogBuffer;->clear()V

    goto :goto_1
.end method

.method stop()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->logStatistics()V

    invoke-virtual {p0}, Lcom/android/inputmethod/research/ResearchLogger;->commitCurrentLogUnit()V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->publishLogBuffer(Lcom/android/inputmethod/research/LogBuffer;Lcom/android/inputmethod/research/ResearchLog;Z)V

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/research/ResearchLog;->close(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mMainLogBuffer:Lcom/android/inputmethod/research/MainLogBuffer;

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLog:Lcom/android/inputmethod/research/ResearchLog;

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/research/ResearchLog;->close(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/android/inputmethod/research/ResearchLogger;->mFeedbackLogBuffer:Lcom/android/inputmethod/research/LogBuffer;

    :cond_1
    return-void
.end method

.method public uploadNow()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/research/ResearchLogger;->mInputMethodService:Landroid/inputmethodservice/InputMethodService;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLogger;->mUploadIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public userTimestamp()V
    .locals 3

    invoke-static {}, Lcom/android/inputmethod/research/ResearchLogger;->getInstance()Lcom/android/inputmethod/research/ResearchLogger;

    move-result-object v0

    sget-object v1, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_USER_TIMESTAMP:[Ljava/lang/String;

    sget-object v2, Lcom/android/inputmethod/research/ResearchLogger;->EVENTKEYS_NULLVALUES:[Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/research/ResearchLogger;->enqueueEvent([Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
