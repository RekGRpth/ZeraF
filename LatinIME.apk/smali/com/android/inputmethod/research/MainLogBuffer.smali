.class public Lcom/android/inputmethod/research/MainLogBuffer;
.super Lcom/android/inputmethod/research/LogBuffer;
.source "MainLogBuffer.java"


# static fields
.field private static final DEFAULT_NUMBER_OF_WORDS_BETWEEN_SAMPLES:I = 0x12

.field private static final N_GRAM_SIZE:I = 0x2


# instance fields
.field mMinWordPeriod:I

.field private final mResearchLog:Lcom/android/inputmethod/research/ResearchLog;

.field private mSuggest:Lcom/android/inputmethod/latin/Suggest;

.field mWordsUntilSafeToSample:I


# direct methods
.method public constructor <init>(Lcom/android/inputmethod/research/ResearchLog;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/research/ResearchLog;

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/inputmethod/research/LogBuffer;-><init>(I)V

    iput-object p1, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    const/16 v1, 0x14

    iput v1, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mMinWordPeriod:I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iget v1, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mMinWordPeriod:I

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iput v1, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    return-void
.end method


# virtual methods
.method public isSafeToLog()Z
    .locals 7

    const/4 v5, 0x0

    iget v6, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    if-lez v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/Suggest;->hasMainDictionary()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/Suggest;->getMainDictionary()Lcom/android/inputmethod/latin/Dictionary;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/android/inputmethod/research/LogBuffer;->mLogUnits:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    iget-object v6, p0, Lcom/android/inputmethod/research/LogBuffer;->mLogUnits:Ljava/util/LinkedList;

    invoke-virtual {v6, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/inputmethod/research/LogUnit;

    invoke-virtual {v3}, Lcom/android/inputmethod/research/LogUnit;->getWord()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-virtual {v3}, Lcom/android/inputmethod/research/LogUnit;->hasDigit()Z

    move-result v6

    if-nez v6, :cond_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v4}, Lcom/android/inputmethod/latin/Dictionary;->isValidWord(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_0

    :cond_4
    const/4 v5, 0x1

    goto :goto_0
.end method

.method protected onShiftOut(Lcom/android/inputmethod/research/LogUnit;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/research/LogUnit;

    iget-object v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mResearchLog:Lcom/android/inputmethod/research/ResearchLog;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/research/ResearchLog;->publish(Lcom/android/inputmethod/research/LogUnit;Z)V

    :cond_0
    return-void
.end method

.method public resetWordCounter()V
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mMinWordPeriod:I

    iput v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    return-void
.end method

.method public setSuggest(Lcom/android/inputmethod/latin/Suggest;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/latin/Suggest;

    iput-object p1, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    return-void
.end method

.method public shiftIn(Lcom/android/inputmethod/research/LogUnit;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/research/LogUnit;

    invoke-super {p0, p1}, Lcom/android/inputmethod/research/LogBuffer;->shiftIn(Lcom/android/inputmethod/research/LogUnit;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/research/LogUnit;->hasWord()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/inputmethod/research/MainLogBuffer;->mWordsUntilSafeToSample:I

    :cond_0
    return-void
.end method
