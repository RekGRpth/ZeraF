.class public final Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;
.super Ljava/lang/Object;
.source "KeyCodeDescriptionMapper.java"


# static fields
.field private static final OBSCURED_KEY_RES_ID:I = 0x7f0b0079

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;


# instance fields
.field private final mKeyCodeMap:Landroid/util/SparseIntArray;

.field private final mKeyLabelMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;

    invoke-direct {v0}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;-><init>()V

    sput-object v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->sInstance:Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyLabelMap:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    return-void
.end method

.method private getDescriptionForActionKey(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;Lcom/android/inputmethod/keyboard/Key;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;
    .param p3    # Lcom/android/inputmethod/keyboard/Key;

    iget-object v1, p2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardId;->imeActionId()I

    move-result v0

    iget-object v3, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    packed-switch v0, :pswitch_data_0

    const v2, 0x7f0b0077

    :goto_1
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_0
    const v2, 0x7f0b0078

    goto :goto_1

    :pswitch_1
    const v2, 0x7f0b005d

    goto :goto_1

    :pswitch_2
    const v2, 0x7f0b0061

    goto :goto_1

    :pswitch_3
    const v2, 0x7f0b005e

    goto :goto_1

    :pswitch_4
    const v2, 0x7f0b0060

    goto :goto_1

    :pswitch_5
    const v2, 0x7f0b005f

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getDescriptionForKeyCode(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;Lcom/android/inputmethod/keyboard/Key;Z)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;
    .param p3    # Lcom/android/inputmethod/keyboard/Key;
    .param p4    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p3, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-static {v0}, Ljava/lang/Character;->isDefined(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    :goto_0
    if-eqz p4, :cond_1

    if-eqz v1, :cond_1

    const v2, 0x7f0b0079

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v4

    if-ltz v4, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    int-to-char v2, v0

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    iget-object v4, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v2, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const v4, 0x7f0b006a

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {p1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private getDescriptionForShiftKey(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v0, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const v2, 0x7f0b006b

    :goto_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :pswitch_1
    const v2, 0x7f0b006d

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0b006c

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getDescriptionForSwitchAlphaSymbol(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v0, v1, Lcom/android/inputmethod/keyboard/KeyboardId;->mElementId:I

    packed-switch v0, :pswitch_data_0

    sget-object v3, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Missing description for keyboard element ID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :pswitch_0
    const v2, 0x7f0b006f

    :goto_1
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0b0070

    goto :goto_1

    :pswitch_2
    const v2, 0x7f0b006f

    goto :goto_1

    :pswitch_3
    const v2, 0x7f0b0071

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getInstance()Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->sInstance:Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;

    return-object v0
.end method

.method public static init()V
    .locals 1

    sget-object v0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->sInstance:Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;

    invoke-direct {v0}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->initInternal()V

    return-void
.end method

.method private initInternal()V
    .locals 3

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyLabelMap:Ljava/util/HashMap;

    const-string v1, ":-)"

    const v2, 0x7f0b0076

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0b0074

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x4

    const v2, 0x7f0b006e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    const v2, 0x7f0b0077

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x5

    const v2, 0x7f0b0072

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    const v2, 0x7f0b006b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x6

    const v2, 0x7f0b0075

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x2

    const v2, 0x7f0b006f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/16 v1, 0x9

    const v2, 0x7f0b0073

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/16 v1, -0xa

    const v2, 0x7f0b007a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/4 v1, -0x8

    const v2, 0x7f0b007b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyCodeMap:Landroid/util/SparseIntArray;

    const/16 v1, -0x9

    const v2, 0x7f0b007c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method


# virtual methods
.method public getDescriptionForKey(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;Lcom/android/inputmethod/keyboard/Key;Z)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/Keyboard;
    .param p3    # Lcom/android/inputmethod/keyboard/Key;
    .param p4    # Z

    iget v0, p3, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->getDescriptionForSwitchAlphaSymbol(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->getDescriptionForShiftKey(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v3, -0x7

    if-ne v0, v3, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->getDescriptionForActionKey(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;Lcom/android/inputmethod/keyboard/Key;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v3, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p3, Lcom/android/inputmethod/keyboard/Key;->mLabel:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyLabelMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->mKeyLabelMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget v3, p3, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v4, -0xc

    if-eq v3, v4, :cond_4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/accessibility/KeyCodeDescriptionMapper;->getDescriptionForKeyCode(Landroid/content/Context;Lcom/android/inputmethod/keyboard/Keyboard;Lcom/android/inputmethod/keyboard/Key;Z)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
