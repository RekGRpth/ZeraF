.class public Lcom/mediatek/vcalendar/VCalParser;
.super Ljava/lang/Object;
.source "VCalParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;
    }
.end annotation


# static fields
.field protected static final BUNDLE_KEY_END_MILLIS:Ljava/lang/String; = "key_end_millis"

.field protected static final BUNDLE_KEY_EVENT_ID:Ljava/lang/String; = "key_event_id"

.field protected static final BUNDLE_KEY_START_MILLIS:Ljava/lang/String; = "key_start_millis"

.field static final DEFAULT_ACCOUNT_NAME:Ljava/lang/String; = "PC Sync"

.field private static final TAG:Ljava/lang/String; = "VCalParser"


# instance fields
.field private mCalendarId:J

.field private mCancelRequest:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentAccountName:Ljava/lang/String;

.field private mCurrentCnt:I

.field private mCurrentUri:Landroid/net/Uri;

.field private mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

.field private mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

.field private final mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

.field private mTotalCnt:I

.field private final mUriAccountPears:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mVcsString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/accounts/Account;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const-string v0, "PC Sync"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/mediatek/vcalendar/VCalParser;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/mediatek/vcalendar/VCalStatusChangeOperator;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    iput-object p3, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iput-object p2, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    const-string v0, "PC Sync"

    iput-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    return-void
.end method

.method private initTools()Z
    .locals 9

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    new-instance v2, Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5, v3, v6}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v4, :cond_1

    const-string v2, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initTools: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " calendars exist in the given account."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    :goto_1
    iget-wide v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    const-wide/16 v7, -0x1

    cmp-long v2, v5, v7

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v2, v3, v3, v4}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    move v2, v3

    goto :goto_0

    :cond_1
    const-string v2, "VCalParser"

    const-string v5, "initTools: the given calendar account does not exsit."

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v2, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initTools: accountName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5, v6}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v2, v4

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v2, "VCalParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initTools: the given Uri cannot be parsed, Uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v2, v3

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v2, "VCalParser"

    const-string v4, "initTools: IOException Occured when I/O operation. "

    invoke-static {v2, v4}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v2, v3

    goto/16 :goto_0
.end method

.method private parseVCalVersion(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v3, "VERSION"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VERSION"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v3, "\n"

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/vcalendar/component/VCalendar;->sVersion:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelCurrentParse()V
    .locals 3

    const-string v0, "VCalParser"

    const-string v1, "cancelCurrentParse"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v1, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v2, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationCanceled(II)V

    return-void
.end method

.method public close()V
    .locals 2

    const-string v0, "VCalParser"

    const-string v1, "close."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    :cond_0
    return-void
.end method

.method public startParse()V
    .locals 14

    const-string v10, "VCalParser"

    const-string v11, "startParse: started."

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_0

    const-string v10, "VCalParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "startParse,fileIndex:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    iget-object v10, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Landroid/net/Uri;

    iput-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    iget-object v10, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    iput-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/vcalendar/VCalParser;->initTools()Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "VCalParser"

    const-string v11, "startParse: initTools failed."

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v10}, Lcom/mediatek/vcalendar/FileOperationHelper;->getVEventsCount()I

    move-result v10

    iput v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v10, v11}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationStarted(I)V

    const-string v10, "VCalParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "startParse: Events total count:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_2

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x2

    invoke-interface {v10, v11, v12, v13}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    :cond_2
    const/4 v10, 0x0

    iput v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    const-wide/16 v4, -0x1

    :goto_2
    iget-boolean v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    if-nez v10, :cond_5

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v10}, Lcom/mediatek/vcalendar/FileOperationHelper;->hasNextVEvent()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v10}, Lcom/mediatek/vcalendar/FileOperationHelper;->getNextVEventString()Ljava/lang/String;

    move-result-object v6

    :try_start_0
    invoke-static {v6}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    new-instance v0, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;

    invoke-direct {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v10

    const-string v11, "calendar_id"

    iget-wide v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/mediatek/vcalendar/component/VEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v10

    const-string v11, "organizer"

    invoke-virtual {v10, v11, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/vcalendar/component/VEvent;->toEventsContentValue(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAlarmsList()Ljava/util/LinkedList;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/vcalendar/component/VEvent;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAttendeesList()Ljava/util/LinkedList;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/vcalendar/component/VEvent;->toAttendeesContentValue(Ljava/util/LinkedList;)V

    invoke-virtual {v9}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v10, v11, v12}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalProcessStatusUpdate(II)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v10}, Lcom/mediatek/vcalendar/FileOperationHelper;->hasNextVEvent()Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    const/4 v11, 0x0

    invoke-virtual {v10, v0, v11}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)Landroid/net/Uri;

    :goto_3
    iget v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v10, "VCalParser"

    const-string v11, "startAccountCompose: BuileEvent failed"

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const/4 v13, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_2

    :catch_1
    move-exception v1

    const-string v10, "VCalParser"

    const-string v11, "startParse: VEvent to contentvalues failed"

    invoke-static {v10, v11}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    const/4 v13, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_2

    :cond_4
    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    const/4 v11, 0x1

    invoke-virtual {v10, v0, v11}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)Landroid/net/Uri;

    goto :goto_3

    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v10, "key_start_millis"

    invoke-virtual {v2, v10, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v11, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentCnt:I

    iget v12, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v10, v11, v12, v2}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    sget-object v10, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v10}, Lcom/mediatek/vcalendar/FileOperationHelper;->close()V

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mUriAccountPears:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-boolean v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mCancelRequest:Z

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)Landroid/net/Uri;

    sget-object v10, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0
.end method

.method public startParsePreview()V
    .locals 9

    const/4 v8, -0x1

    const/4 v7, 0x0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: started"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v4, Lcom/mediatek/vcalendar/FileOperationHelper;

    iget-object v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v6}, Lcom/mediatek/vcalendar/FileOperationHelper;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->getFirstEventStr()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: it is not a vcs file."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v4, v7, v8, v7}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    :cond_0
    const/4 v3, 0x0

    if-eqz v2, :cond_1

    :try_start_1
    invoke-static {v2}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    :cond_1
    :goto_0
    if-nez v3, :cond_2

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    invoke-interface {v4, v7, v8, v7}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    const-string v4, "VCalParser"

    const-string v5, "startParse: buildEvents failed, vEvent = null"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startParsePreview: the given Uri cannot be parsed, Uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: IOException Occured when I/O operation. "

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v4, "VCalParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startParsePreview : build calendar failed : \n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;

    invoke-direct {v1}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;-><init>()V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mFileOperationHelper:Lcom/mediatek/vcalendar/FileOperationHelper;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/FileOperationHelper;->getVEventsCount()I

    move-result v4

    iput v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$002(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;I)I

    invoke-static {v1}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$000(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;)I

    move-result v4

    if-gtz v4, :cond_3

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: No VEvent exsits in the file."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    const/4 v5, 0x2

    invoke-interface {v4, v7, v8, v5}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$102(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$202(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;

    :try_start_2
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$302(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;J)J

    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/mediatek/vcalendar/component/VEvent;->getTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;->access$402(Lcom/mediatek/vcalendar/VCalParser$PreviewInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    iget-object v4, p0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    iget v5, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    iget v6, p0, Lcom/mediatek/vcalendar/VCalParser;->mTotalCnt:I

    invoke-interface {v4, v5, v6, v1}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    goto/16 :goto_1

    :catch_3
    move-exception v0

    const-string v4, "VCalParser"

    const-string v5, "startParsePreview: vEvent.getTime failed."

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method public startParseVcsContent()V
    .locals 22

    const-string v18, "VCalParser"

    const-string v19, "startParseVcsContent"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "BEGIN:VEVENT"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "END:VEVENT"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    :cond_0
    const-string v18, "VCalParser"

    const-string v19, "startParseVcsContent: the given Content do not contains a VEvent."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "VCalParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "The failed string : \n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "\r\n"

    const-string v20, "\n"

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "\r"

    const-string v20, "\n"

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/mediatek/vcalendar/VCalParser;->parseVCalVersion(Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v12

    :goto_1
    if-ge v10, v12, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "BEGIN:VEVENT"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "END:VEVENT"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v18

    const-string v19, "END:VEVENT"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v5, v18, v19

    add-int/lit8 v10, v5, 0x1

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_2

    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_4

    :cond_2
    const-string v18, "VCalParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "end parse or error, start="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "; end="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    sget-object v18, Lcom/mediatek/vcalendar/component/VCalendar;->TIMEZONE_LIST:Ljava/util/ArrayList;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mVcsString:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    :try_start_0
    invoke-static/range {v16 .. v16}, Lcom/mediatek/vcalendar/component/VComponentBuilder;->buildEvent(Ljava/lang/String;)Lcom/mediatek/vcalendar/component/VEvent;
    :try_end_0
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    :goto_2
    if-nez v17, :cond_5

    const-string v18, "VCalParser"

    const-string v19, "startParse: buildEvents failed, vEvent = null"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v4

    const-string v18, "VCalParser"

    const-string v19, "startAccountCompose: BuileEvent failed"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :cond_5
    new-instance v18, Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mCurrentAccountName:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-direct/range {v18 .. v21}, Lcom/mediatek/vcalendar/DbOperationHelper;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/vcalendar/DbOperationHelper;->getCalendarIdList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_6

    const-string v18, "VCalParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "startParseVcsContent: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " calendars exist in the given account."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    :goto_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1

    invoke-interface/range {v18 .. v21}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationExceptionOccured(III)V

    goto/16 :goto_0

    :cond_6
    const-string v18, "VCalParser"

    const-string v19, "startParseVcsContent: the given calendar account does not exsit."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;

    invoke-direct {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;-><init>()V

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v18

    const-string v19, "calendar_id"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mCalendarId:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v14, -0x1

    const-wide/16 v6, -0x1

    :try_start_1
    invoke-virtual {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getEventValues()Landroid/content/ContentValues;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/vcalendar/component/VEvent;->toEventsContentValue(Landroid/content/ContentValues;)V

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAlarmsList()Ljava/util/LinkedList;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/vcalendar/component/VEvent;->toAlarmsContentValue(Ljava/util/LinkedList;)V

    invoke-virtual {v3}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAttendeesList()Ljava/util/LinkedList;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/vcalendar/component/VEvent;->toAttendeesContentValue(Ljava/util/LinkedList;)V

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/vcalendar/component/VEvent;->getDtStart()J

    move-result-wide v14

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/vcalendar/component/VEvent;->getDtEnd()J
    :try_end_1
    .catch Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v6

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mDbOperationHelper:Lcom/mediatek/vcalendar/DbOperationHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/mediatek/vcalendar/DbOperationHelper;->addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)Landroid/net/Uri;

    move-result-object v9

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string v18, "key_event_id"

    invoke-static {v9}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v19

    move-object/from16 v0, v18

    move-wide/from16 v1, v19

    invoke-virtual {v8, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v18, "key_start_millis"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v18, "key_end_millis"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vcalendar/VCalParser;->mListener:Lcom/mediatek/vcalendar/VCalStatusChangeOperator;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-interface {v0, v1, v2, v8}, Lcom/mediatek/vcalendar/VCalStatusChangeOperator;->vCalOperationFinished(IILjava/lang/Object;)V

    goto/16 :goto_1

    :catch_1
    move-exception v4

    const-string v18, "VCalParser"

    const-string v19, "startParse: VEvent to contentvalues failed"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4
.end method
