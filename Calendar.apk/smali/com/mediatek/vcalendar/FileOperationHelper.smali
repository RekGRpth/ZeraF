.class public Lcom/mediatek/vcalendar/FileOperationHelper;
.super Ljava/lang/Object;
.source "FileOperationHelper.java"


# static fields
.field private static final READ_MODE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FileOperationHelper"

.field private static final WRITE_MODE:I = 0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDstFile:Ljava/io/File;

.field private mIsClosed:Z

.field private final mMode:I

.field private mReader:Lcom/mediatek/vcalendar/VCalFileReader;

.field private final mSrcUri:Landroid/net/Uri;

.field private mVEventCount:I

.field private mWriter:Ljava/io/PrintWriter;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mIsClosed:Z

    iput-object p1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mSrcUri:Landroid/net/Uri;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mDstFile:Ljava/io/File;

    iput v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mMode:I

    iput-object p2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/vcalendar/VCalFileReader;

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mSrcUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/vcalendar/VCalFileReader;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Landroid/content/Context;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mIsClosed:Z

    iput-object p1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mDstFile:Ljava/io/File;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mSrcUri:Landroid/net/Uri;

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mMode:I

    iput-object p2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/PrintWriter;

    iget-object v2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mDstFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v1, "FileOperationHelper"

    const-string v2, ": FileNotFoundException!"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private writeVEvent(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BEGIN:VEVENT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "END:VEVENT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "FileOperationHelper"

    const-string v1, "writeVEvent: the given str is not a VEvent String"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FileOperationHelper"

    const-string v1, "writeVEvent: The event is not exsited in the given str."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addNextVEventString(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mMode:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const-string v1, "FileOperationHelper"

    const-string v2, "Current File operate mode is READ_MODE, Must not call WriteMode method"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    if-nez v1, :cond_2

    :try_start_0
    new-instance v1, Ljava/io/PrintWriter;

    iget-object v2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mDstFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    invoke-static {p1}, Lcom/mediatek/vcalendar/StringUtil;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lcom/mediatek/vcalendar/FileOperationHelper;->writeVEvent(Ljava/lang/String;)V

    :cond_3
    if-eqz p2, :cond_0

    const-string v1, "FileOperationHelper"

    const-string v2, "addNextVEventString: the flow ended!"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v1, "FileOperationHelper"

    const-string v2, ": FileNotFoundException!"

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public close()V
    .locals 4

    const/4 v3, 0x1

    iget v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mMode:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mIsClosed:Z

    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    invoke-virtual {v1}, Lcom/mediatek/vcalendar/VCalFileReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v1, "FileOperationHelper"

    const-string v2, "Close File failed, IOException."

    invoke-static {v1, v2}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFirstEventStr()Ljava/lang/String;
    .locals 4

    const-string v2, "FileOperationHelper"

    const-string v3, "getFirstEventStr started."

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/VCalFileReader;->getFirstComponentInfo()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getNextVEventString()Ljava/lang/String;
    .locals 7

    const/4 v3, 0x0

    iget v4, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mMode:I

    if-eqz v4, :cond_0

    const-string v4, "FileOperationHelper"

    const-string v5, ":Current File operate mode is WRITE_MODE, Must not call ReadMode method"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v3

    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    invoke-virtual {v4}, Lcom/mediatek/vcalendar/VCalFileReader;->readNextComponent()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "FileOperationHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ":IOException when read nextVEvent, File: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mSrcUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getVEventsCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    invoke-virtual {v0}, Lcom/mediatek/vcalendar/VCalFileReader;->getComponentsCount()I

    move-result v0

    iput v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mVEventCount:I

    iget v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mVEventCount:I

    return v0
.end method

.method public hasNextVEvent()Z
    .locals 5

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mReader:Lcom/mediatek/vcalendar/VCalFileReader;

    invoke-virtual {v2}, Lcom/mediatek/vcalendar/VCalFileReader;->hasNextComponent()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "FileOperationHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException when judge whether has nextVEvent, File: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mSrcUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mIsClosed:Z

    return v0
.end method

.method public setVEventsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mVEventCount:I

    return-void
.end method

.method protected writeVCalendarHead()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarHead()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void
.end method

.method protected writeVCalendarTrail()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    invoke-static {}, Lcom/mediatek/vcalendar/component/VCalendar;->getVCalendarTrail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/FileOperationHelper;->mWriter:Ljava/io/PrintWriter;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void
.end method
