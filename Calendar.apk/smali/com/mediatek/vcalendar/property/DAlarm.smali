.class public Lcom/mediatek/vcalendar/property/DAlarm;
.super Lcom/mediatek/vcalendar/property/Property;
.source "DAlarm.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DAlarm"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "DALARM"

    invoke-direct {p0, v0, p1}, Lcom/mediatek/vcalendar/property/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DAlarm"

    const-string v1, "Constructor: DAlarm property created."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toAlarmsContentValue(Ljava/util/LinkedList;J)V
    .locals 8
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/ContentValues;",
            ">;J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;
        }
    .end annotation

    const-string v3, "DAlarm"

    const-string v4, "toAlarmsContentValue: started."

    invoke-static {v3, v4}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v3, "DAlarm"

    const-string v4, "toAlarmsContentValue: the argument ContentValue must not be null."

    invoke-static {v3, v4}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;

    invoke-direct {v3}, Lcom/mediatek/vcalendar/component/VComponentBuilder$FormatException;-><init>()V

    throw v3

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iget-object v3, p0, Lcom/mediatek/vcalendar/property/Property;->mValue:Ljava/lang/String;

    const-string v4, "UTC"

    invoke-static {v3, v4}, Lcom/mediatek/vcalendar/valuetype/DateTime;->getUtcTimeMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    sub-long v1, v3, p2

    const-string v3, "minutes"

    const-wide/16 v4, -0x1

    mul-long/2addr v4, v1

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "method"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method
