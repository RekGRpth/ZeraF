.class public interface abstract Lcom/mediatek/calendar/ext/ILunarExtension;
.super Ljava/lang/Object;
.source "ILunarExtension.java"


# virtual methods
.method public abstract canShowLunarCalendar()Z
.end method

.method public abstract getGregFestival(II)Ljava/lang/String;
.end method

.method public abstract getLunarFestival(II)Ljava/lang/String;
.end method

.method public abstract getSolarTermNameByIndex(I)Ljava/lang/String;
.end method

.method public abstract getSpecialWord(I)Ljava/lang/String;
.end method
