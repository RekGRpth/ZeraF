.class Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "LunarDatePicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/calendar/lunar/LunarDatePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDay:I

.field private final mMonth:I

.field private final mYear:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState$1;

    invoke-direct {v0}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState$1;-><init>()V

    sput-object v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mYear:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mMonth:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mDay:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mediatek/calendar/lunar/LunarDatePicker$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mediatek/calendar/lunar/LunarDatePicker$1;

    invoke-direct {p0, p1}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;III)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    iput p2, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mYear:I

    iput p3, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mMonth:I

    iput p4, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mDay:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;IIILcom/mediatek/calendar/lunar/LunarDatePicker$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mediatek/calendar/lunar/LunarDatePicker$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;-><init>(Landroid/os/Parcelable;III)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mYear:I

    return v0
.end method

.method static synthetic access$1400(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mMonth:I

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;)I
    .locals 1
    .param p0    # Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mDay:I

    return v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/AbsSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$SavedState;->mDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
