.class public Lcom/android/calendar/ContactsAsyncHelper;
.super Landroid/os/Handler;
.source "ContactsAsyncHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/ContactsAsyncHelper$1;,
        Lcom/android/calendar/ContactsAsyncHelper$WorkerHandler;,
        Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;,
        Lcom/android/calendar/ContactsAsyncHelper$OnImageLoadCompleteListener;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DEFAULT_TOKEN:I = -0x1

.field private static final EVENT_LOAD_DRAWABLE:I = 0x2

.field private static final EVENT_LOAD_IMAGE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "ContactsAsyncHelper"

.field private static mInstance:Lcom/android/calendar/ContactsAsyncHelper;

.field private static sThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/ContactsAsyncHelper;->mInstance:Lcom/android/calendar/ContactsAsyncHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ContactsAsyncWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Lcom/android/calendar/ContactsAsyncHelper$WorkerHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/ContactsAsyncHelper$WorkerHandler;-><init>(Lcom/android/calendar/ContactsAsyncHelper;Landroid/os/Looper;)V

    sput-object v1, Lcom/android/calendar/ContactsAsyncHelper;->sThreadHandler:Landroid/os/Handler;

    return-void
.end method

.method public static final retrieveContactPhotoAsync(Landroid/content/Context;Lcom/android/calendar/event/EditEventHelper$AttendeeItem;Ljava/lang/Runnable;Landroid/net/Uri;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/calendar/event/EditEventHelper$AttendeeItem;
    .param p2    # Ljava/lang/Runnable;
    .param p3    # Landroid/net/Uri;

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;-><init>(Lcom/android/calendar/ContactsAsyncHelper$1;)V

    iput-object p0, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->context:Landroid/content/Context;

    iput-object p1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->item:Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iput-object p3, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->uri:Landroid/net/Uri;

    iput-object p2, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->callback:Ljava/lang/Runnable;

    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->mInstance:Lcom/android/calendar/ContactsAsyncHelper;

    if-nez v2, :cond_1

    new-instance v2, Lcom/android/calendar/ContactsAsyncHelper;

    invoke-direct {v2}, Lcom/android/calendar/ContactsAsyncHelper;-><init>()V

    sput-object v2, Lcom/android/calendar/ContactsAsyncHelper;->mInstance:Lcom/android/calendar/ContactsAsyncHelper;

    :cond_1
    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->sThreadHandler:Landroid/os/Handler;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->arg1:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->sThreadHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public static final updateImageViewWithContactPhotoAsync(Landroid/content/Context;Landroid/widget/ImageView;Landroid/net/Uri;I)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    if-nez p2, :cond_0

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;-><init>(Lcom/android/calendar/ContactsAsyncHelper$1;)V

    iput-object p0, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->context:Landroid/content/Context;

    iput-object p1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->view:Landroid/widget/ImageView;

    iput-object p2, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->uri:Landroid/net/Uri;

    iput p3, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->defaultResource:I

    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->mInstance:Lcom/android/calendar/ContactsAsyncHelper;

    if-nez v2, :cond_1

    new-instance v2, Lcom/android/calendar/ContactsAsyncHelper;

    invoke-direct {v2}, Lcom/android/calendar/ContactsAsyncHelper;-><init>()V

    sput-object v2, Lcom/android/calendar/ContactsAsyncHelper;->mInstance:Lcom/android/calendar/ContactsAsyncHelper;

    :cond_1
    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->sThreadHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->arg1:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eq p3, v3, :cond_2

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    sget-object v2, Lcom/android/calendar/ContactsAsyncHelper;->sThreadHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;

    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->result:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->view:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->view:Landroid/widget/ImageView;

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->result:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->defaultResource:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->view:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->view:Landroid/widget/ImageView;

    iget v2, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->defaultResource:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->result:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->item:Lcom/android/calendar/event/EditEventHelper$AttendeeItem;

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->result:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    iput-object v1, v2, Lcom/android/calendar/event/EditEventHelper$AttendeeItem;->mBadge:Landroid/graphics/drawable/Drawable;

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->callback:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/calendar/ContactsAsyncHelper$WorkerArgs;->callback:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
