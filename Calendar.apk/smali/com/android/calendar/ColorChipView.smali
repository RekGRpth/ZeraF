.class public Lcom/android/calendar/ColorChipView;
.super Landroid/view/View;
.source "ColorChipView.java"


# static fields
.field private static final DEF_BORDER_WIDTH:I = 0x4

.field public static final DRAW_BORDER:I = 0x1

.field public static final DRAW_FADED:I = 0x2

.field public static final DRAW_FULL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ColorChipView"


# instance fields
.field mBorderWidth:I

.field mColor:I

.field private mDefStrokeWidth:F

.field private mDrawStyle:I

.field private mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/ColorChipView;->mDrawStyle:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    invoke-direct {p0}, Lcom/android/calendar/ColorChipView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/ColorChipView;->mDrawStyle:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    invoke-direct {p0}, Lcom/android/calendar/ColorChipView;->init()V

    return-void
.end method

.method private init()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    iput v0, p0, Lcom/android/calendar/ColorChipView;->mDefStrokeWidth:F

    iget-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/lit8 v12, v0, -0x1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    iget-object v2, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/android/calendar/ColorChipView;->mDrawStyle:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    iget v0, p0, Lcom/android/calendar/ColorChipView;->mColor:I

    invoke-static {v0}, Lcom/android/calendar/Utils;->getDeclinedColorFromColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, p0, Lcom/android/calendar/ColorChipView;->mDrawStyle:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/android/calendar/ColorChipView;->mColor:I

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/ColorChipView;->mDefStrokeWidth:F

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    int-to-float v3, v12

    int-to-float v4, v6

    iget-object v5, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :pswitch_1
    iget v0, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    div-int/lit8 v7, v0, 0x2

    move v13, v7

    move v8, v7

    iget-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/16 v0, 0x10

    new-array v9, v0, [F

    const/4 v10, 0x0

    add-int/lit8 v11, v10, 0x1

    aput v1, v9, v10

    add-int/lit8 v10, v11, 0x1

    int-to-float v0, v13

    aput v0, v9, v11

    add-int/lit8 v11, v10, 0x1

    int-to-float v0, v12

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    int-to-float v0, v13

    aput v0, v9, v11

    add-int/lit8 v11, v10, 0x1

    aput v1, v9, v10

    add-int/lit8 v10, v11, 0x1

    sub-int v0, v6, v7

    int-to-float v0, v0

    aput v0, v9, v11

    add-int/lit8 v11, v10, 0x1

    int-to-float v0, v12

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    sub-int v0, v6, v7

    int-to-float v0, v0

    aput v0, v9, v11

    add-int/lit8 v11, v10, 0x1

    int-to-float v0, v8

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    aput v1, v9, v11

    add-int/lit8 v11, v10, 0x1

    int-to-float v0, v8

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    int-to-float v0, v6

    aput v0, v9, v11

    add-int/lit8 v11, v10, 0x1

    sub-int v0, v12, v7

    int-to-float v0, v0

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    aput v1, v9, v11

    add-int/lit8 v11, v10, 0x1

    sub-int v0, v12, v7

    int-to-float v0, v0

    aput v0, v9, v10

    add-int/lit8 v10, v11, 0x1

    int-to-float v0, v6

    aput v0, v9, v11

    iget-object v0, p0, Lcom/android/calendar/ColorChipView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setBorderWidth(I)V
    .locals 0
    .param p1    # I

    if-ltz p1, :cond_0

    iput p1, p0, Lcom/android/calendar/ColorChipView;->mBorderWidth:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/ColorChipView;->mColor:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setDrawStyle(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/calendar/ColorChipView;->mDrawStyle:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method
