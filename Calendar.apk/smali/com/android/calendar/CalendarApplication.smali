.class public Lcom/android/calendar/CalendarApplication;
.super Landroid/app/Application;
.source "CalendarApplication.java"


# static fields
.field private static sInjectedServices:Lcom/mediatek/calendar/InjectedServices;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static injectServices(Lcom/mediatek/calendar/InjectedServices;)V
    .locals 0
    .param p0    # Lcom/mediatek/calendar/InjectedServices;

    sput-object p0, Lcom/android/calendar/CalendarApplication;->sInjectedServices:Lcom/mediatek/calendar/InjectedServices;

    return-void
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/calendar/CalendarApplication;->sInjectedServices:Lcom/mediatek/calendar/InjectedServices;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/calendar/CalendarApplication;->sInjectedServices:Lcom/mediatek/calendar/InjectedServices;

    invoke-virtual {v1, p1}, Lcom/mediatek/calendar/InjectedServices;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {p0}, Lcom/android/calendar/GeneralPreferences;->setDefaultValues(Landroid/content/Context;)V

    const-string v0, "preferences_version"

    invoke-static {p0}, Lcom/android/calendar/Utils;->getVersionCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/Utils;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
