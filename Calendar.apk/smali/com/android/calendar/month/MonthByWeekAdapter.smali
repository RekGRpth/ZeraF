.class public Lcom/android/calendar/month/MonthByWeekAdapter;
.super Lcom/android/calendar/month/SimpleWeeksAdapter;
.source "MonthByWeekAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/month/MonthByWeekAdapter$CalendarGestureListener;
    }
.end annotation


# static fields
.field private static final ANIMATE_TODAY_TIMEOUT:J = 0x3e8L

.field protected static DEFAULT_QUERY_DAYS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MonthByWeek"

.field public static final WEEK_PARAMS_IS_MINI:Ljava/lang/String; = "mini_month"

.field private static mMovedPixelToCancel:F = 0.0f

.field private static mOnDownDelay:I = 0x0

.field private static final mOnTapDelay:I = 0x64

.field private static mTotalClickDelay:I


# instance fields
.field private mAnimateSelectedDay:Z

.field private mAnimateTime:J

.field private mAnimateToday:Z

.field mClickTime:J

.field mClickedView:Lcom/android/calendar/month/MonthWeekEventsView;

.field mClickedXLocation:F

.field protected mController:Lcom/android/calendar/CalendarController;

.field private final mDoClick:Ljava/lang/Runnable;

.field private final mDoSingleTapUp:Ljava/lang/Runnable;

.field protected mEventDayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;"
        }
    .end annotation
.end field

.field protected mFirstJulianDay:I

.field protected mHomeTimeZone:Ljava/lang/String;

.field protected mIsMiniMonth:Z

.field protected mOrientation:I

.field protected mQueryDays:I

.field protected mRealSelectedDay:Landroid/text/format/Time;

.field protected mRealSelectedWeek:I

.field private final mShowAgendaWithMonth:Z

.field mSingleTapUpView:Lcom/android/calendar/month/MonthWeekEventsView;

.field protected mTempTime:Landroid/text/format/Time;

.field protected mToday:Landroid/text/format/Time;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x38

    sput v0, Lcom/android/calendar/month/MonthByWeekAdapter;->DEFAULT_QUERY_DAYS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/month/SimpleWeeksAdapter;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    iput-boolean v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mOrientation:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEvents:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateSelectedDay:Z

    iput-boolean v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateToday:Z

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateTime:J

    new-instance v1, Lcom/android/calendar/month/MonthByWeekAdapter$1;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/MonthByWeekAdapter$1;-><init>(Lcom/android/calendar/month/MonthByWeekAdapter;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mDoClick:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/calendar/month/MonthByWeekAdapter$2;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/MonthByWeekAdapter$2;-><init>(Lcom/android/calendar/month/MonthByWeekAdapter;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mDoSingleTapUp:Ljava/lang/Runnable;

    const-string v1, "mini_month"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mini_month"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    :cond_0
    const v1, 0x7f090001

    invoke-static {p1, v1}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mShowAgendaWithMonth:Z

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthByWeekAdapter;->mOnDownDelay:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    sput v1, Lcom/android/calendar/month/MonthByWeekAdapter;->mMovedPixelToCancel:F

    sget v1, Lcom/android/calendar/month/MonthByWeekAdapter;->mOnDownDelay:I

    add-int/lit8 v1, v1, 0x64

    sput v1, Lcom/android/calendar/month/MonthByWeekAdapter;->mTotalClickDelay:I

    return-void

    :cond_1
    move v1, v3

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/calendar/month/MonthByWeekAdapter;Lcom/android/calendar/month/MonthWeekEventsView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthByWeekAdapter;
    .param p1    # Lcom/android/calendar/month/MonthWeekEventsView;

    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthByWeekAdapter;->clearClickedView(Lcom/android/calendar/month/MonthWeekEventsView;)V

    return-void
.end method

.method private clearClickedView(Lcom/android/calendar/month/MonthWeekEventsView;)V
    .locals 2
    .param p1    # Lcom/android/calendar/month/MonthWeekEventsView;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mDoClick:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Lcom/android/calendar/month/MonthWeekEventsView;->clearClickedDay()V

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickedView:Lcom/android/calendar/month/MonthWeekEventsView;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private isHasSelectedDay(Landroid/text/format/Time;I)Z
    .locals 7
    .param p1    # Landroid/text/format/Time;
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    iget-wide v5, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iget-object v3, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v3

    invoke-static {v0, v3}, Landroid/text/format/Time;->getWeeksSinceEpochFromJulianDay(II)I

    move-result v1

    if-ne p2, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private sendEventsToView(Lcom/android/calendar/month/MonthWeekEventsView;)V
    .locals 7
    .param p1    # Lcom/android/calendar/month/MonthWeekEventsView;

    const/4 v4, 0x3

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "MonthByWeek"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "MonthByWeek"

    const-string v4, "No events loaded, did not pass any events to view."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1, v6, v6}, Lcom/android/calendar/month/MonthWeekEventsView;->setEvents(Ljava/util/List;Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/calendar/month/SimpleWeekView;->getFirstJulianDay()I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mFirstJulianDay:I

    sub-int v1, v2, v3

    iget v3, p1, Lcom/android/calendar/month/SimpleWeekView;->mNumDays:I

    add-int v0, v1, v3

    if-ltz v1, :cond_2

    iget-object v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v0, v3, :cond_4

    :cond_2
    const-string v3, "MonthByWeek"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "MonthByWeek"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Week is outside range of loaded events. viewStart: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " eventsStart: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mFirstJulianDay:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p1, v6, v6}, Lcom/android/calendar/month/MonthWeekEventsView;->setEvents(Ljava/util/List;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v0}, Ljava/util/AbstractList;->subList(II)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEvents:Ljava/util/ArrayList;

    invoke-virtual {p1, v3, v4}, Lcom/android/calendar/month/MonthWeekEventsView;->setEvents(Ljava/util/List;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private updateTimeZones()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mToday:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mToday:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mTempTime:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public animateSelectedDay()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateSelectedDay:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateTime:J

    return-void
.end method

.method public animateToday()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateToday:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateTime:J

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-eqz v9, :cond_0

    invoke-super/range {p0 .. p3}, Lcom/android/calendar/month/SimpleWeeksAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    :goto_0
    return-object v8

    :cond_0
    new-instance v5, Landroid/widget/AbsListView$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-direct {v5, v9, v10}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-eqz p2, :cond_8

    move-object v8, p2

    check-cast v8, Lcom/android/calendar/month/MonthWeekEventsView;

    iget-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateSelectedDay:Z

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    invoke-direct {p0, v9, p1}, Lcom/android/calendar/month/MonthByWeekAdapter;->isHasSelectedDay(Landroid/text/format/Time;I)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateTime:J

    sub-long v9, v0, v9

    const-wide/16 v11, 0x3e8

    cmp-long v9, v9, v11

    if-lez v9, :cond_6

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateSelectedDay:Z

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateToday:Z

    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateTime:J

    :goto_1
    if-nez v2, :cond_1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    :cond_1
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekAdapter;->setRealSelectedWeek()V

    const/4 v6, -0x1

    iget v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedWeek:I

    if-ne v9, p1, :cond_2

    iget-object v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    iget v6, v9, Landroid/text/format/Time;->weekDay:I

    :cond_2
    const/4 v7, -0x1

    iget v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedWeek:I

    if-ne v9, p1, :cond_3

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget v7, v9, Landroid/text/format/Time;->weekDay:I

    :cond_3
    const-string v9, "height"

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getHeight()I

    move-result v10

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTop()I

    move-result v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mNumWeeks:I

    div-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "selected_day"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "show_wk_num"

    iget-boolean v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mShowWeekNumber:Z

    if-eqz v9, :cond_9

    const/4 v9, 0x1

    :goto_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v10, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "week_start"

    iget v10, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mFirstDayOfWeek:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "num_days"

    iget v10, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mDaysPerWeek:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "week"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "focus_month"

    iget v10, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mFocusMonth:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "orientation"

    iget v10, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mOrientation:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v4, :cond_4

    const-string v9, "animate_today"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateToday:Z

    :cond_4
    if-eqz v3, :cond_5

    const-string v9, "animate_selected_day"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mAnimateSelectedDay:Z

    :cond_5
    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget-object v9, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/android/calendar/month/MonthWeekEventsView;->updateToday(Ljava/lang/String;)Z

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget-object v9, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v8, v2, v9}, Lcom/android/calendar/month/MonthWeekEventsView;->setWeekParams(Ljava/util/HashMap;Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/android/calendar/month/MonthByWeekAdapter;->sendEventsToView(Lcom/android/calendar/month/MonthWeekEventsView;)V

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x1

    const/4 v4, 0x1

    new-instance v8, Lcom/android/calendar/month/MonthWeekEventsView;

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/android/calendar/month/MonthWeekEventsView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    goto/16 :goto_1

    :cond_8
    new-instance v8, Lcom/android/calendar/month/MonthWeekEventsView;

    iget-object v9, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/android/calendar/month/MonthWeekEventsView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_2
.end method

.method protected init()V
    .locals 2

    invoke-super {p0}, Lcom/android/calendar/month/SimpleWeeksAdapter;->init()V

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mController:Lcom/android/calendar/CalendarController;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedWeek:I

    iput v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedWeek:I

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mToday:Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mToday:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mTempTime:Landroid/text/format/Time;

    return-void
.end method

.method protected onDayTapped(Landroid/text/format/Time;)V
    .locals 14
    .param p1    # Landroid/text/format/Time;

    const/4 v4, -0x1

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    iput-object v0, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    new-instance v13, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    invoke-direct {v13, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v0

    invoke-virtual {v13, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v0, v13, Landroid/text/format/Time;->hour:I

    iput v0, p1, Landroid/text/format/Time;->hour:I

    iget v0, v13, Landroid/text/format/Time;->minute:I

    iput v0, p1, Landroid/text/format/Time;->minute:I

    iput-boolean v8, p1, Landroid/text/format/Time;->allDay:Z

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->normalize(Z)J

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mShowAgendaWithMonth:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mController:Lcom/android/calendar/CalendarController;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    const-wide/16 v9, 0x1

    move-object v4, p1

    move-object v5, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mController:Lcom/android/calendar/CalendarController;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-eqz v5, :cond_2

    :goto_1
    const-wide/16 v9, 0x5

    move-object v4, p1

    move-object v5, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    :cond_2
    move v8, v4

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mController:Lcom/android/calendar/CalendarController;

    iget-object v1, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-eqz v5, :cond_4

    :goto_2
    const-wide/16 v9, 0x1

    move-object v4, p1

    move-object v5, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    :cond_4
    move v8, v4

    goto :goto_2
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    instance-of v3, p1, Lcom/android/calendar/month/MonthWeekEventsView;

    if-nez v3, :cond_0

    invoke-super {p0, p1, p2}, Lcom/android/calendar/month/SimpleWeeksAdapter;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v3

    :goto_0
    return v3

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iget-object v3, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    check-cast p1, Lcom/android/calendar/month/MonthWeekEventsView;

    iput-object p1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mSingleTapUpView:Lcom/android/calendar/month/MonthWeekEventsView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickTime:J

    sub-long v1, v3, v5

    iget-object v5, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mDoSingleTapUp:Ljava/lang/Runnable;

    sget v3, Lcom/android/calendar/month/MonthByWeekAdapter;->mTotalClickDelay:I

    int-to-long v3, v3

    cmp-long v3, v1, v3

    if-lez v3, :cond_1

    const-wide/16 v3, 0x0

    :goto_1
    invoke-virtual {v5, v6, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    sget v3, Lcom/android/calendar/month/MonthByWeekAdapter;->mTotalClickDelay:I

    int-to-long v3, v3

    sub-long/2addr v3, v1

    goto :goto_1

    :cond_2
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_2
    :pswitch_0
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_1
    check-cast p1, Lcom/android/calendar/month/MonthWeekEventsView;

    iput-object p1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickedView:Lcom/android/calendar/month/MonthWeekEventsView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickedXLocation:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickTime:J

    iget-object v3, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mDoClick:Ljava/lang/Runnable;

    sget v5, Lcom/android/calendar/month/MonthByWeekAdapter;->mOnDownDelay:I

    int-to-long v5, v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    :pswitch_2
    check-cast p1, Lcom/android/calendar/month/MonthWeekEventsView;

    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthByWeekAdapter;->clearClickedView(Lcom/android/calendar/month/MonthWeekEventsView;)V

    goto :goto_2

    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mClickedXLocation:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v4, Lcom/android/calendar/month/MonthByWeekAdapter;->mMovedPixelToCancel:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    check-cast p1, Lcom/android/calendar/month/MonthWeekEventsView;

    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthByWeekAdapter;->clearClickedView(Lcom/android/calendar/month/MonthWeekEventsView;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected refresh()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mFirstDayOfWeek:I

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getShowWeekNumber(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mShowWeekNumber:Z

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mHomeTimeZone:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mOrientation:I

    invoke-direct {p0}, Lcom/android/calendar/month/MonthByWeekAdapter;->updateTimeZones()V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setEvents(IILjava/util/ArrayList;)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x3

    iget-boolean v7, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mIsMiniMonth:Z

    if-eqz v7, :cond_1

    const-string v7, "MonthByWeek"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "MonthByWeek"

    const-string v8, "Attempted to set events for mini view. Events only supported in full view."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p3, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEvents:Ljava/util/ArrayList;

    iput p1, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mFirstJulianDay:I

    iput p2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mQueryDays:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_1
    if-ge v3, p2, :cond_2

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_5

    :cond_3
    const-string v7, "MonthByWeek"

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "MonthByWeek"

    const-string v8, "No events. Returning early--go schedule something fun."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-object v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekAdapter;->refresh()V

    goto :goto_0

    :cond_5
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/Event;

    iget v7, v1, Lcom/android/calendar/Event;->startDay:I

    iget v8, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mFirstJulianDay:I

    sub-int v6, v7, v8

    iget v7, v1, Lcom/android/calendar/Event;->endDay:I

    iget v8, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mFirstJulianDay:I

    sub-int/2addr v7, v8

    add-int/lit8 v0, v7, 0x1

    if-lt v6, p2, :cond_7

    if-ltz v0, :cond_6

    :cond_7
    if-gez v6, :cond_8

    const/4 v6, 0x0

    :cond_8
    if-gt v6, p2, :cond_6

    if-ltz v0, :cond_6

    if-le v0, p2, :cond_9

    move v0, p2

    :cond_9
    move v5, v6

    :goto_2
    if-ge v5, v0, :cond_6

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_a
    const-string v7, "MonthByWeek"

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_b

    const-string v7, "MonthByWeek"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Processed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " events."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iput-object v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mEventDayList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthByWeekAdapter;->refresh()V

    goto/16 :goto_0
.end method

.method public setRealSelectedDay(Landroid/text/format/Time;)V
    .locals 1
    .param p1    # Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    return-void
.end method

.method public setRealSelectedWeek()V
    .locals 4

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedDay:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mFirstDayOfWeek:I

    invoke-static {v2, v3}, Lcom/android/calendar/Utils;->getWeeksSinceEpochFromJulianDay(II)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/month/MonthByWeekAdapter;->mRealSelectedWeek:I

    return-void
.end method

.method public setSelectedDay(Landroid/text/format/Time;)V
    .locals 4
    .param p1    # Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v2, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedDay:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mFirstDayOfWeek:I

    invoke-static {v2, v3}, Lcom/android/calendar/Utils;->getWeeksSinceEpochFromJulianDay(II)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/month/SimpleWeeksAdapter;->mSelectedWeek:I

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
