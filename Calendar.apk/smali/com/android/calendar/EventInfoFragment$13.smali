.class Lcom/android/calendar/EventInfoFragment$13;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/calendar/EventInfoFragment;->setSNSData(Landroid/view/View;Landroid/widget/TextView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/calendar/EventInfoFragment;

.field final synthetic val$accountType:Ljava/lang/String;

.field final synthetic val$userId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/calendar/EventInfoFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment$13;->this$0:Lcom/android/calendar/EventInfoFragment;

    iput-object p2, p0, Lcom/android/calendar/EventInfoFragment$13;->val$accountType:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/calendar/EventInfoFragment$13;->val$userId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment$13;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/EventInfoFragment;->access$3200(Lcom/android/calendar/EventInfoFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment$13;->val$accountType:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment$13;->val$userId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/mediatek/calendar/SNSCalendarDataHelper;->getPostActionIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "EventInfoFragment"

    const-string v2, "sns icon clicked, start to post."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment$13;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/EventInfoFragment;->access$3300(Lcom/android/calendar/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "EventInfoFragment"

    const-string v2, "sns icon clicked, intent is null, can not post."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
