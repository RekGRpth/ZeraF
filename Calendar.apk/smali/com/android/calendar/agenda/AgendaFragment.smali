.class public Lcom/android/calendar/agenda/AgendaFragment;
.super Landroid/app/Fragment;
.source "AgendaFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/calendar/CalendarController$EventHandler;


# static fields
.field protected static final BUNDLE_KEY_RESTORE_INSTANCE_ID:Ljava/lang/String; = "key_restore_instance_id"

.field protected static final BUNDLE_KEY_RESTORE_TIME:Ljava/lang/String; = "key_restore_time"

.field protected static final BUNDLE_KEY_RESTORE_TOP_DEVIATION:Ljava/lang/String; = "key_restore_top_deviation"

.field private static DEBUG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

.field private mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

.field private mController:Lcom/android/calendar/CalendarController;

.field private mEventFragment:Lcom/android/calendar/EventInfoFragment;

.field private mForceReplace:Z

.field private final mInitialTimeMillis:J

.field private mIsTabletConfig:Z

.field mJulianDayOnTop:I

.field private mLastHandledEventId:J

.field private mLastHandledEventTime:Landroid/text/format/Time;

.field private mLastShownEventId:J

.field private mOnAttachAllDay:Z

.field private mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

.field private mOriginalTime:Landroid/text/format/Time;

.field private mQuery:Ljava/lang/String;

.field private mShowEventDetailsWithAgenda:Z

.field private final mTZUpdater:Ljava/lang/Runnable;

.field private final mTime:Landroid/text/format/Time;

.field private mTimeZone:Ljava/lang/String;

.field private mUsedForSearch:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/calendar/agenda/AgendaFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/agenda/AgendaFragment;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/agenda/AgendaFragment;-><init>(JZ)V

    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 5
    .param p1    # J
    .param p3    # Z

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mUsedForSearch:Z

    iput-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachAllDay:Z

    iput-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mForceReplace:Z

    iput-wide v3, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastShownEventId:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mJulianDayOnTop:I

    new-instance v0, Lcom/android/calendar/agenda/AgendaFragment$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/AgendaFragment$1;-><init>(Lcom/android/calendar/agenda/AgendaFragment;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTZUpdater:Ljava/lang/Runnable;

    iput-wide v3, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventId:J

    iput-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    iput-wide p1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mInitialTimeMillis:J

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mInitialTimeMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iput-boolean p3, p0, Lcom/android/calendar/agenda/AgendaFragment;->mUsedForSearch:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-wide v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mInitialTimeMillis:J

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/calendar/agenda/AgendaFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaFragment;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/calendar/agenda/AgendaFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTimeZone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/calendar/agenda/AgendaFragment;)Landroid/text/format/Time;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaFragment;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/agenda/AgendaFragment;)Landroid/text/format/Time;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaFragment;

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaFragment;->getTimeOnTopEvent()Landroid/text/format/Time;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/calendar/agenda/AgendaFragment;)Lcom/android/calendar/CalendarController;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaFragment;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    return-object v0
.end method

.method private getTimeOnTopEvent()Landroid/text/format/Time;
    .locals 2

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTimeZone:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mJulianDayOnTop:I

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->setJulianDay(I)J

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->hour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->minute:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOriginalTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->second:I

    iput v1, v0, Landroid/text/format/Time;->second:I

    return-object v0
.end method

.method private goTo(Lcom/android/calendar/CalendarController$EventInfo;Z)V
    .locals 12
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;
    .param p2    # Z

    const/4 v5, 0x0

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-wide v2, p1, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaFragment;->mQuery:Ljava/lang/String;

    iget-wide v8, p1, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    const-wide/16 v10, 0x8

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_2
    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaListView;->goTo(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedViewHolder()Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-boolean v0, v7, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->allDay:Z

    :goto_3
    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mForceReplace:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/agenda/AgendaFragment;->showEventInfo(Lcom/android/calendar/CalendarController$EventInfo;ZZ)V

    iput-boolean v5, p0, Lcom/android/calendar/agenda/AgendaFragment;->mForceReplace:Z

    goto :goto_1

    :cond_3
    move v6, v5

    goto :goto_2

    :cond_4
    move v0, v5

    goto :goto_3
.end method

.method private search(Ljava/lang/String;Landroid/text/format/Time;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/text/format/Time;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mQuery:Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, p2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const-wide/16 v2, -0x1

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaFragment;->mQuery:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaListView;->goTo(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    goto :goto_0
.end method

.method private showEventInfo(Lcom/android/calendar/CalendarController$EventInfo;ZZ)V
    .locals 20
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;
    .param p2    # Z
    .param p3    # Z

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    sget-object v2, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showEventInfo, event ID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaFragment;->mLastShownEventId:J

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v16

    if-nez v16, :cond_2

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachAllDay:Z

    goto :goto_0

    :cond_2
    invoke-virtual/range {v16 .. v16}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v17

    if-eqz p2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const-string v3, "UTC"

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const-string v3, "UTC"

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v18

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v13

    const v2, 0x7f100011

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v15

    check-cast v15, Lcom/android/calendar/EventInfoFragment;

    if-eqz v15, :cond_4

    if-nez p3, :cond_4

    invoke-virtual {v15}, Lcom/android/calendar/EventInfoFragment;->getStartMillis()J

    move-result-wide v2

    cmp-long v2, v2, v18

    if-nez v2, :cond_4

    invoke-virtual {v15}, Lcom/android/calendar/EventInfoFragment;->getEndMillis()J

    move-result-wide v2

    cmp-long v2, v2, v13

    if-nez v2, :cond_4

    invoke-virtual {v15}, Lcom/android/calendar/EventInfoFragment;->getEventId()J

    move-result-wide v2

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    :cond_4
    new-instance v2, Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct/range {v2 .. v12}, Lcom/android/calendar/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaFragment;->mEventFragment:Lcom/android/calendar/EventInfoFragment;

    const v2, 0x7f100011

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaFragment;->mEventFragment:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    const v3, 0x7f100011

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/AgendaFragment;->mEventFragment:Lcom/android/calendar/EventInfoFragment;

    invoke-virtual {v2, v3, v4}, Lcom/android/calendar/CalendarController;->registerEventHandler(ILcom/android/calendar/CalendarController$EventHandler;)V

    invoke-virtual/range {v17 .. v17}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v15}, Lcom/android/calendar/EventInfoFragment;->reloadEvents()V

    goto/16 :goto_0
.end method


# virtual methods
.method public eventsChanged()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->refresh(Z)V

    :cond_0
    return-void
.end method

.method protected extFindListView(Landroid/view/View;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f100010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method

.method protected extInflateFragmentView(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;

    const v0, 0x7f040007

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getLastShowEventId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastShownEventId:J

    return-wide v0
.end method

.method public getSupportedEventTypes()J
    .locals 4

    const-wide/16 v2, 0xa0

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mUsedForSearch:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x100

    :goto_0
    or-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public handleEvent(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 4
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventId:J

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    :goto_0
    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaFragment;->goTo(Lcom/android/calendar/CalendarController$EventInfo;Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    goto :goto_0

    :cond_2
    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v2, 0x100

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->query:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/agenda/AgendaFragment;->search(Ljava/lang/String;Landroid/text/format/Time;)V

    goto :goto_1

    :cond_3
    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFragment;->eventsChanged()V

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTimeZone:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTimeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachAllDay:Z

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/agenda/AgendaFragment;->showEventInfo(Lcom/android/calendar/CalendarController$EventInfo;ZZ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mOnAttachedInfo:Lcom/android/calendar/CalendarController$EventInfo;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v4, -0x1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    :goto_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    const v3, 0x7f090006

    invoke-static {v2, v3}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mIsTabletConfig:Z

    if-eqz p1, :cond_0

    const-string v2, "key_restore_time"

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    sget-boolean v2, Lcom/android/calendar/agenda/AgendaFragment;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Restoring time to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v4}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    const v3, 0x7f090003

    invoke-static {v2, v3}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v13}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    iget v10, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/agenda/AgendaFragment;->extInflateFragmentView(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/android/calendar/agenda/AgendaFragment;->extFindListView(Landroid/view/View;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/view/View;->setClickable(Z)V

    if-eqz p3, :cond_1

    const-string v13, "key_restore_instance_id"

    const-wide/16 v14, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13, v14, v15}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v13, -0x1

    cmp-long v13, v4, v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v13, v4, v5}, Lcom/android/calendar/agenda/AgendaListView;->setSelectedInstanceId(J)V

    :cond_0
    const-string v13, "key_restore_top_deviation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v7

    invoke-static {v7}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setTopDeviation([I)V

    :cond_1
    const v13, 0x7f100011

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    if-nez v13, :cond_2

    const/16 v13, 0x8

    invoke-virtual {v3, v13}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const v13, 0x7f10000f

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/android/calendar/StickyHeaderListView;

    if-eqz v8, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v13}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/android/calendar/StickyHeaderListView;->setAdapter(Landroid/widget/Adapter;)V

    instance-of v13, v1, Landroid/widget/HeaderViewListAdapter;

    if-eqz v13, :cond_3

    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v13

    check-cast v13, Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v8, v13}, Lcom/android/calendar/StickyHeaderListView;->setIndexer(Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v8, v13}, Lcom/android/calendar/StickyHeaderListView;->setHeaderHeightListener(Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;)V

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Lcom/android/calendar/StickyHeaderListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f08003c

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v14}, Lcom/android/calendar/StickyHeaderListView;->setHeaderSeparator(II)V

    move-object v11, v8

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    if-nez v13, :cond_6

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v11, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    return-object v12

    :cond_3
    instance-of v13, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter;

    if-eqz v13, :cond_4

    check-cast v1, Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v8, v13}, Lcom/android/calendar/StickyHeaderListView;->setIndexer(Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v8, v13}, Lcom/android/calendar/StickyHeaderListView;->setHeaderHeightListener(Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;)V

    goto :goto_0

    :cond_4
    sget-object v13, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    const-string v14, "Cannot find HeaderIndexer for StickyHeaderListView"

    invoke-static {v13, v14}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    goto :goto_1

    :cond_6
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    mul-int/lit8 v13, v10, 0x4

    div-int/lit8 v13, v13, 0xa

    iput v13, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v11, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v13, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int v13, v10, v13

    iput v13, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 11

    const/4 v5, 0x1

    const-wide/16 v9, -0x1

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    sget-boolean v0, Lcom/android/calendar/agenda/AgendaFragment;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnResume to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v0, "preferences_hide_declined"

    invoke-interface {v8, v0, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v7}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventId:J

    cmp-long v0, v0, v9

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventId:J

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaFragment;->mQuery:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaListView;->goTo(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    iput-wide v9, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventId:J

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->onResume()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaFragment;->mQuery:Ljava/lang/String;

    move-wide v2, v9

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaListView;->goTo(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v12, 0x0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget-boolean v8, Lcom/android/calendar/agenda/AgendaFragment;->DEBUG:Z

    if-eqz v8, :cond_0

    sget-object v8, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OnSaveInstanceState start time: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v8, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mShowEventDetailsWithAgenda:Z

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    iget-object v9, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastHandledEventTime:Landroid/text/format/Time;

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    :goto_1
    const-string v8, "key_restore_time"

    invoke-virtual {p1, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v8, v6, v7}, Lcom/android/calendar/CalendarController;->setTime(J)V

    :cond_3
    :goto_2
    sget-boolean v8, Lcom/android/calendar/agenda/AgendaFragment;->DEBUG:Z

    if-eqz v8, :cond_4

    sget-object v8, Lcom/android/calendar/agenda/AgendaFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onSaveInstanceState "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v10}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v9, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v8, v9}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->saveTopDeviation(Landroid/text/format/Time;)[I

    move-result-object v3

    const-string v8, "key_restore_top_deviation"

    invoke-virtual {p1, v8, v3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v8}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedInstanceId()J

    move-result-wide v4

    cmp-long v8, v4, v12

    if-ltz v8, :cond_1

    const-string v8, "key_restore_instance_id"

    invoke-virtual {p1, v8, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v8, v6, v7}, Landroid/text/format/Time;->set(J)V

    goto :goto_1

    :cond_6
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v8}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleEvent()Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v8, v0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime(Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;)J

    move-result-wide v1

    cmp-long v8, v1, v12

    if-lez v8, :cond_7

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mTime:Landroid/text/format/Time;

    invoke-virtual {v8, v1, v2}, Landroid/text/format/Time;->set(J)V

    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v8, v1, v2}, Lcom/android/calendar/CalendarController;->setTime(J)V

    const-string v8, "key_restore_time"

    invoke-virtual {p1, v8, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_7
    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    iput-wide v8, p0, Lcom/android/calendar/agenda/AgendaFragment;->mLastShownEventId:J

    goto :goto_2
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaListView;->getJulianDayFromPosition(I)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mJulianDayOnTop:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mJulianDayOnTop:I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaFragment;->getTimeOnTopEvent()Landroid/text/format/Time;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/CalendarController;->setTime(J)V

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaFragment;->mIsTabletConfig:Z

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/calendar/agenda/AgendaFragment$2;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/AgendaFragment$2;-><init>(Lcom/android/calendar/agenda/AgendaFragment;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFragment;->mAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0, p2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setScrollState(I)V

    :cond_0
    return-void
.end method

.method public removeFragments(Landroid/app/FragmentManager;)V
    .locals 5
    .param p1    # Landroid/app/FragmentManager;

    const v4, 0x7f100011

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFragment;->mController:Lcom/android/calendar/CalendarController;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/CalendarController;->deregisterEventHandler(Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_1
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
