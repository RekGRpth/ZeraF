.class public Lcom/android/calendar/agenda/AgendaWindowAdapter;
.super Landroid/widget/BaseAdapter;
.source "AgendaWindowAdapter.java"

# interfaces
.implements Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;
.implements Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;,
        Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;,
        Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;,
        Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;
    }
.end annotation


# static fields
.field private static final AGENDA_SORT_ORDER:Ljava/lang/String; = "startDay ASC, begin ASC, title ASC"

.field static final BASICLOG:Z = false

.field static final DEBUGLOG:Z = false

.field private static final IDEAL_NUM_OF_EVENTS:I = 0x32

.field public static final INDEX_ALL_DAY:I = 0x3

.field public static final INDEX_BEGIN:I = 0x7

.field public static final INDEX_CAN_ORGANIZER_RESPOND:I = 0xf

.field public static final INDEX_COLOR:I = 0x5

.field public static final INDEX_END:I = 0x8

.field public static final INDEX_END_DAY:I = 0xb

.field public static final INDEX_EVENT_ID:I = 0x9

.field public static final INDEX_EVENT_LOCATION:I = 0x2

.field public static final INDEX_HAS_ALARM:I = 0x4

.field public static final INDEX_INSTANCE_ID:I = 0x0

.field public static final INDEX_ORGANIZER:I = 0xd

.field public static final INDEX_OWNER_ACCOUNT:I = 0xe

.field public static final INDEX_RRULE:I = 0x6

.field public static final INDEX_SELF_ATTENDEE_STATUS:I = 0xc

.field public static final INDEX_START_DAY:I = 0xa

.field public static final INDEX_TIME_ZONE:I = 0x10

.field public static final INDEX_TITLE:I = 0x1

.field private static final MAX_NUM_OF_ADAPTERS:I = 0x5

.field private static final MAX_QUERY_DURATION:I = 0x3c

.field private static final MIN_QUERY_DURATION:I = 0x7

.field private static final OFF_BY_ONE_BUG:I = 0x1

.field private static final PREFETCH_BOUNDARY:I = 0x1

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final QUERY_TYPE_CLEAN:I = 0x2

.field private static final QUERY_TYPE_NEWER:I = 0x1

.field private static final QUERY_TYPE_OLDER:I = 0x0

.field private static final RETRIES_ON_NO_DATA:I = 0x1

.field private static final SLOCK:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String; = "AgendaWindowAdapter"

.field private static sTopDeviationInfo:[I


# instance fields
.field headerFooterOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mAdapterInfos:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

.field mCleanQueryInitiated:Z

.field private final mContext:Landroid/content/Context;

.field private final mDataChangedHandler:Landroid/os/Handler;

.field private final mDataChangedRunnable:Ljava/lang/Runnable;

.field private mDoneSettingUpHeaderFooter:Z

.field private mEmptyCursorCount:I

.field private final mFooterView:Landroid/widget/TextView;

.field private final mFormatter:Ljava/util/Formatter;

.field private final mHeaderView:Landroid/widget/TextView;

.field private mHideDeclined:Z

.field private final mIsTabletConfig:Z

.field private final mItemRightMargin:F

.field private mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

.field mListViewScrollState:I

.field private mNewerRequests:I

.field private mNewerRequestsProcessed:I

.field private mOlderRequests:I

.field private mOlderRequestsProcessed:I

.field private final mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

.field private final mQueryQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private mRowCount:I

.field private mSearchQuery:Ljava/lang/String;

.field private mSelectedInstanceId:J

.field private final mSelectedItemBackgroundColor:I

.field private final mSelectedItemTextColor:I

.field private mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

.field private final mShowEventOnStart:Z

.field private mShuttingDown:Z

.field private mStickyHeaderSize:I

.field private final mStringBuilder:Ljava/lang/StringBuilder;

.field private final mTZUpdater:Ljava/lang/Runnable;

.field private mTimeZone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x2

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const-string v1, "eventLocation"

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const-string v1, "displayColor"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "end"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "canOrganizerRespond"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/Utils;->isJellybeanOrLater()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->PROJECTION:[Ljava/lang/String;

    const-string v1, "calendar_color"

    aput-object v1, v0, v4

    :cond_0
    new-array v0, v3, [I

    sput-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->SLOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/calendar/agenda/AgendaListView;
    .param p3    # Z

    const v6, 0x7f040008

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iput-boolean v4, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDoneSettingUpHeaderFooter:Z

    iput-boolean v4, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mCleanQueryInitiated:Z

    const/16 v1, 0x2c

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStickyHeaderSize:I

    new-instance v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$1;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/AgendaWindowAdapter$1;-><init>(Lcom/android/calendar/agenda/AgendaWindowAdapter;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDataChangedHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$2;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/AgendaWindowAdapter$2;-><init>(Lcom/android/calendar/agenda/AgendaWindowAdapter;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDataChangedRunnable:Ljava/lang/Runnable;

    iput v4, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mListViewScrollState:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    iput-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    new-instance v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$3;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/AgendaWindowAdapter$3;-><init>(Lcom/android/calendar/agenda/AgendaWindowAdapter;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->headerFooterOnTouchListener:Landroid/view/View$OnTouchListener;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mResources:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemBackgroundColor:I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f08003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemTextColor:I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mItemRightMargin:F

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f090006

    invoke-static {v1, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mIsTabletConfig:Z

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTZUpdater:Ljava/lang/Runnable;

    invoke-static {p1, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;-><init>(Lcom/android/calendar/agenda/AgendaWindowAdapter;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mFormatter:Ljava/util/Formatter;

    iput-boolean p3, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShowEventOnStart:Z

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShowEventOnStart:Z

    if-nez v1, :cond_0

    iput v4, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStickyHeaderSize:I

    :cond_0
    iput-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSearchQuery:Ljava/lang/String;

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {v0, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHeaderView:Landroid/widget/TextView;

    invoke-virtual {v0, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mFooterView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHeaderView:Landroid/widget/TextView;

    const v2, 0x7f0c008f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHeaderView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/calendar/agenda/AgendaWindowAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000()[I
    .locals 1

    sget-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/calendar/agenda/AgendaWindowAdapter;)J
    .locals 2
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/android/calendar/agenda/AgendaWindowAdapter;J)J
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)J
    .locals 2
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findInstanceIdFromPosition(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShowEventOnStart:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/calendar/agenda/AgendaWindowAdapter;Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;)Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCursorByPosition(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)I
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCursorPositionByPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/android/calendar/agenda/AgendaWindowAdapter;Landroid/database/Cursor;IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->buildEventInfoFromCursor(Landroid/database/Cursor;IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)J
    .locals 2
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findStartTimeFromPosition(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1900()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->SLOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHeaderView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mEmptyCursorCount:I

    return p1
.end method

.method static synthetic access$2104(Lcom/android/calendar/agenda/AgendaWindowAdapter;)I
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mEmptyCursorCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mEmptyCursorCount:I

    return v0
.end method

.method static synthetic access$2208(Lcom/android/calendar/agenda/AgendaWindowAdapter;)I
    .locals 2
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequestsProcessed:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequestsProcessed:I

    return v0
.end method

.method static synthetic access$2308(Lcom/android/calendar/agenda/AgendaWindowAdapter;)I
    .locals 2
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequestsProcessed:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequestsProcessed:I

    return v0
.end method

.method static synthetic access$2400(Lcom/android/calendar/agenda/AgendaWindowAdapter;II)V
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->updateHeaderFooter(II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/calendar/agenda/AgendaWindowAdapter;II)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->isInRange(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/android/calendar/agenda/AgendaWindowAdapter;Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->doQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->pruneAdapterInfo(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/calendar/agenda/AgendaWindowAdapter;)I
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    return v0
.end method

.method static synthetic access$2802(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    return p1
.end method

.method static synthetic access$2812(Lcom/android/calendar/agenda/AgendaWindowAdapter;I)I
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    return v0
.end method

.method static synthetic access$2902(Lcom/android/calendar/agenda/AgendaWindowAdapter;Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShuttingDown:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Landroid/content/res/Resources;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDoneSettingUpHeaderFooter:Z

    return v0
.end method

.method static synthetic access$402(Lcom/android/calendar/agenda/AgendaWindowAdapter;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDoneSettingUpHeaderFooter:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/calendar/agenda/AgendaWindowAdapter;Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mFooterView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/calendar/agenda/AgendaWindowAdapter;)Ljava/util/LinkedList;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/calendar/agenda/AgendaWindowAdapter;Landroid/text/format/Time;J)I
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaWindowAdapter;
    .param p1    # Landroid/text/format/Time;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findEventPositionNearestTime(Landroid/text/format/Time;J)I

    move-result v0

    return v0
.end method

.method private adjustQueryRange(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V
    .locals 5
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const v4, 0x259d23

    const v3, 0x253d8c

    const/4 v2, 0x2

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-eq v0, v2, :cond_0

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    if-le v0, v4, :cond_1

    iput v4, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    const-string v0, "AgendaWindowAdapter"

    const-string v1, "limitQueryData, reset end"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-eq v0, v2, :cond_2

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-nez v0, :cond_3

    :cond_2
    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    if-ge v0, v3, :cond_3

    iput v3, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    const-string v0, "AgendaWindowAdapter"

    const-string v1, "limitQueryData, reset start"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private buildEventInfoFromCursor(Landroid/database/Cursor;IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;
    .locals 9
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Z

    const-wide/16 v7, 0x0

    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    new-instance v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    invoke-direct {v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;-><init>()V

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->startDay:I

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    if-eqz v2, :cond_4

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    invoke-static {v4, v5, v7, v8}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    :cond_0
    :goto_2
    if-nez p3, :cond_1

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    if-eqz v2, :cond_5

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    invoke-static {v4, v5, v7, v8}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    :goto_3
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    :cond_1
    return-object v0

    :cond_2
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    if-eqz p3, :cond_0

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    iput v3, v1, Landroid/text/format/Time;->hour:I

    iput v3, v1, Landroid/text/format/Time;->minute:I

    iput v3, v1, Landroid/text/format/Time;->second:I

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    goto :goto_2

    :cond_5
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    goto :goto_3
.end method

.method private buildQuerySelection()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHideDeclined:Z

    if-eqz v0, :cond_0

    const-string v0, "visible=1 AND selfAttendeeStatus!=2"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "visible=1"

    goto :goto_0
.end method

.method private buildQueryUri(IILjava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    if-nez p3, :cond_1

    sget-object v1, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2

    :cond_1
    sget-object v1, Landroid/provider/CalendarContract$Instances;->CONTENT_SEARCH_BY_DAY_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private calculateQueryDuration(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    const/16 v0, 0x3c

    iget v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    if-eqz v1, :cond_0

    sub-int v1, p2, p1

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x32

    iget v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    div-int v0, v1, v2

    :cond_0
    const/16 v1, 0x3c

    if-le v0, v1, :cond_2

    const/16 v0, 0x3c

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    const/4 v0, 0x7

    goto :goto_0
.end method

.method private checkQueryRange(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z
    .locals 4
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v1, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->start:I

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v0, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->end:I

    iget v2, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-nez v2, :cond_0

    const v2, 0x253d8c

    if-le v1, v2, :cond_1

    :cond_0
    iget v2, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-ne v2, v3, :cond_2

    const v2, 0x259d23

    if-lt v0, v2, :cond_2

    :cond_1
    const-string v2, "AgendaWindowAdapter"

    const-string v3, "preHandleQuery: out of range, do nothing"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private doQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V
    .locals 11
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const/4 v4, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v10, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->start:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v8, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->end:I

    invoke-direct {p0, v10, v8}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->calculateQueryDuration(II)I

    move-result v9

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    const/16 v2, 0x14

    if-ge v0, v2, :cond_1

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    if-eq v0, v4, :cond_1

    iput v4, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->queryType:I

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    if-le v0, v10, :cond_0

    iput v10, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    :cond_0
    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    if-ge v0, v8, :cond_1

    iput v8, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->adjustQueryRange(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    iget v2, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    iget-object v4, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->searchQuery:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v4}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->buildQueryUri(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    sget-object v4, Lcom/android/calendar/agenda/AgendaWindowAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->buildQuerySelection()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay ASC, begin ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    add-int/lit8 v0, v10, -0x1

    iput v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    sub-int/2addr v0, v9

    iput v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    goto :goto_0

    :pswitch_1
    add-int/lit8 v0, v8, 0x1

    iput v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    iget v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    add-int/2addr v0, v9

    iput v0, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private findEventPositionNearestTime(Landroid/text/format/Time;J)I
    .locals 4
    .param p1    # Landroid/text/format/Time;
    .param p2    # J

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByTime(Landroid/text/format/Time;)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    invoke-virtual {v3, p1, p2, p3}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->findEventPositionNearestTime(Landroid/text/format/Time;J)I

    move-result v3

    add-int v1, v2, v3

    :cond_0
    return v1
.end method

.method private findInstanceIdFromPosition(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getInstanceId(I)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private findStartTimeFromPosition(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getStartTime(I)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private formatDateString(I)Ljava/lang/String;
    .locals 9
    .param p1    # I

    const/4 v1, 0x0

    new-instance v8, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v8, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    invoke-virtual {v8, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mFormatter:Ljava/util/Formatter;

    const v6, 0x10014

    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAdapterInfoByTime(Landroid/text/format/Time;)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;
    .locals 8
    .param p1    # Landroid/text/format/Time;

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v3

    iget-wide v6, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v3, v4, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v6, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->start:I

    if-gt v6, v0, :cond_0

    iget v6, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->end:I

    if-gt v0, v6, :cond_0

    monitor-exit v7

    :goto_0
    return-object v2

    :cond_1
    monitor-exit v7

    const/4 v2, 0x0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private getCursorByPosition(I)Landroid/database/Cursor;
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCursorPositionByPosition(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getCursorPosition(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method static getViewTitle(Landroid/view/View;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/view/View;

    const-string v1, ""

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iget-object v2, v3, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-eqz v3, :cond_0

    check-cast v3, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;

    iget-object v0, v3, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;->dateView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method private isInRange(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v2

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v0, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->start:I

    if-gt v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v0, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->end:I

    if-gt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private pruneAdapterInfo(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;
    .locals 8
    .param p1    # I

    iget-object v6, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    monitor-enter v6

    const/4 v3, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    const/4 v7, 0x5

    if-lt v5, v7, :cond_3

    const/4 v5, 0x1

    if-ne p1, v5, :cond_2

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-object v3, v0

    :cond_0
    :goto_0
    if-eqz v3, :cond_3

    iget-object v5, v3, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    if-eqz v5, :cond_1

    iget-object v5, v3, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_1
    monitor-exit v6

    move-object v4, v3

    :goto_1
    return-object v4

    :cond_2
    if-nez p1, :cond_0

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-object v3, v0

    const/4 v5, 0x0

    iput v5, v3, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->size:I

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_3
    :try_start_1
    iget v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    if-eqz v5, :cond_4

    const/4 v5, 0x2

    if-ne p1, v5, :cond_7

    :cond_4
    const/4 v5, 0x0

    iput v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    const/4 v1, 0x0

    :cond_5
    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    if-eqz v2, :cond_6

    iget-object v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    iget v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->size:I

    add-int/2addr v1, v5

    move-object v3, v2

    :cond_6
    if-nez v2, :cond_5

    if-eqz v3, :cond_7

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    iput v1, v3, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->size:I

    :cond_7
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v3

    goto :goto_1
.end method

.method private queueQuery(IILandroid/text/format/Time;Ljava/lang/String;IJ)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # J

    new-instance v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    invoke-direct {v0, p5}, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;-><init>(I)V

    iput-object p3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->goToTime:Landroid/text/format/Time;

    iput p1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->start:I

    iput p2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->end:I

    iput-object p4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->searchQuery:Ljava/lang/String;

    iput-wide p6, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->id:J

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z

    move-result v1

    return v1
.end method

.method private queueQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z
    .locals 4
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->checkQueryRange(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSearchQuery:Ljava/lang/String;

    iput-object v2, p1, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;->searchQuery:Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/agenda/AgendaWindowAdapter;->SLOCK:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->doQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)V

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public static setTopDeviation([I)V
    .locals 1
    .param p0    # [I

    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    sput-object v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    return-void
.end method

.method private updateHeaderFooter(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHeaderView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0090

    new-array v3, v6, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->formatDateString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mFooterView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0091

    new-array v3, v6, [Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->formatDateString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public OnHeaderHeightChanged(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStickyHeaderSize:I

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public close()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShuttingDown:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->pruneAdapterInfo(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mQueryHandler:Lcom/android/calendar/agenda/AgendaWindowAdapter$QueryHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :cond_0
    return-void
.end method

.method protected getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;
    .locals 5
    .param p1    # I

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v2, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    if-gt v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v2, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v4, v4, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->size:I

    add-int/2addr v2, v4

    if-ge p1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    monitor-exit v3

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAdapterInfos:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    iget v2, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    if-gt v2, p1, :cond_1

    iget v2, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    iget v4, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->size:I

    add-int/2addr v2, v4

    if-ge p1, v2, :cond_1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mLastUsedInfo:Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    return v0
.end method

.method public getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEventByPosition(IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;
    .locals 7
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    add-int/lit8 v4, p1, -0x1

    invoke-virtual {p0, v4}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v6, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v6, v4, v6

    invoke-virtual {v5, v6}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getCursorPosition(I)I

    move-result v0

    const/high16 v5, -0x80000000

    if-eq v0, v5, :cond_0

    const/4 v3, 0x0

    if-gez v0, :cond_2

    neg-int v0, v0

    const/4 v3, 0x1

    :cond_2
    iget-object v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    iget-object v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    invoke-direct {p0, v5, v0, v3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->buildEventInfoFromCursor(Landroid/database/Cursor;IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v1

    if-nez p2, :cond_0

    if-nez v3, :cond_0

    iget-object v5, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v6, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v6, v4, v6

    invoke-virtual {v5, v6}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->findJulianDayFromPosition(I)I

    move-result v5

    iput v5, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->startDay:I

    goto :goto_0
.end method

.method public getHeaderItemsNumber(I)I
    .locals 3
    .param p1    # I

    const/4 v1, -0x1

    if-ltz p1, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mIsTabletConfig:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getHeaderItemsCount(I)I

    move-result v1

    goto :goto_0
.end method

.method public getHeaderPositionFromItemPosition(I)I
    .locals 5
    .param p1    # I

    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mIsTabletConfig:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v4, p1, v4

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getHeaderPosition(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    add-int/2addr v2, v1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 8
    .param p1    # I

    const-wide/16 v2, -0x1

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v5, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v5, p1, v5

    invoke-virtual {v4, v5}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getCursorPosition(I)I

    move-result v0

    const/high16 v4, -0x80000000

    if-ne v0, v4, :cond_1

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    if-ltz v0, :cond_2

    iget-object v2, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v2, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    const/16 v3, 0x9

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x14

    iget-object v6, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->cursor:Landroid/database/Cursor;

    const/4 v7, 0x7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    long-to-int v4, v4

    shl-long/2addr v2, v4

    goto :goto_0

    :cond_2
    iget-object v2, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    invoke-virtual {v2, p1}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->findJulianDayFromPosition(I)I

    move-result v2

    int-to-long v2, v2

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getItemViewType(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSelectedInstanceId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    return-wide v0
.end method

.method public getSelectedViewHolder()Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    return-object v0
.end method

.method public getStickyHeaderHeight()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStickyHeaderSize:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mRowCount:I

    add-int/lit8 v12, v12, -0x1

    move/from16 v0, p1

    if-lt v0, v12, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequests:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequestsProcessed:I

    if-gt v12, v13, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequests:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequests:I

    new-instance v12, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const/4 v13, 0x1

    invoke-direct {v12, v13}, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;-><init>(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z

    :cond_0
    const/4 v12, 0x1

    move/from16 v0, p1

    if-ge v0, v12, :cond_1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequests:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequestsProcessed:I

    if-gt v12, v13, :cond_1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequests:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequests:I

    new-instance v12, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;

    const/4 v13, 0x0

    invoke-direct {v12, v13}, Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;-><init>(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(Lcom/android/calendar/agenda/AgendaWindowAdapter$QuerySpec;)Z

    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    iget v12, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v4, p1, v12

    iget-object v12, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v12, v4, v0, v1}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iget-object v12, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    invoke-virtual {v12, v4}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->isDayHeaderView(I)Z

    move-result v12

    if-eqz v12, :cond_2

    const v12, 0x7f10000b

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v12, 0x7f10000c

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v12, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    invoke-virtual {v12, v4}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->isFirstDayAfterYesterday(I)Z

    move-result v12

    if-eqz v12, :cond_4

    if-eqz v7, :cond_2

    if-eqz v5, :cond_2

    const/16 v12, 0x8

    invoke-virtual {v7, v12}, Landroid/view/View;->setVisibility(I)V

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mIsTabletConfig:Z

    if-nez v12, :cond_6

    :cond_3
    :goto_1
    return-object v9

    :cond_4
    if-eqz v7, :cond_2

    if-eqz v5, :cond_2

    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Landroid/view/View;->setVisibility(I)V

    const/16 v12, 0x8

    invoke-virtual {v5, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    const-string v12, "AgendaWindowAdapter"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "BUG: getAdapterInfoByPosition returned null!!! "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v12}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Bug! "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v9, v8

    goto :goto_0

    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    instance-of v12, v11, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v12, :cond_3

    move-object v10, v11

    check-cast v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    iget-wide v14, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->instanceId:J

    cmp-long v12, v12, v14

    if-nez v12, :cond_7

    const/4 v6, 0x1

    :goto_2
    iget-object v13, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->selectedMarker:Landroid/view/View;

    if-eqz v6, :cond_8

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShowEventOnStart:Z

    if-eqz v12, :cond_8

    const/4 v12, 0x0

    :goto_3
    invoke-virtual {v13, v12}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mShowEventOnStart:Z

    if-eqz v12, :cond_3

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/GridLayout$LayoutParams;

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemBackgroundColor:I

    invoke-virtual {v9, v12}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemTextColor:I

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemTextColor:I

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedItemTextColor:I

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v3, v12, v13, v14, v15}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_7
    const/4 v6, 0x0

    goto :goto_2

    :cond_8
    const/16 v12, 0x8

    goto :goto_3

    :cond_9
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mItemRightMargin:F

    float-to-int v14, v14

    const/4 v15, 0x0

    invoke-virtual {v3, v12, v13, v14, v15}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v12, v10, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->isEnabled(I)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public refresh(Landroid/text/format/Time;JLjava/lang/String;ZZ)V
    .locals 25
    .param p1    # Landroid/text/format/Time;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Z

    if-eqz p4, :cond_0

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSearchQuery:Ljava/lang/String;

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    move-object/from16 v0, p1

    iget-wide v5, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v9

    if-nez p5, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v9}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->isInRange(II)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/calendar/agenda/AgendaListView;->isEventVisible(Landroid/text/format/Time;J)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct/range {p0 .. p3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findEventPositionNearestTime(Landroid/text/format/Time;J)I

    move-result v20

    if-lez v20, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    add-int/lit8 v4, v20, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mStickyHeaderSize:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mListViewScrollState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    :cond_1
    if-eqz p6, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findInstanceIdFromPosition(I)J

    move-result-wide v21

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getSelectedInstanceId()J

    move-result-wide v3

    cmp-long v3, v21, v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setSelectedInstanceId(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDataChangedHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mDataChangedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCursorByPosition(I)Landroid/database/Cursor;

    move-result-object v23

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCursorPositionByPosition(I)I

    move-result v24

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->buildEventInfoFromCursor(Landroid/database/Cursor;IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v3

    const-wide/16 v5, 0x2

    move-object/from16 v0, v19

    iget-wide v7, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    move-object/from16 v0, v19

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    move-object/from16 v0, v19

    iget-wide v11, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v19

    iget-boolean v15, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    invoke-static {v4, v15}, Lcom/android/calendar/CalendarController$EventInfo;->buildViewExtraLong(IZ)J

    move-result-wide v15

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v17

    move-object/from16 v4, p0

    invoke-virtual/range {v3 .. v18}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    :cond_2
    new-instance v7, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mTimeZone:Ljava/lang/String;

    invoke-direct {v7, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v3

    const-wide/16 v5, 0x400

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object v8, v7

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mCleanQueryInitiated:Z

    if-eqz v3, :cond_5

    if-eqz p4, :cond_3

    :cond_5
    add-int/lit8 v10, v9, 0x7

    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mCleanQueryInitiated:Z

    const/4 v13, 0x2

    move-object/from16 v8, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p4

    move-wide/from16 v14, p2

    invoke-direct/range {v8 .. v15}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(IILandroid/text/format/Time;Ljava/lang/String;IJ)Z

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequests:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mOlderRequests:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p0

    move-object/from16 v14, p1

    move-object/from16 v15, p4

    move-wide/from16 v17, p2

    invoke-direct/range {v11 .. v18}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(IILandroid/text/format/Time;Ljava/lang/String;IJ)Z

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequests:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mNewerRequests:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v16, 0x1

    move-object/from16 v11, p0

    move-object/from16 v14, p1

    move-object/from16 v15, p4

    move-wide/from16 v17, p2

    invoke-direct/range {v11 .. v18}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->queueQuery(IILandroid/text/format/Time;Ljava/lang/String;IJ)Z

    goto :goto_0
.end method

.method public saveTopDeviation(Landroid/text/format/Time;)[I
    .locals 14
    .param p1    # Landroid/text/format/Time;

    iget-object v12, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v12}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v4

    iget-object v12, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v4, :cond_2

    const/4 v8, 0x1

    :goto_0
    add-int/lit8 v12, v4, -0x1

    invoke-virtual {p0, v12}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getItemViewType(I)I

    move-result v12

    if-nez v12, :cond_3

    const/4 v7, 0x1

    :goto_1
    if-nez v1, :cond_4

    const/4 v11, 0x0

    :goto_2
    if-nez v7, :cond_0

    if-eqz v8, :cond_1

    :cond_0
    if-eqz v7, :cond_6

    if-nez v1, :cond_5

    const/4 v0, 0x0

    :goto_3
    add-int/2addr v11, v0

    :cond_1
    const/4 v9, 0x0

    if-nez v7, :cond_a

    if-nez v8, :cond_a

    if-nez v1, :cond_9

    const/4 v10, 0x0

    :goto_4
    const-wide/16 v12, -0x1

    invoke-direct {p0, p1, v12, v13}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->findEventPositionNearestTime(Landroid/text/format/Time;J)I

    move-result v3

    move v6, v4

    :goto_5
    if-le v6, v3, :cond_b

    add-int/lit8 v12, v6, -0x1

    invoke-virtual {p0, v12}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getItemViewType(I)I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_b

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_5

    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v11

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_3

    :cond_6
    iget-object v12, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mAgendaListView:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_7

    const/4 v0, 0x0

    :goto_6
    if-nez v1, :cond_8

    const/4 v5, 0x0

    :goto_7
    add-int/2addr v11, v5

    goto :goto_3

    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_6

    :cond_8
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    goto :goto_7

    :cond_9
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v10

    goto :goto_4

    :cond_a
    const/4 v9, 0x1

    :cond_b
    sget-object v12, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    const/4 v13, 0x0

    aput v9, v12, v13

    sget-object v12, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    const/4 v13, 0x1

    aput v11, v12, v13

    sget-object v12, Lcom/android/calendar/agenda/AgendaWindowAdapter;->sTopDeviationInfo:[I

    return-object v12
.end method

.method public setHideDeclinedEvents(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mHideDeclined:Z

    return-void
.end method

.method public setScrollState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mListViewScrollState:I

    return-void
.end method

.method public setSelectedInstanceId(J)V
    .locals 1
    .param p1    # J

    iput-wide p1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    return-void
.end method

.method public setSelectedView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iget-wide v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iget-wide v3, v3, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->instanceId:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedVH:Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iget-wide v1, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->instanceId:J

    iput-wide v1, p0, Lcom/android/calendar/agenda/AgendaWindowAdapter;->mSelectedInstanceId:J

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
