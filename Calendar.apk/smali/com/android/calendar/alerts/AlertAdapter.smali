.class public Lcom/android/calendar/alerts/AlertAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "AlertAdapter.java"


# static fields
.field private static alertActivity:Lcom/android/calendar/alerts/AlertActivity;

.field private static mFirstTime:Z

.field private static mOtherColor:I

.field private static mPastEventColor:I

.field private static mTitleColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/alerts/AlertAdapter;->mFirstTime:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/alerts/AlertActivity;I)V
    .locals 1
    .param p1    # Lcom/android/calendar/alerts/AlertActivity;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    sput-object p1, Lcom/android/calendar/alerts/AlertAdapter;->alertActivity:Lcom/android/calendar/alerts/AlertActivity;

    return-void
.end method

.method public static updateView(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;JJZ)V
    .locals 17
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # Z

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v2, 0x7f10001d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const v2, 0x7f100015

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    const v2, 0x7f100016

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    sget-boolean v2, Lcom/android/calendar/alerts/AlertAdapter;->mFirstTime:Z

    if-eqz v2, :cond_0

    const v2, 0x7f08004e

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/AlertAdapter;->mPastEventColor:I

    const v2, 0x7f08004c

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/AlertAdapter;->mTitleColor:I

    const v2, 0x7f08004d

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/AlertAdapter;->mOtherColor:I

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/calendar/alerts/AlertAdapter;->mFirstTime:Z

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v2, p6, v2

    if-gez v2, :cond_6

    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mPastEventColor:I

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mPastEventColor:I

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mPastEventColor:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const v2, 0x7f0c0047

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v13

    if-eqz p8, :cond_7

    const/16 v7, 0x2012

    const-string v13, "UTC"

    :goto_1
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    or-int/lit16 v7, v7, 0x80

    :cond_3
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11, v13}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p4

    invoke-virtual {v11, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v2, v11, Landroid/text/format/Time;->isDst:I

    if-eqz v2, :cond_8

    const/4 v8, 0x1

    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    move-object/from16 v2, p0

    move-wide/from16 v3, p4

    move-wide/from16 v5, p6

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p8, :cond_4

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    if-eq v13, v2, :cond_4

    const-string v2, " "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v13}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v8, v4, v5}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    :cond_5
    const/16 v2, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void

    :cond_6
    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mTitleColor:I

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mOtherColor:I

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v2, Lcom/android/calendar/alerts/AlertAdapter;->mOtherColor:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_7
    const/16 v7, 0x11

    goto :goto_1

    :cond_8
    const/4 v8, 0x0

    goto :goto_2

    :cond_9
    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 14
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const v1, 0x7f10001c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v1, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/Utils;->getDisplayColorFromColor(I)I

    move-result v10

    invoke-virtual {v13, v10}, Landroid/view/View;->setBackgroundColor(I)V

    const v1, 0x7f10001e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const/16 v1, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v1, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v1, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v9, 0x1

    :goto_1
    move-object/from16 v1, p2

    move-object v2, p1

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/alerts/AlertAdapter;->updateView(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;JJZ)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1
.end method

.method protected onContentChanged()V
    .locals 1

    invoke-super {p0}, Landroid/widget/CursorAdapter;->onContentChanged()V

    sget-object v0, Lcom/android/calendar/alerts/AlertAdapter;->alertActivity:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->closeActivityIfEmpty()V

    return-void
.end method
