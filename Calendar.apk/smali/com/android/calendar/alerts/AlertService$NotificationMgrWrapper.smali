.class public Lcom/android/calendar/alerts/AlertService$NotificationMgrWrapper;
.super Lcom/android/calendar/alerts/NotificationMgr;
.source "AlertService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/AlertService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotificationMgrWrapper"
.end annotation


# instance fields
.field mNm:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 0
    .param p1    # Landroid/app/NotificationManager;

    invoke-direct {p0}, Lcom/android/calendar/alerts/NotificationMgr;-><init>()V

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertService$NotificationMgrWrapper;->mNm:Landroid/app/NotificationManager;

    return-void
.end method


# virtual methods
.method public cancel(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationMgrWrapper;->mNm:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public notify(ILcom/android/calendar/alerts/AlertService$NotificationWrapper;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/calendar/alerts/AlertService$NotificationWrapper;

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationMgrWrapper;->mNm:Landroid/app/NotificationManager;

    iget-object v1, p2, Lcom/android/calendar/alerts/AlertService$NotificationWrapper;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, p1, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
