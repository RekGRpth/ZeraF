.class public Lcom/android/calendar/DayView;
.super Landroid/view/View;
.source "DayView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/DayView$ScrollInterpolator;,
        Lcom/android/calendar/DayView$CalendarGestureListener;,
        Lcom/android/calendar/DayView$UpdateCurrentTime;,
        Lcom/android/calendar/DayView$DismissPopup;,
        Lcom/android/calendar/DayView$ContinueScroll;,
        Lcom/android/calendar/DayView$ContextMenuHandler;,
        Lcom/android/calendar/DayView$GotoBroadcaster;,
        Lcom/android/calendar/DayView$TodayAnimatorListener;
    }
.end annotation


# static fields
.field private static final ACCESS_LEVEL_DELETE:I = 0x1

.field private static final ACCESS_LEVEL_EDIT:I = 0x2

.field private static final ACCESS_LEVEL_NONE:I = 0x0

.field private static ALLDAY_TOP_MARGIN:I = 0x0

.field private static ALL_DAY_EVENT_RECT_BOTTOM_MARGIN:I = 0x0

.field private static AMPM_TEXT_SIZE:F = 0.0f

.field private static final ANIMATION_DURATION:J = 0x190L

.field private static final ANIMATION_SECONDARY_DURATION:J = 0xc8L

.field private static final CALENDARS_INDEX_ACCESS_LEVEL:I = 0x1

.field private static final CALENDARS_INDEX_OWNER_ACCOUNT:I = 0x2

.field private static final CALENDARS_PROJECTION:[Ljava/lang/String;

.field private static CALENDAR_COLOR_SQUARE_SIZE:I = 0x0

.field private static final CLICK_DISPLAY_DURATION:I = 0x32

.field private static CURRENT_TIME_LINE_SIDE_BUFFER:I = 0x0

.field private static CURRENT_TIME_LINE_TOP_OFFSET:I = 0x0

.field private static DATE_HEADER_FONT_SIZE:F = 0.0f

.field private static final DAY_GAP:I = 0x1

.field private static DAY_HEADER_BOTTOM_MARGIN:I = 0x0

.field private static DAY_HEADER_FONT_SIZE:F = 0.0f

.field private static DAY_HEADER_HEIGHT:I = 0x0

.field private static DAY_HEADER_ONE_DAY_BOTTOM_MARGIN:I = 0x0

.field private static DAY_HEADER_ONE_DAY_LEFT_MARGIN:I = 0x0

.field private static DAY_HEADER_ONE_DAY_RIGHT_MARGIN:I = 0x0

.field private static DAY_HEADER_RIGHT_MARGIN:I = 0x0

.field private static DEBUG:Z = false

.field private static DEBUG_SCALING:Z = false

.field private static DEFAULT_CELL_HEIGHT:I = 0x0

.field private static final EVENTS_CROSS_FADE_DURATION:I = 0x190

.field private static EVENT_ALL_DAY_TEXT_BOTTOM_MARGIN:I = 0x0

.field private static EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I = 0x0

.field private static EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I = 0x0

.field private static EVENT_ALL_DAY_TEXT_TOP_MARGIN:I = 0x0

.field private static EVENT_LINE_PADDING:I = 0x0

.field private static EVENT_RECT_ALPHA:I = 0x0

.field private static EVENT_RECT_BOTTOM_MARGIN:I = 0x0

.field private static EVENT_RECT_LEFT_MARGIN:I = 0x0

.field private static EVENT_RECT_RIGHT_MARGIN:I = 0x0

.field private static EVENT_RECT_STROKE_WIDTH:I = 0x0

.field private static EVENT_RECT_TOP_MARGIN:I = 0x0

.field private static EVENT_SQUARE_WIDTH:I = 0x0

.field private static EVENT_TEXT_BOTTOM_MARGIN:I = 0x0

.field private static EVENT_TEXT_FONT_SIZE:F = 0.0f

.field private static EVENT_TEXT_LEFT_MARGIN:I = 0x0

.field private static EVENT_TEXT_RIGHT_MARGIN:I = 0x0

.field private static EVENT_TEXT_TOP_MARGIN:I = 0x0

.field private static EXPAND_ALL_DAY_BOTTOM_MARGIN:I = 0x0

.field private static final FROM_ABOVE:I = 0x1

.field private static final FROM_BELOW:I = 0x2

.field private static final FROM_LEFT:I = 0x4

.field private static final FROM_NONE:I = 0x0

.field private static final FROM_RIGHT:I = 0x8

.field private static final GOTO_SCROLL_DURATION:I = 0xc8

.field private static final GRID_LINE_INNER_WIDTH:F = 1.0f

.field private static GRID_LINE_LEFT_MARGIN:F = 0.0f

.field private static HOURS_LEFT_MARGIN:I = 0x0

.field private static HOURS_MARGIN:I = 0x0

.field private static HOURS_RIGHT_MARGIN:I = 0x0

.field private static HOURS_TEXT_SIZE:F = 0.0f

.field private static HOURS_TOP_MARGIN:I = 0x0

.field private static final HOUR_GAP:I = 0x1

.field private static final INVALID_EVENT_ID:J = -0x1L

.field private static MAX_CELL_HEIGHT:I = 0x0

.field private static final MAX_EVENT_TEXT_LEN:I = 0x1f4

.field private static MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I = 0x0

.field private static MAX_UNEXPANDED_ALLDAY_HEIGHT:I = 0x0

.field private static final MENU_AGENDA:I = 0x2

.field private static final MENU_DAY:I = 0x3

.field private static final MENU_EVENT_CREATE:I = 0x6

.field private static final MENU_EVENT_DELETE:I = 0x8

.field private static final MENU_EVENT_EDIT:I = 0x7

.field private static final MENU_EVENT_SHARE:I = 0x9

.field private static final MENU_EVENT_VIEW:I = 0x5

.field static final MILLIS_PER_DAY:I = 0x5265c00

.field static final MILLIS_PER_HOUR:I = 0x36ee80

.field static final MILLIS_PER_MINUTE:I = 0xea60

.field private static final MINIMUM_SNAP_VELOCITY:I = 0x898

.field static final MINUTES_PER_DAY:I = 0x5a0

.field static final MINUTES_PER_HOUR:I = 0x3c

.field private static MIN_CELL_WIDTH_FOR_TEXT:I = 0x0

.field private static MIN_EVENT_HEIGHT:F = 0.0f

.field private static MIN_HOURS_HEIGHT:I = 0x0

.field private static MIN_HOURS_WIDTH:I = 0x0

.field private static MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F = 0.0f

.field private static MIN_Y_SPAN:I = 0x0

.field private static final MORE_EVENTS_MAX_ALPHA:I = 0x4c

.field private static MULTI_DAY_HEADER_HEIGHT:I = 0x0

.field private static NEW_EVENT_HINT_FONT_SIZE:I = 0x0

.field private static NEW_EVENT_MARGIN:I = 0x0

.field private static NEW_EVENT_MAX_LENGTH:I = 0x0

.field private static NEW_EVENT_WIDTH:I = 0x0

.field private static NORMAL_FONT_SIZE:F = 0.0f

.field private static ONE_DAY_HEADER_HEIGHT:I = 0x0

.field private static final PERIOD_SPACE:Ljava/lang/String; = ". "

.field private static final POPUP_DISMISS_DELAY:I = 0xbb8

.field private static final SELECTION_HIDDEN:I = 0x0

.field private static final SELECTION_LONGPRESS:I = 0x3

.field private static final SELECTION_PRESSED:I = 0x1

.field private static final SELECTION_SELECTED:I = 0x2

.field private static SINGLE_ALLDAY_HEIGHT:I = 0x0

.field private static TAG:Ljava/lang/String; = null

.field private static THEME_ALPHA_GRID_AREA_SELECTED:I = 0x0

.field private static final TOUCH_MODE_DOWN:I = 0x1

.field private static final TOUCH_MODE_HSCROLL:I = 0x40

.field private static final TOUCH_MODE_INITIAL_STATE:I = 0x0

.field private static final TOUCH_MODE_VSCROLL:I = 0x20

.field private static final UPDATE_CURRENT_TIME_DELAY:I = 0x493e0

.field private static mBgColor:I

.field private static mCalendarAmPmLabel:I

.field private static mCalendarDateBannerTextColor:I

.field private static mCalendarGridAreaSelected:I

.field private static mCalendarGridLineInnerHorizontalColor:I

.field private static mCalendarGridLineInnerVerticalColor:I

.field private static mCalendarHourLabelColor:I

.field private static mCellHeight:I

.field private static mClickedColor:I

.field private static mEventTextColor:I

.field protected static mFormatter:Ljava/util/Formatter;

.field private static mFutureBgColor:I

.field private static mFutureBgColorRes:I

.field private static mHorizontalSnapBackThreshold:I

.field private static mMinCellHeight:I

.field private static mMoreAlldayEventsTextAlpha:I

.field private static mMoreEventsTextColor:I

.field private static mNewEventHintColor:I

.field private static mOnDownDelay:I

.field private static mPressedColor:I

.field private static mScale:F

.field private static mScaledPagingTouchSlop:I

.field private static mShowAllAllDayEvents:Z

.field protected static mStringBuilder:Ljava/lang/StringBuilder;

.field private static mUseExpandIcon:Z

.field private static mWeek_saturdayColor:I

.field private static mWeek_sundayColor:I

.field private static sCounter:I


# instance fields
.field private final OVERFLING_DISTANCE:I

.field private final drawTextSanitizerFilter:Ljava/util/regex/Pattern;

.field protected mAcceptedOrTentativeEventBoxDrawable:Landroid/graphics/drawable/Drawable;

.field private mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

.field private mAllDayEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;"
        }
    .end annotation
.end field

.field private mAllDayLayouts:[Landroid/text/StaticLayout;

.field mAlldayAnimator:Landroid/animation/ObjectAnimator;

.field mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

.field private mAlldayHeight:I

.field private mAmString:Ljava/lang/String;

.field private mAnimateDayEventHeight:I

.field private mAnimateDayHeight:I

.field private mAnimateToday:Z

.field private mAnimateTodayAlpha:I

.field private mAnimationDistance:F

.field mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

.field mBaseDate:Landroid/text/format/Time;

.field private final mBold:Landroid/graphics/Typeface;

.field private mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

.field private mCallEdgeEffectOnAbsorb:Z

.field private final mCancelCallback:Ljava/lang/Runnable;

.field private mCancellingAnimations:Z

.field private mCellHeightBeforeScaleGesture:I

.field private mCellWidth:I

.field private final mClearClick:Ljava/lang/Runnable;

.field private mClickedEvent:Lcom/android/calendar/Event;

.field private mClickedYLocation:I

.field protected final mCollapseAlldayDrawable:Landroid/graphics/drawable/Drawable;

.field private mComputeSelectedEvents:Z

.field protected mContext:Landroid/content/Context;

.field private final mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

.field private final mContinueScroll:Lcom/android/calendar/DayView$ContinueScroll;

.field private final mController:Lcom/android/calendar/CalendarController;

.field private final mCreateNewEventString:Ljava/lang/String;

.field private mCurrentTime:Landroid/text/format/Time;

.field protected final mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

.field protected final mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

.field private mDateStrWidth:I

.field private mDateStrWidth2letter:I

.field private mDayStrs:[Ljava/lang/String;

.field private mDayStrs2Letter:[Ljava/lang/String;

.field private final mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

.field private final mDestRect:Landroid/graphics/Rect;

.field private final mDismissPopup:Lcom/android/calendar/DayView$DismissPopup;

.field private mDownTouchTime:J

.field private mEarliestStartHour:[I

.field private final mEdgeEffectBottom:Landroid/widget/EdgeEffect;

.field private final mEdgeEffectTop:Landroid/widget/EdgeEffect;

.field private mEventCountTemplate:Ljava/lang/String;

.field protected final mEventGeometry:Lcom/android/calendar/EventGeometry;

.field private final mEventLoader:Lcom/android/calendar/EventLoader;

.field private final mEventTextPaint:Landroid/graphics/Paint;

.field private mEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;"
        }
    .end annotation
.end field

.field private mEventsAlpha:I

.field private mEventsCrossFadeAnimation:Landroid/animation/ObjectAnimator;

.field private final mExpandAllDayRect:Landroid/graphics/Rect;

.field protected final mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

.field private mFirstCell:I

.field private mFirstDayOfWeek:I

.field private mFirstHour:I

.field private mFirstHourOffset:I

.field private mFirstJulianDay:I

.field private mFirstVisibleDate:I

.field private mFirstVisibleDayOfWeek:I

.field private mGestureCenterHour:F

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mGridAreaHeight:I

.field private final mHScrollInterpolator:Lcom/android/calendar/DayView$ScrollInterpolator;

.field private mHandleActionUp:Z

.field private mHandler:Landroid/os/Handler;

.field private mHasAllDayEvent:[Z

.field private mHourStrs:[Ljava/lang/String;

.field private mHoursTextHeight:I

.field private mHoursWidth:I

.field private mInitialScrollX:F

.field private mInitialScrollY:F

.field private mIs24HourFormat:Z

.field private mIsAccessibilityEnabled:Z

.field private mIsSelectionFocusShow:Z

.field private mLastJulianDay:I

.field private mLastPopupEventID:J

.field private mLastReloadMillis:J

.field private mLastSelectedEventForAccessibility:Lcom/android/calendar/Event;

.field private mLastSelectionDayForAccessibility:I

.field private mLastSelectionHourForAccessibility:I

.field private mLastVelocity:F

.field private mLayouts:[Landroid/text/StaticLayout;

.field private mLines:[F

.field private mLoadedFirstJulianDay:I

.field private final mLongPressItems:[Ljava/lang/CharSequence;

.field private mLongPressTitle:Ljava/lang/String;

.field private mMaxAlldayEvents:I

.field private mMaxUnexpandedAlldayEventCount:I

.field private mMaxViewStartY:I

.field private mMonthLength:I

.field mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

.field private final mNewEventHintString:Ljava/lang/String;

.field protected mNumDays:I

.field private mNumHours:I

.field private mOnFlingCalled:Z

.field private final mPaint:Landroid/graphics/Paint;

.field protected mPaused:Z

.field private mPmString:Ljava/lang/String;

.field private mPopup:Landroid/widget/PopupWindow;

.field private mPopupView:Landroid/view/View;

.field private final mPrevBox:Landroid/graphics/Rect;

.field private mPrevSelectedEvent:Lcom/android/calendar/Event;

.field private mPreviousDirection:I

.field private mRecalCenterHour:Z

.field private final mRect:Landroid/graphics/Rect;

.field private mRemeasure:Z

.field protected final mResources:Landroid/content/res/Resources;

.field private mSavedClickedEvent:Lcom/android/calendar/Event;

.field mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mScrollStartY:I

.field private final mScroller:Landroid/widget/OverScroller;

.field private mScrolling:Z

.field private mSelectedEvent:Lcom/android/calendar/Event;

.field private mSelectedEventForAccessibility:Lcom/android/calendar/Event;

.field private final mSelectedEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;"
        }
    .end annotation
.end field

.field mSelectionAllday:Z

.field private mSelectionDay:I

.field private mSelectionDayForAccessibility:I

.field private mSelectionHour:I

.field private mSelectionHourForAccessibility:I

.field private mSelectionMode:I

.field private final mSelectionPaint:Landroid/graphics/Paint;

.field private final mSelectionRect:Landroid/graphics/Rect;

.field private final mSetClick:Ljava/lang/Runnable;

.field private mSkippedAlldayEvents:[I

.field private mStartingScroll:Z

.field private mStartingSpanY:F

.field private final mTZUpdater:Ljava/lang/Runnable;

.field mTodayAnimator:Landroid/animation/ObjectAnimator;

.field private final mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

.field protected final mTodayHeaderDrawable:Landroid/graphics/drawable/Drawable;

.field private mTodayJulianDay:I

.field private mTouchExplorationEnabled:Z

.field private mTouchMode:I

.field private mTouchStartedInAlldayArea:Z

.field private final mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

.field private mUpdateToast:Z

.field private mViewHeight:I

.field private mViewStartX:I

.field private mViewStartY:I

.field private final mViewSwitcher:Landroid/widget/ViewSwitcher;

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/high16 v7, 0x41400000

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    const-string v0, "DayView"

    sput-object v0, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    sput-boolean v3, Lcom/android/calendar/DayView;->DEBUG:Z

    sput-boolean v3, Lcom/android/calendar/DayView;->DEBUG_SCALING:Z

    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/DayView;->mScale:F

    const/16 v0, 0x40

    sput v0, Lcom/android/calendar/DayView;->DEFAULT_CELL_HEIGHT:I

    const/16 v0, 0x96

    sput v0, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    const/16 v0, 0x64

    sput v0, Lcom/android/calendar/DayView;->MIN_Y_SPAN:I

    const/16 v0, 0xe6

    sput v0, Lcom/android/calendar/DayView;->THEME_ALPHA_GRID_AREA_SELECTED:I

    const/16 v0, 0xa0

    sput v0, Lcom/android/calendar/DayView;->EVENT_RECT_ALPHA:I

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "calendar_access_level"

    aput-object v1, v0, v5

    const-string v1, "ownerAccount"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/DayView;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x80

    sput v0, Lcom/android/calendar/DayView;->mHorizontalSnapBackThreshold:I

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/calendar/DayView;->mStringBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/calendar/DayView;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/calendar/DayView;->mFormatter:Ljava/util/Formatter;

    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    const/16 v0, 0x22

    sput v0, Lcom/android/calendar/DayView;->SINGLE_ALLDAY_HEIGHT:I

    const/high16 v0, 0x41e00000

    sput v0, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    sget v0, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    const/high16 v1, 0x40800000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    const/16 v0, 0xb4

    sput v0, Lcom/android/calendar/DayView;->MIN_HOURS_HEIGHT:I

    sput v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    const/16 v0, 0x22

    sput v0, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    sput v4, Lcom/android/calendar/DayView;->HOURS_TOP_MARGIN:I

    sput v4, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    sput v6, Lcom/android/calendar/DayView;->HOURS_RIGHT_MARGIN:I

    sget v0, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    sget v1, Lcom/android/calendar/DayView;->HOURS_RIGHT_MARGIN:I

    add-int/2addr v0, v1

    sput v0, Lcom/android/calendar/DayView;->HOURS_MARGIN:I

    sput v6, Lcom/android/calendar/DayView;->NEW_EVENT_MARGIN:I

    sput v4, Lcom/android/calendar/DayView;->NEW_EVENT_WIDTH:I

    const/16 v0, 0x10

    sput v0, Lcom/android/calendar/DayView;->NEW_EVENT_MAX_LENGTH:I

    sput v6, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    sput v4, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_TOP_OFFSET:I

    sput v3, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_LEFT_MARGIN:I

    const/4 v0, 0x5

    sput v0, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_RIGHT_MARGIN:I

    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_BOTTOM_MARGIN:I

    sput v6, Lcom/android/calendar/DayView;->DAY_HEADER_RIGHT_MARGIN:I

    const/4 v0, 0x3

    sput v0, Lcom/android/calendar/DayView;->DAY_HEADER_BOTTOM_MARGIN:I

    const/high16 v0, 0x41600000

    sput v0, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    const/high16 v0, 0x42000000

    sput v0, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    sput v7, Lcom/android/calendar/DayView;->NORMAL_FONT_SIZE:F

    sput v7, Lcom/android/calendar/DayView;->EVENT_TEXT_FONT_SIZE:F

    sput v7, Lcom/android/calendar/DayView;->HOURS_TEXT_SIZE:F

    const/high16 v0, 0x41100000

    sput v0, Lcom/android/calendar/DayView;->AMPM_TEXT_SIZE:F

    const/16 v0, 0x60

    sput v0, Lcom/android/calendar/DayView;->MIN_HOURS_WIDTH:I

    const/16 v0, 0x14

    sput v0, Lcom/android/calendar/DayView;->MIN_CELL_WIDTH_FOR_TEXT:I

    const/high16 v0, 0x41c00000

    sput v0, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/DayView;->CALENDAR_COLOR_SQUARE_SIZE:I

    sput v5, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    sput v3, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    sput v5, Lcom/android/calendar/DayView;->EVENT_RECT_LEFT_MARGIN:I

    sput v3, Lcom/android/calendar/DayView;->EVENT_RECT_RIGHT_MARGIN:I

    sput v4, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    sput v4, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sput v4, Lcom/android/calendar/DayView;->EVENT_TEXT_BOTTOM_MARGIN:I

    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/DayView;->EVENT_TEXT_RIGHT_MARGIN:I

    sput v5, Lcom/android/calendar/DayView;->ALL_DAY_EVENT_RECT_BOTTOM_MARGIN:I

    sget v0, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sput v0, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_TOP_MARGIN:I

    sget v0, Lcom/android/calendar/DayView;->EVENT_TEXT_BOTTOM_MARGIN:I

    sput v0, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_BOTTOM_MARGIN:I

    sget v0, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sput v0, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    sget v0, Lcom/android/calendar/DayView;->EVENT_TEXT_RIGHT_MARGIN:I

    sput v0, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I

    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/DayView;->EXPAND_ALL_DAY_BOTTOM_MARGIN:I

    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    sput v6, Lcom/android/calendar/DayView;->EVENT_LINE_PADDING:I

    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/DayView;->NEW_EVENT_HINT_FONT_SIZE:I

    const/16 v0, 0x4c

    sput v0, Lcom/android/calendar/DayView;->mMoreAlldayEventsTextAlpha:I

    sput v3, Lcom/android/calendar/DayView;->mCellHeight:I

    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/DayView;->mMinCellHeight:I

    sput v3, Lcom/android/calendar/DayView;->mScaledPagingTouchSlop:I

    sput-boolean v5, Lcom/android/calendar/DayView;->mUseExpandIcon:Z

    const/16 v0, 0x2d

    sput v0, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sget v0, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sput v0, Lcom/android/calendar/DayView;->MULTI_DAY_HEADER_HEIGHT:I

    sget v0, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sput v0, Lcom/android/calendar/DayView;->ONE_DAY_HEADER_HEIGHT:I

    sput-boolean v3, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    sput v3, Lcom/android/calendar/DayView;->sCounter:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/CalendarController;Landroid/widget/ViewSwitcher;Lcom/android/calendar/EventLoader;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/calendar/CalendarController;
    .param p3    # Landroid/widget/ViewSwitcher;
    .param p4    # Lcom/android/calendar/EventLoader;
    .param p5    # I

    const/4 v3, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mStartingScroll:Z

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mPaused:Z

    new-instance v2, Lcom/android/calendar/DayView$ContinueScroll;

    invoke-direct {v2, p0, v6}, Lcom/android/calendar/DayView$ContinueScroll;-><init>(Lcom/android/calendar/DayView;Lcom/android/calendar/DayView$1;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mContinueScroll:Lcom/android/calendar/DayView$ContinueScroll;

    new-instance v2, Lcom/android/calendar/DayView$UpdateCurrentTime;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$UpdateCurrentTime;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v2, p0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    iput v3, p0, Lcom/android/calendar/DayView;->mLoadedFirstJulianDay:I

    const/16 v2, 0xff

    iput v2, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    new-instance v2, Lcom/android/calendar/DayView$1;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$1;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/calendar/DayView$2;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$2;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mSetClick:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/calendar/DayView$3;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$3;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mClearClick:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/calendar/DayView$TodayAnimatorListener;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$TodayAnimatorListener;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

    new-instance v2, Lcom/android/calendar/DayView$4;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$4;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mAllDayEvents:Ljava/util/ArrayList;

    iput-object v6, p0, Lcom/android/calendar/DayView;->mLayouts:[Landroid/text/StaticLayout;

    iput-object v6, p0, Lcom/android/calendar/DayView;->mAllDayLayouts:[Landroid/text/StaticLayout;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mSelectionRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mSelectionPaint:Landroid/graphics/Paint;

    new-instance v2, Lcom/android/calendar/DayView$DismissPopup;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$DismissPopup;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mDismissPopup:Lcom/android/calendar/DayView$DismissPopup;

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    iput v7, p0, Lcom/android/calendar/DayView;->mAnimationDistance:F

    iput v3, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    iput v7, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    iput v7, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    iput v5, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    sget v2, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v2, v2

    iput v2, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    const/4 v2, 0x4

    iput v2, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    const/4 v2, 0x7

    iput v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    const/16 v2, 0xa

    iput v2, p0, Lcom/android/calendar/DayView;->mNumHours:I

    iput v3, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    new-instance v2, Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-direct {v2, p0, v6}, Lcom/android/calendar/DayView$ContextMenuHandler;-><init>(Lcom/android/calendar/DayView;Lcom/android/calendar/DayView$1;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    iput v5, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    iput v5, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mAnimateToday:Z

    iput v5, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mCancellingAnimations:Z

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mTouchStartedInAlldayArea:Z

    iput-object v6, p0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mTouchExplorationEnabled:Z

    new-instance v2, Lcom/android/calendar/DayView$5;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$5;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCancelCallback:Ljava/lang/Runnable;

    iput-boolean v5, p0, Lcom/android/calendar/DayView;->mIsSelectionFocusShow:Z

    const-string v2, "[\t\n],"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->drawTextSanitizerFilter:Ljava/util/regex/Pattern;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->initAccessibilityVariables()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCreateNewEventString:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c00e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mNewEventHintString:Ljava/lang/String;

    iput p5, p0, Lcom/android/calendar/DayView;->mNumDays:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sput v2, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const/high16 v3, 0x7f0a0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->ONE_DAY_HEADER_HEIGHT:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_BOTTOM_MARGIN:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EXPAND_ALL_DAY_BOTTOM_MARGIN:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sput v2, Lcom/android/calendar/DayView;->HOURS_TEXT_SIZE:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sput v2, Lcom/android/calendar/DayView;->AMPM_TEXT_SIZE:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MIN_HOURS_WIDTH:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->HOURS_RIGHT_MARGIN:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MULTI_DAY_HEADER_HEIGHT:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ne v2, v4, :cond_2

    const v0, 0x7f0a0008

    :goto_0
    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_TEXT_FONT_SIZE:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->NEW_EVENT_HINT_FONT_SIZE:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    sget v2, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    sput v2, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_TEXT_BOTTOM_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_BOTTOM_MARGIN:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_TEXT_RIGHT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sput v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->mScale:F

    cmpl-float v2, v2, v7

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    sput v2, Lcom/android/calendar/DayView;->mScale:F

    sget v2, Lcom/android/calendar/DayView;->mScale:F

    const/high16 v3, 0x3f800000

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    sget v2, Lcom/android/calendar/DayView;->SINGLE_ALLDAY_HEIGHT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->SINGLE_ALLDAY_HEIGHT:I

    sget v2, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    sget v2, Lcom/android/calendar/DayView;->NORMAL_FONT_SIZE:F

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    sput v2, Lcom/android/calendar/DayView;->NORMAL_FONT_SIZE:F

    sget v2, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    sput v2, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    sget v2, Lcom/android/calendar/DayView;->HOURS_TOP_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->HOURS_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->MIN_CELL_WIDTH_FOR_TEXT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MIN_CELL_WIDTH_FOR_TEXT:I

    sget v2, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    sget v2, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v2, v2

    iput v2, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    sget v2, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    sget v2, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_TOP_OFFSET:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_TOP_OFFSET:I

    sget v2, Lcom/android/calendar/DayView;->MIN_Y_SPAN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MIN_Y_SPAN:I

    sget v2, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    sget v2, Lcom/android/calendar/DayView;->DEFAULT_CELL_HEIGHT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DEFAULT_CELL_HEIGHT:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_RIGHT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_RIGHT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_LEFT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_RIGHT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_RIGHT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_BOTTOM_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_BOTTOM_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->CALENDAR_COLOR_SQUARE_SIZE:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->CALENDAR_COLOR_SQUARE_SIZE:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->ALL_DAY_EVENT_RECT_BOTTOM_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->ALL_DAY_EVENT_RECT_BOTTOM_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_RECT_LEFT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_RECT_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_RECT_RIGHT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_RECT_RIGHT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_LINE_PADDING:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->EVENT_LINE_PADDING:I

    sget v2, Lcom/android/calendar/DayView;->NEW_EVENT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->NEW_EVENT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->NEW_EVENT_WIDTH:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->NEW_EVENT_WIDTH:I

    sget v2, Lcom/android/calendar/DayView;->NEW_EVENT_MAX_LENGTH:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sput v2, Lcom/android/calendar/DayView;->NEW_EVENT_MAX_LENGTH:I

    :cond_0
    sget v2, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    sget v3, Lcom/android/calendar/DayView;->HOURS_RIGHT_MARGIN:I

    add-int/2addr v2, v3

    sput v2, Lcom/android/calendar/DayView;->HOURS_MARGIN:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ne v2, v4, :cond_3

    sget v2, Lcom/android/calendar/DayView;->ONE_DAY_HEADER_HEIGHT:I

    :goto_1
    sput v2, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f02005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f02005c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f02005e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mTodayHeaderDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f020034

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f020030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCollapseAlldayDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f08003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/DayView;->mNewEventHintColor:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/calendar/extension/ExtensionFactory;->getCalendarTheme(Landroid/content/Context;)Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f020058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mAcceptedOrTentativeEventBoxDrawable:Landroid/graphics/drawable/Drawable;

    iput-object p4, p0, Lcom/android/calendar/DayView;->mEventLoader:Lcom/android/calendar/EventLoader;

    new-instance v2, Lcom/android/calendar/EventGeometry;

    invoke-direct {v2}, Lcom/android/calendar/EventGeometry;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    sget v3, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    invoke-virtual {v2, v3}, Lcom/android/calendar/EventGeometry;->setMinEventHeight(F)V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Lcom/android/calendar/EventGeometry;->setHourGap(F)V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    invoke-virtual {v2, v4}, Lcom/android/calendar/EventGeometry;->setCellMargin(I)V

    new-array v2, v4, [Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0c00a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iput-object v2, p0, Lcom/android/calendar/DayView;->mLongPressItems:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c00a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/DayView;->mLongPressTitle:Ljava/lang/String;

    new-instance v2, Lcom/android/calendar/DeleteEventHelper;

    invoke-direct {v2, p1, v6, v5}, Lcom/android/calendar/DeleteEventHelper;-><init>(Landroid/content/Context;Landroid/app/Activity;Z)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    iput-object p2, p0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    iput-object p3, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    new-instance v2, Landroid/view/GestureDetector;

    new-instance v3, Lcom/android/calendar/DayView$CalendarGestureListener;

    invoke-direct {v3, p0}, Lcom/android/calendar/DayView$CalendarGestureListener;-><init>(Lcom/android/calendar/DayView;)V

    invoke-direct {v2, p1, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    sget v2, Lcom/android/calendar/DayView;->mCellHeight:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    const-string v3, "preferences_default_cell_height"

    sget v4, Lcom/android/calendar/DayView;->DEFAULT_CELL_HEIGHT:I

    invoke-static {v2, v3, v4}, Lcom/android/calendar/Utils;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/android/calendar/DayView;->mCellHeight:I

    :cond_1
    new-instance v2, Landroid/widget/OverScroller;

    invoke-direct {v2, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mScroller:Landroid/widget/OverScroller;

    new-instance v2, Lcom/android/calendar/DayView$ScrollInterpolator;

    invoke-direct {v2, p0}, Lcom/android/calendar/DayView$ScrollInterpolator;-><init>(Lcom/android/calendar/DayView;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mHScrollInterpolator:Lcom/android/calendar/DayView$ScrollInterpolator;

    new-instance v2, Landroid/widget/EdgeEffect;

    invoke-direct {v2, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    new-instance v2, Landroid/widget/EdgeEffect;

    invoke-direct {v2, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    sput v2, Lcom/android/calendar/DayView;->mScaledPagingTouchSlop:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    sput v2, Lcom/android/calendar/DayView;->mOnDownDelay:I

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v2

    iput v2, p0, Lcom/android/calendar/DayView;->OVERFLING_DISTANCE:I

    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->init(Landroid/content/Context;)V

    return-void

    :cond_2
    const v0, 0x7f0a0009

    goto/16 :goto_0

    :cond_3
    sget v2, Lcom/android/calendar/DayView;->MULTI_DAY_HEADER_HEIGHT:I

    goto/16 :goto_1
.end method

.method static synthetic access$100(Lcom/android/calendar/DayView;)Landroid/text/format/Time;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$1100()I
    .locals 1

    sget v0, Lcom/android/calendar/DayView;->sCounter:I

    return v0
.end method

.method static synthetic access$1104()I
    .locals 1

    sget v0, Lcom/android/calendar/DayView;->sCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/calendar/DayView;->sCounter:I

    return v0
.end method

.method static synthetic access$1200(Lcom/android/calendar/DayView;)Landroid/widget/ViewSwitcher;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    return p1
.end method

.method static synthetic access$1400(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    return v0
.end method

.method static synthetic access$1500(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mLoadedFirstJulianDay:I

    return v0
.end method

.method static synthetic access$1502(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mLoadedFirstJulianDay:I

    return p1
.end method

.method static synthetic access$1602(Lcom/android/calendar/DayView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/calendar/DayView;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAllDayEvents:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/calendar/DayView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mAllDayEvents:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/calendar/DayView;)[Landroid/text/StaticLayout;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mLayouts:[Landroid/text/StaticLayout;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/android/calendar/DayView;[Landroid/text/StaticLayout;)[Landroid/text/StaticLayout;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # [Landroid/text/StaticLayout;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mLayouts:[Landroid/text/StaticLayout;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/android/calendar/DayView;)[Landroid/text/StaticLayout;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAllDayLayouts:[Landroid/text/StaticLayout;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/calendar/DayView;[Landroid/text/StaticLayout;)[Landroid/text/StaticLayout;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # [Landroid/text/StaticLayout;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mAllDayLayouts:[Landroid/text/StaticLayout;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/calendar/DayView;)Lcom/android/calendar/Event;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeEventRelations()V

    return-void
.end method

.method static synthetic access$202(Lcom/android/calendar/DayView;Lcom/android/calendar/Event;)Lcom/android/calendar/Event;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Lcom/android/calendar/Event;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/android/calendar/DayView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    return p1
.end method

.method static synthetic access$2202(Lcom/android/calendar/DayView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->recalc()V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/calendar/DayView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEventsCrossFadeAnimation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/android/calendar/DayView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/animation/ObjectAnimator;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mEventsCrossFadeAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/android/calendar/DayView;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mCancellingAnimations:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    return p1
.end method

.method static synthetic access$2702(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/calendar/DayView;->mUseExpandIcon:Z

    return p0
.end method

.method static synthetic access$2800()Z
    .locals 1

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/android/calendar/DayView;)Lcom/android/calendar/Event;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/calendar/DayView;)Lcom/android/calendar/Event;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSavedClickedEvent:Lcom/android/calendar/Event;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/calendar/DayView;)Landroid/widget/OverScroller;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mScroller:Landroid/widget/OverScroller;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/calendar/DayView;Lcom/android/calendar/Event;)Lcom/android/calendar/Event;
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Lcom/android/calendar/Event;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mSavedClickedEvent:Lcom/android/calendar/Event;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    return v0
.end method

.method static synthetic access$3102(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    return p1
.end method

.method static synthetic access$3200(Lcom/android/calendar/DayView;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mCallEdgeEffectOnAbsorb:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/android/calendar/DayView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mCallEdgeEffectOnAbsorb:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/android/calendar/DayView;)F
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mLastVelocity:F

    return v0
.end method

.method static synthetic access$3302(Lcom/android/calendar/DayView;F)F
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # F

    iput p1, p0, Lcom/android/calendar/DayView;->mLastVelocity:F

    return p1
.end method

.method static synthetic access$3400(Lcom/android/calendar/DayView;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    return v0
.end method

.method static synthetic access$3600(Lcom/android/calendar/DayView;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mScrollStartY:I

    return v0
.end method

.method static synthetic access$3800(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeFirstHour()V

    return-void
.end method

.method static synthetic access$3900(Lcom/android/calendar/DayView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/calendar/DayView;)I
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mClickedYLocation:I

    return v0
.end method

.method static synthetic access$4000(Lcom/android/calendar/DayView;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/calendar/DayView;)Lcom/android/calendar/DayView$UpdateCurrentTime;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    return p1
.end method

.method static synthetic access$4300()Z
    .locals 1

    sget-boolean v0, Lcom/android/calendar/DayView;->DEBUG:Z

    return v0
.end method

.method static synthetic access$4400()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/android/calendar/DayView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->doSingleTapUp(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/android/calendar/DayView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->doLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$4700(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->eventClickCleanup()V

    return-void
.end method

.method static synthetic access$4800(Lcom/android/calendar/DayView;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mTouchStartedInAlldayArea:Z

    return v0
.end method

.method static synthetic access$4900(Lcom/android/calendar/DayView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/DayView;->doScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/calendar/DayView;)Lcom/android/calendar/CalendarController;
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/android/calendar/DayView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/DayView;->doFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    return-void
.end method

.method static synthetic access$5100(Lcom/android/calendar/DayView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->doDown(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$5200(Lcom/android/calendar/DayView;)F
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget v0, p0, Lcom/android/calendar/DayView;->mAnimationDistance:F

    return v0
.end method

.method static synthetic access$5300(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->cancelAnimation()V

    return-void
.end method

.method static synthetic access$602(Lcom/android/calendar/DayView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mAnimateToday:Z

    return p1
.end method

.method static synthetic access$702(Lcom/android/calendar/DayView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    return p1
.end method

.method static synthetic access$800(Lcom/android/calendar/DayView;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/DayView;

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/calendar/DayView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/calendar/DayView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/DayView;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->resetSelectedHour()V

    return-void
.end method

.method private adjustHourSelection()V
    .locals 6

    const/4 v5, 0x0

    const/16 v2, 0x17

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    if-gez v1, :cond_0

    invoke-direct {p0, v3}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    if-lez v1, :cond_0

    iput-object v5, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :cond_0
    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    if-le v1, v2, :cond_1

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    :cond_1
    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_4

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v0, v1, v2

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    aget v1, v1, v0

    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    if-le v1, v2, :cond_3

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-lez v1, :cond_3

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    const/16 v2, 0x8

    if-ge v1, v2, :cond_3

    iput-object v5, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    sget v2, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-gez v1, :cond_2

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v3, p0, Lcom/android/calendar/DayView;->mNumHours:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x3

    if-le v1, v2, :cond_2

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumHours:I

    rsub-int/lit8 v2, v2, 0x18

    if-ge v1, v2, :cond_5

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    sget v2, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v2, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v1, v2, :cond_2

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumHours:I

    rsub-int/lit8 v2, v2, 0x18

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    if-lez v1, :cond_2

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    goto :goto_0
.end method

.method private adjustToBeginningOfWeek(Landroid/text/format/Time;)V
    .locals 3
    .param p1    # Landroid/text/format/Time;

    iget v0, p1, Landroid/text/format/Time;->weekDay:I

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    sub-int v1, v0, v2

    if-eqz v1, :cond_1

    if-gez v1, :cond_0

    add-int/lit8 v1, v1, 0x7

    :cond_0
    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v2, v1

    iput v2, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->normalize(Z)J

    :cond_1
    return-void
.end method

.method private appendEventAccessibilityString(Ljava/lang/StringBuilder;Lcom/android/calendar/Event;)V
    .locals 7
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Lcom/android/calendar/Event;

    invoke-virtual {p2}, Lcom/android/calendar/Event;->getTitleAndLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x10

    iget-boolean v0, p2, Lcom/android/calendar/Event;->allDay:Z

    if-eqz v0, :cond_1

    or-int/lit16 v5, v5, 0x2002

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    iget-wide v1, p2, Lcom/android/calendar/Event;->startMillis:J

    iget-wide v3, p2, Lcom/android/calendar/Event;->endMillis:J

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_1
    or-int/lit8 v5, v5, 0x1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    or-int/lit16 v5, v5, 0x80

    goto :goto_0
.end method

.method private calculateDuration(FFF)J
    .locals 9
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/high16 v6, 0x40000000

    div-float v5, p2, v6

    div-float v2, p1, p2

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->distanceInfluenceForSnapDuration(F)F

    move-result v1

    mul-float v6, v5, v1

    add-float v0, v5, v6

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p3

    const v6, 0x45098000

    invoke-static {v6, p3}, Ljava/lang/Math;->max(FF)F

    move-result p3

    const/high16 v6, 0x447a0000

    div-float v7, v0, p3

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    mul-int/lit8 v6, v6, 0x6

    int-to-long v3, v6

    sget-boolean v6, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v6, :cond_0

    sget-object v6, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "halfScreenSize:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " delta:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " distanceRatio:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " distance:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " velocity:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " duration:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " distanceInfluenceForSnapDuration:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-wide v3
.end method

.method private cancelAnimation()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewAnimator;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    :cond_0
    iget-object v2, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewAnimator;->getOutAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    :cond_1
    return-void
.end method

.method private computeAllDayNeighbors()V
    .locals 13

    const/4 v12, 0x0

    iget-object v11, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v11, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v11, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v11, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/Event;

    iput-object v12, v0, Lcom/android/calendar/Event;->nextUp:Lcom/android/calendar/Event;

    iput-object v12, v0, Lcom/android/calendar/Event;->nextDown:Lcom/android/calendar/Event;

    iput-object v12, v0, Lcom/android/calendar/Event;->nextLeft:Lcom/android/calendar/Event;

    iput-object v12, v0, Lcom/android/calendar/Event;->nextRight:Lcom/android/calendar/Event;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v10, -0x1

    iget-object v11, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    invoke-virtual {v11}, Lcom/android/calendar/Event;->drawAsAllday()Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    invoke-virtual {v11}, Lcom/android/calendar/Event;->getColumn()I

    move-result v10

    :cond_3
    const/4 v4, -0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_a

    iget-object v11, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/Event;

    invoke-virtual {v0}, Lcom/android/calendar/Event;->getColumn()I

    move-result v8

    if-ne v8, v10, :cond_6

    move-object v9, v0

    :cond_4
    :goto_3
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v3, :cond_9

    if-ne v2, v1, :cond_7

    :cond_5
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    if-le v8, v4, :cond_4

    move-object v5, v0

    move v4, v8

    goto :goto_3

    :cond_7
    iget-object v11, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/calendar/Event;

    invoke-virtual {v6}, Lcom/android/calendar/Event;->getColumn()I

    move-result v7

    add-int/lit8 v11, v8, -0x1

    if-ne v7, v11, :cond_8

    iput-object v6, v0, Lcom/android/calendar/Event;->nextUp:Lcom/android/calendar/Event;

    goto :goto_5

    :cond_8
    add-int/lit8 v11, v8, 0x1

    if-ne v7, v11, :cond_5

    iput-object v6, v0, Lcom/android/calendar/Event;->nextDown:Lcom/android/calendar/Event;

    goto :goto_5

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_a
    if-eqz v9, :cond_b

    invoke-direct {p0, v9}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    goto :goto_0

    :cond_b
    invoke-direct {p0, v5}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    goto :goto_0
.end method

.method private computeDayLeftPosition(I)I
    .locals 3
    .param p1    # I

    iget v1, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sub-int v0, v1, v2

    mul-int v1, p1, v0

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    add-int/2addr v1, v2

    return v1
.end method

.method private computeEventRelations()V
    .locals 15

    const/4 v12, 0x0

    iget-object v5, p0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    iget v13, p0, Lcom/android/calendar/DayView;->mLastJulianDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int/2addr v13, v14

    add-int/lit8 v13, v13, 0x1

    new-array v6, v13, [I

    const/4 v13, 0x0

    invoke-static {v6, v13}, Ljava/util/Arrays;->fill([II)V

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v11, :cond_8

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/Event;

    iget v13, v4, Lcom/android/calendar/Event;->startDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mLastJulianDay:I

    if-gt v13, v14, :cond_0

    iget v13, v4, Lcom/android/calendar/Event;->endDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    if-ge v13, v14, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/android/calendar/Event;->drawAsAllday()Z

    move-result v13

    if-eqz v13, :cond_6

    iget v13, v4, Lcom/android/calendar/Event;->startDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v7

    iget v13, v4, Lcom/android/calendar/Event;->endDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mLastJulianDay:I

    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    move-result v10

    move v1, v7

    :goto_2
    if-gt v1, v10, :cond_3

    iget v13, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v13, v1, v13

    aget v14, v6, v13

    add-int/lit8 v0, v14, 0x1

    aput v0, v6, v13

    if-ge v12, v0, :cond_2

    move v12, v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget v13, v4, Lcom/android/calendar/Event;->startDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v2, v13, v14

    iget v13, v4, Lcom/android/calendar/Event;->endDay:I

    iget v14, v4, Lcom/android/calendar/Event;->startDay:I

    sub-int/2addr v13, v14

    add-int/lit8 v3, v13, 0x1

    if-gez v2, :cond_4

    add-int/2addr v3, v2

    const/4 v2, 0x0

    :cond_4
    add-int v13, v2, v3

    iget v14, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-le v13, v14, :cond_5

    iget v13, p0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int v3, v13, v2

    :cond_5
    move v1, v2

    :goto_3
    if-lez v3, :cond_0

    iget-object v13, p0, Lcom/android/calendar/DayView;->mHasAllDayEvent:[Z

    const/4 v14, 0x1

    aput-boolean v14, v13, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    :cond_6
    iget v13, v4, Lcom/android/calendar/Event;->startDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v2, v13, v14

    iget v13, v4, Lcom/android/calendar/Event;->startTime:I

    div-int/lit8 v8, v13, 0x3c

    if-ltz v2, :cond_7

    iget-object v13, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    aget v13, v13, v2

    if-ge v8, v13, :cond_7

    iget-object v13, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    aput v8, v13, v2

    :cond_7
    iget v13, v4, Lcom/android/calendar/Event;->endDay:I

    iget v14, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v2, v13, v14

    iget v13, v4, Lcom/android/calendar/Event;->endTime:I

    div-int/lit8 v8, v13, 0x3c

    iget v13, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v2, v13, :cond_0

    iget-object v13, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    aget v13, v13, v2

    if-ge v8, v13, :cond_0

    iget-object v13, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    aput v8, v13, v2

    goto/16 :goto_1

    :cond_8
    iput v12, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->initAllDayHeights()V

    return-void
.end method

.method private computeFirstHour()V
    .locals 2

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    sget v1, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, -0x1

    sget v1, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    sget v1, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    return-void
.end method

.method private computeMaxStringWidth(I[Ljava/lang/String;Landroid/graphics/Paint;)I
    .locals 9
    .param p1    # I
    .param p2    # [Ljava/lang/String;
    .param p3    # Landroid/graphics/Paint;

    const/4 v3, 0x0

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v5, p2, v0

    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    float-to-double v5, v3

    const-wide/high16 v7, 0x3fe0000000000000L

    add-double/2addr v5, v7

    double-to-int v2, v5

    if-ge v2, p1, :cond_1

    move v2, p1

    :cond_1
    return v2
.end method

.method private computeNeighbors()V
    .locals 45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    if-eqz v43, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v15, 0x0

    :goto_1
    move/from16 v0, v20

    if-ge v15, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/calendar/Event;

    const/16 v43, 0x0

    move-object/from16 v0, v43

    iput-object v0, v14, Lcom/android/calendar/Event;->nextUp:Lcom/android/calendar/Event;

    const/16 v43, 0x0

    move-object/from16 v0, v43

    iput-object v0, v14, Lcom/android/calendar/Event;->nextDown:Lcom/android/calendar/Event;

    const/16 v43, 0x0

    move-object/from16 v0, v43

    iput-object v0, v14, Lcom/android/calendar/Event;->nextLeft:Lcom/android/calendar/Event;

    const/16 v43, 0x0

    move-object/from16 v0, v43

    iput-object v0, v14, Lcom/android/calendar/Event;->nextRight:Lcom/android/calendar/Event;

    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/android/calendar/Event;

    const v37, 0x186a0

    const v38, 0x186a0

    const/16 v30, 0x0

    const/16 v28, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->getCurrentSelectionPosition()Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    if-eqz v43, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Lcom/android/calendar/Event;->top:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Lcom/android/calendar/Event;->bottom:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v43, v0

    move/from16 v0, v32

    move/from16 v1, v43

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v43, v0

    move/from16 v0, v27

    move/from16 v1, v43

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v43, v0

    move/from16 v0, v31

    move/from16 v1, v43

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v43, v0

    move/from16 v0, v29

    move/from16 v1, v43

    if-lt v0, v1, :cond_d

    :cond_3
    const/16 v43, 0x0

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    :cond_4
    :goto_2
    iget v0, v3, Landroid/graphics/Rect;->right:I

    move/from16 v43, v0

    move/from16 v0, v29

    move/from16 v1, v43

    if-lt v0, v1, :cond_10

    const/16 v30, 0x8

    add-int v43, v32, v27

    div-int/lit8 v28, v43, 0x2

    :cond_5
    :goto_3
    const/4 v15, 0x0

    :goto_4
    move/from16 v0, v20

    if-ge v15, v0, :cond_32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/calendar/Event;

    iget v0, v14, Lcom/android/calendar/Event;->startTime:I

    move/from16 v39, v0

    iget v13, v14, Lcom/android/calendar/Event;->endTime:I

    iget v0, v14, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v17, v0

    iget v0, v14, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v33, v0

    iget v0, v14, Lcom/android/calendar/Event;->top:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v40, v0

    iget v0, v3, Landroid/graphics/Rect;->top:I

    move/from16 v43, v0

    move/from16 v0, v40

    move/from16 v1, v43

    if-ge v0, v1, :cond_6

    iget v0, v3, Landroid/graphics/Rect;->top:I

    move/from16 v40, v0

    :cond_6
    iget v0, v14, Lcom/android/calendar/Event;->bottom:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v2, v0

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    move/from16 v43, v0

    move/from16 v0, v43

    if-le v2, v0, :cond_7

    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    :cond_7
    const/16 v41, 0x2710

    const/16 v11, 0x2710

    const/16 v18, 0x2710

    const/16 v34, 0x2710

    const/16 v42, 0x0

    const/4 v12, 0x0

    const/16 v19, 0x0

    const/16 v35, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v43, 0x1

    move/from16 v0, v30

    move/from16 v1, v43

    if-ne v0, v1, :cond_14

    move/from16 v0, v17

    move/from16 v1, v28

    if-lt v0, v1, :cond_13

    sub-int v9, v17, v28

    :cond_8
    :goto_5
    sub-int v10, v40, v27

    :cond_9
    :goto_6
    move/from16 v0, v37

    if-lt v9, v0, :cond_a

    move/from16 v0, v37

    if-ne v9, v0, :cond_b

    move/from16 v0, v38

    if-ge v10, v0, :cond_b

    :cond_a
    move-object/from16 v36, v14

    move/from16 v37, v9

    move/from16 v38, v10

    :cond_b
    const/16 v16, 0x0

    :goto_7
    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_31

    move/from16 v0, v16

    if-ne v0, v15, :cond_1d

    :cond_c
    :goto_8
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v43, v0

    move/from16 v0, v32

    move/from16 v1, v43

    if-ge v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v32, v0

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v43, v0

    move/from16 v0, v27

    move/from16 v1, v43

    if-le v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v27, v0

    goto/16 :goto_2

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    goto/16 :goto_2

    :cond_10
    iget v0, v3, Landroid/graphics/Rect;->left:I

    move/from16 v43, v0

    move/from16 v0, v31

    move/from16 v1, v43

    if-gt v0, v1, :cond_11

    const/16 v30, 0x4

    add-int v43, v32, v27

    div-int/lit8 v28, v43, 0x2

    goto/16 :goto_3

    :cond_11
    iget v0, v3, Landroid/graphics/Rect;->top:I

    move/from16 v43, v0

    move/from16 v0, v27

    move/from16 v1, v43

    if-gt v0, v1, :cond_12

    const/16 v30, 0x1

    add-int v43, v29, v31

    div-int/lit8 v28, v43, 0x2

    goto/16 :goto_3

    :cond_12
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    move/from16 v43, v0

    move/from16 v0, v32

    move/from16 v1, v43

    if-lt v0, v1, :cond_5

    const/16 v30, 0x2

    add-int v43, v29, v31

    div-int/lit8 v28, v43, 0x2

    goto/16 :goto_3

    :cond_13
    move/from16 v0, v33

    move/from16 v1, v28

    if-gt v0, v1, :cond_8

    sub-int v9, v28, v33

    goto/16 :goto_5

    :cond_14
    const/16 v43, 0x2

    move/from16 v0, v30

    move/from16 v1, v43

    if-ne v0, v1, :cond_17

    move/from16 v0, v17

    move/from16 v1, v28

    if-lt v0, v1, :cond_16

    sub-int v9, v17, v28

    :cond_15
    :goto_9
    sub-int v10, v32, v2

    goto/16 :goto_6

    :cond_16
    move/from16 v0, v33

    move/from16 v1, v28

    if-gt v0, v1, :cond_15

    sub-int v9, v28, v33

    goto :goto_9

    :cond_17
    const/16 v43, 0x4

    move/from16 v0, v30

    move/from16 v1, v43

    if-ne v0, v1, :cond_1a

    move/from16 v0, v28

    if-gt v2, v0, :cond_19

    sub-int v9, v28, v2

    :cond_18
    :goto_a
    sub-int v10, v17, v31

    goto/16 :goto_6

    :cond_19
    move/from16 v0, v40

    move/from16 v1, v28

    if-lt v0, v1, :cond_18

    sub-int v9, v40, v28

    goto :goto_a

    :cond_1a
    const/16 v43, 0x8

    move/from16 v0, v30

    move/from16 v1, v43

    if-ne v0, v1, :cond_9

    move/from16 v0, v28

    if-gt v2, v0, :cond_1c

    sub-int v9, v28, v2

    :cond_1b
    :goto_b
    sub-int v10, v29, v33

    goto/16 :goto_6

    :cond_1c
    move/from16 v0, v40

    move/from16 v1, v28

    if-lt v0, v1, :cond_1b

    sub-int v9, v40, v28

    goto :goto_b

    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/calendar/Event;

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->endTime:I

    move/from16 v43, v0

    move/from16 v0, v43

    move/from16 v1, v39

    if-gt v0, v1, :cond_25

    move/from16 v0, v24

    move/from16 v1, v33

    if-ge v0, v1, :cond_1e

    move/from16 v0, v25

    move/from16 v1, v17

    if-le v0, v1, :cond_1e

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->endTime:I

    move/from16 v43, v0

    sub-int v8, v39, v43

    move/from16 v0, v41

    if-ge v8, v0, :cond_20

    move/from16 v41, v8

    move-object/from16 v42, v21

    :cond_1e
    :goto_c
    move/from16 v0, v24

    move/from16 v1, v33

    if-lt v0, v1, :cond_2d

    add-int v43, v40, v2

    div-int/lit8 v4, v43, 0x2

    const/4 v8, 0x0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->bottom:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->top:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v22

    if-gt v0, v4, :cond_2b

    sub-int v8, v4, v22

    :cond_1f
    :goto_d
    move/from16 v0, v34

    if-ge v8, v0, :cond_2c

    move/from16 v34, v8

    move-object/from16 v35, v21

    goto/16 :goto_8

    :cond_20
    move/from16 v0, v41

    if-ne v8, v0, :cond_1e

    add-int v43, v17, v33

    div-int/lit8 v4, v43, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v6, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v7, v0

    if-gt v7, v4, :cond_23

    sub-int v5, v4, v7

    :cond_21
    :goto_e
    const/16 v23, 0x0

    move/from16 v0, v25

    if-gt v0, v4, :cond_24

    sub-int v23, v4, v25

    :cond_22
    :goto_f
    move/from16 v0, v23

    if-ge v0, v5, :cond_1e

    move/from16 v41, v8

    move-object/from16 v42, v21

    goto :goto_c

    :cond_23
    if-lt v6, v4, :cond_21

    sub-int v5, v6, v4

    goto :goto_e

    :cond_24
    move/from16 v0, v24

    if-lt v0, v4, :cond_22

    sub-int v23, v24, v4

    goto :goto_f

    :cond_25
    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->startTime:I

    move/from16 v43, v0

    move/from16 v0, v43

    if-lt v0, v13, :cond_1e

    move/from16 v0, v24

    move/from16 v1, v33

    if-ge v0, v1, :cond_1e

    move/from16 v0, v25

    move/from16 v1, v17

    if-le v0, v1, :cond_1e

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->startTime:I

    move/from16 v43, v0

    sub-int v8, v43, v13

    if-ge v8, v11, :cond_26

    move v11, v8

    move-object/from16 v12, v21

    goto/16 :goto_c

    :cond_26
    if-ne v8, v11, :cond_1e

    add-int v43, v17, v33

    div-int/lit8 v4, v43, 0x2

    const/4 v5, 0x0

    iget v0, v12, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v6, v0

    iget v0, v12, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v7, v0

    if-gt v7, v4, :cond_29

    sub-int v5, v4, v7

    :cond_27
    :goto_10
    const/16 v23, 0x0

    move/from16 v0, v25

    if-gt v0, v4, :cond_2a

    sub-int v23, v4, v25

    :cond_28
    :goto_11
    move/from16 v0, v23

    if-ge v0, v5, :cond_1e

    move v11, v8

    move-object/from16 v12, v21

    goto/16 :goto_c

    :cond_29
    if-lt v6, v4, :cond_27

    sub-int v5, v6, v4

    goto :goto_10

    :cond_2a
    move/from16 v0, v24

    if-lt v0, v4, :cond_28

    sub-int v23, v24, v4

    goto :goto_11

    :cond_2b
    move/from16 v0, v26

    if-lt v0, v4, :cond_1f

    sub-int v8, v26, v4

    goto/16 :goto_d

    :cond_2c
    move/from16 v0, v34

    if-ne v8, v0, :cond_c

    sub-int v23, v24, v33

    move-object/from16 v0, v35

    iget v0, v0, Lcom/android/calendar/Event;->left:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    sub-int v5, v43, v33

    move/from16 v0, v23

    if-ge v0, v5, :cond_c

    move/from16 v34, v8

    move-object/from16 v35, v21

    goto/16 :goto_8

    :cond_2d
    move/from16 v0, v25

    move/from16 v1, v17

    if-gt v0, v1, :cond_c

    add-int v43, v40, v2

    div-int/lit8 v4, v43, 0x2

    const/4 v8, 0x0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->bottom:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/calendar/Event;->top:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v22

    if-gt v0, v4, :cond_2f

    sub-int v8, v4, v22

    :cond_2e
    :goto_12
    move/from16 v0, v18

    if-ge v8, v0, :cond_30

    move/from16 v18, v8

    move-object/from16 v19, v21

    goto/16 :goto_8

    :cond_2f
    move/from16 v0, v26

    if-lt v0, v4, :cond_2e

    sub-int v8, v26, v4

    goto :goto_12

    :cond_30
    move/from16 v0, v18

    if-ne v8, v0, :cond_c

    sub-int v23, v17, v25

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/calendar/Event;->right:F

    move/from16 v43, v0

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    sub-int v5, v17, v43

    move/from16 v0, v23

    if-ge v0, v5, :cond_c

    move/from16 v18, v8

    move-object/from16 v19, v21

    goto/16 :goto_8

    :cond_31
    move-object/from16 v0, v42

    iput-object v0, v14, Lcom/android/calendar/Event;->nextUp:Lcom/android/calendar/Event;

    iput-object v12, v14, Lcom/android/calendar/Event;->nextDown:Lcom/android/calendar/Event;

    move-object/from16 v0, v19

    iput-object v0, v14, Lcom/android/calendar/Event;->nextLeft:Lcom/android/calendar/Event;

    move-object/from16 v0, v35

    iput-object v0, v14, Lcom/android/calendar/Event;->nextRight:Lcom/android/calendar/Event;

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_4

    :cond_32
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    goto/16 :goto_0
.end method

.method private distanceInfluenceForSnapDuration(F)F
    .locals 4
    .param p1    # F

    const/high16 v0, 0x3f000000

    sub-float/2addr p1, v0

    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L

    mul-double/2addr v0, v2

    double-to-float p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private doDown(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v6, 0x0

    iput v3, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    iput v6, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mOnFlingCalled:Z

    iget-object v7, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/android/calendar/DayView;->mContinueScroll:Lcom/android/calendar/DayView$ContinueScroll;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v5, v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    invoke-direct {p0, v4, v5, v6}, Lcom/android/calendar/DayView;->setSelectionFromPosition(IIZ)Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    if-ne v1, v7, :cond_1

    iget v7, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    if-ne v2, v7, :cond_1

    :goto_0
    if-nez v3, :cond_2

    iget-object v6, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iput-object v6, p0, Lcom/android/calendar/DayView;->mSavedClickedEvent:Lcom/android/calendar/Event;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/calendar/DayView;->mDownTouchTime:J

    iget-object v6, p0, Lcom/android/calendar/DayView;->mSetClick:Ljava/lang/Runnable;

    sget v7, Lcom/android/calendar/DayView;->mOnDownDelay:I

    int-to-long v7, v7

    invoke-virtual {p0, v6, v7, v8}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_1
    iput-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iput v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iput v2, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_1
    move v3, v6

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/DayView;->eventClickCleanup()V

    goto :goto_1
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x1

    iget-object v5, p0, Lcom/android/calendar/DayView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    sget v0, Lcom/android/calendar/DayView;->mFutureBgColor:I

    if-eqz v0, :cond_0

    invoke-direct {p0, v1, p1, v5}, Lcom/android/calendar/DayView;->drawBgColors(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    :cond_0
    invoke-direct {p0, v1, p1, v5}, Lcom/android/calendar/DayView;->drawGridBackground(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-direct {p0, v1, p1, v5}, Lcom/android/calendar/DayView;->drawHours(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget v7, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v5}, Landroid/graphics/Paint;->getAlpha()I

    move-result v12

    iget v0, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v2, 0x0

    :goto_0
    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v2, v0, :cond_3

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    if-ne v7, v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    iget-object v4, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->minute:I

    sget v6, Lcom/android/calendar/DayView;->mCellHeight:I

    mul-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x3c

    add-int/2addr v0, v4

    add-int/lit8 v3, v0, 0x1

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-lt v3, v0, :cond_1

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x2

    if-ge v3, v0, :cond_1

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/DayView;->drawCurrentTimeLine(Landroid/graphics/Rect;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    :cond_1
    move-object v6, p0

    move v8, v2

    move-object v10, p1

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/calendar/DayView;->drawEvents(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    if-ne v7, v0, :cond_2

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-lt v3, v0, :cond_2

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x2

    if-ge v3, v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mAnimateToday:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v0

    sget v4, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v2, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v0

    sget v4, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/graphics/Rect;->right:I

    sget v0, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_TOP_OFFSET:I

    sub-int v0, v3, v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v0, v4

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    iget v4, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-direct {p0, v1, p1, v5}, Lcom/android/calendar/DayView;->drawSelectedRect(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-void
.end method

.method private doExpandAllDayClick()V
    .locals 10

    const/16 v3, 0x4c

    const-wide/16 v4, 0xc8

    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    iget v0, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    sget v8, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v8, v8

    sub-int/2addr v0, v8

    :goto_1
    iput v0, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    :cond_0
    iput-boolean v1, p0, Lcom/android/calendar/DayView;->mCancellingAnimations:Z

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_3
    iput-boolean v2, p0, Lcom/android/calendar/DayView;->mCancellingAnimations:Z

    invoke-direct {p0}, Lcom/android/calendar/DayView;->getAllDayAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mAlldayAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->getAllDayEventAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    const-string v8, "moreAllDayEventsTextAlpha"

    const/4 v0, 0x2

    new-array v9, v0, [I

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v0, :cond_7

    move v0, v3

    :goto_2
    aput v0, v9, v2

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v0, :cond_8

    :goto_3
    aput v2, v9, v1

    invoke-static {p0, v8, v9}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mAlldayAnimator:Landroid/animation/ObjectAnimator;

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v0, :cond_9

    move-wide v0, v4

    :goto_4
    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    sget-boolean v0, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v0, :cond_a

    move-wide v0, v6

    :goto_5
    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mMoreAlldayEventsAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    sget-boolean v1, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v1, :cond_b

    :goto_6
    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAlldayEventAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    iget v0, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_3

    :cond_9
    move-wide v0, v6

    goto :goto_4

    :cond_a
    const-wide/16 v0, 0x190

    goto :goto_5

    :cond_b
    move-wide v4, v6

    goto :goto_6
.end method

.method private doFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 13
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Lcom/android/calendar/DayView;->cancelAnimation()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-direct {p0}, Lcom/android/calendar/DayView;->eventClickCleanup()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/DayView;->mOnFlingCalled:Z

    iget v1, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    sget-boolean v1, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doFling: velocityX "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int v12, v1, v2

    if-gez v12, :cond_2

    const/4 v1, 0x1

    :goto_0
    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    int-to-float v3, v3

    move/from16 v0, p3

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/android/calendar/DayView;->switchViews(ZFFF)Landroid/view/View;

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v1, v1, 0x20

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v2, "doFling: no fling"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    sget-boolean v1, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v1, :cond_5

    sget-object v1, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doFling: mViewStartY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " velocityY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    iget-object v1, p0, Lcom/android/calendar/DayView;->mScroller:Landroid/widget/OverScroller;

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    const/4 v4, 0x0

    move/from16 v0, p4

    neg-float v5, v0

    float-to-int v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iget v10, p0, Lcom/android/calendar/DayView;->OVERFLING_DISTANCE:I

    iget v11, p0, Lcom/android/calendar/DayView;->OVERFLING_DISTANCE:I

    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    const/4 v1, 0x0

    cmpl-float v1, p4, v1

    if-lez v1, :cond_7

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/DayView;->mCallEdgeEffectOnAbsorb:Z

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/DayView;->mContinueScroll:Lcom/android/calendar/DayView$ContinueScroll;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    cmpg-float v1, p4, v1

    if-gez v1, :cond_6

    iget v1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v2, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-eq v1, v2, :cond_6

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/DayView;->mCallEdgeEffectOnAbsorb:Z

    goto :goto_2
.end method

.method private doLongPress(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0}, Lcom/android/calendar/DayView;->eventClickCleanup()V

    iget-boolean v3, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v1, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/android/calendar/DayView;->setSelectionFromPosition(IIZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x3

    iput v3, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Landroid/view/View;->performLongClick()Z

    goto :goto_0
.end method

.method private doScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 10
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Lcom/android/calendar/DayView;->cancelAnimation()V

    iget-boolean v8, p0, Lcom/android/calendar/DayView;->mStartingScroll:Z

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/calendar/DayView;->mInitialScrollX:F

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/calendar/DayView;->mInitialScrollY:F

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mStartingScroll:Z

    :cond_0
    iget v8, p0, Lcom/android/calendar/DayView;->mInitialScrollX:F

    add-float/2addr v8, p3

    iput v8, p0, Lcom/android/calendar/DayView;->mInitialScrollX:F

    iget v8, p0, Lcom/android/calendar/DayView;->mInitialScrollY:F

    add-float/2addr v8, p4

    iput v8, p0, Lcom/android/calendar/DayView;->mInitialScrollY:F

    iget v8, p0, Lcom/android/calendar/DayView;->mInitialScrollX:F

    float-to-int v3, v8

    iget v8, p0, Lcom/android/calendar/DayView;->mInitialScrollY:F

    float-to-int v4, v8

    invoke-direct {p0, p2}, Lcom/android/calendar/DayView;->getAverageY(Landroid/view/MotionEvent;)F

    move-result v5

    iget-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    if-eqz v8, :cond_1

    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v8, v8

    add-float/2addr v8, v5

    sget v9, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    int-to-float v9, v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    :cond_1
    iget v8, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iput v8, p0, Lcom/android/calendar/DayView;->mScrollStartY:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/calendar/DayView;->mPreviousDirection:I

    if-le v0, v1, :cond_8

    iget-object v8, p0, Lcom/android/calendar/DayView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v8}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v8

    if-eqz v8, :cond_7

    const/16 v7, 0x14

    :goto_0
    sget v8, Lcom/android/calendar/DayView;->mScaledPagingTouchSlop:I

    mul-int/2addr v8, v7

    if-le v0, v8, :cond_2

    const/16 v8, 0x40

    iput v8, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    neg-int v8, v8

    invoke-direct {p0, v8}, Lcom/android/calendar/DayView;->initNextView(I)Z

    :cond_2
    :goto_1
    iget v8, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v8, v8, 0x20

    if-eqz v8, :cond_6

    iget v8, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    int-to-float v9, v9

    mul-float/2addr v8, v9

    sub-float/2addr v8, v5

    sget v9, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v9, v9

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v8, p0, Lcom/android/calendar/DayView;->mScrollStartY:I

    int-to-float v8, v8

    add-float/2addr v8, p4

    float-to-int v6, v8

    if-gez v6, :cond_b

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    iget v9, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    int-to-float v9, v9

    div-float v9, p4, v9

    invoke-virtual {v8, v9}, Landroid/widget/EdgeEffect;->onPull(F)V

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_3
    :goto_2
    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-gez v8, :cond_c

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    :cond_4
    :goto_3
    iget-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    if-eqz v8, :cond_5

    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v8, v8

    add-float/2addr v8, v5

    sget v9, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    int-to-float v9, v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    :cond_5
    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeFirstHour()V

    :cond_6
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_7
    const/4 v7, 0x2

    goto :goto_0

    :cond_8
    const/16 v8, 0x20

    iput v8, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    goto :goto_1

    :cond_9
    iget v8, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v8, v8, 0x40

    if-eqz v8, :cond_2

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    if-eqz v3, :cond_2

    if-lez v3, :cond_a

    const/4 v2, 0x1

    :goto_4
    iget v8, p0, Lcom/android/calendar/DayView;->mPreviousDirection:I

    if-eq v2, v8, :cond_2

    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    neg-int v8, v8

    invoke-direct {p0, v8}, Lcom/android/calendar/DayView;->initNextView(I)Z

    iput v2, p0, Lcom/android/calendar/DayView;->mPreviousDirection:I

    goto/16 :goto_1

    :cond_a
    const/4 v2, -0x1

    goto :goto_4

    :cond_b
    iget v8, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v6, v8, :cond_3

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    iget v9, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    int-to-float v9, v9

    div-float v9, p4, v9

    invoke-virtual {v8, v9}, Landroid/widget/EdgeEffect;->onPull(F)V

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_2

    :cond_c
    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v9, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v8, v9, :cond_4

    iget v8, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iput v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    goto :goto_3
.end method

.method private doSingleTapUp(Landroid/view/MotionEvent;)V
    .locals 52
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mScrolling:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v0, v3

    move/from16 v49, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v0, v3

    move/from16 v50, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-le v3, v4, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mFirstCell:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mHoursWidth:I

    move/from16 v0, v49

    if-ge v0, v3, :cond_2

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    move/from16 v0, v50

    if-le v0, v3, :cond_2

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    add-int/2addr v3, v4

    move/from16 v0, v50

    if-lt v0, v3, :cond_3

    :cond_2
    sget-boolean v3, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-nez v3, :cond_4

    move/from16 v0, v50

    move/from16 v1, v37

    if-ge v0, v1, :cond_4

    move/from16 v0, v50

    int-to-float v3, v0

    move/from16 v0, v37

    int-to-float v4, v0

    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    sub-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->doExpandAllDayClick()V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v49

    move/from16 v2, v50

    invoke-direct {v0, v1, v2, v3}, Lcom/android/calendar/DayView;->setSelectionFromPosition(IIZ)Z

    move-result v48

    if-nez v48, :cond_5

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    move/from16 v0, v50

    if-ge v0, v3, :cond_0

    new-instance v9, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v9, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v9, v3}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput v3, v9, Landroid/text/format/Time;->hour:I

    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v5, 0x20

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x2

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v3 .. v16}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v3, :cond_8

    const/16 v42, 0x1

    :goto_1
    if-nez v42, :cond_6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mTouchExplorationEnabled:Z

    if-eqz v3, :cond_9

    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, v44

    if-ne v0, v3, :cond_9

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    move/from16 v0, v45

    if-ne v0, v3, :cond_9

    const/16 v43, 0x1

    :goto_2
    if-eqz v43, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSavedClickedEvent:Lcom/android/calendar/Event;

    if-nez v3, :cond_a

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v3, :cond_7

    const-wide/16 v22, 0x10

    :cond_7
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v46

    const-wide/32 v3, 0x36ee80

    add-long v40, v46, v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v12, 0x1

    const-wide/16 v14, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v0, v3

    move/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v0, v3

    move/from16 v21, v0

    const-wide/16 v24, -0x1

    move-object/from16 v11, p0

    invoke-virtual/range {v10 .. v25}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    :goto_3
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_0

    :cond_8
    const/16 v42, 0x0

    goto :goto_1

    :cond_9
    const/16 v43, 0x0

    goto :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    :cond_b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget v3, v3, Lcom/android/calendar/Event;->top:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget v4, v4, Lcom/android/calendar/Event;->bottom:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-boolean v3, v3, Lcom/android/calendar/Event;->allDay:Z

    if-nez v3, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mFirstCell:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    sub-int/2addr v3, v4

    add-int v51, v51, v3

    :cond_c
    move/from16 v0, v51

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mClickedYLocation:I

    sget v3, Lcom/android/calendar/DayView;->mOnDownDelay:I

    add-int/lit8 v3, v3, 0x32

    int-to-long v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/calendar/DayView;->mDownTouchTime:J

    sub-long/2addr v5, v7

    sub-long v38, v3, v5

    const-wide/16 v3, 0x0

    cmp-long v3, v38, v3

    if-lez v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mClearClick:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    move-wide/from16 v1, v38

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mClearClick:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    :cond_e
    new-instance v28, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    move-object/from16 v0, v28

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move-object/from16 v0, v28

    invoke-static {v0, v3}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    move-object/from16 v0, v28

    iput v3, v0, Landroid/text/format/Time;->hour:I

    const/4 v3, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    new-instance v29, Landroid/text/format/Time;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, v29

    iget v3, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v29

    iput v3, v0, Landroid/text/format/Time;->hour:I

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    move-object/from16 v24, v0

    const-wide/16 v26, 0x20

    const-wide/16 v30, -0x1

    const/16 v32, 0x0

    const-wide/16 v33, 0x2

    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v25, p0

    invoke-virtual/range {v24 .. v36}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_3
.end method

.method private drawAfterScroll(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/DayView;->drawAllDayHighlights(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget v2, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    iget v3, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v2, v3, p1, v0}, Lcom/android/calendar/DayView;->drawAllDayEvents(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/DayView;->drawUpperLeftCorner(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    :cond_0
    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/DayView;->drawScrollLine(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/DayView;->drawDayHeaderLoop(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mIs24HourFormat:Z

    if-nez v2, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/DayView;->drawAmPm(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method private drawAllDayEvents(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 38
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/Canvas;
    .param p4    # Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/DayView;->NORMAL_FONT_SIZE:F

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v0, v3

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v3, v3

    add-float v3, v3, v34

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    int-to-float v5, v5

    add-float v35, v3, v5

    const/16 v36, 0x0

    const/16 v28, 0x0

    sget v3, Lcom/android/calendar/DayView;->mCalendarGridLineInnerVerticalColor:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mHoursWidth:I

    int-to-float v0, v3

    move/from16 v36, v0

    const/high16 v3, 0x3f800000

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v29, v28, 0x1

    sget v5, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    aput v5, v3, v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v28, v29, 0x1

    aput v34, v3, v29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v29, v28, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mNumDays:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v5

    int-to-float v5, v5

    aput v5, v3, v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v28, v29, 0x1

    aput v34, v3, v29

    const/16 v20, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mNumDays:I

    move/from16 v0, v20

    if-gt v0, v3, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    int-to-float v0, v3

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v29, v28, 0x1

    aput v36, v3, v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v28, v29, 0x1

    aput v34, v3, v29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v29, v28, 0x1

    aput v36, v3, v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v28, v29, 0x1

    aput v35, v3, v29

    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mLines:[F

    const/4 v5, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v28

    move-object/from16 v2, p4

    invoke-virtual {v0, v3, v5, v1, v2}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int v37, v3, v5

    add-int v3, p1, p2

    add-int/lit8 v27, v3, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mAllDayEvents:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v30

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v0, v3

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    int-to-float v0, v3

    move/from16 v31, v0

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int v18, v3, v5

    move/from16 v0, p2

    new-array v3, v0, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-le v3, v5, :cond_3

    sget-boolean v3, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v0, v3

    move/from16 v31, v0

    move/from16 v0, v18

    int-to-float v3, v0

    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    sub-float/2addr v3, v5

    float-to-int v0, v3

    move/from16 v18, v0

    const/16 v25, 0x1

    :cond_1
    :goto_1
    invoke-virtual {v7}, Landroid/graphics/Paint;->getAlpha()I

    move-result v19

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v10, 0x0

    :goto_2
    move/from16 v0, v30

    if-ge v10, v0, :cond_c

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/Event;

    iget v0, v4, Lcom/android/calendar/Event;->startDay:I

    move/from16 v32, v0

    iget v0, v4, Lcom/android/calendar/Event;->endDay:I

    move/from16 v22, v0

    move/from16 v0, v32

    move/from16 v1, v27

    if-gt v0, v1, :cond_2

    move/from16 v0, v22

    move/from16 v1, p1

    if-ge v0, v1, :cond_4

    :cond_2
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-eqz v3, :cond_1

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int v18, v3, v5

    goto :goto_1

    :cond_4
    move/from16 v0, v32

    move/from16 v1, p1

    if-ge v0, v1, :cond_5

    move/from16 v32, p1

    :cond_5
    move/from16 v0, v22

    move/from16 v1, v27

    if-le v0, v1, :cond_6

    move/from16 v22, v27

    :cond_6
    sub-int v33, v32, p1

    sub-int v23, v22, p1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-le v3, v5, :cond_8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    int-to-float v0, v3

    move/from16 v26, v0

    :goto_4
    sget v3, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    int-to-float v3, v3

    cmpl-float v3, v26, v3

    if-lez v3, :cond_7

    sget v3, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    int-to-float v0, v3

    move/from16 v26, v0

    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v4, Lcom/android/calendar/Event;->left:F

    add-int/lit8 v3, v23, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    iput v3, v4, Lcom/android/calendar/Event;->right:F

    move/from16 v0, v37

    int-to-float v3, v0

    invoke-virtual {v4}, Lcom/android/calendar/Event;->getColumn()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v26

    add-float/2addr v3, v5

    iput v3, v4, Lcom/android/calendar/Event;->top:F

    iget v3, v4, Lcom/android/calendar/Event;->top:F

    add-float v3, v3, v26

    sget v5, Lcom/android/calendar/DayView;->ALL_DAY_EVENT_RECT_BOTTOM_MARGIN:I

    int-to-float v5, v5

    sub-float/2addr v3, v5

    iput v3, v4, Lcom/android/calendar/Event;->bottom:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-le v3, v5, :cond_b

    iget v3, v4, Lcom/android/calendar/Event;->top:F

    move/from16 v0, v18

    int-to-float v5, v0

    cmpl-float v3, v3, v5

    if-ltz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    move-object/from16 v0, p0

    move/from16 v1, v33

    move/from16 v2, v23

    invoke-direct {v0, v3, v1, v2}, Lcom/android/calendar/DayView;->incrementSkipCount([III)V

    goto/16 :goto_3

    :cond_8
    div-float v26, v21, v31

    goto :goto_4

    :cond_9
    iget v3, v4, Lcom/android/calendar/Event;->bottom:F

    move/from16 v0, v18

    int-to-float v5, v0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_b

    if-eqz v25, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    move-object/from16 v0, p0

    move/from16 v1, v33

    move/from16 v2, v23

    invoke-direct {v0, v3, v1, v2}, Lcom/android/calendar/DayView;->incrementSkipCount([III)V

    goto/16 :goto_3

    :cond_a
    move/from16 v0, v18

    int-to-float v3, v0

    iput v3, v4, Lcom/android/calendar/Event;->bottom:F

    :cond_b
    iget v3, v4, Lcom/android/calendar/Event;->top:F

    float-to-int v8, v3

    iget v3, v4, Lcom/android/calendar/Event;->bottom:F

    float-to-int v9, v3

    move-object/from16 v3, p0

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v3 .. v9}, Lcom/android/calendar/DayView;->drawEventRect(Lcom/android/calendar/Event;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/calendar/DayView;->setupAllDayTextRect(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/DayView;->mAllDayLayouts:[Landroid/text/StaticLayout;

    move-object/from16 v8, p0

    move-object v11, v4

    move-object v12, v7

    invoke-direct/range {v8 .. v13}, Lcom/android/calendar/DayView;->getEventLayout([Landroid/text/StaticLayout;ILcom/android/calendar/Event;Landroid/graphics/Paint;Landroid/graphics/Rect;)Landroid/text/StaticLayout;

    move-result-object v12

    iget v15, v13, Landroid/graphics/Rect;->top:I

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v11, p0

    move-object/from16 v14, p3

    invoke-direct/range {v11 .. v17}, Lcom/android/calendar/DayView;->drawEventText(Landroid/text/StaticLayout;Landroid/graphics/Rect;Landroid/graphics/Canvas;IIZ)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, v32

    if-gt v0, v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, v22

    if-lt v0, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_c
    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    sget v3, Lcom/android/calendar/DayView;->mMoreAlldayEventsTextAlpha:I

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    if-eqz v3, :cond_f

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Paint;->getAlpha()I

    move-result v19

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    sget v3, Lcom/android/calendar/DayView;->mMoreAlldayEventsTextAlpha:I

    shl-int/lit8 v3, v3, 0x18

    sget v5, Lcom/android/calendar/DayView;->mMoreEventsTextColor:I

    and-int/2addr v3, v5

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v10, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    array-length v3, v3

    if-ge v10, v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    aget v3, v3, v10

    if-lez v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSkippedAlldayEvents:[I

    aget v3, v3, v10

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v3, v10, v2}, Lcom/android/calendar/DayView;->drawMoreAlldayEvents(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    :cond_d
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    :cond_e
    move-object/from16 v0, p4

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v3, :cond_10

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->computeAllDayNeighbors()V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v6, v8}, Lcom/android/calendar/DayView;->saveSelectionPosition(FFFF)V

    :cond_10
    return-void
.end method

.method private drawAllDayHighlights(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    const/4 v5, 0x0

    sget v3, Lcom/android/calendar/DayView;->mFutureBgColor:I

    if-eqz v3, :cond_1

    iput v5, p1, Landroid/graphics/Rect;->top:I

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    iput v5, p1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iput v3, p1, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/android/calendar/DayView;->mBgColor:I

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    iput v3, p1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    iput v5, p1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    iput v3, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 v1, -0x1

    iget v3, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v2, v3, v4

    if-gez v2, :cond_3

    const/4 v1, 0x0

    :cond_0
    :goto_0
    if-ltz v1, :cond_1

    iput v5, p1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iput v3, p1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v3}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    iput v3, p1, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/android/calendar/DayView;->mFutureBgColor:I

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    iget-boolean v3, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    add-int/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x2

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v0, v3, v4

    iget-object v3, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v4, v0, 0x1

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/android/calendar/DayView;->mCalendarGridAreaSelected:I

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v3, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    return-void

    :cond_3
    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    add-int/lit8 v3, v2, 0x1

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v3, v4, :cond_0

    add-int/lit8 v1, v2, 0x1

    goto :goto_0
.end method

.method private drawAmPm(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Paint;

    const/16 v4, 0xc

    sget v2, Lcom/android/calendar/DayView;->mCalendarAmPmLabel:I

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v2, Lcom/android/calendar/DayView;->AMPM_TEXT_SIZE:F

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAmString:Ljava/lang/String;

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-lt v2, v4, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPmString:Ljava/lang/String;

    :cond_0
    iget v2, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/DayView;->mHoursTextHeight:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/lit8 v1, v2, 0x1

    sget v2, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    int-to-float v2, v2

    int-to-float v3, v1

    invoke-virtual {p1, v0, v2, v3, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-ge v2, v4, :cond_1

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v3, p0, Lcom/android/calendar/DayView;->mNumHours:I

    add-int/2addr v2, v3

    if-le v2, v4, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPmString:Ljava/lang/String;

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    rsub-int/lit8 v3, v3, 0xc

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/DayView;->mHoursTextHeight:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/lit8 v1, v2, 0x1

    sget v2, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    int-to-float v2, v2

    int-to-float v3, v1

    invoke-virtual {p1, v0, v2, v3, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method private drawBgColors(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v2, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v1, v2, v3

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iput v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    iput v4, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    iput v2, p1, Landroid/graphics/Rect;->right:I

    sget v2, Lcom/android/calendar/DayView;->mBgColor:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ne v2, v5, :cond_1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->hour:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    iput v2, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iput v2, p1, Landroid/graphics/Rect;->right:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/android/calendar/DayView;->mFutureBgColor:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    :goto_0
    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void

    :cond_1
    if-ltz v1, :cond_3

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->hour:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_2

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/android/calendar/DayView;->mFutureBgColor:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    add-int/lit8 v2, v1, 0x1

    iget v3, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iput v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/android/calendar/DayView;->mFutureBgColor:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_3
    if-gez v1, :cond_0

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iput v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    sget v2, Lcom/android/calendar/DayView;->mFutureBgColor:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private drawCurrentTimeLine(Landroid/graphics/Rect;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Canvas;
    .param p5    # Landroid/graphics/Paint;

    invoke-direct {p0, p2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v0

    sget v1, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v0

    sget v1, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_SIDE_BUFFER:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    sget v0, Lcom/android/calendar/DayView;->CURRENT_TIME_LINE_TOP_OFFSET:I

    sub-int v0, p3, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/android/calendar/DayView;->mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mAnimateToday:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCurrentTimeAnimateLine:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method private drawDayHeader(Ljava/lang/String;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Canvas;
    .param p5    # Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/calendar/DayView;->mFirstVisibleDate:I

    add-int v3, v11, p2

    iget v11, p0, Lcom/android/calendar/DayView;->mMonthLength:I

    if-le v3, v11, :cond_0

    iget v11, p0, Lcom/android/calendar/DayView;->mMonthLength:I

    sub-int/2addr v3, v11

    :cond_0
    const/4 v11, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget v11, p0, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    iget v12, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v8, v11, v12

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget v11, p0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v12, 0x1

    if-le v11, v12, :cond_4

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    sget v2, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    iget v11, p0, Lcom/android/calendar/DayView;->mDateStrWidth:I

    iget v12, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    if-lt v11, v12, :cond_1

    iget v11, p0, Lcom/android/calendar/DayView;->mDateStrWidth2letter:I

    iget v12, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    if-lt v11, v12, :cond_1

    const/high16 v11, 0x40000000

    sub-float/2addr v5, v11

    const/high16 v11, 0x40800000

    sub-float/2addr v2, v11

    :cond_1
    sget v11, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sget v12, Lcom/android/calendar/DayView;->DAY_HEADER_BOTTOM_MARGIN:I

    sub-int/2addr v11, v12

    int-to-float v10, v11

    sget-object v11, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget v11, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    move/from16 v0, p2

    if-ne v8, v0, :cond_2

    iget-object v11, p0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    :goto_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v11

    float-to-int v7, v11

    iget v11, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    if-le v11, v7, :cond_3

    iget v11, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    sub-int/2addr v11, v7

    div-int/lit8 v6, v11, 0x2

    :goto_1
    add-int/lit8 v11, p2, 0x1

    invoke-direct {p0, v11}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v11

    sub-int v9, v11, v6

    int-to-float v11, v9

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v0, v4, v11, v10, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    int-to-float v11, v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v12

    sub-float/2addr v11, v12

    float-to-int v9, v11

    sget v11, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    int-to-float v11, v9

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v0, p1, v11, v10, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_2
    return-void

    :cond_2
    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    sget v11, Lcom/android/calendar/DayView;->ONE_DAY_HEADER_HEIGHT:I

    sget v12, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_BOTTOM_MARGIN:I

    sub-int/2addr v11, v12

    int-to-float v10, v11

    sget-object v11, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v11

    sget v12, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_LEFT_MARGIN:I

    add-int v9, v11, v12

    sget v11, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    int-to-float v11, v9

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v0, p1, v11, v10, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    int-to-float v11, v9

    move-object/from16 v0, p5

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v12

    sget v13, Lcom/android/calendar/DayView;->DAY_HEADER_ONE_DAY_RIGHT_MARGIN:I

    int-to-float v13, v13

    add-float/2addr v12, v13

    add-float/2addr v11, v12

    float-to-int v9, v11

    sget v11, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    move/from16 v0, p2

    if-ne v8, v0, :cond_5

    iget-object v11, p0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    :goto_3
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    int-to-float v11, v9

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v0, v4, v11, v10, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_2

    :cond_5
    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_3
.end method

.method private drawDayHeaderLoop(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 11
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    const/4 v10, 0x1

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ne v0, v10, :cond_0

    sget v0, Lcom/android/calendar/DayView;->ONE_DAY_HEADER_HEIGHT:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    iget v0, p0, Lcom/android/calendar/DayView;->mDateStrWidth:I

    iget v1, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    if-ge v0, v1, :cond_3

    iget-object v8, p0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    :goto_1
    invoke-virtual {p3, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v2, 0x0

    :goto_2
    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v2, v0, :cond_7

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstVisibleDayOfWeek:I

    add-int v9, v2, v0

    const/16 v0, 0xe

    if-lt v9, v0, :cond_1

    add-int/lit8 v9, v9, -0xe

    :cond_1
    sget v6, Lcom/android/calendar/DayView;->mCalendarDateBannerTextColor:I

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ne v0, v10, :cond_5

    const/4 v0, 0x6

    if-ne v9, v0, :cond_4

    sget v6, Lcom/android/calendar/DayView;->mWeek_saturdayColor:I

    :cond_2
    :goto_3
    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setColor(I)V

    aget-object v1, v8, v9

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/DayView;->drawDayHeader(Ljava/lang/String;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    iget-object v8, p0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    goto :goto_1

    :cond_4
    if-nez v9, :cond_2

    sget v6, Lcom/android/calendar/DayView;->mWeek_sundayColor:I

    goto :goto_3

    :cond_5
    rem-int/lit8 v7, v2, 0x7

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    invoke-static {v7, v0}, Lcom/android/calendar/Utils;->isSaturday(II)Z

    move-result v0

    if-eqz v0, :cond_6

    sget v6, Lcom/android/calendar/DayView;->mWeek_saturdayColor:I

    goto :goto_3

    :cond_6
    iget v0, p0, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    invoke-static {v7, v0}, Lcom/android/calendar/Utils;->isSunday(II)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v6, Lcom/android/calendar/DayView;->mWeek_sundayColor:I

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method private drawEventRect(Lcom/android/calendar/Event;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;
    .locals 8
    .param p1    # Lcom/android/calendar/Event;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Landroid/graphics/Paint;
    .param p5    # I
    .param p6    # I

    iget-object v5, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    iget v6, p1, Lcom/android/calendar/Event;->top:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    add-int/2addr v6, v7

    invoke-static {v6, p5}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->top:I

    iget v6, p1, Lcom/android/calendar/Event;->bottom:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    sub-int/2addr v6, v7

    invoke-static {v6, p6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    iget v6, p1, Lcom/android/calendar/Event;->left:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_LEFT_MARGIN:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->left:I

    iget v6, p1, Lcom/android/calendar/Event;->right:F

    float-to-int v6, v6

    iput v6, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    if-ne p1, v6, :cond_5

    sget v2, Lcom/android/calendar/DayView;->mClickedColor:I

    :goto_0
    iget v6, p1, Lcom/android/calendar/Event;->selfAttendeeStatus:I

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_1
    sget-object v6, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :cond_1
    :goto_2
    const/4 v6, 0x0

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget v6, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    int-to-float v6, v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v3, v6

    sget v6, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    int-to-float v6, v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v1, v6

    iget v6, p1, Lcom/android/calendar/Event;->top:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    add-int/2addr v6, v7

    add-int/2addr v6, v3

    invoke-static {v6, p5}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->top:I

    iget v6, p1, Lcom/android/calendar/Event;->bottom:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    sub-int/2addr v6, v7

    sub-int/2addr v6, v1

    invoke-static {v6, p6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    iget v6, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v3

    iput v6, v5, Landroid/graphics/Rect;->left:I

    iget v6, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v1

    iput v6, v5, Landroid/graphics/Rect;->right:I

    sget v6, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    int-to-float v6, v6

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p3}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    sget v6, Lcom/android/calendar/DayView;->EVENT_RECT_ALPHA:I

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p2, v5, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v6, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-ne v6, p1, :cond_4

    iget-object v6, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    if-eqz v6, :cond_4

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget v6, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    iput-object p1, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    sget v2, Lcom/android/calendar/DayView;->mPressedColor:I

    const/4 v4, 0x1

    :cond_2
    :goto_3
    if-eqz v4, :cond_3

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p2, v5, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_4
    iget v6, p1, Lcom/android/calendar/Event;->top:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_TOP_MARGIN:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->top:I

    iget v6, p1, Lcom/android/calendar/Event;->bottom:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_BOTTOM_MARGIN:I

    sub-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    iget v6, p1, Lcom/android/calendar/Event;->left:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_LEFT_MARGIN:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->left:I

    iget v6, p1, Lcom/android/calendar/Event;->right:F

    float-to-int v6, v6

    sget v7, Lcom/android/calendar/DayView;->EVENT_RECT_RIGHT_MARGIN:I

    sub-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->right:I

    return-object v5

    :cond_5
    iget v2, p1, Lcom/android/calendar/Event;->color:I

    goto/16 :goto_0

    :pswitch_0
    iget-object v6, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    if-eq p1, v6, :cond_1

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto/16 :goto_2

    :pswitch_1
    iget-object v6, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    if-eq p1, v6, :cond_0

    invoke-static {v2}, Lcom/android/calendar/Utils;->getDeclinedColorFromColor(I)I

    move-result v2

    goto/16 :goto_1

    :cond_6
    iget v6, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    iput-object p1, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    sget v2, Lcom/android/calendar/DayView;->mPressedColor:I

    const/4 v4, 0x1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private drawEventText(Landroid/text/StaticLayout;Landroid/graphics/Rect;Landroid/graphics/Canvas;IIZ)V
    .locals 9
    .param p1    # Landroid/text/StaticLayout;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Landroid/graphics/Canvas;
    .param p4    # I
    .param p5    # I
    .param p6    # Z

    iget v7, p2, Landroid/graphics/Rect;->right:I

    iget v8, p2, Landroid/graphics/Rect;->left:I

    sub-int v6, v7, v8

    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    iget v8, p2, Landroid/graphics/Rect;->top:I

    sub-int v0, v7, v8

    if-eqz p1, :cond_0

    sget v7, Lcom/android/calendar/DayView;->MIN_CELL_WIDTH_FOR_TEXT:I

    if-ge v6, v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    if-gt v2, v0, :cond_2

    move v5, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_0

    iget v7, p2, Landroid/graphics/Rect;->top:I

    if-gt v7, p5, :cond_0

    iget v7, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v5

    if-lt v7, p4, :cond_0

    invoke-virtual {p3}, Landroid/graphics/Canvas;->save()I

    if-eqz p6, :cond_3

    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    iget v8, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v5

    div-int/lit8 v4, v7, 0x2

    :goto_2
    iget v7, p2, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v8, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v8, v4

    int-to-float v8, v8

    invoke-virtual {p3, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v7, 0x0

    iput v7, p2, Landroid/graphics/Rect;->left:I

    iput v6, p2, Landroid/graphics/Rect;->right:I

    const/4 v7, 0x0

    iput v7, p2, Landroid/graphics/Rect;->top:I

    iput v5, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p3, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    invoke-virtual {p1, p3}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p3}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private drawEvents(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 28
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Canvas;
    .param p5    # Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    add-int/lit8 v4, v3, 0x1

    add-int/lit8 v3, p2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    sub-int/2addr v3, v4

    add-int/lit8 v6, v3, 0x1

    sget v23, Lcom/android/calendar/DayView;->mCellHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectionRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    add-int/lit8 v5, v23, 0x1

    mul-int/2addr v3, v5

    add-int v3, v3, p3

    move-object/from16 v0, v27

    iput v3, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v27

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int v3, v3, v23

    move-object/from16 v0, v27

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v27

    iput v4, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v27

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v6

    move-object/from16 v0, v27

    iput v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    sub-int v14, v3, v5

    invoke-virtual {v12}, Landroid/graphics/Paint;->getAlpha()I

    move-result v22

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    invoke-virtual {v12, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    const/16 v25, 0x0

    :goto_0
    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_3

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/calendar/Event;

    move/from16 v3, p1

    move/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/EventGeometry;->computeEventRect(IIIILcom/android/calendar/Event;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v25, v25, 0x1

    goto :goto_0

    :cond_1
    iget v3, v7, Lcom/android/calendar/Event;->bottom:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v5, v5

    cmpg-float v3, v3, v5

    if-ltz v3, :cond_0

    iget v3, v7, Lcom/android/calendar/Event;->top:F

    int-to-float v5, v14

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, v27

    invoke-virtual {v2, v7, v0}, Lcom/android/calendar/EventGeometry;->eventIntersectsSelection(Lcom/android/calendar/Event;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    move-object/from16 v8, p0

    move-object v9, v7

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-direct/range {v8 .. v14}, Lcom/android/calendar/DayView;->drawEventRect(Lcom/android/calendar/Event;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/calendar/DayView;->setupTextRect(Landroid/graphics/Rect;)V

    iget v3, v13, Landroid/graphics/Rect;->top:I

    if-gt v3, v14, :cond_0

    iget v3, v13, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-lt v3, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/DayView;->mLayouts:[Landroid/text/StaticLayout;

    move-object/from16 v8, p0

    move/from16 v10, v25

    move-object v11, v7

    invoke-direct/range {v8 .. v13}, Lcom/android/calendar/DayView;->getEventLayout([Landroid/text/StaticLayout;ILcom/android/calendar/Event;Landroid/graphics/Paint;Landroid/graphics/Rect;)Landroid/text/StaticLayout;

    move-result-object v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    add-int/lit8 v19, v3, 0x4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mViewHeight:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    sub-int v20, v3, v5

    const/16 v21, 0x0

    move-object/from16 v15, p0

    move-object/from16 v17, v13

    move-object/from16 v18, p4

    invoke-direct/range {v15 .. v21}, Lcom/android/calendar/DayView;->drawEventText(Landroid/text/StaticLayout;Landroid/graphics/Rect;Landroid/graphics/Canvas;IIZ)V

    goto/16 :goto_1

    :cond_3
    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-nez v3, :cond_4

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v3, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->computeNeighbors()V

    :cond_4
    return-void
.end method

.method private drawGridBackground(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 15
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v7

    iget v13, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v13}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v13

    int-to-float v9, v13

    const/4 v12, 0x0

    sget v13, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v13, v13, 0x1

    int-to-float v3, v13

    const/4 v5, 0x0

    const/4 v8, 0x0

    sget v13, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v13, v13, 0x1

    mul-int/lit8 v13, v13, 0x18

    add-int/lit8 v13, v13, 0x1

    int-to-float v10, v13

    iget v13, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    int-to-float v11, v13

    sget v13, Lcom/android/calendar/DayView;->mCalendarGridLineInnerHorizontalColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v13, 0x3f800000

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v12, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    move v6, v5

    :goto_0
    const/16 v13, 0x18

    if-gt v4, v13, :cond_0

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v5, v6, 0x1

    sget v14, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    aput v14, v13, v6

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v6, v5, 0x1

    aput v12, v13, v5

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v5, v6, 0x1

    aput v9, v13, v6

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v6, v5, 0x1

    aput v12, v13, v5

    add-float/2addr v12, v3

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    sget v13, Lcom/android/calendar/DayView;->mCalendarGridLineInnerVerticalColor:I

    sget v14, Lcom/android/calendar/DayView;->mCalendarGridLineInnerHorizontalColor:I

    if-eq v13, v14, :cond_2

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    const/4 v14, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v13, v14, v6, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    const/4 v5, 0x0

    sget v13, Lcom/android/calendar/DayView;->mCalendarGridLineInnerVerticalColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    const/4 v2, 0x0

    :goto_2
    iget v13, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-gt v2, v13, :cond_1

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v13

    int-to-float v11, v13

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v6, v5, 0x1

    aput v11, v13, v5

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v5, v6, 0x1

    const/4 v14, 0x0

    aput v14, v13, v6

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v6, v5, 0x1

    aput v11, v13, v5

    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    add-int/lit8 v5, v6, 0x1

    aput v10, v13, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    iget-object v13, p0, Lcom/android/calendar/DayView;->mLines:[F

    const/4 v14, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v13, v14, v5, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void

    :cond_2
    move v5, v6

    goto :goto_1
.end method

.method private drawHours(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    invoke-direct {p0, p3}, Lcom/android/calendar/DayView;->setupHourTextPaint(Landroid/graphics/Paint;)V

    iget v3, p0, Lcom/android/calendar/DayView;->mHoursTextHeight:I

    add-int/lit8 v3, v3, 0x1

    sget v4, Lcom/android/calendar/DayView;->HOURS_TOP_MARGIN:I

    add-int v2, v3, v4

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x18

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/calendar/DayView;->mHourStrs:[Ljava/lang/String;

    aget-object v1, v3, v0

    sget v3, Lcom/android/calendar/DayView;->HOURS_LEFT_MARGIN:I

    int-to-float v3, v3

    int-to-float v4, v2

    invoke-virtual {p2, v1, v3, v4, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private drawScrollLine(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 8
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v6

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    add-int/lit8 v7, v0, -0x1

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v0, Lcom/android/calendar/DayView;->mCalendarGridLineInnerHorizontalColor:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget v1, Lcom/android/calendar/DayView;->GRID_LINE_LEFT_MARGIN:F

    int-to-float v2, v7

    int-to-float v3, v6

    int-to-float v4, v7

    move-object v0, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private drawSelectedRect(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 16
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/DayView;->mIsSelectionFocusShow:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v9, v3, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v3, v4

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v3, v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/android/calendar/DayView;->saveSelectionPosition(FFFF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    invoke-interface {v3}, Lcom/mediatek/calendar/extension/ICalendarThemeExt;->isThemeManagerEnable()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    invoke-interface {v3}, Lcom/mediatek/calendar/extension/ICalendarThemeExt;->getThemeColor()I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget v3, Lcom/android/calendar/DayView;->THEME_ALPHA_GRID_AREA_SELECTED:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    :goto_1
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->right:I

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget v3, Lcom/android/calendar/DayView;->mNewEventHintColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    sget v3, Lcom/android/calendar/DayView;->NEW_EVENT_WIDTH:I

    int-to-float v3, v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int v15, v3, v4

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    div-int/lit8 v4, v15, 0x2

    add-int v12, v3, v4

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    div-int/lit8 v4, v4, 0x2

    add-int v13, v3, v4

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    invoke-static {v3, v15}, Ljava/lang/Math;->min(II)I

    move-result v3

    sget v4, Lcom/android/calendar/DayView;->NEW_EVENT_MARGIN:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v11, v3, v4

    sget v3, Lcom/android/calendar/DayView;->NEW_EVENT_MAX_LENGTH:I

    invoke-static {v11, v3}, Ljava/lang/Math;->min(II)I

    move-result v11

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sub-int/2addr v3, v11

    div-int/lit8 v14, v3, 0x2

    sub-int v3, v15, v11

    div-int/lit8 v10, v3, 0x2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v10

    int-to-float v4, v3

    int-to-float v5, v13

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v10

    int-to-float v6, v3

    int-to-float v7, v13

    move-object/from16 v3, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    int-to-float v4, v12

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v14

    int-to-float v5, v3

    int-to-float v6, v12

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v14

    int-to-float v7, v3

    move-object/from16 v3, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_2
    sget v3, Lcom/android/calendar/DayView;->mCalendarGridAreaSelected:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    :cond_3
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v3, Lcom/android/calendar/DayView;->NEW_EVENT_HINT_FONT_SIZE:I

    int-to-float v3, v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v3, 0x1

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/DayView;->mNewEventHintString:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sget v5, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    add-float/2addr v5, v6

    sget v6, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private drawTextSanitizer(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v2, p0, Lcom/android/calendar/DayView;->drawTextSanitizerFilter:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz p2, :cond_1

    const-string p1, ""

    const/4 v0, 0x0

    :cond_0
    :goto_0
    const/16 v2, 0xa

    const/16 v3, 0x20

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_1
    if-le v0, p2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    move v0, p2

    goto :goto_0
.end method

.method private drawUpperLeftCorner(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    invoke-direct {p0, p3}, Lcom/android/calendar/DayView;->setupHourTextPaint(Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-le v0, v1, :cond_0

    sget-boolean v0, Lcom/android/calendar/DayView;->mUseExpandIcon:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/DayView;->mCollapseAlldayDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mCollapseAlldayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private eventClickCleanup()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mClearClick:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSetClick:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v1, p0, Lcom/android/calendar/DayView;->mClickedEvent:Lcom/android/calendar/Event;

    iput-object v1, p0, Lcom/android/calendar/DayView;->mSavedClickedEvent:Lcom/android/calendar/Event;

    return-void
.end method

.method private findSelectedEvent(II)V
    .locals 33
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/DayView;->mCellWidth:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    move/from16 v32, v0

    sub-int v31, v31, v32

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v4

    const/4 v5, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    move/from16 v31, v0

    if-eqz v31, :cond_9

    const v23, 0x461c4000

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v10, v0

    sget v31, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sget v32, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int v30, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_0

    add-int/lit8 v21, v21, -0x1

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mAllDayEvents:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v24

    const/16 v18, 0x0

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/calendar/Event;

    invoke-virtual {v7}, Lcom/android/calendar/Event;->drawAsAllday()Z

    move-result v31

    if-eqz v31, :cond_1

    sget-boolean v31, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-nez v31, :cond_2

    invoke-virtual {v7}, Lcom/android/calendar/Event;->getColumn()I

    move-result v31

    move/from16 v0, v31

    move/from16 v1, v21

    if-lt v0, v1, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_2
    iget v0, v7, Lcom/android/calendar/Event;->startDay:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-gt v0, v1, :cond_1

    iget v0, v7, Lcom/android/calendar/Event;->endDay:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-lt v0, v1, :cond_1

    sget-boolean v31, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v31, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v25, v0

    :goto_2
    div-float v17, v10, v25

    sget v31, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v17, v31

    if-lez v31, :cond_3

    sget v31, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v17, v0

    :cond_3
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v31, v0

    invoke-virtual {v7}, Lcom/android/calendar/Event;->getColumn()I

    move-result v32

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    mul-float v32, v32, v17

    add-float v15, v31, v32

    add-float v14, v15, v17

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v15, v31

    if-gez v31, :cond_7

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v14, v31

    if-lez v31, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v8, v7

    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    :cond_5
    :goto_3
    return-void

    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v25, v0

    goto :goto_2

    :cond_7
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v15, v31

    if-ltz v31, :cond_8

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v31, v0

    sub-float v29, v15, v31

    :goto_4
    cmpg-float v31, v29, v23

    if-gez v31, :cond_1

    move/from16 v23, v29

    move-object v8, v7

    goto/16 :goto_1

    :cond_8
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v31, v0

    sub-float v29, v31, v14

    goto :goto_4

    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mViewStartY:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mFirstCell:I

    move/from16 v32, v0

    sub-int v31, v31, v32

    add-int p2, p2, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    add-int/lit8 v31, p1, -0xa

    move/from16 v0, v31

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v31, p1, 0xa

    move/from16 v0, v31

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v31, p2, -0xa

    move/from16 v0, v31

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v31, p2, 0xa

    move/from16 v0, v31

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    const/16 v18, 0x0

    :goto_5
    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_c

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/calendar/Event;

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/EventGeometry;->computeEventRect(IIIILcom/android/calendar/Event;)Z

    move-result v31

    if-nez v31, :cond_b

    :cond_a
    :goto_6
    add-int/lit8 v18, v18, 0x1

    goto :goto_5

    :cond_b
    move-object/from16 v0, v26

    invoke-virtual {v2, v7, v0}, Lcom/android/calendar/EventGeometry;->eventIntersectsSelection(Lcom/android/calendar/Event;Landroid/graphics/Rect;)Z

    move-result v31

    if-eqz v31, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mViewWidth:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mViewHeight:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v22, v0

    const/16 v19, 0x0

    :goto_7
    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/calendar/Event;

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v2, v0, v1, v13}, Lcom/android/calendar/EventGeometry;->pointToEvent(FFLcom/android/calendar/Event;)F

    move-result v9

    cmpg-float v31, v9, v22

    if-gez v31, :cond_d

    move/from16 v22, v9

    move-object v8, v13

    :cond_d
    add-int/lit8 v19, v19, 0x1

    goto :goto_7

    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/android/calendar/Event;->startDay:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v11, v0, Lcom/android/calendar/Event;->endDay:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v31, v0

    move/from16 v0, v31

    move/from16 v1, v27

    if-ge v0, v1, :cond_10

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    :cond_f
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/android/calendar/Event;->startTime:I

    move/from16 v31, v0

    div-int/lit8 v28, v31, 0x3c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/android/calendar/Event;->startTime:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/calendar/Event;->endTime:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-ge v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/android/calendar/Event;->endTime:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, -0x1

    div-int/lit8 v12, v31, 0x3c

    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    move/from16 v31, v0

    move/from16 v0, v31

    move/from16 v1, v28

    if-ge v0, v1, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v31, v0

    move/from16 v0, v31

    move/from16 v1, v27

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    goto/16 :goto_3

    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-le v0, v11, :cond_f

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    goto :goto_8

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/android/calendar/Event;->endTime:I

    move/from16 v31, v0

    div-int/lit8 v12, v31, 0x3c

    goto :goto_9

    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-le v0, v12, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-ne v0, v11, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    goto/16 :goto_3
.end method

.method private getAllDayAnimator()Landroid/animation/ObjectAnimator;
    .locals 7

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->MIN_HOURS_HEIGHT:I

    sub-int v3, v4, v5

    iget v4, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    int-to-float v4, v4

    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v4, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-eqz v4, :cond_0

    iget v1, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    :goto_0
    sget-boolean v4, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v4, :cond_1

    move v2, v3

    :goto_1
    const-string v4, "animateDayHeight"

    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v1, v5, v6

    const/4 v6, 0x1

    aput v2, v5, v6

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v4, 0x190

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lcom/android/calendar/DayView$7;

    invoke-direct {v4, p0}, Lcom/android/calendar/DayView$7;-><init>(Lcom/android/calendar/DayView;)V

    invoke-virtual {v0, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v0

    :cond_0
    iget v1, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    goto :goto_0

    :cond_1
    sget v4, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    int-to-float v4, v4

    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    sub-float/2addr v4, v5

    const/high16 v5, 0x3f800000

    sub-float/2addr v4, v5

    float-to-int v2, v4

    goto :goto_1
.end method

.method private getAllDayEventAnimator()Landroid/animation/ObjectAnimator;
    .locals 8

    iget v5, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    sget v6, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v5, v6

    sget v6, Lcom/android/calendar/DayView;->MIN_HOURS_HEIGHT:I

    sub-int v4, v5, v6

    iget v5, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    int-to-float v5, v5

    sget v6, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v5, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    div-int v3, v4, v5

    iget v1, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    sget-boolean v5, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v5, :cond_0

    move v2, v3

    :goto_0
    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v2, v5

    goto :goto_0

    :cond_1
    const-string v5, "animateDayEventHeight"

    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    aput v1, v6, v7

    const/4 v7, 0x1

    aput v2, v6, v7

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v5, 0x190

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    goto :goto_1
.end method

.method private getAverageY(Landroid/view/MotionEvent;)F
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    int-to-float v3, v0

    div-float/2addr v1, v3

    return v1
.end method

.method private getCurrentSelectionPosition()Landroid/graphics/Rect;
    .locals 4

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->top:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    sub-int v1, v2, v3

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    return-object v0
.end method

.method private static getEventAccessLevel(Landroid/content/Context;Lcom/android/calendar/Event;)I
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/calendar/Event;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, p1, Lcom/android/calendar/Event;->id:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "calendar_id"

    aput-object v5, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEventAccessLevel, query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/calendar/LogUtil;->v(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/android/calendar/DayView;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const/4 v9, 0x0

    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    const/16 v2, 0x1f4

    if-ge v6, v2, :cond_3

    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    iget-boolean v2, p1, Lcom/android/calendar/Event;->guestsCanModify:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    goto :goto_0

    :cond_4
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/android/calendar/Event;->organizer:Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x2

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private getEventLayout([Landroid/text/StaticLayout;ILcom/android/calendar/Event;Landroid/graphics/Paint;Landroid/graphics/Rect;)Landroid/text/StaticLayout;
    .locals 13
    .param p1    # [Landroid/text/StaticLayout;
    .param p2    # I
    .param p3    # Lcom/android/calendar/Event;
    .param p4    # Landroid/graphics/Paint;
    .param p5    # Landroid/graphics/Rect;

    if-ltz p2, :cond_0

    array-length v3, p1

    if-lt p2, v3, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    aget-object v1, p1, p2

    if-eqz v1, :cond_2

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v4

    if-eq v3, v4, :cond_5

    :cond_2
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/Event;->title:Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f3

    invoke-direct {p0, v3, v4}, Lcom/android/calendar/DayView;->drawTextSanitizer(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_3
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/Event;->location:Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/Event;->location:Ljava/lang/CharSequence;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    rsub-int v4, v4, 0x1f4

    invoke-direct {p0, v3, v4}, Lcom/android/calendar/DayView;->drawTextSanitizer(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    move-object/from16 v0, p3

    iget v3, v0, Lcom/android/calendar/Event;->selfAttendeeStatus:I

    packed-switch v3, :pswitch_data_0

    sget v3, Lcom/android/calendar/DayView;->mEventTextColor:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    new-instance v5, Landroid/text/TextPaint;

    move-object/from16 v0, p4

    invoke-direct {v5, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Rect;->width()I

    move-result v6

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Rect;->width()I

    move-result v12

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    aput-object v1, p1, p2

    :cond_5
    invoke-virtual {v1}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    iget v4, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_0

    :pswitch_0
    move-object/from16 v0, p3

    iget v3, v0, Lcom/android/calendar/Event;->color:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :pswitch_1
    sget v3, Lcom/android/calendar/DayView;->mEventTextColor:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v3, 0xc0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getNewEvent(IJI)Lcom/android/calendar/Event;
    .locals 5
    .param p0    # I
    .param p1    # J
    .param p3    # I

    invoke-static {}, Lcom/android/calendar/Event;->newInstance()Lcom/android/calendar/Event;

    move-result-object v0

    iput p0, v0, Lcom/android/calendar/Event;->startDay:I

    iput p0, v0, Lcom/android/calendar/Event;->endDay:I

    iput-wide p1, v0, Lcom/android/calendar/Event;->startMillis:J

    iget-wide v1, v0, Lcom/android/calendar/Event;->startMillis:J

    const-wide/32 v3, 0x36ee80

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/android/calendar/Event;->endMillis:J

    iput p3, v0, Lcom/android/calendar/Event;->startTime:I

    iget v1, v0, Lcom/android/calendar/Event;->startTime:I

    add-int/lit8 v1, v1, 0x3c

    iput v1, v0, Lcom/android/calendar/Event;->endTime:I

    return-object v0
.end method

.method private incrementSkipCount([III)V
    .locals 2
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    array-length v1, p1

    if-le p3, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v0, p2

    :goto_0
    if-gt v0, p3, :cond_0

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 23
    .param p1    # Landroid/content/Context;

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    invoke-static/range {p1 .. p1}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    new-instance v19, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mTZUpdater:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mCurrentTime:Landroid/text/format/Time;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    invoke-static {v6, v7, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mTodayJulianDay:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080009

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mWeek_saturdayColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000a

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mWeek_sundayColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080010

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarDateBannerTextColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000b

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mFutureBgColorRes:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000e

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mBgColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000d

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarAmPmLabel:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080011

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarGridAreaSelected:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080012

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarGridLineInnerHorizontalColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080013

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarGridLineInnerVerticalColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mCalendarHourLabelColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const/high16 v20, 0x7f080000

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mPressedColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/mediatek/calendar/extension/ICalendarThemeExt;->isThemeManagerEnable()Z

    move-result v19

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mCalendarThemeExt:Lcom/mediatek/calendar/extension/ICalendarThemeExt;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/mediatek/calendar/extension/ICalendarThemeExt;->getThemeColor()I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mClickedColor:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080006

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mEventTextColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08001f

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mMoreEventsTextColor:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    sget v20, Lcom/android/calendar/DayView;->EVENT_TEXT_FONT_SIZE:F

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    sget-object v20, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mEventTextPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080014

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectionPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v19, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/16 v19, 0xe

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    const/16 v19, 0xe

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    const/4 v11, 0x1

    :goto_1
    const/16 v19, 0x7

    move/from16 v0, v19

    if-gt v11, v0, :cond_2

    add-int/lit8 v12, v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x14

    move/from16 v0, v20

    invoke-static {v11, v0}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v19, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    move-object/from16 v19, v0

    add-int/lit8 v20, v12, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    move-object/from16 v21, v0

    aget-object v21, v21, v12

    aput-object v21, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x1e

    move/from16 v0, v20

    invoke-static {v11, v0}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v19, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    move-object/from16 v20, v0

    aget-object v20, v20, v12

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x32

    move/from16 v0, v20

    invoke-static {v11, v0}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v19, v12

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v19, v0

    add-int/lit8 v20, v12, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v21, v0

    aget-object v21, v21, v12

    aput-object v21, v19, v20

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080040

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sput v19, Lcom/android/calendar/DayView;->mClickedColor:I

    goto/16 :goto_0

    :cond_2
    sget v19, Lcom/android/calendar/DayView;->DATE_HEADER_FONT_SIZE:F

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mBold:Landroid/graphics/Typeface;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v8, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    const-string v20, " 28"

    aput-object v20, v8, v19

    const/16 v19, 0x1

    const-string v20, " 30"

    aput-object v20, v8, v19

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v8, v2}, Lcom/android/calendar/DayView;->computeMaxStringWidth(I[Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mDateStrWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mDateStrWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mDateStrWidth2letter:I

    sget v19, Lcom/android/calendar/DayView;->DAY_HEADER_FONT_SIZE:F

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mDateStrWidth:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/android/calendar/DayView;->computeMaxStringWidth(I[Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mDateStrWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mDateStrWidth2letter:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mDayStrs2Letter:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/android/calendar/DayView;->computeMaxStringWidth(I[Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mDateStrWidth2letter:I

    sget v19, Lcom/android/calendar/DayView;->HOURS_TEXT_SIZE:F

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->handleOnResume()V

    new-instance v19, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v19 .. v19}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v19 .. v19}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v4

    const/16 v19, 0x0

    aget-object v19, v4, v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mAmString:Ljava/lang/String;

    const/16 v19, 0x1

    aget-object v19, v4, v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mPmString:Ljava/lang/String;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v5, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mAmString:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v5, v19

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPmString:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v5, v19

    sget v19, Lcom/android/calendar/DayView;->AMPM_TEXT_SIZE:F

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    sget v19, Lcom/android/calendar/DayView;->HOURS_MARGIN:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mHoursWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v5, v2}, Lcom/android/calendar/DayView;->computeMaxStringWidth(I[Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v20

    sget v21, Lcom/android/calendar/DayView;->HOURS_RIGHT_MARGIN:I

    add-int v20, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mHoursWidth:I

    sget v19, Lcom/android/calendar/DayView;->MIN_HOURS_WIDTH:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mHoursWidth:I

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/DayView;->mHoursWidth:I

    const-string v19, "layout_inflater"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    const v19, 0x7f040012

    const/16 v20, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    move-object/from16 v19, v0

    new-instance v20, Landroid/view/ViewGroup$LayoutParams;

    const/16 v21, -0x1

    const/16 v22, -0x2

    invoke-direct/range {v20 .. v22}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v19, Landroid/widget/PopupWindow;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v9

    const v19, 0x103000b

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const v21, 0x1010054

    aput v21, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/TypedArray;->recycle()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    new-instance v19, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mTZUpdater:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mNumDays:I

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mNumDays:I

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v0, v0, [Z

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mHasAllDayEvent:[Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mNumDays:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v14, v19, 0x19

    mul-int/lit8 v19, v14, 0x4

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/DayView;->mLines:[F

    return-void
.end method

.method private initAccessibilityVariables()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    invoke-direct {p0}, Lcom/android/calendar/DayView;->isTouchExplorationEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mTouchExplorationEnabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initFirstHour()V
    .locals 2

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mNumHours:I

    div-int/lit8 v1, v1, 0x5

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mNumHours:I

    add-int/2addr v0, v1

    const/16 v1, 0x18

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/DayView;->mNumHours:I

    rsub-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    goto :goto_0
.end method

.method private initNextView(I)Z
    .locals 7
    .param p1    # I

    iget-object v3, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/DayView;

    iget-object v0, v2, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    if-lez p1, :cond_0

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/text/format/Time;->monthDay:I

    iget v3, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int/2addr v3, v4

    invoke-direct {v2, v3}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    invoke-direct {p0, v2}, Lcom/android/calendar/DayView;->initView(Lcom/android/calendar/DayView;)V

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {v2}, Lcom/android/calendar/DayView;->reloadEvents()V

    return v1

    :cond_0
    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v3, v4

    iput v3, v0, Landroid/text/format/Time;->monthDay:I

    iget v3, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private initView(Lcom/android/calendar/DayView;)V
    .locals 5
    .param p1    # Lcom/android/calendar/DayView;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-gtz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/android/calendar/DayView;->setFirstVisibleHour(I)V

    sget-object v0, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The view overScroll,now mFirstHour is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",and set it to 0 when init nextView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    invoke-direct {p1, v0}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    iget-object v0, p1, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iput v0, p1, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    iput v0, p1, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/android/calendar/DayView;->remeasure(II)V

    invoke-virtual {p1}, Lcom/android/calendar/DayView;->initAllDayHeights()V

    invoke-direct {p1, v4}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    iput-object v4, p1, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    iput v0, p1, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    iget-object v0, p1, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    iput-boolean v0, p1, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :goto_0
    invoke-direct {p1}, Lcom/android/calendar/DayView;->recalc()V

    return-void

    :cond_1
    iput-boolean v3, p1, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    goto :goto_0
.end method

.method private isTouchExplorationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private recalc()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->adjustToBeginningOfWeek(Landroid/text/format/Time;)V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getJulianDayInGeneral(Landroid/text/format/Time;Z)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    iget v1, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/DayView;->mLastJulianDay:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/DayView;->mMonthLength:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstVisibleDate:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->weekDay:I

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstVisibleDayOfWeek:I

    return-void
.end method

.method private remeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v12, 0x0

    sget v8, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    const/high16 v9, 0x40800000

    mul-float/2addr v8, v9

    float-to-int v8, v8

    sput v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    sget v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    div-int/lit8 v9, p2, 0x6

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    sput v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    sget v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    sget v9, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v9, v9

    mul-int/lit8 v9, v9, 0x2

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    sput v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    sget v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    int-to-float v8, v8

    sget v9, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    div-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    const/4 v2, 0x0

    :goto_0
    iget v8, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-ge v2, v8, :cond_0

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEarliestStartHour:[I

    const/16 v9, 0x19

    aput v9, v8, v2

    iget-object v8, p0, Lcom/android/calendar/DayView;->mHasAllDayEvent:[Z

    aput-boolean v12, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget v5, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    sget v8, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int v8, p2, v8

    div-int/lit8 v8, v8, 0x18

    sget v9, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    float-to-int v9, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    sput v8, Lcom/android/calendar/DayView;->mMinCellHeight:I

    sget v8, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v9, Lcom/android/calendar/DayView;->mMinCellHeight:I

    if-ge v8, v9, :cond_1

    sget v8, Lcom/android/calendar/DayView;->mMinCellHeight:I

    sput v8, Lcom/android/calendar/DayView;->mCellHeight:I

    :cond_1
    sget v8, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    iput v8, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    const/4 v0, 0x0

    if-lez v5, :cond_c

    sget v8, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int v8, p2, v8

    sget v9, Lcom/android/calendar/DayView;->MIN_HOURS_HEIGHT:I

    sub-int v4, v8, v9

    const/4 v8, 0x1

    if-ne v5, v8, :cond_8

    sget v0, Lcom/android/calendar/DayView;->SINGLE_ALLDAY_HEIGHT:I

    :cond_2
    :goto_1
    sget v8, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    add-int/2addr v8, v0

    sget v9, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    :goto_2
    iput v0, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    iget v8, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    sub-int v8, p2, v8

    iput v8, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    iget-object v8, p0, Lcom/android/calendar/DayView;->mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v8, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget v9, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sub-int/2addr v9, v1

    div-int/lit8 v9, v9, 0x2

    sget v10, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    iput v9, v8, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v1

    iget v10, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sget v11, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I

    sub-int/2addr v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iput v9, v8, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget v9, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    sget v10, Lcom/android/calendar/DayView;->EXPAND_ALL_DAY_BOTTOM_MARGIN:I

    sub-int/2addr v9, v10

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/android/calendar/DayView;->mExpandAllDayRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    iget-object v10, p0, Lcom/android/calendar/DayView;->mExpandAlldayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v8, Landroid/graphics/Rect;->top:I

    iget v8, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    div-int/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mNumHours:I

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEventGeometry:Lcom/android/calendar/EventGeometry;

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Lcom/android/calendar/EventGeometry;->setHourHeight(F)V

    sget v8, Lcom/android/calendar/DayView;->MIN_EVENT_HEIGHT:F

    const v9, 0x476a6000

    mul-float/2addr v8, v9

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    int-to-float v9, v9

    const/high16 v10, 0x42700000

    div-float/2addr v9, v10

    div-float/2addr v8, v9

    float-to-long v6, v8

    iget-object v8, p0, Lcom/android/calendar/DayView;->mEvents:Ljava/util/ArrayList;

    invoke-static {v8, v6, v7}, Lcom/android/calendar/Event;->computePositions(Ljava/util/ArrayList;J)V

    sget v8, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v8, v8, 0x1

    mul-int/lit8 v8, v8, 0x18

    add-int/lit8 v8, v8, 0x1

    iget v9, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    sget-boolean v8, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v8, :cond_3

    sget-object v8, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mViewStartY: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mMaxViewStartY: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v9, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v8, v9, :cond_4

    iget v8, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iput v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeFirstHour()V

    :cond_4
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_5

    invoke-direct {p0}, Lcom/android/calendar/DayView;->initFirstHour()V

    iput v12, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    :cond_5
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    if-lt v8, v9, :cond_6

    sget v8, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    :cond_6
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    sget v9, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v9, v9, 0x1

    mul-int/2addr v8, v9

    iget v9, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v8, p0, Lcom/android/calendar/DayView;->mNumDays:I

    iget v9, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    add-int/lit8 v9, v9, 0x1

    mul-int v3, v8, v9

    iget-object v8, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v8, :cond_7

    iget-wide v8, p0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    iget-object v10, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-wide v10, v10, Lcom/android/calendar/Event;->id:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v8}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_7
    iget-object v8, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    add-int/lit8 v9, v3, -0x14

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v8, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    const/4 v9, -0x2

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setHeight(I)V

    return-void

    :cond_8
    iget v8, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-gt v5, v8, :cond_9

    sget v8, Lcom/android/calendar/DayView;->MAX_HEIGHT_OF_ONE_ALLDAY_EVENT:I

    mul-int v0, v5, v8

    sget v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    if-le v0, v8, :cond_2

    sget v0, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    goto/16 :goto_1

    :cond_9
    iget v8, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    if-eqz v8, :cond_a

    iget v8, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    sget v9, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_1

    :cond_a
    int-to-float v8, v5

    sget v9, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v8, v9

    float-to-int v0, v8

    sget-boolean v8, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-nez v8, :cond_b

    sget v8, Lcom/android/calendar/DayView;->MAX_UNEXPANDED_ALLDAY_HEIGHT:I

    if-le v0, v8, :cond_b

    iget v8, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    int-to-float v8, v8

    sget v9, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v8, v9

    float-to-int v0, v8

    goto/16 :goto_1

    :cond_b
    if-le v0, v4, :cond_2

    move v0, v4

    goto/16 :goto_1

    :cond_c
    iput-boolean v12, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    goto/16 :goto_2
.end method

.method private resetSelectedHour()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v2, p0, Lcom/android/calendar/DayView;->mNumHours:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    iget v1, p0, Lcom/android/calendar/DayView;->mNumHours:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x3

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    goto :goto_0
.end method

.method private saveSelectionPosition(FFFF)V
    .locals 2
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    float-to-int v1, p1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    float-to-int v1, p3

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    float-to-int v1, p2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPrevBox:Landroid/graphics/Rect;

    float-to-int v1, p4

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private sendAccessibilityEventAsNeeded(Z)V
    .locals 17
    .param p1    # Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/DayView;->mLastSelectionDayForAccessibility:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/DayView;->mSelectionDayForAccessibility:I

    if-eq v11, v12, :cond_9

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/DayView;->mLastSelectionHourForAccessibility:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/DayView;->mSelectionHourForAccessibility:I

    if-eq v11, v12, :cond_a

    const/4 v5, 0x1

    :goto_2
    if-nez v3, :cond_2

    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mLastSelectedEventForAccessibility:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    if-eq v11, v12, :cond_0

    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/DayView;->mSelectionDayForAccessibility:I

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/calendar/DayView;->mLastSelectionDayForAccessibility:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/DayView;->mSelectionHourForAccessibility:I

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/calendar/DayView;->mLastSelectionHourForAccessibility:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/calendar/DayView;->mLastSelectedEventForAccessibility:Lcom/android/calendar/Event;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeForAccessibility()Landroid/text/format/Time;

    move-result-object v11

    const-string v12, "%A "

    invoke-virtual {v11, v12}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeForAccessibility()Landroid/text/format/Time;

    move-result-object v12

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/calendar/DayView;->mIs24HourFormat:Z

    if-eqz v11, :cond_b

    const-string v11, "%k"

    :goto_3
    invoke-virtual {v12, v11}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    if-nez v3, :cond_5

    if-eqz v5, :cond_6

    :cond_5
    const-string v11, ". "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    if-eqz p1, :cond_e

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mEventCountTemplate:Ljava/lang/String;

    if-nez v11, :cond_7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    const v12, 0x7f0c00e1

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/calendar/DayView;->mEventCountTemplate:Ljava/lang/String;

    :cond_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    if-nez v11, :cond_c

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/Event;

    const/4 v11, 0x1

    if-le v10, v11, :cond_8

    sget-object v11, Lcom/android/calendar/DayView;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    sget-object v11, Lcom/android/calendar/DayView;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/DayView;->mEventCountTemplate:Ljava/lang/String;

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v7, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v6, v7

    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/DayView;->appendEventAccessibilityString(Ljava/lang/StringBuilder;Lcom/android/calendar/Event;)V

    goto :goto_4

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_b
    const-string v11, "%l%p"

    goto/16 :goto_3

    :cond_c
    const/4 v11, 0x1

    if-le v10, v11, :cond_d

    sget-object v11, Lcom/android/calendar/DayView;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    sget-object v11, Lcom/android/calendar/DayView;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/DayView;->mEventCountTemplate:Ljava/lang/String;

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v11}, Lcom/android/calendar/DayView;->appendEventAccessibilityString(Ljava/lang/StringBuilder;Lcom/android/calendar/Event;)V

    :cond_e
    :goto_5
    if-nez v3, :cond_f

    if-nez v5, :cond_f

    if-eqz p1, :cond_0

    :cond_f
    const/16 v11, 0x8

    invoke-static {v11}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-virtual {v4, v11}, Landroid/view/accessibility/AccessibilityRecord;->setAddedCount(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto/16 :goto_0

    :cond_10
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/DayView;->mCreateNewEventString:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method private setSelectedDay(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iput p1, p0, Lcom/android/calendar/DayView;->mSelectionDayForAccessibility:I

    return-void
.end method

.method private setSelectedEvent(Lcom/android/calendar/Event;)V
    .locals 0
    .param p1    # Lcom/android/calendar/Event;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iput-object p1, p0, Lcom/android/calendar/DayView;->mSelectedEventForAccessibility:Lcom/android/calendar/Event;

    return-void
.end method

.method private setSelectedHour(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput p1, p0, Lcom/android/calendar/DayView;->mSelectionHourForAccessibility:I

    return-void
.end method

.method private setSelectionFromPosition(IIZ)Z
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    iget-object v4, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget v3, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iget v5, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :cond_0
    iget v8, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    if-ge p1, v8, :cond_1

    iget p1, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    :cond_1
    iget v8, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sub-int v8, p1, v8

    iget v9, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    add-int/lit8 v9, v9, 0x1

    div-int v1, v8, v9

    iget v8, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-lt v1, v8, :cond_2

    iget v8, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/lit8 v1, v8, -0x1

    :cond_2
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    add-int/2addr v1, v8

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    sget v8, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    if-ge p2, v8, :cond_3

    invoke-direct {p0, v6}, Lcom/android/calendar/DayView;->sendAccessibilityEventAsNeeded(Z)V

    :goto_0
    return v6

    :cond_3
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    invoke-direct {p0, v8}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    iget v8, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    if-ge p2, v8, :cond_5

    iput-boolean v7, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/DayView;->findSelectedEvent(II)V

    invoke-direct {p0, v7}, Lcom/android/calendar/DayView;->sendAccessibilityEventAsNeeded(Z)V

    if-eqz p3, :cond_4

    iput-object v4, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iput v3, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    iput v5, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput-boolean v2, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :cond_4
    move v6, v7

    goto :goto_0

    :cond_5
    iget v8, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    sub-int v0, p2, v8

    iget v8, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    if-ge v0, v8, :cond_6

    iget v8, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    add-int/lit8 v8, v8, -0x1

    invoke-direct {p0, v8}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    :goto_2
    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    goto :goto_1

    :cond_6
    iget v8, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iget v9, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    sub-int v9, v0, v9

    sget v10, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v10, v10, 0x1

    div-int/2addr v9, v10

    add-int/2addr v8, v9

    invoke-direct {p0, v8}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    goto :goto_2
.end method

.method private setupAllDayTextRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1    # Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_2

    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_BOTTOM_MARGIN:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_3

    iget v0, p1, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_TOP_MARGIN:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_BOTTOM_MARGIN:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    :cond_3
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_1

    iget v0, p1, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p1, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_RIGHT_MARGIN:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method private setupHourTextPaint(Landroid/graphics/Paint;)V
    .locals 1
    .param p1    # Landroid/graphics/Paint;

    sget v0, Lcom/android/calendar/DayView;->mCalendarHourLabelColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget v0, Lcom/android/calendar/DayView;->HOURS_TEXT_SIZE:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private setupTextRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1    # Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_2

    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_BOTTOM_MARGIN:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_3

    iget v0, p1, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_TOP_MARGIN:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_BOTTOM_MARGIN:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    :cond_3
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    sget v2, Lcom/android/calendar/DayView;->EVENT_TEXT_RIGHT_MARGIN:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_1

    iget v0, p1, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_LEFT_MARGIN:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p1, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/android/calendar/DayView;->EVENT_TEXT_RIGHT_MARGIN:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method private switchViews(ZFFF)Landroid/view/View;
    .locals 25
    .param p1    # Z
    .param p2    # F
    .param p3    # F
    .param p4    # F

    sub-float v4, p3, p2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/DayView;->mAnimationDistance:F

    sget-boolean v4, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "switchViews("

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ") O:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " Dist:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/DayView;->mAnimationDistance:F

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float v22, v4, p3

    const/high16 v4, 0x3f800000

    cmpl-float v4, v22, v4

    if-lez v4, :cond_1

    const/high16 v22, 0x3f800000

    :cond_1
    if-eqz p1, :cond_3

    const/high16 v4, 0x3f800000

    sub-float v5, v4, v22

    const/4 v7, 0x0

    move/from16 v0, v22

    neg-float v0, v0

    move/from16 v21, v0

    const/high16 v12, -0x40800000

    :goto_0
    new-instance v23, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget-object v4, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    invoke-virtual {v4}, Lcom/android/calendar/CalendarController;->getTime()J

    move-result-wide v9

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/text/format/Time;->set(J)V

    if-eqz p1, :cond_4

    move-object/from16 v0, v23

    iget v4, v0, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v4, v6

    move-object/from16 v0, v23

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const/4 v6, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v9

    invoke-virtual {v4, v9, v10}, Lcom/android/calendar/CalendarController;->setTime(J)V

    move-object/from16 v20, v23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v6, 0x7

    if-ne v4, v6, :cond_2

    new-instance v20, Landroid/text/format/Time;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->adjustToBeginningOfWeek(Landroid/text/format/Time;)V

    :cond_2
    new-instance v19, Landroid/text/format/Time;

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, v19

    iget v4, v0, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/lit8 v6, v6, -0x1

    add-int/2addr v4, v6

    move-object/from16 v0, v19

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    new-instance v3, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v8, Landroid/view/animation/TranslateAnimation;

    const/4 v9, 0x1

    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v10, v21

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float v4, p3, v4

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2}, Lcom/android/calendar/DayView;->calculateDuration(FFF)J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mHScrollInterpolator:Lcom/android/calendar/DayView$ScrollInterpolator;

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mHScrollInterpolator:Lcom/android/calendar/DayView$ScrollInterpolator;

    invoke-virtual {v8, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    move-wide/from16 v0, v17

    invoke-virtual {v8, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v4, Lcom/android/calendar/DayView$GotoBroadcaster;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-direct {v4, v0, v1, v2}, Lcom/android/calendar/DayView$GotoBroadcaster;-><init>(Lcom/android/calendar/DayView;Landroid/text/format/Time;Landroid/text/format/Time;)V

    invoke-virtual {v8, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v8}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    move-result-object v24

    check-cast v24, Lcom/android/calendar/DayView;

    invoke-virtual/range {v24 .. v24}, Lcom/android/calendar/DayView;->cleanup()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->showNext()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    move-result-object v24

    check-cast v24, Lcom/android/calendar/DayView;

    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4, v6}, Lcom/android/calendar/DayView;->setSelected(Landroid/text/format/Time;ZZ)V

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->requestFocus()Z

    invoke-virtual/range {v24 .. v24}, Lcom/android/calendar/DayView;->reloadEvents()V

    invoke-virtual/range {v24 .. v24}, Lcom/android/calendar/DayView;->updateTitle()V

    invoke-virtual/range {v24 .. v24}, Lcom/android/calendar/DayView;->restartCurrentTimeUpdates()V

    return-object v24

    :cond_3
    const/high16 v4, 0x3f800000

    sub-float v5, v22, v4

    const/4 v7, 0x0

    move/from16 v21, v22

    const/high16 v12, 0x3f800000

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, v23

    iget v4, v0, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int/2addr v4, v6

    move-object/from16 v0, v23

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_1
.end method

.method private switchViews(Z)V
    .locals 31
    .param p1    # Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    if-eqz p1, :cond_4

    if-nez v30, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v8

    const-wide/32 v2, 0x36ee80

    add-long v10, v8, v2

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v2, :cond_0

    const-wide/16 v14, 0x10

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v4, 0x1

    const-wide/16 v6, -0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    move-object/from16 v16, v0

    const-wide/16 v18, 0x2

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->id:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->startMillis:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->endMillis:J

    move-wide/from16 v24, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v28

    move-object/from16 v17, p0

    invoke-virtual/range {v16 .. v29}, Lcom/android/calendar/CalendarController;->sendEventRelatedEvent(Ljava/lang/Object;JJJJIIJ)V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    move-object/from16 v16, v0

    const-wide/16 v18, 0x2

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->id:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->startMillis:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->endMillis:J

    move-wide/from16 v24, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v28

    move-object/from16 v17, p0

    invoke-virtual/range {v16 .. v29}, Lcom/android/calendar/CalendarController;->sendEventRelatedEvent(Ljava/lang/Object;JJJJIIJ)V

    goto :goto_0

    :cond_6
    if-nez v30, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v8

    const-wide/32 v2, 0x36ee80

    add-long v10, v8, v2

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v2, :cond_7

    const-wide/16 v14, 0x10

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v4, 0x1

    const-wide/16 v6, -0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mIsAccessibilityEnabled:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    move-object/from16 v16, v0

    const-wide/16 v18, 0x2

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->id:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->startMillis:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/android/calendar/Event;->endMillis:J

    move-wide/from16 v24, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v28

    move-object/from16 v17, p0

    invoke-virtual/range {v16 .. v29}, Lcom/android/calendar/CalendarController;->sendEventRelatedEvent(Ljava/lang/Object;JJJJIIJ)V

    goto/16 :goto_0
.end method

.method private updateEventDetails()V
    .locals 15

    const/16 v14, 0x8

    const/4 v13, 0x0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-wide v0, p0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    iget-object v2, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-wide v2, v2, Lcom/android/calendar/Event;->id:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-wide v0, v0, Lcom/android/calendar/Event;->id:J

    iput-wide v0, p0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mDismissPopup:Lcom/android/calendar/DayView$DismissPopup;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v7, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    const v1, 0x7f10001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iget-object v0, v7, Lcom/android/calendar/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    const v1, 0x7f100018

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iget-boolean v0, v7, Lcom/android/calendar/Event;->hasAlarm:Z

    if-eqz v0, :cond_5

    move v0, v13

    :goto_1
    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iget-boolean v0, v7, Lcom/android/calendar/Event;->isRepeating:Z

    if-eqz v0, :cond_6

    move v0, v13

    :goto_2
    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, v7, Lcom/android/calendar/Event;->allDay:Z

    if-eqz v0, :cond_7

    const v5, 0x82012

    :goto_3
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    or-int/lit16 v5, v5, 0x80

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    iget-wide v1, v7, Lcom/android/calendar/Event;->startMillis:J

    iget-wide v3, v7, Lcom/android/calendar/Event;->endMillis:J

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    const v1, 0x7f100030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    const v1, 0x7f100016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iget-object v0, v7, Lcom/android/calendar/Event;->location:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    :goto_4
    invoke-virtual {v12, v14}, Landroid/view/View;->setVisibility(I)V

    if-nez v6, :cond_4

    iget-object v0, v7, Lcom/android/calendar/Event;->location:Ljava/lang/CharSequence;

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    const/16 v1, 0x53

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    const/4 v3, 0x5

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mDismissPopup:Lcom/android/calendar/DayView$DismissPopup;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_5
    move v0, v14

    goto :goto_1

    :cond_6
    move v0, v14

    goto :goto_2

    :cond_7
    const v5, 0x81413

    goto :goto_3

    :cond_8
    move v14, v13

    goto :goto_4
.end method


# virtual methods
.method public cleanup()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mPaused:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mDismissPopup:Lcom/android/calendar/DayView$DismissPopup;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    const-string v1, "preferences_default_cell_height"

    sget v2, Lcom/android/calendar/DayView;->mCellHeight:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/Utils;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/android/calendar/DayView;->eventClickCleanup()V

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    return-void
.end method

.method clearCachedEvents()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/DayView;->mLastReloadMillis:J

    return-void
.end method

.method public compareToVisibleTimeRange(Landroid/text/format/Time;)I
    .locals 8
    .param p1    # Landroid/text/format/Time;

    const/4 v7, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v1, v4, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v2, v4, Landroid/text/format/Time;->minute:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v3, v4, Landroid/text/format/Time;->second:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v5, v4, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v5, v4, Landroid/text/format/Time;->minute:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v5, v4, Landroid/text/format/Time;->second:I

    sget-boolean v4, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Begin "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v6}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Diff  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-static {p1, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v5, v4, Landroid/text/format/Time;->monthDay:I

    iget v6, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v5, v6

    iput v5, v4, Landroid/text/format/Time;->monthDay:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-static {p1, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v0

    sget-boolean v4, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "End   "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v6}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v5, v4, Landroid/text/format/Time;->monthDay:I

    iget v6, p0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/text/format/Time;->monthDay:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    if-gez v0, :cond_4

    const/4 v0, 0x0

    :cond_2
    :goto_0
    sget-boolean v4, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v4, :cond_3

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Diff: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v1, v4, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v2, v4, Landroid/text/format/Time;->minute:I

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iput v3, v4, Landroid/text/format/Time;->second:I

    return v0

    :cond_4
    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected drawMoreAlldayEvents(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Paint;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f000000

    invoke-direct {p0, p3}, Lcom/android/calendar/DayView;->computeDayLeftPosition(I)I

    move-result v4

    sget v5, Lcom/android/calendar/DayView;->EVENT_ALL_DAY_TEXT_LEFT_MARGIN:I

    add-int v2, v4, v5

    iget v4, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v4, v4

    sget v5, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v3, v4

    iget-object v0, p0, Lcom/android/calendar/DayView;->mRect:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->top:I

    iput v2, v0, Landroid/graphics/Rect;->left:I

    sget v4, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    add-int/2addr v4, v3

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    sget v4, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    add-int/2addr v4, v2

    iput v4, v0, Landroid/graphics/Rect;->right:I

    sget v4, Lcom/android/calendar/DayView;->mMoreEventsTextColor:I

    invoke-virtual {p4, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget v4, Lcom/android/calendar/DayView;->EVENT_RECT_STROKE_WIDTH:I

    int-to-float v4, v4

    invoke-virtual {p4, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p4, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p1, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p4, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v4, Lcom/android/calendar/DayView;->EVENT_TEXT_FONT_SIZE:F

    invoke-virtual {p4, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e0005

    invoke-virtual {v4, v5, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    sget v4, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    add-int/2addr v3, v4

    sget v4, Lcom/android/calendar/DayView;->EVENT_SQUARE_WIDTH:I

    sget v5, Lcom/android/calendar/DayView;->EVENT_LINE_PADDING:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    int-to-float v5, v2

    int-to-float v6, v3

    invoke-virtual {p1, v4, v5, v6, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getEventsAlpha()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    return v0
.end method

.method getFirstVisibleHour()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    return v0
.end method

.method getNewEvent()Lcom/android/calendar/Event;
    .locals 4

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedMinutesSinceMidnight()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/DayView;->getNewEvent(IJI)Lcom/android/calendar/Event;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedDay()Landroid/text/format/Time;
    .locals 2

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    return-object v0
.end method

.method getSelectedEvent()Lcom/android/calendar/Event;
    .locals 4

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedMinutesSinceMidnight()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/DayView;->getNewEvent(IJI)Lcom/android/calendar/Event;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    goto :goto_0
.end method

.method getSelectedMinutesSinceMidnight()I
    .locals 1

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    mul-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method getSelectedTime()Landroid/text/format/Time;
    .locals 2

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    return-object v0
.end method

.method getSelectedTimeForAccessibility()Landroid/text/format/Time;
    .locals 2

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHourForAccessibility:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    return-object v0
.end method

.method getSelectedTimeInMillis()J
    .locals 3

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    iget v1, p0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v1

    return-wide v1
.end method

.method public handleOnResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/calendar/DayView;->initAccessibilityVariables()V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    const-string v1, "preferences_tardis_1"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/Utils;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sput v2, Lcom/android/calendar/DayView;->mFutureBgColor:I

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mIs24HourFormat:Z

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mIs24HourFormat:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/calendar/CalendarData;->s24Hours:[Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/android/calendar/DayView;->mHourStrs:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstDayOfWeek:I

    iput v2, p0, Lcom/android/calendar/DayView;->mLastSelectionDayForAccessibility:I

    iput v2, p0, Lcom/android/calendar/DayView;->mLastSelectionHourForAccessibility:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mLastSelectedEventForAccessibility:Lcom/android/calendar/Event;

    iput v2, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    return-void

    :cond_0
    sget v0, Lcom/android/calendar/DayView;->mFutureBgColorRes:I

    sput v0, Lcom/android/calendar/DayView;->mFutureBgColor:I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/calendar/CalendarData;->s12HoursNoAmPm:[Ljava/lang/String;

    goto :goto_1
.end method

.method public initAllDayHeights()V
    .locals 3

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    iget v2, p0, Lcom/android/calendar/DayView;->mMaxUnexpandedAlldayEventCount:I

    if-gt v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v1, Lcom/android/calendar/DayView;->mShowAllAllDayEvents:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    sget v2, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v1, v2

    sget v2, Lcom/android/calendar/DayView;->MIN_HOURS_HEIGHT:I

    sub-int v0, v1, v2

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxAlldayEvents:I

    div-int v1, v0, v1

    iput v1, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/android/calendar/DayView;->MIN_UNEXPANDED_ALLDAY_EVENT_HEIGHT:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    goto :goto_0
.end method

.method isEventSelected()Z
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopupView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/DayView;->switchViews(Z)V

    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 11
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    iget v0, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v1

    const/16 v5, 0x1403

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    move-wide v3, v1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v9}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_4

    const/4 v0, 0x1

    if-lt v8, v0, :cond_3

    const/4 v0, 0x0

    const/4 v3, 0x5

    const/4 v4, 0x0

    const v10, 0x7f0c004e

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080041

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    invoke-static {v0, v3}, Lcom/android/calendar/DayView;->getEventAccessLevel(Landroid/content/Context;Lcom/android/calendar/Event;)I

    move-result v6

    const/4 v0, 0x2

    if-ne v6, v0, :cond_1

    const/4 v0, 0x0

    const/4 v3, 0x7

    const/4 v4, 0x0

    const v10, 0x7f0c0050

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x108003e

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x65

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :cond_1
    const/4 v0, 0x1

    if-lt v6, v0, :cond_2

    const/4 v0, 0x0

    const/16 v3, 0x8

    const/4 v4, 0x0

    const v10, 0x7f0c0051

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x108003c

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_2
    const/4 v0, 0x0

    const/4 v3, 0x6

    const/4 v4, 0x0

    const v10, 0x7f0c004f

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080033

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x6e

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void

    :cond_3
    const/4 v0, 0x0

    const/4 v3, 0x6

    const/4 v4, 0x0

    const v10, 0x7f0c004f

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080033

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x6e

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    if-lt v8, v0, :cond_7

    const/4 v0, 0x0

    const/4 v3, 0x5

    const/4 v4, 0x0

    const v10, 0x7f0c004e

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080041

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    invoke-static {v0, v3}, Lcom/android/calendar/DayView;->getEventAccessLevel(Landroid/content/Context;Lcom/android/calendar/Event;)I

    move-result v6

    const/4 v0, 0x2

    if-ne v6, v0, :cond_5

    const/4 v0, 0x0

    const/4 v3, 0x7

    const/4 v4, 0x0

    const v10, 0x7f0c0050

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x108003e

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x65

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :cond_5
    const/4 v0, 0x1

    if-lt v6, v0, :cond_6

    const/4 v0, 0x0

    const/16 v3, 0x8

    const/4 v4, 0x0

    const v10, 0x7f0c0051

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x108003c

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_6
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/calendar/MTKUtils;->isEventShareAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    const/16 v3, 0x9

    const/4 v4, 0x0

    const v10, 0x7f0c0001

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080033

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x73

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :cond_7
    const/4 v0, 0x0

    const/4 v3, 0x6

    const/4 v4, 0x0

    const v10, 0x7f0c004f

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x1080033

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x6e

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x0

    const v10, 0x7f0c0049

    invoke-interface {p1, v0, v3, v4, v10}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContextMenuHandler:Lcom/android/calendar/DayView$ContextMenuHandler;

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x108003b

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v0, 0x64

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->cleanup()V

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-boolean v4, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/android/calendar/DayView;->remeasure(II)V

    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    neg-int v4, v4

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    add-int/2addr v4, v5

    int-to-float v3, v4

    iget v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mDestRect:Landroid/graphics/Rect;

    iget v4, p0, Lcom/android/calendar/DayView;->mFirstCell:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    iput v6, v0, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iput v4, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->doDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget v4, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    if-lez v4, :cond_6

    iget v4, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    int-to-float v2, v4

    :goto_0
    neg-float v4, v3

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/DayView;

    iput v6, v1, Lcom/android/calendar/DayView;->mTouchMode:I

    invoke-virtual {v1, p1}, Lcom/android/calendar/DayView;->onDraw(Landroid/graphics/Canvas;)V

    neg-float v4, v2

    invoke-virtual {p1, v4, v7}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/calendar/DayView;->drawAfterScroll(Landroid/graphics/Canvas;)V

    iget-boolean v4, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/android/calendar/DayView;->mUpdateToast:Z

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/android/calendar/DayView;->updateEventDetails()V

    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mUpdateToast:Z

    :cond_1
    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    iget-object v4, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v4

    if-nez v4, :cond_4

    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    if-eqz v4, :cond_2

    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v4, v4

    invoke-virtual {p1, v7, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    iget-object v4, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_3
    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    if-eqz v4, :cond_4

    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v7, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_4
    iget-object v4, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v4

    if-nez v4, :cond_5

    const/high16 v4, 0x43340000

    iget v5, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v6, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_6
    iget v4, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    neg-int v4, v4

    int-to-float v2, v4

    goto :goto_0

    :cond_7
    iget v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    int-to-float v4, v4

    neg-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown hover event action. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mTouchExplorationEnabled:Z

    if-nez v2, :cond_2

    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :cond_1
    :goto_1
    return v1

    :pswitch_1
    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v3, "ACTION_HOVER_ENTER"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v3, "ACTION_HOVER_MOVE"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_3
    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v3, "ACTION_HOVER_EXIT"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/DayView;->setSelectionFromPosition(IIZ)Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 22
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-nez v2, :cond_4

    const/16 v2, 0x42

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x16

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x15

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x13

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x14

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/16 v2, 0x17

    move/from16 v0, p1

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    move/from16 v0, p1

    if-eq v0, v2, :cond_3

    const/16 v2, 0x52

    move/from16 v0, p1

    if-ne v0, v2, :cond_4

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/DayView;->mSelectionMode:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mScrolling:Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v20, v0

    sparse-switch p1, :sswitch_data_0

    invoke-super/range {p0 .. p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    move-object/from16 v19, v0

    if-nez v19, :cond_5

    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    move-object/from16 v0, v19

    iget-wide v3, v0, Lcom/android/calendar/Event;->startMillis:J

    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/android/calendar/Event;->endMillis:J

    move-object/from16 v0, v19

    iget-wide v7, v0, Lcom/android/calendar/Event;->id:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

    const/4 v9, -0x1

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/DeleteEventHelper;->delete(JJJI)V

    const/4 v2, 0x1

    goto :goto_0

    :sswitch_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->switchViews(Z)V

    const/4 v2, 0x1

    goto :goto_0

    :sswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->startTracking()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_6
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-object v2, v2, Lcom/android/calendar/Event;->nextLeft:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-nez v2, :cond_8

    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    add-int/lit8 v20, v20, -0x1

    :cond_8
    const/16 v18, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    move/from16 v0, v20

    if-lt v0, v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mLastJulianDay:I

    move/from16 v0, v20

    if-le v0, v2, :cond_12

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v21

    check-cast v21, Lcom/android/calendar/DayView;

    move-object/from16 v0, v21

    iget-object v13, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v13, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    move/from16 v0, v20

    if-ge v0, v2, :cond_11

    iget v2, v13, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/DayView;->mNumDays:I

    sub-int/2addr v2, v9

    iput v2, v13, Landroid/text/format/Time;->monthDay:I

    :goto_2
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->initView(Lcom/android/calendar/DayView;)V

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v13}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/lit8 v9, v9, -0x1

    add-int/2addr v2, v9

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v11, 0x20

    const-wide/16 v15, -0x1

    const/16 v17, 0x0

    move-object/from16 v10, p0

    move-object v14, v5

    invoke-virtual/range {v9 .. v17}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    const/4 v2, 0x1

    goto/16 :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-object v2, v2, Lcom/android/calendar/Event;->nextRight:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-nez v2, :cond_b

    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    add-int/lit8 v20, v20, 0x1

    :cond_b
    const/16 v18, 0x1

    goto/16 :goto_1

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-object v2, v2, Lcom/android/calendar/Event;->nextUp:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-nez v2, :cond_d

    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->adjustHourSelection()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    :cond_d
    const/16 v18, 0x1

    goto/16 :goto_1

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    iget-object v2, v2, Lcom/android/calendar/Event;->nextDown:Lcom/android/calendar/Event;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvent:Lcom/android/calendar/Event;

    if-nez v2, :cond_f

    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/android/calendar/DayView;->mLastPopupEventID:J

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-eqz v2, :cond_10

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    :cond_f
    :goto_3
    const/16 v18, 0x1

    goto/16 :goto_1

    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/DayView;->adjustHourSelection()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    goto :goto_3

    :cond_11
    iget v2, v13, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/2addr v2, v9

    iput v2, v13, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_2

    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    move/from16 v0, v20

    if-eq v2, v0, :cond_13

    new-instance v13, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v13, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionDay:I

    invoke-static {v13, v2}, Lcom/android/calendar/Utils;->setJulianDayInGeneral(Landroid/text/format/Time;I)J

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/DayView;->mSelectionHour:I

    iput v2, v13, Landroid/text/format/Time;->hour:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v11, 0x20

    const-wide/16 v15, -0x1

    const/16 v17, 0x0

    move-object/from16 v10, p0

    move-object v14, v13

    invoke-virtual/range {v9 .. v17}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    :cond_13
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/DayView;->mUpdateToast:Z

    if-eqz v18, :cond_14

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_14
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x13 -> :sswitch_5
        0x14 -> :sswitch_6
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x42 -> :sswitch_1
        0x43 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2

    :pswitch_0
    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    if-ne v2, v6, :cond_1

    const/4 v2, 0x2

    iput v2, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    invoke-direct {p0, v6}, Lcom/android/calendar/DayView;->switchViews(Z)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/calendar/DayView;->mSelectionMode:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Landroid/view/View;->performLongClick()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x1

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v1

    iget-boolean v0, p0, Lcom/android/calendar/DayView;->mSelectionAllday:Z

    if-nez v0, :cond_0

    or-int/lit8 v5, v5, 0x1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    or-int/lit16 v5, v5, 0x80

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    move-wide v3, v1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/DayView;->mLongPressTitle:Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/calendar/DayView;->mLongPressTitle:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/DayView;->mLongPressItems:[Ljava/lang/CharSequence;

    new-instance v4, Lcom/android/calendar/DayView$8;

    invoke-direct {v4, p0}, Lcom/android/calendar/DayView$8;-><init>(Lcom/android/calendar/DayView;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return v6
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v6, 0x1

    sget v3, Lcom/android/calendar/DayView;->MIN_Y_SPAN:I

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/android/calendar/DayView;->mCellHeightBeforeScaleGesture:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    iget v4, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    sput v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v4, Lcom/android/calendar/DayView;->mMinCellHeight:I

    if-ge v3, v4, :cond_3

    iput v2, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    sget v3, Lcom/android/calendar/DayView;->mMinCellHeight:I

    sput v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v3, Lcom/android/calendar/DayView;->mMinCellHeight:I

    iput v3, p0, Lcom/android/calendar/DayView;->mCellHeightBeforeScaleGesture:I

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    float-to-int v3, v3

    sget v4, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    sub-int v1, v3, v4

    iget v3, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v3, v1

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v3, v3, 0x18

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    sget-boolean v3, Lcom/android/calendar/DayView;->DEBUG_SCALING:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v3, v3

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    div-float v0, v3, v4

    sget-object v3, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onScale: mGestureCenterHour:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\tViewStartHour: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\tmViewStartY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\tmCellHeight:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/calendar/DayView;->mCellHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SpanY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    if-gez v3, :cond_4

    const/4 v3, 0x0

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    add-int/2addr v3, v1

    int-to-float v3, v3

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeFirstHour()V

    iput-boolean v6, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return v6

    :cond_3
    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v4, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    if-le v3, v4, :cond_0

    iput v2, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    sget v3, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    sput v3, Lcom/android/calendar/DayView;->mCellHeight:I

    sget v3, Lcom/android/calendar/DayView;->MAX_CELL_HEIGHT:I

    iput v3, p0, Lcom/android/calendar/DayView;->mCellHeightBeforeScaleGesture:I

    goto/16 :goto_0

    :cond_4
    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v4, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v3, v4, :cond_2

    iget v3, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    iput v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    add-int/2addr v3, v1

    int-to-float v3, v3

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    goto :goto_1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    sget v3, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    int-to-float v3, v3

    sub-float v1, v2, v3

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v1

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    sget v2, Lcom/android/calendar/DayView;->MIN_Y_SPAN:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    sget v2, Lcom/android/calendar/DayView;->mCellHeight:I

    iput v2, p0, Lcom/android/calendar/DayView;->mCellHeightBeforeScaleGesture:I

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG_SCALING:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    div-float v0, v2, v3

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onScaleBegin: mGestureCenterHour:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/calendar/DayView;->mGestureCenterHour:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tViewStartHour: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmViewStartY:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmCellHeight:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/android/calendar/DayView;->mCellHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " SpanY:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    iput v0, p0, Lcom/android/calendar/DayView;->mScrollStartY:I

    iput v1, p0, Lcom/android/calendar/DayView;->mInitialScrollY:F

    iput v1, p0, Lcom/android/calendar/DayView;->mInitialScrollX:F

    iput v1, p0, Lcom/android/calendar/DayView;->mStartingSpanY:F

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v2, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v3, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v4, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    div-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/DayView;->mNumDays:I

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    iput p2, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    iget v3, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/EdgeEffect;->setSize(II)V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    iget v3, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    iget v4, p0, Lcom/android/calendar/DayView;->mViewHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/EdgeEffect;->setSize(II)V

    iget v2, p0, Lcom/android/calendar/DayView;->mHoursWidth:I

    sub-int v0, p1, v2

    iget v2, p0, Lcom/android/calendar/DayView;->mNumDays:I

    mul-int/lit8 v2, v2, 0x1

    sub-int v2, v0, v2

    iget v3, p0, Lcom/android/calendar/DayView;->mNumDays:I

    div-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/DayView;->mCellWidth:I

    div-int/lit8 v2, p1, 0x7

    sput v2, Lcom/android/calendar/DayView;->mHorizontalSnapBackThreshold:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sget v2, Lcom/android/calendar/DayView;->HOURS_TEXT_SIZE:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/calendar/DayView;->mHoursTextHeight:I

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/DayView;->remeasure(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ev.getPointerCount() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-eq v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const/4 v5, 0x6

    if-eq v2, v5, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const/4 v5, 0x5

    if-ne v2, v5, :cond_2

    :cond_1
    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mRecalCenterHour:Z

    :cond_2
    iget v2, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v2, v2, 0x40

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/DayView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_3
    packed-switch v0, :pswitch_data_0

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_4

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not MotionEvent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_5
    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mStartingScroll:Z

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_6

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_DOWN ev.getDownTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Cnt="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget v2, p0, Lcom/android/calendar/DayView;->mAlldayHeight:I

    sget v5, Lcom/android/calendar/DayView;->DAY_HEADER_HEIGHT:I

    add-int/2addr v2, v5

    sget v5, Lcom/android/calendar/DayView;->ALLDAY_TOP_MARGIN:I

    add-int v1, v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    int-to-float v5, v1

    cmpg-float v2, v2, v5

    if-gez v2, :cond_7

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mTouchStartedInAlldayArea:Z

    :goto_1
    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    iget-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_7
    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mTouchStartedInAlldayArea:Z

    goto :goto_1

    :pswitch_1
    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_8

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_MOVE Cnt="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_2
    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_9

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_UP Cnt="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v2, p0, Lcom/android/calendar/DayView;->mEdgeEffectBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mStartingScroll:Z

    iget-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    if-nez v2, :cond_a

    iput-boolean v3, p0, Lcom/android/calendar/DayView;->mHandleActionUp:Z

    iput v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_0

    :cond_a
    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mOnFlingCalled:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    if-eqz v2, :cond_b

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    invoke-direct {p0}, Lcom/android/calendar/DayView;->resetSelectedHour()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_b
    iget v2, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_5

    iput v4, p0, Lcom/android/calendar/DayView;->mTouchMode:I

    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sget v5, Lcom/android/calendar/DayView;->mHorizontalSnapBackThreshold:I

    if-le v2, v5, :cond_e

    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_c

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v5, "- horizontal scroll: switch views"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget v2, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    if-lez v2, :cond_d

    move v2, v3

    :goto_2
    iget v5, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/calendar/DayView;->mViewWidth:I

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-direct {p0, v2, v5, v6, v7}, Lcom/android/calendar/DayView;->switchViews(ZFFF)Landroid/view/View;

    iput v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    goto/16 :goto_0

    :cond_d
    move v2, v4

    goto :goto_2

    :cond_e
    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_f

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v5, "- horizontal scroll: snap back"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    invoke-direct {p0}, Lcom/android/calendar/DayView;->recalc()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    iput v4, p0, Lcom/android/calendar/DayView;->mViewStartX:I

    goto/16 :goto_0

    :pswitch_3
    sget-boolean v2, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v2, :cond_10

    sget-object v2, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    const-string v5, "ACTION_CANCEL"

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget-object v2, p0, Lcom/android/calendar/DayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mScrolling:Z

    invoke-direct {p0}, Lcom/android/calendar/DayView;->resetSelectedHour()V

    goto/16 :goto_0

    :cond_11
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method reloadEvents()V
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    invoke-direct {p0, v1}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    iput-object v1, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v8, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mTZUpdater:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v8, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iput v3, v8, Landroid/text/format/Time;->hour:I

    iput v3, v8, Landroid/text/format/Time;->minute:I

    iput v3, v8, Landroid/text/format/Time;->second:I

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    iget-wide v0, p0, Lcom/android/calendar/DayView;->mLastReloadMillis:J

    cmp-long v0, v6, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-wide v6, p0, Lcom/android/calendar/DayView;->mLastReloadMillis:J

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEventLoader:Lcom/android/calendar/EventLoader;

    iget v1, p0, Lcom/android/calendar/DayView;->mNumDays:I

    iget v3, p0, Lcom/android/calendar/DayView;->mFirstJulianDay:I

    new-instance v4, Lcom/android/calendar/DayView$6;

    invoke-direct {v4, p0, v2}, Lcom/android/calendar/DayView$6;-><init>(Lcom/android/calendar/DayView;Ljava/util/ArrayList;)V

    iget-object v5, p0, Lcom/android/calendar/DayView;->mCancelCallback:Ljava/lang/Runnable;

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/EventLoader;->loadEventsInBackground(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public restartCurrentTimeUpdates()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mPaused:Z

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/calendar/DayView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/DayView;->mUpdateCurrentTime:Lcom/android/calendar/DayView$UpdateCurrentTime;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public selectionFocusShow(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/DayView;->mIsSelectionFocusShow:Z

    return-void
.end method

.method public setAnimateDayEventHeight(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mAnimateDayEventHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setAnimateDayHeight(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mAnimateDayHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setAnimateTodayAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setEventsAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method setFirstVisibleHour(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    return-void
.end method

.method public setMoreAllDayEventsTextAlpha(I)V
    .locals 0
    .param p1    # I

    sput p1, Lcom/android/calendar/DayView;->mMoreAlldayEventsTextAlpha:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setSelected(Landroid/text/format/Time;ZZ)V
    .locals 11
    .param p1    # Landroid/text/format/Time;
    .param p2    # Z
    .param p3    # Z

    const/4 v5, 0x0

    const/4 v10, 0x2

    const/high16 v8, -0x80000000

    const/4 v9, 0x0

    const/4 v7, 0x1

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-virtual {v4, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->setSelectedHour(I)V

    invoke-direct {p0, v5}, Lcom/android/calendar/DayView;->setSelectedEvent(Lcom/android/calendar/Event;)V

    iput-object v5, p0, Lcom/android/calendar/DayView;->mPrevSelectedEvent:Lcom/android/calendar/Event;

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-static {v4, v9}, Lcom/android/calendar/Utils;->getJulianDayInGeneral(Landroid/text/format/Time;Z)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/calendar/DayView;->setSelectedDay(I)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mSelectedEvents:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iput-boolean v7, p0, Lcom/android/calendar/DayView;->mComputeSelectedEvents:Z

    const/high16 v1, -0x80000000

    if-nez p2, :cond_2

    iget v4, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    iget v5, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    if-ge v4, v5, :cond_7

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    sget v5, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v5, v5, 0x1

    mul-int v1, v4, v5

    :cond_0
    :goto_0
    sget-boolean v4, Lcom/android/calendar/DayView;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/android/calendar/DayView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Go "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " 1st "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "CH "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lh "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " gh "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ymax "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget v4, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le v1, v4, :cond_8

    iget v1, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/DayView;->recalc()V

    iput-boolean v7, p0, Lcom/android/calendar/DayView;->mRemeasure:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    const/4 v0, 0x0

    if-eq v1, v8, :cond_3

    const-string v4, "viewStartY"

    new-array v5, v10, [I

    iget v6, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    aput v6, v5, v9

    aput v1, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v0, 0x1

    :cond_3
    if-eqz p3, :cond_6

    iget-object v5, p0, Lcom/android/calendar/DayView;->mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/Animator;->removeAllListeners()V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_4
    const-string v4, "animateTodayAlpha"

    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/calendar/DayView;->mAnimateTodayAlpha:I

    aput v8, v6, v7

    const/4 v7, 0x1

    const/16 v8, 0xff

    aput v8, v6, v7

    invoke-static {p0, v4, v6}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/calendar/DayView;->mAnimateToday:Z

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/android/calendar/DayView$TodayAnimatorListener;->setFadingIn(Z)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

    iget-object v6, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4, v6}, Lcom/android/calendar/DayView$TodayAnimatorListener;->setAnimator(Landroid/animation/Animator;)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    iget-object v6, p0, Lcom/android/calendar/DayView;->mTodayAnimatorListener:Lcom/android/calendar/DayView$TodayAnimatorListener;

    invoke-virtual {v4, v6}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    :cond_5
    iget-object v4, p0, Lcom/android/calendar/DayView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_6
    invoke-direct {p0, v9}, Lcom/android/calendar/DayView;->sendAccessibilityEventAsNeeded(Z)V

    return-void

    :cond_7
    iget v4, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    iget v5, p0, Lcom/android/calendar/DayView;->mFirstHourOffset:I

    sub-int/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v5, v5, 0x1

    div-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/DayView;->mFirstHour:I

    add-int v2, v4, v5

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    if-lt v4, v2, :cond_0

    iget-object v4, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->minute:I

    int-to-float v5, v5

    const/high16 v6, 0x42700000

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    sget v5, Lcom/android/calendar/DayView;->mCellHeight:I

    add-int/lit8 v5, v5, 0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/DayView;->mGridAreaHeight:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v1, v4

    goto/16 :goto_0

    :cond_8
    if-gez v1, :cond_2

    if-eq v1, v8, :cond_2

    const/4 v1, 0x0

    goto/16 :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public setViewStartY(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    if-le p1, v0, :cond_0

    iget p1, p0, Lcom/android/calendar/DayView;->mMaxViewStartY:I

    :cond_0
    iput p1, p0, Lcom/android/calendar/DayView;->mViewStartY:I

    invoke-direct {p0}, Lcom/android/calendar/DayView;->computeFirstHour()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public stopEventsAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEventsCrossFadeAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayView;->mEventsCrossFadeAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    const/16 v0, 0xff

    iput v0, p0, Lcom/android/calendar/DayView;->mEventsAlpha:I

    return-void
.end method

.method public updateTitle()V
    .locals 14

    const/4 v6, 0x0

    const/4 v2, 0x1

    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/DayView;->mBaseDate:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->normalize(Z)J

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/DayView;->mNumDays:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    iget v0, v5, Landroid/text/format/Time;->minute:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->minute:I

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    const-wide/16 v10, 0x14

    iget v0, p0, Lcom/android/calendar/DayView;->mNumDays:I

    if-eq v0, v2, :cond_0

    const-wide/16 v0, 0x20

    or-long/2addr v10, v0

    iget v0, v4, Landroid/text/format/Time;->month:I

    iget v1, v5, Landroid/text/format/Time;->month:I

    if-eq v0, v1, :cond_0

    const-wide/32 v0, 0x10000

    or-long/2addr v10, v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayView;->mController:Lcom/android/calendar/CalendarController;

    const-wide/16 v2, 0x400

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    move-object v1, p0

    move-object v12, v6

    move-object v13, v6

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/CalendarController;->sendEvent(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    return-void
.end method
