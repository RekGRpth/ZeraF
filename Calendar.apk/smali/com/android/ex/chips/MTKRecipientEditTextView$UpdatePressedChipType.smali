.class public final enum Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;
.super Ljava/lang/Enum;
.source "MTKRecipientEditTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/MTKRecipientEditTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdatePressedChipType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

.field public static final enum ADD_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

.field public static final enum DELETE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

.field public static final enum UPDATE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    const-string v1, "ADD_CONTACT"

    invoke-direct {v0, v1, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->ADD_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    new-instance v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    const-string v1, "UPDATE_CONTACT"

    invoke-direct {v0, v1, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->UPDATE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    new-instance v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    const-string v1, "DELETE_CONTACT"

    invoke-direct {v0, v1, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->DELETE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    sget-object v1, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->ADD_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->UPDATE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->DELETE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->$VALUES:[Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    return-object v0
.end method

.method public static values()[Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;
    .locals 1

    sget-object v0, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->$VALUES:[Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    return-object v0
.end method
