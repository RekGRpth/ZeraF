.class public Lcom/android/ex/chips/MTKRecipientEditTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "MTKRecipientEditTextView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/ActionMode$Callback;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/android/ex/chips/RecipientAlternatesAdapter$OnCheckedItemChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipShadow;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientTextWatcher;,
        Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;
    }
.end annotation


# static fields
.field static final CHIP_LIMIT:I = 0x2

.field private static final COMMIT_CHAR_CHINESE_COMMA:C = '\uff0c'

.field private static final COMMIT_CHAR_CHINESE_SEMICOLON:C = '\uff1b'

.field private static final COMMIT_CHAR_COMMA:C = ','

.field private static final COMMIT_CHAR_SEMICOLON:C = ';'

.field private static final COMMIT_CHAR_SPACE:C = ' '

.field private static DISMISS:I = 0x0

.field private static final DISMISS_DELAY:J = 0x12cL

.field private static final MAX_CHIPS_PARSED:I = 0x64

.field private static final NAME_WRAPPER_CHAR:C = '\"'

.field private static final TAG:Ljava/lang/String; = "RecipientEditTextView"

.field private static sExcessTopPadding:I

.field private static sSelectedTextColor:I


# instance fields
.field private bPasted:Z

.field private bTouchedAfterPasted:Z

.field private mActionBarHeight:I

.field private final mAddTextWatcher:Ljava/lang/Runnable;

.field private mAddressPopup:Landroid/widget/ListPopupWindow;

.field private mAlternatesLayout:I

.field private mAlternatesListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mAlternatesPopup:Landroid/widget/ListPopupWindow;

.field private mCheckedItem:I

.field private mChipBackground:Landroid/graphics/drawable/Drawable;

.field private mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

.field private mChipDelete:Landroid/graphics/drawable/Drawable;

.field private mChipFontSize:F

.field private mChipHeight:F

.field private mChipPadding:I

.field private mCopyAddress:Ljava/lang/String;

.field private mCopyDialog:Landroid/app/Dialog;

.field private mDefaultContactPhoto:Landroid/graphics/Bitmap;

.field private mDefaultTextSize:F

.field private mDefaultVerticalOffset:I

.field private mDelayedShrink:Ljava/lang/Runnable;

.field private mDisableBringPointIntoView:Z

.field private mDragEnabled:Z

.field private mDuringAccelerateRemoveChip:Z

.field private mDuringAppendStrings:Z

.field private mEnableScrollAddText:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mHandlePendingChips:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mHasEllipsizedFirstChip:Z

.field private mIndividualReplacements:Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

.field private mInvalidChipBackground:Landroid/graphics/drawable/Drawable;

.field private mJustExpanded:Z

.field private mLimitedWidthForSpan:I

.field private mLineSpacingExtra:F

.field private mMaxLines:I

.field private mMoreChip:Landroid/text/style/ImageSpan;

.field private mMoreItem:Landroid/widget/TextView;

.field private mMoveCursorToVisible:Z

.field private mNoChips:Z

.field private final mPendingChips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingChipsCount:I

.field private mPendingStrings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviousWidth:I

.field private mRETVDiscardNextActionUp:Z

.field private mRemovedSpans:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/ex/chips/RecipientChip;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSelectedChip:Lcom/android/ex/chips/RecipientChip;

.field private mShouldShrink:Z

.field private mStringToBeRestore:Ljava/lang/String;

.field private mTemporaryRecipients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/ex/chips/RecipientChip;",
            ">;"
        }
    .end annotation
.end field

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private mTriedGettingScrollView:Z

.field private mValidator:Landroid/widget/AutoCompleteTextView$Validator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, -0x1

    const-string v0, "dismiss"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->DISMISS:I

    sput v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->sSelectedTextColor:I

    sput v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->sExcessTopPadding:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x1

    const/4 v2, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    iput-boolean v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mShouldShrink:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDragEnabled:Z

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$1;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$1;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$2;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$2;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandlePendingChips:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$3;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$3;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDelayedShrink:Ljava/lang/Runnable;

    iput-boolean v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mEnableScrollAddText:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRETVDiscardNextActionUp:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bPasted:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bTouchedAfterPasted:Z

    iput v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLimitedWidthForSpan:I

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoveCursorToVisible:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDisableBringPointIntoView:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPreviousWidth:I

    iput v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultTextSize:F

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mJustExpanded:Z

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultVerticalOffset:I

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setChipDimensions(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->sSelectedTextColor:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->sSelectedTextColor:I

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v1

    iput v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultTextSize:F

    new-instance v1, Landroid/widget/ListPopupWindow;

    invoke-direct {v1, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    new-instance v1, Landroid/widget/ListPopupWindow;

    invoke-direct {v1, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    sget-object v1, Lcom/android/internal/R$styleable;->AutoCompleteTextView:[I

    const v2, 0x101006b

    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultVerticalOffset:I

    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$4;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$4;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0}, Landroid/widget/TextView;->getInputType()I

    move-result v1

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setInputType(I)V

    invoke-virtual {p0, p0}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0, p0}, Landroid/widget/TextView;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$5;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$5;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientTextWatcher;

    invoke-direct {v1, p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientTextWatcher;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/MTKRecipientEditTextView$1;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Landroid/view/GestureDetector;

    invoke-direct {v1, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {p0, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    iput v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMaxLines:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/text/TextWatcher;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/ex/chips/MTKRecipientEditTextView;Landroid/text/TextWatcher;)Landroid/text/TextWatcher;
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Landroid/text/TextWatcher;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->unselectChip(Lcom/android/ex/chips/RecipientChip;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/text/style/ImageSpan;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/ex/chips/MTKRecipientEditTextView;)Z
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->chipsPending()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/ex/chips/MTKRecipientEditTextView;)Z
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitByCharacter()V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/widget/MultiAutoCompleteTextView$Tokenizer;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/widget/AutoCompleteTextView$Validator;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shrink()V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)I
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)I
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/ex/chips/MTKRecipientEditTextView;)Z
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDisableBringPointIntoView:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/android/ex/chips/MTKRecipientEditTextView;)Z
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientEntry;
    .param p2    # I
    .param p3    # Z
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/ex/chips/MTKRecipientEditTextView;)Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mIndividualReplacements:Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/ex/chips/MTKRecipientEditTextView;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/ex/chips/MTKRecipientEditTextView;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tokenizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createValidatedEntry(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/ex/chips/MTKRecipientEditTextView;Z)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/android/ex/chips/MTKRecipientEditTextView;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/ex/chips/MTKRecipientEditTextView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->recoverLayout()V

    return-void
.end method

.method static synthetic access$3200(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/android/ex/chips/MTKRecipientEditTextView;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tempLogPrint(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/ex/chips/MTKRecipientEditTextView;)I
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPreviousWidth:I

    return v0
.end method

.method static synthetic access$3500(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->rotateToPortrait()V

    return-void
.end method

.method static synthetic access$3600(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->rotateToLandscape()V

    return-void
.end method

.method static synthetic access$3700(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->unRegisterGlobalLayoutListener()V

    return-void
.end method

.method static synthetic access$3800(Lcom/android/ex/chips/MTKRecipientEditTextView;II)Z
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->alreadyHasChip(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/ex/chips/MTKRecipientEditTextView;)Lcom/android/ex/chips/RecipientChip;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)Lcom/android/ex/chips/RecipientChip;
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/ex/chips/MTKRecipientEditTextView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->DISMISS:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0
    .param p0    # Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->scrollBottomIntoView()V

    return-void
.end method

.method private alreadyHasChip(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v2

    const-class v3, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v2, p1, p2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_2

    array-length v2, v0

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private appendPendingStrings()V
    .locals 6

    const/4 v4, 0x0

    const-string v3, "RecipientEditTextView"

    const-string v5, "[Debug] appendPendingStrings-start"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-gtz v0, :cond_1

    const-string v3, "RecipientEditTextView"

    const-string v4, "[Debug] appendPendingStrings-end (null)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    const-string v1, ""

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v1, v4, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->append(Ljava/lang/CharSequence;II)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iput-boolean v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    const-string v3, "RecipientEditTextView"

    const-string v4, "[Debug] appendPendingStrings-end"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private calculateAvailableWidth(Z)F
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    return v0
.end method

.method private calculateNumChipsCanShow()I
    .locals 14

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v10

    if-eqz v10, :cond_0

    array-length v13, v10

    if-nez v13, :cond_2

    :cond_0
    move v7, v12

    :cond_1
    :goto_0
    return v7

    :cond_2
    invoke-direct {p0, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateAvailableWidth(Z)F

    move-result v13

    float-to-int v4, v13

    array-length v7, v10

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipInterval()I

    move-result v1

    const/4 v0, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_3

    aget-object v13, v10, v3

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    add-int/2addr v13, v1

    add-int/2addr v8, v13

    if-le v8, v4, :cond_7

    const/4 v0, 0x0

    :cond_3
    if-nez v0, :cond_1

    if-ne v3, v7, :cond_4

    if-nez v0, :cond_4

    add-int/lit8 v3, v3, -0x1

    :cond_4
    invoke-direct {p0, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getMeasuredMoreSpanWidth(I)I

    move-result v6

    sub-int v2, v4, v6

    const/4 v5, 0x0

    move v5, v3

    :goto_2
    if-ltz v5, :cond_5

    aget-object v13, v10, v5

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    sub-int/2addr v13, v1

    sub-int/2addr v8, v13

    if-ge v8, v2, :cond_8

    :cond_5
    if-nez v5, :cond_9

    aget-object v13, v10, v12

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    if-le v13, v2, :cond_6

    aget-object v12, v10, v12

    invoke-direct {p0, v12, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    iput-boolean v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    :cond_6
    move v7, v11

    goto :goto_0

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_8
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    :cond_9
    move v7, v5

    goto :goto_0
.end method

.method private calculateOffsetFromBottom(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    add-int/lit8 v2, p1, 0x1

    sub-int v0, v1, v2

    iget v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    float-to-int v1, v1

    mul-int/2addr v1, v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    neg-int v1, v1

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getDropDownVerticalOffset()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method private checkChipWidths()V
    .locals 9

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    if-eqz v3, :cond_1

    move-object v0, v3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    invoke-virtual {v2}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    if-lez v6, :cond_0

    iget v6, v1, Landroid/graphics/Rect;->right:I

    iget v7, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v7

    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int/2addr v7, v8

    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_0

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v6

    invoke-virtual {p0, v2, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChip(Lcom/android/ex/chips/RecipientChip;Lcom/android/ex/chips/RecipientEntry;)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private chipsPending()Z
    .locals 1

    iget v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearSelectedChip()V
    .locals 1

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->unselectChip(Lcom/android/ex/chips/RecipientChip;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setCursorVisible(Z)V

    return-void
.end method

.method private commitByCharacter()V
    .locals 4

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v0, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldCreateChip(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v2, v1, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method private commitChip(IILandroid/text/Editable;)Z
    .locals 17
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/text/Editable;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v15

    if-lez v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/MultiAutoCompleteTextView;->enoughToFilter()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v15

    move/from16 v0, p2

    if-ne v0, v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v15

    if-nez v15, :cond_0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/ex/chips/MTKRecipientEditTextView;->submitItemAtPosition(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    const/4 v15, 0x1

    :goto_0
    return v15

    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    move-object/from16 v0, p3

    move/from16 v1, p1

    invoke-interface {v15, v0, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v14

    invoke-interface/range {p3 .. p3}, Landroid/text/Editable;->length()I

    move-result v15

    add-int/lit8 v16, v14, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_2

    add-int/lit8 v15, v14, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/text/Editable;->charAt(I)C

    move-result v5

    const/16 v15, 0x2c

    if-eq v5, v15, :cond_1

    const/16 v15, 0x3b

    if-eq v5, v15, :cond_1

    const v15, 0xff0c

    if-eq v5, v15, :cond_1

    const v15, 0xff1b

    if-ne v5, v15, :cond_2

    :cond_1
    add-int/lit8 v14, v14, 0x1

    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v15

    if-eqz v15, :cond_7

    if-eqz v3, :cond_7

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v15

    if-lez v15, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/widget/MultiAutoCompleteTextView;->enoughToFilter()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v15

    move/from16 v0, p2

    if-ne v0, v15, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v15

    invoke-interface {v15}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v4, :cond_5

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v15

    invoke-interface {v15, v11}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v10}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v13, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->submitItemAtPosition(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    const/4 v11, 0x0

    :goto_2
    if-ge v11, v4, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v15

    invoke-interface {v15, v11}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v10}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Lcom/android/ex/chips/RecipientEntry;->getDestinationKind()I

    move-result v15

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    invoke-static {v13}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->submitItemAtPosition(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->clearComposingText()V

    if-eqz v13, :cond_a

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_a

    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createTokenizedEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v10

    if-eqz v10, :cond_8

    const-string v15, ""

    move-object/from16 v0, p3

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-static {v0, v1, v2, v15}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v15}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_8

    const/4 v15, -0x1

    move/from16 v0, p1

    if-le v0, v15, :cond_8

    const/4 v15, -0x1

    move/from16 v0, p2

    if-le v0, v15, :cond_8

    move-object/from16 v0, p3

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-interface {v0, v1, v2, v6}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v15

    move/from16 v0, p2

    if-ne v0, v15, :cond_9

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->sanitizeBetween()V

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_a
    const/4 v15, 0x0

    goto/16 :goto_0
.end method

.method private commitDefault()Z
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v6, v0, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldCreateChip(II)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-direct {p0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v4, v6, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v3

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v4

    if-eq v3, v4, :cond_2

    invoke-direct {p0, v2, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handleEdit(II)V

    move v4, v5

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2, v1, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    move-result v4

    goto :goto_0
.end method

.method private constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;
    .locals 10
    .param p1    # Lcom/android/ex/chips/RecipientEntry;
    .param p2    # I
    .param p3    # Z
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    const/4 v9, 0x0

    iget-object v7, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    if-nez v7, :cond_0

    new-instance v7, Ljava/lang/NullPointerException;

    const-string v8, "Unable to render any chips as setChipDimensions was not called."

    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v1

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eqz p3, :cond_1

    invoke-direct {p0, p1, v3, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createSelectedChip(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object v6

    :goto_0
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v5, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v5, v9, v9, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v4, Lcom/android/ex/chips/RecipientChip;

    invoke-direct {v4, v5, p1, p2}, Lcom/android/ex/chips/RecipientChip;-><init>(Landroid/graphics/drawable/Drawable;Lcom/android/ex/chips/RecipientEntry;I)V

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-object v4

    :cond_1
    invoke-direct {p0, p1, v3, v2, p4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createUnselectedChip(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Landroid/text/Layout;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method

.method private createAlternatesAdapter(Lcom/android/ex/chips/RecipientChip;)Landroid/widget/ListAdapter;
    .locals 10
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/ex/chips/RecipientAlternatesAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getContactId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getDataId()J

    move-result-wide v4

    iget v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v7

    check-cast v7, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual {v7}, Lcom/android/ex/chips/BaseRecipientAdapter;->getQueryType()I

    move-result v7

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v8

    check-cast v8, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual {v8}, Lcom/android/ex/chips/BaseRecipientAdapter;->getShowPhoneAndEmail()Z

    move-result v9

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/android/ex/chips/RecipientAlternatesAdapter;-><init>(Landroid/content/Context;JJIILcom/android/ex/chips/RecipientAlternatesAdapter$OnCheckedItemChangedListener;Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/android/ex/chips/RecipientAlternatesAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getContactId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getDataId()J

    move-result-wide v5

    iget v7, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v8

    check-cast v8, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual {v8}, Lcom/android/ex/chips/BaseRecipientAdapter;->getQueryType()I

    move-result v8

    move-object v9, p0

    invoke-direct/range {v1 .. v9}, Lcom/android/ex/chips/RecipientAlternatesAdapter;-><init>(Landroid/content/Context;JJIILcom/android/ex/chips/RecipientAlternatesAdapter$OnCheckedItemChangedListener;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;
    .locals 10
    .param p1    # Lcom/android/ex/chips/RecipientEntry;
    .param p2    # Z

    const/4 v7, 0x0

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createAddressText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v1, v7

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v4

    iget-object v8, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v8, v9, v4}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v6, v8, -0x1

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-boolean v8, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-nez v8, :cond_0

    const/4 v8, 0x0

    :try_start_0
    invoke-direct {p0, p1, v5, p2, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    const/4 v8, 0x0

    const/16 v9, 0x21

    invoke-virtual {v1, v0, v8, v6, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/android/ex/chips/RecipientChip;->setOriginalText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v8, "RecipientEditTextView"

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v7

    goto :goto_0
.end method

.method private createMoreSpan(I)Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;
    .locals 14
    .param p1    # I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Landroid/text/TextPaint;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v6, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v3

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int v13, v3, v4

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v7

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultTextSize:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v10

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v10, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move v8, v10

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11, v2}, Landroid/text/Layout;->getLineDescent(I)I

    move-result v3

    sub-int/2addr v8, v3

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    int-to-float v5, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    new-instance v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v12, v3, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v12, v2, v2, v13, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v2, Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    invoke-direct {v2, p0, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Landroid/graphics/drawable/Drawable;)V

    return-object v2
.end method

.method private createReplacementChip(IILandroid/text/Editable;)V
    .locals 16
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/text/Editable;

    invoke-direct/range {p0 .. p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->alreadyHasChip(II)Z

    move-result v13

    if-eqz v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x2c

    invoke-virtual {v13, v14}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ne v5, v13, :cond_2

    const/4 v13, 0x0

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createTokenizedEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createAddressText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v14

    invoke-interface {v13, v14, v8}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v10

    :goto_1
    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-nez v13, :cond_4

    const/4 v14, 0x0

    invoke-virtual {v9}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_3

    invoke-virtual {v9}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_7

    :cond_3
    const/4 v13, 0x1

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10, v14, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    const/4 v13, 0x0

    const/16 v14, 0x21

    invoke-virtual {v4, v3, v13, v11, v14}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_3
    move-object/from16 v0, p3

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-interface {v0, v1, v2, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    if-nez v13, :cond_5

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    :cond_5
    invoke-virtual {v4}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/android/ex/chips/RecipientChip;->setOriginalText(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    const/4 v10, 0x0

    goto :goto_1

    :cond_7
    const/4 v13, 0x0

    goto :goto_2

    :catch_0
    move-exception v7

    const-string v13, "RecipientEditTextView"

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method

.method private createSelectedChip(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Landroid/text/Layout;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # Lcom/android/ex/chips/RecipientEntry;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # Landroid/text/Layout;

    iget v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    float-to-int v9, v2

    move v8, v9

    const/4 v2, 0x1

    new-array v12, v2, [F

    const-string v2, " "

    invoke-virtual {p2, v2, v12}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChipDisplayText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateAvailableWidth(Z)F

    move-result v3

    int-to-float v4, v8

    sub-float/2addr v3, v4

    const/4 v4, 0x0

    aget v4, v12, v4

    sub-float/2addr v3, v4

    invoke-direct {p0, v2, p2, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->ellipsizeText(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;

    move-result-object v1

    mul-int/lit8 v2, v8, 0x2

    const/4 v3, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {p2, v1, v3, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    add-int/2addr v3, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v9, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v11, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget v2, Lcom/android/ex/chips/MTKRecipientEditTextView;->sSelectedTextColor:I

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    int-to-float v4, v4

    move-object v5, v1

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5, p2, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getTextYOffset(Ljava/lang/String;Landroid/text/TextPaint;I)F

    move-result v5

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    sub-int v3, v11, v8

    iget v4, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iget v4, v7, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, 0x0

    iget v5, v7, Landroid/graphics/Rect;->right:I

    sub-int v5, v11, v5

    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    sub-int v6, v9, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_0
    return-object v10

    :cond_0
    const-string v2, "RecipientEditTextView"

    const-string v3, "Unable to draw a background for the chips as it was never set"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createSingleAddressAdapter(Lcom/android/ex/chips/RecipientChip;)Landroid/widget/ListAdapter;
    .locals 4
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    new-instance v0, Lcom/android/ex/chips/SingleRecipientArrayAdapter;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/ex/chips/SingleRecipientArrayAdapter;-><init>(Landroid/content/Context;ILcom/android/ex/chips/RecipientEntry;)V

    return-object v0
.end method

.method private createTokenizedEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v5, ","

    invoke-virtual {p1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_1
    invoke-static {p1}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v5

    goto :goto_0

    :cond_2
    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isValid(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v3, :cond_7

    array-length v5, v3

    if-lez v5, :cond_7

    aget-object v5, v3, v6

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v5, 0x2c

    if-eq v0, v5, :cond_4

    const/16 v5, 0x3b

    if-eq v0, v5, :cond_4

    const v5, 0xff0c

    if-eq v0, v5, :cond_4

    const v5, 0xff1b

    if-ne v0, v5, :cond_5

    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_5
    invoke-static {v1, p1}, Lcom/android/ex/chips/RecipientEntry;->constructGeneratedEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v5

    goto :goto_0

    :cond_6
    aget-object v5, v3, v6

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v5

    goto/16 :goto_0

    :cond_7
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v5, p1}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v5, p1}, Landroid/widget/AutoCompleteTextView$Validator;->fixText(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {v4}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    array-length v5, v2

    if-lez v5, :cond_8

    aget-object v5, v2, v6

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v4

    :cond_8
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    :goto_2
    invoke-static {v4}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v5

    goto/16 :goto_0

    :cond_9
    const/4 v4, 0x0

    goto :goto_1

    :cond_a
    move-object v4, p1

    goto :goto_2
.end method

.method private createUnselectedChip(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Landroid/text/Layout;Z)Landroid/graphics/Bitmap;
    .locals 28
    .param p1    # Lcom/android/ex/chips/RecipientEntry;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # Landroid/text/Layout;
    .param p4    # Z

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    float-to-int v0, v5

    move/from16 v18, v0

    move/from16 v19, v18

    const/4 v5, 0x1

    new-array v0, v5, [F

    move-object/from16 v26, v0

    const-string v5, " "

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    invoke-virtual/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChipDisplayText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLimitedWidthForSpan:I

    const/4 v7, -0x1

    if-ne v5, v7, :cond_3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateAvailableWidth(Z)F

    move-result v5

    move/from16 v0, v19

    int-to-float v7, v0

    sub-float/2addr v5, v7

    const/4 v7, 0x0

    aget v7, v26, v7

    sub-float/2addr v5, v7

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v6, v1, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->ellipsizeText(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;

    move-result-object v4

    const/16 v17, 0x0

    const/4 v5, 0x0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v0, v5

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    mul-int/lit8 v5, v5, 0x2

    add-int v25, v16, v5

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v25

    move/from16 v1, v18

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v24

    invoke-virtual/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipBackground(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v10, :cond_b

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v5

    if-eqz v5, :cond_5

    const-wide/16 v5, -0x1

    cmp-long v5, v12, v5

    if-eqz v5, :cond_4

    const/4 v14, 0x1

    :goto_1
    if-eqz v14, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getPhotoBytes()[B

    move-result-object v22

    if-nez v22, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getPhotoThumbnailUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    check-cast v5, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getPhotoThumbnailUri()Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Lcom/android/ex/chips/BaseRecipientAdapter;->fetchPhoto(Lcom/android/ex/chips/RecipientEntry;Landroid/net/Uri;)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getPhotoBytes()[B

    move-result-object v22

    :cond_0
    if-eqz v22, :cond_7

    const/4 v5, 0x0

    move-object/from16 v0, v22

    array-length v6, v0

    move-object/from16 v0, v22

    invoke-static {v0, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v21

    :goto_2
    if-eqz v21, :cond_1

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    mul-int/lit8 v5, v5, 0x2

    add-int v5, v5, v16

    add-int v25, v5, v19

    new-instance v23, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    move-object/from16 v0, v23

    invoke-direct {v0, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v11}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    new-instance v15, Landroid/graphics/RectF;

    sub-int v5, v25, v19

    iget v6, v11, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, v11, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    iget v7, v11, Landroid/graphics/Rect;->right:I

    sub-int v7, v25, v7

    int-to-float v7, v7

    iget v8, v11, Landroid/graphics/Rect;->bottom:I

    sub-int v8, v18, v8

    int-to-float v8, v8

    invoke-direct {v15, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v20, Landroid/graphics/Matrix;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Matrix;-><init>()V

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v15, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v25

    move/from16 v1, v18

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v24

    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, v24

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    invoke-virtual {v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_1
    :goto_3
    if-nez v3, :cond_2

    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, v24

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    :cond_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v25

    move/from16 v1, v18

    invoke-virtual {v10, v5, v6, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v10, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x106000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz v17, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    move/from16 v27, v0

    :goto_4
    const/4 v5, 0x0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    move/from16 v0, v27

    int-to-float v7, v0

    move-object v8, v4

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-direct {v0, v8, v1, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getTextYOffset(Ljava/lang/String;Landroid/text/TextPaint;I)F

    move-result v8

    move-object/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    :goto_5
    return-object v24

    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLimitedWidthForSpan:I

    sub-int v5, v5, v19

    int-to-float v5, v5

    const/4 v7, 0x0

    aget v7, v26, v7

    sub-float/2addr v5, v7

    goto/16 :goto_0

    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_5
    const-wide/16 v5, -0x1

    cmp-long v5, v12, v5

    if-eqz v5, :cond_6

    const-wide/16 v5, -0x2

    cmp-long v5, v12, v5

    if-eqz v5, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v14, 0x1

    goto/16 :goto_1

    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultContactPhoto:Landroid/graphics/Bitmap;

    move-object/from16 v21, v0

    goto/16 :goto_2

    :cond_8
    if-eqz p4, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    mul-int/lit8 v6, v6, 0x2

    sub-int v6, v25, v6

    sub-int v6, v6, v16

    div-int/lit8 v6, v6, 0x2

    add-int v27, v5, v6

    goto :goto_4

    :cond_b
    const-string v5, "RecipientEditTextView"

    const-string v6, "Unable to draw a background for the chips as it was never set"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private createValidatedEntry(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;
    .locals 6
    .param p1    # Lcom/android/ex/chips/RecipientEntry;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v2

    const-wide/16 v4, -0x2

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/ex/chips/RecipientEntry;->constructGeneratedEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/android/ex/chips/RecipientEntry;->isCreatedRecipient(J)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v2, v0}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-static {v0}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method private ellipsizeText(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # F

    iget v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipFontSize:F

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_0

    const-string v0, "RecipientEditTextView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RecipientEditTextView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max width is negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, p2, p3, v0}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private expand()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mShouldShrink:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7fffffff

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    if-eqz v2, :cond_1

    aget-object v2, v0, v3

    const/4 v4, -0x1

    invoke-direct {p0, v2, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeMoreChip()V

    invoke-virtual {p0, v5}, Landroid/widget/TextView;->setCursorVisible(Z)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_4

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    :goto_0
    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    new-instance v2, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    invoke-direct {v2, p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/MTKRecipientEditTextView$1;)V

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    :cond_2
    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-eqz v2, :cond_3

    iput-boolean v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mJustExpanded:Z

    :cond_3
    return-void

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method private filterInvalidCharacter(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0xff0c

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const v1, 0xff1b

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^( *,)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "( *,)+"

    const-string v2, ","

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(, *)+"

    const-string v2, ", "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^( *;)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "( *;)+"

    const-string v2, ";"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(; *)+"

    const-string v2, "; "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^\\s+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private findChip(I)Lcom/android/ex/chips/RecipientChip;
    .locals 9
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    const-class v8, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v5, v6, v7, v8}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/android/ex/chips/RecipientChip;

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_1

    aget-object v0, v1, v3

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v2

    if-lt p1, v4, :cond_0

    if-gt p1, v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private findText(Landroid/text/Editable;I)I
    .locals 2
    .param p1    # Landroid/text/Editable;
    .param p2    # I

    invoke-interface {p1, p2}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    :goto_0
    return p2

    :cond_0
    const/4 p2, -0x1

    goto :goto_0
.end method

.method private focusNext()Z
    .locals 2

    const/16 v1, 0x82

    invoke-virtual {p0, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getChipEnd(Lcom/android/ex/chips/RecipientChip;)I
    .locals 1
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private getChipInterval()I
    .locals 3

    const/4 v2, 0x1

    new-array v1, v2, [F

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    const/4 v2, 0x0

    aget v2, v1, v2

    float-to-int v2, v2

    return v2
.end method

.method private getChipStart(Lcom/android/ex/chips/RecipientChip;)I
    .locals 1
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private getChipWidth(Lcom/android/ex/chips/RecipientChip;)I
    .locals 1
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p1}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method private getExcessTopPadding()I
    .locals 2

    sget v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->sExcessTopPadding:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    float-to-int v0, v0

    sput v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->sExcessTopPadding:I

    :cond_0
    sget v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->sExcessTopPadding:I

    return v0
.end method

.method private getMeasuredMoreSpanWidth(I)I
    .locals 6
    .param p1    # I

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/TextPaint;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    return v2
.end method

.method private getOffsetFromBottom(I)I
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Landroid/text/Layout;->getLineTop(I)I

    move-result v3

    add-int v1, v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v3

    sub-int v3, v1, v3

    sub-int/2addr v2, v3

    neg-int v0, v2

    goto :goto_0
.end method

.method private getTextYOffset(Ljava/lang/String;Landroid/text/TextPaint;I)F
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p2, p1, v2, v3, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/graphics/Paint;->descent()F

    move-result v3

    float-to-int v3, v3

    sub-int v1, v2, v3

    sub-int v2, p3, v1

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p3, v2

    int-to-float v2, v2

    return v2
.end method

.method private handleEdit(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, -0x1

    if-eq p1, v6, :cond_0

    if-ne p2, v6, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0, p2}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v2

    const-string v5, ""

    invoke-static {v1, p1, p2, v5}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v5, 0x0

    invoke-direct {p0, v2, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v3

    if-eqz v0, :cond_2

    if-le p1, v6, :cond_2

    if-le v3, v6, :cond_2

    invoke-interface {v1, p1, v3, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_2
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method

.method private handlePasteAndReplace()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handlePaste()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/MTKRecipientEditTextView$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method private handlePasteClip(Landroid/content/ClipData;)V
    .locals 12
    .param p1    # Landroid/content/ClipData;

    const/16 v11, 0x20

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v10}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v10

    add-int/lit8 v3, v10, 0x1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v6

    if-eq v6, v3, :cond_1

    if-nez v6, :cond_3

    :cond_1
    :goto_0
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v10

    const-string v11, "text/plain"

    invoke-virtual {v10, v11}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v10

    if-ge v2, v10, :cond_7

    invoke-virtual {p1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-direct {p0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->filterInvalidCharacter(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v7

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ltz v7, :cond_6

    if-ltz v1, :cond_6

    if-eq v7, v1, :cond_6

    invoke-interface {v0, v7, v1, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v10

    invoke-virtual {p0, v10}, Landroid/widget/EditText;->setSelection(I)V

    :goto_2
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handlePasteAndReplace()V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v9, v6

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_3
    if-le v9, v3, :cond_4

    add-int/lit8 v10, v9, -0x1

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v11, :cond_4

    add-int/lit8 v9, v9, -0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v10, v9, -0x1

    if-le v10, v3, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v9, v10, :cond_5

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v11, :cond_5

    add-int/lit8 v9, v9, 0x1

    :cond_5
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10, v9, v6}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    :cond_6
    invoke-interface {v0, v1, v5}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_2

    :cond_7
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private isEndChip()Z
    .locals 6

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-lez v2, :cond_0

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v4, 0x2c

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3b

    if-eq v0, v4, :cond_0

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_0
    sub-int v4, v1, v2

    const/4 v5, 0x2

    if-gt v4, v5, :cond_1

    if-eqz v2, :cond_1

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isInDelete(Lcom/android/ex/chips/RecipientChip;IFF)Z
    .locals 1
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # I
    .param p3    # F
    .param p4    # F

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isPhoneNumber(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    sget-object v1, Landroid/util/Patterns;->PHONE_PATTERN_MTK:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0
.end method

.method private isValid(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v0, p1}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method private postHandlePendingChips()V
    .locals 2

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandlePendingChips:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandlePendingChips:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private putOffsetInRange(I)I
    .locals 9
    .param p1    # I

    move v3, p1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v2

    move v5, v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {v6, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v7

    const/16 v8, 0x20

    if-ne v7, v8, :cond_0

    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    if-lt v3, v5, :cond_1

    move v4, v3

    :goto_1
    return v4

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    :goto_2
    if-ltz v3, :cond_2

    invoke-direct {p0, v0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findText(Landroid/text/Editable;I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_2

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v7

    if-nez v7, :cond_2

    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_2
    move v4, v3

    goto :goto_1
.end method

.method private recoverLayout()V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    move v2, v6

    :goto_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v1

    new-instance v3, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v3}, Landroid/text/BoringLayout$Metrics;-><init>()V

    new-instance v4, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v4}, Landroid/text/BoringLayout$Metrics;-><init>()V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v5

    sub-int v5, v0, v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v2

    goto :goto_0
.end method

.method private registerGlobalLayoutListener()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$9;

    invoke-direct {v1, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$9;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    return-void
.end method

.method private replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V
    .locals 9
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # I

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v3

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v1

    iput p2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLimitedWidthForSpan:I

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v7

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x1

    :goto_0
    invoke-direct {p0, v7, v3, v6, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    const/4 v5, -0x1

    iput v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLimitedWidthForSpan:I

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    const-string v5, ""

    invoke-static {v4, v3, v1, v5}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/16 v5, 0x21

    invoke-interface {v4, v0, v3, v1, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    return-void

    :cond_1
    move v5, v6

    goto :goto_0
.end method

.method private rotateToLandscape()V
    .locals 29

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v22, v0

    if-eqz v23, :cond_0

    if-nez v22, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v26, 0x0

    aget-object v26, v23, v26

    const/16 v27, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v26, v0

    if-eqz v26, :cond_0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateAvailableWidth(Z)F

    move-result v26

    move/from16 v0, v26

    float-to-int v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->width()I

    move-result v21

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipInterval()I

    move-result v7

    move v4, v14

    move/from16 v17, v4

    const/16 v26, 0x1

    move/from16 v0, v22

    move/from16 v1, v26

    if-ne v0, v1, :cond_2

    add-int v26, v21, v7

    sub-int v4, v4, v26

    const/16 v26, 0x0

    aget-object v26, v23, v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v26

    sub-int v26, v4, v26

    if-gez v26, :cond_2

    const/16 v26, 0x0

    aget-object v26, v23, v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    const/4 v10, 0x0

    :goto_1
    move/from16 v0, v22

    if-ge v10, v0, :cond_3

    const/16 v26, 0x0

    aget-object v26, v23, v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v26

    add-int v26, v26, v7

    sub-int v17, v17, v26

    if-gtz v17, :cond_5

    :cond_3
    const/4 v13, 0x0

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    if-ge v13, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/ex/chips/RecipientChip;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v26

    add-int v26, v26, v7

    sub-int v17, v17, v26

    if-gtz v17, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    if-ne v13, v0, :cond_8

    if-ltz v17, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->expand()V

    goto/16 :goto_0

    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_7
    add-int/lit8 v13, v13, -0x1

    :cond_8
    sub-int v17, v17, v21

    const/16 v16, 0x0

    move/from16 v16, v13

    :goto_3
    if-ltz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/ex/chips/RecipientChip;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v26

    add-int v26, v26, v7

    add-int v17, v17, v26

    if-ltz v17, :cond_b

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v26, v22, -0x1

    aget-object v26, v23, v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v12

    const/4 v8, 0x0

    move v6, v12

    const/4 v5, 0x0

    const/4 v15, 0x0

    :goto_4
    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientChip;->getOriginalText()Ljava/lang/CharSequence;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v26

    add-int v6, v8, v26

    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v8, v0, :cond_a

    const/16 v26, 0x21

    move/from16 v0, v26

    invoke-interface {v11, v5, v8, v6, v0}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    :cond_b
    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreSpan(I)Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    move-result-object v18

    new-instance v9, Landroid/text/SpannableString;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v26

    add-int/lit8 v27, v6, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v19

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v26, 0x0

    invoke-virtual {v9}, Landroid/text/SpannableString;->length()I

    move-result v27

    const/16 v28, 0x21

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v26

    add-int/lit8 v27, v6, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v19

    invoke-interface {v0, v1, v2, v9}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    goto/16 :goto_0
.end method

.method private rotateToPortrait()V
    .locals 25

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v15, v0

    if-eqz v17, :cond_0

    if-nez v15, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v22, v0

    if-nez v22, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChip()V

    goto :goto_0

    :cond_2
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateAvailableWidth(Z)F

    move-result v22

    move/from16 v0, v22

    float-to-int v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v14

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipInterval()I

    move-result v4

    sub-int v22, v8, v14

    sub-int v3, v22, v4

    move v11, v3

    const/4 v6, 0x0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v15, :cond_3

    aget-object v22, v17, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipWidth(Lcom/android/ex/chips/RecipientChip;)I

    move-result v22

    add-int v22, v22, v4

    sub-int v11, v11, v22

    if-gtz v11, :cond_5

    :cond_3
    if-ne v6, v15, :cond_4

    if-gez v11, :cond_0

    add-int/lit8 v6, v6, -0x1

    :cond_4
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v15, v0, :cond_6

    if-nez v6, :cond_0

    if-gez v11, :cond_0

    const/16 v22, 0x0

    aget-object v22, v17, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    goto :goto_0

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_6
    if-nez v6, :cond_7

    add-int/lit8 v6, v6, 0x1

    if-gez v11, :cond_7

    const/16 v22, 0x0

    aget-object v22, v17, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->replaceChipOnSameTextRange(Lcom/android/ex/chips/RecipientChip;I)V

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHasEllipsizedFirstChip:Z

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v21

    aget-object v22, v17, v6

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v13

    const/4 v9, 0x0

    move v7, v6

    move v10, v9

    :goto_2
    if-ge v7, v15, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    add-int/lit8 v9, v10, 0x1

    aget-object v23, v17, v7

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    if-eqz v22, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    aget-object v23, v17, v7

    invoke-virtual/range {v22 .. v23}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_9

    :cond_8
    aget-object v22, v17, v7

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v19

    aget-object v22, v17, v7

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v18

    aget-object v22, v17, v7

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/android/ex/chips/RecipientChip;->setOriginalText(Ljava/lang/String;)V

    :cond_9
    aget-object v22, v17, v7

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v7, v7, 0x1

    move v10, v9

    goto :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreSpan(I)Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    move-result-object v12

    new-instance v5, Landroid/text/SpannableString;

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-interface {v0, v1, v13}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v23

    const/16 v24, 0x21

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v5, v12, v0, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-interface {v0, v1, v13, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    goto/16 :goto_0
.end method

.method private scrollBottomIntoView()V
    .locals 7

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mShouldShrink:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    new-array v3, v4, [I

    invoke-virtual {p0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    const/4 v4, 0x1

    aget v4, v3, v4

    add-int v0, v4, v2

    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    float-to-int v4, v4

    iget v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mActionBarHeight:I

    add-int/2addr v4, v5

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getExcessTopPadding()I

    move-result v5

    add-int v1, v4, v5

    if-le v0, v1, :cond_0

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    const/4 v5, 0x0

    sub-int v6, v0, v1

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->scrollBy(II)V

    :cond_0
    return-void
.end method

.method private scrollLineIntoView(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateOffsetFromBottom(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    :cond_0
    return-void
.end method

.method private selectChip(Lcom/android/ex/chips/RecipientChip;)Lcom/android/ex/chips/RecipientChip;
    .locals 10
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getValue()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeChip(Lcom/android/ex/chips/RecipientChip;)V

    invoke-interface {v1, v5}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Landroid/widget/TextView;->setCursorVisible(Z)V

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-virtual {p0, v6}, Landroid/widget/EditText;->setSelection(I)V

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    new-instance v3, Lcom/android/ex/chips/RecipientChip;

    const/4 v6, 0x0

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lcom/android/ex/chips/RecipientEntry;->constructFakeEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v7

    const/4 v8, -0x1

    invoke-direct {v3, v6, v7, v8}, Lcom/android/ex/chips/RecipientChip;-><init>(Landroid/graphics/drawable/Drawable;Lcom/android/ex/chips/RecipientEntry;I)V

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getContactId()J

    move-result-wide v6

    const-wide/16 v8, -0x2

    cmp-long v6, v6, v8

    if-nez v6, :cond_5

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v6

    invoke-interface {v6, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    :try_start_0
    iget-boolean v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-eqz v6, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct {p0, v6, v4, v7, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    const-string v6, ""

    invoke-static {v1, v4, v2, v6}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v6, -0x1

    if-eq v4, v6, :cond_2

    const/4 v6, -0x1

    if-ne v2, v6, :cond_4

    :cond_2
    const-string v6, "RecipientEditTextView"

    const-string v7, "The chip being selected no longer exists but should."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/android/ex/chips/RecipientChip;->setSelected(Z)V

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->scrollLineIntoView(I)V

    :cond_3
    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {p0, v3, v6, v7, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->showAddress(Lcom/android/ex/chips/RecipientChip;Landroid/widget/ListPopupWindow;ILandroid/content/Context;)V

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Landroid/widget/TextView;->setCursorVisible(Z)V

    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "RecipientEditTextView"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    const/16 v6, 0x21

    invoke-interface {v1, v3, v4, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v6

    invoke-interface {v6, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    :try_start_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct {p0, v6, v4, v7, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    const-string v6, ""

    invoke-static {v1, v4, v2, v6}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v6, -0x1

    if-eq v4, v6, :cond_6

    const/4 v6, -0x1

    if-ne v2, v6, :cond_8

    :cond_6
    const-string v6, "RecipientEditTextView"

    const-string v7, "The chip being selected no longer exists but should."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/android/ex/chips/RecipientChip;->setSelected(Z)V

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->scrollLineIntoView(I)V

    :cond_7
    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {p0, v3, v6, v7, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->showAlternates(Lcom/android/ex/chips/RecipientChip;Landroid/widget/ListPopupWindow;ILandroid/content/Context;)V

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Landroid/widget/TextView;->setCursorVisible(Z)V

    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v6, "RecipientEditTextView"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_8
    const/16 v6, 0x21

    invoke-interface {v1, v3, v4, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method

.method private setChipDimensions(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v9, 0x1

    const/high16 v8, -0x40800000

    const/4 v7, 0x0

    const/4 v6, -0x1

    sget-object v3, Lcom/android/ex/chips/R$styleable;->RecipientEditTextView:[I

    invoke-virtual {p1, p2, v3, v7, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_0

    sget v3, Lcom/android/ex/chips/R$drawable;->chip_background:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_1

    sget v3, Lcom/android/ex/chips/R$drawable;->chip_background_selected:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackgroundPressed:Landroid/graphics/drawable/Drawable;

    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_2

    sget v3, Lcom/android/ex/chips/R$drawable;->chip_delete:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipDelete:Landroid/graphics/drawable/Drawable;

    :cond_2
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    if-ne v3, v6, :cond_3

    sget v3, Lcom/android/ex/chips/R$dimen;->chip_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipPadding:I

    :cond_3
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    if-ne v3, v6, :cond_4

    sget v3, Lcom/android/ex/chips/R$layout;->chips_alternate_item:I

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesLayout:I

    :cond_4
    sget v3, Lcom/android/ex/chips/R$drawable;->ic_contact_picture:I

    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultContactPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/android/ex/chips/R$layout;->more_item:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    cmpl-float v3, v3, v8

    if-nez v3, :cond_5

    sget v3, Lcom/android/ex/chips/R$dimen;->chip_height:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    :cond_5
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipFontSize:F

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipFontSize:F

    cmpl-float v3, v3, v8

    if-nez v3, :cond_6

    sget v3, Lcom/android/ex/chips/R$dimen;->chip_text_size:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipFontSize:F

    :cond_6
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mInvalidChipBackground:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mInvalidChipBackground:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_7

    sget v3, Lcom/android/ex/chips/R$drawable;->chip_background_invalid:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mInvalidChipBackground:Landroid/graphics/drawable/Drawable;

    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/ex/chips/R$dimen;->line_spacing_extra:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mLineSpacingExtra:F

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x10102eb

    invoke-virtual {v3, v4, v2, v9}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_8

    iget v3, v2, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mActionBarHeight:I

    :cond_8
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private setDisableBringPointIntoView(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDisableBringPointIntoView:Z

    return-void
.end method

.method private shouldCreateChip(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/MultiAutoCompleteTextView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->alreadyHasChip(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z
    .locals 4
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getContactId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v2

    if-nez v2, :cond_1

    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private showAddress(Lcom/android/ex/chips/RecipientChip;Landroid/widget/ListPopupWindow;ILandroid/content/Context;)V
    .locals 6
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # Landroid/widget/ListPopupWindow;
    .param p3    # I
    .param p4    # Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getOffsetFromBottom(I)I

    move-result v0

    invoke-virtual {p2, p3}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    invoke-virtual {p2, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultVerticalOffset:I

    add-int/2addr v3, v0

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createSingleAddressAdapter(Lcom/android/ex/chips/RecipientChip;)Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v3, Lcom/android/ex/chips/MTKRecipientEditTextView$8;

    invoke-direct {v3, p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView$8;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->show()V

    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    return-void
.end method

.method private showAlternates(Lcom/android/ex/chips/RecipientChip;Landroid/widget/ListPopupWindow;ILandroid/content/Context;)V
    .locals 7
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # Landroid/widget/ListPopupWindow;
    .param p3    # I
    .param p4    # Landroid/content/Context;

    const/4 v6, 0x1

    const/4 v5, -0x1

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getOffsetFromBottom(I)I

    move-result v0

    invoke-virtual {p2, p3}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    invoke-virtual {p2, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDefaultVerticalOffset:I

    add-int/2addr v3, v0

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createAlternatesAdapter(Lcom/android/ex/chips/RecipientChip;)Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p2, v3}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCheckedItem:I

    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->show()V

    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCheckedItem:I

    if-eq v3, v5, :cond_0

    iget v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCheckedItem:I

    invoke-virtual {v2, v3, v6}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    iput v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCheckedItem:I

    :cond_0
    return-void
.end method

.method private showCopyDialog(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x1

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v3, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    sget v4, Lcom/android/ex/chips/R$layout;->copy_chip_dialog_layout:I

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setContentView(I)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    const v4, 0x1020019

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v0, Lcom/android/ex/chips/R$string;->copy_number:I

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v3, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    return-void

    :cond_0
    sget v0, Lcom/android/ex/chips/R$string;->copy_email:I

    goto :goto_0
.end method

.method private shrink()V
    .locals 13

    const-wide/16 v10, -0x1

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v12, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-eqz v12, :cond_1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChip()V

    goto :goto_0

    :cond_1
    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v12, :cond_4

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {v12}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v1

    :goto_1
    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v12, :cond_5

    cmp-long v10, v1, v10

    if-eqz v10, :cond_5

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v10

    if-nez v10, :cond_2

    const-wide/16 v10, -0x2

    cmp-long v10, v1, v10

    if-nez v10, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v10

    if-eqz v10, :cond_5

    :cond_3
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :goto_2
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChip()V

    goto :goto_0

    :cond_4
    move-wide v1, v10

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v10

    if-gtz v10, :cond_6

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDelayedShrink:Ljava/lang/Runnable;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDelayedShrink:Ljava/lang/Runnable;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_6
    iget v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v10, :cond_8

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->postHandlePendingChips()V

    :cond_7
    :goto_3
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->textIsAllBlank(Landroid/text/Editable;)Z

    move-result v8

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v4

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v10, v3, v4}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v6

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v10

    const-class v11, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v10, v6, v4, v11}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_9

    array-length v10, v0

    if-nez v10, :cond_7

    :cond_9
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v10, v7, v6}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v9

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v10

    if-ge v9, v10, :cond_a

    invoke-interface {v7, v9}, Landroid/text/Editable;->charAt(I)C

    move-result v10

    const/16 v11, 0x2c

    if-ne v10, v11, :cond_a

    add-int/lit8 v9, v9, 0x1

    :cond_a
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v5

    if-eq v9, v5, :cond_b

    if-nez v8, :cond_b

    invoke-direct {p0, v6, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handleEdit(II)V

    goto :goto_3

    :cond_b
    invoke-direct {p0, v6, v4, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    goto :goto_3
.end method

.method private startDrag(Lcom/android/ex/chips/RecipientChip;)V
    .locals 5
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    new-instance v2, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipShadow;

    invoke-direct {v2, p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipShadow;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeChip(Lcom/android/ex/chips/RecipientChip;)V

    return-void
.end method

.method private submitItemAtPosition(I)V
    .locals 7
    .param p1    # I

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createValidatedEntry(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->clearComposingText()V

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v2

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v5, v6, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    const-string v5, ""

    invoke-static {v1, v4, v2, v5}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v5, 0x0

    invoke-direct {p0, v3, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ltz v4, :cond_1

    if-ltz v2, :cond_1

    invoke-interface {v1, v4, v2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->sanitizeBetween()V

    goto :goto_0
.end method

.method private tempLogPrint(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, p2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, p2}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, p2}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    const-string v5, "RecipientEditTextView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Debug] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ---> spanStart="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", spanEnd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", spanFlag="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", spanID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", spanName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private textIsAllBlank(Landroid/text/Editable;)Z
    .locals 4
    .param p1    # Landroid/text/Editable;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private tokenizeAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private tokenizeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private unRegisterGlobalLayoutListener()V
    .locals 2

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    :cond_0
    return-void
.end method

.method private unselectChip(Lcom/android/ex/chips/RecipientChip;)V
    .locals 7
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    const/4 v5, -0x1

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v3

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eq v3, v5, :cond_0

    if-ne v2, v5, :cond_4

    :cond_0
    const-string v4, "RecipientEditTextView"

    const-string v5, "The chip doesn\'t exist or may be a chip a user was editing"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/widget/EditText;->setSelection(I)V

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitDefault()Z

    :cond_1
    :goto_0
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Landroid/widget/TextView;->setCursorVisible(Z)V

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_2
    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddressPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v4

    invoke-interface {v4, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    const-string v4, ""

    invoke-static {v1, v3, v2, v4}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    invoke-interface {v1, p1}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    :try_start_0
    iget-boolean v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-nez v4, :cond_1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {p0, v4, v3, v5, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->constructChipSpan(Lcom/android/ex/chips/RecipientEntry;IZZ)Lcom/android/ex/chips/RecipientChip;

    move-result-object v4

    const/16 v5, 0x21

    invoke-interface {v1, v4, v3, v2, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "RecipientEditTextView"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public append(Ljava/lang/CharSequence;II)V
    .locals 8
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I

    const/16 v7, 0x2c

    iget-boolean v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    if-eqz v4, :cond_0

    const-string v4, "RecipientEditTextView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[append] (mDuringAppendStrings) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;II)V

    :goto_0
    return-void

    :cond_0
    const-string v4, "RecipientEditTextView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[append] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingStrings:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-le v3, v4, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x22

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-le v1, v3, :cond_2

    invoke-virtual {v2, v7, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v3

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v4

    if-lez v4, :cond_3

    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v4, :cond_4

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->postHandlePendingChips()V

    :cond_4
    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public appendList(Lcom/android/ex/chips/MTKRecipientList;)V
    .locals 13
    .param p1    # Lcom/android/ex/chips/MTKRecipientList;

    const/16 v12, 0x2c

    const/4 v11, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/ex/chips/MTKRecipientList;->getRecipientCount()I

    move-result v9

    if-gtz v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/MTKRecipientList;->getRecipientCount()I

    move-result v4

    const-string v6, ""

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v4, :cond_2

    invoke-virtual {p1, v8}, Lcom/android/ex/chips/MTKRecipientList;->getRecipient(I)Lcom/android/ex/chips/MTKRecipient;

    move-result-object v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipient;->getFormatString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_3
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {p0, v6, v11, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->append(Ljava/lang/CharSequence;II)V

    iput-boolean v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAppendStrings:Z

    const-string v9, "RecipientEditTextView"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v4, :cond_4

    invoke-virtual {p1, v8}, Lcom/android/ex/chips/MTKRecipientList;->getRecipient(I)Lcom/android/ex/chips/MTKRecipient;

    move-result-object v3

    const-string v9, "RecipientEditTextView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[appendList] Recipient -> Name = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipient;->getDisplayName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " & Dest = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipient;->getDestination()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    :goto_3
    if-ge v8, v4, :cond_7

    invoke-virtual {p1, v8}, Lcom/android/ex/chips/MTKRecipientList;->getRecipient(I)Lcom/android/ex/chips/MTKRecipient;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipient;->getFormatString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    invoke-static {v7}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v9

    if-lez v9, :cond_6

    const-string v9, "RecipientEditTextView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[appendList] perText= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    const/4 v9, -0x1

    if-le v5, v9, :cond_5

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v9, 0x22

    invoke-virtual {v2, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-le v1, v5, :cond_5

    invoke-virtual {v2, v12, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v5

    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v9

    if-lez v9, :cond_6

    iget v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_7
    iget v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v9, :cond_8

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->postHandlePendingChips()V

    :cond_8
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public bringPointIntoView(I)Z
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDisableBringPointIntoView:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->bringPointIntoView(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructPressedChip()V
    .locals 10

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v8

    if-lez v8, :cond_3

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v8

    :goto_0
    invoke-virtual {p0, v8}, Landroid/widget/EditText;->setSelection(I)V

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->textIsAllBlank(Landroid/text/Editable;)Z

    move-result v6

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v2

    iget-object v8, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v8, v1, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v8

    const-class v9, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v8, v4, v2, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_0

    array-length v8, v0

    if-nez v8, :cond_2

    :cond_0
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    iget-object v8, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v8, v5, v4}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v7

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v8

    if-ge v7, v8, :cond_1

    invoke-interface {v5, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v8

    const/16 v9, 0x2c

    if-ne v8, v9, :cond_1

    add-int/lit8 v7, v7, 0x1

    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v3

    if-eq v7, v3, :cond_4

    if-nez v6, :cond_4

    invoke-direct {p0, v4, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handleEdit(II)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v8, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0, v4, v2, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    goto :goto_1
.end method

.method countTokens(Landroid/text/Editable;)I
    .locals 3
    .param p1    # Landroid/text/Editable;

    const/4 v1, 0x0

    const/4 v0, 0x0

    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, p1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->movePastTerminators(I)I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    :cond_1
    return v1
.end method

.method createAddressText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;
    .locals 7
    .param p1    # Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v1, 0x0

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    :goto_0
    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-eqz v6, :cond_4

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_4

    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v6, v5}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    :goto_1
    return-object v6

    :cond_2
    if-eqz v0, :cond_3

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v4

    if-eqz v4, :cond_3

    array-length v6, v4

    if-lez v6, :cond_3

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v6}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_3
    new-instance v3, Landroid/text/util/Rfc822Token;

    const/4 v6, 0x0

    invoke-direct {v3, v1, v0, v6}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_4
    move-object v6, v5

    goto :goto_1
.end method

.method createChipDisplayText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v1, 0x0

    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    if-eqz v2, :cond_3

    array-length v3, v2

    if-lez v3, :cond_3

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    :goto_0
    return-object v1

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    move-object v1, v0

    goto :goto_0

    :cond_5
    new-instance v3, Landroid/text/util/Rfc822Token;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v0, v4}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method createMoreChip()V
    .locals 23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChipPlainText()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mShouldShrink:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Landroid/text/Editable;->length()I

    move-result v21

    const-class v22, Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    invoke-interface/range {v19 .. v22}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Landroid/text/style/ImageSpan;

    array-length v0, v14

    move/from16 v19, v0

    if-lez v19, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v19

    const/16 v20, 0x0

    aget-object v20, v14, v20

    invoke-interface/range {v19 .. v20}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v19

    if-nez v19, :cond_3

    array-length v0, v9

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v19

    if-eqz v19, :cond_5

    array-length v0, v9

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_5

    :cond_4
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    goto :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v12

    array-length v7, v9

    const/4 v8, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateNumChipsCanShow()I

    move-result v19

    sub-int v8, v7, v19

    if-gtz v8, :cond_7

    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    goto :goto_0

    :cond_6
    add-int/lit8 v8, v7, -0x2

    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreSpan(I)Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    move-result-object v6

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    const/16 v17, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v15

    const/16 v18, 0x0

    new-instance v18, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual/range {v18 .. v18}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual/range {v18 .. v18}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    sub-int v5, v7, v8

    :goto_1
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v5, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    aget-object v20, v9, v5

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sub-int v19, v7, v8

    move/from16 v0, v19

    if-ne v5, v0, :cond_8

    aget-object v19, v9, v5

    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v17

    :cond_8
    array-length v0, v9

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    if-ne v5, v0, :cond_9

    aget-object v19, v9, v5

    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v16

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    aget-object v20, v9, v5

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    :cond_a
    aget-object v19, v9, v5

    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    aget-object v19, v9, v5

    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    aget-object v19, v9, v5

    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/android/ex/chips/RecipientChip;->setOriginalText(Ljava/lang/String;)V

    :cond_b
    aget-object v19, v9, v5

    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_c
    if-eqz v18, :cond_d

    invoke-virtual/range {v18 .. v18}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->recoverLayout()V

    invoke-interface {v15}, Landroid/text/Editable;->length()I

    move-result v19

    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    invoke-interface {v15}, Landroid/text/Editable;->length()I

    move-result v16

    :cond_e
    move/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v13

    new-instance v3, Landroid/text/SpannableString;

    invoke-interface {v15, v13, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v19, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v20

    const/16 v21, 0x21

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v3, v6, v0, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-interface {v15, v13, v4, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v19

    if-nez v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMaxLines:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto/16 :goto_0
.end method

.method createMoreChipPlainText()V
    .locals 10

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    const/4 v4, 0x0

    move v1, v4

    const/4 v2, 0x0

    :goto_0
    const/4 v7, 0x2

    if-ge v2, v7, :cond_0

    iget-object v7, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v7, v5, v4}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->movePastTerminators(I)I

    move-result v1

    move v4, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->countTokens(Landroid/text/Editable;)I

    move-result v6

    add-int/lit8 v7, v6, -0x2

    invoke-direct {p0, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreSpan(I)Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    move-result-object v3

    new-instance v0, Landroid/text/SpannableString;

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-interface {v5, v1, v7}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v7, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/16 v9, 0x21

    invoke-virtual {v0, v3, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-interface {v5, v1, v7, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    iput-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    return-void
.end method

.method public enableDrag()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDragEnabled:Z

    return-void
.end method

.method getChipBackground(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Lcom/android/ex/chips/RecipientEntry;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mInvalidChipBackground:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method getContactIds()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientChip;->getContactId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v5
.end method

.method getDataIds()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientChip;->getDataId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v5
.end method

.method protected getEnableDiscardNextActionUp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRETVDiscardNextActionUp:Z

    return v0
.end method

.method getLastChip()Lcom/android/ex/chips/RecipientChip;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v0, v2

    :cond_0
    return-object v1
.end method

.method getMoreChip()Landroid/text/style/ImageSpan;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    invoke-interface {v1, v4, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/MTKRecipientEditTextView$MoreImageSpan;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    aget-object v1, v0, v4

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;
    .locals 13

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v9

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    const-class v11, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v9, v12, v10, v11}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    const/4 v4, 0x0

    move-object v0, v6

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    instance-of v9, v1, Lcom/android/ex/chips/RecipientChip;

    if-nez v9, :cond_0

    const/4 v4, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_3

    move-object v0, v6

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    const-string v9, "getSortedRecipients"

    invoke-direct {p0, v9, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tempLogPrint(Ljava/lang/String;Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v9

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    const-class v11, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v9, v12, v10, v11}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/android/ex/chips/RecipientChip;

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v8

    new-instance v9, Lcom/android/ex/chips/MTKRecipientEditTextView$7;

    invoke-direct {v9, p0, v8}, Lcom/android/ex/chips/MTKRecipientEditTextView$7;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Landroid/text/Spannable;)V

    invoke-static {v5, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/android/ex/chips/RecipientChip;

    return-object v9
.end method

.method getSpannable()Landroid/text/Spannable;
    .locals 1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method getViewWidth()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public handleAndGetText()Landroid/text/Editable;
    .locals 1

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->appendPendingStrings()V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method handlePaste()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/ex/chips/RecipientChip;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v13

    invoke-interface {v12, v9, v13}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v6

    invoke-virtual {v9, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v11, v6

    move v8, v11

    const/4 v3, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz v11, :cond_3

    :cond_0
    if-eqz v11, :cond_1

    if-nez v3, :cond_1

    move v8, v11

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v12, v9, v11}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v11

    invoke-direct {p0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    if-ne v8, v11, :cond_0

    :cond_1
    if-eq v11, v6, :cond_3

    if-eqz v3, :cond_2

    move v11, v8

    :cond_2
    move v7, v6

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    sub-int v5, v12, v6

    :goto_0
    if-ge v11, v7, :cond_3

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13, v11}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v12

    invoke-virtual {p0, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView;->movePastTerminators(I)I

    move-result v10

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-direct {p0, v11, v10, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    invoke-direct {p0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v1

    if-nez v1, :cond_5

    :cond_3
    invoke-virtual {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isCompletedToken(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v4, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v11

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v12

    invoke-direct {p0, v11, v12, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitChip(IILandroid/text/Editable;)Z

    invoke-direct {p0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0

    :cond_5
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v12

    invoke-interface {v12, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v12

    add-int/lit8 v11, v12, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-interface {v12}, Landroid/text/Editable;->length()I

    move-result v12

    sub-int v7, v12, v5

    goto :goto_0
.end method

.method handlePendingChips()V
    .locals 20

    const-string v13, "RecipientEditTextView"

    const-string v14, "[Debug] handlePendingChips-start"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->appendPendingStrings()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getViewWidth()I

    move-result v13

    if-gtz v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    monitor-enter v14

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    const/16 v15, 0x64

    if-gt v13, v15, :cond_e

    const/4 v11, 0x0

    new-instance v11, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {v11}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual {v11}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v5, v13, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ne v5, v13, :cond_3

    if-eqz v11, :cond_2

    invoke-virtual {v11}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-interface {v13}, Landroid/text/Editable;->length()I

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->requestLayout()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    add-int v9, v10, v13

    if-ltz v10, :cond_5

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x2

    if-ge v9, v13, :cond_4

    invoke-interface {v4, v9}, Landroid/text/Editable;->charAt(I)C

    move-result v13

    const/16 v15, 0x2c

    if-ne v13, v15, :cond_4

    add-int/lit8 v9, v9, 0x1

    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createReplacementChip(IILandroid/text/Editable;)V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v13

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v15

    const-class v16, Lcom/android/ex/chips/RecipientChip;

    move-object/from16 v0, v16

    invoke-interface {v13, v10, v15, v0}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/android/ex/chips/RecipientChip;

    if-eqz v2, :cond_9

    array-length v13, v2

    if-lez v13, :cond_9

    const/4 v8, 0x0

    const/4 v12, 0x0

    :goto_2
    array-length v13, v2

    if-ge v12, v13, :cond_6

    aget-object v13, v2, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    if-ne v13, v10, :cond_8

    aget-object v13, v2, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v7

    const/4 v8, 0x1

    :cond_6
    if-nez v8, :cond_7

    const/4 v7, 0x0

    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_9
    const/4 v7, 0x0

    goto :goto_3

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->sanitizeEnd()V

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->recoverLayout()V

    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    if-eqz v13, :cond_b

    const-string v13, "RecipientEditTextView"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[handlePendingChips] Restore text ->"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    invoke-interface {v13, v15}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    if-eqz v13, :cond_12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/16 v15, 0x64

    if-gt v13, v15, :cond_12

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->hasFocus()Z

    move-result v13

    if-nez v13, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v15, 0x2

    if-ge v13, v15, :cond_f

    :cond_c
    new-instance v13, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v15}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/MTKRecipientEditTextView$1;)V

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Void;

    invoke-virtual {v13, v15}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    :cond_d
    :goto_5
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChips:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v13, "RecipientEditTextView"

    const-string v14, "[Debug] handlePendingChips-end"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_e
    const/4 v13, 0x1

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    goto/16 :goto_4

    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v13

    :cond_f
    const/4 v6, 0x0

    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v13

    if-eqz v13, :cond_10

    invoke-direct/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->calculateNumChipsCanShow()I

    move-result v6

    :goto_6
    new-instance v13, Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v15}, Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/MTKRecipientEditTextView$1;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mIndividualReplacements:Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mIndividualReplacements:Lcom/android/ex/chips/MTKRecipientEditTextView$IndividualReplacementTask;

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v6}, Ljava/util/AbstractList;->subList(II)Ljava/util/List;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v17, v15, v16

    invoke-virtual {v13, v15}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-le v13, v6, :cond_11

    new-instance v13, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v15, v6, v0}, Ljava/util/AbstractList;->subList(II)Ljava/util/List;

    move-result-object v15

    invoke-direct {v13, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChip()V

    goto/16 :goto_5

    :cond_10
    const/4 v6, 0x2

    goto :goto_6

    :cond_11
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    goto :goto_7

    :cond_12
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTemporaryRecipients:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->hasFocus()Z

    move-result v13

    if-nez v13, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createMoreChip()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5
.end method

.method isCompletedToken(Ljava/lang/CharSequence;)Z
    .locals 6
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v5, p1, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v5, 0x2c

    if-eq v0, v5, :cond_2

    const/16 v5, 0x3b

    if-eq v0, v5, :cond_2

    const v5, 0xff0c

    if-eq v0, v5, :cond_2

    const v5, 0xff1b

    if-ne v0, v5, :cond_0

    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method protected isPhoneQuery()Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual {v0}, Lcom/android/ex/chips/BaseRecipientAdapter;->getQueryType()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrollAddText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mEnableScrollAddText:Z

    return v0
.end method

.method protected isTouchPointInChip(FF)Z
    .locals 8
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v2, v6}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v4

    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v6

    int-to-float v6, v6

    sub-float v0, p1, v6

    const/4 v6, 0x0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v0, v6

    cmpl-float v6, v0, v4

    if-lez v6, :cond_0

    const/4 v1, 0x0

    :cond_0
    return v1
.end method

.method public moveCursorToVisibleOffset()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoveCursorToVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoveCursorToVisible:Z

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/TextView;->moveCursorToVisibleOffset()Z

    move-result v0

    goto :goto_0
.end method

.method movePastTerminators(I)I
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result v2

    if-lt p1, v2, :cond_0

    move v1, p1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x2c

    if-eq v0, v2, :cond_1

    const/16 v2, 0x3b

    if-ne v0, v2, :cond_2

    :cond_1
    add-int/lit8 p1, p1, 0x1

    :cond_2
    invoke-virtual {p0}, Landroid/widget/TextView;->length()I

    move-result v2

    if-ge p1, v2, :cond_3

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_3

    add-int/lit8 p1, p1, 0x1

    :cond_3
    move v1, p1

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method

.method public onCheckedItemChanged(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAlternatesPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    :cond_0
    iput p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCheckedItem:I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "clipboard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    const-string v1, ""

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyAddress:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public onClick(Lcom/android/ex/chips/RecipientChip;IFF)V
    .locals 8
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # I
    .param p3    # F
    .param p4    # F

    const/4 v7, 0x1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientChip;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isInDelete(Lcom/android/ex/chips/RecipientChip;IFF)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getMaxLines()I

    move-result v2

    if-eq v2, v7, :cond_0

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    add-int/lit8 v5, v2, -0x1

    sub-int v5, v1, v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/text/Layout;->getLineTop(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/text/Layout;->getLineTop(I)I

    move-result v6

    add-int v4, v5, v6

    if-gt v4, v3, :cond_0

    invoke-direct {p0, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeChip(Lcom/android/ex/chips/RecipientChip;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    goto :goto_1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "RecipientEditTextView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onConfigurationChanged] current view width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", line count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->registerGlobalLayoutListener()V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPreviousWidth:I

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v0, 0x0

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/lit16 v1, v2, 0xff

    and-int/lit8 v2, v1, 0x6

    if-eqz v2, :cond_0

    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    xor-int/2addr v2, v1

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v2, v2, 0x6

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    :cond_0
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v3, 0x40000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v3, -0x40000001

    and-int/2addr v2, v3

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/android/ex/chips/R$string;->done:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 0
    .param p1    # Landroid/view/ActionMode;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyAddress:Ljava/lang/String;

    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 2
    .param p1    # Landroid/view/DragEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handlePasteClip(Landroid/content/ClipData;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v1, 0x6

    if-ne p2, v1, :cond_3

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitDefault()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->focusNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFilterComplete(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bTouchedAfterPasted:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onFilterComplete(I)V

    :cond_0
    iput-boolean v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bPasted:Z

    iput-boolean v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bTouchedAfterPasted:Z

    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v0, 0x0

    return v0
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # Landroid/graphics/Rect;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shrink()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->expand()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->submitItemAtPosition(I)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitDefault()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->focusNext()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :goto_1
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->focusNext()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitDefault()Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x3d -> :sswitch_1
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->putOffsetInRange(I)I

    move-result v1

    invoke-virtual {p0, v2, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isTouchPointInChip(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-boolean v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDragEnabled:Z

    if-eqz v4, :cond_3

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->startDrag(Lcom/android/ex/chips/RecipientChip;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->showCopyDialog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreDraw()Z
    .locals 2

    invoke-super {p0}, Landroid/widget/TextView;->onPreDraw()Z

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setDisableBringPointIntoView(Z)V

    return v0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v0, 0x0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 15
    .param p1    # Landroid/os/Parcelable;

    const-string v12, "RecipientEditTextView"

    const-string v13, "[onRestoreInstanceState]"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v6, p1

    check-cast v6, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;

    iget-boolean v2, v6, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;->frozenWithFocus:Z

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    const/4 v12, 0x0

    invoke-super {p0, v12}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    const-string v12, "RecipientEditTextView"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[onRestore] Text->"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_6

    if-eqz v1, :cond_6

    const-string v12, "RecipientEditTextView"

    const-string v13, "[onRestore] Do restore process"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    if-eqz v12, :cond_1

    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    const/4 v13, 0x0

    invoke-interface {v12, v13, v8}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    new-instance v5, Lcom/android/ex/chips/MTKRecipientList;

    invoke-direct {v5}, Lcom/android/ex/chips/MTKRecipientList;-><init>()V

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    :goto_1
    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v12, v7, v10}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v9

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v9, v12, :cond_4

    invoke-virtual {v7, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v10, v9, 0x2

    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tokenizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    :goto_2
    invoke-virtual {v5, v12, v0}, Lcom/android/ex/chips/MTKRecipientList;->addRecipient(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v12

    invoke-super {p0, v12}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tokenizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->appendList(Lcom/android/ex/chips/MTKRecipientList;)V

    if-ge v10, v9, :cond_5

    invoke-virtual {v7, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/android/ex/chips/MTKRecipientList;->getRecipientCount()I

    move-result v12

    if-eqz v12, :cond_7

    iput-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mStringToBeRestore:Ljava/lang/String;

    :cond_5
    :goto_3
    iget-object v12, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    iget-object v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mAddTextWatcher:Ljava/lang/Runnable;

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_6
    return-void

    :cond_7
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-interface {v12, v4}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_3
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    const-string v2, "RecipientEditTextView"

    const-string v3, "[onSaveInstanceState]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    invoke-super {p0}, Landroid/widget/TextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;

    invoke-direct {v0, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;->frozenWithFocus:Z

    :goto_0
    const-string v2, "RecipientEditTextView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onSave] Text ->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientSavedState;->frozenWithFocus:Z

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v0, 0x0

    return v0
.end method

.method public onSelectionChanged(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    if-ge p1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mNoChips:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mJustExpanded:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_2

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    :goto_0
    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setSelection(I)V

    iput-boolean v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mJustExpanded:Z

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onSelectionChanged(II)V

    return-void

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onSizeChanged(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v1, :cond_1

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->postHandlePendingChips()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTriedGettingScrollView:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-nez v1, :cond_2

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->checkChipWidths()V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mScrollView:Landroid/widget/ScrollView;

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTriedGettingScrollView:Z

    :cond_4
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    const v2, 0x1020022

    if-ne p1, v2, :cond_0

    iput-boolean v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bPasted:Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "clipboard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->handlePasteClip(Landroid/content/ClipData;)V

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    move-result v1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-boolean v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bPasted:Z

    if-eqz v9, :cond_2

    iput-boolean v8, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->bTouchedAfterPasted:Z

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    :cond_2
    const/high16 v6, -0x40800000

    const/high16 v7, -0x40800000

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x1

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyAddress:Ljava/lang/String;

    if-nez v9, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-ne v9, v8, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {p0, v6, v7}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v9

    invoke-direct {p0, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->putOffsetInRange(I)I

    move-result v4

    invoke-virtual {p0, v6, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isTouchPointInChip(FF)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-direct {p0, v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_3

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-super {p0, v10}, Landroid/widget/TextView;->setShowSoftInputOnFocus(Z)V

    :cond_3
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-nez v9, :cond_4

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v9, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_4
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getEnableDiscardNextActionUp()Z

    move-result v9

    if-eqz v9, :cond_6

    if-ne v0, v8, :cond_6

    invoke-virtual {p0, v10}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setEnableDiscardNextActionUp(Z)V

    if-nez v5, :cond_0

    invoke-super {p0, v8}, Landroid/widget/TextView;->setShowSoftInputOnFocus(Z)V

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mCopyAddress:Ljava/lang/String;

    if-nez v9, :cond_a

    if-ne v0, v8, :cond_a

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v9

    if-eqz v9, :cond_8

    iget-boolean v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoveCursorToVisible:Z

    if-eqz v9, :cond_8

    iput-boolean v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoveCursorToVisible:Z

    if-nez v5, :cond_7

    invoke-super {p0, v8}, Landroid/widget/TextView;->setShowSoftInputOnFocus(Z)V

    :cond_7
    move v3, v8

    goto :goto_0

    :cond_8
    if-eqz v2, :cond_e

    if-ne v0, v8, :cond_9

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v9, :cond_c

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eq v9, v2, :cond_c

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->selectChip(Lcom/android/ex/chips/RecipientChip;)Lcom/android/ex/chips/RecipientChip;

    move-result-object v9

    iput-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    :cond_9
    :goto_2
    const/4 v1, 0x1

    const/4 v3, 0x1

    :cond_a
    :goto_3
    if-ne v0, v8, :cond_b

    if-nez v1, :cond_b

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :cond_b
    if-nez v5, :cond_0

    invoke-super {p0, v8}, Landroid/widget/TextView;->setShowSoftInputOnFocus(Z)V

    goto/16 :goto_0

    :cond_c
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-nez v9, :cond_d

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->length()I

    move-result v9

    invoke-virtual {p0, v9}, Landroid/widget/EditText;->setSelection(I)V

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->commitDefault()Z

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->selectChip(Lcom/android/ex/chips/RecipientChip;)Lcom/android/ex/chips/RecipientChip;

    move-result-object v9

    iput-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    goto :goto_2

    :cond_d
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p0, v9, v4, v6, v7}, Lcom/android/ex/chips/MTKRecipientEditTextView;->onClick(Lcom/android/ex/chips/RecipientChip;IFF)V

    goto :goto_2

    :cond_e
    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    invoke-direct {p0, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->shouldShowEditableText(Lcom/android/ex/chips/RecipientChip;)Z

    move-result v9

    if-eqz v9, :cond_a

    const/4 v1, 0x1

    goto :goto_3
.end method

.method protected performFiltering(Ljava/lang/CharSequence;I)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I

    invoke-virtual {p0}, Landroid/widget/MultiAutoCompleteTextView;->enoughToFilter()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isCompletedToken(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v4, p1, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v2

    const-class v4, Lcom/android/ex/chips/RecipientChip;

    invoke-interface {v2, v3, v1, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/RecipientChip;

    if-eqz v0, :cond_0

    array-length v4, v0

    if-lez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public performValidation()V
    .locals 0

    return-void
.end method

.method removeChip(Lcom/android/ex/chips/RecipientChip;)V
    .locals 13
    .param p1    # Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v7

    invoke-interface {v7, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v7, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    move v9, v5

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    if-ne p1, v11, :cond_1

    const/4 v10, 0x1

    :goto_0
    if-eqz v10, :cond_0

    const/4 v11, 0x0

    iput-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    :cond_0
    :goto_1
    if-ltz v9, :cond_2

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v11

    if-ge v9, v11, :cond_2

    invoke-interface {v8, v9}, Landroid/text/Editable;->charAt(I)C

    move-result v11

    const/16 v12, 0x20

    if-ne v11, v12, :cond_2

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v11

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v12

    if-ne v11, v12, :cond_3

    invoke-direct {p0, p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v11

    invoke-direct {p0, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v12

    if-ne v11, v12, :cond_3

    const/4 v3, 0x0

    :cond_3
    invoke-interface {v7, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_4

    array-length v11, v0

    if-nez v11, :cond_5

    :cond_4
    const/4 v3, 0x0

    :cond_5
    new-instance v4, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;

    invoke-direct {v4, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    if-eqz v3, :cond_7

    const/4 v1, 0x0

    const/4 v1, 0x0

    :goto_2
    array-length v11, v0

    if-ge v1, v11, :cond_6

    aget-object v11, v0, v1

    invoke-direct {p0, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v11

    if-lt v11, v6, :cond_b

    :cond_6
    array-length v11, v0

    invoke-virtual {v4, v1, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->removeChipsWithoutNotification(II)V

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    :cond_7
    if-ltz v6, :cond_8

    if-lez v9, :cond_8

    invoke-interface {v8, v6, v9}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_8
    if-eqz v3, :cond_9

    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    sub-int v11, v5, v6

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v4, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->addChipsBackWithoutNotification(I)V

    :cond_9
    if-eqz v10, :cond_a

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method removeMoreChip()V
    .locals 12

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v7

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    invoke-interface {v7, v10}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    const/4 v10, 0x0

    iput-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v6

    if-eqz v6, :cond_0

    array-length v10, v6

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    array-length v10, v6

    add-int/lit8 v10, v10, -0x1

    aget-object v10, v6, v10

    invoke-interface {v7, v10}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    const/4 v9, 0x0

    new-instance v9, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    invoke-direct {v9, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientChip;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientChip;->getOriginalText()Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v8, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v10

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v4, v1

    const/4 v10, -0x1

    if-eq v2, v10, :cond_2

    const/16 v10, 0x21

    invoke-interface {v3, v0, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_3
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_4
    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->recoverLayout()V

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRemovedSpans:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1    # Landroid/text/TextWatcher;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-super {p0, p1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method replaceChip(Lcom/android/ex/chips/RecipientChip;Lcom/android/ex/chips/RecipientEntry;)V
    .locals 15
    .param p1    # Lcom/android/ex/chips/RecipientChip;
    .param p2    # Lcom/android/ex/chips/RecipientEntry;

    iget-object v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    move-object/from16 v0, p1

    if-ne v0, v13, :cond_8

    const/4 v12, 0x1

    :goto_0
    if-eqz v12, :cond_0

    const/4 v13, 0x0

    iput-object v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mSelectedChip:Lcom/android/ex/chips/RecipientChip;

    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v10

    invoke-direct/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-interface {v13, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v6

    invoke-direct/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v14

    if-ne v13, v14, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    invoke-direct {p0, v6}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v14

    if-ne v13, v14, :cond_1

    const/4 v7, 0x0

    :cond_1
    new-instance v8, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;

    invoke-direct {v8, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v9

    if-eqz v7, :cond_3

    const/4 v5, 0x0

    const/4 v5, 0x0

    :goto_1
    array-length v13, v2

    if-ge v5, v13, :cond_2

    aget-object v13, v2, v5

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v13

    if-lt v13, v10, :cond_9

    :cond_2
    array-length v13, v2

    invoke-virtual {v8, v5, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->removeChipsWithoutNotification(II)V

    const/4 v13, 0x1

    iput-boolean v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    :cond_3
    if-eqz v1, :cond_5

    const/4 v13, -0x1

    if-eq v10, v13, :cond_4

    const/4 v13, -0x1

    if-ne v4, v13, :cond_a

    :cond_4
    const-string v13, "RecipientEditTextView"

    const-string v14, "The chip to replace does not exist but should."

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    invoke-interface {v3, v13, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_5
    :goto_2
    if-eqz v7, :cond_6

    const/4 v13, 0x0

    iput-boolean v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mDuringAccelerateRemoveChip:Z

    sub-int v13, v4, v10

    add-int/lit8 v13, v13, 0x1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v8, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->addChipsBackWithoutNotification(I)V

    :cond_6
    const/4 v13, 0x1

    invoke-virtual {p0, v13}, Landroid/widget/TextView;->setCursorVisible(Z)V

    if-eqz v12, :cond_7

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->clearSelectedChip()V

    :cond_7
    return-void

    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_a
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    move v11, v4

    :goto_3
    if-ltz v11, :cond_b

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v13

    if-ge v11, v13, :cond_b

    invoke-interface {v3, v11}, Landroid/text/Editable;->charAt(I)C

    move-result v13

    const/16 v14, 0x20

    if-ne v13, v14, :cond_b

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_b
    invoke-interface {v3, v10, v11, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_2
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method sanitizeBetween()V
    .locals 8

    iget v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v6, v3

    if-lez v6, :cond_0

    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    aget-object v2, v3, v6

    const/4 v0, 0x0

    array-length v6, v3

    const/4 v7, 0x1

    if-le v6, v7, :cond_2

    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-object v0, v3, v6

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v6

    invoke-interface {v6, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v6

    invoke-interface {v6, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    const/4 v6, -0x1

    if-eq v4, v6, :cond_0

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-gt v4, v6, :cond_0

    invoke-interface {v5, v4}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_3

    add-int/lit8 v4, v4, 0x1

    :cond_3
    if-ltz v4, :cond_0

    if-ltz v1, :cond_0

    if-ge v4, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6, v4, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method sanitizeEnd()V
    .locals 8

    iget v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mPendingChipsCount:I

    if-lez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v5, v0

    if-lez v5, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getMoreChip()Landroid/text/style/ImageSpan;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    if-eqz v5, :cond_3

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreChip:Landroid/text/style/ImageSpan;

    :goto_1
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v4

    if-le v4, v2, :cond_0

    const-string v5, "RecipientEditTextView"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "RecipientEditTextView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "There were extra characters after the last tokenizable entry."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v5, v2, 0x1

    invoke-interface {v1, v5, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getLastChip()Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    goto :goto_1
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/widget/ListAdapter;",
            ":",
            "Landroid/widget/Filterable;",
            ">(TT;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    check-cast p1, Lcom/android/ex/chips/BaseRecipientAdapter;

    new-instance v0, Lcom/android/ex/chips/MTKRecipientEditTextView$6;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/MTKRecipientEditTextView$6;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/BaseRecipientAdapter;->registerUpdateObserver(Lcom/android/ex/chips/BaseRecipientAdapter$EntriesUpdatedObserver;)V

    return-void
.end method

.method setChipBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipBackground:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method setChipHeight(I)V
    .locals 1
    .param p1    # I

    int-to-float v0, p1

    iput v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mChipHeight:F

    return-void
.end method

.method protected setEnableDiscardNextActionUp(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mRETVDiscardNextActionUp:Z

    return-void
.end method

.method setMoreItem(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mMoreItem:Landroid/widget/TextView;

    return-void
.end method

.method public setOnFocusListShrinkRecipients(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mShouldShrink:Z

    return-void
.end method

.method public setScrollAddText(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mEnableScrollAddText:Z

    return-void
.end method

.method public setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V
    .locals 1
    .param p1    # Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iget-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-super {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    return-void
.end method

.method public setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V
    .locals 0
    .param p1    # Landroid/widget/AutoCompleteTextView$Validator;

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    return-void
.end method

.method public showDropDown()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isEndChip()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    goto :goto_0
.end method

.method public updatePressedChip(FFLcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;)V
    .locals 15
    .param p1    # F
    .param p2    # F
    .param p3    # Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    invoke-virtual/range {p0 .. p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v13

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->putOffsetInRange(I)I

    move-result v9

    invoke-virtual/range {p0 .. p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isTouchPointInChip(FF)Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-direct {p0, v9}, Lcom/android/ex/chips/MTKRecipientEditTextView;->findChip(I)Lcom/android/ex/chips/RecipientChip;

    move-result-object v3

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    sget-object v13, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->DELETE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    move-object/from16 v0, p3

    if-ne v0, v13, :cond_3

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientChip;->getValue()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createTokenizedEntry(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipStart(Lcom/android/ex/chips/RecipientChip;)I

    move-result v11

    invoke-direct {p0, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getChipEnd(Lcom/android/ex/chips/RecipientChip;)I

    move-result v6

    const-string v13, ""

    invoke-static {v5, v11, v6, v13}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    const/4 v13, 0x0

    invoke-direct {p0, v8, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createChip(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v13, -0x1

    if-le v11, v13, :cond_0

    const/4 v13, -0x1

    if-le v6, v13, :cond_0

    add-int/lit8 v13, v6, 0x1

    invoke-interface {v5, v11, v13, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v13

    invoke-virtual {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createAddressText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v1}, Lcom/android/ex/chips/RecipientAlternatesAdapter;->getMatchingRecipients(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/ex/chips/RecipientEntry;->getContactId()J

    move-result-wide v13

    invoke-static {v13, v14}, Lcom/android/ex/chips/RecipientEntry;->isCreatedRecipient(J)Z

    move-result v13

    if-nez v13, :cond_4

    sget-object v13, Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;->UPDATE_CONTACT:Lcom/android/ex/chips/MTKRecipientEditTextView$UpdatePressedChipType;

    move-object/from16 v0, p3

    if-ne v0, v13, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v13

    invoke-interface {v13, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_0

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/ex/chips/RecipientEntry;->getDestination()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    invoke-static {v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneNumber(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createValidatedEntry(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v8

    :goto_2
    if-nez v8, :cond_5

    invoke-virtual {p0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->isPhoneQuery()Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v8

    :cond_5
    move-object v12, v8

    if-eqz v12, :cond_0

    invoke-virtual {v8}, Lcom/android/ex/chips/RecipientEntry;->getPhotoThumbnailUri()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v13

    check-cast v13, Lcom/android/ex/chips/BaseRecipientAdapter;

    invoke-virtual {v13, v10}, Lcom/android/ex/chips/BaseRecipientAdapter;->updatePhotoCacheByUri(Landroid/net/Uri;)V

    :cond_6
    iget-object v13, p0, Lcom/android/ex/chips/MTKRecipientEditTextView;->mHandler:Landroid/os/Handler;

    new-instance v14, Lcom/android/ex/chips/MTKRecipientEditTextView$10;

    invoke-direct {v14, p0, v3, v12}, Lcom/android/ex/chips/MTKRecipientEditTextView$10;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;Lcom/android/ex/chips/RecipientEntry;)V

    invoke-virtual {v13, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->tokenizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, v13}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createValidatedEntry(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v8

    goto :goto_2
.end method
