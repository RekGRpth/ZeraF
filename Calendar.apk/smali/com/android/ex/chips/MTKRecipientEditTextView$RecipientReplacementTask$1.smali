.class Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;
.super Ljava/lang/Object;
.source "MTKRecipientEditTextView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

.field final synthetic val$originalRecipients:Ljava/util/ArrayList;

.field final synthetic val$replacements:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iput-object p2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$replacements:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$originalRecipients:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const-string v10, "RecipientEditTextView"

    const-string v11, "[Debug] RecipientReplacementTask,run-start"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    const/4 v3, 0x0

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$replacements:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x0

    if-lez v8, :cond_0

    new-instance v9, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {v9, v10}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    :cond_0
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$originalRecipients:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientChip;

    add-int/lit8 v10, v8, -0x1

    if-ne v3, v10, :cond_2

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_1
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v11, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v11, v11, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-interface {v11}, Landroid/text/Editable;->length()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v10}, Landroid/view/View;->requestLayout()V

    :cond_2
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    const/4 v10, -0x1

    if-eq v7, v10, :cond_4

    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v5, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$replacements:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/ex/chips/RecipientChip;

    new-instance v1, Landroid/text/SpannableString;

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v6}, Lcom/android/ex/chips/RecipientChip;->getEntry()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->createAddressText(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v10, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v11

    const/16 v12, 0x21

    invoke-virtual {v1, v6, v10, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-interface {v5, v7, v2, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    invoke-virtual {v1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/android/ex/chips/RecipientChip;->setOriginalText(Ljava/lang/String;)V

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v10}, Landroid/view/View;->hasFocus()Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$replacements:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/ex/chips/RecipientChip;

    if-eqz v6, :cond_3

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v10}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3000(Lcom/android/ex/chips/MTKRecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v10

    if-nez v10, :cond_5

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v10, v11}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3002(Lcom/android/ex/chips/MTKRecipientEditTextView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_5
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v10}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3000(Lcom/android/ex/chips/MTKRecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->val$originalRecipients:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    if-lez v8, :cond_7

    iget-object v10, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask$1;->this$1:Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;

    iget-object v10, v10, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientReplacementTask;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v10}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3100(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    :cond_7
    const-string v10, "RecipientEditTextView"

    const-string v11, "[Debug] RecipientReplacementTask,run-end"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
