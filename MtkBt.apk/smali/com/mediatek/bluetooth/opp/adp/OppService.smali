.class public Lcom/mediatek/bluetooth/opp/adp/OppService;
.super Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;
.source "OppService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/opp/adp/OppService$OppsTaskHandler;,
        Lcom/mediatek/bluetooth/opp/adp/OppService$OppcTaskHandler;
    }
.end annotation


# instance fields
.field private mIsOppcResetTask:Z

.field private mIsOppsResetTask:Z

.field private mIsTaskWorkThreadInterrupted:Z

.field private mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

.field private mOppcCurrentStoragePath:Ljava/lang/String;

.field private mOppcWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

.field private mOppsCurrentStoragePath:Ljava/lang/String;

.field private mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

.field private mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsTaskWorkThreadInterrupted:Z

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppcResetTask:Z

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppsResetTask:Z

    iput-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcCurrentStoragePath:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsCurrentStoragePath:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/opp/adp/OppService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcCurrentStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/bluetooth/opp/adp/OppService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcCurrentStoragePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/opp/adp/OppService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsCurrentStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/opp/adp/OppService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsCurrentStoragePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/opp/adp/OppService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppcResetTask:Z

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/bluetooth/opp/adp/OppService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppcResetTask:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/opp/adp/OppService;)Lcom/mediatek/bluetooth/opp/adp/OppManager;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/opp/adp/OppService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsTaskWorkThreadInterrupted:Z

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/opp/adp/OppService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppsResetTask:Z

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/bluetooth/opp/adp/OppService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/opp/adp/OppService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsOppsResetTask:Z

    return p1
.end method

.method private registerSdcardBroadcastReceiver()V
    .locals 2

    const-string v1, "OppService.registerSdcardBroadcastReceiver()[+]"

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/bluetooth/opp/adp/OppService$1;

    invoke-direct {v1, p0}, Lcom/mediatek/bluetooth/opp/adp/OppService$1;-><init>(Lcom/mediatek/bluetooth/opp/adp/OppService;)V

    iput-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private unregisterSdCardBroadcastReceiver()V
    .locals 1

    const-string v0, "OppService.unregisterSdCardBroadcastReceiver()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mSdcardBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    const/16 v5, 0xb

    const/16 v4, 0xa

    const/16 v3, 0xe

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "OppService.onCreate()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->onCreate()V

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->mIsServiceNativeEnabled:Z

    if-nez v0, :cond_0

    const-string v0, "OppService native onCreate failed."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-static {p0, v2, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    invoke-static {p0, v1, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/mediatek/bluetooth/opp/adp/OppManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/bluetooth/opp/adp/OppManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    invoke-virtual {v0, p0}, Lcom/mediatek/bluetooth/opp/adp/OppManager;->setOppService(Lcom/mediatek/bluetooth/opp/adp/OppService;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppManager;->cancelAllNotification()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/opp/adp/OppService;->registerSdcardBroadcastReceiver()V

    invoke-static {p0, v2, v4}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, v2, v5}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    :goto_1
    invoke-static {p0, v1, v4}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, v1, v5}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    :goto_2
    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    const-string v1, "BtOppc"

    new-instance v2, Lcom/mediatek/bluetooth/opp/adp/OppService$OppcTaskHandler;

    invoke-direct {v2, p0}, Lcom/mediatek/bluetooth/opp/adp/OppService$OppcTaskHandler;-><init>(Lcom/mediatek/bluetooth/opp/adp/OppService;)V

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;-><init>(Ljava/lang/String;Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    const-string v1, "BtOpps"

    new-instance v2, Lcom/mediatek/bluetooth/opp/adp/OppService$OppsTaskHandler;

    invoke-direct {v2, p0}, Lcom/mediatek/bluetooth/opp/adp/OppService$OppsTaskHandler;-><init>(Lcom/mediatek/bluetooth/opp/adp/OppService;)V

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;-><init>(Ljava/lang/String;Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string v0, "OppService.onCreate()[-]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p0, v2, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    goto :goto_1

    :cond_2
    invoke-static {p0, v1, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 5

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "OppService.onDestroy()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->mIsServiceNativeEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppManager;->oppOnServiceStop()V

    const-string v0, "OppService.onDestroy() interrupt OppTaskWorkerThread..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mIsTaskWorkThreadInterrupted:Z

    const-string v0, "OppService.onDestroy() disable oppc/opps native service..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, v1, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsDisable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, v2, v3}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/bluetooth/opp/adp/OppService;->unregisterSdCardBroadcastReceiver()V

    :cond_0
    const-string v0, "OppService.onDestroy() call native destroy()..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppManager:Lcom/mediatek/bluetooth/opp/adp/OppManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppManager;->setOppService(Lcom/mediatek/bluetooth/opp/adp/OppService;)V

    :cond_1
    const-string v0, "OppService.onDestroy()[-]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-static {p0, v1, v4}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    goto :goto_0

    :cond_3
    invoke-static {p0, v2, v4}, Lcom/mediatek/bluetooth/opp/adp/BluetoothOppService;->sendActivationBroadcast(Landroid/content/Context;ZI)V

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v1, "OppService.onStartCommand()[+]"

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->mIsServiceNativeEnabled:Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OppService.onStartCommand() action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.opp.action.OPPC_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppcWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;->notifyNewTask()V

    :goto_0
    const-string v1, "OppService.onStartCommand()[-]"

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1

    :cond_0
    const-string v1, "com.mediatek.bluetooth.opp.action.OPPS_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsStopListenDisconnect()V

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;->notifyNewTask()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppService;->mOppsWorker:Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskWorkerThread;->notifyNewTask()V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OppService.onStartCommand() warn: isServiceNativeEnabled["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->mIsServiceNativeEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] or null Intent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
