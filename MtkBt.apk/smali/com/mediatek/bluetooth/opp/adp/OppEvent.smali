.class public Lcom/mediatek/bluetooth/opp/adp/OppEvent;
.super Ljava/lang/Object;
.source "OppEvent.java"


# static fields
.field public static final BT_OPPC_CONNECTED:I = 0x5

.field public static final BT_OPPC_DISABLE_FAIL:I = 0x4

.field public static final BT_OPPC_DISABLE_SUCCESS:I = 0x3

.field public static final BT_OPPC_DISCONNECT:I = 0x10

.field public static final BT_OPPC_ENABLE_FAIL:I = 0x2

.field public static final BT_OPPC_ENABLE_SUCCESS:I = 0x1

.field public static final BT_OPPC_EXCH_FAIL:I = 0xf

.field public static final BT_OPPC_EXCH_START:I = 0xd

.field public static final BT_OPPC_EXCH_SUCCESS:I = 0xe

.field public static final BT_OPPC_GROUP_END:I = 0x1e

.field public static final BT_OPPC_GROUP_START:I = 0x0

.field public static final BT_OPPC_PROGRESS_UPDATE:I = 0x6

.field public static final BT_OPPC_PULL_FAIL:I = 0xc

.field public static final BT_OPPC_PULL_START:I = 0xa

.field public static final BT_OPPC_PULL_SUCCESS:I = 0xb

.field public static final BT_OPPC_PUSH_FAIL:I = 0x9

.field public static final BT_OPPC_PUSH_START:I = 0x7

.field public static final BT_OPPC_PUSH_SUCCESS:I = 0x8

.field public static final BT_OPPS_DISABLE_FAIL:I = 0x68

.field public static final BT_OPPS_DISABLE_SUCCESS:I = 0x67

.field public static final BT_OPPS_DISCONNECT:I = 0x70

.field public static final BT_OPPS_ENABLE_FAIL:I = 0x66

.field public static final BT_OPPS_ENABLE_SUCCESS:I = 0x65

.field public static final BT_OPPS_GROUP_END:I = 0x82

.field public static final BT_OPPS_GROUP_START:I = 0x64

.field public static final BT_OPPS_PROGRESS_UPDATE:I = 0x69

.field public static final BT_OPPS_PULL_ACCESS_REQUEST:I = 0x72

.field public static final BT_OPPS_PULL_FAIL:I = 0x6f

.field public static final BT_OPPS_PULL_START:I = 0x6d

.field public static final BT_OPPS_PULL_SUCCESS:I = 0x6e

.field public static final BT_OPPS_PUSH_ACCESS_REQUEST:I = 0x71

.field public static final BT_OPPS_PUSH_FAIL:I = 0x6c

.field public static final BT_OPPS_PUSH_START:I = 0x6a

.field public static final BT_OPPS_PUSH_SUCCESS:I = 0x6b


# instance fields
.field public event:I

.field public parameters:[Ljava/lang/String;


# direct methods
.method public constructor <init>(I[Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    iput-object p2, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    return-void
.end method

.method public static getEventName(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknow event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "BT_OPPC_ENABLE_SUCCESS"

    goto :goto_0

    :sswitch_1
    const-string v0, "BT_OPPC_ENABLE_FAIL"

    goto :goto_0

    :sswitch_2
    const-string v0, "BT_OPPC_DISABLE_SUCCESS"

    goto :goto_0

    :sswitch_3
    const-string v0, "BT_OPPC_DISABLE_FAIL"

    goto :goto_0

    :sswitch_4
    const-string v0, "BT_OPPC_PROGRESS_UPDATE"

    goto :goto_0

    :sswitch_5
    const-string v0, "BT_OPPC_PUSH_START"

    goto :goto_0

    :sswitch_6
    const-string v0, "BT_OPPC_PUSH_SUCCESS"

    goto :goto_0

    :sswitch_7
    const-string v0, "BT_OPPC_PUSH_FAIL"

    goto :goto_0

    :sswitch_8
    const-string v0, "BT_OPPC_PULL_START"

    goto :goto_0

    :sswitch_9
    const-string v0, "BT_OPPC_PULL_SUCCESS"

    goto :goto_0

    :sswitch_a
    const-string v0, "BT_OPPC_PULL_FAIL"

    goto :goto_0

    :sswitch_b
    const-string v0, "BT_OPPC_EXCH_START"

    goto :goto_0

    :sswitch_c
    const-string v0, "BT_OPPC_EXCH_SUCCESS"

    goto :goto_0

    :sswitch_d
    const-string v0, "BT_OPPC_EXCH_FAIL"

    goto :goto_0

    :sswitch_e
    const-string v0, "BT_OPPC_DISCONNECT"

    goto :goto_0

    :sswitch_f
    const-string v0, "BT_OPPS_ENABLE_SUCCESS"

    goto :goto_0

    :sswitch_10
    const-string v0, "BT_OPPS_ENABLE_FAIL"

    goto :goto_0

    :sswitch_11
    const-string v0, "BT_OPPS_DISABLE_SUCCESS"

    goto :goto_0

    :sswitch_12
    const-string v0, "BT_OPPS_DISABLE_FAIL"

    goto :goto_0

    :sswitch_13
    const-string v0, "BT_OPPS_PROGRESS_UPDATE"

    goto :goto_0

    :sswitch_14
    const-string v0, "BT_OPPS_PUSH_START"

    goto :goto_0

    :sswitch_15
    const-string v0, "BT_OPPS_PUSH_SUCCESS"

    goto :goto_0

    :sswitch_16
    const-string v0, "BT_OPPS_PUSH_FAIL"

    goto :goto_0

    :sswitch_17
    const-string v0, "BT_OPPS_PULL_START"

    goto :goto_0

    :sswitch_18
    const-string v0, "BT_OPPS_PULL_SUCCESS"

    goto :goto_0

    :sswitch_19
    const-string v0, "BT_OPPS_PULL_FAIL"

    goto :goto_0

    :sswitch_1a
    const-string v0, "BT_OPPS_DISCONNECT"

    goto :goto_0

    :sswitch_1b
    const-string v0, "BT_OPPS_PUSH_ACCESS_REQUEST"

    goto :goto_0

    :sswitch_1c
    const-string v0, "BT_OPPS_PULL_ACCESS_REQUEST"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x9 -> :sswitch_7
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0xc -> :sswitch_a
        0xd -> :sswitch_b
        0xe -> :sswitch_c
        0xf -> :sswitch_d
        0x10 -> :sswitch_e
        0x65 -> :sswitch_f
        0x66 -> :sswitch_10
        0x67 -> :sswitch_11
        0x68 -> :sswitch_12
        0x69 -> :sswitch_13
        0x6a -> :sswitch_14
        0x6b -> :sswitch_15
        0x6c -> :sswitch_16
        0x6d -> :sswitch_17
        0x6e -> :sswitch_18
        0x6f -> :sswitch_19
        0x70 -> :sswitch_1a
        0x71 -> :sswitch_1b
        0x72 -> :sswitch_1c
    .end sparse-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    instance-of v3, p1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    iget v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget v2, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    add-int/lit8 v1, v2, 0x1f

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-static {v6}, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->getEventName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
