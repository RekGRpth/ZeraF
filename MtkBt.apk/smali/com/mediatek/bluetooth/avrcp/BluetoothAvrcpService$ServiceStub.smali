.class Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;
.super Lcom/mediatek/bluetooth/avrcp/IBTAvrcpService$Stub;
.source "BluetoothAvrcpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceStub"
.end annotation


# instance fields
.field mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpService$Stub;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public connect(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->connectNative(Ljava/lang/String;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connectBrowse()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->connectBrowseNative()Z

    const/4 v0, 0x1

    return v0
.end method

.method public debugPTSAttributes(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    const/16 v0, 0xaa

    if-ne p1, v0, :cond_0

    const-string v0, "EXT_AVRCP"

    const-string v2, "[BT][AVRCP] stop server self !"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->disable()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "EXT_AVRCP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] debugPTSAttributes mode to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    sput p1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->sPTSDebugMode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "EXT_AVRCP"

    const-string v1, "[BT][AVRCP] mService not exist "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->disconnectNative()Z

    const/4 v0, 0x1

    return v0
.end method

.method public disconnectBrowse()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->disconnectBrowseNative()Z

    const/4 v0, 0x1

    return v0
.end method

.method public getStatus()B
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public selectPlayerId(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    int-to-short v0, p1

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1, v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$002(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;S)S

    const-string v2, "EXT_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] player_id:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " availFalg:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$100(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)Z

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " addresflag:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$200(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)Z

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$100(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$102(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;Z)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v1, v4, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationAvailPlayersChangedNative(BB)Z

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$200(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-static {v1, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->access$202(Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;Z)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    invoke-virtual {v1, v4, v4, v0, v4}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->notificationAddressedPlayerChangedNative(BBSS)Z

    :cond_2
    :goto_0
    return v4

    :cond_3
    const-string v1, "EXT_AVRCP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BT][AVRCP] Out of range player_id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v1, "EXT_AVRCP"

    const-string v2, "[BT][AVRCP] mService not exist "

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDebugElementAttribute()Z
    .locals 13

    const/4 v0, 0x3

    new-array v6, v0, [I

    const/4 v0, 0x0

    const/4 v1, 0x1

    aput v1, v6, v0

    const/4 v0, 0x1

    const/4 v1, 0x5

    aput v1, v6, v0

    const/4 v0, 0x2

    const/4 v1, 0x2

    aput v1, v6, v0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x0

    new-array v1, v1, [B

    const-string v2, "Test"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->connectInd([BLjava/lang/String;I)V

    const/4 v8, 0x0

    const/16 v7, 0xa

    const/4 v5, 0x3

    const/4 v8, 0x5

    const/16 v7, 0xa

    const/4 v5, 0x3

    const/4 v8, 0x0

    const/16 v7, 0xa

    const/4 v5, 0x3

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->setBrowsedplayerInd(S)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getItemAttributesInd(BJSB[I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getItemAttributesInd(BJSB[I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getItemAttributesInd(BJSB[I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x3

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getItemAttributesInd(BJSB[I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x4

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->getItemAttributesInd(BJSB[I)V

    const-wide v9, 0x7fffffffffffffffL

    const-wide/high16 v11, -0x8000000000000000L

    const-string v0, "EXT_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] uid1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v9, v10}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->changePathInd(IBJ)V

    const-string v0, "EXT_AVRCP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BT][AVRCP] uid2 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v11, v12}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService;->changePathInd(IBJ)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/avrcp/BluetoothAvrcpService$ServiceStub;->debugPTSAttributes(I)Z

    const/4 v0, 0x1

    return v0
.end method
