.class public abstract Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;
.super Landroid/os/Binder;
.source "IBluetoothHidServerNotify.java"

# interfaces
.implements Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

.field static final TRANSACTION_activateReq:I = 0x1

.field static final TRANSACTION_authorizeReq:I = 0xf

.field static final TRANSACTION_clearService:I = 0xe

.field static final TRANSACTION_connectReq:I = 0x3

.field static final TRANSACTION_deactivateReq:I = 0x2

.field static final TRANSACTION_disconnectReq:I = 0x4

.field static final TRANSACTION_finishActionReq:I = 0x10

.field static final TRANSACTION_getIdleReq:I = 0xc

.field static final TRANSACTION_getProtocolReq:I = 0xa

.field static final TRANSACTION_getReportReq:I = 0x8

.field static final TRANSACTION_getStateByAddr:I = 0xd

.field static final TRANSACTION_sendReportReq:I = 0x6

.field static final TRANSACTION_setIdleReq:I = 0xb

.field static final TRANSACTION_setProtocolReq:I = 0x9

.field static final TRANSACTION_setReportReq:I = 0x7

.field static final TRANSACTION_unplugReq:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    :sswitch_0
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->activateReq()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_2
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->deactivateReq()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_3
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->connectReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_4
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->disconnectReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_5
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->unplugReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_6
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->sendReportReq(Ljava/lang/String;Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_7
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->setReportReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_8
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->getReportReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_9
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->setProtocolReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_a
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->getProtocolReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_b
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->setIdleReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_c
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->getIdleReq(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_d
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->getStateByAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->clearService()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    move v1, v3

    :cond_1
    invoke-virtual {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->authorizeReq(Ljava/lang/String;Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_10
    const-string v4, "com.mediatek.bluetooth.hid.IBluetoothHidServerNotify"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;->finishActionReq()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
