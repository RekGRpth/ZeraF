.class public Lcom/mediatek/activity/AssembledPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "AssembledPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;
    }
.end annotation


# instance fields
.field private registeredPreference:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput-object p1, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v2, "onCreate()[+]"

    invoke-static {v2}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;

    invoke-interface {v0}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->getPreferenceResourceId()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-interface {v0, p0}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->onCreate(Landroid/preference/PreferenceActivity;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const-string v2, "onDestroy()[+]"

    invoke-static {v2}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;

    invoke-interface {v0}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->onDestroy()V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v2, "onRestoreInstanceState()[+]"

    invoke-static {v2}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;

    invoke-interface {v0, p1}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    const-string v2, "onResume()[+]"

    invoke-static {v2}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v2, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;

    invoke-interface {v0}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->onResume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v2, "onSaveInstanceState()[+]"

    invoke-static {v2}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/activity/AssembledPreferenceActivity;->registeredPreference:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;

    invoke-interface {v0, p1}, Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
