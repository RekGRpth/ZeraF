.class public Lcom/android/bluetooth/pbap/ContactStruct;
.super Ljava/lang/Object;
.source "ContactStruct.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/pbap/ContactStruct$Property;,
        Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;,
        Lcom/android/bluetooth/pbap/ContactStruct$ImData;,
        Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;,
        Lcom/android/bluetooth/pbap/ContactStruct$PostalData;,
        Lcom/android/bluetooth/pbap/ContactStruct$EmailData;,
        Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;
    }
.end annotation


# static fields
.field private static final ACCOUNT_TYPE_GOOGLE:Ljava/lang/String; = "com.google"

.field private static final GOOGLE_MY_CONTACTS_GROUP:Ljava/lang/String; = "System Group: My Contacts"

.field private static final IM_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String; = "vcard.ContactStruct"


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mBirthday:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mEmailList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$EmailData;",
            ">;"
        }
    .end annotation
.end field

.field private mFamilyName:Ljava/lang/String;

.field private mFullName:Ljava/lang/String;

.field private mGivenName:Ljava/lang/String;

.field private mImList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$ImData;",
            ">;"
        }
    .end annotation
.end field

.field private mMiddleName:Ljava/lang/String;

.field private mNickNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNoteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOrganizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneticFamilyName:Ljava/lang/String;

.field private mPhoneticFullName:Ljava/lang/String;

.field private mPhoneticGivenName:Ljava/lang/String;

.field private mPhoneticMiddleName:Ljava/lang/String;

.field private mPhotoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private mPostalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PostalData;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefIsSetAddress:Z

.field private mPrefIsSetEmail:Z

.field private mPrefIsSetOrganization:Z

.field private mPrefIsSetPhone:Z

.field private mPrefix:Ljava/lang/String;

.field private mSuffix:Ljava/lang/String;

.field private final mVCardType:I

.field private mWebsiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-AIM"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-MSN"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-YAHOO"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-ICQ"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-JABBER"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-SKYPE-USERNAME"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-GOOGLE-TALK"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    const-string v1, "X-GOOGLE TALK"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/high16 v0, -0x40000000

    invoke-direct {p0, v0}, Lcom/android/bluetooth/pbap/ContactStruct;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/bluetooth/pbap/ContactStruct;-><init>(ILandroid/accounts/Account;)V

    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mVCardType:I

    iput-object p2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$EmailData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PostalData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/high16 v1, -0x40000000

    invoke-direct {p0, v1}, Lcom/android/bluetooth/pbap/ContactStruct;-><init>(I)V

    iput-object p1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    iput-object p12, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    iput-object p13, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    return-void
.end method

.method private addEmail(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    new-instance v1, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addIm(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mImList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mImList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mImList:Ljava/util/List;

    new-instance v1, Lcom/android/bluetooth/pbap/ContactStruct$ImData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/bluetooth/pbap/ContactStruct$ImData;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addNickName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addNote(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addOrganization(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    new-instance v1, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addPhone(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    if-nez v6, :cond_0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/android/bluetooth/pbap/VCardUtils;->isAllowablePhoneNumberChar(C)Z

    move-result v6

    if-nez v6, :cond_1

    if-nez v2, :cond_2

    const/16 v6, 0x2b

    if-ne v1, v6, :cond_2

    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    new-instance v4, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, p1, v6, p3, p4}, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addPhotoBytes(Ljava/lang/String;[B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    iget-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    :cond_0
    new-instance v0, Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p2}, Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;-><init>(ILjava/lang/String;[B)V

    iget-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addPostal(ILjava/util/List;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    new-instance v1, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;-><init>(ILjava/util/List;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private constructDisplayName()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mVCardType:I

    invoke-static {v5}, Lcom/android/bluetooth/pbap/VCardConfig;->getNameOrderType(I)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    aput-object v6, v5, v11

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    :goto_0
    const/4 v1, 0x1

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_0
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/bluetooth/pbap/VCardUtils;->containsOnlyPrintableAscii(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/bluetooth/pbap/VCardUtils;->containsOnlyPrintableAscii(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    aput-object v6, v5, v11

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    :cond_2
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    aput-object v6, v5, v11

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    :sswitch_1
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    aput-object v6, v5, v11

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    :cond_3
    const/16 v5, 0x20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    :cond_5
    :goto_3
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    if-nez v5, :cond_6

    const-string v5, ""

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    :cond_6
    return-void

    :cond_7
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    goto :goto_3

    :cond_8
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    :cond_9
    iget v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mVCardType:I

    iget-object v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    invoke-static {v5, v6, v7, v8}, Lcom/android/bluetooth/pbap/VCardUtils;->constructNameFromElements(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    goto :goto_3

    :cond_a
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_b

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    iget-object v5, v5, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    goto :goto_3

    :cond_b
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_c

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;

    iget-object v5, v5, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->data:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    goto :goto_3

    :cond_c
    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;

    iget v6, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mVCardType:I

    invoke-virtual {v5, v6}, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;->getFormattedAddress(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method private handleNProperty(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v3, :cond_0

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    const/4 v0, 0x5

    :cond_2
    packed-switch v0, :pswitch_data_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handlePhoneticNameFromSound(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v3, :cond_0

    const/4 v1, 0x3

    if-le v0, v1, :cond_2

    const/4 v0, 0x3

    :cond_2
    packed-switch v0, :pswitch_data_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private listToString(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v5, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v5, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x1

    if-ge v1, v5, :cond_0

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    return-object v5

    :cond_2
    if-ne v3, v5, :cond_3

    const/4 v5, 0x0

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v5, ""

    goto :goto_1
.end method

.method private setPosition(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    :cond_0
    iget-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v2, 0x2

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/bluetooth/pbap/ContactStruct;->addOrganization(ILjava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x1

    :cond_1
    iget-object v2, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;

    iput-object p1, v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;->positionName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addProperty(Lcom/android/bluetooth/pbap/ContactStruct$Property;)V
    .locals 24
    .param p1    # Lcom/android/bluetooth/pbap/ContactStruct$Property;

    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/pbap/ContactStruct$Property;->access$000(Lcom/android/bluetooth/pbap/ContactStruct$Property;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/pbap/ContactStruct$Property;->access$100(Lcom/android/bluetooth/pbap/ContactStruct$Property;)Ljava/util/Map;

    move-result-object v9

    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/pbap/ContactStruct$Property;->access$200(Lcom/android/bluetooth/pbap/ContactStruct$Property;)Ljava/util/List;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/pbap/ContactStruct$Property;->access$300(Lcom/android/bluetooth/pbap/ContactStruct$Property;)[B

    move-result-object v11

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v21

    if-nez v21, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/bluetooth/pbap/ContactStruct;->listToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v21, "VERSION"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    const-string v21, "vcard.ContactStruct"

    const-string v22, "addProperty,propName equals VERSION"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v21, "FN"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v21, "NAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    move-object/from16 v21, v0

    if-nez v21, :cond_4

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v21, "N"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/bluetooth/pbap/ContactStruct;->handleNProperty(Ljava/util/List;)V

    goto :goto_0

    :cond_5
    const-string v21, "SORT-STRING"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFullName:Ljava/lang/String;

    goto :goto_0

    :cond_6
    const-string v21, "NICKNAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string v21, "X-NICKNAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/bluetooth/pbap/ContactStruct;->addNickName(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    const-string v21, "SOUND"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_9

    const-string v21, "X-IRMC-N"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/bluetooth/pbap/ContactStruct;->handlePhoneticNameFromSound(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_9
    const-string v21, "vcard.ContactStruct"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "addProperty,typeCollection is null,or typeCollection doesn\'t contains"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "X-IRMC-N"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    const-string v21, "ADR"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_16

    const/16 v20, 0x1

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_b

    const/16 v20, 0x0

    :cond_c
    if-nez v20, :cond_0

    const/4 v15, -0x1

    const-string v8, ""

    const/4 v6, 0x0

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_14

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_14

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v18

    const-string v21, "PREF"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetAddress:Z

    move/from16 v21, v0

    if-nez v21, :cond_d

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetAddress:Z

    const/4 v6, 0x1

    goto :goto_1

    :cond_d
    const-string v21, "HOME"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    const/4 v15, 0x1

    const-string v8, ""

    goto :goto_1

    :cond_e
    const-string v21, "WORK"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_f

    const-string v21, "COMPANY"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_10

    :cond_f
    const/4 v15, 0x2

    const-string v8, ""

    goto :goto_1

    :cond_10
    const-string v21, "PARCEL"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_11

    const-string v21, "DOM"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_11

    const-string v21, "INTL"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_12

    :cond_11
    const-string v21, "vcard.ContactStruct"

    const-string v22, "addProperty,typeString equals PARCEL or DOM or INTL"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_12
    const-string v21, "X-"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_13

    if-gez v15, :cond_13

    const/16 v21, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    :cond_13
    const/4 v15, 0x0

    move-object/from16 v8, v18

    goto/16 :goto_1

    :cond_14
    if-gez v15, :cond_15

    const/4 v15, 0x1

    :cond_15
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v14, v8, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addPostal(ILjava/util/List;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_16
    const-string v21, "EMAIL"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1e

    const/4 v15, -0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_1c

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v18

    const-string v21, "PREF"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetEmail:Z

    move/from16 v21, v0

    if-nez v21, :cond_17

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetEmail:Z

    const/4 v6, 0x1

    goto :goto_2

    :cond_17
    const-string v21, "HOME"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_18

    const/4 v15, 0x1

    goto :goto_2

    :cond_18
    const-string v21, "WORK"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_19

    const/4 v15, 0x2

    goto :goto_2

    :cond_19
    const-string v21, "CELL"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1a

    const/4 v15, 0x4

    goto :goto_2

    :cond_1a
    const-string v21, "X-"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1b

    if-gez v15, :cond_1b

    const/16 v21, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    :cond_1b
    const/4 v15, 0x0

    move-object/from16 v8, v18

    goto :goto_2

    :cond_1c
    if-gez v15, :cond_1d

    const/4 v15, 0x3

    :cond_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v8, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addEmail(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_1e
    const-string v21, "ORG"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_23

    const/4 v15, 0x1

    const/4 v6, 0x0

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_20

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1f
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_20

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string v21, "PREF"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetOrganization:Z

    move/from16 v21, v0

    if-nez v21, :cond_1f

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetOrganization:Z

    const/4 v6, 0x1

    goto :goto_3

    :cond_20
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_21
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_22

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_21

    const/16 v21, 0x20

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const-string v22, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v15, v1, v2, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addOrganization(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_23
    const-string v21, "TITLE"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_24

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/bluetooth/pbap/ContactStruct;->setPosition(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_24
    const-string v21, "ROLE"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_25

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/bluetooth/pbap/ContactStruct;->setPosition(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_25
    const-string v21, "PHOTO"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_26

    const-string v21, "LOGO"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_29

    :cond_26
    const/4 v4, 0x0

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_27

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    :cond_27
    const-string v21, "VALUE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Collection;

    if-eqz v10, :cond_28

    const-string v21, "URL"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_28

    const-string v21, "vcard.ContactStruct"

    const-string v22, "addProperty,paramMapValue is null, or paramMapValue doesn\'t contains URL"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_28
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/android/bluetooth/pbap/ContactStruct;->addPhotoBytes(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :cond_29
    const-string v21, "TEL"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2d

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/pbap/VCardUtils;->getPhoneTypeFromStrings(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    instance-of v0, v0, Ljava/lang/Integer;

    move/from16 v21, v0

    if-eqz v21, :cond_2a

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const/4 v8, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetPhone:Z

    move/from16 v21, v0

    if-nez v21, :cond_2c

    if-eqz v16, :cond_2c

    const-string v21, "PREF"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2c

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetPhone:Z

    const/4 v6, 0x1

    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v8, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addPhone(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_2a
    const/4 v15, 0x0

    if-eqz v17, :cond_2b

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_7
    goto :goto_5

    :cond_2b
    const-string v8, ""

    goto :goto_7

    :cond_2c
    const/4 v6, 0x0

    goto :goto_6

    :cond_2d
    const-string v21, "X-SKYPE-PSTNNUMBER"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2f

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    const/4 v15, 0x7

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetPhone:Z

    move/from16 v21, v0

    if-nez v21, :cond_2e

    if-eqz v16, :cond_2e

    const-string v21, "PREF"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2e

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetPhone:Z

    const/4 v6, 0x1

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13, v8, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addPhone(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_2e
    const/4 v6, 0x0

    goto :goto_8

    :cond_2f
    sget-object v21, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    move-object/from16 v0, v21

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_35

    sget-object v21, Lcom/android/bluetooth/pbap/ContactStruct;->IM_MAP:Ljava/util/Map;

    move-object/from16 v0, v21

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const/4 v6, 0x0

    const-string v21, "TYPE"

    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Collection;

    if-eqz v16, :cond_33

    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_30
    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_33

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string v21, "PREF"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_31

    const/4 v6, 0x1

    goto :goto_9

    :cond_31
    const-string v21, "HOME"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_32

    const/4 v15, 0x1

    goto :goto_9

    :cond_32
    const-string v21, "WORK"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_30

    const/4 v15, 0x3

    goto :goto_9

    :cond_33
    if-gez v15, :cond_34

    const/4 v15, 0x1

    :cond_34
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v15, v13, v1, v6}, Lcom/android/bluetooth/pbap/ContactStruct;->addIm(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_35
    const-string v21, "NOTE"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_36

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/bluetooth/pbap/ContactStruct;->addNote(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_36
    const-string v21, "URL"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    move-object/from16 v21, v0

    if-nez v21, :cond_37

    new-instance v21, Ljava/util/ArrayList;

    const/16 v22, 0x1

    invoke-direct/range {v21 .. v22}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_38
    const-string v21, "X-PHONETIC-FIRST-NAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_39

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_39
    const-string v21, "X-PHONETIC-MIDDLE-NAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3a

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3a
    const-string v21, "X-PHONETIC-LAST-NAME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3b

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3b
    const-string v21, "BDAY"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3c

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mBirthday:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3c
    const-string v21, "vcard.ContactStruct"

    const-string v22, "addProperty,propName is unknown"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public consolidateFields()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/bluetooth/pbap/ContactStruct;->constructDisplayName()V

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFullName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFullName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFullName:Ljava/lang/String;

    :cond_0
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetPhone:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;

    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->isPrimary:Z

    :cond_1
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetAddress:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;

    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;->isPrimary:Z

    :cond_2
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetEmail:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    :cond_3
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefIsSetOrganization:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;

    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;->isPrimary:Z

    :cond_4
    return-void
.end method

.method public getBirthday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/bluetooth/pbap/ContactStruct;->constructDisplayName()V

    :cond_0
    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$EmailData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    return-object v0
.end method

.method public getFamilyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mFullName:Ljava/lang/String;

    return-object v0
.end method

.method public getGivenName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    return-object v0
.end method

.method public getMiddleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getNickNameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    return-object v0
.end method

.method public final getNotes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    return-object v0
.end method

.method public final getOrganizationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    return-object v0
.end method

.method public final getPhoneList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    return-object v0
.end method

.method public getPhoneticFamilyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneticFullName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFullName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneticGivenName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneticMiddleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPhotoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    return-object v0
.end method

.method public final getPostalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/pbap/ContactStruct$PostalData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public isIgnorable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/ContactStruct;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pushIntoContentResolver(Landroid/content/ContentResolver;)V
    .locals 24
    .param p1    # Landroid/content/ContentResolver;

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_4

    const-string v2, "account_name"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "account_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "com.google"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "sourceid"

    aput-object v5, v4, v2

    const-string v5, "title=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "System Group: My Contacts"

    aput-object v7, v6, v2

    const/4 v7, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v15

    :cond_0
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mGivenName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data3"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mFamilyName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data5"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mMiddleName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data4"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPrefix:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data6"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mSuffix:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data7"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticGivenName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data9"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticFamilyName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data8"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneticMiddleName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    invoke-virtual/range {p0 .. p0}, Lcom/android/bluetooth/pbap/ContactStruct;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mNickNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/nickname"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v16

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    if-eqz v12, :cond_2

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const/4 v12, 0x0

    :cond_2
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v2

    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    :cond_4
    const-string v2, "account_name"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "account_type"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhoneList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    move-object/from16 v0, v20

    iget v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-object/from16 v0, v20

    iget v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->type:I

    if-nez v2, :cond_6

    const-string v2, "data3"

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->label:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_6
    const-string v2, "data1"

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->data:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-object/from16 v0, v20

    iget-boolean v2, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhoneData;->isPrimary:Z

    if-eqz v2, :cond_7

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_7
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    if-eqz v2, :cond_a

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mOrganizationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/organization"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    move-object/from16 v0, v19

    iget v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;->companyName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data4"

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$OrganizationData;->positionName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    if-eqz v12, :cond_9

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_9
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mEmailList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    iget v3, v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    iget v2, v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->type:I

    if-nez v2, :cond_b

    const-string v2, "data3"

    iget-object v3, v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->label:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_b
    const-string v2, "data1"

    iget-object v3, v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->data:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    iget-boolean v2, v11, Lcom/android/bluetooth/pbap/ContactStruct$EmailData;->isPrimary:Z

    if-eqz v2, :cond_c

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_c
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPostalList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/bluetooth/pbap/ContactStruct$PostalData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mVCardType:I

    move-object/from16 v0, v22

    invoke-static {v2, v8, v0}, Lcom/android/bluetooth/pbap/VCardUtils;->insertStructuredPostalDataUsingContactsStruct(ILandroid/content/ContentProviderOperation$Builder;Lcom/android/bluetooth/pbap/ContactStruct$PostalData;)V

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mImList:Ljava/util/List;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mImList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_f
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/im"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    iget v3, v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    iget v2, v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;->type:I

    if-nez v2, :cond_10

    const-string v2, "data3"

    iget-object v3, v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;->label:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_10
    const-string v2, "data1"

    iget-object v3, v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;->data:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    iget-boolean v2, v14, Lcom/android/bluetooth/pbap/ContactStruct$ImData;->isPrimary:Z

    if-eqz v2, :cond_f

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_6

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mNoteList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/note"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v17

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    if-eqz v2, :cond_14

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mPhotoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_8
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/photo"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data15"

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct$PhotoData;->photoBytes:[B

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    if-eqz v12, :cond_13

    const-string v2, "is_primary"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const/4 v12, 0x0

    :cond_13
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mWebsiteList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/website"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v23

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mBirthday:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/bluetooth/pbap/ContactStruct;->mBirthday:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_16
    if-eqz v15, :cond_17

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v2, "raw_contact_id"

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "group_sourceid"

    invoke-virtual {v8, v2, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_17
    :try_start_1
    const-string v2, "com.android.contacts"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_a
    return-void

    :catch_0
    move-exception v10

    const-string v2, "vcard.ContactStruct"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v10}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :catch_1
    move-exception v10

    const-string v2, "vcard.ContactStruct"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v10}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a
.end method
