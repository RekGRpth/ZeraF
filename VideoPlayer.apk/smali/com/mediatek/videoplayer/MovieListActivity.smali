.class public Lcom/mediatek/videoplayer/MovieListActivity;
.super Landroid/app/Activity;
.source "MovieListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;,
        Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;,
        Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;,
        Lcom/mediatek/videoplayer/MovieListActivity$DeleteTask;
    }
.end annotation


# static fields
.field private static final EXTRA_ALL_VIDEO_FOLDER:Ljava/lang/String; = "mediatek.intent.extra.ALL_VIDEO_FOLDER"

.field private static final EXTRA_ENABLE_VIDEO_LIST:Ljava/lang/String; = "mediatek.intent.extra.ENABLE_VIDEO_LIST"

.field private static final INDEX_DATA:I = 0x5

.field private static final INDEX_DATE_MODIFIED:I = 0x8

.field private static final INDEX_DISPLAY_NAME:I = 0x1

.field private static final INDEX_DRUATION:I = 0x3

.field private static final INDEX_FILE_SIZE:I = 0x6

.field private static final INDEX_ID:I = 0x0

.field private static final INDEX_IS_DRM:I = 0x7

.field private static final INDEX_MIME_TYPE:I = 0x4

.field private static final INDEX_SUPPORT_3D:I = 0x9

.field private static final INDEX_TAKEN_DATE:I = 0x2

.field private static final KEY_LOGO_BITMAP:Ljava/lang/String; = "logo-bitmap"

.field private static final KEY_TREAT_UP_AS_BACK:Ljava/lang/String; = "treat-up-as-back"

.field private static final LOG:Z = true

.field private static final MENU_DELETE_ALL:I = 0x1

.field private static final MENU_DELETE_ONE:I = 0x2

.field private static final MENU_DRM_DETAIL:I = 0x4

.field private static final MENU_PROPERTY:I = 0x3

.field private static final ORDER_COLUMN:Ljava/lang/String; = "datetaken DESC, _id DESC "

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "MovieListActivity"

.field private static final VIDEO_URI:Landroid/net/Uri;

.field private static sExternalStoragePaths:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

.field private mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

.field private mCachedVideoInfo:Lcom/mediatek/videoplayer/CachedVideoInfo;

.field private mDefaultDrawable:Landroid/graphics/Bitmap;

.field private mDefaultOverlay3D:Landroid/graphics/Bitmap;

.field private mEmptyView:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field private mNoSdView:Landroid/view/ViewGroup;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private final mStorageListener:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/mediatek/videoplayer/MovieListActivity;->VIDEO_URI:Landroid/net/Uri;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_drm"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "stereo_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/videoplayer/MovieListActivity;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/videoplayer/MovieListActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videoplayer/MovieListActivity$1;-><init>(Lcom/mediatek/videoplayer/MovieListActivity;)V

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mStorageListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/videoplayer/MovieListActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/videoplayer/MovieListActivity;->refreshSdStatus(Z)V

    return-void
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/videoplayer/MovieListActivity;->sExternalStoragePaths:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/videoplayer/MovieListActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showEmpty()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/videoplayer/MovieListActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showList()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/videoplayer/MovieListActivity;)Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/videoplayer/MovieListActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideScanningProgress()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/videoplayer/MovieListActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/videoplayer/MovieListActivity;->showDeleteProgress(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/videoplayer/MovieListActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideDeleteProgress()V

    return-void
.end method

.method static synthetic access$600()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/mediatek/videoplayer/MovieListActivity;->VIDEO_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/videoplayer/MovieListActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultDrawable:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/videoplayer/MovieListActivity;)Lcom/mediatek/videoplayer/CachedVideoInfo;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedVideoInfo:Lcom/mediatek/videoplayer/CachedVideoInfo;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/videoplayer/MovieListActivity;)Lcom/mediatek/videoplayer/CachedThumbnail;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->getCachedManager()Lcom/mediatek/videoplayer/CachedThumbnail;

    move-result-object v0

    return-object v0
.end method

.method private getCachedManager()Lcom/mediatek/videoplayer/CachedThumbnail;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultDrawable:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    invoke-static {p0, v0, v1}, Lcom/mediatek/videoplayer/CachedThumbnail;->getCachedManager(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/videoplayer/CachedThumbnail;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0, v1}, Lcom/mediatek/videoplayer/CachedThumbnail;->addListener(Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    return-object v0
.end method

.method private hideDeleteProgress()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideProgress()V

    return-void
.end method

.method private hideProgress()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private hideScanningProgress()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideProgress()V

    return-void
.end method

.method private refreshMovieList()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;->getQueryHandler()Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter$QueryHandler;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/videoplayer/MovieListActivity;->VIDEO_URI:Landroid/net/Uri;

    sget-object v4, Lcom/mediatek/videoplayer/MovieListActivity;->PROJECTION:[Ljava/lang/String;

    const-string v7, "datetaken DESC, _id DESC "

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private refreshSdStatus(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "MovieListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshSdStatus("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/videoplayer/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    invoke-static {p0}, Lcom/mediatek/videoplayer/MtkUtils;->isMediaScanning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showScanningProgress()V

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showList()V

    invoke-static {p0}, Lcom/mediatek/videoplayer/MtkUtils;->disableSpinnerState(Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideScanningProgress()V

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showList()V

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->refreshMovieList()V

    invoke-static {p0}, Lcom/mediatek/videoplayer/MtkUtils;->enableSpinnerState(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->hideScanningProgress()V

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->showSdcardLost()V

    invoke-static {p0}, Lcom/mediatek/videoplayer/MtkUtils;->disableSpinnerState(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private registerStorageListener()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mStorageListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private showDelete(Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V
    .locals 6
    .param p1    # Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    const/4 v5, 0x1

    const-string v0, "MovieListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showDelete("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/videoplayer/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f04000a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f04000b

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mTitle:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/mediatek/videoplayer/MovieListActivity$3;

    invoke-direct {v2, p0, p1}, Lcom/mediatek/videoplayer/MovieListActivity$3;-><init>(Lcom/mediatek/videoplayer/MovieListActivity;Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private showDeleteProgress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/videoplayer/MovieListActivity;->showProgress(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private showDetail(Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V
    .locals 2
    .param p1    # Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    new-instance v0, Lcom/mediatek/videoplayer/DetailDialog;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/videoplayer/DetailDialog;-><init>(Landroid/content/Context;Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V

    const v1, 0x7f040009

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private showEmpty()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mEmptyView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mNoSdView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showList()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mNoSdView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showProgress(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/DialogInterface$OnCancelListener;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v2, :cond_0

    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p2}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private showScanningProgress()V
    .locals 2

    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/mediatek/videoplayer/MovieListActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/videoplayer/MovieListActivity$2;-><init>(Lcom/mediatek/videoplayer/MovieListActivity;)V

    invoke-direct {p0, v0, v1}, Lcom/mediatek/videoplayer/MovieListActivity;->showProgress(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private showSdcardLost()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mNoSdView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget-object v4, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    const/4 v0, 0x0

    instance-of v4, v2, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    if-eqz v4, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_1
    return v3

    :pswitch_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->clone()Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/videoplayer/MovieListActivity;->showDelete(Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V

    goto :goto_1

    :cond_2
    const-string v4, "MovieListActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wrong context item info "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->clone()Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/videoplayer/MovieListActivity;->showDetail(Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V

    goto :goto_1

    :cond_3
    const-string v4, "MovieListActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wrong context item info "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_2
    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mediatek/videoplayer/MtkUtils;->isSupportDrm()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mData:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/mediatek/videoplayer/MtkUtils;->showDrmDetails(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v3, "MovieListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "wrong context item info "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const-string v0, "storage"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/storage/StorageManager;

    invoke-virtual {v7}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/videoplayer/MovieListActivity;->sExternalStoragePaths:[Ljava/lang/String;

    const v0, 0x102000a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    const v0, 0x1020004

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mEmptyView:Landroid/widget/TextView;

    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mNoSdView:Landroid/view/ViewGroup;

    new-instance v0, Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    const v3, 0x7f030002

    const/4 v4, 0x0

    new-array v5, v1, [Ljava/lang/String;

    new-array v6, v1, [I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;-><init>(Lcom/mediatek/videoplayer/MovieListActivity;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/mediatek/videoplayer/MovieListActivity;->registerStorageListener()V

    invoke-static {p0}, Lcom/mediatek/videoplayer/MtkUtils;->isMediaMounted(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/videoplayer/MovieListActivity;->refreshSdStatus(Z)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020003

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultDrawable:Landroid/graphics/Bitmap;

    new-instance v0, Lcom/mediatek/videoplayer/CachedVideoInfo;

    invoke-direct {v0}, Lcom/mediatek/videoplayer/CachedVideoInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedVideoInfo:Lcom/mediatek/videoplayer/CachedVideoInfo;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020001

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    const-string v0, "MovieListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mDefaultDrawable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mDefaultDrawable:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/videoplayer/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "onCreate()"

    invoke-static {v0}, Lcom/mediatek/videoplayer/MtkUtils;->logMemory(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v5, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    move-object v1, p3

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget-object v3, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    const/4 v0, 0x0

    instance-of v3, v2, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    if-eqz v3, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v3, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mTitle:Ljava/lang/String;

    invoke-interface {p1, v3}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    const/4 v3, 0x2

    const v4, 0x7f04000a

    invoke-interface {p1, v5, v3, v5, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v3, 0x3

    const v4, 0x7f040009

    invoke-interface {p1, v5, v3, v5, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/videoplayer/MtkUtils;->isSupportDrm()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mIsDrm:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x4

    const v4, 0x2050062

    invoke-interface {p1, v5, v3, v5, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;->clearCachedHolder()V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0, v2}, Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0, v1}, Lcom/mediatek/videoplayer/CachedThumbnail;->removeListener(Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;)Z

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/CachedThumbnail;->clearCachedPreview()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedVideoInfo:Lcom/mediatek/videoplayer/CachedVideoInfo;

    invoke-virtual {v0, v2}, Lcom/mediatek/videoplayer/CachedVideoInfo;->setLocale(Ljava/util/Locale;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mStorageListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "onDestroy()"

    invoke-static {v0}, Lcom/mediatek/videoplayer/MtkUtils;->logMemory(Ljava/lang/String;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v8, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    const/4 v1, 0x0

    instance-of v5, v4, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    if-eqz v5, :cond_1

    move-object v1, v4

    check-cast v1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "video/*"

    iget-object v5, v1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    if-eqz v5, :cond_0

    const-string v5, ""

    iget-object v6, v1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v3, v1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    :cond_0
    sget-object v5, Lcom/mediatek/videoplayer/MovieListActivity;->VIDEO_URI:Landroid/net/Uri;

    iget-wide v6, v1, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mId:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "mediatek.intent.extra.ALL_VIDEO_FOLDER"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "treat-up-as-back"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "mediatek.intent.extra.ENABLE_VIDEO_LIST"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "logo-bitmap"

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020002

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    const-string v5, "MovieListActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onItemClick("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") holder="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/videoplayer/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mAdapter:Lcom/mediatek/videoplayer/MovieListActivity$MovieListAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity;->mCachedVideoInfo:Lcom/mediatek/videoplayer/CachedVideoInfo;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/videoplayer/CachedVideoInfo;->setLocale(Ljava/util/Locale;)V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method
