.class Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "MovieListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videoplayer/MovieListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FastBitmapDrawable"
.end annotation


# static fields
.field private static final LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "FastBitmapDrawable"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mHeight:I

.field private final mWidth:I

.field final synthetic this$0:Lcom/mediatek/videoplayer/MovieListActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/videoplayer/MovieListActivity;II)V
    .locals 0
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->this$0:Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    iput p2, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mWidth:I

    iput p3, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mHeight:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mHeight:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mWidth:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1    # Landroid/graphics/ColorFilter;

    return-void
.end method
