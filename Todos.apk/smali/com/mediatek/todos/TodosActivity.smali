.class public Lcom/mediatek/todos/TodosActivity;
.super Landroid/app/Activity;
.source "TodosActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;,
        Lcom/mediatek/todos/TodosActivity$AdapterViewListener;
    }
.end annotation


# static fields
.field private static final DIALOG_DELETE_ITEMS:I = 0x1

.field private static final REQUEST_ADD_NEW:I = 0x1

.field private static final REQUEST_SHOW_DETAILS:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TodosActivity"


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mAdapterViewListener:Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

.field private mBtnChangeState:Landroid/view/MenuItem;

.field private mBtnDelete:Landroid/view/MenuItem;

.field private mBtnDeselectAll:Landroid/view/MenuItem;

.field private mBtnSelectAll:Landroid/view/MenuItem;

.field private mNumberTextView:Landroid/widget/TextView;

.field private mShowingDialog:Z

.field private mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

.field private mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

.field private mTodosListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/todos/TodosActivity;->mShowingDialog:Z

    iput-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    iput-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    iput-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mAdapterViewListener:Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

    iput-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/todos/TodosActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/todos/TodosActivity;->mShowingDialog:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->onChangeItemStateClick()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->updateToEditNull()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->updateActionModeTitle()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->startTodoActionMode()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/todos/TodosActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/mediatek/todos/TodosActivity;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mBtnChangeState:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity;->mBtnChangeState:Landroid/view/MenuItem;

    return-object p1
.end method

.method static synthetic access$602(Lcom/mediatek/todos/TodosActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$702(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity;->mBtnSelectAll:Landroid/view/MenuItem;

    return-object p1
.end method

.method static synthetic access$802(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDeselectAll:Landroid/view/MenuItem;

    return-object p1
.end method

.method static synthetic access$902(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/mediatek/todos/TodosActivity;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDelete:Landroid/view/MenuItem;

    return-object p1
.end method

.method private configureActionBar()V
    .locals 5

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0a0011

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mNumberTextView:Landroid/widget/TextView;

    return-void
.end method

.method private getSelectedCount()I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedTodosNumber()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedDonesNumber()I

    move-result v0

    goto :goto_0

    :cond_1
    const-string v1, "TodosActivity"

    const-string v2, "mTodosListAdapter.getEditType():+mTodosListAdapter.getEditType(),may be has error."

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initViews()V
    .locals 2

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodosListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    new-instance v0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;-><init>(Lcom/mediatek/todos/TodosActivity;)V

    iput-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mAdapterViewListener:Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mAdapterViewListener:Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mAdapterViewListener:Lcom/mediatek/todos/TodosActivity$AdapterViewListener;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    return-void
.end method

.method private onChangeItemStateClick()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v0, "1"

    const-string v1, "2"

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2, v0, v1}, Lcom/mediatek/todos/TodosListAdapter;->updateSelectedStatus(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->updateToEditNull()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const-string v0, "2"

    const-string v1, "1"

    goto :goto_0
.end method

.method private startTodoActionMode()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;-><init>(Lcom/mediatek/todos/TodosActivity;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    :cond_0
    return-void
.end method

.method private updateActionModeTitle()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->getSelectedCount()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v2, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateToEditNull()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodosListAdapter;->setEditingType(I)Z

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v3, "TodosActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult request="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v3, "data_passed"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v3, v1}, Lcom/mediatek/todos/TodosListAdapter;->addItem(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v3, v1}, Lcom/mediatek/todos/TodosListAdapter;->getItemPosition(Lcom/mediatek/todos/TodoInfo;)I

    move-result v0

    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :pswitch_1
    const-string v3, "data_passed"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    if-nez v1, :cond_0

    const-string v3, "TodosActivity"

    const-string v4, "result: OPERATOR_UPDATE, but data didn\'t contain the info"

    invoke-static {v3, v4}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v3, v1}, Lcom/mediatek/todos/TodosListAdapter;->updateItemData(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v3, v1}, Lcom/mediatek/todos/TodosListAdapter;->getItemPosition(Lcom/mediatek/todos/TodoInfo;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListView:Landroid/widget/ListView;

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    const-string v3, "data_passed"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    if-nez v1, :cond_1

    const-string v3, "TodosActivity"

    const-string v4, "result: OPERATOR_DELETE, but data didn\'t contain the info"

    invoke-static {v3, v4}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v3, v1}, Lcom/mediatek/todos/TodosListAdapter;->removeItem(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodosListAdapter;->isEditing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->updateToEditNull()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->initViews()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->configureActionBar()V

    invoke-static {p0}, Lcom/mediatek/todos/TimeChangeReceiver;->registTimeChangeReceiver(Landroid/content/Context;)Lcom/mediatek/todos/TimeChangeReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TimeChangeReceiver;->addDateChangeListener(Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;)V

    const-string v0, "TodosActivity"

    const-string v1, "TodosActivity.onCreate() finished."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f07000b

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07000e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x2

    const v2, 0x7f07000c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/mediatek/todos/TodosActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/todos/TodosActivity$1;-><init>(Lcom/mediatek/todos/TodosActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v1, -0x1

    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/mediatek/todos/TodosActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/todos/TodosActivity$2;-><init>(Lcom/mediatek/todos/TodosActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v1, Lcom/mediatek/todos/TodosActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/todos/TodosActivity$3;-><init>(Lcom/mediatek/todos/TodosActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    move-object v1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f090002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-virtual {v0}, Lcom/mediatek/todos/TimeChangeReceiver;->clearChangeListener()V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v2

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0010
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosActivity;->mShowingDialog:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->removeDialog(I)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/app/Dialog;

    const v5, 0x7f07000e

    const v4, 0x7f07000d

    const/4 v3, 0x1

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, ""

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedTodosNumber()I

    move-result v1

    if-le v1, v3, :cond_1

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedDonesNumber()I

    move-result v1

    if-le v1, v3, :cond_3

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosActivity;->mShowingDialog:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    return-void
.end method

.method updateBottomBarWidgetState()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v2, "TodosActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateBottomBarWidgetState(), editing="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v4}, Lcom/mediatek/todos/TodosListAdapter;->isEditing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->isEditing()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v2

    if-ne v2, v5, :cond_2

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedTodosNumber()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getTodosDataSource()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    :cond_0
    :goto_0
    const-string v2, "TodosActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectedNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", dataSourceNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/todos/TodosActivity;->updateToEditNull()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getSeletedDonesNumber()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mTodosListAdapter:Lcom/mediatek/todos/TodosListAdapter;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getDonesDataSource()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_3
    if-lez v1, :cond_4

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDeselectAll:Landroid/view/MenuItem;

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnChangeState:Landroid/view/MenuItem;

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDelete:Landroid/view/MenuItem;

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_2
    if-ne v1, v0, :cond_5

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnSelectAll:Landroid/view/MenuItem;

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDeselectAll:Landroid/view/MenuItem;

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnChangeState:Landroid/view/MenuItem;

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnDelete:Landroid/view/MenuItem;

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity;->mBtnSelectAll:Landroid/view/MenuItem;

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public updateHeaderNumberText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity;->mNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
