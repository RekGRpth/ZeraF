.class Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;
.super Ljava/lang/Object;
.source "TodosActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/TodosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TodosActionModeCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/todos/TodosActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)V

    return v2

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/todos/TodosListAdapter;->allSelectAction(Z)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodosListAdapter;->allSelectAction(Z)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0}, Lcom/mediatek/todos/TodosActivity;->access$1000(Lcom/mediatek/todos/TodosActivity;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0, v2}, Lcom/mediatek/todos/TodosActivity;->access$002(Lcom/mediatek/todos/TodosActivity;Z)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0013
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0, p1}, Lcom/mediatek/todos/TodosActivity;->access$602(Lcom/mediatek/todos/TodosActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    const v1, 0x7f0a0018

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/TodosActivity;->access$502(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    const v1, 0x7f0a0016

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/TodosActivity;->access$702(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    const v1, 0x7f0a0017

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/TodosActivity;->access$802(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    const v1, 0x7f0a0013

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/TodosActivity;->access$902(Lcom/mediatek/todos/TodosActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1    # Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/todos/TodosActivity;->access$602(Lcom/mediatek/todos/TodosActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodosActivity;->onBackPressed()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/mediatek/todos/TodosActivity$TodosActionModeCallBack;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v0}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)V

    const/4 v0, 0x1

    return v0
.end method
