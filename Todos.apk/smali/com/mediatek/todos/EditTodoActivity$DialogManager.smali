.class Lcom/mediatek/todos/EditTodoActivity$DialogManager;
.super Ljava/lang/Object;
.source "EditTodoActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/EditTodoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DialogManager"
.end annotation


# instance fields
.field private mIsAnyDialogShown:Z

.field final synthetic this$0:Lcom/mediatek/todos/EditTodoActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/todos/EditTodoActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/todos/EditTodoActivity$DialogManager;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/todos/EditTodoActivity$DialogManager;->mIsAnyDialogShown:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/todos/EditTodoActivity;Lcom/mediatek/todos/EditTodoActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/todos/EditTodoActivity;
    .param p2    # Lcom/mediatek/todos/EditTodoActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/todos/EditTodoActivity$DialogManager;-><init>(Lcom/mediatek/todos/EditTodoActivity;)V

    return-void
.end method


# virtual methods
.method public isAnyDialogShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/todos/EditTodoActivity$DialogManager;->mIsAnyDialogShown:Z

    return v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/todos/EditTodoActivity$DialogManager;->mIsAnyDialogShown:Z

    return-void
.end method

.method public setDialogShown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/todos/EditTodoActivity$DialogManager;->mIsAnyDialogShown:Z

    return-void
.end method
