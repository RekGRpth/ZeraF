.class Lcom/mediatek/todos/TodosListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TodosListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/TodosListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolder"
.end annotation


# instance fields
.field mChangeInfoState:Landroid/widget/ImageView;

.field mTodoInfoCheckBox:Landroid/widget/CheckBox;

.field mTodoInfoDueDate:Landroid/widget/TextView;

.field mTodoInfoState:Landroid/widget/ImageView;

.field mTodoInfoText:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoState:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoDueDate:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    return-void
.end method
