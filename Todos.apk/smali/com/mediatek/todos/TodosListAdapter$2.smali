.class Lcom/mediatek/todos/TodosListAdapter$2;
.super Ljava/lang/Object;
.source "TodosListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/TodosListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/mediatek/todos/TodoInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/todos/TodosListAdapter;


# direct methods
.method constructor <init>(Lcom/mediatek/todos/TodosListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/todos/TodosListAdapter$2;->this$0:Lcom/mediatek/todos/TodosListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/mediatek/todos/TodoInfo;Lcom/mediatek/todos/TodoInfo;)I
    .locals 6
    .param p1    # Lcom/mediatek/todos/TodoInfo;
    .param p2    # Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v2

    const/4 v4, 0x0

    cmp-long v5, v0, v2

    if-gez v5, :cond_1

    const/4 v4, 0x1

    :cond_0
    :goto_0
    return v4

    :cond_1
    cmp-long v5, v0, v2

    if-lez v5, :cond_0

    const/4 v4, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/mediatek/todos/TodoInfo;

    check-cast p2, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/todos/TodosListAdapter$2;->compare(Lcom/mediatek/todos/TodoInfo;Lcom/mediatek/todos/TodoInfo;)I

    move-result v0

    return v0
.end method
