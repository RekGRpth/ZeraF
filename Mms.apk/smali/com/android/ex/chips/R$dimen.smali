.class public final Lcom/android/ex/chips/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final avatar_width_height:I = 0x7f090003

.field public static final bottom_panel_min_height:I = 0x7f09000b

.field public static final chip_height:I = 0x7f09001b

.field public static final chip_padding:I = 0x7f09001a

.field public static final chip_text_size:I = 0x7f09001c

.field public static final emoticon_bound_size:I = 0x7f090012

.field public static final emoticon_preview_height:I = 0x7f09000f

.field public static final emoticon_preview_land_offset:I = 0x7f090010

.field public static final emoticon_preview_port_offset:I = 0x7f090011

.field public static final emoticon_preview_width:I = 0x7f09000e

.field public static final group_photo_width:I = 0x7f090016

.field public static final img_minwidth:I = 0x7f090014

.field public static final input_text_height:I = 0x7f090004

.field public static final input_text_height_adjusted:I = 0x7f090005

.field public static final ipmsg_message_list_divier_height:I = 0x7f090015

.field public static final line_spacing_extra:I = 0x7f09001d

.field public static final longpress_conversationlist_margin:I = 0x7f090019

.field public static final message_item_avatar_on_right_text_indent:I = 0x7f090000

.field public static final message_item_text_padding_left_right:I = 0x7f090001

.field public static final message_item_text_padding_top:I = 0x7f090002

.field public static final mms_inline_attachment_size:I = 0x7f090006

.field public static final share_panel_common_lan_height:I = 0x7f090017

.field public static final share_panel_common_port_height:I = 0x7f090018

.field public static final share_panel_lan_height:I = 0x7f09000c

.field public static final share_panel_port_height:I = 0x7f09000d

.field public static final shortcut_bound_size:I = 0x7f090013

.field public static final widget_margin_bottom:I = 0x7f09000a

.field public static final widget_margin_left:I = 0x7f090008

.field public static final widget_margin_right:I = 0x7f090009

.field public static final widget_margin_top:I = 0x7f090007


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
