.class public Lcom/android/mms/MmsPluginManager;
.super Ljava/lang/Object;
.source "MmsPluginManager.java"


# static fields
.field public static final MMS_PLUGIN_TYPE_APPLICATION_GUIDE:I = 0x4

.field public static final MMS_PLUGIN_TYPE_DIALOG_NOTIFY:I = 0x1

.field public static final MMS_PLUGIN_TYPE_MMS_ATTACHMENT_ENHANCE:I = 0x5

.field public static final MMS_PLUGIN_TYPE_MMS_COMPOSE:I = 0x8

.field public static final MMS_PLUGIN_TYPE_MMS_TRANSACTION:I = 0x7

.field public static final MMS_PLUGIN_TYPE_SMS_RECEIVER:I = 0x3

.field public static final MMS_PLUGIN_TYPE_TEXT_SIZE_ADJUST:I = 0x2

.field private static TAG:Ljava/lang/String;

.field private static mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;

.field private static mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

.field private static mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

.field private static mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

.field private static mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

.field private static mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

.field private static mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "MmsPluginManager"

    sput-object v0, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMmsPluginObject(I)Ljava/lang/Object;
    .locals 4
    .param p0    # I

    const/4 v0, 0x0

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMmsPlugin, type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMmsPlugin, type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " don\'t exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static initPlugins(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    :try_start_0
    const-class v1, Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsDialogNotify;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsDialogNotifyPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/util/AndroidException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    const-class v1, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsTextSizeAdjustPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/util/AndroidException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-class v1, Lcom/mediatek/mms/ext/ISmsReceiver;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/ISmsReceiver;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mSmsReceiverPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/util/AndroidException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    const-class v1, Lcom/mediatek/mms/ext/IAppGuideExt;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IAppGuideExt;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;
    :try_end_3
    .catch Landroid/util/AndroidException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    const-class v1, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsAttachmentEnhancePlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/util/AndroidException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    const-class v1, Lcom/mediatek/mms/ext/IMmsTransaction;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsTransaction;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsTransactionPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/util/AndroidException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    const-class v1, Lcom/mediatek/mms/ext/IMmsCompose;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsCompose;

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsComposePlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/util/AndroidException; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsComposePlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsDialogNotifyImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/MmsDialogNotifyImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsDialogNotifyPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsDialogNotifyPlugin:Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/MmsTextSizeAdjustImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsTextSizeAdjustPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/SmsReceiverImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/SmsReceiverImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mSmsReceiverPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mSmsReceiverPlugin:Lcom/mediatek/mms/ext/ISmsReceiver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/DefaultAppGuideExt;

    invoke-direct {v1}, Lcom/mediatek/mms/ext/DefaultAppGuideExt;-><init>()V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mAppGuideExt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mAppGuideExt:Lcom/mediatek/mms/ext/IAppGuideExt;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :catch_4
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsAttachmentEnhanceImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/MmsAttachmentEnhanceImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsAttachmentEnhancePlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsAttachmentEnhancePlugin:Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :catch_5
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsTransactionImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/MmsTransactionImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    sget-object v1, Lcom/android/mms/MmsPluginManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsTransactionPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsPluginManager;->mMmsTransactionPlugin:Lcom/mediatek/mms/ext/IMmsTransaction;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_6
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsComposeImpl;

    invoke-direct {v1, p0}, Lcom/mediatek/mms/ext/MmsComposeImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/mms/MmsPluginManager;->mMmsComposePlugin:Lcom/mediatek/mms/ext/IMmsCompose;

    goto/16 :goto_6
.end method
