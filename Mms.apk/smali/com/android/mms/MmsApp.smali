.class public Lcom/android/mms/MmsApp;
.super Landroid/app/Application;
.source "MmsApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/MmsApp$ToastHandler;
    }
.end annotation


# static fields
.field public static final EVENT_QUIT:I = 0x64

.field public static final LOG_TAG:Ljava/lang/String; = "Mms"

.field public static final MSG_DONE:I = 0xc

.field public static final MSG_MMS_CAN_NOT_OPEN:I = 0xa

.field public static final MSG_MMS_CAN_NOT_SAVE:I = 0x8

.field public static final MSG_MMS_TOO_BIG_TO_DOWNLOAD:I = 0x6

.field public static final MSG_RETRIEVE_FAILURE_DEVICE_MEMORY_FULL:I = 0x2

.field public static final MSG_SHOW_TRANSIENTLY_FAILED_NOTIFICATION:I = 0x4

.field public static final TXN_TAG:Ljava/lang/String; = "Mms/Txn"

.field private static mToastHandler:Lcom/android/mms/MmsApp$ToastHandler;

.field private static mToastthread:Landroid/os/HandlerThread;

.field private static sMmsApp:Lcom/android/mms/MmsApp;


# instance fields
.field private mCountryDetector:Landroid/location/CountryDetector;

.field private mCountryIso:Ljava/lang/String;

.field private mCountryListener:Landroid/location/CountryListener;

.field private mDrmManagerClient:Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

.field private mPduLoaderManager:Lcom/android/mms/util/PduLoaderManager;

.field private mRecentSuggestions:Landroid/provider/SearchRecentSuggestions;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mThumbnailManager:Lcom/android/mms/util/ThumbnailManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/mms/MmsApp;->sMmsApp:Lcom/android/mms/MmsApp;

    sput-object v0, Lcom/android/mms/MmsApp;->mToastthread:Landroid/os/HandlerThread;

    sput-object v0, Lcom/android/mms/MmsApp;->mToastHandler:Lcom/android/mms/MmsApp$ToastHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private InitToastThread()V
    .locals 1

    sget-object v0, Lcom/android/mms/MmsApp;->mToastHandler:Lcom/android/mms/MmsApp$ToastHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mms/MmsApp$ToastHandler;

    invoke-direct {v0, p0}, Lcom/android/mms/MmsApp$ToastHandler;-><init>(Lcom/android/mms/MmsApp;)V

    sput-object v0, Lcom/android/mms/MmsApp;->mToastHandler:Lcom/android/mms/MmsApp$ToastHandler;

    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/android/mms/MmsApp;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/mms/MmsApp;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mms/MmsApp;->mCountryIso:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100()Lcom/android/mms/MmsApp;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsApp;->sMmsApp:Lcom/android/mms/MmsApp;

    return-object v0
.end method

.method public static declared-synchronized getApplication()Lcom/android/mms/MmsApp;
    .locals 2

    const-class v0, Lcom/android/mms/MmsApp;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/android/mms/MmsApp;->sMmsApp:Lcom/android/mms/MmsApp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getToastHandler()Lcom/android/mms/MmsApp$ToastHandler;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsApp;->mToastHandler:Lcom/android/mms/MmsApp$ToastHandler;

    return-object v0
.end method


# virtual methods
.method public getCurrentCountryIso()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/android/mms/MmsApp;->mCountryIso:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/MmsApp;->mCountryDetector:Landroid/location/CountryDetector;

    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/MmsApp;->mCountryIso:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/android/mms/MmsApp;->mCountryIso:Ljava/lang/String;

    return-object v1
.end method

.method public getDrmManagerClient()Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;
    .locals 2

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mDrmManagerClient:Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/MmsApp;->mDrmManagerClient:Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

    :cond_0
    iget-object v0, p0, Lcom/android/mms/MmsApp;->mDrmManagerClient:Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

    return-object v0
.end method

.method public getPduLoaderManager()Lcom/android/mms/util/PduLoaderManager;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mPduLoaderManager:Lcom/android/mms/util/PduLoaderManager;

    return-object v0
.end method

.method public getRecentSuggestions()Landroid/provider/SearchRecentSuggestions;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mRecentSuggestions:Landroid/provider/SearchRecentSuggestions;

    return-object v0
.end method

.method public getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/mms/MmsApp;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    :cond_0
    iget-object v0, p0, Lcom/android/mms/MmsApp;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method public getThumbnailManager()Lcom/android/mms/util/ThumbnailManager;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mThumbnailManager:Lcom/android/mms/util/ThumbnailManager;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    invoke-static {}, Lcom/android/mms/layout/LayoutManager;->getInstance()Lcom/android/mms/layout/LayoutManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/mms/layout/LayoutManager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    const-string v2, "Mms/Txn"

    const-string v3, "MmsApp.onCreate"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Mms:strictmode"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_0
    sput-object p0, Lcom/android/mms/MmsApp;->sMmsApp:Lcom/android/mms/MmsApp;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getIpMessagePlugin(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mms/ipmessage/ServiceManager;->startIpService()V

    invoke-static {p0}, Lcom/android/mms/util/SmileyParser2;->init(Landroid/content/Context;)V

    const v2, 0x7f05000b

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    const-string v2, "country_detector"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/CountryDetector;

    iput-object v2, p0, Lcom/android/mms/MmsApp;->mCountryDetector:Landroid/location/CountryDetector;

    new-instance v2, Lcom/android/mms/MmsApp$1;

    invoke-direct {v2, p0}, Lcom/android/mms/MmsApp$1;-><init>(Lcom/android/mms/MmsApp;)V

    iput-object v2, p0, Lcom/android/mms/MmsApp;->mCountryListener:Landroid/location/CountryListener;

    iget-object v2, p0, Lcom/android/mms/MmsApp;->mCountryDetector:Landroid/location/CountryDetector;

    iget-object v3, p0, Lcom/android/mms/MmsApp;->mCountryListener:Landroid/location/CountryListener;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/location/CountryDetector;->addCountryListener(Landroid/location/CountryListener;Landroid/os/Looper;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lcom/android/mms/util/PduLoaderManager;

    invoke-direct {v2, v0}, Lcom/android/mms/util/PduLoaderManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/mms/MmsApp;->mPduLoaderManager:Lcom/android/mms/util/PduLoaderManager;

    new-instance v2, Lcom/android/mms/util/ThumbnailManager;

    invoke-direct {v2, v0}, Lcom/android/mms/util/ThumbnailManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/mms/MmsApp;->mThumbnailManager:Lcom/android/mms/util/ThumbnailManager;

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/MmsPluginManager;->initPlugins(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/data/Contact;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/util/DraftCache;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/data/Conversation;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/util/DownloadManager;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/util/RateController;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/layout/LayoutManager;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/util/SmileyParser;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/transaction/MessagingNotification;->init(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/mms/MmsApp;->InitToastThread()V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getFolderModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/android/mms/util/MuteCache;->init(Landroid/content/Context;)V

    :cond_1
    const/4 v2, 0x7

    invoke-static {v2}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsTransaction;

    invoke-interface {v1}, Lcom/mediatek/mms/ext/IMmsTransaction;->isRestartPendingsEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/mms/transaction/TransactionService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_0
    const-string v2, "MmsApp.onCreate"

    invoke-static {v0, v2}, Lcom/android/mms/MmsConfig;->printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-static {v0}, Lcom/android/mms/transaction/MmsSystemEventReceiver;->setPendingMmsFailed(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mPduLoaderManager:Lcom/android/mms/util/PduLoaderManager;

    invoke-virtual {v0}, Lcom/android/mms/util/PduLoaderManager;->onLowMemory()V

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mThumbnailManager:Lcom/android/mms/util/ThumbnailManager;

    invoke-virtual {v0}, Lcom/android/mms/util/ThumbnailManager;->onLowMemory()V

    return-void
.end method

.method public onTerminate()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/MmsApp;->mCountryDetector:Landroid/location/CountryDetector;

    iget-object v1, p0, Lcom/android/mms/MmsApp;->mCountryListener:Landroid/location/CountryListener;

    invoke-virtual {v0, v1}, Landroid/location/CountryDetector;->removeCountryListener(Landroid/location/CountryListener;)V

    return-void
.end method
