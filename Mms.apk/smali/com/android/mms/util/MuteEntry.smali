.class Lcom/android/mms/util/MuteEntry;
.super Ljava/lang/Object;
.source "MuteCache.java"


# instance fields
.field threadMute:J

.field threadMuteStart:J

.field threadNotificationEnabled:Z


# direct methods
.method public constructor <init>(JJZ)V
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/mms/util/MuteEntry;->threadMute:J

    iput-wide v0, p0, Lcom/android/mms/util/MuteEntry;->threadMuteStart:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/util/MuteEntry;->threadNotificationEnabled:Z

    iput-wide p1, p0, Lcom/android/mms/util/MuteEntry;->threadMute:J

    iput-wide p3, p0, Lcom/android/mms/util/MuteEntry;->threadMuteStart:J

    iput-boolean p5, p0, Lcom/android/mms/util/MuteEntry;->threadNotificationEnabled:Z

    return-void
.end method
