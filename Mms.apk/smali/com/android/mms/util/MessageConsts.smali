.class public Lcom/android/mms/util/MessageConsts;
.super Ljava/lang/Object;
.source "MessageConsts.java"


# static fields
.field public static final ACTION_SHARE:I

.field public static final defaultIconArr:[I

.field public static final emoticonIdList:[I

.field public static final giftIconArr:[I

.field public static final ipmsgShareIconArr:[I

.field public static final shareIconArr:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/mms/util/MessageConsts;->shareIconArr:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/mms/util/MessageConsts;->ipmsgShareIconArr:[I

    const/16 v0, 0x46

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/mms/util/MessageConsts;->emoticonIdList:[I

    const/16 v0, 0x28

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/mms/util/MessageConsts;->defaultIconArr:[I

    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/mms/util/MessageConsts;->giftIconArr:[I

    return-void

    :array_0
    .array-data 4
        0x7f02011e
        0x7f020115
        0x7f020116
        0x7f02011a
        0x7f020103
        0x7f020104
        0x7f020105
        0x7f020119
        0x7f0200f7
    .end array-data

    :array_1
    .array-data 4
        0x7f02011e
        0x7f020115
        0x7f020116
        0x7f020109
        0x7f020103
        0x7f020104
        0x7f020105
        0x7f02011b
        0x7f02011a
        0x7f020119
        0x7f0200f7
    .end array-data

    :array_2
    .array-data 4
        0x7f020055
        0x7f020056
        0x7f020057
        0x7f020058
        0x7f020059
        0x7f02005a
        0x7f02005b
        0x7f02005c
        0x7f02005d
        0x7f02005e
        0x7f02005f
        0x7f020060
        0x7f020061
        0x7f020062
        0x7f020063
        0x7f020064
        0x7f020065
        0x7f020066
        0x7f020067
        0x7f020068
        0x7f020069
        0x7f02006a
        0x7f02006b
        0x7f02006c
        0x7f02006d
        0x7f02006e
        0x7f02006f
        0x7f020070
        0x7f020071
        0x7f020072
        0x7f020073
        0x7f020074
        0x7f020075
        0x7f020076
        0x7f020077
        0x7f020078
        0x7f020079
        0x7f02007a
        0x7f02007b
        0x7f02007c
        0x7f020089
        0x7f02013b
        0x7f02013e
        0x7f020172
        0x7f02014c
        0x7f020035
        0x7f020148
        0x7f020121
        0x7f020162
        0x7f020134
        0x7f02002a
        0x7f02003b
        0x7f020022
        0x7f020011
        0x7f020085
        0x7f02015b
        0x7f020092
        0x7f020012
        0x7f02001e
        0x7f02013a
        0x7f020153
        0x7f020084
        0x7f020149
        0x7f02014a
        0x7f020082
        0x7f02000b
        0x7f020170
        0x7f020023
        0x7f020142
        0x7f020143
    .end array-data

    :array_3
    .array-data 4
        0x7f020055
        0x7f020056
        0x7f020057
        0x7f020058
        0x7f020059
        0x7f02005a
        0x7f02005b
        0x7f02005c
        0x7f02005d
        0x7f02005e
        0x7f02005f
        0x7f020060
        0x7f020061
        0x7f020062
        0x7f020063
        0x7f020064
        0x7f020065
        0x7f020066
        0x7f020067
        0x7f020068
        0x7f020069
        0x7f02006a
        0x7f02006b
        0x7f02006c
        0x7f02006d
        0x7f02006e
        0x7f02006f
        0x7f020070
        0x7f020071
        0x7f020072
        0x7f020073
        0x7f020074
        0x7f020075
        0x7f020076
        0x7f020077
        0x7f020078
        0x7f020079
        0x7f02007a
        0x7f02007b
        0x7f02007c
    .end array-data

    :array_4
    .array-data 4
        0x7f020089
        0x7f02013b
        0x7f02013e
        0x7f020172
        0x7f02014c
        0x7f020035
        0x7f020148
        0x7f020121
        0x7f020162
        0x7f020134
        0x7f02002a
        0x7f02003b
        0x7f020022
        0x7f020011
        0x7f020085
        0x7f02015b
        0x7f020092
        0x7f020012
        0x7f02001e
        0x7f02013a
        0x7f020153
        0x7f020084
        0x7f020149
        0x7f02014a
        0x7f020082
        0x7f02000b
        0x7f020170
        0x7f020023
        0x7f020142
        0x7f020143
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
