.class public Lcom/android/mms/util/PduLoaderManager$PduLoaded;
.super Ljava/lang/Object;
.source "PduLoaderManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/util/PduLoaderManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PduLoaded"
.end annotation


# instance fields
.field public final mPdu:Lcom/google/android/mms/pdu/GenericPdu;

.field public final mSlideshow:Lcom/android/mms/model/SlideshowModel;


# direct methods
.method public constructor <init>(Lcom/google/android/mms/pdu/GenericPdu;Lcom/android/mms/model/SlideshowModel;)V
    .locals 0
    .param p1    # Lcom/google/android/mms/pdu/GenericPdu;
    .param p2    # Lcom/android/mms/model/SlideshowModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/util/PduLoaderManager$PduLoaded;->mPdu:Lcom/google/android/mms/pdu/GenericPdu;

    iput-object p2, p0, Lcom/android/mms/util/PduLoaderManager$PduLoaded;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    return-void
.end method
