.class public final Lcom/android/mms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final LinearLayout01:I = 0x7f0f016c

.field public static final LinearLayout02:I = 0x7f0f016e

.field public static final account_spinner:I = 0x7f0f00b1

.field public static final account_spinner_list:I = 0x7f0f00b4

.field public static final action_btn:I = 0x7f0f00e3

.field public static final action_cell_broadcasts:I = 0x7f0f019d

.field public static final action_compose_new:I = 0x7f0f0195

.field public static final action_debug_dump:I = 0x7f0f019e

.field public static final action_delete_all:I = 0x7f0f019a

.field public static final action_omacp:I = 0x7f0f019c

.field public static final action_select_all:I = 0x7f0f01ab

.field public static final action_settings:I = 0x7f0f019b

.field public static final action_siminfo:I = 0x7f0f0199

.field public static final action_wappush:I = 0x7f0f019f

.field public static final add_shortcut:I = 0x7f0f01a2

.field public static final album_name:I = 0x7f0f0013

.field public static final artist_name:I = 0x7f0f0014

.field public static final attach_Name:I = 0x7f0f00be

.field public static final attachment:I = 0x7f0f006b

.field public static final attachment_editor:I = 0x7f0f0032

.field public static final attachment_editor_scroll_view:I = 0x7f0f0031

.field public static final attachment_icon:I = 0x7f0f0167

.field public static final attachment_name:I = 0x7f0f0166

.field public static final audio:I = 0x7f0f008a

.field public static final audio_attachment_view:I = 0x7f0f0010

.field public static final audio_attachment_view_stub:I = 0x7f0f0036

.field public static final audio_downLoad_progress:I = 0x7f0f00e9

.field public static final audio_error_msg:I = 0x7f0f0015

.field public static final audio_icon:I = 0x7f0f010a

.field public static final audio_image:I = 0x7f0f0011

.field public static final audio_info:I = 0x7f0f00e8

.field public static final audio_name:I = 0x7f0f0012

.field public static final avatar:I = 0x7f0f004e

.field public static final background:I = 0x7f0f0053

.field public static final body:I = 0x7f0f0027

.field public static final body_scroll:I = 0x7f0f0025

.field public static final body_text_view:I = 0x7f0f0144

.field public static final bottom_panel:I = 0x7f0f0038

.field public static final bottom_text:I = 0x7f0f0110

.field public static final boxname:I = 0x7f0f00b2

.field public static final bt_chat_select:I = 0x7f0f0024

.field public static final bt_invite_negative:I = 0x7f0f004d

.field public static final bt_invite_postive:I = 0x7f0f004c

.field public static final btn_activate:I = 0x7f0f0062

.field public static final btn_download_msg:I = 0x7f0f0100

.field public static final btn_load_all_message:I = 0x7f0f00c8

.field public static final btn_no:I = 0x7f0f0052

.field public static final btn_yes:I = 0x7f0f0051

.field public static final buttonPanel:I = 0x7f0f0050

.field public static final button_and_counter:I = 0x7f0f0129

.field public static final button_with_counter:I = 0x7f0f003e

.field public static final buttons_linear:I = 0x7f0f012a

.field public static final by_card:I = 0x7f0f00bb

.field public static final cancel:I = 0x7f0f0048

.field public static final cancel_button:I = 0x7f0f012d

.field public static final cancel_select:I = 0x7f0f01a7

.field public static final caption_separator:I = 0x7f0f0122

.field public static final cbmsg_list_item_recv:I = 0x7f0f001b

.field public static final changed_linear_layout:I = 0x7f0f002a

.field public static final checkbox:I = 0x7f0f0132

.field public static final contact_img:I = 0x7f0f0111

.field public static final content_linear:I = 0x7f0f0117

.field public static final content_scroll_view:I = 0x7f0f0116

.field public static final content_selector:I = 0x7f0f000e

.field public static final content_size:I = 0x7f0f00e4

.field public static final controler:I = 0x7f0f008d

.field public static final conv_root:I = 0x7f0f0073

.field public static final conversation_list:I = 0x7f0f0187

.field public static final conversation_list_name:I = 0x7f0f0066

.field public static final conversation_list_spinner:I = 0x7f0f0065

.field public static final create_group_chat:I = 0x7f0f0196

.field public static final custom_title_root:I = 0x7f0f0063

.field public static final date:I = 0x7f0f006f

.field public static final date_lock:I = 0x7f0f00b9

.field public static final date_view:I = 0x7f0f001f

.field public static final default_panel:I = 0x7f0f00a0

.field public static final delete:I = 0x7f0f0049

.field public static final delete_btn:I = 0x7f0f012c

.field public static final delete_locked:I = 0x7f0f007d

.field public static final delete_panel:I = 0x7f0f0045

.field public static final delivered_indicator:I = 0x7f0f0020

.field public static final delivery_date:I = 0x7f0f0084

.field public static final detail_content_linear:I = 0x7f0f0118

.field public static final details_indicator:I = 0x7f0f0021

.field public static final divider_1:I = 0x7f0f0047

.field public static final divider_2:I = 0x7f0f0152

.field public static final done:I = 0x7f0f0097

.field public static final done_button:I = 0x7f0f0094

.field public static final downLoad_progress:I = 0x7f0f00fc

.field public static final draft:I = 0x7f0f0069

.field public static final drm_audio_lock:I = 0x7f0f008c

.field public static final drm_imagevideo_lock:I = 0x7f0f008b

.field public static final drm_lock:I = 0x7f0f0016

.field public static final drop_down_menu_text:I = 0x7f0f0078

.field public static final duration_text:I = 0x7f0f0168

.field public static final edit_slideshow_button:I = 0x7f0f015f

.field public static final embedded_reply_text_editor:I = 0x7f0f0127

.field public static final embedded_text_editor:I = 0x7f0f003c

.field public static final emoticon_bottom_panel:I = 0x7f0f004f

.field public static final emoticon_button:I = 0x7f0f003a

.field public static final emoticon_panel:I = 0x7f0f0098

.field public static final emoticon_panel_zone:I = 0x7f0f0099

.field public static final empty:I = 0x7f0f0075

.field public static final empty2:I = 0x7f0f0074

.field public static final empty_message:I = 0x7f0f0151

.field public static final error:I = 0x7f0f006a

.field public static final expiration_indicator:I = 0x7f0f018e

.field public static final export:I = 0x7f0f01a0

.field public static final ffwd:I = 0x7f0f00d0

.field public static final file_attachment_button_remove:I = 0x7f0f00b0

.field public static final file_attachment_divider:I = 0x7f0f00af

.field public static final file_attachment_name_info:I = 0x7f0f00ad

.field public static final file_attachment_size_info:I = 0x7f0f00ae

.field public static final file_attachment_thumbnail:I = 0x7f0f00ac

.field public static final file_attachment_view:I = 0x7f0f00ab

.field public static final file_attachment_view_stub:I = 0x7f0f0033

.field public static final folderview_cancel_select:I = 0x7f0f01a5

.field public static final folderview_delete:I = 0x7f0f01a6

.field public static final folderview_select_all:I = 0x7f0f01a4

.field public static final forwallpaperchooser:I = 0x7f0f017d

.field public static final forward:I = 0x7f0f0190

.field public static final from:I = 0x7f0f006d

.field public static final gallery:I = 0x7f0f017a

.field public static final gif_content:I = 0x7f0f00f3

.field public static final gift_panel:I = 0x7f0f00a2

.field public static final group_mms_sender:I = 0x7f0f011a

.field public static final group_sender:I = 0x7f0f0119

.field public static final gv_ad_emoticon_gridview:I = 0x7f0f0004

.field public static final gv_default_emoticon_gridview:I = 0x7f0f0079

.field public static final gv_dynamic_emoticon_gridview:I = 0x7f0f0086

.field public static final gv_gift_emoticon_gridview:I = 0x7f0f00c1

.field public static final gv_large_emoticon_gridview:I = 0x7f0f00c7

.field public static final gv_normal_emoticon_gridview:I = 0x7f0f0139

.field public static final gv_share_gridview:I = 0x7f0f014c

.field public static final gv_xm_emoticon_gridview:I = 0x7f0f018f

.field public static final has_file_attachment:I = 0x7f0f00bd

.field public static final history:I = 0x7f0f002e

.field public static final icon:I = 0x7f0f0080

.field public static final icon_id:I = 0x7f0f00b6

.field public static final iconlist:I = 0x7f0f0068

.field public static final image:I = 0x7f0f0089

.field public static final image_attachment_view:I = 0x7f0f00c3

.field public static final image_attachment_view_stub:I = 0x7f0f0034

.field public static final image_content:I = 0x7f0f00c4

.field public static final image_downLoad_progress:I = 0x7f0f00e5

.field public static final image_preview:I = 0x7f0f0163

.field public static final image_size_bg:I = 0x7f0f00e2

.field public static final image_view:I = 0x7f0f0103

.field public static final img_location:I = 0x7f0f00f0

.field public static final important:I = 0x7f0f0192

.field public static final important_group:I = 0x7f0f0191

.field public static final important_indicator:I = 0x7f0f00dc

.field public static final invitation_linear:I = 0x7f0f004a

.field public static final ip_audio:I = 0x7f0f00e6

.field public static final ip_audio_icon:I = 0x7f0f00e7

.field public static final ip_emoticon:I = 0x7f0f00f2

.field public static final ip_image:I = 0x7f0f00e0

.field public static final ip_location:I = 0x7f0f00ef

.field public static final ip_message_thumbnail:I = 0x7f0f003b

.field public static final ip_message_typing_status:I = 0x7f0f0030

.field public static final ip_message_typing_status_view:I = 0x7f0f002f

.field public static final ip_vcalendar:I = 0x7f0f00ed

.field public static final ip_vcard:I = 0x7f0f00eb

.field public static final ip_vcard_icon:I = 0x7f0f00fd

.field public static final ipmsg_accept:I = 0x7f0f00f5

.field public static final ipmsg_download_file_cancel:I = 0x7f0f00f9

.field public static final ipmsg_download_file_progress:I = 0x7f0f00fa

.field public static final ipmsg_download_file_size:I = 0x7f0f00f8

.field public static final ipmsg_file_download:I = 0x7f0f00f7

.field public static final ipmsg_file_downloading_controller_view:I = 0x7f0f00f4

.field public static final ipmsg_icon:I = 0x7f0f0155

.field public static final ipmsg_reject:I = 0x7f0f00f6

.field public static final ipmsg_resend:I = 0x7f0f00fe

.field public static final itemDivider:I = 0x7f0f0085

.field public static final item_list:I = 0x7f0f0131

.field public static final iv_emoticon_icon:I = 0x7f0f007b

.field public static final iv_emoticon_img:I = 0x7f0f00aa

.field public static final iv_large_emoticon_icon:I = 0x7f0f0006

.field public static final iv_share_icon:I = 0x7f0f014e

.field public static final iv_silent:I = 0x7f0f0001

.field public static final label:I = 0x7f0f0095

.field public static final label_downloading:I = 0x7f0f0101

.field public static final left_text:I = 0x7f0f010c

.field public static final line:I = 0x7f0f0149

.field public static final ll_emoticon_click_zone:I = 0x7f0f007a

.field public static final ll_empty_activate:I = 0x7f0f0060

.field public static final ll_empty_groupchat:I = 0x7f0f0059

.field public static final ll_empty_important:I = 0x7f0f005d

.field public static final ll_empty_spam:I = 0x7f0f0055

.field public static final ll_large_item:I = 0x7f0f0005

.field public static final ll_share_flipper:I = 0x7f0f0003

.field public static final ll_share_item:I = 0x7f0f014d

.field public static final load_all_message_view:I = 0x7f0f00de

.field public static final loading_text:I = 0x7f0f018c

.field public static final location_addr:I = 0x7f0f00f1

.field public static final locked_indicator:I = 0x7f0f0022

.field public static final mark_as_nonspam:I = 0x7f0f01a3

.field public static final mark_as_read_btn:I = 0x7f0f012b

.field public static final mark_as_spam:I = 0x7f0f01a1

.field public static final media_size_info:I = 0x7f0f0017

.field public static final mediacontroller_progress:I = 0x7f0f00cb

.field public static final menu_select_all:I = 0x7f0f01a9

.field public static final menu_select_cancel:I = 0x7f0f01aa

.field public static final message:I = 0x7f0f007c

.field public static final message_block:I = 0x7f0f013f

.field public static final message_count:I = 0x7f0f00b3

.field public static final message_delete:I = 0x7f0f01ae

.field public static final message_forward:I = 0x7f0f01ad

.field public static final messages:I = 0x7f0f0150

.field public static final mms_download_controls:I = 0x7f0f00ff

.field public static final mms_downloading_view_stub:I = 0x7f0f00dd

.field public static final mms_file_attachment_view_stub:I = 0x7f0f00db

.field public static final mms_layout_view_parent:I = 0x7f0f001d

.field public static final mms_layout_view_stub:I = 0x7f0f00d9

.field public static final mms_view:I = 0x7f0f0102

.field public static final msg_content:I = 0x7f0f011b

.field public static final msg_counter:I = 0x7f0f0114

.field public static final msg_date:I = 0x7f0f00ba

.field public static final msg_dlg_image_view:I = 0x7f0f011d

.field public static final msg_dlg_mms_view:I = 0x7f0f011c

.field public static final msg_dlg_play_slideshow_button:I = 0x7f0f011e

.field public static final msg_list_item_recv:I = 0x7f0f00d2

.field public static final msg_list_item_send:I = 0x7f0f00fb

.field public static final msg_recipent:I = 0x7f0f00b8

.field public static final msg_recv_timer:I = 0x7f0f0125

.field public static final msg_subject:I = 0x7f0f00bc

.field public static final msg_text:I = 0x7f0f00bf

.field public static final mute:I = 0x7f0f006c

.field public static final name:I = 0x7f0f0134

.field public static final navigation_bar:I = 0x7f0f0136

.field public static final new_quick_text:I = 0x7f0f016f

.field public static final next:I = 0x7f0f00cf

.field public static final next_slide_button:I = 0x7f0f008f

.field public static final no_internet_text:I = 0x7f0f0077

.field public static final no_itnernet_view:I = 0x7f0f0076

.field public static final number:I = 0x7f0f00b7

.field public static final number_picker:I = 0x7f0f013a

.field public static final on_line_divider:I = 0x7f0f00df

.field public static final on_line_divider_str:I = 0x7f0f013b

.field public static final page_divider1:I = 0x7f0f0107

.field public static final page_divider2:I = 0x7f0f0109

.field public static final page_dividers:I = 0x7f0f013d

.field public static final page_index:I = 0x7f0f0108

.field public static final panel_container:I = 0x7f0f0043

.field public static final pause:I = 0x7f0f00ce

.field public static final play_audio_button:I = 0x7f0f0018

.field public static final play_slideshow_button:I = 0x7f0f0104

.field public static final playing_audio:I = 0x7f0f013c

.field public static final pre_slide_button:I = 0x7f0f008e

.field public static final presence:I = 0x7f0f0067

.field public static final prev:I = 0x7f0f00cd

.field public static final preview_button:I = 0x7f0f0090

.field public static final previous:I = 0x7f0f0115

.field public static final progress:I = 0x7f0f012f

.field public static final progress_percent:I = 0x7f0f0130

.field public static final qcb_avatar:I = 0x7f0f005a

.field public static final quickText_add_button:I = 0x7f0f0170

.field public static final quick_text:I = 0x7f0f013e

.field public static final quick_text_list:I = 0x7f0f016d

.field public static final rb_dot_first:I = 0x7f0f009b

.field public static final rb_dot_forth:I = 0x7f0f009e

.field public static final rb_dot_sec:I = 0x7f0f009c

.field public static final rb_dot_third:I = 0x7f0f009d

.field public static final recepient_bar_relative:I = 0x7f0f0112

.field public static final recepient_name:I = 0x7f0f0113

.field public static final recipient:I = 0x7f0f0082

.field public static final recipients_editor:I = 0x7f0f0140

.field public static final recipients_editor_stub:I = 0x7f0f002c

.field public static final recipients_picker:I = 0x7f0f0141

.field public static final recipients_subject_linear:I = 0x7f0f002b

.field public static final remove_audio_button:I = 0x7f0f001a

.field public static final remove_image_button:I = 0x7f0f00c6

.field public static final remove_important:I = 0x7f0f0194

.field public static final remove_important_group:I = 0x7f0f0193

.field public static final remove_slide_button:I = 0x7f0f0092

.field public static final remove_slideshow_button:I = 0x7f0f0161

.field public static final remove_video_button:I = 0x7f0f0179

.field public static final replace_audio_button:I = 0x7f0f0019

.field public static final replace_image_button:I = 0x7f0f0091

.field public static final replace_video_button:I = 0x7f0f0178

.field public static final reply_linear:I = 0x7f0f0126

.field public static final reply_send_button:I = 0x7f0f0128

.field public static final resend_item:I = 0x7f0f0142

.field public static final rew:I = 0x7f0f00d1

.field public static final rg_share_dot:I = 0x7f0f009a

.field public static final right_text:I = 0x7f0f010f

.field public static final save:I = 0x7f0f01a8

.field public static final search:I = 0x7f0f0197

.field public static final select_all:I = 0x7f0f0046

.field public static final select_check_box:I = 0x7f0f001c

.field public static final select_items:I = 0x7f0f012e

.field public static final selected_conv_count:I = 0x7f0f0071

.field public static final selectedid:I = 0x7f0f0148

.field public static final selection_cancel:I = 0x7f0f0137

.field public static final selection_done:I = 0x7f0f0138

.field public static final selection_menu:I = 0x7f0f0072

.field public static final send_button:I = 0x7f0f003d

.field public static final send_button_ipmsg:I = 0x7f0f0042

.field public static final send_button_mms:I = 0x7f0f0040

.field public static final send_button_sms:I = 0x7f0f0041

.field public static final send_invitations:I = 0x7f0f0198

.field public static final send_slideshow_button:I = 0x7f0f0160

.field public static final sender_name:I = 0x7f0f00d7

.field public static final sender_name_separator:I = 0x7f0f00d8

.field public static final sender_photo:I = 0x7f0f00d6

.field public static final set:I = 0x7f0f017b

.field public static final share_button:I = 0x7f0f0039

.field public static final share_panel:I = 0x7f0f0044

.field public static final share_panel_main:I = 0x7f0f014a

.field public static final share_panel_zone:I = 0x7f0f014b

.field public static final sim3g:I = 0x7f0f000a

.field public static final simCardPicid:I = 0x7f0f0146

.field public static final simCardTexid:I = 0x7f0f0147

.field public static final simIcon:I = 0x7f0f0008

.field public static final simName:I = 0x7f0f000c

.field public static final simNumber:I = 0x7f0f000d

.field public static final simNumberShort:I = 0x7f0f000b

.field public static final simStatus:I = 0x7f0f0009

.field public static final sim_divider:I = 0x7f0f00d3

.field public static final sim_icon:I = 0x7f0f0154

.field public static final sim_info_layout:I = 0x7f0f0007

.field public static final sim_info_linear:I = 0x7f0f0123

.field public static final sim_layout:I = 0x7f0f0153

.field public static final sim_name:I = 0x7f0f0124

.field public static final sim_number:I = 0x7f0f0158

.field public static final sim_number_short:I = 0x7f0f0156

.field public static final sim_status:I = 0x7f0f0023

.field public static final sim_suggested:I = 0x7f0f0159

.field public static final siminfolist:I = 0x7f0f015a

.field public static final size:I = 0x7f0f0135

.field public static final slide_editor_scroll_view:I = 0x7f0f0088

.field public static final slide_editor_view:I = 0x7f0f0087

.field public static final slide_item_list:I = 0x7f0f0105

.field public static final slide_number_text:I = 0x7f0f0164

.field public static final slide_page:I = 0x7f0f0106

.field public static final slide_view:I = 0x7f0f015b

.field public static final slidegroup:I = 0x7f0f01ac

.field public static final slideshow_attachment_view:I = 0x7f0f015c

.field public static final slideshow_attachment_view_portrait:I = 0x7f0f0162

.field public static final slideshow_attachment_view_stub:I = 0x7f0f0037

.field public static final slideshow_image:I = 0x7f0f015d

.field public static final slideshow_text:I = 0x7f0f015e

.field public static final smiley_icon:I = 0x7f0f0169

.field public static final smiley_name:I = 0x7f0f016a

.field public static final smiley_panel_ad_btn:I = 0x7f0f00a7

.field public static final smiley_panel_btn_group:I = 0x7f0f009f

.field public static final smiley_panel_default_btn:I = 0x7f0f00a1

.field public static final smiley_panel_del_btn:I = 0x7f0f00a4

.field public static final smiley_panel_dynamic_btn:I = 0x7f0f00a8

.field public static final smiley_panel_gift_btn:I = 0x7f0f00a3

.field public static final smiley_panel_large_btn:I = 0x7f0f00a9

.field public static final smiley_panel_normal_btn:I = 0x7f0f00a5

.field public static final smiley_panel_xm_btn:I = 0x7f0f00a6

.field public static final smiley_text:I = 0x7f0f016b

.field public static final sms_viewer_title:I = 0x7f0f00c0

.field public static final spinner_line_2:I = 0x7f0f00b5

.field public static final status:I = 0x7f0f0083

.field public static final status_bar_latest_event_content:I = 0x7f0f0171

.field public static final status_bar_latest_event_content_large_icon:I = 0x7f0f0172

.field public static final subject:I = 0x7f0f002d

.field public static final subtitle:I = 0x7f0f0145

.field public static final term_textview:I = 0x7f0f000f

.field public static final text:I = 0x7f0f0096

.field public static final text1:I = 0x7f0f00c2

.field public static final text_caption:I = 0x7f0f00ea

.field public static final text_counter:I = 0x7f0f003f

.field public static final text_expire:I = 0x7f0f00da

.field public static final text_layout:I = 0x7f0f0157

.field public static final text_load_all_message:I = 0x7f0f00c9

.field public static final text_message:I = 0x7f0f0093

.field public static final text_preview:I = 0x7f0f0165

.field public static final text_view:I = 0x7f0f001e

.field public static final thumbnail:I = 0x7f0f0133

.field public static final time:I = 0x7f0f00cc

.field public static final time_current:I = 0x7f0f00ca

.field public static final time_divider:I = 0x7f0f00d4

.field public static final time_divider_str:I = 0x7f0f0173

.field public static final title:I = 0x7f0f0070

.field public static final titleDivider:I = 0x7f0f0081

.field public static final title_template:I = 0x7f0f007f

.field public static final title_text_view:I = 0x7f0f0143

.field public static final topPanel:I = 0x7f0f007e

.field public static final top_text:I = 0x7f0f010b

.field public static final tv_activate_content:I = 0x7f0f0061

.field public static final tv_avatar:I = 0x7f0f0056

.field public static final tv_empty_content:I = 0x7f0f0054

.field public static final tv_from:I = 0x7f0f0057

.field public static final tv_group_from:I = 0x7f0f005b

.field public static final tv_group_subject:I = 0x7f0f005c

.field public static final tv_invite_msg:I = 0x7f0f004b

.field public static final tv_nms_content:I = 0x7f0f005e

.field public static final tv_nms_flag:I = 0x7f0f005f

.field public static final tv_share_name:I = 0x7f0f014f

.field public static final tv_subject:I = 0x7f0f0058

.field public static final tv_top_subtitle:I = 0x7f0f0002

.field public static final tv_top_title:I = 0x7f0f0000

.field public static final unread:I = 0x7f0f006e

.field public static final unread_conv_count:I = 0x7f0f0064

.field public static final unread_divider:I = 0x7f0f00d5

.field public static final unread_divider_str:I = 0x7f0f0174

.field public static final vca_dlg_image_view:I = 0x7f0f011f

.field public static final vca_image_view:I = 0x7f0f0120

.field public static final vcalendar_info:I = 0x7f0f00ee

.field public static final vcard_info:I = 0x7f0f00ec

.field public static final video:I = 0x7f0f010d

.field public static final video_attachment_view:I = 0x7f0f0175

.field public static final video_attachment_view_stub:I = 0x7f0f0035

.field public static final video_media_paly:I = 0x7f0f00e1

.field public static final video_media_play:I = 0x7f0f0121

.field public static final video_play_overlay:I = 0x7f0f010e

.field public static final video_thumbnail:I = 0x7f0f0176

.field public static final view_image_button:I = 0x7f0f00c5

.field public static final view_parent:I = 0x7f0f0026

.field public static final view_video_button:I = 0x7f0f0177

.field public static final wall_paper:I = 0x7f0f0029

.field public static final wallpaper_chooser_fragment:I = 0x7f0f017c

.field public static final wallpaper_image:I = 0x7f0f017f

.field public static final wallpaperchooserdialog:I = 0x7f0f017e

.field public static final wallpaperitemeachimageview:I = 0x7f0f0180

.field public static final wallpaperitemeachtextview:I = 0x7f0f0181

.field public static final widget_compose:I = 0x7f0f0186

.field public static final widget_conversation:I = 0x7f0f0188

.field public static final widget_header:I = 0x7f0f0182

.field public static final widget_icon:I = 0x7f0f0183

.field public static final widget_label:I = 0x7f0f0184

.field public static final widget_loading:I = 0x7f0f018b

.field public static final widget_read_background:I = 0x7f0f018a

.field public static final widget_unread_background:I = 0x7f0f0189

.field public static final widget_unread_count:I = 0x7f0f0185

.field public static final wpms_list_item:I = 0x7f0f018d

.field public static final wpms_timestamp:I = 0x7f0f0028


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
