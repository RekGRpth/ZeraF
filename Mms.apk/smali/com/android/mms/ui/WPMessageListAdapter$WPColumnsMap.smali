.class public Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;
.super Ljava/lang/Object;
.source "WPMessageListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WPColumnsMap"
.end annotation


# instance fields
.field public mColumnMsgId:I

.field public mColumnWpmsAction:I

.field public mColumnWpmsAddr:I

.field public mColumnWpmsCreate:I

.field public mColumnWpmsDate:I

.field public mColumnWpmsError:I

.field public mColumnWpmsExpiration:I

.field public mColumnWpmsLocked:I

.field public mColumnWpmsRead:I

.field public mColumnWpmsServiceAddr:I

.field public mColumnWpmsSiid:I

.field public mColumnWpmsSimId:I

.field public mColumnWpmsText:I

.field public mColumnWpmsThreadId:I

.field public mColumnWpmsType:I

.field public mColumnWpmsURL:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnMsgId:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsThreadId:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAddr:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsServiceAddr:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsRead:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsDate:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsType:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsSiid:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsURL:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsCreate:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsExpiration:I

    const/16 v0, 0xb

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAction:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsText:I

    const/16 v0, 0xd

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsSimId:I

    const/16 v0, 0xe

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsLocked:I

    const/16 v0, 0xf

    iput v0, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsError:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnMsgId:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    const-string v1, "thread_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsThreadId:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-string v1, "address"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAddr:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    const-string v1, "service_center"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsServiceAddr:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    const-string v1, "read"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsRead:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    const-string v1, "date"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsDate:I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    const-string v1, "type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsType:I
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    const-string v1, "siid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsSiid:I
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    :try_start_8
    const-string v1, "url"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsURL:I
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_8

    :goto_8
    :try_start_9
    const-string v1, "created"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsCreate:I
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_9

    :goto_9
    :try_start_a
    const-string v1, "expiration"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsExpiration:I
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_a

    :goto_a
    :try_start_b
    const-string v1, "action"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsAction:I
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_b

    :goto_b
    :try_start_c
    const-string v1, "text"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsText:I
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_c

    :goto_c
    :try_start_d
    const-string v1, "sim_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsSimId:I
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_d

    :goto_d
    :try_start_e
    const-string v1, "locked"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsLocked:I
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_e

    :goto_e
    :try_start_f
    const-string v1, "error"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mms/ui/WPMessageListAdapter$WPColumnsMap;->mColumnWpmsError:I
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_f

    :goto_f
    return-void

    :catch_0
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :catch_4
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :catch_5
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_6
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :catch_7
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :catch_8
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :catch_9
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :catch_a
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :catch_b
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    :catch_c
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    :catch_d
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    :catch_e
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    :catch_f
    move-exception v0

    const-string v1, "colsMap"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f
.end method
