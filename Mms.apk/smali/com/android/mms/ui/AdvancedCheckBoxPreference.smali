.class public Lcom/android/mms/ui/AdvancedCheckBoxPreference;
.super Landroid/preference/CheckBoxPreference;
.source "AdvancedCheckBoxPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;
    }
.end annotation


# static fields
.field private static final SHOW_NUMBER_LENGTH:I = 0x4

.field private static final TAG:Ljava/lang/String; = "AdvancedCheckBoxPreference"

.field private static sSim3G:Landroid/widget/TextView;

.field private static sSimColor:Landroid/widget/ImageView;

.field private static sSimName:Landroid/widget/TextView;

.field private static sSimNumber:Landroid/widget/TextView;

.field private static sSimNumberShort:Landroid/widget/TextView;

.field private static sSimStatus:Landroid/widget/ImageView;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentId:I

.field private mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mms/ui/AdvancedCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x101008f

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mms/ui/AdvancedCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    sget-object v1, Lcom/android/internal/R$styleable;->CheckBoxPreference:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    move-object v0, p1

    check-cast v0, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iput-object v0, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iput-object p1, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/16 v6, 0x8

    const/4 v5, 0x4

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    const v2, 0x7f0f000c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimName:Landroid/widget/TextView;

    const v2, 0x7f0f000d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumber:Landroid/widget/TextView;

    const v2, 0x7f0f000b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    const v2, 0x7f0f0009

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimStatus:Landroid/widget/ImageView;

    const v2, 0x7f0f0008

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimColor:Landroid/widget/ImageView;

    const v2, 0x7f0f000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSim3G:Landroid/widget/TextView;

    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v4, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v3, v4}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimName(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimNumber(I)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimNumber(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumber:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimNumber(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getNumberFormat(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_3

    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimStatus(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/mms/ui/MessageUtils;->getSimStatusResource(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v2, v1, :cond_1

    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimStatus:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimColor:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v4, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v3, v4}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSim3G:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumber:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v4, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v3, v4}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getSimNumber(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iget v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mCurrentId:I

    invoke-interface {v2, v3}, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;->getNumberFormat(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_5

    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    sget-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimName:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumber:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimNumberShort:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimStatus:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSimColor:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->sSim3G:Landroid/widget/TextView;

    return-object v0
.end method

.method public setNotifyChange(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    check-cast p1, Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    iput-object p1, p0, Lcom/android/mms/ui/AdvancedCheckBoxPreference;->mSimInfo:Lcom/android/mms/ui/AdvancedCheckBoxPreference$GetSimInfo;

    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    return-void
.end method
