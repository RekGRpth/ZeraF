.class Lcom/android/mms/ui/ComposeMessageActivity$78$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$78;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$78;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4400(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/ui/RecipientsEditor;->getNumbers()Ljava/util/List;

    move-result-object v3

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15800(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$78;->val$mmsLimitCount:I

    mul-int/lit8 v5, v5, 0x2

    if-le v4, v5, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->val$mmsLimitCount:I

    if-ge v0, v4, :cond_1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/ContactList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/data/ContactList;->getNumbers()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/mms/data/WorkingMessage;->setWorkingRecipients(Ljava/util/List;)V

    :goto_2
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15900(Lcom/android/mms/ui/ComposeMessageActivity;)V

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/mms/data/ContactList;->getByNumbers(Ljava/lang/Iterable;Z)Lcom/android/mms/data/ContactList;

    move-result-object v1

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5300(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/android/mms/data/ContactList;)V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4702(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/android/mms/data/ContactList;)Lcom/android/mms/data/ContactList;

    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15800(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    :goto_3
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->val$mmsLimitCount:I

    if-lt v0, v4, :cond_3

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$78$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$78;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$78;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/mms/data/WorkingMessage;->setWorkingRecipients(Ljava/util/List;)V

    move-object v2, v3

    goto :goto_2
.end method
