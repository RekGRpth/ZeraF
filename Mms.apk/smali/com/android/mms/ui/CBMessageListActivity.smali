.class public Lcom/android/mms/ui/CBMessageListActivity;
.super Landroid/app/Activity;
.source "CBMessageListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/mms/util/DraftCache$OnDraftChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;,
        Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;
    }
.end annotation


# static fields
.field private static final CHECKED_MESSAGE_LIMITS:Ljava/lang/String; = "checked_message_limits"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final DEBUG:Z = false

.field public static final DELETE_MESSAGE_TOKEN:I = 0x76e

.field private static final FOR_MULTIDELETE:Ljava/lang/String; = "ForMultiDelete"

.field public static final HAVE_LOCKED_MESSAGES_TOKEN:I = 0x76f

.field private static final LOCAL_LOGV:Z = false

.field public static final MENU_DELETE_MSG:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CBMessageListActivity"

.field private static final THREADID:Ljava/lang/String; = "thread_id"

.field private static final THREAD_LIST_QUERY_TOKEN:I = 0x76d


# instance fields
.field private mBottomPanel:Landroid/widget/LinearLayout;

.field private mCancelButton:Landroid/widget/ImageButton;

.field private final mContentChangedListener:Lcom/android/mms/ui/CBMessageListAdapter$OnContentChangedListener;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private final mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

.field private mConversation:Lcom/android/mms/data/Conversation;

.field private mDeleteButton:Landroid/widget/ImageButton;

.field private mDeletePanel:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mIsSelectedAll:Z

.field private final mMessageListItemHandler:Landroid/os/Handler;

.field private mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

.field private mMsgListView:Landroid/widget/ListView;

.field private mOptionMenu:Landroid/view/Menu;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

.field private mSelectAllButton:Landroid/widget/ImageButton;

.field private mSharePanel:Lcom/mediatek/ipmsg/ui/SharePanel;

.field private final mThreadListKeyListener:Landroid/view/View$OnKeyListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://cb/messages/#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/CBMessageListActivity;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/mms/ui/CBMessageListActivity$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListActivity$2;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mContentChangedListener:Lcom/android/mms/ui/CBMessageListAdapter$OnContentChangedListener;

    new-instance v0, Lcom/android/mms/ui/CBMessageListActivity$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListActivity$3;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMessageListItemHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/mms/ui/CBMessageListActivity$5;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListActivity$5;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    new-instance v0, Lcom/android/mms/ui/CBMessageListActivity$7;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListActivity$7;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/data/Conversation;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/CBMessageListActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->startAsyncQuery()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/mms/ui/CBMessageListActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mms/ui/CBMessageListActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->checkDeleteMode()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/view/Menu;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    return-object v0
.end method

.method private checkDeleteMode()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/mms/ui/CBMessageListActivity;->markCheckedState(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v0, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeletePanel:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v0, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/CBMessageListAdapter;->clearList()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeletePanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static confirmDeleteMessage(JLandroid/content/AsyncQueryHandler;Landroid/content/Context;)V
    .locals 5
    .param p0    # J
    .param p2    # Landroid/content/AsyncQueryHandler;
    .param p3    # Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;-><init>(JLandroid/content/AsyncQueryHandler;Landroid/content/Context;)V

    const-wide/16 v3, -0x1

    cmp-long v0, p0, v3

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0, v1, p3}, Lcom/android/mms/ui/CBMessageListActivity;->confirmDeleteMessageDialog(Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;ZZLandroid/content/Context;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static confirmDeleteMessageDialog(Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;ZZLandroid/content/Context;)V
    .locals 7
    .param p0    # Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Landroid/content/Context;

    const/4 v6, 0x0

    const v4, 0x7f04001b

    invoke-static {p3, v4, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f0f007c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0b02a1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0f007d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-nez p2, :cond_0

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b029d

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1010355

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b02a6

    invoke-virtual {v4, v5, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b027d

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    invoke-static {p3}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v4

    if-lez v4, :cond_1

    invoke-static {p3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v4

    const/16 v5, 0x75

    invoke-virtual {v4, v5}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;->setDeleteLockedMessage(Z)V

    new-instance v4, Lcom/android/mms/ui/CBMessageListActivity$6;

    invoke-direct {v4, p0, v1}, Lcom/android/mms/ui/CBMessageListActivity$6;-><init>(Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private confirmMultiDelete()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b029d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b02a6

    new-instance v2, Lcom/android/mms/ui/CBMessageListActivity$9;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/CBMessageListActivity$9;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b027d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private confirmThreadDelete()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b022c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/android/mms/ui/CBMessageListActivity$8;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/CBMessageListActivity$8;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public static createIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    invoke-static {p1, p2}, Lcom/android/mms/data/Conversation;->getUri(J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private initActivityState(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v1, 0x0

    if-eqz p1, :cond_0

    const-string v3, "thread_id"

    invoke-virtual {p1, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    :goto_0
    cmp-long v3, v1, v4

    if-lez v3, :cond_1

    invoke-static {p0, v1, v2, v6}, Lcom/android/mms/data/Conversation;->get(Landroid/content/Context;JZ)Lcom/android/mms/data/Conversation;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    :goto_1
    return-void

    :cond_0
    const-string v3, "thread_id"

    invoke-virtual {p2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3, v6}, Lcom/android/mms/data/Conversation;->get(Landroid/content/Context;Landroid/net/Uri;Z)Lcom/android/mms/data/Conversation;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    goto :goto_1

    :cond_2
    invoke-static {p0}, Lcom/android/mms/data/Conversation;->createNew(Landroid/content/Context;)Lcom/android/mms/data/Conversation;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    goto :goto_1
.end method

.method private initListAdapter()V
    .locals 2

    new-instance v0, Lcom/android/mms/ui/CBMessageListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/CBMessageListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mContentChangedListener:Lcom/android/mms/ui/CBMessageListAdapter$OnContentChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/CBMessageListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/CBMessageListAdapter$OnContentChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMessageListItemHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/CBMessageListAdapter;->setMsgListItemHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method

.method private initResourceRefs()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    new-instance v0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;-><init>(Lcom/android/mms/ui/CBMessageListActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    const v0, 0x7f0f002e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    const v0, 0x7f0f0045

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeletePanel:Landroid/view/View;

    const v0, 0x7f0f0046

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mSelectAllButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mSelectAllButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0048

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mCancelButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mCancelButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0049

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    const v0, 0x7f0f0038

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mBottomPanel:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mBottomPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0f0044

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/ipmsg/ui/SharePanel;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mSharePanel:Lcom/mediatek/ipmsg/ui/SharePanel;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mSharePanel:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    new-instance v1, Lcom/android/mms/ui/CBMessageListActivity$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CBMessageListActivity$1;-><init>(Lcom/android/mms/ui/CBMessageListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private initialize(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/mms/ui/CBMessageListActivity;->initActivityState(Landroid/os/Bundle;Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->markAsRead()V

    return-void
.end method

.method private varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Mms/Cb"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private markCheckedState(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/android/mms/ui/CBMessageListAdapter;->setItemsValue(Z[J)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/CBMessageListItem;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private startAsyncQuery()V
    .locals 5

    :try_start_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v3}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity;->mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    const/16 v4, 0x76d

    invoke-static {v3, v1, v2, v4}, Lcom/android/mms/data/CBMessage;->startQueryForThreadId(Landroid/content/AsyncQueryHandler;JI)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Lcom/google/android/mms/util/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method


# virtual methods
.method public isThreadidExist(Landroid/net/Uri;)Z
    .locals 9
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v1, "CBMessageListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intentData = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_1

    new-array v2, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v8

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v7, :cond_0

    move v1, v7

    :goto_0
    return v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v1, v8

    goto :goto_0

    :cond_2
    move v1, v8

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mDeleteButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/CBMessageListAdapter;->getSelectedNumber()I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->confirmThreadDelete()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->confirmMultiDelete()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mCancelButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/CBMessageListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/CBMessageListActivity;->markCheckedState(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mSelectAllButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/CBMessageListActivity;->markCheckedState(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v0, "Mms/Cb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    iget-object v4, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v4}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    if-ltz v4, :cond_0

    invoke-static {p0, v0}, Lcom/android/mms/data/CBMessage;->from(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/CBMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/data/CBMessage;->getMessageId()J

    move-result-wide v2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    return v4

    :pswitch_0
    iget-object v4, p0, Lcom/android/mms/ui/CBMessageListActivity;->mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    invoke-static {v2, v3, v4, p0}, Lcom/android/mms/ui/CBMessageListActivity;->confirmDeleteMessage(JLandroid/content/AsyncQueryHandler;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f04000e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const/4 v0, 0x0

    const-string v3, "bFromLaunch"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v3, "CBMessageListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bFromLaunch = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/mms/ui/CBMessageListActivity;->isThreadidExist(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->initResourceRefs()V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/CBMessageListActivity;->initialize(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->initListAdapter()V

    goto :goto_0
.end method

.method public onDraftChanged(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mQueryHandler:Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    new-instance v1, Lcom/android/mms/ui/CBMessageListActivity$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/mms/ui/CBMessageListActivity$4;-><init>(Lcom/android/mms/ui/CBMessageListActivity;JZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v1, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iput-boolean v2, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    iput-boolean v2, p0, Lcom/android/mms/ui/CBMessageListActivity;->mIsSelectedAll:Z

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->checkDeleteMode()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mms/ui/CBMessageListActivity;->initialize(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mms/ui/CBMessageListActivity;->privateOnStart()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return v1

    :sswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iput-boolean v1, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->checkDeleteMode()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v0, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iput-boolean v2, v0, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->checkDeleteMode()V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v1, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v1, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    const v1, 0x7f0b0226

    invoke-interface {p1, v3, v3, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x108003c

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    iget-boolean v1, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v1, :cond_3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    :goto_1
    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mOptionMenu:Landroid/view/Menu;

    goto :goto_0

    :cond_3
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "thread_id"

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "Mms/Cb"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saved thread id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, v1, v0}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-static {p0}, Lcom/android/mms/data/CBMessage;->cleanup(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/mms/util/DraftCache;->addOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mms/data/Contact;

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/mms/ui/CBMessageListActivity;->privateOnStart()V

    invoke-static {}, Lcom/android/mms/data/CBMessage;->loadingMessages()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Mms/Cb"

    const-string v2, "onStart: loadingMessages = false"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mms/util/DraftCache;->removeOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->markAsRead()V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListActivity;->mMsgListAdapter:Lcom/android/mms/ui/CBMessageListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method protected privateOnStart()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mms/ui/CBMessageListActivity;->startAsyncQuery()V

    return-void
.end method
