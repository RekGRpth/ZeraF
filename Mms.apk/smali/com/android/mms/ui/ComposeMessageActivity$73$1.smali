.class Lcom/android/mms/ui/ComposeMessageActivity$73$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$73;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$73;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v5, 0x1

    const/4 v4, 0x2

    sget v6, Lcom/android/mms/data/WorkingMessage;->sCreationMode:I

    const/4 v0, 0x0

    sput v0, Lcom/android/mms/data/WorkingMessage;->sCreationMode:I

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedType:I

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v2, v2, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedMidea:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-boolean v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedAppend:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mms/data/WorkingMessage;->setAttachment(ILandroid/net/Uri;Z)I

    move-result v7

    const-string v0, "Mms:app"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Restricted Midea: dataUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedMidea:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedType:I

    if-ne v0, v5, :cond_3

    const/4 v0, -0x4

    if-eq v7, v0, :cond_1

    const/4 v0, -0x2

    if-ne v7, v0, :cond_3

    :cond_1
    const-string v0, "Mms:app"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addImage: resize image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedMidea:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->log(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedMidea:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v2, v2, Lcom/android/mms/ui/ComposeMessageActivity$73;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15300(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$73;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15400(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/MessageUtils$ResizeImageResultCallback;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-boolean v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedAppend:Z

    invoke-static/range {v0 .. v5}, Lcom/android/mms/ui/MessageUtils;->resizeImage(Landroid/content/Context;Landroid/net/Uri;Landroid/os/Handler;Lcom/android/mms/ui/MessageUtils$ResizeImageResultCallback;ZZ)V

    sput v6, Lcom/android/mms/data/WorkingMessage;->sCreationMode:I

    :goto_0
    return-void

    :cond_3
    sput v6, Lcom/android/mms/data/WorkingMessage;->sCreationMode:I

    const v8, 0x7f0b02e2

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    const v8, 0x7f0b02e1

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v7, v8}, Lcom/android/mms/ui/ComposeMessageActivity;->access$9600(Lcom/android/mms/ui/ComposeMessageActivity;II)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$73$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$73;

    iget v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$73;->val$mRestrictedType:I

    if-ne v0, v4, :cond_4

    const v8, 0x7f0b02e3

    goto :goto_1
.end method
