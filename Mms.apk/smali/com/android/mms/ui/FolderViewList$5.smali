.class Lcom/android/mms/ui/FolderViewList$5;
.super Ljava/lang/Object;
.source "FolderViewList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/FolderViewList;->confirmDeleteMessageDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;

.field final synthetic val$checkbox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderViewList;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    iput-object p2, p0, Lcom/android/mms/ui/FolderViewList$5;->val$checkbox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1300(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/DeleteProgressDialogUtil;->getProgressDialog(Landroid/content/Context;)Lcom/android/mms/ui/NewProgressDialog;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->setProgressDialog(Lcom/android/mms/ui/NewProgressDialog;)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1300(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->showProgressDialog()V

    const/4 v0, 0x0

    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    if-nez v2, :cond_2

    const-string v2, "content://mms-sms/folder_delete/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, 0x1

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->val$checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    const-string v1, " locked=0 "

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1300(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    move-result-object v2

    const/16 v3, 0x3e9

    invoke-static {v2, v3, v0, v1}, Lcom/android/mms/data/FolderView;->startDeleteBoxMessage(Landroid/content/AsyncQueryHandler;ILandroid/net/Uri;Ljava/lang/String;)V

    return-void

    :cond_2
    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const-string v2, "content://mms-sms/folder_delete/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, 0x4

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    const-string v2, "content://mms-sms/folder_delete/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, 0x3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const-string v2, "content://mms-sms/folder_delete/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, 0x2

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$5;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1400(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND locked=0 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
