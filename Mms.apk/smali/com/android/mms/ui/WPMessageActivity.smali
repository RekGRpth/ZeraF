.class public Lcom/android/mms/ui/WPMessageActivity;
.super Landroid/app/Activity;
.source "WPMessageActivity.java"

# interfaces
.implements Lcom/android/mms/data/Contact$UpdateListener;
.implements Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/WPMessageActivity$ModeCallback;,
        Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;,
        Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;
    }
.end annotation


# static fields
.field private static ACTIONMODE:Ljava/lang/String; = null

.field private static final DEBUG:Z = false

.field private static final DELETE_MESSAGE_TOKEN:I = 0x25e4

.field private static final DISPLAY_UNREAD_COUNT_CONTENT_FOR_ABOVE_99:Ljava/lang/String; = "99+"

.field private static final LOCAL_LOGV:Z = false

.field private static final MAX_DISPLAY_UNREAD_COUNT:I = 0x63

.field private static final MENU_DELETE_ALL_WAPPUSH:I = 0x5

.field private static final MENU_MULTI_DELETE_MESSAGES:I = 0x9

.field private static final MESSAGE_LIST_QUERY_TOKEN:I = 0x2537

.field public static final REQUEST_CODE_ADD_CONTACT:I = 0x12

.field private static final SEEN_PROJECTION:[Ljava/lang/String;

.field private static final SI_ACTION_DELETE:I = 0x4

.field private static final SI_ACTION_HIGH:I = 0x3

.field private static final SI_ACTION_LOW:I = 0x1

.field private static final SI_ACTION_MEDIUM:I = 0x2

.field private static final SI_ACTION_NONE:I = 0x0

.field private static final SL_ACTION_CACHE:I = 0x3

.field private static final SL_ACTION_HIGH:I = 0x2

.field private static final SL_ACTION_LOW:I = 0x1

.field private static final STR_CN:Ljava/lang/String; = "\n"

.field private static final STR_RN:Ljava/lang/String; = "\\r\\n"

.field private static final TAG:Ljava/lang/String; = "Mms/WapPush"

.field private static final TRACE:Z = false

.field private static final UNREAD_MESSAGES_QUERY_TOKEN:I = 0x2538

.field private static final UNSEEN_SELECTION:Ljava/lang/String; = "seen=0"

.field private static mDeleteCounter:I

.field protected static sDestroy:Z


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeListener:Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

.field private mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private final mDataSetChangedListener:Lcom/android/mms/ui/WPMessageListAdapter$OnDataSetChangedListener;

.field private mEmptyViewDefault:Landroid/view/View;

.field private final mMessageListItemHandler:Landroid/os/Handler;

.field private mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

.field public mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

.field private mMsgListView:Lcom/android/mms/ui/WPMessageListView;

.field private mNeedToMarkAsSeen:Z

.field private mPossiblePendingNotification:Z

.field private mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

.field private mThreadId:Ljava/lang/Long;

.field private mUnreadConvCount:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "actionMode"

    sput-object v0, Lcom/android/mms/ui/WPMessageActivity;->ACTIONMODE:Ljava/lang/String;

    sput v2, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    sput-boolean v2, Lcom/android/mms/ui/WPMessageActivity;->sDestroy:Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "seen"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/mms/ui/WPMessageActivity;->SEEN_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/mms/ui/WPMessageActivity$ModeCallback;-><init>(Lcom/android/mms/ui/WPMessageActivity;Lcom/android/mms/ui/WPMessageActivity$1;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mActionModeListener:Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mThreadId:Ljava/lang/Long;

    iput-object v2, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMessageListItemHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageActivity$3;-><init>(Lcom/android/mms/ui/WPMessageActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mDataSetChangedListener:Lcom/android/mms/ui/WPMessageListAdapter$OnDataSetChangedListener;

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/WPMessageActivity;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-boolean v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mNeedToMarkAsSeen:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/android/mms/ui/WPMessageActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/WPMessageActivity;->mNeedToMarkAsSeen:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/WPMessageActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mUnreadConvCount:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageListView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    return-object v0
.end method

.method static synthetic access$1400(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/ui/WPMessageActivity;->blockingMarkAllWapPushMessagesAsSeen(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    return p0
.end method

.method static synthetic access$208()I
    .locals 2

    sget v0, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$210()I
    .locals 2

    sget v0, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/android/mms/ui/WPMessageActivity;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/mms/ui/WPMessageActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$ModeCallback;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mActionModeListener:Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

    return-object v0
.end method

.method static synthetic access$500(Landroid/content/Context;Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    invoke-static {p0, p1}, Lcom/android/mms/ui/WPMessageActivity;->getWPMessageDetails(Landroid/content/Context;Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/mms/ui/WPMessageActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/WPMessageActivity;->mPossiblePendingNotification:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/mms/ui/WPMessageActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startMsgListQuery()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/mms/ui/WPMessageActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startUnreadQuery()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mms/ui/WPMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mEmptyViewDefault:Landroid/view/View;

    return-object v0
.end method

.method private addRecipientsListeners()V
    .locals 0

    invoke-static {p0}, Lcom/android/mms/data/Contact;->addListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    return-void
.end method

.method private static blockingMarkAllWapPushMessagesAsSeen(Landroid/content/Context;)V
    .locals 10
    .param p0    # Landroid/content/Context;

    const/4 v9, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/mms/ui/WPMessageActivity;->SEEN_PROJECTION:[Ljava/lang/String;

    const-string v3, "seen=0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const/4 v6, 0x0

    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    if-nez v6, :cond_1

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8, v9}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "seen"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "seen=0"

    invoke-virtual {v0, v1, v8, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private checkPendingNotification()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mPossiblePendingNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mPossiblePendingNotification:Z

    :cond_0
    return-void
.end method

.method public static confirmDeleteMessageDialog(Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;Ljava/util/HashSet;Landroid/content/Context;)V
    .locals 7
    .param p0    # Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    const v4, 0x7f04001b

    invoke-static {p2, v4, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f0f007c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-nez p1, :cond_0

    const v4, 0x7f0b01ef

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v4, 0x7f0f007d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b029d

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1010355

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b02a6

    invoke-virtual {v4, v5, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b027d

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    const v4, 0x7f0b01f0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private copyToClipboard(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v1, "clipboard"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    invoke-virtual {v0, p1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;->CONTENT_URI_THREAD:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private getMessageItem(IJZ)Lcom/android/mms/ui/WPMessageItem;
    .locals 2
    .param p1    # I
    .param p2    # J
    .param p4    # Z

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/android/mms/ui/WPMessageListAdapter;->getCachedMessageItem(IJLandroid/database/Cursor;)Lcom/android/mms/ui/WPMessageItem;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getWPMessageDetails(Landroid/content/Context;Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/String;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v10, 0x7f0b0021

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/android/mms/ui/WPMessageItem;->mAction:I

    iget v8, p1, Lcom/android/mms/ui/WPMessageItem;->mType:I

    if-nez v8, :cond_3

    packed-switch v5, :pswitch_data_0

    const-string v10, "Mms/WapPush"

    const-string v11, "WPMessageActivity: getWPMessageDetails si priority error."

    invoke-static {v10, v11}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v10, 0x7f0b02ad

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p1, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v10, 0x7f0b02b1

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/android/mms/ui/WPMessageItem;->mDate:J

    const/4 v10, 0x1

    invoke-static {p0, v0, v1, v10}, Lcom/android/mms/ui/MessageUtils;->formatTimeStampString(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p1, Lcom/android/mms/ui/WPMessageItem;->mExpirationLong:J

    const-wide/16 v10, 0x0

    cmp-long v10, v3, v10

    if-eqz v10, :cond_0

    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v10, 0x7f0b0027

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {p0, v3, v4, v13}, Lcom/android/mms/ui/MessageUtils;->formatTimeStampString(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v9, p1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    if-eqz v9, :cond_1

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "\n\n"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v10, 0x7f0b00bf

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v7, p1, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    if-eqz v7, :cond_2

    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "\n\n"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p1, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    :pswitch_0
    const-string v10, "Mms/WapPush"

    const-string v11, "WPMessageActivity: action error, none"

    invoke-static {v10, v11}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const v10, 0x7f0b0022

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_2
    const v10, 0x7f0b0023

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_3
    const v10, 0x7f0b0024

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_4
    const-string v10, "Mms/WapPush"

    const-string v11, "WPMessageActivity: action error, delete"

    invoke-static {v10, v11}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    const/4 v10, 0x1

    if-ne v10, v8, :cond_4

    packed-switch v5, :pswitch_data_1

    const-string v10, "Mms/WapPush"

    const-string v11, "WPMessageActivity: getWPMessageDetails sl priority error."

    invoke-static {v10, v11}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const v10, 0x7f0b0022

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_6
    const v10, 0x7f0b0024

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_7
    const v10, 0x7f0b0022

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_4
    const-string v10, "Mms/WapPush"

    const-string v11, "WPMessageActivity: getWPMessageDetails type error."

    invoke-static {v10, v11}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private goToConversationList()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/ConversationList;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/WPMessageActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private initActivityState(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->addRecipientsListeners()V

    return-void
.end method

.method private initMessageList()V
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "highlight"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    move-object v5, v2

    :goto_1
    new-instance v0, Lcom/android/mms/ui/WPMessageListAdapter;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    const/4 v4, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/WPMessageListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;ZLjava/util/regex/Pattern;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mDataSetChangedListener:Lcom/android/mms/ui/WPMessageListAdapter$OnDataSetChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/WPMessageListAdapter;->setOnDataSetChangedListener(Lcom/android/mms/ui/WPMessageListAdapter$OnDataSetChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    new-instance v1, Lcom/android/mms/ui/WPMessageActivity$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageActivity$1;-><init>(Lcom/android/mms/ui/WPMessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    new-instance v1, Lcom/android/mms/ui/WPMessageActivity$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageActivity$2;-><init>(Lcom/android/mms/ui/WPMessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v5

    goto :goto_1
.end method

.method private initPlugin(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-interface {v0, p0, p0}, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;->init(Lcom/mediatek/mms/ext/IMmsTextSizeAdjustHost;Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 8
    .param p0    # Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v5, 0x3

    aget-object v5, v2, v5

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v5, "Mms/WapPush"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WPMessageActivity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private removeRecipientsListeners()V
    .locals 0

    invoke-static {p0}, Lcom/android/mms/data/Contact;->removeListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    return-void
.end method

.method private setupActionBar()V
    .locals 7

    const/16 v6, 0x10

    const/4 v5, -0x2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040011

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v5, v5, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0f0064

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/WPMessageActivity;->mUnreadConvCount:Landroid/widget/TextView;

    return-void
.end method

.method private startMsgListQuery()V
    .locals 9

    const/16 v2, 0x2537

    sget-object v3, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    if-nez v3, :cond_0

    const-string v0, "Mms/WapPush"

    const-string v1, "startMsgListQuery: wpMessagesUri is null, bail!"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Mms/WapPush"

    const-string v1, "startMsgListQuery"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    invoke-virtual {v0, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    const/16 v1, 0x2537

    const/4 v2, 0x0

    sget-object v4, Lcom/android/mms/ui/WPMessageListAdapter;->WP_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "date DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-static {p0, v8}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method

.method private startUnreadQuery()V
    .locals 9

    const/16 v2, 0x2538

    sget-object v3, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    if-nez v3, :cond_0

    const-string v0, "Mms/WapPush"

    const-string v1, "startUnreadQuery: wpMessagesUri is null, bail!"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Mms/WapPush"

    const-string v1, "startUnreadQuery"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    invoke-virtual {v0, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    const/16 v1, 0x2538

    const/4 v2, 0x0

    sget-object v4, Lcom/android/mms/ui/WPMessageListAdapter;->WP_PROJECTION:[Ljava/lang/String;

    const-string v5, "read=0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-static {p0, v8}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-interface {v1, p1}, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_1
    return v0
.end method

.method public initialize(J)V
    .locals 3
    .param p1    # J

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mms/ui/WPMessageActivity;->initActivityState(Landroid/content/Intent;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalThreadId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mms/ui/WPMessageActivity;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->initMessageList()V

    return-void
.end method

.method public markAllMessageAsSeen()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/WPMessageActivity$5;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageActivity$5;-><init>(Lcom/android/mms/ui/WPMessageActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0, p0}, Lcom/android/mms/ui/WPMessageActivity;->initPlugin(Landroid/content/Context;)V

    const-string v0, "Mms/WapPush"

    const-string v1, "WPMessageActivity: Enter onCreate function."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f040076

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/mms/ui/WPMessageActivity;->openApplication()V

    const v0, 0x7f0f002e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/WPMessageListView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0f0075

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mEmptyViewDefault:Landroid/view/View;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mContentResolver:Landroid/content/ContentResolver;

    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;-><init>(Lcom/android/mms/ui/WPMessageActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/mms/ui/WPMessageActivity;->initialize(J)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/mms/ui/WPMessageActivity;->sDestroy:Z

    new-instance v0, Lcom/mediatek/wappush/SiExpiredCheck;

    invoke-direct {v0, p0}, Lcom/mediatek/wappush/SiExpiredCheck;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

    invoke-virtual {v0}, Lcom/mediatek/wappush/SiExpiredCheck;->startSiExpiredCheckThread()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/mms/ui/WPMessageActivity;->sDestroy:Z

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

    invoke-virtual {v0}, Lcom/mediatek/wappush/SiExpiredCheck;->stopSiExpiredCheckThread()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-eqz v6, :cond_0

    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;-><init>(Lcom/android/mms/ui/WPMessageActivity;Ljava/util/HashSet;Landroid/content/AsyncQueryHandler;Landroid/content/Context;Landroid/view/ActionMode;)V

    invoke-static {v0, v2, p0}, Lcom/android/mms/ui/WPMessageActivity;->confirmDeleteMessageDialog(Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;Ljava/util/HashSet;Landroid/content/Context;)V

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v1, "Mms/WapPush"

    const-string v3, "WPMessageActivity: Unexpected ClassCastException."

    invoke-static {v1, v3, v7}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x43
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/high16 v3, 0x100000

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v1, "Mms/WapPush"

    const-string v2, "onNewIntent"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v3

    if-ne v3, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mms/ui/WPMessageActivity;->openApplication()V

    :cond_0
    const-wide/16 v1, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/mms/ui/WPMessageActivity;->initialize(J)V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startMsgListQuery()V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startUnreadQuery()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :sswitch_0
    new-instance v0, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;

    move-object v1, p0

    move-object v4, p0

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;-><init>(Lcom/android/mms/ui/WPMessageActivity;Ljava/util/HashSet;Landroid/content/AsyncQueryHandler;Landroid/content/Context;Landroid/view/ActionMode;)V

    invoke-static {v0, v2, p0}, Lcom/android/mms/ui/WPMessageActivity;->confirmDeleteMessageDialog(Lcom/android/mms/ui/WPMessageActivity$DeleteMessageListener;Ljava/util/HashSet;Landroid/content/Context;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->removeRecipientsListeners()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    const-string v1, "Mms/WapPush"

    const-string v2, "onPrepareOptionsMenu"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x5

    const v2, 0x7f0b01f1

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0200d0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onRestart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    const-string v0, "Mms/WapPush"

    const-string v1, "onRestart function."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->addRecipientsListeners()V

    const-string v0, "Mms/WapPush"

    const-string v1, "onResume function"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

    invoke-virtual {v0}, Lcom/mediatek/wappush/SiExpiredCheck;->startExpiredCheck()V

    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mNeedToMarkAsSeen:Z

    invoke-static {}, Lcom/android/mms/MmsConfig;->getAdjustFontSizeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "message_font_size"

    const/high16 v2, 0x41900000

    invoke-static {p0, v1, v2}, Lcom/android/mms/ui/MessageUtils;->getPreferenceValueFloat(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/WPMessageActivity;->setTextSize(F)V

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity;->mMmsTextSizeAdjustPlugin:Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;

    invoke-interface {v1}, Lcom/mediatek/mms/ext/IMmsTextSizeAdjust;->refresh()V

    :cond_1
    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startMsgListQuery()V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->startUnreadQuery()V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->setupActionBar()V

    const v1, 0x7f0b01e7

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(I)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_0
    const-string v0, "Mms/WapPush"

    const-string v1, "WPMessageActivity: onStop stopExpiredCheck."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mSiExpiredCheck:Lcom/mediatek/wappush/SiExpiredCheck;

    invoke-virtual {v0}, Lcom/mediatek/wappush/SiExpiredCheck;->stopExpiredCheck()V

    return-void
.end method

.method public onUpdate(Lcom/android/mms/data/Contact;)V
    .locals 2
    .param p1    # Lcom/android/mms/data/Contact;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity;->mMessageListItemHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mms/ui/WPMessageActivity$4;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageActivity$4;-><init>(Lcom/android/mms/ui/WPMessageActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->checkPendingNotification()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageActivity;->checkPendingNotification()V

    :cond_0
    return-void
.end method

.method public openApplication()V
    .locals 8

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "URL"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v4}, Lcom/android/mms/ui/MessageUtils;->checkAndModifyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v2, v5, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v5, "com.android.browser.application_id"

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v5, 0x80000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/android/mms/ui/WPMessageActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const v5, 0x7f0b0028

    const/4 v6, 0x1

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    const-string v5, "Mms/WapPush"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Scheme "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "is not supported!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTextSize(F)V
    .locals 5
    .param p1    # F

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v4, p1}, Lcom/android/mms/ui/WPMessageListAdapter;->setTextSize(F)V

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity;->mMsgListView:Lcom/android/mms/ui/WPMessageListView;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    instance-of v4, v3, Lcom/android/mms/ui/WPMessageListItem;

    if-eqz v4, :cond_1

    move-object v2, v3

    check-cast v2, Lcom/android/mms/ui/WPMessageListItem;

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/WPMessageListItem;->setTextSize(F)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    invoke-static {p1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
