.class final Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;
.super Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;
.source "FolderViewList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderViewList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ThreadListQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/FolderViewList;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const-wide/16 v3, -0x2

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$2300()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$2310()I

    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "igonre a onDeleteComplete,mDeleteCounter:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/mms/ui/FolderViewList;->access$2300()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$2302(I)I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0, v3, v4, v2}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;JZ)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0, v3, v4}, Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/transaction/CBMessagingNotification;->updateAllNotifications(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->clearbackupstate()V

    invoke-virtual {p0}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->progress()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;->dismissProgressDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 9
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5, v4}, Lcom/android/mms/ui/FolderViewList;->access$802(Lcom/android/mms/ui/FolderViewList;Z)Z

    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_5

    :cond_0
    const-string v3, "FolderViewList"

    const-string v5, "cursor == null||count==0."

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    if-eqz p3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$700(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1000(Lcom/android/mms/ui/FolderViewList;)V

    :cond_2
    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$600(Lcom/android/mms/ui/FolderViewList;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateSendFailedNotification(Landroid/content/Context;)V

    :cond_3
    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_4
    :goto_0
    return-void

    :cond_5
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/ui/FolderViewListAdapter;->getOnContentChangedListener()Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;

    move-result-object v5

    if-nez v5, :cond_7

    :cond_6
    const-string v3, "FolderViewList"

    const-string v4, "onQueryComplete, no OnContentChangedListener"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_7
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2000(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2000(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_8

    const-string v5, "FolderViewList"

    const-string v6, "onQueryComplete mSearchView != null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2000(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_8
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    sparse-switch p1, :sswitch_data_0

    const-string v5, "FolderViewList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onQueryComplete called with unknown token "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_1
    :sswitch_0
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v5}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v5, "FolderViewList"

    const-string v6, "onQueryComplete invalidateOptionsMenu"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v5, v4}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    const-string v5, "FolderViewList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onQueryComplete : mNeedQuery ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v7}, Lcom/android/mms/ui/FolderViewList;->access$900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$700(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1000(Lcom/android/mms/ui/FolderViewList;)V

    :cond_a
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5, v4}, Lcom/android/mms/ui/FolderViewList;->access$2102(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2200(Lcom/android/mms/ui/FolderViewList;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2200(Lcom/android/mms/ui/FolderViewList;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    :cond_b
    if-eqz p3, :cond_4

    const/4 v5, -0x1

    invoke-interface {p3, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v1, 0x0

    :goto_2
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    const/16 v5, 0xd

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-lez v5, :cond_10

    move v1, v3

    :goto_3
    if-eqz v1, :cond_c

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5, v3}, Lcom/android/mms/ui/FolderViewList;->access$2102(Lcom/android/mms/ui/FolderViewList;Z)Z

    :cond_c
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$2200(Lcom/android/mms/ui/FolderViewList;)Ljava/util/Map;

    move-result-object v5

    const/4 v6, 0x6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v7, v7

    invoke-static {v6, v7, v8}, Lcom/android/mms/ui/FolderViewListAdapter;->getKey(IJ)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :sswitch_1
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "FolderViewList"

    const-string v6, "onQueryComplete DRAFTFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5, v4}, Lcom/android/mms/ui/FolderViewList;->access$1902(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/android/mms/data/Conversation;->markAllConversationsAsSeen(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v6}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    :sswitch_2
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v5

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5, v4}, Lcom/android/mms/ui/FolderViewList;->access$1902(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/android/mms/data/Conversation;->markAllConversationsAsSeen(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v6}, Lcom/android/mms/ui/FolderViewList;->access$1500(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_d
    const/4 v0, 0x0

    :cond_e
    :goto_4
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_f

    const/4 v5, 0x5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-nez v5, :cond_e

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "FolderViewList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onQueryComplete INBOXFOLDER_LIST_QUERY_TOKEN count "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1900(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$600(Lcom/android/mms/ui/FolderViewList;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateSendFailedNotification(Landroid/content/Context;)V

    goto/16 :goto_1

    :sswitch_3
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "FolderViewList"

    const-string v6, "onQueryComplete OUTBOXFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    :sswitch_4
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "FolderViewList"

    const-string v6, "onQueryComplete SENTFOLDER_LIST_QUERY_TOKEN"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_1

    :cond_10
    move v1, v4

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3f1 -> :sswitch_1
        0x457 -> :sswitch_2
        0x461 -> :sswitch_3
        0x46b -> :sswitch_4
    .end sparse-switch
.end method
