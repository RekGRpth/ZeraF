.class public Lcom/android/mms/ui/ChipsRecipientAdapter;
.super Lcom/android/ex/chips/BaseRecipientAdapter;
.source "ChipsRecipientAdapter.java"


# static fields
.field private static final DEFAULT_PREFERRED_MAX_RESULT_COUNT:I = 0xa


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0, v1}, Lcom/android/ex/chips/BaseRecipientAdapter;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0, v1}, Lcom/android/ex/chips/BaseRecipientAdapter;->setShowDuplicateResults(Z)V

    return-void
.end method


# virtual methods
.method protected getItemLayout()I
    .locals 1

    const v0, 0x7f04003c

    return v0
.end method

.method public setShowEmailAddress(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/ex/chips/BaseRecipientAdapter;->setShowPhoneAndEmail(Z)V

    return-void
.end method
