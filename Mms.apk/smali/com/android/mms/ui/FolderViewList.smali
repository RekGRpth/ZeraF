.class public Lcom/android/mms/ui/FolderViewList;
.super Landroid/app/ListActivity;
.source "FolderViewList.java"

# interfaces
.implements Lcom/android/mms/util/DraftCache$OnDraftChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/FolderViewList$MultiSelectOnLongClickListener;,
        Lcom/android/mms/ui/FolderViewList$ModeCallback;,
        Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;,
        Lcom/android/mms/ui/FolderViewList$BaseProgressQueryHandler;,
        Lcom/android/mms/ui/FolderViewList$DownloadMessageListener;,
        Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;
    }
.end annotation


# static fields
.field private static ACTIONMODE:Ljava/lang/String; = null

.field private static BOXTYPE:Ljava/lang/String; = null

.field private static final CB_URI:Landroid/net/Uri;

.field private static final CONV_TAG:Ljava/lang/String; = "Mms/FolderViewList"

.field private static final DEBUG:Z = false

.field public static final DRAFTFOLDER_LIST_QUERY_TOKEN:I = 0x3f1

.field private static final FOLDERVIEW_DELETE_OBSOLETE_THREADS_TOKEN:I = 0x3eb

.field public static final FOLDERVIEW_DELETE_TOKEN:I = 0x3e9

.field public static final FOLDERVIEW_HAVE_LOCKED_MESSAGES_TOKEN:I = 0x3ea

.field public static final FOLDERVIEW_KEY:Ljava/lang/String; = "floderview_key"

.field private static final FOR_FOLDERMODE_MULTIDELETE:Ljava/lang/String; = "ForFolderMultiDelete"

.field private static final FOR_MULTIDELETE:Ljava/lang/String; = "ForMultiDelete"

.field public static final INBOXFOLDER_LIST_QUERY_TOKEN:I = 0x457

.field private static final LOCAL_LOGV:Z = false

.field public static final MAX_FONT_SCALE:F = 1.1f

.field public static final MENU_ADD_TO_CONTACTS:I = 0x3

.field public static final MENU_CHANGEVIEW:I = 0x1

.field public static final MENU_DELETE:I = 0x0

.field public static final MENU_FORWORD:I = 0x5

.field public static final MENU_MULTIDELETE:I = 0x0

.field public static final MENU_REPLY:I = 0x6

.field public static final MENU_SIM_SMS:I = 0x4

.field public static final MENU_VIEW:I = 0x1

.field public static final MENU_VIEW_CONTACT:I = 0x2

.field private static final MMS_URI:Landroid/net/Uri;

.field private static NEED_RESTORE_ADAPTER_STATE:Ljava/lang/String; = null

.field public static final OPTION_DRAFTBOX:I = 0x2

.field public static final OPTION_INBOX:I = 0x0

.field public static final OPTION_OUTBOX:I = 0x1

.field public static final OPTION_SENTBOX:I = 0x3

.field public static final OUTBOXFOLDER_LIST_QUERY_TOKEN:I = 0x461

.field public static final REQUEST_CODE_DELETE_RESULT:I = 0xb4

.field public static final REQUEST_CODE_SELECT_SIMINFO:I = 0xb4

.field public static final SENTFOLDER_LIST_QUERY_TOKEN:I = 0x46b

.field private static final SIM_SMS_URI:Landroid/net/Uri;

.field private static final SMS_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "FolderViewList"

.field private static final VIEW_ITEM_KEY_BOXNAME:Ljava/lang/String; = "spinner_line_2"

.field private static final WAPPUSH_URI:Landroid/net/Uri;

.field private static mDeleteCounter:I

.field private static mSELECT_ITEM_IDS:Ljava/lang/String;

.field public static mgViewID:I


# instance fields
.field private context:Landroid/content/Context;

.field private mAccountDropdown:Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;

.field public mActionMode:Landroid/view/ActionMode;

.field private mAdapter:Landroid/widget/SimpleAdapter;

.field private mContact:Lcom/android/mms/data/Contact;

.field private final mContentChangedListener:Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;

.field private mCountTextView:Landroid/widget/TextView;

.field private final mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

.field private mDisableSearchFlag:Z

.field private mFolderSpinner:Landroid/view/View;

.field private mFontScale:F

.field private mHandler:Landroid/os/Handler;

.field private mHasLockedMsg:Z

.field private mIsInActivity:Z

.field private mIsNeedRestoreAdapterState:Z

.field private mIsQuerying:Z

.field private mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

.field private mListItemLockInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mListSelectedItem:[J

.field public mModeCallBack:Lcom/android/mms/ui/FolderViewList$ModeCallback;

.field private mNeedQuery:Z

.field private mNeedToMarkAsSeen:Z

.field private mNeedUpdateListView:Z

.field private mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

.field mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mSIM_SMS_URI_NEW:Landroid/net/Uri;

.field private mSearchItem:Landroid/view/MenuItem;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSpinnerTextView:Landroid/widget/TextView;

.field private final mThreadListKeyListener:Landroid/view/View$OnKeyListener;

.field private mType:I

.field private where:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://sms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->SMS_URI:Landroid/net/Uri;

    const-string v0, "content://mms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->MMS_URI:Landroid/net/Uri;

    const-string v0, "content://wappush/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->WAPPUSH_URI:Landroid/net/Uri;

    const-string v0, "content://cb/messages/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->CB_URI:Landroid/net/Uri;

    const-string v0, "content://mms-sms/sim_sms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->SIM_SMS_URI:Landroid/net/Uri;

    const/4 v0, 0x0

    sput v0, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    const-string v0, "actionMode"

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->ACTIONMODE:Ljava/lang/String;

    const-string v0, "boxType"

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->BOXTYPE:Ljava/lang/String;

    const-string v0, "needRestore"

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->NEED_RESTORE_ADAPTER_STATE:Ljava/lang/String;

    const-string v0, "selectItemIds"

    sput-object v0, Lcom/android/mms/ui/FolderViewList;->mSELECT_ITEM_IDS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mContact:Lcom/android/mms/data/Contact;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-string v0, "content://mms-sms/sim_sms/#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSIM_SMS_URI_NEW:Landroid/net/Uri;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mDisableSearchFlag:Z

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mNeedUpdateListView:Z

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mIsQuerying:Z

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mNeedQuery:Z

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mIsInActivity:Z

    new-instance v0, Lcom/android/mms/ui/FolderViewList$ModeCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/mms/ui/FolderViewList$ModeCallback;-><init>(Lcom/android/mms/ui/FolderViewList;Lcom/android/mms/ui/FolderViewList$1;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mModeCallBack:Lcom/android/mms/ui/FolderViewList$ModeCallback;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mIsNeedRestoreAdapterState:Z

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mHasLockedMsg:Z

    new-instance v0, Lcom/android/mms/ui/FolderViewList$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/FolderViewList$2;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mContentChangedListener:Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;

    new-instance v0, Lcom/android/mms/ui/FolderViewList$6;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/FolderViewList$6;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/android/mms/ui/FolderViewList$7;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/FolderViewList$7;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/mms/ui/FolderViewList$9;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/FolderViewList$9;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/FolderViewList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    return-void
.end method

.method static synthetic access$1100()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/FolderViewList;->MMS_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/FolderViewList;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/FolderViewList;->markMmsIndReaded(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/mms/ui/FolderViewList;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/mms/ui/FolderViewList;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mCountTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/mms/ui/FolderViewList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mNeedToMarkAsSeen:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mNeedToMarkAsSeen:Z

    return p1
.end method

.method static synthetic access$200(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SimpleAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mAdapter:Landroid/widget/SimpleAdapter;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/mms/ui/FolderViewList;)Landroid/widget/SearchView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mHasLockedMsg:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/android/mms/ui/FolderViewList;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListItemLockInfo:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2300()I
    .locals 1

    sget v0, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$2302(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    return p0
.end method

.method static synthetic access$2308()I
    .locals 2

    sget v0, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$2310()I
    .locals 2

    sget v0, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/android/mms/ui/FolderViewList;->mDeleteCounter:I

    return v0
.end method

.method static synthetic access$2400(Lcom/android/mms/ui/FolderViewList;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSearchItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mDisableSearchFlag:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/android/mms/ui/FolderViewList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mIsNeedRestoreAdapterState:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mIsNeedRestoreAdapterState:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/android/mms/ui/FolderViewList;)[J
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListSelectedItem:[J

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/mms/ui/FolderViewList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->confirmMultiDelete()V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/mms/ui/FolderViewList;J)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/FolderViewList;->isMsgLocked(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mAccountDropdown:Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;

    return-object v0
.end method

.method static synthetic access$3000()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/FolderViewList;->WAPPUSH_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$3100()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/FolderViewList;->CB_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/mms/ui/FolderViewList;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mFolderSpinner:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/mms/ui/FolderViewList;I)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/FolderViewList;->onAccountSpinnerItemClicked(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/mms/ui/FolderViewList;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mms/ui/FolderViewList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mIsInActivity:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/mms/ui/FolderViewList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mIsQuerying:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mIsQuerying:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/mms/ui/FolderViewList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/FolderViewList;

    iget-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mNeedQuery:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/mms/ui/FolderViewList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewList;->mNeedQuery:Z

    return p1
.end method

.method private confirmDeleteMessageDialog()V
    .locals 8

    const/4 v7, 0x0

    const/4 v4, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b029d

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v5, 0x1010355

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const v6, 0x7f04001b

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0f007c

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f0b0107

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f0f007d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-boolean v5, p0, Lcom/android/mms/ui/FolderViewList;->mHasLockedMsg:Z

    if-eqz v5, :cond_0

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b02a6

    new-instance v5, Lcom/android/mms/ui/FolderViewList$5;

    invoke-direct {v5, p0, v1}, Lcom/android/mms/ui/FolderViewList$5;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b027d

    invoke-virtual {v0, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    const/16 v4, 0x8

    goto :goto_0
.end method

.method private confirmDownloadDialog(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x7f0b022e

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b00f8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b027d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private confirmMultiDelete()V
    .locals 8

    const/4 v7, 0x0

    const/4 v4, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b029d

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v5, 0x1010355

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const v6, 0x7f04001b

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0f007c

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f0b0073

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f0f007d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->selectedMsgHasLocked()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b02a6

    new-instance v5, Lcom/android/mms/ui/FolderViewList$10;

    invoke-direct {v5, p0, v1}, Lcom/android/mms/ui/FolderViewList$10;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b027d

    invoke-virtual {v0, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    const/16 v4, 0x8

    goto :goto_0
.end method

.method private forwardMessage(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.mms.ui.ForwardMessageActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "forwarded_message"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "sms_body"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private getData()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "spinner_line_2"

    const v4, 0x7f0b00e9

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v3, "spinner_line_2"

    const v4, 0x7f0b00ea

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v3, "spinner_line_2"

    const v4, 0x7f0b00eb

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v3, "spinner_line_2"

    const v4, 0x7f0b00ec

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private initListAdapter()V
    .locals 2

    const-string v0, "FolderViewList"

    const-string v1, "initListAdapter"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    if-nez v0, :cond_0

    const-string v0, "FolderViewList"

    const-string v1, "create it"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/mms/ui/FolderViewListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/FolderViewListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mContentChangedListener:Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/FolderViewListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    :cond_0
    return-void
.end method

.method private initSpinnerListAdapter()V
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->getData()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f040029

    new-array v4, v5, [Ljava/lang/String;

    const-string v1, "spinner_line_2"

    aput-object v1, v4, v6

    new-array v5, v5, [I

    const v1, 0x7f0f00b5

    aput v1, v5, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mAdapter:Landroid/widget/SimpleAdapter;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->setupActionBar()V

    new-instance v0, Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mAccountDropdown:Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mAccountDropdown:Lcom/android/mms/ui/FolderViewList$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private isMsgLocked(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListItemLockInfo:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListItemLockInfo:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListItemLockInfo:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private markMmsIndReaded(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/FolderViewList$8;

    invoke-direct {v1, p0, p1}, Lcom/android/mms/ui/FolderViewList$8;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/net/Uri;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-wide/16 v0, -0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;JZ)V

    return-void
.end method

.method private onAccountSpinnerItemClicked(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccountSpinnerItemClicked mgViewID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const/4 v0, 0x0

    sput v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00e9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iput-boolean v2, p0, Lcom/android/mms/ui/FolderViewList;->mNeedToMarkAsSeen:Z

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    goto :goto_0

    :pswitch_1
    sput v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00ea

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    sput v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00eb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    sput v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private selectedMsgHasLocked()Z
    .locals 4

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v2}, Lcom/android/mms/ui/FolderViewListAdapter;->getBackUpItemList()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/FolderViewList;->isMsgLocked(J)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setBoxTitle(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mgViewID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00e9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00ea

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00eb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setupActionBar()V
    .locals 7

    const/16 v6, 0x10

    const/4 v5, -0x2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040028

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v5, v5, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0f00b3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mCountTextView:Landroid/widget/TextView;

    const v2, 0x7f0f00b1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mFolderSpinner:Landroid/view/View;

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mSpinnerTextView:Landroid/widget/TextView;

    const v3, 0x7f0b00e9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mFolderSpinner:Landroid/view/View;

    new-instance v3, Lcom/android/mms/ui/FolderViewList$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/FolderViewList$1;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showSimInfoSelectDialog()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v2, Lcom/android/mms/ui/SiminfoSelectedActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "FolderViewList"

    const-string v2, "showSimInfoSelectDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xb4

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startAsyncQuery()V
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mNeedQuery:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mIsQuerying:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    const-string v1, "FolderViewList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startAsyncQuery mgViewID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x457

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/mms/data/FolderView;->startQueryForInboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    const/16 v1, 0x213

    invoke-static {p0, v1}, Lcom/android/mms/transaction/MessagingNotification;->cancelNotification(Landroid/content/Context;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x461

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/mms/data/FolderView;->startQueryForOutBoxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x3f1

    invoke-static {v1, v2}, Lcom/android/mms/data/FolderView;->startQueryForDraftboxView(Landroid/content/AsyncQueryHandler;I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x46b

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/mms/data/FolderView;->startQueryForSentboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private startAsyncQuery(I)V
    .locals 4
    .param p1    # I

    :try_start_0
    const-string v1, "FolderViewList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startAsyncQuery(int iPostTime) mgViewID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x457

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/android/mms/data/FolderView;->startQueryForInboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V

    const/16 v1, 0x213

    invoke-static {p0, v1}, Lcom/android/mms/transaction/MessagingNotification;->cancelNotification(Landroid/content/Context;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x461

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/android/mms/data/FolderView;->startQueryForOutBoxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x3f1

    invoke-static {v1, v2, p1}, Lcom/android/mms/data/FolderView;->startQueryForDraftboxView(Landroid/content/AsyncQueryHandler;II)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v2, 0x46b

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/android/mms/data/FolderView;->startQueryForSentboxView(Landroid/content/AsyncQueryHandler;ILjava/lang/String;I)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v1, 0xb4

    const/4 v0, -0x1

    if-ne p1, v1, :cond_1

    if-ne p2, v0, :cond_1

    const-string v0, "sim_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult where="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_0

    if-ne p2, v0, :cond_0

    const-string v0, "delete_flag"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mNeedUpdateListView:Z

    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult mNeedUpdateListView ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/mms/ui/FolderViewList;->mNeedUpdateListView:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onAddContactButtonClickInt(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0085

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v3, -0x1

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0087

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/mms/ui/FolderViewList$3;

    invoke-direct {v5, p0, p1}, Lcom/android/mms/ui/FolderViewList$3;-><init>(Lcom/android/mms/ui/FolderViewList;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v3, -0x2

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0086

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/mms/ui/FolderViewList$4;

    invoke-direct {v5, p0, p1}, Lcom/android/mms/ui/FolderViewList$4;-><init>(Lcom/android/mms/ui/FolderViewList;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v2, 0x7f04002f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    new-instance v2, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v2, Lcom/android/mms/ui/FolderViewList$MultiSelectOnLongClickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/mms/ui/FolderViewList$MultiSelectOnLongClickListener;-><init>(Lcom/android/mms/ui/FolderViewList;Lcom/android/mms/ui/FolderViewList$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    if-eqz p1, :cond_1

    sget-object v2, Lcom/android/mms/ui/FolderViewList;->BOXTYPE:Ljava/lang/String;

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    sget-object v2, Lcom/android/mms/ui/FolderViewList;->NEED_RESTORE_ADAPTER_STATE:Ljava/lang/String;

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/mms/ui/FolderViewList;->mIsNeedRestoreAdapterState:Z

    :goto_0
    const v2, 0x7f0f0075

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    iput-object p0, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->initListAdapter()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->initSpinnerListAdapter()V

    const-string v2, ""

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const-string v2, "FolderViewList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate, mgViewID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-direct {p0, v2}, Lcom/android/mms/ui/FolderViewList;->setBoxTitle(I)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mListItemLockInfo:Ljava/util/Map;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    iput v2, p0, Lcom/android/mms/ui/FolderViewList;->mFontScale:F

    const-string v2, "FolderViewList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "system fontscale is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/mms/ui/FolderViewList;->mFontScale:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lcom/android/mms/ui/FolderViewList;->mFontScale:F

    const v3, 0x3f8ccccd

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/mms/ui/FolderViewListAdapter;->setSubjectSingleLineMode(Z)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "floderview_key"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    iput-boolean v4, p0, Lcom/android/mms/ui/FolderViewList;->mIsNeedRestoreAdapterState:Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "FolderViewList"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v1, 0x3f1

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v1, 0x457

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v1, 0x461

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v1, 0x46b

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    if-eqz v0, :cond_1

    const-string v0, "FolderViewList"

    const-string v1, "clear mListAdapter"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_1
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void
.end method

.method public onDraftChanged(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    const-string v0, "FolderViewList"

    const-string v1, "Override onDraftChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mQueryHandler:Lcom/android/mms/ui/FolderViewList$ThreadListQueryHandler;

    const/16 v1, 0x3f1

    invoke-static {v0, v1}, Lcom/android/mms/data/FolderView;->startQueryForDraftboxView(Landroid/content/AsyncQueryHandler;I)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->moveTaskToBack(Z)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    iget-boolean v1, p0, Lcom/android/mms/ui/FolderViewList;->mDisableSearchFlag:Z

    if-eqz v1, :cond_0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 16
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-virtual/range {p0 .. p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    if-nez v1, :cond_1

    const-string v12, "FolderViewList"

    const-string v13, "cursor == null"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v12, 0x6

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v12, 0x0

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    int-to-long v12, v8

    invoke-static {v11, v12, v13}, Lcom/android/mms/ui/FolderViewListAdapter;->getKey(IJ)J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/FolderViewList;->mModeCallBack:Lcom/android/mms/ui/FolderViewList$ModeCallback;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v12, v5, v6}, Lcom/android/mms/ui/FolderViewListAdapter;->isContainItemId(J)Z

    move-result v12

    if-nez v12, :cond_2

    const/4 v12, 0x1

    :goto_1
    invoke-virtual {v13, v5, v6, v12}, Lcom/android/mms/ui/FolderViewList$ModeCallback;->setItemChecked(JZ)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v12}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    :cond_3
    const-string v12, "FolderViewList"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "messageid ="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "  mgViewID = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget v14, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v12, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_4

    const/4 v12, 0x1

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    move-object/from16 v0, p0

    invoke-static {v0, v9, v10}, Lcom/android/mms/ui/ComposeMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v4

    const-string v12, "folderbox"

    sget v13, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v4, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v12, "hiderecipient"

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v12, "showinput"

    const/4 v13, 0x1

    invoke-virtual {v4, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    const/4 v12, 0x1

    if-ne v11, v12, :cond_5

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v13, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v12, Lcom/android/mms/ui/FolderViewList;->SMS_URI:Landroid/net/Uri;

    int-to-long v13, v8

    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v12, "msg_type"

    const/4 v13, 0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v12, "folderbox"

    sget v13, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v12, 0xb4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_5
    const/4 v12, 0x3

    if-ne v11, v12, :cond_6

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v13, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v12, Lcom/android/mms/ui/FolderViewList;->WAPPUSH_URI:Landroid/net/Uri;

    int-to-long v13, v8

    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v12, "msg_type"

    const/4 v13, 0x3

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v12, "folderbox"

    sget v13, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    const/4 v12, 0x4

    if-ne v11, v12, :cond_7

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v13, Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v12, Lcom/android/mms/ui/FolderViewList;->CB_URI:Landroid/net/Uri;

    int-to-long v13, v8

    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v12, "msg_type"

    const/4 v13, 0x4

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v12, "folderbox"

    sget v13, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const/4 v12, 0x2

    if-ne v11, v12, :cond_0

    const-string v12, "FolderViewList"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "TYPE1 = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/16 v14, 0x9

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "   mgViewID="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget v14, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v12, 0x9

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/16 v13, 0x82

    if-ne v12, v13, :cond_9

    invoke-static {}, Lcom/android/mms/util/DownloadManager;->getInstance()Lcom/android/mms/util/DownloadManager;

    move-result-object v2

    sget-object v12, Lcom/android/mms/ui/FolderViewList;->MMS_URI:Landroid/net/Uri;

    int-to-long v13, v8

    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/android/mms/util/DownloadManager;->getState(Landroid/net/Uri;)I

    move-result v7

    const/16 v12, 0x81

    if-eq v7, v12, :cond_8

    new-instance v12, Lcom/android/mms/ui/FolderViewList$DownloadMessageListener;

    sget-object v13, Lcom/android/mms/ui/FolderViewList;->MMS_URI:Landroid/net/Uri;

    int-to-long v14, v8

    invoke-static {v13, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    const/16 v14, 0xa

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v13, v14, v8}, Lcom/android/mms/ui/FolderViewList$DownloadMessageListener;-><init>(Lcom/android/mms/ui/FolderViewList;Landroid/net/Uri;II)V

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/mms/ui/FolderViewList;->confirmDownloadDialog(Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const v13, 0x7f0b00f7

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_9
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v13, Lcom/android/mms/ui/MmsPlayerActivity;

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v12, Lcom/android/mms/ui/FolderViewList;->MMS_URI:Landroid/net/Uri;

    int-to-long v13, v8

    invoke-static {v12, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v12, "dirmode"

    const/4 v13, 0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v12, "folderbox"

    sget v13, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v12, 0xb4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    const-string v0, "floderview_key"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const-string v0, "FolderViewList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewIntent, mgViewID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-direct {p0, v0}, Lcom/android/mms/ui/FolderViewList;->setBoxTitle(I)V

    sget v0, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/android/mms/data/FolderView;->markFailedSmsMmsSeen(Landroid/content/Context;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1    # Landroid/view/MenuItem;

    const/4 v13, -0x1

    const/4 v10, 0x0

    const/high16 v12, 0x10000000

    const/4 v11, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :goto_0
    return v11

    :sswitch_0
    invoke-static {v10}, Lcom/android/mms/MmsConfig;->setMmsDirMode(Z)V

    invoke-static {p0}, Lcom/android/mms/ui/MessageUtils;->updateNotification(Landroid/content/Context;)V

    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/android/mms/ui/ConversationList;

    invoke-direct {v8, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    new-instance v2, Landroid/content/Intent;

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewList;->context:Landroid/content/Context;

    const-class v9, Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "folderbox"

    sget v9, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    new-instance v6, Landroid/content/Intent;

    const-class v8, Lcom/android/mms/ui/SettingListActivity;

    invoke-direct {v6, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v6, v13}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;I)Z

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->showSimInfoSelectDialog()V

    goto :goto_0

    :sswitch_4
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v8, "com.mediatek.omacp"

    const-string v9, "com.mediatek.omacp.message.OmacpMessageList"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v4, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v4, v13}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;I)Z

    goto :goto_0

    :sswitch_5
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->confirmDeleteMessageDialog()V

    goto :goto_0

    :sswitch_7
    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-le v8, v11, :cond_0

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v8, Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v8, "preference"

    const-string v9, "pref_key_manage_sim_messages"

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-ne v8, v11, :cond_1

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v8, Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v9, "SlotId"

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v8}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlot()I

    move-result v8

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    const v8, 0x7f0b006d

    invoke-static {p0, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v8, Landroid/content/ComponentName;

    const-string v9, "com.android.cellbroadcastreceiver"

    const-string v10, "com.android.cellbroadcastreceiver.CellBroadcastListActivity"

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {v0, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v8, "FolderViewList"

    const-string v9, "ActivityNotFoundException for CellBroadcastListActivity"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_0
        0x4 -> :sswitch_7
        0x7f0f0195 -> :sswitch_1
        0x7f0f0199 -> :sswitch_3
        0x7f0f019b -> :sswitch_2
        0x7f0f019c -> :sswitch_4
        0x7f0f019d -> :sswitch_8
        0x7f0f019f -> :sswitch_5
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 21
    .param p1    # Landroid/view/Menu;

    invoke-interface/range {p1 .. p1}, Landroid/view/Menu;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v17

    if-lez v17, :cond_0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const v20, 0x7f0b02bc

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v17

    const v18, 0x7f0e0001

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v17, 0x7f0f019a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    const v17, 0x7f0f019e

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    const v17, 0x7f0f0197

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/mms/ui/FolderViewList;->mSearchItem:Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mSearchItem:Landroid/view/MenuItem;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/SearchView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v17, v0

    const v18, 0x7f0b0303

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    const-string v17, "search"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/app/SearchManager;

    if-eqz v15, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/FolderViewList;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    :cond_1
    const v17, 0x7f0f019d

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x1110044

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    if-eqz v9, :cond_2

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const-string v17, "com.android.cellbroadcastreceiver"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    const/4 v9, 0x0

    :cond_2
    :goto_0
    if-nez v9, :cond_3

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    const/16 v17, 0x0

    const/16 v18, 0x1

    const/16 v19, 0x0

    const v20, 0x7f0b00ee

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/16 v17, 0x0

    const/16 v18, 0x4

    const/16 v19, 0x0

    const v20, 0x7f0b003e

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v17

    const v18, 0x7f0200cf

    invoke-interface/range {v17 .. v18}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    invoke-static/range {p0 .. p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_4

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_5

    :cond_4
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const-string v17, "FolderViewList"

    const-string v18, "onPrepareOptionsMenu MenuItem setEnabled(false)"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    if-eqz v11, :cond_6

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_6

    sget v17, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    const v17, 0x7f0f0199

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_6
    const v17, 0x7f0f019c

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v13, 0x0

    :try_start_1
    const-string v17, "com.mediatek.omacp"

    const/16 v18, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    :goto_1
    if-eqz v13, :cond_7

    const-string v17, "omacp"

    const/16 v18, 0x5

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    const-string v17, "configuration_msg_exist"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_7

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_7
    const/16 v17, 0x1

    return v17

    :catch_0
    move-exception v7

    const/4 v9, 0x0

    goto/16 :goto_0

    :catch_1
    move-exception v6

    const-string v17, "Mms/FolderViewList"

    const-string v18, "ConversationList NotFoundContext"

    invoke-static/range {v17 .. v18}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    sget-object v0, Lcom/android/mms/ui/FolderViewList;->ACTIONMODE:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mms/ui/FolderViewList;->mSELECT_ITEM_IDS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListSelectedItem:[J

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mModeCallBack:Lcom/android/mms/ui/FolderViewList$ModeCallback;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    const-string v0, "FolderViewList"

    const-string v1, "onRestoreInstanceState: start actionMode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/IAppGuideExt;

    if-eqz v0, :cond_0

    const-string v1, "MMS"

    invoke-interface {v0, v1}, Lcom/mediatek/mms/ext/IAppGuideExt;->showAppGuide(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    if-eqz v4, :cond_1

    const-string v4, "FolderViewList"

    const-string v5, "onSaveInstanceState: mActionMode not null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/android/mms/ui/FolderViewList;->ACTIONMODE:Ljava/lang/String;

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v4, Lcom/android/mms/ui/FolderViewList;->BOXTYPE:Ljava/lang/String;

    sget v5, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "FolderViewList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSaveInstanceState    mgViewID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/android/mms/ui/FolderViewList;->NEED_RESTORE_ADAPTER_STATE:Ljava/lang/String;

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/FolderViewListAdapter;->getBackUpItemList()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/Long;

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Long;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Long;

    array-length v4, v2

    new-array v3, v4, [J

    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/android/mms/ui/FolderViewList;->mSELECT_ITEM_IDS:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    const-string v4, "FolderViewList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSaveInstanceState--selectItemIds:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 7

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    if-eqz v3, :cond_0

    const-string v3, "FolderViewList"

    const-string v4, "set OnContentChangedListener"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList;->mContentChangedListener:Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/FolderViewListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;)V

    :cond_0
    iget-boolean v3, p0, Lcom/android/mms/ui/FolderViewList;->mNeedUpdateListView:Z

    if-eqz v3, :cond_1

    const-string v3, "FolderViewList"

    const-string v4, "onStart mNeedUpdateListView"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    invoke-virtual {v3, v5}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/mms/ui/FolderViewList;->mNeedUpdateListView:Z

    :cond_1
    invoke-static {v6}, Lcom/android/mms/MmsConfig;->setMmsDirMode(Z)V

    invoke-static {v6}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsDialogNotify;->closeMsgDialog()V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/android/mms/util/DraftCache;->addOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/util/DraftCache;->refresh()V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getSimCardInfo()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_4

    iput-object v5, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    :cond_2
    :goto_0
    iput-boolean v6, p0, Lcom/android/mms/ui/FolderViewList;->mNeedToMarkAsSeen:Z

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList;->startAsyncQuery()V

    iput-boolean v6, p0, Lcom/android/mms/ui/FolderViewList;->mIsInActivity:Z

    const-string v3, "FolderViewList.onStart"

    invoke-static {p0, v3}, Lcom/android/mms/MmsConfig;->printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    if-lez v1, :cond_3

    add-int/lit8 v3, v1, -0x1

    invoke-static {p0, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sim_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/FolderViewList;->where:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/FolderViewList;->mIsInActivity:Z

    invoke-static {}, Lcom/android/mms/data/Contact;->invalidateCache()V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mms/util/DraftCache;->removeOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    if-eqz v0, :cond_0

    const-string v0, "FolderViewList"

    const-string v1, "clear OnContentChangedListener"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList;->mListAdapter:Lcom/android/mms/ui/FolderViewListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/FolderViewListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/FolderViewListAdapter$OnContentChangedListener;)V

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/widget/MmsWidgetProvider;->notifyDatasetChanged(Landroid/content/Context;)V

    return-void
.end method
