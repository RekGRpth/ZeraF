.class public Lcom/android/mms/ui/MultiSaveActivity;
.super Landroid/app/Activity;
.source "MultiSaveActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MultiSaveActivity"


# instance fields
.field private mActionBarText:Landroid/widget/Button;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

.field private mMultiSaveList:Landroid/widget/ListView;

.field private mSelectionItem:Landroid/view/MenuItem;

.field private mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

.field private needQuit:Z

.field private smode:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->needQuit:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MultiSaveActivity;)Lcom/android/mms/ui/MultiSaveListAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->cancelSelectAll()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->selectAll()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/mms/ui/MultiSaveActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->save()V

    return-void
.end method

.method private cancelSelectAll()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    :cond_0
    return-void
.end method

.method private copyMedia()Z
    .locals 9

    const/4 v4, 0x1

    iget-object v7, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListAdapter;->getItemList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->isSelected()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->getPduPart()Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v7}, Lcom/android/mms/ui/MultiSaveListItemData;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    invoke-static {v6}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isImageType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "application/ogg"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v3}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".dcf"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    invoke-direct {p0, v3, v0}, Lcom/android/mms/ui/MultiSaveActivity;->copyPart(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z

    move-result v7

    and-int/2addr v4, v7

    goto :goto_1

    :cond_3
    const-string v7, "text/plain"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "text/html"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_4
    invoke-direct {p0, v3, v0}, Lcom/android/mms/ui/MultiSaveActivity;->copyPartNoUri(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z

    move-result v7

    and-int/2addr v4, v7

    goto :goto_1

    :cond_5
    return v4
.end method

.method private copyPart(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z
    .locals 14
    .param p1    # Lcom/google/android/mms/pdu/PduPart;
    .param p2    # Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v9

    const-string v11, "Mms/MultiSaveActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "copyPart, copy part into sdcard uri "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v5, 0x0

    :try_start_0
    iget-object v11, p0, Lcom/android/mms/ui/MultiSaveActivity;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v11, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v7

    instance-of v11, v7, Ljava/io/FileInputStream;

    if-eqz v11, :cond_5

    move-object v0, v7

    check-cast v0, Ljava/io/FileInputStream;

    move-object v4, v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-static {v0, v11}, Lcom/android/mms/ui/MessageUtils;->getStorageFile(Ljava/lang/String;Landroid/content/Context;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_2

    if-eqz v7, :cond_0

    :try_start_1
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    :cond_0
    if-eqz v5, :cond_1

    :try_start_2
    #Replaced unresolvable odex instruction with a throw
    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    :cond_1
    :goto_0
    return v10

    :cond_2
    :try_start_3
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/16 v11, 0x1f40

    :try_start_4
    new-array v1, v11, [B

    const/4 v8, 0x0

    :goto_1
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v11, -0x1

    if-eq v8, v11, :cond_4

    const/4 v11, 0x0

    invoke-virtual {v6, v1, v11, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v5, v6

    :goto_2
    :try_start_5
    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while opening or reading stream"

    invoke-static {v11, v12, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v7, :cond_3

    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_3
    if-eqz v5, :cond_1

    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    :goto_3
    invoke-static {v11, v12, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    :try_start_8
    new-instance v11, Landroid/content/Intent;

    const-string v12, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v11}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object v5, v6

    :cond_5
    if-eqz v7, :cond_6

    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :cond_6
    if-eqz v5, :cond_7

    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    :cond_7
    const/4 v10, 0x1

    goto :goto_0

    :catchall_0
    move-exception v11

    :goto_4
    if-eqz v7, :cond_8

    :try_start_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    :cond_8
    if-eqz v5, :cond_9

    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3

    :cond_9
    throw v11

    :catch_2
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    :goto_5
    invoke-static {v11, v12, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_3
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_3

    :catch_4
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_5

    :catch_5
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_5

    :catch_6
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_3

    :catch_7
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_5

    :catch_8
    move-exception v2

    const-string v11, "Mms/MultiSaveActivity"

    const-string v12, "IOException caught while closing stream"

    goto :goto_3

    :catchall_1
    move-exception v11

    move-object v5, v6

    goto :goto_4

    :catch_9
    move-exception v2

    goto :goto_2
.end method

.method private copyPartNoUri(Lcom/google/android/mms/pdu/PduPart;Ljava/lang/String;)Z
    .locals 8
    .param p1    # Lcom/google/android/mms/pdu/PduPart;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/android/mms/ui/MessageUtils;->getStorageFile(Ljava/lang/String;Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "default file is null"

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    #Replaced unresolvable odex instruction with a throw
    throw v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v7

    array-length v7, v7

    invoke-virtual {v3, v5, v6, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_2

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_2
    const/4 v4, 0x1

    move-object v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_5
    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "IOException caught while opening or reading stream"

    invoke-static {v5, v6, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_0

    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "IOException caught while closing stream"

    :goto_2
    invoke-static {v5, v6, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_3
    throw v5

    :catch_2
    move-exception v0

    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "IOException caught while closing stream"

    goto :goto_2

    :catch_3
    move-exception v0

    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "IOException caught while closing stream"

    goto :goto_2

    :catch_4
    move-exception v0

    const-string v5, "Mms/MultiSaveActivity"

    const-string v6, "IOException caught while closing stream"

    invoke-static {v5, v6, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    goto :goto_0

    :catchall_1
    move-exception v5

    move-object v2, v3

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method private initActivityState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    const-string v2, "is_all_selected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "select_list"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v1}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    goto :goto_0

    :cond_2
    const-string v2, "Mms/MultiSaveActivity"

    const-string v3, "initActivityState, fresh start select all"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    goto :goto_0
.end method

.method private initListAdapter(J)V
    .locals 26
    .param p1    # J

    invoke-static/range {p0 .. p2}, Lcom/android/mms/ui/PduBodyCache;->getPduBody(Landroid/content/Context;J)Lcom/google/android/mms/pdu/PduBody;

    move-result-object v12

    const/4 v3, 0x5

    invoke-static {v3}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    const/16 v21, 0x0

    if-nez v12, :cond_0

    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "initListAdapter, oops, getPduBody returns null"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v12}, Lcom/google/android/mms/pdu/PduBody;->getPartsNum()I

    move-result v24

    new-instance v11, Ljava/util/ArrayList;

    move/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/android/mms/model/SlideshowModel;->createFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;)Lcom/android/mms/model/SlideshowModel;
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/android/mms/model/SlideshowModel;->getAttachFiles()Ljava/util/ArrayList;

    move-result-object v10

    const/16 v18, 0x0

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_11

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/google/android/mms/pdu/PduBody;->getPart(I)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getContentLocation()[B

    move-result-object v14

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getName()[B

    move-result-object v23

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getContentId()[B

    move-result-object v13

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getFilename()[B

    move-result-object v17

    const/16 v16, 0x0

    const/16 v22, 0x0

    if-eqz v14, :cond_5

    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Ljava/lang/String;-><init>([B)V

    :goto_2
    move-object/from16 v22, v16

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_a

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "part Uri = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    move-object/from16 v0, v22

    invoke-static {v3, v0}, Lcom/android/mms/ui/MessageUtils;->getContentType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    if-eqz v20, :cond_f

    if-eqz v22, :cond_f

    invoke-interface/range {v20 .. v20}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_d

    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "In multisave initList"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "smode = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    const-wide/16 v6, 0x1

    cmp-long v3, v3, v6

    if-nez v3, :cond_b

    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "save all attachment including slides"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isImageType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "application/ogg"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v5}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const-string v3, "text/plain"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "text/html"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Lcom/android/mms/ui/MultiSaveListItemData;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v3, v0, v5, v1, v2}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;J)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "smode = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/16 v19, 0x0

    :goto_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v19

    if-ge v0, v3, :cond_9

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/model/FileAttachmentModel;

    invoke-virtual {v3}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "text/plain"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "text/html"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v8, 0x0

    new-instance v3, Lcom/android/mms/ui/MultiSaveListItemData;

    move-object/from16 v4, p0

    move-wide/from16 v6, p1

    invoke-direct/range {v3 .. v8}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;JI)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    :catch_0
    move-exception v15

    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "Create from pdubody exception!"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    if-eqz v23, :cond_6

    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto/16 :goto_2

    :cond_6
    if-eqz v13, :cond_7

    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/lang/String;-><init>([B)V

    goto/16 :goto_2

    :cond_7
    if-eqz v17, :cond_8

    new-instance v16, Ljava/lang/String;

    invoke-direct/range {v16 .. v17}, Ljava/lang/String;-><init>([B)V

    goto/16 :goto_2

    :cond_8
    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "initListAdapter: filename = null,continue"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    :goto_4
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    :cond_a
    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "PartUri = null"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-nez v3, :cond_2

    const-string v3, "Mms/MultiSaveActivity"

    const-string v4, "Only save attachment files no including slides"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x0

    :goto_5
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v19

    if-ge v0, v3, :cond_2

    if-eqz v9, :cond_c

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/model/FileAttachmentModel;

    invoke-virtual {v3}, Lcom/android/mms/model/FileAttachmentModel;->getUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_c

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/model/FileAttachmentModel;

    invoke-virtual {v3}, Lcom/android/mms/model/FileAttachmentModel;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    new-instance v3, Lcom/android/mms/ui/MultiSaveListItemData;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v3, v0, v5, v1, v2}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;J)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    add-int/lit8 v19, v19, 0x1

    goto :goto_5

    :cond_d
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isImageType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "application/ogg"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    invoke-static {v5}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v3

    if-nez v3, :cond_e

    if-eqz v22, :cond_9

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, ".dcf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_e
    new-instance v3, Lcom/android/mms/ui/MultiSaveListItemData;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v3, v0, v5, v1, v2}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;J)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_f
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isImageType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, "application/ogg"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    invoke-static/range {v25 .. v25}, Lcom/mediatek/encapsulation/com/google/android/mms/EncapsulatedContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    invoke-static {v5}, Lcom/android/mms/model/FileAttachmentModel;->isSupportedFile(Lcom/google/android/mms/pdu/PduPart;)Z

    move-result v3

    if-nez v3, :cond_10

    if-eqz v22, :cond_9

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, ".dcf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_10
    new-instance v3, Lcom/android/mms/ui/MultiSaveListItemData;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v3, v0, v5, v1, v2}, Lcom/android/mms/ui/MultiSaveListItemData;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/PduPart;J)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_11
    invoke-virtual {v11}, Ljava/util/ArrayList;->trimToSize()V

    new-instance v3, Lcom/android/mms/ui/MultiSaveListAdapter;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v11}, Lcom/android/mms/ui/MultiSaveListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0
.end method

.method private markCheckedState(Z)V
    .locals 6
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "markCheckState count is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/MultiSaveListItem;

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/MultiSaveListItem;->selectItem(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private save()V
    .locals 8

    iget-object v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->copyMedia()Z

    move-result v3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v4, "multi_save_result"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v4, -0x1

    invoke-virtual {p0, v4, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v4, 0x5

    invoke-static {v4}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    const-string v4, "Mms/MultiSaveActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMmsAttachmentEnhancePlugin = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    const-string v4, "Mms/MultiSaveActivity"

    const-string v5, "OUT MMS_SAVE_OTHER_ATTACHMENT"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    const-string v4, "Mms/MultiSaveActivity"

    const-string v5, "IN MMS_SAVE_OTHER_ATTACHMENT"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_1

    const v2, 0x7f0b02f3

    :goto_0
    const/4 v4, 0x0

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    const v2, 0x7f0b02f4

    goto :goto_0
.end method

.method private selectAll()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/android/mms/ui/MultiSaveActivity;->markCheckedState(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/android/mms/ui/MultiSaveListAdapter;->setItemsValue(Z[I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    :cond_0
    return-void
.end method

.method private setUpActionBar()V
    .locals 9

    const/4 v8, -0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v5, 0x10

    const/16 v6, 0x1a

    invoke-virtual {v0, v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v1, Lcom/android/mms/ui/CustomMenu;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CustomMenu;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040048

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v5, Landroid/app/ActionBar$LayoutParams;

    const/16 v6, 0x77

    invoke-direct {v5, v8, v8, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v2, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v5, 0x7f0f0072

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/Button;

    iget-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/Button;

    const v6, 0x7f0e0008

    invoke-virtual {v1, v5, v6}, Lcom/android/mms/ui/CustomMenu;->addDropDownMenu(Landroid/widget/Button;I)Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    move-result-object v5

    iput-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    iget-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    const v6, 0x7f0f01ab

    invoke-virtual {v5, v6}, Lcom/android/mms/ui/CustomMenu$DropDownMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    new-instance v5, Lcom/android/mms/ui/MultiSaveActivity$2;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiSaveActivity$2;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v1, v5}, Lcom/android/mms/ui/CustomMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    const v5, 0x7f0f0137

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v5, Lcom/android/mms/ui/MultiSaveActivity$3;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiSaveActivity$3;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f0f0138

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    new-instance v5, Lcom/android/mms/ui/MultiSaveActivity$4;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiSaveActivity$4;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->updateActionBarText()V

    return-void
.end method

.method private updateActionBarText()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mActionBarText:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MultiSaveListAdapter;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    const v1, 0x7f0b00ae

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mms/ui/MultiSaveActivity;->mSelectionItem:Landroid/view/MenuItem;

    const v1, 0x7f0b0072

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v4, 0x5

    invoke-static {v4}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    const v4, 0x7f0b030c

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setTitle(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->mContentResolver:Landroid/content/ContentResolver;

    const v4, 0x7f040046

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f0f0131

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->mMultiSaveList:Landroid/widget/ListView;

    new-instance v5, Lcom/android/mms/ui/MultiSaveActivity$1;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiSaveActivity$1;-><init>(Lcom/android/mms/ui/MultiSaveActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-wide/16 v2, -0x1

    if-eqz v0, :cond_0

    const-string v4, "msgid"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "msgid"

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->getSaveAttachMode(Landroid/content/Intent;)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/android/mms/ui/MultiSaveActivity;->smode:J

    :cond_0
    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/MultiSaveActivity;->initListAdapter(J)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiSaveActivity;->initActivityState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiSaveActivity;->setUpActionBar()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-string v3, "Mms/MultiSaveActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSaveInstanceState, with bundle "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->isAllSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "is_all_selected"

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getSelectedNumber()I

    move-result v3

    new-array v0, v3, [I

    iget-object v3, p0, Lcom/android/mms/ui/MultiSaveActivity;->mListAdapter:Lcom/android/mms/ui/MultiSaveListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListAdapter;->getItemList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/ui/MultiSaveListItemData;

    invoke-virtual {v3}, Lcom/android/mms/ui/MultiSaveListItemData;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    aput v1, v0, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string v3, "select_list"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_0
.end method
