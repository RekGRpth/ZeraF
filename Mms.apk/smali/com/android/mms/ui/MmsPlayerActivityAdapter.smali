.class public Lcom/android/mms/ui/MmsPlayerActivityAdapter;
.super Landroid/widget/BaseAdapter;
.source "MmsPlayerActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;
    }
.end annotation


# static fields
.field private static final LOCAL_LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "Mms/MmsPlayerAdapter"


# instance fields
.field private mAllCount:I

.field private mContext:Landroid/content/Context;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private final mFactory:Landroid/view/LayoutInflater;

.field private mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mListItemViewCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private parser:Lcom/android/mms/util/SmileyParser2;

.field private textSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-static {}, Lcom/android/mms/util/SmileyParser2;->getInstance()Lcom/android/mms/util/SmileyParser2;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->parser:Lcom/android/mms/util/SmileyParser2;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mFactory:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mAllCount:I

    iput-object p1, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MmsPlayerActivityAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MmsPlayerActivityAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public clearAllCache()V
    .locals 5

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v4, 0x7f0f0089

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/encapsulation/com/mediatek/banyan/widget/EncapsulatedMTKImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mediatek/banyan/widget/MTKImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mAllCount:I

    return v0
.end method

.method public getItem(I)Lcom/android/mms/ui/MmsPlayerActivityItemData;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->getItem(I)Lcom/android/mms/ui/MmsPlayerActivityItemData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getListItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mms/ui/MmsPlayerActivityItemData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 33
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "getView, for position "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    move-object/from16 v28, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/mms/ui/MmsPlayerActivityItem;

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    invoke-virtual/range {v28 .. v28}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getText()Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_0

    invoke-virtual {v12}, Lcom/android/mms/ui/MmsPlayerActivityItem;->getCurrentTextView()Landroid/widget/TextView;

    move-result-object v20

    const-string v29, "Mms/MmsPlayerAdapter"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "getView(): text view is null? "

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    if-nez v20, :cond_1

    const/16 v28, 0x1

    :goto_0
    move-object/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-eqz v28, :cond_0

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "getView(): before set text size, textSize = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getTextSize()F

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v28, v0

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "getView(): after set text size, textSize = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getTextSize()F

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v28, "Mms/MmsPlayerAdapter"

    const-string v29, "getView(): from cache."

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v13, v12

    :goto_1
    return-object v13

    :cond_1
    const/16 v28, 0x0

    goto :goto_0

    :cond_2
    const-string v28, "Mms/MmsPlayerAdapter"

    const-string v29, "getView(): create new one."

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mFactory:Landroid/view/LayoutInflater;

    move-object/from16 v28, v0

    const v29, 0x7f040040

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/android/mms/ui/MmsPlayerActivityItem;

    const v28, 0x7f0f0108

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    const v28, 0x7f0f0089

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/mediatek/encapsulation/com/mediatek/banyan/widget/EncapsulatedMTKImageView;

    const v28, 0x7f0f010d

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    const v28, 0x7f0f010e

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    const v28, 0x7f0f008a

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const v28, 0x7f0f0012

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    const v28, 0x7f0f010a

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    const v28, 0x7f0f010b

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItem:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/mms/ui/MmsPlayerActivityItemData;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f0b00fc

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    add-int/lit8 v32, p1, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    invoke-virtual/range {v28 .. v30}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageType()Ljava/lang/String;

    move-result-object v8

    if-eqz v9, :cond_6

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "set image: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/mediatek/banyan/widget/MTKImageView;->setImageURI(Landroid/net/Uri;)V

    const/16 v28, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v28, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    new-instance v28, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;

    const-string v29, ""

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v9, v8, v2}, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;-><init>(Lcom/android/mms/ui/MmsPlayerActivityAdapter;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getVideoThumbnail()Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getVideoUri()Landroid/net/Uri;

    move-result-object v27

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getVideoType()Ljava/lang/String;

    move-result-object v26

    if-eqz v23, :cond_7

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "set video: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v28, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v28, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    new-instance v28, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;

    const-string v29, ""

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    move-object/from16 v3, v26

    move-object/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;-><init>(Lcom/android/mms/ui/MmsPlayerActivityAdapter;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getAudioName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getAudioUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getAudioType()Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_8

    const-string v28, "Mms/MmsPlayerAdapter"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "show audio name:"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v28, 0x41900000

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    const/16 v28, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v28, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v28, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    new-instance v28, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7, v6, v5}, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;-><init>(Lcom/android/mms/ui/MmsPlayerActivityAdapter;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getText()Ljava/lang/String;

    move-result-object v24

    if-nez v9, :cond_3

    if-eqz v23, :cond_4

    :cond_3
    if-eqz v24, :cond_4

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v28

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v29

    sub-int v28, v28, v29

    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->abs(I)I

    move-result v14

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoTop()I

    move-result v28

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextTop()I

    move-result v29

    sub-int v28, v28, v29

    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->abs(I)I

    move-result v25

    move/from16 v0, v25

    if-le v14, v0, :cond_c

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v28

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_a

    const v28, 0x7f0f010c

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v28

    if-lez v28, :cond_9

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    :cond_4
    :goto_5
    if-eqz v24, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->parser:Lcom/android/mms/util/SmileyParser2;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/mms/util/SmileyParser2;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v28

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-eqz v28, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    move/from16 v28, v0

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_5
    const/16 v28, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static/range {v20 .. v20}, Lcom/android/mms/MmsConfig;->setExtendUrlSpan(Landroid/widget/TextView;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lcom/android/mms/ui/MmsPlayerActivityItem;->setCurrentTextView(Landroid/widget/TextView;)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->mListItemViewCache:Ljava/util/HashMap;

    move-object/from16 v28, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v13, v12

    goto/16 :goto_1

    :cond_6
    const/16 v28, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    const/16 v28, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_8
    const/16 v28, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v28, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v28, 0x8

    move/from16 v0, v28

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_9
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoLeft()I

    move-result v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_a
    const v28, 0x7f0f010f

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v28

    if-lez v28, :cond_b

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextWidth()I

    move-result v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_b
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextLeft()I

    move-result v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getTextTop()I

    move-result v28

    invoke-virtual {v11}, Lcom/android/mms/ui/MmsPlayerActivityItemData;->getImageOrVideoTop()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_4

    const v28, 0x7f0f0110

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    goto/16 :goto_5

    :cond_d
    const/16 v28, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setTextSize(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->textSize:F

    return-void
.end method
