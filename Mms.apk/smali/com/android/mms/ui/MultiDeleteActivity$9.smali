.class Lcom/android/mms/ui/MultiDeleteActivity$9;
.super Ljava/lang/Object;
.source "MultiDeleteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiDeleteActivity;->forwardOneMms(Lcom/android/mms/ui/MessageItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;

.field final synthetic val$msgItem:Lcom/android/mms/ui/MessageItem;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MessageItem;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iput-object p2, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v4, v4, Lcom/android/mms/ui/MessageItem;->mType:Ljava/lang/String;

    const-string v5, "mms"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v2, Lcom/google/android/mms/pdu/SendReq;

    invoke-direct {v2}, Lcom/google/android/mms/pdu/SendReq;-><init>()V

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const v5, 0x7f0b0259

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v4, v4, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v5, v5, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v4, v4, Lcom/android/mms/ui/MessageItem;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->makeCopy()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setBody(Lcom/google/android/mms/pdu/PduBody;)V

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v4}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4402(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;Landroid/net/Uri;)Landroid/net/Uri;

    :try_start_0
    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v4}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v1

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v4}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Mms$Draft;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v7}, Lcom/android/mms/ui/MmsPreferenceActivity;->getIsGroupMmsEnabled(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {v1, v2, v5, v6, v7}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZZ)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4402(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v4}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v6

    invoke-static {v6}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4400(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/mms/transaction/MessagingNotification;->getThreadId(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4502(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;J)J
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v4, "Mms/MultiDeleteActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to copy message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v6, v6, Lcom/android/mms/ui/MessageItem;->mMessageUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity$9;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const v5, 0x7f0b0266

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
