.class public Lcom/android/mms/ui/WPMessageListItem;
.super Landroid/widget/LinearLayout;
.source "WPMessageListItem.java"

# interfaces
.implements Lcom/android/mms/data/Contact$UpdateListener;


# static fields
.field private static final DEFAULT_ICON_INDENT:I = 0x5

.field public static final EXTRA_URLS:Ljava/lang/String; = "com.android.mms.ExtraUrls"

.field private static final STYLE_BOLD:Landroid/text/style/StyleSpan;

.field private static final WP_TAG:Ljava/lang/String; = "Mms/WapPush"

.field private static sDefaultContactImage:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

.field private mBodyTextView:Landroid/widget/TextView;

.field mColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private mDateView:Landroid/widget/TextView;

.field private mDefaultCountryIso:Ljava/lang/String;

.field private mExpirationIndicator:Landroid/widget/ImageView;

.field private mFromView:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mIsLastItemInList:Z

.field private mItemContainer:Landroid/view/View;

.field private mLinkSpan:Landroid/text/style/ClickableSpan;

.field private mMessageItem:Lcom/android/mms/ui/WPMessageItem;

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mPresenceView:Landroid/widget/ImageView;

.field private mSpan:Landroid/text/style/LineHeightSpan;

.field private mUiHandler:Landroid/os/Handler;

.field private mUpdateFromViewRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/mms/ui/WPMessageListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Lcom/android/mms/ui/WPMessageListItem$1;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageListItem$1;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    new-instance v0, Lcom/android/mms/ui/WPMessageListItem$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageListItem$2;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mUiHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/mms/ui/WPMessageListItem$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/WPMessageListItem$3;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mUpdateFromViewRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    sget-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$1;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$2;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mms/ui/WPMessageListItem$3;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/WPMessageListItem$3;-><init>(Lcom/android/mms/ui/WPMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mUpdateFromViewRunnable:Ljava/lang/Runnable;

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    sget-object v1, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/WPMessageListItem;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/WPMessageListItem;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageListItem;->updateFromView()V

    return-void
.end method

.method private bindCommonMessage(Lcom/android/mms/ui/WPMessageItem;)V
    .locals 11
    .param p1    # Lcom/android/mms/ui/WPMessageItem;

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v2, p1, Lcom/android/mms/ui/WPMessageItem;->mContact:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/mms/ui/WPMessageItem;->mText:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/mms/ui/WPMessageItem;->mURL:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    iget-object v6, p1, Lcom/android/mms/ui/WPMessageItem;->mExpiration:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/mms/ui/WPMessageItem;->mHighlight:Ljava/util/regex/Pattern;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/mms/ui/WPMessageListItem;->formatMessage(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;

    move-result-object v8

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p1, Lcom/android/mms/ui/WPMessageItem;->mTimestamp:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mDateView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mFromView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mFromView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageListItem;->formatMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/android/mms/data/Contact;->addListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    iget v0, p1, Lcom/android/mms/ui/WPMessageItem;->mIsExpired:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private formatMessage()Ljava/lang/CharSequence;
    .locals 5

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v2, v3, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v4, 0x104000e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_0
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private formatMessage(Lcom/android/mms/ui/WPMessageItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Lcom/android/mms/ui/WPMessageItem;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/util/regex/Pattern;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/android/mms/util/SmileyParser2;->getInstance()Lcom/android/mms/util/SmileyParser2;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/android/mms/util/SmileyParser2;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageListItem;->mLinkSpan:Landroid/text/style/ClickableSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    if-eqz p7, :cond_2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private updateAvatarView()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    sget-object v0, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v3, v8}, Lcom/android/mms/ui/MmsQuickContactBadge;->setGroupAvator(Z)V

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    iget-object v3, v3, Lcom/android/mms/ui/WPMessageItem;->mAddress:Ljava/lang/String;

    invoke-static {v3, v7}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v1

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/android/mms/ui/WPMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    const-wide/16 v5, -0x1

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/android/mms/data/Contact;->getAvatar(Landroid/content/Context;Landroid/graphics/drawable/Drawable;J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v3, v2, v7}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    :goto_0
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->existsInDatabase()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    invoke-virtual {v3, v2, v7}, Landroid/widget/QuickContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private updateFromView()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mFromView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageListItem;->formatMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageListItem;->updateAvatarView()V

    return-void
.end method


# virtual methods
.method public bind(Lcom/android/mms/ui/WPMessageItem;Z)V
    .locals 3
    .param p1    # Lcom/android/mms/ui/WPMessageItem;
    .param p2    # Z

    const/4 v2, 0x0

    const-string v0, "Mms/WapPush"

    const-string v1, "bind msgItem"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    iput-boolean p2, p0, Lcom/android/mms/ui/WPMessageListItem;->mIsLastItemInList:Z

    invoke-virtual {p0, v2}, Landroid/view/View;->setLongClickable(Z)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/android/mms/ui/WPMessageListItem;->updateUnReadView()V

    invoke-virtual {p0}, Lcom/android/mms/ui/WPMessageListItem;->updateBackground()V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/WPMessageListItem;->bindCommonMessage(Lcom/android/mms/ui/WPMessageItem;)V

    invoke-direct {p0}, Lcom/android/mms/ui/WPMessageListItem;->updateAvatarView()V

    return-void
.end method

.method public getItemContainer()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mItemContainer:Landroid/view/View;

    return-object v0
.end method

.method public getMessageItem()Lcom/android/mms/ui/WPMessageItem;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0f006d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mFromView:Landroid/widget/TextView;

    const v0, 0x7f0f002d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    const v0, 0x7f0f0067

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPresenceView:Landroid/widget/ImageView;

    const v0, 0x7f0f006f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mDateView:Landroid/widget/TextView;

    const v0, 0x7f0f004e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/MmsQuickContactBadge;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mAvatarView:Lcom/android/mms/ui/MmsQuickContactBadge;

    const v0, 0x7f0f018e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mExpirationIndicator:Landroid/widget/ImageView;

    return-void
.end method

.method public onUpdate(Lcom/android/mms/data/Contact;)V
    .locals 3
    .param p1    # Lcom/android/mms/data/Contact;

    const-string v0, "Mms/WapPush"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " contact: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mUpdateFromViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageListItem;->mUpdateFromViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setMsgListItemHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageListItem;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method public unbind()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    return-void
.end method

.method protected updateBackground()V
    .locals 3

    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    invoke-virtual {v2}, Lcom/android/mms/ui/WPMessageItem;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    const v1, 0x7f02012a

    :goto_0
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    invoke-virtual {v2}, Lcom/android/mms/ui/WPMessageItem;->isUnread()Z

    move-result v2

    if-eqz v2, :cond_1

    const v1, 0x7f02002e

    goto :goto_0

    :cond_1
    const v1, 0x7f02002d

    goto :goto_0
.end method

.method protected updateUnReadView()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mMessageItem:Lcom/android/mms/ui/WPMessageItem;

    invoke-virtual {v0}, Lcom/android/mms/ui/WPMessageItem;->isUnread()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPresenceView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/WPMessageListItem;->mPresenceView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
