.class public Lcom/android/mms/ui/ConversationList;
.super Landroid/app/ListActivity;
.source "ConversationList.java"

# interfaces
.implements Lcom/android/mms/transaction/MmsSystemEventReceiver$OnSimInforChangedListener;
.implements Lcom/android/mms/util/DraftCache$OnDraftChangedListener;
.implements Lcom/mediatek/mms/ext/IMmsConversationHost;
.implements Lcom/mediatek/mms/ipmessage/INotificationsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/ConversationList$NetworkStateReceiver;,
        Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;,
        Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;,
        Lcom/android/mms/ui/ConversationList$ItemLongClickListener;,
        Lcom/android/mms/ui/ConversationList$ModeCallback;,
        Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;,
        Lcom/android/mms/ui/ConversationList$DeleteThreadListener;
    }
.end annotation


# static fields
.field private static final CHANGE_SCROLL_LISTENER_MIN_CURSOR_COUNT:I = 0x64

.field private static final CHECKED_MESSAGE_LIMITS:Ljava/lang/String; = "checked_message_limits"

.field private static final CONV_TAG:Ljava/lang/String; = "Mms/convList"

.field private static final DEBUG:Z = false

.field public static final DELETE_CONVERSATION_TOKEN:I = 0x709

.field private static final DELETE_OBSOLETE_THREADS_TOKEN:I = 0x70b

.field private static final DISPLAY_UNREAD_COUNT_CONTENT_FOR_ABOVE_99:Ljava/lang/String; = "99+"

.field private static final DROP_DOWN_KEY_NAME:Ljava/lang/String; = "drop_down_menu_text"

.field public static final HAVE_LOCKED_MESSAGES_TOKEN:I = 0x70a

.field private static final IPMSG_TAG:Ljava/lang/String; = "Mms/ipmsg/ConvList"

.field private static final KEY_SELECTION_SIMID:Ljava/lang/String; = "SIMID"

.field private static final LAST_LIST_OFFSET:Ljava/lang/String; = "last_list_offset"

.field private static final LAST_LIST_POS:Ljava/lang/String; = "last_list_pos"

.field private static final LOCAL_LOGV:Z = false

.field private static final MAX_DISPLAY_UNREAD_COUNT:I = 0x63

.field public static final MAX_FONT_SCALE:F = 1.1f

.field public static final MENU_ADD_TO_CONTACTS:I = 0x3

.field public static final MENU_DELETE:I = 0x0

.field public static final MENU_VIEW:I = 0x1

.field public static final MENU_VIEW_CONTACT:I = 0x2

.field public static final OPTION_CONVERSATION_LIST_ALL:I = 0x0

.field public static final OPTION_CONVERSATION_LIST_GROUP_CHATS:I = 0x2

.field public static final OPTION_CONVERSATION_LIST_IMPORTANT:I = 0x1

.field public static final OPTION_CONVERSATION_LIST_SPAM:I = 0x3

.field private static final REQUEST_CODE_INVITE:I = 0x65

.field private static final REQUEST_CODE_SELECT_CONTACT_FOR_GROUP:I = 0x64

.field private static final SAVE_HISTORY_MIMETYPE_TEXT:Ljava/lang/String; = "text/plain"

.field private static final SAVE_HISTORY_MIMETYPE_ZIP:Ljava/lang/String; = "application/zip"

.field private static final SAVE_HISTORY_SUFFIX:Ljava/lang/String; = ".zip"

.field private static final TAG:Ljava/lang/String; = "ConversationList"

.field private static final THREAD_LIST_QUERY_TOKEN:I = 0x6a5

.field private static final UNREAD_THREADS_QUERY_TOKEN:I = 0x6a6

.field private static final WP_TAG:Ljava/lang/String; = "Mms/WapPush"

.field private static sActivity:Landroid/app/Activity;

.field public static sConversationListOption:I

.field private static sDeleteCounter:I


# instance fields
.field private mAccountDropdown:Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;

.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeListener:Lcom/android/mms/ui/ConversationList$ModeCallback;

.field private final mContentChangedListener:Lcom/android/mms/ui/ConversationListAdapter$OnContentChangedListener;

.field private mContext:Landroid/content/Context;

.field private final mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

.field private mConversationSpinner:Landroid/view/View;

.field private mDataValid:Z

.field private final mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

.field private mDisableSearchFalg:Z

.field private mDropdownAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

.field private mEmptyViewDefault:Landroid/view/View;

.field private mFontScale:F

.field private mHandler:Landroid/os/Handler;

.field private mIsInActivity:Z

.field mIsSendEmail:Z

.field private mIsShowSIMIndicator:Z

.field private mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

.field private mNeedQuery:Z

.field private mNeedToMarkAsSeen:Z

.field private mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

.field private mNetworkStatusBar:Landroid/widget/LinearLayout;

.field private mOptionsMenu:Landroid/view/Menu;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

.field mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mSaveChatHistory:Landroid/app/ProgressDialog;

.field private mSavedFirstItemOffset:I

.field private mSavedFirstVisiblePosition:I

.field private mScrollListener:Lcom/android/mms/ui/MyScrollListener;

.field private mSearchItem:Landroid/view/MenuItem;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

.field private mSelectionMenuItem:Landroid/view/MenuItem;

.field private mSimSmsItem:Landroid/view/MenuItem;

.field private mSpinnerTextView:Landroid/widget/TextView;

.field private mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

.field private final mThreadListKeyListener:Landroid/view/View$OnKeyListener;

.field private mType:I

.field private mTypingCounter:I

.field private mUnreadConvCount:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput v1, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    const/4 v0, 0x0

    sput-object v0, Lcom/android/mms/ui/ConversationList;->sActivity:Landroid/app/Activity;

    sput v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    new-instance v0, Lcom/android/mms/ui/MyScrollListener;

    const/16 v1, 0x64

    const-string v2, "ConversationList_Scroll_Tread"

    invoke-direct {v0, v1, v2}, Lcom/android/mms/ui/MyScrollListener;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mScrollListener:Lcom/android/mms/ui/MyScrollListener;

    iput-boolean v3, p0, Lcom/android/mms/ui/ConversationList;->mDisableSearchFalg:Z

    new-instance v0, Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-direct {v0, p0, v4}, Lcom/android/mms/ui/ConversationList$ModeCallback;-><init>(Lcom/android/mms/ui/ConversationList;Lcom/android/mms/ui/ConversationList$1;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mActionModeListener:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iput-object v4, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    iput-object v4, p0, Lcom/android/mms/ui/ConversationList;->mContext:Landroid/content/Context;

    iput-boolean v3, p0, Lcom/android/mms/ui/ConversationList;->mIsSendEmail:Z

    iput-boolean v3, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    iput-boolean v3, p0, Lcom/android/mms/ui/ConversationList;->mIsInActivity:Z

    new-instance v0, Lcom/android/mms/ui/ConversationList$1;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/ConversationList$1;-><init>(Lcom/android/mms/ui/ConversationList;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mContentChangedListener:Lcom/android/mms/ui/ConversationListAdapter$OnContentChangedListener;

    new-instance v0, Lcom/android/mms/ui/ConversationList$4;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/ConversationList$4;-><init>(Lcom/android/mms/ui/ConversationList;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    new-instance v0, Lcom/android/mms/ui/ConversationList$5;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/ConversationList$5;-><init>(Lcom/android/mms/ui/ConversationList;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    new-instance v0, Lcom/android/mms/ui/ConversationList$7;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/ConversationList$7;-><init>(Lcom/android/mms/ui/ConversationList;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/android/mms/ui/ConversationList$8;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/ConversationList$8;-><init>(Lcom/android/mms/ui/ConversationList;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mIsShowSIMIndicator:Z

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/ui/ConversationList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mIsInActivity:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/ConversationList;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget v0, p0, Lcom/android/mms/ui/ConversationList;->mType:I

    return v0
.end method

.method static synthetic access$1102(Lcom/android/mms/ui/ConversationList;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/ConversationList;->mType:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/ConversationList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    return v0
.end method

.method static synthetic access$2000()I
    .locals 1

    sget v0, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    return v0
.end method

.method static synthetic access$2002(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    return p0
.end method

.method static synthetic access$2008()I
    .locals 2

    sget v0, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    return v0
.end method

.method static synthetic access$2010()I
    .locals 2

    sget v0, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/android/mms/ui/ConversationList;->sDeleteCounter:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/mms/ui/ConversationList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/android/mms/ui/ConversationList;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDeleteObsoleteThreadsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/mms/ui/ConversationList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery()V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/mms/ui/ConversationList;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList;->updateEmptyView(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/mms/ui/ConversationList;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mNeedToMarkAsSeen:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/android/mms/ui/ConversationList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/ConversationList;->mNeedToMarkAsSeen:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/android/mms/ui/ConversationList;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/mms/ui/ConversationList;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationList$ModeCallback;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mActionModeListener:Lcom/android/mms/ui/ConversationList$ModeCallback;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/mms/ui/ConversationList;)Landroid/view/Menu;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mOptionsMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/mms/ui/ConversationList;Landroid/view/Menu;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Landroid/view/Menu;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList;->setDeleteMenuVisible(Landroid/view/Menu;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/mms/ui/ConversationList;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget v0, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    return v0
.end method

.method static synthetic access$2902(Lcom/android/mms/ui/ConversationList;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    return p1
.end method

.method static synthetic access$300(Lcom/android/mms/ui/ConversationList;J)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery(J)V

    return-void
.end method

.method static synthetic access$3000(Lcom/android/mms/ui/ConversationList;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget v0, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstItemOffset:I

    return v0
.end method

.method static synthetic access$3100(Lcom/android/mms/ui/ConversationList;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mUnreadConvCount:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/android/mms/ui/ConversationList;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/ConversationList;->mDisableSearchFalg:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/CustomMenu$DropDownMenu;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/android/mms/ui/ConversationList;Lcom/android/mms/ui/CustomMenu$DropDownMenu;)Lcom/android/mms/ui/CustomMenu$DropDownMenu;
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenu:Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/android/mms/ui/ConversationList;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenuItem:Landroid/view/MenuItem;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/android/mms/ui/ConversationList;Ljava/util/HashSet;Landroid/view/ActionMode;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Ljava/util/HashSet;
    .param p2    # Landroid/view/ActionMode;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/ConversationList;->showExportDialog(Ljava/util/HashSet;Landroid/view/ActionMode;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/android/mms/ui/ConversationList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->updateSelectionTitle()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/mms/ui/ConversationList;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/mms/ui/ConversationList;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mAccountDropdown:Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/android/mms/ui/ConversationList;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mConversationSpinner:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/android/mms/ui/ConversationList;I)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList;->onAccountSpinnerItemClicked(I)V

    return-void
.end method

.method static synthetic access$4400(Lcom/android/mms/ui/ConversationList;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSaveChatHistory:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/android/mms/ui/ConversationList;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList;->mSaveChatHistory:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/android/mms/ui/ConversationList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->initSpinnerListAdapter()V

    return-void
.end method

.method static synthetic access$4600(Lcom/android/mms/ui/ConversationList;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/android/mms/ui/ConversationList;)Lcom/mediatek/ipmsg/ui/ConversationEmptyView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/android/mms/ui/ConversationList;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/android/mms/ui/ConversationList;Z)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList;->showInternetStatusBar(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/mms/ui/ConversationList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->markCheckedMessageLimit()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/mms/ui/ConversationList;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList;
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/ConversationList;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/mms/ui/ConversationList;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSearchItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method public static confirmDeleteThread(JLandroid/content/AsyncQueryHandler;)V
    .locals 3
    .param p0    # J
    .param p2    # Landroid/content/AsyncQueryHandler;

    const/4 v0, 0x0

    const-wide/16 v1, -0x1

    cmp-long v1, p0, v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v0, p2}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThreads(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;)V

    return-void
.end method

.method public static confirmDeleteThreadDialog(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;Ljava/util/Collection;ZLandroid/content/Context;)V
    .locals 15
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;
    .param p2    # Z
    .param p3    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mms/ui/ConversationList$DeleteThreadListener;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;Z",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const v1, 0x7f04001b

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    const v1, 0x7f0f007c

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    if-nez p1, :cond_4

    const v1, 0x7f0b02a0

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v1, 0x7f0f007d

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    if-nez p2, :cond_5

    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    const/4 v11, 0x0

    const/4 v14, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_1

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmDeleteThreadDialog max SMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v11, 0x0

    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_3

    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmDeleteThreadDialog max MMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v11, 0x0

    :cond_3
    invoke-virtual {p0, v12, v14}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->setMaxMsgId(II)V

    new-instance v7, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b029d

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b02a6

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b027d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_4
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v9

    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0005

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v9, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static/range {p3 .. p3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x75

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    invoke-virtual {v8}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->setDeleteLockedMessage(Z)V

    new-instance v1, Lcom/android/mms/ui/ConversationList$6;

    invoke-direct {v1, p0, v8}, Lcom/android/mms/ui/ConversationList$6;-><init>(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v11, 0x0

    throw v1

    :catchall_1
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v11, 0x0

    throw v1
.end method

.method public static confirmDeleteThreads(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;)V
    .locals 1
    .param p1    # Landroid/content/AsyncQueryHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/AsyncQueryHandler;",
            ")V"
        }
    .end annotation

    const/16 v0, 0x70a

    invoke-static {p1, p0, v0}, Lcom/android/mms/data/Conversation;->startQueryHaveLockedMessages(Landroid/content/AsyncQueryHandler;Ljava/util/Collection;I)V

    return-void
.end method

.method public static createAddContactIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "vnd.android.cursor.item/contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0}, Landroid/provider/Telephony$Mms;->isEmailAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "email"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object v0

    :cond_0
    const-string v1, "phone"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "phone_type"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private createNewMessage()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static getContext()Landroid/app/Activity;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/ConversationList;->sActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;
    .locals 3
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p2, :cond_2

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_2
    const/4 v1, 0x1

    if-eq p2, v1, :cond_3

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_3
    const/4 v1, 0x2

    if-eq p2, v1, :cond_4

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x83

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_4
    const/4 v1, 0x3

    if-eq p2, v1, :cond_0

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x84

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private initListAdapter()V
    .locals 2

    new-instance v0, Lcom/android/mms/ui/ConversationListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/ConversationListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method

.method private initPlugin(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    :try_start_0
    const-class v1, Lcom/mediatek/mms/ext/IMmsConversation;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p1, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsConversation;

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsConversationPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/util/AndroidException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    invoke-interface {v1, p0}, Lcom/mediatek/mms/ext/IMmsConversation;->init(Lcom/mediatek/mms/ext/IMmsConversationHost;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsConversationImpl;

    invoke-direct {v1, p1}, Lcom/mediatek/mms/ext/MmsConversationImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsConversationPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initSpinnerListAdapter()V
    .locals 4

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f040017

    const v2, 0x7f0f0078

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->setupActionBar2()V

    new-instance v0, Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;-><init>(Lcom/android/mms/ui/ConversationList;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mAccountDropdown:Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mAccountDropdown:Lcom/android/mms/ui/ConversationList$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private markCheckedMessageLimit()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "checked_message_limits"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private onAccountSpinnerItemClicked(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    sget v0, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    :goto_1
    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery()V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :pswitch_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :pswitch_2
    if-lez p1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :pswitch_3
    if-le p1, v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    sput v0, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_1

    :pswitch_5
    sput v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_1

    :pswitch_6
    const/4 v0, 0x2

    sput v0, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x83

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_1

    :pswitch_7
    const/4 v0, 0x3

    sput v0, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x84

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private openIpMsgThread(JZ)V
    .locals 3
    .param p1    # J
    .param p3    # Z

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->CHAT_DETAILS_BY_THREAD_ID:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "thread_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "boolean"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "need_new_task"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private openThread(JI)V
    .locals 1
    .param p1    # J
    .param p3    # I

    packed-switch p3, :pswitch_data_0

    invoke-static {p0, p1, p2}, Lcom/android/mms/ui/ComposeMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/android/mms/ui/WPMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/android/mms/ui/CBMessageListActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setDeleteMenuVisible(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    if-eqz p1, :cond_0

    const v1, 0x7f0f019a

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v1}, Lcom/android/mms/ui/ConversationListAdapter;->isDataValid()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mms/ui/ConversationList;->mDataValid:Z

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setupActionBar()V
    .locals 7

    const/16 v6, 0x10

    const/4 v5, -0x2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040011

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v5, v5, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0f0064

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mUnreadConvCount:Landroid/widget/TextView;

    return-void
.end method

.method private setupActionBar2()V
    .locals 8

    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, -0x2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040012

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v5, v5, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0f0064

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mUnreadConvCount:Landroid/widget/TextView;

    const v2, 0x7f0f0066

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    const v2, 0x7f0f0065

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mConversationSpinner:Landroid/view/View;

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v3

    const/16 v4, 0x81

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mConversationSpinner:Landroid/view/View;

    new-instance v3, Lcom/android/mms/ui/ConversationList$9;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/ConversationList$9;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mConversationSpinner:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showExportDialog(Ljava/util/HashSet;Landroid/view/ActionMode;)V
    .locals 6
    .param p2    # Landroid/view/ActionMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/view/ActionMode;",
            ")V"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v4

    const/16 v5, 0x7d

    invoke-virtual {v4, v5}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v4

    const/16 v5, 0x7e

    invoke-virtual {v4, v5}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/mms/ui/ConversationList$17;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList;->mContext:Landroid/content/Context;

    const v5, 0x1090011

    invoke-direct {v0, p0, v4, v5, v3}, Lcom/android/mms/ui/ConversationList$17;-><init>(Lcom/android/mms/ui/ConversationList;Landroid/content/Context;ILjava/util/List;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/android/mms/ui/ConversationList$18;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/mms/ui/ConversationList$18;-><init>(Lcom/android/mms/ui/ConversationList;Ljava/util/HashSet;Landroid/view/ActionMode;)V

    const v4, 0x7f0b015a

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/android/mms/ui/ConversationList$19;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/ConversationList$19;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showInternetStatusBar(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStatusBar:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStatusBar:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private startAsyncQuery()V
    .locals 2

    const-wide/16 v0, 0xa

    invoke-direct {p0, v0, v1}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery(J)V

    return-void
.end method

.method private startAsyncQuery(J)V
    .locals 7
    .param p1    # J

    :try_start_0
    invoke-static {}, Lcom/android/mms/data/Conversation;->getActivated()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    sget v2, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    sget v3, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/ConversationList;->getDropDownMenuData(Landroid/widget/ArrayAdapter;I)Landroid/widget/ArrayAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    :goto_1
    return-void

    :pswitch_0
    const-string v2, "Mms/ipmsg/ConvList"

    const-string v3, "startAsyncQuery(): query for all messages except spam"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v3

    const/16 v4, 0x81

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "threads._id not in (SELECT DISTINCT thread_id FROM thread_settings WHERE spam=1) "

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a5

    invoke-static {v2, v3, v1}, Lcom/android/mms/data/Conversation;->startQueryExtend(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read=0 and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0xa

    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/mms/data/Conversation;->startQuery(Landroid/content/AsyncQueryHandler;ILjava/lang/String;J)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_1

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v3

    const/16 v4, 0x82

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "threads._id IN (SELECT thread_id FROM sms WHERE locked=1 UNION SELECT thread_id FROM pdu WHERE locked=1)"

    const-string v2, "Mms/ipmsg/ConvList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAsyncQuery(): query for important messages, selection = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a5

    invoke-static {v2, v3, v1}, Lcom/android/mms/data/Conversation;->startQueryExtend(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read=0 and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0xa

    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/mms/data/Conversation;->startQuery(Landroid/content/AsyncQueryHandler;ILjava/lang/String;J)V

    goto/16 :goto_0

    :pswitch_2
    const-string v2, "Mms/ipmsg/ConvList"

    const-string v3, "startAsyncQuery(): query for group messages"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v3

    const/16 v4, 0x83

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "threads._id IN (SELECT DISTINCT thread_id FROM thread_settings WHERE spam=0) AND threads.recipient_ids IN (SELECT _id FROM canonical_addresses WHERE SUBSTR(address, 1, 4) = \'7---\')"

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a5

    invoke-static {v2, v3, v1}, Lcom/android/mms/data/Conversation;->startQueryExtend(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read=0 and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0xa

    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/mms/data/Conversation;->startQuery(Landroid/content/AsyncQueryHandler;ILjava/lang/String;J)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mSpinnerTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v3

    const/16 v4, 0x84

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "threads._id IN (SELECT DISTINCT thread_id FROM thread_settings WHERE spam=1) "

    const-string v2, "Mms/ipmsg/ConvList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAsyncQuery(): query for spam messages, selection = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a5

    invoke-static {v2, v3, v1}, Lcom/android/mms/data/Conversation;->startQueryExtend(Landroid/content/AsyncQueryHandler;ILjava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read=0 and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0xa

    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/mms/data/Conversation;->startQuery(Landroid/content/AsyncQueryHandler;ILjava/lang/String;J)V

    goto/16 :goto_0

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    check-cast v2, Landroid/widget/TextView;

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0b020c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a5

    invoke-static {v2, v3, p1, p2}, Lcom/android/mms/data/Conversation;->startQueryForAll(Landroid/content/AsyncQueryHandler;IJ)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v3, 0x6a6

    const-string v4, "read=0"

    invoke-static {v2, v3, v4, p1, p2}, Lcom/android/mms/data/Conversation;->startQuery(Landroid/content/AsyncQueryHandler;ILjava/lang/String;J)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateEmptyView(Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v3, 0x1

    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "active:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sConversationListOption:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "ConversationList"

    const-string v1, "unkown position!"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v0}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setAllChatEmpty()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v0, v3}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setImportantEmpty(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v0, v3}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setGroupChatEmpty(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v0, v3}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setSpamEmpty(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateSelectionTitle()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/ConversationListAdapter;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenuItem:Landroid/view/MenuItem;

    const v1, 0x7f0b00ae

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSelectionMenuItem:Landroid/view/MenuItem;

    const v1, 0x7f0b0072

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public changeMode()V
    .locals 3

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/mms/MmsConfig;->setMmsDirMode(Z)V

    invoke-static {p0}, Lcom/android/mms/ui/MessageUtils;->updateNotification(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/FolderViewList;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "floderview_key"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public notificationsReceived(Landroid/content/Intent;)V
    .locals 14
    .param p1    # Landroid/content/Intent;

    const-string v11, "Mms/noti"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "convList.notificationsReceived(): start, intent = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getActionTypeByAction(Ljava/lang/String;)I

    move-result v11

    packed-switch v11, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iget-object v11, p0, Lcom/android/mms/ui/ConversationList;->mHandler:Landroid/os/Handler;

    new-instance v12, Lcom/android/mms/ui/ConversationList$10;

    invoke-direct {v12, p0}, Lcom/android/mms/ui/ConversationList$10;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v11, "com.mediatek.mms.ipmessage.saveHistroyDone"

    const/4 v12, 0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v11, "ConversationList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "save history done: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "com.mediatek.mms.ipmessage.saveHistoryFile"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v11, "ConversationList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "save history file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v11, p0, Lcom/android/mms/ui/ConversationList;->mIsSendEmail:Z

    if-nez v11, :cond_4

    if-nez v2, :cond_2

    new-instance v11, Lcom/android/mms/ui/ConversationList$11;

    invoke-direct {v11, p0, v5}, Lcom/android/mms/ui/ConversationList$11;-><init>(Lcom/android/mms/ui/ConversationList;Ljava/lang/String;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    if-eqz v5, :cond_3

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_3
    new-instance v11, Lcom/android/mms/ui/ConversationList$12;

    invoke-direct {v11, p0}, Lcom/android/mms/ui/ConversationList$12;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_4
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/android/mms/ui/ConversationList;->mIsSendEmail:Z

    if-nez v2, :cond_6

    if-eqz v5, :cond_6

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    const-string v11, "ConversationList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "File: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-string v11, "android.intent.action.SEND"

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "android.intent.extra.STREAM"

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v11, "message/rfc822"

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v11, "ConversationList"

    const-string v12, "invoke email failed!"

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v11, "ConversationList"

    const-string v12, "file does not exist!"

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    if-eqz v5, :cond_7

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_7
    new-instance v11, Lcom/android/mms/ui/ConversationList$13;

    invoke-direct {v11, p0}, Lcom/android/mms/ui/ConversationList$13;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_2
    const-string v11, "group_id"

    const/4 v12, -0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v11, "ConversationList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "update group info,group id:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getContactManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ContactManager;

    move-result-object v11

    int-to-short v12, v6

    invoke-virtual {v11, v12}, Lcom/mediatek/mms/ipmessage/ContactManager;->getNumberByEngineId(S)Ljava/lang/String;

    move-result-object v8

    const-string v11, "ConversationList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "group number:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-static {v8, v11}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v1

    if-eqz v1, :cond_8

    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/android/mms/data/Contact;->setName(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->clearAvatar()V

    :cond_8
    new-instance v11, Lcom/android/mms/ui/ConversationList$14;

    invoke-direct {v11, p0}, Lcom/android/mms/ui/ConversationList$14;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_3
    invoke-static {}, Lcom/android/mms/data/Contact;->invalidateGroupCache()V

    new-instance v11, Lcom/android/mms/ui/ConversationList$15;

    invoke-direct {v11, p0}, Lcom/android/mms/ui/ConversationList$15;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v11, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    if-nez v11, :cond_0

    invoke-static {}, Lcom/android/mms/data/Conversation;->getActivated()Z

    move-result v11

    if-nez v11, :cond_0

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v11, Lcom/android/mms/ui/ConversationList$16;

    invoke-direct {v11, p0}, Lcom/android/mms/ui/ConversationList$16;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {p0, v11}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 14
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v11, "Mms/ipmsg/ConvList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onActivityResult(): requestCode="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", resultCode="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, -0x1

    move/from16 v0, p2

    if-eq v0, v11, :cond_0

    const-string v11, "Mms/ipmsg/ConvList"

    const-string v12, "onActivityResult(): result is not OK."

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    const-string v11, "Mms/ipmsg/ConvList"

    const-string v12, "onActivityResult(): default return."

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const-string v11, "contactId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    move-object v1, v7

    array-length v6, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v2, v1, v3

    const-string v11, "Mms/ipmsg/ConvList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onActivityResult(): SELECT_CONTACT get contact id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    new-instance v4, Landroid/content/Intent;

    sget-object v11, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->NEW_GROUP_CHAT:Ljava/lang/String;

    invoke-direct {v4, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v11, "sim_id"

    const-string v12, "SIMID"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v11, "array"

    invoke-virtual {v4, v11, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v4}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    const-string v11, "Mms/ipmsg/ConvList"

    const-string v12, "onActivityResult(): SELECT_CONTACT get contact id is NULL!"

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v11, "contactId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    const-string v11, "Mms/ipmsg/ConvList"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mSelectContactsNumbers:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Landroid/content/Intent;

    const-class v11, Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v5, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v11, "android.intent.action.SENDTO"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "smsto:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v11, "sms_body"

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v12

    const/16 v13, 0xe9

    invoke-virtual {v12, v13}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    const-string v11, "Mms/ipmsg/ConvList"

    const-string v12, "onActivityResult(): INVITE get contact id is NULL!"

    invoke-static {v11, v12}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->moveTaskToBack(Z)Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1    # Landroid/view/MenuItem;

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v7}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    if-ltz v7, :cond_0

    invoke-static {p0, v3}, Lcom/android/mms/data/Conversation;->from(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/Conversation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v5

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v7

    return v7

    :pswitch_0
    iget-object v7, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    invoke-static {v5, v6, v7}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThread(JLandroid/content/AsyncQueryHandler;)V

    goto :goto_0

    :pswitch_1
    iget v7, p0, Lcom/android/mms/ui/ConversationList;->mType:I

    invoke-direct {p0, v5, v6, v7}, Lcom/android/mms/ui/ConversationList;->openThread(JI)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mms/data/Contact;

    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v1}, Lcom/android/mms/data/Contact;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v7, 0x80000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mms/data/Contact;

    invoke-virtual {v7}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->createAddContactIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0, p0}, Lcom/android/mms/ui/ConversationList;->initPlugin(Landroid/content/Context;)V

    sput-object p0, Lcom/android/mms/ui/ConversationList;->sActivity:Landroid/app/Activity;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMmsDirMode()Z

    move-result v2

    invoke-static {}, Lcom/android/mms/MmsConfig;->getFolderModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/android/mms/ui/FolderViewList;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "floderview_key"

    invoke-virtual {v3, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-static {v9}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-interface {v1}, Lcom/mediatek/mms/ext/IMmsDialogNotify;->closeMsgDialog()V

    const v5, 0x7f040016

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    new-instance v5, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    invoke-direct {v5, p0}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    new-instance v5, Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;-><init>(Lcom/android/mms/ui/ConversationList;Landroid/content/ContentResolver;)V

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v5

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList;->mConvListOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList;->mThreadListKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList;->mScrollListener:Lcom/android/mms/ui/MyScrollListener;

    invoke-virtual {v5, v6}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v5

    if-lez v5, :cond_1

    invoke-static {p0, p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->addIpMsgNotificationListeners(Landroid/content/Context;Lcom/mediatek/mms/ipmessage/INotificationsListener;)V

    :cond_1
    const v5, 0x7f0f0075

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    const v5, 0x7f0f0074

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    :goto_0
    const v5, 0x7f0f0076

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStatusBar:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStatusBar:Landroid/widget/LinearLayout;

    const v6, 0x7f0f0077

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    if-eqz v4, :cond_2

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0x69

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    new-instance v6, Lcom/android/mms/ui/ConversationList$ItemLongClickListener;

    invoke-direct {v6, p0}, Lcom/android/mms/ui/ConversationList$ItemLongClickListener;-><init>(Lcom/android/mms/ui/ConversationList;)V

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->initListAdapter()V

    iput-object p0, p0, Lcom/android/mms/ui/ConversationList;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {v9}, Lcom/android/mms/data/Conversation;->setActivated(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->initSpinnerListAdapter()V

    const-string v5, ""

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mHandler:Landroid/os/Handler;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "checked_message_limits"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_2
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->fontScale:F

    iput v5, p0, Lcom/android/mms/ui/ConversationList;->mFontScale:F

    iget v5, p0, Lcom/android/mms/ui/ConversationList;->mFontScale:F

    const v6, 0x3f8ccccd

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    const-string v5, "ConversationList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "system fontscale is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/mms/ui/ConversationList;->mFontScale:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v5, v9}, Lcom/android/mms/ui/ConversationListAdapter;->setSubjectSingleLineMode(Z)V

    :cond_3
    if-eqz p1, :cond_7

    const-string v5, "last_list_pos"

    invoke-virtual {p1, v5, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    const-string v5, "last_list_offset"

    invoke-virtual {p1, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstItemOffset:I

    :goto_3
    return-void

    :cond_4
    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v5, v6}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_5
    const-string v5, "ConversationList"

    const-string v6, "normal message layout"

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->setupActionBar()V

    const v5, 0x7f0b0214

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setTitle(I)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList;->runOneTimeStorageLimitCheckForLegacyMessages()V

    goto :goto_2

    :cond_7
    iput v10, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    iput v8, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstItemOffset:I

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 14
    .param p1    # Landroid/view/Menu;

    const/4 v13, 0x0

    const/4 v12, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v9

    const v10, 0x7f0e0001

    invoke-virtual {v9, v10, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v9, 0x7f0f0197

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    iput-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchItem:Landroid/view/MenuItem;

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v9}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/SearchView;

    iput-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchView:Landroid/widget/SearchView;

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchView:Landroid/widget/SearchView;

    iget-object v10, p0, Lcom/android/mms/ui/ConversationList;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v9, v10}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchView:Landroid/widget/SearchView;

    const v10, 0x7f0b0303

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v9, v12}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    const-string v9, "search"

    invoke-virtual {p0, v9}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/SearchManager;

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v3

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v9, v3}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    :cond_0
    const v9, 0x7f0f019d

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x1110044

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v9, "com.android.cellbroadcastreceiver"

    invoke-virtual {v7, v9}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    const/4 v4, 0x0

    :cond_1
    :goto_0
    if-nez v4, :cond_2

    invoke-interface {v0, v13}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    const v9, 0x7f0f0196

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/mediatek/mms/ipmessage/ServiceManager;->isFeatureSupported(I)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getPluginMenuIDBase()I

    move-result v10

    invoke-interface {v9, p1, v10}, Lcom/mediatek/mms/ext/IMmsConversation;->addOptionMenu(Landroid/view/Menu;I)V

    const/4 v1, 0x0

    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v9

    if-ge v1, v9, :cond_4

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v5}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b003e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    iput-object v5, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    :cond_4
    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    if-eqz v9, :cond_6

    invoke-static {}, Lcom/android/mms/ui/ConversationList;->getContext()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_8

    :cond_5
    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    invoke-interface {v9, v13}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_6
    :goto_2
    return v12

    :catch_0
    move-exception v2

    const/4 v4, 0x0

    goto :goto_0

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_8
    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    invoke-interface {v9, v12}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "ConversationList"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v1, 0x6a5

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v1, 0x6a6

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    if-eqz v0, :cond_2

    const-string v0, "ConversationList"

    const-string v1, "clear it"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mScrollListener:Lcom/android/mms/ui/MyScrollListener;

    invoke-virtual {v0}, Lcom/android/mms/ui/MyScrollListener;->destroyThread()V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    :cond_3
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {p0, p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->removeIpMsgNotificationListeners(Landroid/content/Context;Lcom/mediatek/mms/ipmessage/INotificationsListener;)V

    :cond_4
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/android/mms/data/Conversation;->clearCache()V

    :cond_5
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void
.end method

.method public onDraftChanged(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    new-instance v1, Lcom/android/mms/ui/ConversationList$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/mms/ui/ConversationList$3;-><init>(Lcom/android/mms/ui/ConversationList;JZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mDisableSearchFalg:Z

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x54
        :pswitch_0
    .end packed-switch
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 12
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v9, "ConversationList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onListItemClick: pos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v2}, Lcom/android/mms/data/Conversation;->from(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/Conversation;

    move-result-object v1

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mActionMode:Landroid/view/ActionMode;

    if-eqz v9, :cond_3

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->isChecked()Z

    move-result v0

    iget-object v10, p0, Lcom/android/mms/ui/ConversationList;->mActionModeListener:Lcom/android/mms/ui/ConversationList$ModeCallback;

    if-nez v0, :cond_2

    const/4 v9, 0x1

    :goto_1
    const/4 v11, 0x0

    invoke-virtual {v10, p3, v9, v11}, Lcom/android/mms/ui/ConversationList$ModeCallback;->setItemChecked(IZLandroid/database/Cursor;)V

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mActionModeListener:Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-static {v9}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$1000(Lcom/android/mms/ui/ConversationList$ModeCallback;)V

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v9}, Lcom/android/mms/ui/ConversationListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v7

    const-string v9, "Mms/WapPush"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ConversationList: conv.getType() is : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getType()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v9

    if-lez v9, :cond_9

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getType()I

    move-result v9

    const/16 v10, 0xa

    if-ne v9, v10, :cond_4

    new-instance v4, Landroid/content/Intent;

    sget-object v9, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->SERVICE_CENTER:Ljava/lang/String;

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v4}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->markAsRead()V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ge v9, v10, :cond_6

    :cond_5
    const-string v9, "ConversationList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "a thread with no recipients, threadId:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, ""

    :goto_2
    const-string v9, "Mms/ipmsg/ConvList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "open thread by number "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_8

    const-string v9, "7---"

    invoke-virtual {v6, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    const-string v9, "Mms/ipmsg/ConvList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "open group thread by thread id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->markAsSeen()V

    sget v9, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    const/4 v9, 0x1

    invoke-direct {p0, v7, v8, v9}, Lcom/android/mms/ui/ConversationList;->openIpMsgThread(JZ)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/mms/data/Contact;

    invoke-virtual {v9}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_7
    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/android/mms/ui/ConversationList;->openIpMsgThread(JZ)V

    goto/16 :goto_0

    :cond_8
    sget v9, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_9

    const-string v9, "Mms/ipmsg/ConvList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "open important thread by thread id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getType()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_0

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getType()I

    move-result v9

    const/4 v10, 0x3

    if-eq v9, v10, :cond_0

    invoke-static {p0, v7, v8}, Lcom/android/mms/ui/ComposeMessageActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v3

    const-string v9, "load_important"

    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getType()I

    move-result v9

    invoke-direct {p0, v7, v8, v9}, Lcom/android/mms/ui/ConversationList;->openThread(JI)V

    goto/16 :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery()V

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/IMmsDialogNotify;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsDialogNotify;->closeMsgDialog()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 20
    .param p1    # Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList;->mMmsConversationPlugin:Lcom/mediatek/mms/ext/IMmsConversation;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/mms/ext/IMmsConversation;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v17

    if-eqz v17, :cond_0

    const/16 v17, 0x1

    :goto_0
    return v17

    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v17

    packed-switch v17, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v17

    packed-switch v17, :pswitch_data_1

    :pswitch_1
    const/16 v17, 0x1

    goto :goto_0

    :pswitch_2
    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->checkCurrentIpMessageServiceStatus(Landroid/app/Activity;ZLandroid/os/Handler;)Z

    move-result v17

    if-eqz v17, :cond_2

    new-instance v6, Landroid/content/Intent;

    sget-object v17, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->CONTACT:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "type"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v17, "request_code"

    const/16 v18, 0x64

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    const/16 v17, 0x1

    goto :goto_0

    :pswitch_3
    new-instance v7, Landroid/content/Intent;

    sget-object v17, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->CONTACT:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v17, "type"

    const/16 v18, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v17, "request_code"

    const/16 v18, 0x65

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/android/mms/ui/ConversationList;->createNewMessage()V

    :goto_2
    const/16 v17, 0x0

    goto :goto_0

    :pswitch_5
    invoke-static/range {p0 .. p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_4

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v3}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v12

    const/4 v14, 0x0

    :goto_3
    if-ge v14, v12, :cond_3

    invoke-virtual {v11, v14}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/android/mms/data/Conversation;->getFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/Conversation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThreads(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;)V

    goto :goto_2

    :cond_4
    const-wide/16 v17, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThread(JLandroid/content/AsyncQueryHandler;)V

    goto :goto_2

    :pswitch_6
    new-instance v10, Landroid/content/Intent;

    const-class v17, Lcom/android/mms/ui/SettingListActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v17, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v10, v1}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;I)Z

    goto :goto_2

    :pswitch_7
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    const-string v17, "com.mediatek.omacp"

    const-string v18, "com.mediatek.omacp.message.OmacpMessageList"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v17, 0x10000000

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/16 v17, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v13, v1}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;I)Z

    goto/16 :goto_2

    :pswitch_8
    new-instance v16, Landroid/content/Intent;

    const-class v17, Lcom/android/mms/ui/WPMessageActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    :pswitch_9
    invoke-static/range {p0 .. p0}, Lcom/android/mms/LogTag;->dumpInternalTables(Landroid/content/Context;)V

    goto/16 :goto_2

    :pswitch_a
    new-instance v4, Landroid/content/Intent;

    const-string v17, "android.intent.action.MAIN"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v17, Landroid/content/ComponentName;

    const-string v18, "com.android.cellbroadcastreceiver"

    const-string v19, "com.android.cellbroadcastreceiver.CellBroadcastListActivity"

    invoke-direct/range {v17 .. v19}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v17, 0x10000000

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    const/16 v17, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v9

    const-string v17, "ConversationList"

    const-string v18, "ActivityNotFoundException for CellBroadcastListActivity"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0196
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f0f0195
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method protected onPause()V
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;->hideSIMIndicator(Landroid/content/ComponentName;)V

    iput-boolean v2, p0, Lcom/android/mms/ui/ConversationList;->mIsShowSIMIndicator:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    iput v2, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstItemOffset:I

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 11
    .param p1    # Landroid/view/Menu;

    const/4 v10, 0x0

    const/4 v9, 0x1

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList;->mOptionsMenu:Landroid/view/Menu;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList;->setDeleteMenuVisible(Landroid/view/Menu;)V

    const v7, 0x7f0f019e

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const v7, 0x7f0f019c

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v5, 0x0

    :try_start_0
    const-string v7, "com.mediatek.omacp"

    const/4 v8, 0x2

    invoke-virtual {p0, v7, v8}, Landroid/content/ContextWrapper;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_0
    if-eqz v5, :cond_1

    const-string v7, "omacp"

    const/4 v8, 0x5

    invoke-virtual {v5, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "configuration_msg_exist"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    iget-object v7, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/android/mms/ui/ConversationList;->getContext()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_2
    iget-object v7, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_3
    :goto_1
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    const v7, 0x7f0f0196

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    const v7, 0x7f0f019f

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return v9

    :catch_0
    move-exception v1

    const-string v7, "Mms/convList"

    const-string v8, "ConversationList NotFoundContext"

    invoke-static {v7, v8}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v7, p0, Lcom/android/mms/ui/ConversationList;->mSimSmsItem:Landroid/view/MenuItem;

    invoke-interface {v7, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v2, 0x4

    invoke-static {v2}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ext/IAppGuideExt;

    const-string v2, "MMS"

    invoke-interface {v0, v2}, Lcom/mediatek/mms/ext/IAppGuideExt;->showAppGuide(Ljava/lang/String;)V

    sput-boolean v3, Lcom/android/mms/ui/ComposeMessageActivity;->mDestroy:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    iput-boolean v3, p0, Lcom/android/mms/ui/ConversationList;->mIsShowSIMIndicator:Z

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    invoke-virtual {v2, v1}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;->hideSIMIndicator(Landroid/content/ComponentName;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    const-string v3, "sms_sim_setting"

    invoke-virtual {v2, v1, v3}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;->showSIMIndicator(Landroid/content/ComponentName;Ljava/lang/String;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "last_list_pos"

    iget v1, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstVisiblePosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "last_list_offset"

    iget v1, p0, Lcom/android/mms/ui/ConversationList;->mSavedFirstItemOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSimInforChanged()V
    .locals 3

    const-string v0, "Mms"

    const-string v1, "onSimInforChanged(): Conversation List"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList;->mIsShowSIMIndicator:Z

    if-eqz v0, :cond_0

    const-string v0, "Mms"

    const-string v1, "Hide current indicator and show new one."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;->hideSIMIndicator(Landroid/content/ComponentName;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mStatusBarManager:Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const-string v2, "sms_sim_setting"

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/encapsulation/android/app/EncapsulatedStatusBarManager;->showSIMIndicator(Landroid/content/ComponentName;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-static {v6}, Lcom/android/mms/MmsConfig;->setMmsDirMode(Z)V

    const-string v1, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Performance test][Mms] loading data start time ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mDropdownAdapter:Landroid/widget/ArrayAdapter;

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5}, Lcom/android/mms/data/Conversation;->setActivated(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->initSpinnerListAdapter()V

    const-string v1, ""

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mEmptyViewDefault:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mEmptyView:Lcom/mediatek/ipmsg/ui/ConversationEmptyView;

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/android/mms/ui/ConversationList$NetworkStateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/mms/ui/ConversationList$NetworkStateReceiver;-><init>(Lcom/android/mms/ui/ConversationList;Lcom/android/mms/ui/ConversationList$1;)V

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mNetworkStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xef

    invoke-static {v1, v2}, Lcom/android/mms/transaction/MessagingNotification;->cancelNotification(Landroid/content/Context;I)V

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/mms/util/DraftCache;->addOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    iput-boolean v5, p0, Lcom/android/mms/ui/ConversationList;->mNeedToMarkAsSeen:Z

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->startAsyncQuery()V

    iput-boolean v5, p0, Lcom/android/mms/ui/ConversationList;->mIsInActivity:Z

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    if-eqz v1, :cond_2

    const-string v1, "ConversationList"

    const-string v2, "set onContentChanged listener"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList;->mContentChangedListener:Lcom/android/mms/ui/ConversationListAdapter$OnContentChangedListener;

    invoke-virtual {v1, v2}, Lcom/android/mms/ui/ConversationListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/ConversationListAdapter$OnContentChangedListener;)V

    :cond_2
    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/util/DraftCache;->refresh()V

    invoke-static {}, Lcom/android/mms/data/Conversation;->loadingThreads()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/android/mms/data/Contact;->invalidateCache()V

    :cond_3
    const-string v1, "ConversationList.onStart"

    invoke-static {p0, v1}, Lcom/android/mms/MmsConfig;->printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iput-boolean v2, p0, Lcom/android/mms/ui/ConversationList;->mIsInActivity:Z

    invoke-static {}, Lcom/android/mms/util/DraftCache;->getInstance()Lcom/android/mms/util/DraftCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mms/util/DraftCache;->removeOnDraftChangedListener(Lcom/android/mms/util/DraftCache$OnDraftChangedListener;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    if-eqz v0, :cond_0

    const-string v0, "ConversationList"

    const-string v1, "remove OnContentChangedListener"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mListAdapter:Lcom/android/mms/ui/ConversationListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/ConversationListAdapter;->setOnContentChangedListener(Lcom/android/mms/ui/ConversationListAdapter$OnContentChangedListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    if-eqz v0, :cond_1

    const-string v0, "ConversationList"

    const-string v1, "cancel undone queries in onStop"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v1, 0x6a5

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList;->mQueryHandler:Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    const/16 v1, 0x6a6

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    iput-boolean v2, p0, Lcom/android/mms/ui/ConversationList;->mNeedQuery:Z

    :cond_1
    const-string v0, "ConversationList"

    const-string v1, "update MmsWidget"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mms/widget/MmsWidgetProvider;->notifyDatasetChanged(Landroid/content/Context;)V

    return-void
.end method

.method public declared-synchronized runOneTimeStorageLimitCheckForLegacyMessages()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/android/mms/util/Recycler;->isAutoDeleteEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList;->markCheckedMessageLimit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/ConversationList$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/ConversationList$2;-><init>(Lcom/android/mms/ui/ConversationList;)V

    const-string v2, "ConversationList.runOneTimeStorageLimitCheckForLegacyMessages"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public showSimSms()V
    .locals 6

    const/high16 v5, 0x10000000

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/android/mms/ui/SelectCardPreferenceActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "preference"

    const-string v3, "pref_key_manage_sim_messages"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "preferenceTitleId"

    const v3, 0x7f0b028e

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "SlotId"

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v2}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlot()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const v2, 0x7f0b006d

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
