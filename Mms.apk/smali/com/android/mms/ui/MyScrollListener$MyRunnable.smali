.class Lcom/android/mms/ui/MyScrollListener$MyRunnable;
.super Ljava/lang/Object;
.source "MyScrollListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MyScrollListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyRunnable"
.end annotation


# instance fields
.field private bindDefaultViewTimes:I

.field private bindViewTimes:I

.field private count:I

.field mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

.field private mNeedRun:Z

.field final synthetic this$0:Lcom/android/mms/ui/MyScrollListener;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/MyScrollListener;Z)V
    .locals 1
    .param p2    # Z

    iput-object p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    iget v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindDefaultViewTimes:I

    iput-boolean p2, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    :goto_0
    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v3}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    iget-boolean v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OnScrollListener.run(): count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    iget v4, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindViewTimes:I

    rem-int/2addr v3, v4

    iget v4, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->bindDefaultViewTimes:I

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    :goto_2
    iget v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v3}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    invoke-virtual {v3, v6}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    goto :goto_2

    :cond_2
    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v3}, Lcom/android/mms/ui/MyScrollListener;->access$200(Lcom/android/mms/ui/MyScrollListener;)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    :try_start_1
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "wait has been interrupted."

    invoke-static {v3, v4, v1}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_3
    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    invoke-virtual {v3, v6}, Lcom/android/mms/ui/MessageCursorAdapter;->setIsScrolling(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    invoke-static {}, Lcom/android/mms/MmsApp;->getToastHandler()Lcom/android/mms/MmsApp$ToastHandler;

    move-result-object v3

    new-instance v4, Lcom/android/mms/ui/MyScrollListener$MyRunnable$1;

    invoke-direct {v4, p0, v0}, Lcom/android/mms/ui/MyScrollListener$MyRunnable$1;-><init>(Lcom/android/mms/ui/MyScrollListener$MyRunnable;Lcom/android/mms/ui/MessageCursorAdapter;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v3, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->this$0:Lcom/android/mms/ui/MyScrollListener;

    invoke-static {v3}, Lcom/android/mms/ui/MyScrollListener;->access$000(Lcom/android/mms/ui/MyScrollListener;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "OnScrollListener.run(): listener is wait."

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_4
    :try_start_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    :catch_1
    move-exception v1

    :try_start_5
    invoke-static {}, Lcom/android/mms/ui/MyScrollListener;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "wait has been interrupted."

    invoke-static {v3, v4, v1}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4
.end method

.method public setConversationListAdapter(Lcom/android/mms/ui/MessageCursorAdapter;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/MessageCursorAdapter;

    iput-object p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mListAdapter:Lcom/android/mms/ui/MessageCursorAdapter;

    return-void
.end method

.method public setNeedRun(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->mNeedRun:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MyScrollListener$MyRunnable;->count:I

    :cond_0
    return-void
.end method
