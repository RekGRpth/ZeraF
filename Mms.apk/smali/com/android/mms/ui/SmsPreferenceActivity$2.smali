.class Lcom/android/mms/ui/SmsPreferenceActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "SmsPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SmsPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SmsPreferenceActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/SmsPreferenceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$500(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/preference/ListPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/DialogPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const-string v2, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$600(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$600(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$600(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$700(Lcom/android/mms/ui/SmsPreferenceActivity;)V

    iget-object v2, p0, Lcom/android/mms/ui/SmsPreferenceActivity$2;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$800(Lcom/android/mms/ui/SmsPreferenceActivity;)V

    :cond_2
    return-void
.end method
