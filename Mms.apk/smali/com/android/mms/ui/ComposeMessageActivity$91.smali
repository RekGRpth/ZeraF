.class Lcom/android/mms/ui/ComposeMessageActivity$91;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->showSharePanelOrEmoticonPanel(ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$isShowEmoticon:Z

.field final synthetic val$isShowKeyboard:Z

.field final synthetic val$isShowShare:Z


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;ZZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-boolean p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowShare:Z

    iput-boolean p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowEmoticon:Z

    iput-boolean p4, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowKeyboard:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16100(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    const/16 v1, 0x12c

    :try_start_0
    const-string v2, "Mms/compose"

    const-string v4, "showSharePanelOrEmoticonPanel(): object start wait."

    invoke-static {v2, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16100(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/Object;

    move-result-object v2

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    const-string v2, "Mms/compose"

    const-string v4, "showSharePanelOrEmoticonPanel(): object end wait."

    invoke-static {v2, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    new-instance v3, Lcom/android/mms/ui/ComposeMessageActivity$91$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/ComposeMessageActivity$91$1;-><init>(Lcom/android/mms/ui/ComposeMessageActivity$91;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "Mms/compose"

    const-string v4, "InterruptedException"

    invoke-static {v2, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
