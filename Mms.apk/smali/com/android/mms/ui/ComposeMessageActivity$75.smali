.class Lcom/android/mms/ui/ComposeMessageActivity$75;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->addTextVCardAsync([J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$contactsIds:[J


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;[J)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->val$contactsIds:[J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$3500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v1, ""

    :goto_0
    new-instance v2, Lcom/android/mms/ui/VCardAttachment;

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v2, v3}, Lcom/android/mms/ui/VCardAttachment;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->val$contactsIds:[J

    invoke-virtual {v2, v3, v1}, Lcom/android/mms/ui/VCardAttachment;->getTextVCardString([JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$75;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    new-instance v4, Lcom/android/mms/ui/ComposeMessageActivity$75$1;

    invoke-direct {v4, p0, v0}, Lcom/android/mms/ui/ComposeMessageActivity$75$1;-><init>(Lcom/android/mms/ui/ComposeMessageActivity$75;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const-string v1, "\n"

    goto :goto_0
.end method
