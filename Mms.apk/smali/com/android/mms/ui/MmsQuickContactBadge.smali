.class public Lcom/android/mms/ui/MmsQuickContactBadge;
.super Landroid/widget/QuickContactBadge;
.source "MmsQuickContactBadge.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsGroupAvator:Z

.field private mThreadId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mIsGroupAvator:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mediatek/mms/ipmessage/IpMessageConsts$RemoteActivities;->QUICK_CONTACT:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "thread_id"

    iget-wide v2, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mThreadId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->startRemoteActivity(Landroid/content/Context;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/QuickContactBadge;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setGroupAvator(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mIsGroupAvator:Z

    return-void
.end method

.method public setThreadId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/mms/ui/MmsQuickContactBadge;->mThreadId:J

    return-void
.end method
