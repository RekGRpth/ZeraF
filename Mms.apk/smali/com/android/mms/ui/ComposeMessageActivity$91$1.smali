.class Lcom/android/mms/ui/ComposeMessageActivity$91$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$91;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$91;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowShare:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowShare:Z

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$7700(Lcom/android/mms/ui/ComposeMessageActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowEmoticon:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowEmoticon:Z

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$7800(Lcom/android/mms/ui/ComposeMessageActivity;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowKeyboard:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowKeyboard:Z

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$17400(Lcom/android/mms/ui/ComposeMessageActivity;Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowShare:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowEmoticon:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-boolean v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->val$isShowKeyboard:Z

    if-eqz v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$7900(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$3500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8000(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x48

    div-int/lit16 v1, v1, 0x320

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$3500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$8000(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v1

    mul-int/lit16 v1, v1, 0x8c

    div-int/lit16 v1, v1, 0x320

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    goto :goto_0

    :cond_5
    const-string v0, "Mms/compose"

    const-string v1, "showSharePanelOrEmoticonPanel(): new thread."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$91$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$91;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$91;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$12100(Lcom/android/mms/ui/ComposeMessageActivity;)V

    goto :goto_0
.end method
