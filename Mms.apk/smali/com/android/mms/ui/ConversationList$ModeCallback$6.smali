.class Lcom/android/mms/ui/ConversationList$ModeCallback$6;
.super Ljava/lang/Object;
.source "ConversationList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ConversationList$ModeCallback;->setAllItemChecked(Landroid/view/ActionMode;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

.field final synthetic val$checked:Z


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ConversationList$ModeCallback;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iput-boolean p2, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->val$checked:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$1000(Lcom/android/mms/ui/ConversationList$ModeCallback;)V

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->val$checked:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$3900(Lcom/android/mms/ui/ConversationList$ModeCallback;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/ConversationListAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$6;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$3900(Lcom/android/mms/ui/ConversationList$ModeCallback;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
