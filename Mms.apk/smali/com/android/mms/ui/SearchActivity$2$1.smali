.class Lcom/android/mms/ui/SearchActivity$2$1;
.super Landroid/widget/CursorAdapter;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/SearchActivity$2;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/SearchActivity$2;

.field final synthetic val$addressPos:I

.field final synthetic val$bodyPos:I

.field final synthetic val$charsetPos:I

.field final synthetic val$m_typePos:I

.field final synthetic val$msgBoxPos:I

.field final synthetic val$msgTypePos:I

.field final synthetic val$rowidPos:I

.field final synthetic val$threadIdPos:I

.field final synthetic val$typeIndex:I


# direct methods
.method constructor <init>(Lcom/android/mms/ui/SearchActivity$2;Landroid/content/Context;Landroid/database/Cursor;ZIIIIIIIII)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Z

    iput-object p1, p0, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iput p5, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$charsetPos:I

    iput p6, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$addressPos:I

    iput p7, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$bodyPos:I

    iput p8, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$threadIdPos:I

    iput p9, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$rowidPos:I

    iput p10, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$msgTypePos:I

    iput p11, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$msgBoxPos:I

    iput p12, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$typeIndex:I

    iput p13, p0, Lcom/android/mms/ui/SearchActivity$2$1;->val$m_typePos:I

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const v1, 0x7f0f0070

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v18, v1

    check-cast v18, Landroid/widget/TextView;

    const v1, 0x7f0f0145

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/mms/ui/SearchActivity$TextViewSnippet;

    move-object/from16 v17, v1

    check-cast v17, Lcom/android/mms/ui/SearchActivity$TextViewSnippet;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$charsetPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$addressPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v1, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    new-instance v20, Lcom/google/android/mms/pdu/EncodedStringValue;

    invoke-static {v12}, Lcom/google/android/mms/pdu/PduPersister;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    move-object/from16 v0, v20

    invoke-direct {v0, v11, v1}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(I[B)V

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v12

    :cond_0
    if-eqz v12, :cond_3

    const/4 v1, 0x0

    invoke-static {v12, v1}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v15

    :goto_0
    if-eqz v15, :cond_4

    invoke-virtual {v15}, Lcom/android/mms/data/Contact;->getNameAndNumber()Ljava/lang/String;

    move-result-object v19

    :goto_1
    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$bodyPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v1, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    if-eqz v13, :cond_1

    const-string v1, ""

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    const v1, 0x7f0b0221

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->this$1:Lcom/android/mms/ui/SearchActivity$2;

    iget-object v1, v1, Lcom/android/mms/ui/SearchActivity$2;->this$0:Lcom/android/mms/ui/SearchActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SearchActivity;->access$300(Lcom/android/mms/ui/SearchActivity;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v13, v1}, Lcom/android/mms/ui/SearchActivity$TextViewSnippet;->setText(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$threadIdPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$rowidPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$msgTypePos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$msgBoxPos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$typeIndex:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/mms/ui/SearchActivity$2$1;->val$m_typePos:I

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string v1, "Mms/SearchActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "onQueryComplete msgType = "

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "rowid ="

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/android/mms/ui/SearchActivity$2$1$1;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v10}, Lcom/android/mms/ui/SearchActivity$2$1$1;-><init>(Lcom/android/mms/ui/SearchActivity$2$1;IJIIJI)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    const/4 v15, 0x0

    goto/16 :goto_0

    :cond_4
    const-string v19, ""

    goto/16 :goto_1

    :cond_5
    :try_start_0
    new-instance v14, Ljava/lang/String;

    const-string v1, "ISO-8859-1"

    invoke-virtual {v13, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v14, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v13, v14

    goto/16 :goto_2

    :catch_0
    move-exception v16

    const-string v1, "Mms/SearchActivity"

    const-string v2, "onQueryComplete UnsupportedEncodingException"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040057

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
