.class public Lcom/android/mms/ui/EmoticonPanel;
.super Landroid/widget/LinearLayout;
.source "EmoticonPanel.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;,
        Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;,
        Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/EmoticonPanel"


# instance fields
.field private mColumnArray:[I

.field private mContext:Landroid/content/Context;

.field private mDefaultIndex:I

.field private mDefaultName:[Ljava/lang/String;

.field private mDefaultTab:Landroid/widget/RadioButton;

.field private mDelEmoticon:Landroid/widget/Button;

.field private mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

.field private mDeleteRunnable:Ljava/lang/Runnable;

.field private mDotFirst:Landroid/widget/RadioButton;

.field private mDotForth:Landroid/widget/RadioButton;

.field private mDotSec:Landroid/widget/RadioButton;

.field private mDotThird:Landroid/widget/RadioButton;

.field private mGiftIndex:I

.field private mGiftName:[Ljava/lang/String;

.field private mGiftTab:Landroid/widget/RadioButton;

.field private mHandler:Landroid/os/Handler;

.field private mLandEmotionColumnNumber:I

.field private mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

.field private mNeedQuickDelete:Z

.field private mObject:Ljava/lang/Object;

.field private mOrientation:I

.field private mPortEmotionColumnNumber:I

.field private mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

.field private mSharePanelMain:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mObject:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mNeedQuickDelete:Z

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/EmoticonPanel$3;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDeleteRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mObject:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mNeedQuickDelete:Z

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    iput v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$3;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/EmoticonPanel$3;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDeleteRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/EmoticonPanel;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/mms/ui/EmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/mms/ui/EmoticonPanel;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/mms/ui/EmoticonPanel;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    return p1
.end method

.method static synthetic access$1700(Lcom/android/mms/ui/EmoticonPanel;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/EmoticonPanel;->getDefaultName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/mms/ui/EmoticonPanel;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/EmoticonPanel;->getGiftName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/EmoticonPanel;)Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/EmoticonPanel;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->startDelEmoticon()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/mms/ui/EmoticonPanel;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->stopDelEmoticon()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/mms/ui/EmoticonPanel;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mms/ui/EmoticonPanel;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/mms/ui/EmoticonPanel;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-boolean v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mNeedQuickDelete:Z

    return v0
.end method

.method static synthetic access$900(Lcom/android/mms/ui/EmoticonPanel;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/mms/ui/EmoticonPanel;

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDeleteRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private addDefaultPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040019

    iget-object v7, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f0079

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-ne v5, v9, :cond_3

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/EmoticonPanel;->getDefaultIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;-><init>(Lcom/android/mms/ui/EmoticonPanel;[I)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v5, Lcom/android/mms/ui/EmoticonPanel$6;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/EmoticonPanel$6;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    invoke-virtual {v1, v5}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addDefaultPanel()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x8

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x4

    new-array v0, v3, [Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    aput-object v3, v0, v6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    invoke-direct {p0, v3}, Lcom/android/mms/ui/EmoticonPanel;->calculateDefaultPageCount(I)I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/mms/ui/EmoticonPanel;->addDefaultPage(I)V

    aget-object v3, v0, v1

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v4, Lcom/android/mms/ui/EmoticonPanel$4;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/EmoticonPanel$4;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    return-void
.end method

.method private addGiftPage(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040030

    iget-object v7, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f00c1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-eq v5, v9, :cond_3

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v9

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    :goto_1
    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/EmoticonPanel;->getGiftIconArray(I)[I

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;-><init>(Lcom/android/mms/ui/EmoticonPanel;[I)V

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v5, Lcom/android/mms/ui/EmoticonPanel$7;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/EmoticonPanel$7;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    invoke-virtual {v1, v5}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1
.end method

.method private addGiftPanel()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x8

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setDefaultScreen(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x4

    new-array v0, v3, [Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    aput-object v3, v0, v6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    aput-object v4, v0, v3

    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    invoke-direct {p0, v3}, Lcom/android/mms/ui/EmoticonPanel;->calculateGiftPageCount(I)I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/mms/ui/EmoticonPanel;->addGiftPage(I)V

    aget-object v3, v0, v1

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setToScreen(I)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    new-instance v4, Lcom/android/mms/ui/EmoticonPanel$5;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/EmoticonPanel$5;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    invoke-virtual {v3, v4}, Lcom/android/mms/ui/LevelControlLayout;->setOnScrollToScreen(Lcom/android/mms/ui/LevelControlLayout$OnScrollToScreenListener;)V

    return-void
.end method

.method private calculateDefaultPageCount(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v1, v3, v4

    :goto_0
    sget-object v3, Lcom/android/mms/util/MessageConsts;->defaultIconArr:[I

    array-length v2, v3

    div-int v0, v2, v1

    mul-int v3, v0, v1

    if-le v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v1, v3, v4

    goto :goto_0
.end method

.method private calculateGiftPageCount(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v1, v3, v4

    :goto_0
    sget-object v3, Lcom/android/mms/util/MessageConsts;->giftIconArr:[I

    array-length v2, v3

    div-int v0, v2, v1

    mul-int v3, v0, v1

    if-le v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v1, v3, v4

    goto :goto_0
.end method

.method private getDefaultIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/android/mms/util/MessageConsts;->defaultIconArr:[I

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    iget v6, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v3, v5, v6

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v6

    iget v6, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getDefaultName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v1, v3, v4

    :goto_0
    const/16 v3, 0x14

    if-lt p1, v3, :cond_2

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v1, v3, v4

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultIndex:I

    mul-int/2addr v3, v1

    add-int v0, p1, v3

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultName:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultName:[Ljava/lang/String;

    aget-object v2, v2, v0

    goto :goto_1
.end method

.method private getGiftIconArray(I)[I
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    sget-object v4, Lcom/android/mms/util/MessageConsts;->giftIconArr:[I

    iget v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    iget v6, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v3, v5, v6

    :goto_0
    new-array v0, v3, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    mul-int v5, p1, v3

    add-int v2, v5, v1

    array-length v5, v4

    if-lt v2, v5, :cond_2

    :cond_0
    return-object v0

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v5, v5, v6

    iget v6, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v3, v5, v6

    goto :goto_0

    :cond_2
    mul-int v5, p1, v3

    add-int/2addr v5, v1

    aget v5, v4, v5

    aput v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getGiftName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mOrientation:I

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    mul-int v1, v3, v4

    :goto_0
    const/16 v3, 0x14

    if-lt p1, v3, :cond_2

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mColumnArray:[I

    aget v3, v3, v4

    iget v4, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    mul-int v1, v3, v4

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftIndex:I

    mul-int/2addr v3, v1

    add-int v0, p1, v3

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftName:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftName:[Ljava/lang/String;

    aget-object v2, v2, v0

    goto :goto_1
.end method

.method private startDelEmoticon()V
    .locals 2

    const-string v0, "Mms/EmoticonPanel"

    const-string v1, "Delete one emoticon."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->stopDelEmoticon()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mNeedQuickDelete:Z

    new-instance v0, Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;-><init>(Lcom/android/mms/ui/EmoticonPanel;Lcom/android/mms/ui/EmoticonPanel$1;)V

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private stopDelEmoticon()V
    .locals 2

    const-string v0, "Mms/EmoticonPanel"

    const-string v1, "Stop quick delete."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mNeedQuickDelete:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    invoke-virtual {v0}, Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;->stopThread()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticonThread:Lcom/android/mms/ui/EmoticonPanel$DelEmoticonThread;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0f00a1

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->addDefaultPanel()V

    goto :goto_0

    :cond_2
    const v1, 0x7f0f00a3

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->addGiftPanel()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 5

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v3, 0x7f0f0099

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/mms/ui/LevelControlLayout;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    iput-object p0, p0, Lcom/android/mms/ui/EmoticonPanel;->mSharePanelMain:Landroid/widget/LinearLayout;

    const v3, 0x7f0f009b

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotFirst:Landroid/widget/RadioButton;

    const v3, 0x7f0f009c

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotSec:Landroid/widget/RadioButton;

    const v3, 0x7f0f009d

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotThird:Landroid/widget/RadioButton;

    const v3, 0x7f0f009e

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDotForth:Landroid/widget/RadioButton;

    const v3, 0x7f0f00a1

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    const v3, 0x7f0f00a3

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060022

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultName:[Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060023

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftName:[Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0a0000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    :goto_0
    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    :goto_1
    new-instance v2, Lcom/android/mms/ui/EmoticonPanel$1;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/EmoticonPanel$1;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    const v3, 0x7f0f00a0

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v3, 0x7f0f00a2

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v3, 0x7f0f00a4

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticon:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDelEmoticon:Landroid/widget/Button;

    new-instance v4, Lcom/android/mms/ui/EmoticonPanel$2;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/EmoticonPanel$2;-><init>(Lcom/android/mms/ui/EmoticonPanel;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    invoke-virtual {v3, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    invoke-virtual {v3, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/android/mms/ui/EmoticonPanel;->resetShareItem()V

    return-void

    :cond_0
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mPortEmotionColumnNumber:I

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/mms/ui/EmoticonPanel;->mLandEmotionColumnNumber:I

    goto :goto_1
.end method

.method public recycleView()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    return-void
.end method

.method public resetShareItem()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mDefaultTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->addDefaultPanel()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mScrollLayout:Lcom/android/mms/ui/LevelControlLayout;

    invoke-virtual {v0}, Lcom/android/mms/ui/LevelControlLayout;->autoRecovery()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel;->mGiftTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mms/ui/EmoticonPanel;->addGiftPanel()V

    goto :goto_0
.end method

.method public setEditEmoticonListener(Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    iput-object p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mListener:Lcom/android/mms/ui/EmoticonPanel$EditEmoticonListener;

    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/EmoticonPanel;->mHandler:Landroid/os/Handler;

    return-void
.end method
