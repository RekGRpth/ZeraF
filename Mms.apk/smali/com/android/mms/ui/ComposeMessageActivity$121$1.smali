.class Lcom/android/mms/ui/ComposeMessageActivity$121$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$121;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$121;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$121;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$3500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$121;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0xe9

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$121;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-virtual {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->hiddeInvitePanel()V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$121;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getChatManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ChatManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$121$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$121;

    iget-object v1, v1, Lcom/android/mms/ui/ComposeMessageActivity$121;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$2600(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/mms/ipmessage/ChatManager;->handleInviteDlg(J)Z

    return-void
.end method
