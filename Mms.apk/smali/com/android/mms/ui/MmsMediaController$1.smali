.class Lcom/android/mms/ui/MmsMediaController$1;
.super Ljava/lang/Object;
.source "MmsMediaController.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MmsMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MmsMediaController;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MmsMediaController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    iget-object v0, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    invoke-static {v0}, Lcom/android/mms/ui/MmsMediaController;->access$000(Lcom/android/mms/ui/MmsMediaController;)V

    iget-object v0, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    iget-boolean v0, v0, Lcom/android/mms/ui/MmsMediaController;->mShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    iget-object v0, v0, Lcom/android/mms/ui/MmsMediaController;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    iget-object v1, v1, Lcom/android/mms/ui/MmsMediaController;->mDecor:Landroid/view/View;

    iget-object v2, p0, Lcom/android/mms/ui/MmsMediaController$1;->this$0:Lcom/android/mms/ui/MmsMediaController;

    iget-object v2, v2, Lcom/android/mms/ui/MmsMediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method
