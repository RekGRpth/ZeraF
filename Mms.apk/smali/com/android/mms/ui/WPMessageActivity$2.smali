.class Lcom/android/mms/ui/WPMessageActivity$2;
.super Ljava/lang/Object;
.source "WPMessageActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/WPMessageActivity;->initMessageList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/WPMessageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v4, 0x1

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p2, Lcom/android/mms/ui/WPMessageListItem;

    invoke-virtual {p2}, Lcom/android/mms/ui/WPMessageListItem;->getMessageItem()Lcom/android/mms/ui/WPMessageItem;

    move-result-object v3

    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/WPMessageActivity;->access$300(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/ActionMode;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/android/mms/ui/WPMessageItem;->isChecked()Z

    move-result v1

    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/WPMessageActivity;->access$400(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

    move-result-object v5

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {v5, p3, v4}, Lcom/android/mms/ui/WPMessageActivity$ModeCallback;->setItemChecked(IZ)V

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v4, v4, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v4, v4, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/WPMessageListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v5, v3}, Lcom/android/mms/ui/WPMessageActivity;->access$500(Landroid/content/Context;Lcom/android/mms/ui/WPMessageItem;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/mms/ui/WPMessageActivity$2;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b01e7

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/android/mms/ui/WPMessageActivity$2$1;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/WPMessageActivity$2$1;-><init>(Lcom/android/mms/ui/WPMessageActivity$2;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b01e8

    new-instance v5, Lcom/android/mms/ui/WPMessageActivity$2$2;

    invoke-direct {v5, p0, v3}, Lcom/android/mms/ui/WPMessageActivity$2$2;-><init>(Lcom/android/mms/ui/WPMessageActivity$2;Lcom/android/mms/ui/WPMessageItem;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    invoke-virtual {v3}, Lcom/android/mms/ui/WPMessageItem;->markAsRead()V

    goto :goto_0
.end method
