.class Lcom/android/mms/ui/FolderModeSmsViewer$2;
.super Ljava/lang/Object;
.source "FolderModeSmsViewer.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderModeSmsViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/FolderModeSmsViewer;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/FolderModeSmsViewer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 5
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v4, 0x0

    const v1, 0x7f0b0234

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    new-instance v0, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;-><init>(Lcom/android/mms/ui/FolderModeSmsViewer;Lcom/android/mms/ui/FolderModeSmsViewer$1;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1, p1, v0}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$100(Lcom/android/mms/ui/FolderModeSmsViewer;Landroid/view/ContextMenu;Lcom/android/mms/ui/FolderModeSmsViewer$MsgListMenuClickListener;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$200(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/16 v1, 0x18

    const v2, 0x7f0b0233

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v1, "Mms/FolderModeSmsViewer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSimCount ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v3}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$300(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$300(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v1

    if-lez v1, :cond_0

    const/16 v1, 0x1f

    const v2, 0x7f0b0035

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    const/16 v1, 0x22

    const v2, 0x7f0b00ad

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v1, 0x11

    const v2, 0x7f0b0224

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/16 v1, 0x12

    const v2, 0x7f0b0226

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$200(Lcom/android/mms/ui/FolderModeSmsViewer;)I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/FolderModeSmsViewer$2;->this$0:Lcom/android/mms/ui/FolderModeSmsViewer;

    invoke-static {v1}, Lcom/android/mms/ui/FolderModeSmsViewer;->access$400(Lcom/android/mms/ui/FolderModeSmsViewer;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x1d

    const v2, 0x7f0b02be

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v1, 0x1c

    const v2, 0x7f0b02bd

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method
