.class Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;
.super Ljava/lang/Object;
.source "MessagingPreferenceActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MessagingPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositiveButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MessagingPreferenceActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/MessagingPreferenceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/MessagingPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/MessagingPreferenceActivity;Lcom/android/mms/ui/MessagingPreferenceActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/MessagingPreferenceActivity;
    .param p2    # Lcom/android/mms/ui/MessagingPreferenceActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;-><init>(Lcom/android/mms/ui/MessagingPreferenceActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-static {}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->getInstance()Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/MessagingPreferenceActivity;

    iget-object v1, p0, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/MessagingPreferenceActivity;

    invoke-static {v1}, Lcom/android/mms/ui/MessagingPreferenceActivity;->access$2000(Lcom/android/mms/ui/MessagingPreferenceActivity;)Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlot()I

    move-result v1

    iput v1, v2, Lcom/android/mms/ui/MessagingPreferenceActivity;->mSlotId:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener$1;

    invoke-direct {v2, p0, v0}, Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener$1;-><init>(Lcom/android/mms/ui/MessagingPreferenceActivity$PositiveButtonListener;Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method
