.class Lcom/android/mms/ui/ManageSimMessages$7;
.super Ljava/lang/Object;
.source "ManageSimMessages.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ManageSimMessages;->confirmMultiDelete()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ManageSimMessages;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ManageSimMessages;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v3}, Lcom/mediatek/encapsulation/android/provider/EncapsulatedSettings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v1}, Lcom/android/mms/ui/ManageSimMessages;->showAirPlaneToast()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->showDialog(I)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/mms/ui/ManageSimMessages$7$1;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/ManageSimMessages$7$1;-><init>(Lcom/android/mms/ui/ManageSimMessages$7;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v1}, Lcom/android/mms/ui/ManageSimMessages;->access$500(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v1

    iput-boolean v3, v1, Lcom/android/mms/ui/MessageListAdapter;->mIsDeleteMode:Z

    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v1}, Lcom/android/mms/ui/ManageSimMessages;->access$1700(Lcom/android/mms/ui/ManageSimMessages;)Landroid/app/ActionBar;

    move-result-object v1

    const/16 v2, 0xa

    const/16 v3, 0x1a

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v1, p0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    iput-boolean v4, v1, Lcom/android/mms/ui/ManageSimMessages;->isDeleting:Z

    goto :goto_0
.end method
