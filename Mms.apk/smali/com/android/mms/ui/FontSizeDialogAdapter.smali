.class public Lcom/android/mms/ui/FontSizeDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "FontSizeDialogAdapter.java"


# instance fields
.field private mChoices:[Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mTV:Landroid/widget/CheckedTextView;

.field private mValues:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mChoices:[Ljava/lang/String;

    iput-object p3, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mValues:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mChoices:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mChoices:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mTV:Landroid/widget/CheckedTextView;

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mTV:Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mChoices:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mChoices:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mTV:Landroid/widget/CheckedTextView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/android/mms/ui/FontSizeDialogAdapter;->mValues:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_0
    return-object p2
.end method
