.class Lcom/android/mms/ui/ManageSimMessages$7$1;
.super Ljava/lang/Object;
.source "ManageSimMessages.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ManageSimMessages$7;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ManageSimMessages$7;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ManageSimMessages$7;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    iget-object v0, v0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v0}, Lcom/android/mms/ui/ManageSimMessages;->access$500(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v6

    iget-object v0, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    iget-object v0, v0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v0}, Lcom/android/mms/ui/ManageSimMessages;->access$500(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSimMsgItemList()Ljava/util/Map;

    move-result-object v13

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v6, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    iget-object v0, v0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v0}, Lcom/android/mms/ui/ManageSimMessages;->access$500(Lcom/android/mms/ui/ManageSimMessages;)Lcom/android/mms/ui/MessageListAdapter;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    iget-object v0, v0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v0, v7}, Lcom/android/mms/ui/ManageSimMessages;->access$1500(Lcom/android/mms/ui/ManageSimMessages;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    const-string v0, ";"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v0, v8, v1

    invoke-interface {v13, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    aget-object v0, v8, v1

    invoke-interface {v13, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v10, 0x0

    :goto_1
    array-length v0, v8

    if-ge v10, v0, :cond_0

    aget-object v0, v8, v10

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mms/ui/ManageSimMessages$7$1;->this$1:Lcom/android/mms/ui/ManageSimMessages$7;

    iget-object v0, v0, Lcom/android/mms/ui/ManageSimMessages$7;->this$0:Lcom/android/mms/ui/ManageSimMessages;

    invoke-static {v0}, Lcom/android/mms/ui/ManageSimMessages;->access$100(Lcom/android/mms/ui/ManageSimMessages;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/mms/ui/ManageSimMessages;->access$1600()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "ForMultiDelete"

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method
