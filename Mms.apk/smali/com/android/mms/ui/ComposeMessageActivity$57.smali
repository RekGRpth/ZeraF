.class Lcom/android/mms/ui/ComposeMessageActivity$57;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->startMsgListQuery(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$conversationUri:Landroid/net/Uri;

.field final synthetic val$importantSelection:Ljava/lang/String;

.field final synthetic val$order:Ljava/lang/String;

.field final synthetic val$threadId:J


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-wide p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$threadId:J

    iput-object p4, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$conversationUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$importantSelection:Ljava/lang/String;

    iput-object p6, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$order:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4000(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/ComposeMessageActivity$BackgroundQueryHandler;

    move-result-object v0

    const/16 v1, 0x2537

    iget-wide v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$threadId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$conversationUri:Landroid/net/Uri;

    sget-object v4, Lcom/android/mms/ui/MessageListAdapter;->PROJECTION:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$importantSelection:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/mms/ui/ComposeMessageActivity$57;->val$order:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
