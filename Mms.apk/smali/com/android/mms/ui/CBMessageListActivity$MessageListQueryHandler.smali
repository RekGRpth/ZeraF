.class final Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;
.super Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;
.source "CBMessageListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/CBMessageListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MessageListQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/CBMessageListActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/CBMessageListActivity;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    const/16 v1, 0x709

    if-ne p1, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->progress()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->dismissProgressDialog()V

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/data/Conversation;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    :cond_2
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/data/CBMessage;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/transaction/CBMessagingNotification;->updateNewMessageIndicator(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$100(Lcom/android/mms/ui/CBMessageListActivity;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->onContentChanged()V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v1

    iput-boolean v2, v1, Lcom/android/mms/ui/CBMessageListAdapter;->mIsDeleteMode:Z

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$700(Lcom/android/mms/ui/CBMessageListActivity;)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$800(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/view/Menu;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v1}, Lcom/android/mms/ui/CBMessageListActivity;->access$800(Lcom/android/mms/ui/CBMessageListActivity;)Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->progress()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->dismissProgressDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x76e
        :pswitch_0
    .end packed-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 9
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v3, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v3, "Mms/Cb"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete called with unknown token "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v3}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/android/mms/ui/CBMessageListAdapter;->initListMap(Landroid/database/Cursor;)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v3}, Lcom/android/mms/ui/CBMessageListActivity;->access$200(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v3}, Lcom/android/mms/ui/CBMessageListActivity;->access$000(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/mms/data/Conversation;->setMessageCount(I)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v3}, Lcom/android/mms/ui/CBMessageListActivity;->access$700(Lcom/android/mms/ui/CBMessageListActivity;)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :pswitch_2
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    new-instance v6, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;

    iget-object v5, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v5}, Lcom/android/mms/ui/CBMessageListActivity;->access$600(Lcom/android/mms/ui/CBMessageListActivity;)Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;

    move-result-object v5

    iget-object v7, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-direct {v6, v1, v2, v5, v7}, Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;-><init>(JLandroid/content/AsyncQueryHandler;Landroid/content/Context;)V

    const-wide/16 v7, -0x1

    cmp-long v5, v1, v7

    if-nez v5, :cond_0

    move v5, v3

    :goto_1
    if-eqz p3, :cond_1

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_1

    :goto_2
    iget-object v4, p0, Lcom/android/mms/ui/CBMessageListActivity$MessageListQueryHandler;->this$0:Lcom/android/mms/ui/CBMessageListActivity;

    invoke-static {v6, v5, v3, v4}, Lcom/android/mms/ui/CBMessageListActivity;->confirmDeleteMessageDialog(Lcom/android/mms/ui/CBMessageListActivity$DeleteMessageListener;ZZLandroid/content/Context;)V

    goto :goto_0

    :cond_0
    move v5, v4

    goto :goto_1

    :cond_1
    move v3, v4

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x76d
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
