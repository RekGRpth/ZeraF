.class Lcom/android/mms/ui/ComposeMessageActivity$67;
.super Landroid/os/Handler;
.source "ComposeMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ComposeMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const v6, 0x7f0b0039

    const/4 v7, 0x0

    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    const-string v3, "Mms/compose"

    const-string v4, "inUIHandler msg unhandled."

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :sswitch_0
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    const v4, 0x7f0b0038

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    const v6, 0x7f0b02db

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_3
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget v3, p1, Landroid/os/Message;->arg1:I

    int-to-long v0, v3

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3, v2, v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14000(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;J)V

    goto :goto_0

    :sswitch_4
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14102(Lcom/android/mms/ui/ComposeMessageActivity;Z)Z

    goto :goto_0

    :sswitch_5
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1100(Lcom/android/mms/ui/ComposeMessageActivity;)V

    goto :goto_0

    :sswitch_6
    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5200(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/android/mms/ui/ComposeMessageActivity$67$1;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/ComposeMessageActivity$67$1;-><init>(Lcom/android/mms/ui/ComposeMessageActivity$67;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_5
        0x66 -> :sswitch_3
        0x6a -> :sswitch_0
        0x6c -> :sswitch_1
        0x6e -> :sswitch_2
        0x70 -> :sswitch_4
        0x1f4 -> :sswitch_6
    .end sparse-switch
.end method
