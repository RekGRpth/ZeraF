.class Lcom/android/mms/ui/ComposeMessageActivity$67$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$67;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$67;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v2, -0x1

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14200(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/util/HashMap;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14300(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14200(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14200(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/util/HashMap;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14200(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/util/HashMap;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v6, v6, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14400(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v6

    if-ne v5, v6, :cond_3

    move v2, v0

    :cond_0
    const/4 v5, -0x1

    if-eq v2, v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/app/AlertDialog;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14500(Lcom/android/mms/ui/ComposeMessageActivity;)Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    const v5, 0x7f0f0159

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    const v6, 0x7f0b0084

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$67$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$67;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$67;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14200(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method
